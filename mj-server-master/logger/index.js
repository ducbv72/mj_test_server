const pino = require('pino');

const logger = pino(
  {
    level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
  },

  pino.destination(`${__dirname}/logger.log`),
);

module.exports = logger;
