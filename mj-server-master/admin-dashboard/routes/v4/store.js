const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const fs = require('fs');
const router = express.Router();
const formidable = require('formidable');
const mime = require('mime-type/with-db');
const Config = require('../../../config');

router.get(
  '/admin/stores',
  common.restrict,
  common.checkAccess,
  (req, res, next) => {
    const db = req.app.db;
    // get the top results
    db.stores
      .find({})
      .limit(10)
      .toArray((err, topResults) => {
        if (err) {
          console.info(err.stack);
        }
        res.render('stores', {
          title: 'Stores',
          results: topResults,
          session: req.session,
          admin: true,
          config: req.app.config,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          helpers: req.handlebars.helpers,
          parentMenu: 'store',
          menu: 'list'
        });
      });
  },
);

router.get(
  '/admin/store/filter/:search',
  common.restrict,
  common.checkAccess,
  (req, res, next) => {
    const db = req.app.db;
    const searchTerm = req.params.search;
    const storesIndex = req.app.storesIndex;

    const lunrIdArray = [];
    storesIndex.search(searchTerm).forEach((id) => {
      lunrIdArray.push(common.getId(id.ref));
    });

    // we search on the lunr indexes
    db.stores.find({ _id: { $in: lunrIdArray } }).toArray((err, results) => {
      if (err) {
        console.error(colors.red('Error searching', err));
      }
      res.render('stores', {
        title: 'Results',
        results: results,
        admin: true,
        config: req.app.config,
        session: req.session,
        searchTerm: searchTerm,
        message: common.clearSessionValue(req.session, 'message'),
        messageType: common.clearSessionValue(req.session, 'messageType'),
        helpers: req.handlebars.helpers,
      });
    });
  },
);

// insert form
router.get(
  '/admin/store/new',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('store_new', {
      title: 'New Store',
      session: req.session,
      storeName: common.clearSessionValue(req.session, 'storeName'),
      storeHandle: common.clearSessionValue(req.session, 'storeHandle'),
      isFeatureCollection: common.clearSessionValue(
        req.session,
        'isFeatureCollection',
      ),
      isHideStore: common.clearSessionValue(req.session, 'isHideStore'),
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
      parentMenu: 'store',
      menu: 'new'
    });
  },
);

// insert new product form action
router.post(
  '/admin/store/insert',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const form = new formidable.IncomingForm();
    form.uploadDir = './public/uploads/stores/';
    form.parse(req, async (err, fields, files) => {
      if (err) {
        console.log(colors.red('Error inserting document: ' + err));
        // keep the current stuff
        req.session.storeName = req.body.storeName;
        req.session.storeHandle = req.body.storeHandle;
        req.session.isFeatureCollection = req.body.isFeatureCollection;
        req.session.isHideStore = req.body.isHideStore;
        req.session.message = 'Error: Inserting store';
        req.session.messageType = 'danger';
        // redirect to insert
        res.redirect('/admin/store/new');
        return;
      }
      if (files.file.size === 0 || files.file_biger_size.size === 0) {
        req.session.message = 'Please allow upload image file';
        req.session.messageType = 'danger';
        res.redirect('/admin/store/new');
        return;
      }
      // Get the mime type of the file
      const mimeTypeBigfile = mime.lookup(files.file_biger_size.name);
      if (
        !common.allowedMimeType.includes(mimeTypeBigfile) ||
        files.file_biger_size.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(files.file_biger_size.path);
        // Redirect to error
        req.session.message =
          'File type not allowed or too large. Please try again.';
        req.session.messageType = 'danger';
        res.redirect('/admin/store/new');
        return;
      }

      // Get the mime type of the file
      const mimeType = mime.lookup(files.file.name);
      if (
        !common.allowedMimeType.includes(mimeType) ||
        files.file.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(files.file.path);
        // Redirect to error
        req.session.message =
          'File type not allowed or too large. Please try again.';
        req.session.messageType = 'danger';
        res.redirect('/admin/store/new');
        return;
      }

      //upload bigger file
      const s3biggerFile = await common.uploadFileToS3(
        files.file_biger_size.path,
        Config.S3.storeImageFolder,
      );
      const s3NormalFile = await common.uploadFileToS3(
        files.file.path,
        Config.S3.storeImageFolder,
      );
      fs.unlinkSync(files.file_biger_size.path);
      fs.unlinkSync(files.file.path);
      const doc = {
        name: common.cleanHtml(fields.storeName),
        handle: common.cleanHtml(fields.storeHandle),
        isFeatureCollection: common.checkboxBool(fields.isFeatureCollection),
        isHideStore: common.checkboxBool(fields.isHideStore),
        image: s3NormalFile,
        image_bigger: s3biggerFile,
      };
      db.stores.insert(doc, (err, newDoc) => {
        if (err) {
          console.log(colors.red('Error inserting document: ' + err));
          // keep the current stuff
          req.session.storeName = req.body.storeName;
          req.session.storeHandle = req.body.storeHandle;
          req.session.isFeatureCollection = req.body.isFeatureCollection;
          req.session.isHideStore = req.body.isHideStore;
          req.session.message = 'Error: Inserting store';
          req.session.messageType = 'danger';
          // redirect to insert
          res.redirect('/admin/store/new');
        } else {
          // get the new ID
          const newId = newDoc.insertedIds[0];
          req.session.message = 'New store successfully created';
          req.session.messageType = 'success';

          // redirect to new doc
          res.redirect('/admin/store/edit/' + newId);
        }
      });
    });
  },
);

// render the editor
router.get(
  '/admin/store/edit/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    db.stores.findOne({ _id: common.getId(req.params.id) }, (err, result) => {
      if (err) {
        console.info(err.stack);
      }
      res.render('store_edit', {
        title: 'Edit Store',
        store: result,
        admin: true,
        session: req.session,
        message: common.clearSessionValue(req.session, 'message'),
        messageType: common.clearSessionValue(req.session, 'messageType'),
        config: req.app.config,
        editor: true,
        helpers: req.handlebars.helpers,
      });
    });
  },
);

router.get(
  '/admin/store/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.redirect('/admin/stores');
  },
);

// Update an existing product form action
router.post(
  '/admin/store/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const form = new formidable.IncomingForm();
    form.uploadDir = './public/uploads/stores/';
    form.parse(req, async (err, fields, files) => {
      const [storeItem] = await Promise.all([
        db.stores.findOne({ _id: common.getId(fields.storeId) }),
      ]);

      if (!storeItem) {
        req.session.message = 'Failed updating store.';
        req.session.messageType = 'danger';
        res.redirect('/admin/store/edit/' + fields.storeId);
        return;
      }
      let biggerFileName = '';
      let normalFileName = '';
      if (files.file_biger_size.size > 0) {
        // Get the mime type of the file
        const mimeType = mime.lookup(files.file_biger_size.name);
        if (
          !common.allowedMimeType.includes(mimeType) ||
          files.file_biger_size.size > common.fileSizeLimit
        ) {
          // Remove temp file
          fs.unlinkSync(files.file_biger_size.path);
          // Redirect to error
          req.session.message =
            'File type not allowed or too large. Please try again.';
          req.session.messageType = 'danger';
          res.redirect('/admin/store/edit/' + fields.storeId);
          return;
        }
        //upload bigger file
        biggerFileName = await common.uploadFileToS3(
          files.file_biger_size.path,
          Config.S3.storeImageFolder,
        );
        fs.unlinkSync(files.file_biger_size.path);
      }
      if (files.file.size > 0) {
        // Get the mime type of the file
        const mimeType = mime.lookup(files.file_biger_size.name);
        if (
          !common.allowedMimeType.includes(mimeType) ||
          files.file_biger_size.size > common.fileSizeLimit
        ) {
          // Remove temp file
          fs.unlinkSync(files.file_biger_size.path);
          // Redirect to error
          req.session.message =
            'File type not allowed or too large. Please try again.';
          req.session.messageType = 'danger';
          res.redirect('/admin/store/edit/' + fields.storeId);
          return;
        }
        normalFileName = await common.uploadFileToS3(
          files.file.path,
          Config.S3.storeImageFolder,
        );
        fs.unlinkSync(files.file.path);
      }

      const storeUpdate = {
        name: common.cleanHtml(fields.storeName),
        handle: common.cleanHtml(fields.storeHandle),
        isFeatureCollection: common.checkboxBool(fields.isFeatureCollection),
        isHideStore: common.checkboxBool(fields.isHideStore),
        image: storeItem.image,
        image_bigger: storeItem.image_bigger,
      };
      if (biggerFileName.length > 0) {
        storeUpdate.image_bigger = biggerFileName;
      }
      if (normalFileName.length > 0) {
        storeUpdate.image = normalFileName;
      }
      db.stores.update(
        { _id: common.getId(storeItem._id) },
        { $set: storeUpdate },
        {},
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed to save store: ' + err));
            req.session.message = 'Failed to save. Please try again';
            req.session.messageType = 'danger';
            res.redirect('/admin/store/edit/' + fields.storeId);
          } else {
            // Update the index
            req.session.message = 'Successfully saved';
            req.session.messageType = 'success';
            res.redirect('/admin/store/edit/' + fields.storeId);
          }
        },
      );
    });
  },
);

// delete product
router.get(
  '/admin/store/delete/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    // remove the article
    db.stores.remove(
      { _id: common.getId(req.params.id) },
      {},
      (err, numRemoved) => {
        if (err) {
          console.info(err.stack);
        }
        req.session.message = 'Store successfully deleted';
        req.session.messageType = 'success';
        res.redirect('/admin/stores');
      },
    );
  },
);

router.get(
  '/admin/stores/list',
  common.restrict,
  common.checkAccess,
  (req, res, next) => {
    const db = req.app.db;
    // get the top results
    db.stores.find({}).toArray((err, topResults) => {
      if (err) {
        res.status(400).json({ message: 'not found', data: [] });
      }
      res.status(200).json({ message: 'ok', data: topResults });
    });
  },
);

module.exports = router;
