const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const bcrypt = require('bcryptjs');
const url = require('url');
const router = express.Router();
const authenticator = require('authenticator');
const QRCode = require('qr-image');
const fs = require('fs');
const Promise = require('promise');
const path = require('path');

router.get(
  '/admin/users',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    let users;
    const roleGroup = req.session.groupRole;
    const isSuperAdmin = req.session.isSuperAdmin || req.session.isAdmin;
    // if(isSuperAdmin){
    [users] = await Promise.all([db.users.find({}).toArray()]);
    // }else{
    //     [users] = await Promise.all([
    //         db.users.find({_id: common.getId(req.session.userId)}).toArray()
    //     ]);
    // }

    res.render('users', {
      title: 'Users',
      menu: 'setting',
      users: users,
      admin: true,
      config: req.app.config,
      isAdmin: req.session.isAdmin,
      helpers: req.handlebars.helpers,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
    });
  },
);

// edit user
router.get(
  '/admin/user/edit/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    db.users.findOne(
      { _id: common.getId(req.params.id) },
      async (err, user) => {
        if (err) {
          console.info(err.stack);
        }
        // if the user we want to edit is not the current logged in user and the current user is not
        // an admin we render an access denied message
        if (
          user.userEmail !== req.session.user &&
          req.session.isAdmin === false
        ) {
          req.session.message = 'Access denied';
          req.session.messageType = 'danger';
          res.redirect('/Users/');
          return;
        }

        if (!user.formattedKey) {
          const formattedKey = authenticator.generateKey();
          user.formattedKey = formattedKey;
          const formattedToken = authenticator.generateToken(formattedKey);

          authenticator.verifyToken(formattedKey, formattedToken);

          authenticator.verifyToken(formattedKey, '000 000');

          const url = authenticator.generateTotpUri(
            formattedKey,
            user.userEmail,
            'MightyJaxx',
            'SHA1',
            6,
            30,
          );

          const qr_svg = QRCode.image(url, { type: 'png' });
          const mainPath = './public/qrcode-authen/';
          const fileName = new Date().getTime().toString() + '.png';
          const dir = mainPath + fileName;
          if (!fs.existsSync(mainPath)) {
            fs.mkdirSync(mainPath);
          }
          qr_svg.pipe(fs.createWriteStream(dir));
          user.qrcodeImage = '/qrcode-authen/' + fileName;

          const [newDoc] = await Promise.all([
            db.users.update({ _id: user._id }, { $set: user }, {}),
          ]);
        }

        // save user key

        res.render('user_edit', {
          title: 'User edit',
          menu: 'setting',
          user: user,
          admin: true,
          session: req.session,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          helpers: req.handlebars.helpers,
          config: req.app.config,
        });
      },
    );
  },
);

// users new
router.get(
  '/admin/user/new',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('user_new', {
      title: 'User - New',
      menu: 'setting',
      admin: true,
      session: req.session,
      helpers: req.handlebars.helpers,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      config: req.app.config,
    });
  },
);

// delete user
router.get(
  '/admin/user/delete/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    if (req.session.isAdmin === true) {
      db.users.remove(
        { _id: common.getId(req.params.id) },
        {},
        (err, numRemoved) => {
          if (err) {
            console.info(err.stack);
          }
          req.session.message = 'User deleted.';
          req.session.messageType = 'success';
          res.redirect('/admin/users');
        },
      );
    } else {
      req.session.message = 'Access denied.';
      req.session.messageType = 'danger';
      res.redirect('/admin/users');
    }
  },
);

// update a user
router.post(
  '/admin/user/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    let isAdmin = req.body.groupRole === 'admin';

    // get the user we want to update
    db.users.findOne({ _id: common.getId(req.body.userId) }, (err, user) => {
      if (err) {
        console.info(err.stack);
      }

      // If the current user changing own account ensure isAdmin retains existing
      if (user.userEmail === req.session.user) {
        isAdmin = user.isAdmin;
      }

      // if the user we want to edit is not the current logged in user and the current user is not
      // an admin we render an access denied message
      if (
        user.userEmail !== req.session.user &&
        req.session.isAdmin === false
      ) {
        req.session.message = 'Access denied';
        req.session.messageType = 'danger';
        res.redirect('/admin/users/');
        return;
      }

      // create the update doc
      const updateDoc = {};
      updateDoc.isAdmin = isAdmin;
      updateDoc.usersName = req.body.usersName;
      updateDoc.formattedKey = user.formattedKey;
      updateDoc.qrcodeImage = user.qrcodeImage;
      updateDoc.groupRole = req.body.groupRole;
      updateDoc.isSuperAdmin = user.isSuperAdmin ? user.isSuperAdmin : false;
      if (req.body.userPassword) {
        updateDoc.userPassword = bcrypt.hashSync(req.body.userPassword);
      }

      if (!updateDoc.formattedKey) {
        const formattedKey = authenticator.generateKey();
        updateDoc.formattedKey = formattedKey;
        const formattedToken = authenticator.generateToken(formattedKey);

        authenticator.verifyToken(formattedKey, formattedToken);

        authenticator.verifyToken(formattedKey, '000 000');

        const url = authenticator.generateTotpUri(
          formattedKey,
          updateDoc.userEmail,
          'MightyJaxx',
          'SHA1',
          6,
          30,
        );

        const qr_svg = QRCode.image(url, { type: 'png' });
        const mainPath = './public/qrcode-authen/';
        const fileName = new Date().getTime().toString() + '.png';
        const dir = mainPath + fileName;
        if (!fs.existsSync(mainPath)) {
          fs.mkdirSync(mainPath);
        }
        qr_svg.pipe(fs.createWriteStream(dir));
        updateDoc.qrcodeImage = '/qrcode-authen/' + fileName;
      }

      db.users.update(
        { _id: common.getId(req.body.userId) },
        {
          $set: updateDoc,
        },
        { multi: false },
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed updating user: ' + err));
            req.session.message = 'Failed to update user';
            req.session.messageType = 'danger';
            res.redirect('/admin/user/edit/' + req.body.userId);
          } else {
            // show the view
            req.session.message = 'User account updated.';
            req.session.messageType = 'success';
            res.redirect('/admin/user/edit/' + req.body.userId);
          }
        },
      );
    });
  },
);

// update a user
router.post(
  '/admin/profile/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    // get the user we want to update
    db.users.findOne({ _id: common.getId(req.body.userId) }, (err, user) => {
      if (err) {
        console.info(err.stack);
      }

      // create the update doc
      const updateDoc = {};
      updateDoc.isAdmin = user.isAdmin;
      updateDoc.usersName = req.body.usersName;
      updateDoc.formattedKey = user.formattedKey;
      updateDoc.qrcodeImage = user.qrcodeImage;
      updateDoc.groupRole = user.groupRole;
      updateDoc.isSuperAdmin = user.isSuperAdmin ? user.isSuperAdmin : false;
      if (req.body.userPassword) {
        updateDoc.userPassword = bcrypt.hashSync(req.body.userPassword);
      }

      if (!updateDoc.formattedKey) {
        const formattedKey = authenticator.generateKey();
        updateDoc.formattedKey = formattedKey;
        const formattedToken = authenticator.generateToken(formattedKey);

        authenticator.verifyToken(formattedKey, formattedToken);

        authenticator.verifyToken(formattedKey, '000 000');

        const url = authenticator.generateTotpUri(
          formattedKey,
          updateDoc.userEmail,
          'MightyJaxx',
          'SHA1',
          6,
          30,
        );

        const qr_svg = QRCode.image(url, { type: 'png' });
        const mainPath = './public/qrcode-authen/';
        const fileName = new Date().getTime().toString() + '.png';
        const dir = mainPath + fileName;
        if (!fs.existsSync(mainPath)) {
          fs.mkdirSync(mainPath);
        }
        qr_svg.pipe(fs.createWriteStream(dir));
        updateDoc.qrcodeImage = '/qrcode-authen/' + fileName;
      }

      db.users.update(
        { _id: common.getId(req.body.userId) },
        {
          $set: updateDoc,
        },
        { multi: false },
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed updating user: ' + err));
            req.session.message = 'Failed to update user';
            req.session.messageType = 'danger';
            res.redirect('/admin/user/edit/' + req.body.userId);
          } else {
            // show the view
            req.session.message = 'User account updated.';
            req.session.messageType = 'success';
            res.redirect('/admin/user/edit/' + req.body.userId);
          }
        },
      );
    });
  },
);

// insert a user
router.post(
  '/admin/user/insert',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    // set the account to admin if using the setup form. Eg: First user account
    const urlParts = url.parse(req.header('Referer'));

    // Check number of users
    db.users.count({}, (err, userCount) => {
      let isAdmin = false;

      // if no users, setup user as admin
      if (userCount === 0) {
        isAdmin = true;
      }
      const mainPath = './public/qrcode-authen/';
      const fileName = new Date().getTime().toString() + '.png';
      const filePath = 'public/qrcode-authen/' + fileName;
      const urlImage = '/qrcode-authen/' + fileName;
      const doc = {
        usersName: req.body.usersName,
        userEmail: req.body.userEmail,
        groupRole: req.body.groupRole,
        isSuperAdmin: false,
        userPassword: bcrypt.hashSync(req.body.userPassword, 10),
        isAdmin: isAdmin,
      };

      if (!doc.formattedKey) {
        const formattedKey = authenticator.generateKey();
        doc.formattedKey = formattedKey;
        const formattedToken = authenticator.generateToken(formattedKey);

        authenticator.verifyToken(formattedKey, formattedToken);

        authenticator.verifyToken(formattedKey, '000 000');

        const url = authenticator.generateTotpUri(
          formattedKey,
          doc.userEmail,
          'MightyJaxx',
          'SHA1',
          6,
          30,
        );

        const qr_svg = QRCode.image(url, { type: 'png' });

        const dir = mainPath + fileName;
        if (!fs.existsSync(mainPath)) {
          fs.mkdirSync(mainPath);
        }
        qr_svg.pipe(fs.createWriteStream(dir));
        doc.qrcodeImage = urlImage;
      }

      // check for existing user
      db.users.findOne({ userEmail: req.body.userEmail }, (err, user) => {
        if (user) {
          // user already exists with that email address
          console.error(
            colors.red(
              'Failed to insert user, possibly already exists: ' + err,
            ),
          );
          fs.unlinkSync(filePath);
          req.session.message = 'A user with that email address already exists';
          req.session.messageType = 'danger';
          res.redirect('/admin/user/new');
          return;
        }
        // email is ok to be used.
        db.users.insert(doc, (err, doc) => {
          // show the view
          if (err) {
            if (doc) {
              console.error(colors.red('Failed to insert user: ' + err));
              req.session.message = 'User exists';
              req.session.messageType = 'danger';
              res.redirect('/admin/user/edit/' + doc._id);
              return;
            }
            console.error(colors.red('Failed to insert user: ' + err));
            req.session.message = 'New user creation failed';
            req.session.messageType = 'danger';
            res.redirect('/admin/user/new');
            return;
          }
          req.session.message = 'User account inserted';
          req.session.messageType = 'success';

          // if from setup we add user to session and redirect to login.
          // Otherwise we show users screen
          if (urlParts.path === '/admin/setup') {
            req.session.user = req.body.userEmail;
            res.redirect('/admin/login');
            return;
          }
          res.redirect('/admin/users');
        });
      });
    });
  },
);

module.exports = router;
