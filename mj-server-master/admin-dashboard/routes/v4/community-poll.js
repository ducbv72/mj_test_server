const express = require('express');
const common = require('../../../lib/common');
const fs = require('fs');
const router = express.Router();
const xl = require('excel4node');
const path = require('path');
const multer = require('multer');
const { ObjectId } = require('mongodb');
const dayjs = require('dayjs');

const {
  listPolls,
  insertProfilePoll,
  deleteCommunityPoll,
  getUpdatePollForm,
  updateCommunityPoll,
} = require('../../../services/community-profile.service');

const storageProfilePoll = multer.diskStorage({
  destination: function (req, file, cb) {
    const dir = 'public/uploads/community-poll/';

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    cb(null, 'public/uploads/community-poll/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const listUploadImage = multer({ storage: storageProfilePoll });

//List polls
router.get(
  '/admin/community-profile/list-polls',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const { results } = await listPolls(req);
    res.render('community_poll_list', {
      menu: 'poll',
      parentMenu: 'community-profile',
      results: results,
      session: req.session,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

//Get form create new poll
router.get(
  '/admin/community-poll/new',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    // get the top results
    const formTitle = 'Create poll';
    const result = await db.profiles
      .find({})
      .sort({ updatedAt: -1 })
      .limit(100)
      .toArray();
    res.render('community_poll_new', {
      title: 'Community polls',
      profiles: result,
      session: req.session,
      formTitle,
      showSaveDraft: true,
      showPublish: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
      menu: 'poll',
      parentMenu: 'community-profile',
    });
  },
);

//Insert new poll
router.post(
  '/admin/community-poll/insert',
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([{ name: 'bannerImage', maxCount: 1 }]),
  async (req, res) => {
    const { errors, data } = await insertProfilePoll(req, common);
    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect('/admin/community-poll/new');
    }

    req.session.message = 'Created!';
    req.session.messageType = 'success';

    return res.redirect(`/admin/community-profile/list-polls`);
  },
);

//Get form update poll
router.get(
  '/admin/community-poll/update/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const { result } = await getUpdatePollForm(req, common);
    res.render('community_poll_new', result);
  },
);

router.put(
  '/admin/community-poll/update-status/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const updateStatus = await db.polls.updateOne(
      { _id: ObjectId(req.params.id) },
      { $set: { isDraft: true } },
    );
    if (!updateStatus.result.n) {
      return res.status(400).json({ message: 'Update status failed' });
    }
    res.status(200).json({ message: 'Updated!' });
  },
);

router.post(
  '/admin/community-poll/update/:id',
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([{ name: 'bannerImage', maxCount: 1 }]),
  async (req, res) => {
    const { errors, data } = await updateCommunityPoll(req, common);
    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect(`/admin/community-poll/update/${req.params.id}`);
    }

    req.session.message = 'Updated!';
    req.session.messageType = 'success';

    res.redirect(`/admin/community-profile/list-polls`);
  },
);

//Delete draft poll
router.delete(
  '/admin/community-poll/delete/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const { errors, data } = await deleteCommunityPoll(req);
    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.end();
    }

    req.session.message = 'Deleted!';
    req.session.messageType = 'success';

    res.end();
  },
);

router.get(
  '/admin/community-poll/export/:poll',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    try {
      const id = req.params.poll;
      const db = req.app.db;
      const getPoll = await db.polls.findOne({ _id: ObjectId(id) });
      const getBrand = await db.profiles.findOne({ _id: getPoll.profileId });

      if (!getPoll) {
        return res
          .status(400)
          .json('Sorry, this poll is currently unavailable');
      }

      if (
        !(
          getPoll.isPublished &&
          new Date(getPoll.pollStartOn).getTime() < new Date().getTime()
        )
      ) {
        return res
          .status(400)
          .json('Sorry, this poll is currently unavailable');
      }

      const pollStartOn = dayjs(getPoll.pollStartOn)
        .format('DD/MM/YYYY-hh:mm A')
        .replace('-', ' ');
      const pollExpiresOn = dayjs(getPoll.pollExpiresOn)
        .format('DD/MM/YYYY-hh:mm A')
        .replace('-', ' ');

      const getListChoice = await db.userPolls
        .aggregate([
          {
            $match: { pollId: getPoll._id },
          },
          {
            $lookup: {
              from: 'customers',
              let: { id: '$userId' },
              pipeline: [
                { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
                { $project: { email: 1, userName: 1 } },
              ],
              as: 'customer',
            },
          },
          {
            $sort: { createdAt: 1 },
          },
        ])
        .toArray();

      const removeAnswerWithNoChoice = [];
      const numOfAnswer = [];
      let findRate = 0;
      const numOfQuestion = getPoll.options.length;
      const totalAnswer = getListChoice.length;

      for (let i = 1; i <= numOfQuestion; i++) {
        const numOfOpt = getListChoice.filter(
          (e) => e.nameOfAnswer[0].key === `option${i}`,
        );
        if (numOfOpt.length > 0) {
          removeAnswerWithNoChoice.push({
            totalRespondents: `${i}. ${getPoll.options[i - 1].name}`,
            count: numOfOpt.length,
            order: i,
          });
        }
        if (numOfOpt.length === 0) {
          numOfAnswer.push({
            totalRespondents: `${i}. ${getPoll.options[i - 1].name}`,
            rate: 0,
            count: 0,
            order: i,
          });
        }
      }

      removeAnswerWithNoChoice.map((e, i) => {
        if (i < removeAnswerWithNoChoice.length - 1) {
          const rate = Math.round((e.count / totalAnswer) * 100);
          e.rate = rate;
          findRate += rate;
        } else {
          e.rate = 100 - findRate;
        }
        return e;
      });
      const overallResultsTally = numOfAnswer
        .concat(removeAnswerWithNoChoice)
        .sort((a, b) => a.order - b.order)
        .map((e) => {
          delete e.order;
          e.rate = `${e.rate}%`;
          e.count = `${e.count} / ${totalAnswer}`;
          return e;
        });

      const resultData = getListChoice.map((e) => {
        return {
          email: e.customer[0].email,
          response: e.nameOfAnswer[0].name,
          time: dayjs(e.createdAt).format('DD/MM/YYYY-hh:mm:ss A'),
        };
      });

      // Create a new instance of a Workbook class
      const wb = new xl.Workbook();

      const options = {
        pageSetup: {
          orientation: 'landscape',
        },
      };

      // Add Worksheets to the workbook
      const ws = wb.addWorksheet('Sheet 1', options);

      const font = 'Arial';

      // Create a reusable style
      const style = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          name: font,
        },

        alignment: {
          wrapText: true,
          vertical: 'center',
          horizontal: 'center',
        },
      });

      const columnTitleStyle = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          name: font,
          bold: true,
        },

        alignment: {
          wrapText: true,
          vertical: 'center',
          horizontal: 'center',
        },
      });
      const columnTitleStyleLeft = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          name: font,
          bold: true,
        },

        alignment: {
          wrapText: true,
          vertical: 'center',
          horizontal: 'left',
        },
      });

      const styleContentCenter = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          name: font,
        },

        alignment: {
          wrapText: true,
          horizontal: 'center',
          vertical: 'center',
        },
      });
      const styleContentLeft = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          name: font,
        },

        alignment: {
          wrapText: true,
          horizontal: 'left',
          vertical: 'center',
        },
      });

      ws.cell(1, 1).string(`Question: `).style(styleContentCenter);
      ws.cell(1, 2).string(`${getPoll.question}`).style(styleContentCenter);

      const tab = 'CHAR(32)&CHAR(32)&';

      const startRow = 3;
      const resultDataLength = resultData.length;
      let newRow = 0;

      // COLUMN TITLE
      ws.cell(startRow - 1, 1)
        .string('User email')
        .style(columnTitleStyle);
      ws.cell(startRow - 1, 2)
        .string('Response')
        .style(columnTitleStyle);
      ws.cell(startRow - 1, 3)
        .string('Datetime of response')
        .style(columnTitleStyle);

      for (let i = 0; i < resultDataLength; i++) {
        // FULLNAME - FIRST COLUMN
        ws.cell(startRow + i, 1)
          .string(resultData[i].email)
          .style(style);

        // SECOND COLUMN
        ws.cell(startRow + i, 2)
          .string(resultData[i].response)
          .style(style);

        // THIRD COLUMN
        ws.cell(startRow + i, 3)
          .string(resultData[i].time)
          .style(style);
      }

      ws.cell(startRow - 1, 1, startRow + resultDataLength - 1, 3).style({
        border: {
          top: {
            style: 'thin',
          },
          right: {
            style: 'thin',
          },
          bottom: {
            style: 'thin',
          },
          left: {
            style: 'thin',
          },
        },
      });
      ws.cell(
        startRow + resultDataLength - 1,
        1,
        startRow + resultDataLength - 1,
        3,
      ).style({
        border: {
          bottom: {
            style: 'thick',
          },
        },
      });
      ws.cell(startRow - 1, 1, startRow - 1, 3).style({
        border: {
          top: {
            style: 'thick',
          },
        },
      });
      ws.cell(2, 1, startRow + resultDataLength - 1, 1).style({
        border: {
          left: {
            style: 'thick',
          },
        },
      });
      ws.cell(2, 2, startRow + resultDataLength - 1, 2).style({
        border: {
          left: {
            style: 'thick',
          },
        },
      });
      ws.cell(2, 3, startRow + resultDataLength - 1, 3).style({
        border: {
          left: {
            style: 'thick',
          },
          right: {
            style: 'thick',
          },
        },
      });

      newRow = startRow + resultDataLength + 3;
      ws.column(1).setWidth(30);
      ws.column(2).setWidth(30);
      ws.column(3).setWidth(20);
      ws.column(4).setWidth(15);
      ws.column(5).setWidth(20);
      ws.column(6).setWidth(20);

      ws.cell(1, 5).string('Poll Starts On').style(columnTitleStyle);
      ws.cell(2, 5).string('Poll Expires On').style(columnTitleStyle);
      ws.cell(1, 6).string(pollStartOn).style(style);
      ws.cell(2, 6).string(pollExpiresOn).style(style);

      const columnTitleStyleCenter = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          bold: true,
          name: font,
        },

        alignment: {
          wrapText: true,
          horizontal: 'center',
          vertical: 'center',
        },
      });

      // Overall Results Tally
      const overallResultsTallyLength = overallResultsTally.length;
      // COLUMN TITLE
      ws.cell(newRow, 2)
        .string('Total Respondents')
        .style(columnTitleStyleLeft);
      ws.cell(newRow, 3)
        .string(`${getListChoice.length}`)
        .style(columnTitleStyleCenter);
      ws.cell(newRow, 4).string('% breakdown').style(columnTitleStyleCenter);

      newRow += 1;

      for (let i = 0; i < overallResultsTallyLength; i++) {
        // FULLNAME - FIRST COLUMN
        ws.cell(newRow + i, 2)
          .string(overallResultsTally[i].totalRespondents)
          .style(styleContentLeft);

        // SECOND COLUMN
        ws.cell(newRow + i, 3)
          .string(overallResultsTally[i].count)
          .style(styleContentCenter);

        // THIRD COLUMN
        ws.cell(newRow + i, 4)
          .string(overallResultsTally[i].rate)
          .style(styleContentCenter);
      }

      ws.cell(1, 5, 2, 6).style({
        border: {
          top: {
            style: 'thick',
          },
          right: {
            style: 'thick',
          },
          bottom: {
            style: 'thick',
          },
          left: {
            style: 'thick',
          },
        },
      });

      const endRow = newRow + overallResultsTallyLength - 1;
      ws.cell(newRow - 1, 2, endRow, 4).style({
        border: {
          top: {
            style: 'thin',
          },
          right: {
            style: 'thin',
          },
          bottom: {
            style: 'thin',
          },
          left: {
            style: 'thin',
          },
        },
      });

      const exportFileName = `${dayjs().format('YYYY.MM.DD')}_${
        getBrand.profileName
      }_${getPoll.question}.xlsx`;
      const filePath = path.join(process.cwd(), exportFileName);
      await wb.write(exportFileName, res);
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: error.message });
    }
  },
);

module.exports = router;
