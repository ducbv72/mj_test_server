const express = require('express');
const common = require('../../../lib/common');
const escape = require('html-entities').AllHtmlEntities;
const colors = require('colors');
const bcrypt = require('bcryptjs');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const glob = require('glob');
const mime = require('mime-type/with-db');
const router = express.Router();
const ethereumConfig = require('../../../config/ethereumConfig');
const Config = require('../../../config');
const configUrl = require('../../../config/url.json');
const authenticator = require('authenticator');
const axios = require('axios');
const dayjs = require('dayjs');
const { bonus_points_update } = require('../../../config/smileModel');
const { shopify } = require('../../../config/shopify');
const mj_shopify = shopify;
const upload = multer({ dest: 'public/uploads/' });
const AWS = require('aws-sdk');
// Admin section
router.get('/admin/dashboard', common.restrict, (req, res, next) => {
  res.render('admin', {
    menu: 'analytic',
    title: 'Dashboard',
    referringUrl: req.header('Referer'),
    config: req.app.config,
    session: req.session,
    editor: true,
    admin: true,
    message: common.clearSessionValue(req.session, 'message'),
    messageType: common.clearSessionValue(req.session, 'messageType'),
    helpers: req.handlebars.helpers,
  });
});

// logout
router.get('/admin/logout', (req, res) => {
  req.session.user = null;
  req.session.message = null;
  req.session.messageType = null;
  res.redirect('/');
});

// login form
router.get('/admin/login', (req, res) => {
  const db = req.app.db;
  db.users.count({}, (err, userCount) => {
    if (err) {
      // if there are no users set the "needsSetup" session
      req.session.needsSetup = true;
      res.redirect('/admin/setup');
    }
    // we check for a user. If one exists, redirect to login form otherwise setup
    if (userCount > 0) {
      // set needsSetup to false as a user exists
      req.session.needsSetup = false;
      res.render('login', {
        menu: '',
        title: 'Login',
        referringUrl: req.header('Referer'),
        config: req.app.config,
        message: common.clearSessionValue(req.session, 'message'),
        messageType: common.clearSessionValue(req.session, 'messageType'),
        helpers: req.handlebars.helpers,
        showFooter: 'showFooter',
      });
    } else {
      // if there are no users set the "needsSetup" session
      req.session.needsSetup = true;
      res.redirect('/admin/setup');
    }
  });
});

// login the user and check the password
router.post('/admin/login_action', (req, res) => {
  const db = req.app.db;

  db.users.findOne(
    { userEmail: common.mongoSanitize(req.body.email) },
    (err, user) => {
      if (err) {
        res
          .status(400)
          .json({ message: 'A user with that email does not exist.' });
        return;
      }

      // check if user exists with that email
      if (user === undefined || user === null) {
        return res
          .status(400)
          .json({ message: 'A user with that email does not exist.' });
      } else {
        if (
          process.env.IGNORE_TOKEN === 'true' &&
          req.body.token === '222222'
        ) {
          bcrypt
            .compare(req.body.password, user.userPassword)
            .then((result) => {
              if (result) {
                req.session.user = req.body.email;
                req.session.groupRole = user.groupRole;
                req.session.usersName = user.usersName;
                req.session.isSuperAdmin = user.isSuperAdmin;
                req.session.userId = user._id.toString();
                req.session.isAdmin =
                  user.isAdmin || req.session.userRole === 'admin';
                return res.status(200).json({ message: 'Login successful' });
              } else {
                return res.status(400).json({
                  message: 'Access denied. Check password and try again.',
                });
              }
            });
        } else {
          // we have a user under that email so we compare the password
          bcrypt
            .compare(req.body.password, user.userPassword)
            .then((result) => {
              if (result) {
                // verify token
                const result = authenticator.verifyToken(
                  user.formattedKey,
                  req.body.token,
                );
                if (result) {
                  req.session.user = req.body.email;
                  req.session.groupRole = user.groupRole;
                  req.session.usersName = user.usersName;
                  req.session.isSuperAdmin = user.isSuperAdmin;
                  req.session.userId = user._id.toString();
                  req.session.isAdmin =
                    user.isAdmin || req.session.userRole === 'admin';
                  return res.status(200).json({ message: 'Login successful' });
                } else {
                  return res.status(400).json({ message: 'Invalid Token.' });
                }
              } else {
                // password is not correct
                return res.status(400).json({
                  message: 'Access denied. Check password and try again.',
                });
              }
            });
        }
      }
    },
  );
});

// settings update
router.get(
  '/admin/settings',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('settings', {
      title: 'Setting',
      menu: 'setting',
      session: req.session,
      admin: true,
      themes: common.getThemes(),
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
      config: req.app.config,
      footerHtml:
        typeof req.app.config.footerHtml !== 'undefined'
          ? escape.decode(req.app.config.footerHtml)
          : null,
      googleAnalytics:
        typeof req.app.config.googleAnalytics !== 'undefined'
          ? escape.decode(req.app.config.googleAnalytics)
          : null,
    });
  },
);

router.get(
  '/admin/settings/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.redirect('/admin/settings');
  },
);

// settings update
router.post(
  '/admin/settings/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    try {
      const result = common.updateConfig(req.body);
      if (result === true) {
        req.session.message = 'Successfully updated';
        req.session.messageType = 'success';
        res.redirect('/admin/settings');
        res.configDirty = true;
        return;
      }
      res.status(400).json({ message: 'Permission denied' });
    } catch (exception) {
      console.log(exception);
    }
  },
);

// validate the permalink
router.post('/admin/api/validate_permalink', common.restrict, (req, res) => {
  // if doc id is provided it checks for permalink in any products other that one provided,
  // else it just checks for any products with that permalink
  const db = req.app.db;

  let query = {};
  if (typeof req.body.docId === 'undefined' || req.body.docId === '') {
    query = { productPermalink: req.body.permalink };
  } else {
    query = {
      productPermalink: req.body.permalink,
      _id: { $ne: common.getId(req.body.docId) },
    };
  }

  db.products.count(query, (err, products) => {
    if (err) {
      console.info(err.stack);
    }
    if (products > 0) {
      res.status(400).json({ message: 'Permalink already exists' });
    } else {
      res.status(200).json({ message: 'Permalink validated successfully' });
    }
  });
});

// upload the file
router.post(
  '/admin/file/upload',
  common.restrict,
  common.checkAccess,
  upload.single('upload_file'),
  async (req, res, next) => {
    const db = req.app.db;
    if (req.file) {
      const file = req.file;

      // Get the mime type of the file
      const mimeType = mime.lookup(file.originalname);

      // Check for allowed mime type and file size
      if (
        !common.allowedMimeType.includes(mimeType) ||
        file.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(file.path);
        res.status(400).json({
          code: 400,
          message: 'File type not allowed or too large. Please try again.',
        });
        return;
      }

      const product = await db.products.findOne({
        _id: common.getId(req.body.productId),
      });
      if (!product) {
        fs.unlinkSync(file.path);
        // Redirect to error
        res.status(400).json({
          code: 400,
          message: 'Product not found',
        });
        return;
      }

      //save file to upload folder as normal to use for old version
      //we will remove it totally after few months.
      const productPath = product.productPermalink || product._id.toString();
      const uploadDir = path.join('public/uploads', productPath);

      // Check directory and create (if needed)
      common.checkDirectorySync(uploadDir);

      const source = fs.createReadStream(file.path);
      const dest = fs.createWriteStream(
        path.join(uploadDir, file.originalname.replace(/ /g, '_')),
      );

      // save the new file
      source.pipe(dest);
      source.on('end', () => {});
      const imagePath = path.join(
        '/uploads',
        productPath,
        file.originalname.replace(/ /g, '_'),
      );

      //upload bigger file
      const imageUrlS3 = await common.uploadFileToS3(
        file.path,
        Config.S3.productImageFolder,
      );
      if (req.body.upload_type === 'backgroundImage') {
        // we will delete this later because
        product.productBackgroundImage = imagePath;
        product.productBackgroundImageS3 = imageUrlS3;
      } else {
        product.productImageUrl = imageUrlS3;
        product.productImageUrlS3 = imageUrlS3;
      }

      //apply new image to product
      await db.products.updateOne(
        { _id: common.getId(req.body.productId) },
        { $set: product },
      );
      //apply new image for certificates
      const result = await db.orders.updateMany(
        {
          sku: product.productVariantSku,
        },
        {
          $set: {
            'orderProducts.0.productImageUrl': product.productImageUrl,
            'orderProducts.0.productImageUrlS3': product.productImageUrlS3,
            'orderProducts.0.productBackgroundImageS3':
              product.productBackgroundImageS3,
            'orderProducts.0.productBackgroundImage':
              product.productBackgroundImage,
          },
        },
        { multi: true },
      );

      res.status(200).json({
        code: 200,
        message: 'File uploaded successfully',
        data: {
          imageUrl: imagePath,
        },
      });
    } else {
      // Redirect to error
      res.status(400).json({
        code: 400,
        message: 'File upload error. Please select a file.',
      });
    }
  },
);
//upload muLti file
router.post(
  '/admin/files/multiUpload',
  common.restrict,
  common.checkAccess,
  //upload.array('upload_file'),
  upload.fields([
    {
      name: 'upload_file',
    },
    {
      name: 'upload_thumb_file',
    },
  ]),
  async (req, res, next) => {
    const db = req.app.db;

    if (req.files.upload_file.length > 0) {
      const fileUploadArr = [];
      for (let i = 0; i < req.files.upload_file.length; i++) {
        const file = req.files.upload_file[i];
        //console.log('data array: ', req.body.array_size[i]);
        console.log('file data: ', JSON.stringify(file));
        // Get the mime type of the file
        const mimeType = mime.lookup(file.originalname);

        // Check for allowed mime type and file size
        if (mimeType.includes('video')) {
          console.log('mime: ', mimeType);
          if (!common.allowedVideoType.includes(mimeType)) {
            // Remove temp file
            fs.unlinkSync(file.path);
            res.status(400).json({
              code: 400,
              message: 'File type not allowed or too large. Please try again.',
            });
            return;
          }
        } else {
          if (!common.allowedMimeType.includes(mimeType)) {
            // Remove temp file
            fs.unlinkSync(file.path);
            res.status(400).json({
              code: 400,
              message: 'File type not allowed or too large. Please try again.',
            });
            return;
          }
        }

        const [product] = await Promise.all([
          db.products.findOne({ _id: common.getId(req.body.productId) }),
        ]);
        if (!product) {
          fs.unlinkSync(file.path);
          // Redirect to error
          res.status(400).json({
            code: 400,
            message: 'Product not found',
          });
          return;
        }
        // //save file to upload folder as normal to use for old version
        // //we will remove it totally after few months.
        // const productPath = product.productPermalink || product._id.toString();
        // const uploadDir = path.join('public/uploads', productPath);

        // // Check directory and create (if needed)
        // common.checkDirectorySync(uploadDir);

        // const source = fs.createReadStream(file.path);
        // const dest = fs.createWriteStream(
        //   path.join(uploadDir, file.originalname.replace(/ /g, '_')),
        // );

        // // save the new file
        // source.pipe(dest);
        // source.on('end', () => {});
        // const imagePath = path.join(
        //   '/uploads',
        //   productPath,
        //   file.originalname.replace(/ /g, '_'),
        // );

        //upload bigger file
        if (common.allowedVideoType.includes(mimeType)) {
          const imageUrlS3 = await common.uploadVideoToS3(
            file.path,
            Config.S3.productImageFolder,
          );
          if (imageUrlS3 != '') {
            fs.unlinkSync(file.path);
          }
          const fileName = file.originalname;
          let fileThumbS3Url = '';
          fileId = (Math.random() + 1).toString(36).substring(2);

          let thumbFile = null;
          if (
            req.files.upload_thumb_file &&
            req.files.upload_thumb_file.length > 0
          ) {
            for (let j = 0; j < req.files.upload_thumb_file.length; j++) {
              const thumbFileItem = req.files.upload_thumb_file[j];
              const thumbName = thumbFileItem.originalname.substring(
                0,
                thumbFileItem.originalname.lastIndexOf('_thumb'),
              );
              const fileNameWithoutExt = fileName.substring(
                0,
                fileName.lastIndexOf('.'),
              );

              console.log('thumbName: ', thumbName);
              console.log('fileName: ', fileNameWithoutExt);

              if (thumbName == fileNameWithoutExt) {
                thumbFile = thumbFileItem;
                break;
              }
            }
          }

          if (thumbFile) {
            fileThumbS3Url = await common.uploadFileToS3(
              thumbFile.path,
              Config.S3.productImageFolder,
            );
            if (fileThumbS3Url != '') {
              fs.unlinkSync(thumbFile.path);
            }
          }

          fileUploadArr.push({
            fileId: fileId,
            assetsFileName: fileName,
            assetsFileUrl: imageUrlS3,
            assetsFileThumbUrl: fileThumbS3Url,
            allowDownload: true,
          });
        } else if (common.allowedMimeType.includes(mimeType)) {
          const imageUrlS3 = await common.uploadFileToS3(
            file.path,
            Config.S3.productImageFolder,
          );
          if (imageUrlS3 != '') {
            console.log('file path: ', file.path);
            fs.unlinkSync(file.path);
          }
          const fileName = file.originalname;
          fileId = (Math.random() + 1).toString(36).substring(2);
          fileUploadArr.push({
            fileId: fileId,
            assetsFileName: fileName,
            assetsFileUrl: imageUrlS3,
            assetsFileThumbUrl: imageUrlS3,
            allowDownload: true,
          });
        }
      }
      db.products.update(
        { _id: common.getId(req.body.productId) },
        {
          $push: {
            productAssetsFile: {
              $each: fileUploadArr,
              $position: 0,
            },
          },
        },
        { multi: false },
        (err, numReplaced) => {
          if (err) {
            console.info(err.stack);
          }
          res.status(200).json({
            code: 200,
            message: 'File uploaded successfully',
          });
          return;
        },
      );
    } else {
      // Redirect to error
      res.status(400).json({
        code: 400,
        message: 'File upload error. Please select a file.',
      });
    }
  },
);

//upload muLti file
router.post(
  '/admin/files/multiUpload',
  common.restrict,
  common.checkAccess,
  upload.array('upload_file'),
  async (req, res, next) => {
    const db = req.app.db;
    console.log('Files: ', JSON.stringify(req.files));

    if (req.files.upload_file.length > 0) {
      const fileUploadArr = [];
      for (let i = 0; i < req.files.upload_file.length; i++) {
        const file = req.files.upload_file[i];
        //console.log('data array: ', req.body.array_size[i]);
        console.log('file data: ', JSON.stringify(file));
        // Get the mime type of the file
        const mimeType = mime.lookup(file.originalname);

        // Check for allowed mime type and file size
        if (mimeType.includes('video')) {
          console.log('mime: ', mimeType);
          if (!common.allowedVideoType.includes(mimeType)) {
            // Remove temp file
            fs.unlinkSync(file.path);
            res.status(400).json({
              code: 400,
              message: 'File type not allowed or too large. Please try again.',
            });
            return;
          }
        } else {
          if (!common.allowedMimeType.includes(mimeType)) {
            // Remove temp file
            fs.unlinkSync(file.path);
            res.status(400).json({
              code: 400,
              message: 'File type not allowed or too large. Please try again.',
            });
            return;
          }
        }

        const [product] = await Promise.all([
          db.products.findOne({ _id: common.getId(req.body.productId) }),
        ]);
        if (!product) {
          fs.unlinkSync(file.path);
          // Redirect to error
          res.status(400).json({
            code: 400,
            message: 'Product not found',
          });
          return;
        }
        // //save file to upload folder as normal to use for old version
        // //we will remove it totally after few months.
        // const productPath = product.productPermalink || product._id.toString();
        // const uploadDir = path.join('public/uploads', productPath);

        // // Check directory and create (if needed)
        // common.checkDirectorySync(uploadDir);

        // const source = fs.createReadStream(file.path);
        // const dest = fs.createWriteStream(
        //   path.join(uploadDir, file.originalname.replace(/ /g, '_')),
        // );

        // // save the new file
        // source.pipe(dest);
        // source.on('end', () => {});
        // const imagePath = path.join(
        //   '/uploads',
        //   productPath,
        //   file.originalname.replace(/ /g, '_'),
        // );

        //upload bigger file
        if (common.allowedVideoType.includes(mimeType)) {
          const imageUrlS3 = await common.uploadVideoToS3(
            file.path,
            Config.S3.productImageFolder,
          );
          if (imageUrlS3 != '') {
            fs.unlinkSync(file.path);
          }
          const fileName = file.originalname;
          let fileThumbS3Url = '';
          fileId = (Math.random() + 1).toString(36).substring(2);

          let thumbFile = null;
          if (
            req.files.upload_thumb_file &&
            req.files.upload_thumb_file.length > 0
          ) {
            for (let j = 0; j < req.files.upload_thumb_file.length; j++) {
              const thumbFileItem = req.files.upload_thumb_file[j];
              const thumbName = thumbFileItem.originalname.substring(
                0,
                thumbFileItem.originalname.lastIndexOf('_thumb'),
              );
              const fileNameWithoutExt = fileName.substring(
                0,
                fileName.lastIndexOf('.'),
              );

              console.log('thumbName: ', thumbName);
              console.log('fileName: ', fileNameWithoutExt);

              if (thumbName == fileNameWithoutExt) {
                thumbFile = thumbFileItem;
                break;
              }
            }
          }

          if (thumbFile) {
            fileThumbS3Url = await common.uploadFileToS3(
              thumbFile.path,
              Config.S3.productImageFolder,
            );
            if (fileThumbS3Url != '') {
              fs.unlinkSync(thumbFile.path);
            }
          }

          fileUploadArr.push({
            fileId: fileId,
            assetsFileName: fileName,
            assetsFileUrl: imageUrlS3,
            assetsFileThumbUrl: fileThumbS3Url,
            allowDownload: true,
          });
        } else if (common.allowedMimeType.includes(mimeType)) {
          const imageUrlS3 = await common.uploadFileToS3(
            file.path,
            Config.S3.productImageFolder,
          );
          if (imageUrlS3 != '') {
            console.log('file path: ', file.path);
            fs.unlinkSync(file.path);
          }
          const fileName = file.originalname;
          fileId = (Math.random() + 1).toString(36).substring(2);
          fileUploadArr.push({
            fileId: fileId,
            assetsFileName: fileName,
            assetsFileUrl: imageUrlS3,
            assetsFileThumbUrl: imageUrlS3,
            allowDownload: true,
          });
        }
      }
      db.products.update(
        { _id: common.getId(req.body.productId) },
        {
          $push: {
            productAssetsFile: {
              $each: fileUploadArr,
              $position: 0,
            },
          },
        },
        { multi: false },
        (err, numReplaced) => {
          if (err) {
            console.info(err.stack);
          }
          res.status(200).json({
            code: 200,
            message: 'File uploaded successfully',
          });
          return;
        },
      );
    } else {
      // Redirect to error
      res.status(400).json({
        code: 400,
        message: 'File upload error. Please select a file.',
      });
    }
  },
);

//upload muLti file
router.post(
  '/admin/files/multiUpload',
  common.restrict,
  common.checkAccess,
  upload.array('upload_file'),
  async (req, res, next) => {
    const db = req.app.db;
    console.log('Files: ', JSON.stringify(req.files));

    if (req.files.upload_file.length > 0) {
      const fileUploadArr = [];
      for (let i = 0; i < req.files.upload_file.length; i++) {
        const file = req.files.upload_file[i];
        //console.log('data array: ', req.body.array_size[i]);
        console.log('file data: ', JSON.stringify(file));
        // Get the mime type of the file
        const mimeType = mime.lookup(file.originalname);

        // Check for allowed mime type and file size
        if (mimeType.includes('video')) {
          console.log('mime: ', mimeType);
          if (!common.allowedVideoType.includes(mimeType)) {
            // Remove temp file
            fs.unlinkSync(file.path);
            res.status(400).json({
              code: 400,
              message: 'File type not allowed or too large. Please try again.',
            });
            return;
          }
        } else {
          if (!common.allowedMimeType.includes(mimeType)) {
            // Remove temp file
            fs.unlinkSync(file.path);
            res.status(400).json({
              code: 400,
              message: 'File type not allowed or too large. Please try again.',
            });
            return;
          }
        }

        const [product] = await Promise.all([
          db.products.findOne({ _id: common.getId(req.body.productId) }),
        ]);
        if (!product) {
          fs.unlinkSync(file.path);
          // Redirect to error
          res.status(400).json({
            code: 400,
            message: 'Product not found',
          });
          return;
        }
        // //save file to upload folder as normal to use for old version
        // //we will remove it totally after few months.
        // const productPath = product.productPermalink || product._id.toString();
        // const uploadDir = path.join('public/uploads', productPath);

        // // Check directory and create (if needed)
        // common.checkDirectorySync(uploadDir);

        // const source = fs.createReadStream(file.path);
        // const dest = fs.createWriteStream(
        //   path.join(uploadDir, file.originalname.replace(/ /g, '_')),
        // );

        // // save the new file
        // source.pipe(dest);
        // source.on('end', () => {});
        // const imagePath = path.join(
        //   '/uploads',
        //   productPath,
        //   file.originalname.replace(/ /g, '_'),
        // );

        //upload bigger file
        if (common.allowedVideoType.includes(mimeType)) {
          const imageUrlS3 = await common.uploadVideoToS3(
            file.path,
            Config.S3.productImageFolder,
          );
          if (imageUrlS3 != '') {
            fs.unlinkSync(file.path);
          }
          const fileName = file.originalname;
          let fileThumbS3Url = '';
          fileId = (Math.random() + 1).toString(36).substring(2);

          let thumbFile = null;
          if (
            req.files.upload_thumb_file &&
            req.files.upload_thumb_file.length > 0
          ) {
            for (let j = 0; j < req.files.upload_thumb_file.length; j++) {
              const thumbFileItem = req.files.upload_thumb_file[j];
              const thumbName = thumbFileItem.originalname.substring(
                0,
                thumbFileItem.originalname.lastIndexOf('_thumb'),
              );
              const fileNameWithoutExt = fileName.substring(
                0,
                fileName.lastIndexOf('.'),
              );

              console.log('thumbName: ', thumbName);
              console.log('fileName: ', fileNameWithoutExt);

              if (thumbName == fileNameWithoutExt) {
                thumbFile = thumbFileItem;
                break;
              }
            }
          }

          if (thumbFile) {
            fileThumbS3Url = await common.uploadFileToS3(
              thumbFile.path,
              Config.S3.productImageFolder,
            );
            if (fileThumbS3Url != '') {
              fs.unlinkSync(thumbFile.path);
            }
          }

          fileUploadArr.push({
            fileId: fileId,
            assetsFileName: fileName,
            assetsFileUrl: imageUrlS3,
            assetsFileThumbUrl: fileThumbS3Url,
            allowDownload: true,
          });
        } else if (common.allowedMimeType.includes(mimeType)) {
          const imageUrlS3 = await common.uploadFileToS3(
            file.path,
            Config.S3.productImageFolder,
          );
          if (imageUrlS3 != '') {
            console.log('file path: ', file.path);
            fs.unlinkSync(file.path);
          }
          const fileName = file.originalname;
          fileId = (Math.random() + 1).toString(36).substring(2);
          fileUploadArr.push({
            fileId: fileId,
            assetsFileName: fileName,
            assetsFileUrl: imageUrlS3,
            assetsFileThumbUrl: imageUrlS3,
            allowDownload: true,
          });
        }
      }
      db.products.update(
        { _id: common.getId(req.body.productId) },
        {
          $push: {
            productAssetsFile: {
              $each: fileUploadArr,
              $position: 0,
            },
          },
        },
        { multi: false },
        (err, numReplaced) => {
          if (err) {
            console.info(err.stack);
          }
          res.status(200).json({
            code: 200,
            message: 'File uploaded successfully',
          });
          return;
        },
      );
    } else {
      // Redirect to error
      res.status(400).json({
        code: 400,
        message: 'File upload error. Please select a file.',
      });
    }
  },
);

// delete a file via ajax request
router.post(
  '/admin/file/delete',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    req.session.message = null;
    req.session.messageType = null;

    fs.unlink('public/' + req.body.img, (err) => {
      if (err) {
        console.error(colors.red('File delete error: ' + err));
        res.writeHead(400, { 'Content-Type': 'application/text' });
        res.end('Failed to delete file: ' + err);
      } else {
        res.writeHead(200, { 'Content-Type': 'application/text' });
        res.end('File deleted successfully');
      }
    });
  },
);

router.get('/admin/files', common.restrict, (req, res) => {
  // loop files in /public/uploads/
  glob('public/uploads/**', { nosort: true }, (er, files) => {
    // sort array
    files.sort();

    // declare the array of objects
    const fileList = [];
    const dirList = [];

    // loop these files
    for (let i = 0; i < files.length; i++) {
      // only want files
      if (fs.lstatSync(files[i]).isDirectory() === false) {
        // declare the file object and set its values
        const file = {
          id: i,
          path: files[i].substring(6),
        };

        // push the file object into the array
        fileList.push(file);
      } else {
        const dir = {
          id: i,
          path: files[i].substring(6),
        };

        // push the dir object into the array
        dirList.push(dir);
      }
    }

    // render the files route
    res.render('files', {
      title: 'Files',
      files: fileList,
      admin: true,
      dirs: dirList,
      session: req.session,
      config: common.get(),
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
    });
  });
});

// view blockchain status
router.get(
  '/admin/blockchains/:page',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const page = Number(req.params.page);
    // get the top results
    const [blockchains, count] = await Promise.all([
      db.merkleroots
        .find({})
        .skip(page * ethereumConfig.productLitmitPerPage)
        .limit(ethereumConfig.productLitmitPerPage)
        .sort({ createdDate: -1 })
        .toArray(),
      db.merkleroots.count({}),
    ]);

    const total = count / ethereumConfig.productLitmitPerPage;
    const array = [];
    for (let i = 0; i < total; i++) {
      array.push(i);
    }
    res.render('blockchains', {
      title: 'Cart',
      menu: 'blockchain',
      blockchains: blockchains,
      total: total,
      page: page,
      totalPages: array,
      count: count,
      session: req.session,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

const getAccountId = async (req, res, next) => {
  try {
    const { data } = await axios.get(configUrl.smileAccountUrl, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
      },
    });
    req.accountID = data.accounts.find((e) => e.name === 'Mighty Jaxx').id;
    next();
  } catch (error) {
    let solve = '';
    if (error.message.includes(401)) {
      solve += 'May be missing the SMILEIO_PRIVATE_KEY';
    } else if (error.message.includes(403)) {
      solve += 'You need permission to access this content';
    }
    res.json({
      code: 400,
      message: `${error.name}: ${error.message}`,
      Solve: solve,
    });
  }
};

function getMultiplier(tags) {
  const listTags = tags.split(',');
  const findMultiplier = listTags.find((e) => /multiplier/i.test(e));
  if (!findMultiplier) {
    return null;
  }
  const getCoefficient = Number(findMultiplier.replace(/-multiplier/i, ''));
  return getCoefficient;
}

router.post('/admin/flow-trigger', getAccountId, async (req, res, next) => {
  try {
    const db = req.app.db;
    const { id } = req.body;
    const accountId = req.accountID;
    const order = await mj_shopify.order.get(id).catch((err) => {
      console.log(err.message);
    });
    const shopifyCustomerId = order.customer.id;
    const smileIOUrl = configUrl.smileActivityRuleUrl.replace(
      '{account_id}',
      accountId,
    );
    const customerUrl = configUrl.smileCustomerUrl.replace(
      '{email}',
      order.customer.email,
    );
    const updatePointUrl = configUrl.smilePointsTransactionUrl;

    //Find customer in smile io
    const { data: customer } = await axios.get(customerUrl, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
        'smile-client': 'smile-admin',
      },
    });

    //check customer
    if (!customer.customers.length) {
      console.log(`Can't find this customer`);
      return res.end();
    }
    //Get customer in smileio
    const smileCustomer = customer.customers[0];

    //Get rule with points reward in smileio
    const { data: activity_rules } = await axios.get(smileIOUrl, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
      },
    });

    // Get rule with type = 'shopify_online_order'
    const activity_rule = activity_rules.activity_rules.find(
      (e) => e.type === 'shopify_online_order',
    );

    // Get points reward
    const multiplier = activity_rule.multiplier;

    // Retrieve a list of products that have been ordered by the user
    const lineItem = order.line_items;

    let bonus_points = 0;
    let getCoefficient;

    for (let i = 0; i < lineItem.length; i++) {
      if (lineItem[i].product_id === null) {
        continue;
      }
      const product = await mj_shopify.product.get(lineItem[i].product_id);
      const variant = await mj_shopify.productVariant.get(
        lineItem[i].variant_id,
      );
      const tags = product.tags;
      const price = variant.price;
      const quantity = lineItem[i].quantity;
      getCoefficient = getMultiplier(tags);
      if (!getCoefficient) {
        getCoefficient = 1;
      }
      const earn_poinst = price * (getCoefficient - 1) * multiplier * quantity;
      bonus_points = bonus_points + earn_poinst;
    }

    const updateData = bonus_points_update(
      smileCustomer.id,
      `order: ${order.id}`,
      'bonus points',
      accountId,
      Math.round(bonus_points),
    );
    if (Math.round(bonus_points) > 0) {
      const { data: updatePoint } = await axios.post(
        updatePointUrl,
        { ...updateData },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
          },
        },
      );
      console.log(updatePoint);
    } else {
      console.log('bonus ponint is 0');
    }
    res.end();
  } catch (error) {
    console.log(error);
    res.end();
  }
});

router.post(
  '/admin/flow-trigger-birthday',
  getAccountId,
  async (req, res, next) => {
    try {
      const db = req.app.db;
      const { id } = req.body;
      const accountId = req.accountID;
      const order = await mj_shopify.order.get(id).catch((err) => {
        console.log(err.message);
      });
      const shopifyCustomerId = order.customer.id;
      const smileIOUrl = configUrl.smileActivityRuleUrl.replace(
        '{account_id}',
        accountId,
      );
      const customerUrl = configUrl.smileCustomerUrl.replace(
        '{email}',
        order.customer.email,
      );
      const updatePointUrl = configUrl.smilePointsTransactionUrl;

      //Find customer in smile io
      const { data: customer } = await axios.get(customerUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
          'smile-client': 'smile-admin',
        },
      });

      //check customer
      if (!customer.customers.length) {
        console.log(`Can't find this customer`);
        return res.end();
      }
      //Get customer in smileio
      const smileCustomer = customer.customers[0];

      //Get rule with points reward in smileio
      const { data: activity_rules } = await axios.get(smileIOUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
        },
      });

      // Get rule with type = 'shopify_online_order'
      const activity_rule = activity_rules.activity_rules.find(
        (e) => e.type === 'shopify_online_order',
      );

      // Get points reward
      const multiplier = activity_rule.multiplier;

      // Retrieve a list of products that have been ordered by the user
      const lineItem = order.line_items;

      let smile_coins_purchase = 0;

      for (let i = 0; i < lineItem.length; i++) {
        if (lineItem[i].product_id === null) {
          continue;
        }
        const product = await mj_shopify.product.get(lineItem[i].product_id);
        const variant = await mj_shopify.productVariant.get(
          lineItem[i].variant_id,
        );
        const tags = product.tags;
        const price = variant.price;
        const quantity = lineItem[i].quantity;

        smile_coins_purchase += price * multiplier * quantity;
      }

      let bonus_points_birthday = Number(smile_coins_purchase);

      let [dbCustomer] = await Promise.all([
        db.customers.findOne({ customerId: String(shopifyCustomerId) }),
      ]);

      if (!dbCustomer) {
        dbCustomer = {
          email: order.customer.email,
          firstName: order.customer.firstName,
          lastName: order.customer.lastName,
          address1: order.customer.default_address.address1,
          address2: order.customer.default_address.address2,
          country: order.customer.default_address.country,
          state: order.customer.state,
          phone: order.customer.phone,
          password: bcrypt.hashSync(order.customer.email, 10),
          customerId: order.customer.id.toString(),
          created: new Date(),
          feedPrivate: false,
          vaultPrivate: false,
          lastOrderBirthdayDate: null,
          lastOrderBirthdayId: '',
          lastOrderBirthdayBonusCoins: null,
        };
        db.customers.insertOne(dbCustomer);
      }

      if (
        ((dbCustomer &&
          dbCustomer.birthday &&
          dayjs(dbCustomer.birthday).format('MM') === dayjs().format('MM')) ||
          (smileCustomer.date_of_birth &&
            dayjs(smileCustomer.date_of_birth).format('MM') ===
              dayjs().format('MM'))) &&
        (!dbCustomer.lastOrderBirthdayDate ||
          (dbCustomer.lastOrderBirthdayDate &&
            dayjs().valueOf() >=
              dayjs(dbCustomer.lastOrderBirthdayDate)
                .add(1, 'year')
                .startOf('month')
                .subtract(1, 'day')
                .endOf('day')
                .valueOf()))
      ) {
        const shopifyCustomer = await mj_shopify.customer.get(
          Number(shopifyCustomerId),
        );

        if (
          shopifyCustomer.tags.includes('Smile VIP - Captain') ||
          shopifyCustomer.tags.includes('smile vip - captain')
        ) {
          bonus_points_birthday *= 2.5;
        }

        if (
          shopifyCustomer.tags.includes('Smile VIP - Quartermaster') ||
          shopifyCustomer.tags.includes('smile vip - quartermaster')
        ) {
          bonus_points_birthday *= 2;
        }

        if (
          shopifyCustomer.tags.includes('Smile VIP - Navigator') ||
          shopifyCustomer.tags.includes('smile vip - navigator')
        ) {
          bonus_points_birthday *= 1.5;
        }

        if (
          shopifyCustomer.tags.includes('Smile VIP - Apprentice') ||
          shopifyCustomer.tags.includes('smile vip - apprentice')
        ) {
          bonus_points_birthday *= 1;
        }

        const bonus_coins = bonus_points_birthday - smile_coins_purchase;

        if (bonus_points_birthday) {
          await Promise.all([
            db.customers.updateOne(
              { customerId: String(shopifyCustomerId) },
              {
                $set: {
                  lastOrderBirthdayDate: new Date(),
                  lastOrderBirthdayId: id,
                  lastOrderBirthdayBonusCoins: bonus_coins,
                },
              },
            ),
          ]);

          const updateData = bonus_points_update(
            smileCustomer.id,
            `order: ${order.id}`,
            'bonus points',
            accountId,
            Math.round(bonus_coins),
          );

          if (Math.round(bonus_coins) > 0) {
            const { data: updatePoint } = await axios.post(
              updatePointUrl,
              { ...updateData },
              {
                headers: {
                  'Content-Type': 'application/json',
                  Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
                },
              },
            );
          }
        }
      }

      return res.end();
    } catch (error) {
      console.log(error);
      return res.end();
    }
  },
);

router.post(
  '/admin/shopify-webhooks/cancel-order-birthday',
  getAccountId,
  async (req, res, next) => {
    try {
      const db = req.app.db;
      const order = req.body;
      const accountId = req.accountID;
      const shopifyCustomerId = order.customer.id;
      const smileIOUrl = configUrl.smileActivityRuleUrl.replace(
        '{account_id}',
        accountId,
      );
      const customerUrl = configUrl.smileCustomerUrl.replace(
        '{email}',
        order.customer.email,
      );
      const updatePointUrl = configUrl.smilePointsTransactionUrl;

      const [dbCustomer] = await Promise.all([
        db.customers.findOne({ customerId: String(shopifyCustomerId) }),
      ]);

      if (
        !dbCustomer.lastOrderBirthdayId ||
        String(dbCustomer.lastOrderBirthdayId) !== String(order.id)
      ) {
        return res.end();
      }

      //Find customer in smile io
      const { data: customer } = await axios.get(customerUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
          'smile-client': 'smile-admin',
        },
      });

      //check customer
      if (!customer.customers.length) {
        console.log(`Can't find this customer`);
        return res.end();
      }
      //Get customer in smileio
      const smileCustomer = customer.customers[0];

      await Promise.all([
        db.customers.updateOne(
          { customerId: String(shopifyCustomerId) },
          {
            $set: {
              lastOrderBirthdayDate: null,
              lastOrderBirthdayId: '',
              lastOrderBirthdayBonusCoins: null,
            },
          },
        ),
      ]);

      const updateData = bonus_points_update(
        smileCustomer.id,
        `cancel order: ${order.id}`,
        'bonus points',
        accountId,
        -Math.round(dbCustomer.lastOrderBirthdayBonusCoins),
      );

      if (Math.round(dbCustomer.lastOrderBirthdayBonusCoins) > 0) {
        const { data: updatePoint } = await axios.post(
          updatePointUrl,
          { ...updateData },
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            },
          },
        );
      }

      return res.end();
    } catch (error) {
      console.log(error);
      return res.end();
    }
  },
);

const vip_tier = [
  { title: 'Apprentice Rank', name: 'Apprentice' },
  { title: 'Navigator Rank', vouchers: '$10 off', name: 'Navigator' },
  { title: 'Quartermaster Rank', vouchers: '$15 off', name: 'Quartermaster' },
  { title: 'Captain Rank', vouchers: '$20 off', name: 'Captain' },
];

router.get('/admin/smile-info/:email', getAccountId, async (req, res, next) => {
  try {
    const db = req.app.db;
    const accountId = req.accountID;
    const email = req.params.email;
    const rewardProgramsPointUrl =
      configUrl.smileRewardProgramsPointUrl.replace('{account_id}', accountId);
    const rewardProgramsReferralUrl =
      configUrl.smileRewardProgramsReferralUrl.replace(
        '{account_id}',
        accountId,
      );
    const customerUrl = configUrl.smileCustomerUrl.replace('{email}', email);
    const pointsTransactionUrl = configUrl.smilePointsTransactionUrl;

    // Get customer and reward_programs in smileIo
    const [
      { data: pointPrograms },
      { data: rewardPrograms },
      { data: customer },
    ] = await Promise.all([
      axios.get(rewardProgramsPointUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
          'smile-client': 'smile-admin',
        },
      }),
      axios.get(rewardProgramsReferralUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
        },
      }),
      axios.get(
        `${customerUrl}?include=vip_tier%2Cnext_vip_tier%2Cexternal_url%2Cis_excluded`,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        },
      ),
    ]);

    if (!customer) {
      return res.json({
        code: 400,
        message: 'Cannot find customer with your email',
      });
    }

    const referralsReward = rewardPrograms.reward_programs.find(
      (e) => e.type === 'referrals',
    );
    const vouchersUrl = configUrl.smileVouchersUrl.replace(
      '{customer_id}',
      customer.customers[0].id,
    );
    const referralsUrl = configUrl.smileReferralsUrl.replace(
      '{reward_program_id}',
      referralsReward.id,
    );
    // const pointTransactionUrl = `https://api.smile.io/v1/points_transactions?customer_id=${customer.customers[0].id}&page=1&page_size=1`;

    const [
      { data: vouchers },
      { data: referrals },
      customerDb,
      { data: pointTransactions },
    ] = await Promise.all([
      axios.get(vouchersUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
          'smile-client': 'smile-admin',
        },
      }),
      axios.get(referralsUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
        },
      }),
      db.customers.findOne({ email }),
      axios.get(
        pointsTransactionUrl + '?customer_id=' + customer.customers[0].id,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${process.env.SMILEIO_PRIVATE_KEY}`,
          },
        },
      ),
    ]);

    const pointTransaction = pointTransactions.points_transactions.find(
      (e) => e.points_change > 0,
    );

    const isCaptain = customer.customers[0].next_vip_tier ? false : true;
    const total_point = Math.round(customer.customers[0].vip_metric);
    const appliedDiscountCodes =
      (customerDb && customerDb.appliedDiscountCodes) || [];

    // Point to next vip tier
    const delta_to_next_vip_tier = isCaptain
      ? total_point
      : Math.round(customer.customers[0].delta_to_next_vip_tier);

    // Current point in this vip tier
    const point_in_this_tier = isCaptain
      ? total_point
      : customer.customers[0].next_vip_tier.range_minimum -
        customer.customers[0].vip_tier.range_minimum -
        Math.round(customer.customers[0].delta_to_next_vip_tier);

    // Total point to next vip tier
    const vip_progress = isCaptain
      ? total_point
      : customer.customers[0].next_vip_tier.range_minimum -
        customer.customers[0].vip_tier.range_minimum;

    const _vouchers = vouchers
      ? [...vouchers.reward_fulfillments]
          .filter((e) => e.usage_status === 'unused')
          .filter((e) => e.source_type !== 'vip_tier_change')
          .filter(
            (e) =>
              !appliedDiscountCodes.includes(e.code) &&
              (!e.expires_at ||
                (e.expires_at &&
                  dayjs().valueOf() < dayjs(e.expires_at).valueOf())),
          )
          .map((e) => ({
            id: e.id,
            name: e.name,
            expires_at: e.expires_at,
            reason_text: e.reason_text,
            used_at: e.used_at,
            usage_status: e.usage_status,
            code: e.code,
          }))
      : [];

    const vipVouchers = vouchers
      ? vouchers.reward_fulfillments
          .filter(
            (v) =>
              v.source_type === 'vip_tier_change' &&
              v.usage_status === 'unused',
          )
          .filter(
            (v) =>
              !appliedDiscountCodes.includes(v.code) &&
              (!v.expires_at ||
                (v.expires_at &&
                  dayjs().valueOf() < dayjs(v.expires_at).valueOf())),
          )
          .map((v) => {
            return {
              id: v.id,
              name: v.name,
              expires_at: v.expires_at,
              description: v.reason_text
                .replace('Entered ', '')
                .replace('tier', 'Rank'),
              used_at: v.used_at,
              usage_status: v.usage_status,
              code: v.code,
            };
          })
          .reverse()
      : [];

    let findIdx =
      vip_tier.findIndex(
        (e) => e.name === customer.customers[0].vip_tier.name,
      ) + 1;
    const lockVochers = [];
    for (findIdx; findIdx < vip_tier.length; findIdx++) {
      lockVochers.push(vip_tier[findIdx]);
    }

    let createdAt = new Date();
    let points_expired_at = null;
    if (pointTransactions && pointTransactions.points_transactions.length) {
      const pointTransaction = pointTransactions.points_transactions.find(
        (e) => e.points_change > 0,
      );
      if (pointTransaction) {
        createdAt = pointTransaction.created_at;
      }
    }

    if (pointPrograms.reward_programs[0].points_expiry_is_enabled) {
      points_expires_at = dayjs(createdAt)
        .add(
          pointPrograms.reward_programs[0].points_expiry_interval_count,
          'day',
        )
        .format();
    }

    const result = {
      customerId: customer.customers[0].id,
      date_of_birth: customer.customers[0].date_of_birth,
      points_balance: customer.customers[0].points_balance,
      points_expires_at,
      vip_tier: customer.customers[0].vip_tier.name,
      vip_tier_expires_at: customer.customers[0].vip_tier_expires_at,
      next_vip_tier: isCaptain
        ? 'Captain'
        : customer.customers[0].next_vip_tier.name,
      delta_to_next_vip_tier,
      point_in_this_tier,
      vip_progress,
      referral_url: customer.customers[0].referral_url,
      friend_tracking_reward:
        referrals.referral_rewards[0].friend_tracking_reward.name,
      advocate_reward: referrals.referral_rewards[0].advocate_reward.name,
      vouchers: _vouchers,
      vipVouchers,
      lockVochers,
    };

    res.json({ code: 200, result });
  } catch (error) {
    console.log(error);
  }
});

router.get('/admin/shopify-customer/:email', async (req, res, next) => {
  try {
    const email = req.params.email;
    const db = req.app.db;
    const [data] = await Promise.all([
      db.customers.findOne(
        { email: email },
        { projection: { _id: 0, birthday: 1, gender: 1 } },
      ),
    ]);
    res.json({ code: 200, data });
  } catch (err) {
    console.log(err);
  }
});

router.get(
  '/admin/download-directly/:fileName',
  common.restrict,
  async (req, res) => {
    const db = req.app.db;
    if (!req.params.fileName) {
      res.status(404).render('error');
      return;
    }
    AWS.config.update({
      region: Config.S3UdidRequest.region,
      signatureVersion: 'v4',
      accessKeyId: Config.S3UdidRequest.accessKeyId,
      secretAccessKey: Config.S3UdidRequest.secretAccessKey,
    });
    const signedUrlExpireSeconds = 60 * 5;
    const s3 = new AWS.S3();
    const url = s3.getSignedUrl('getObject', {
      Bucket: Config.S3UdidRequest.bucket,
      Key: req.params.fileName,
      Expires: signedUrlExpireSeconds,
    });

    res.redirect(url);
  },
);

const getAnalyticsCreepyCuties = async (db, user, filter) => {
  const [analyticsCC] = await Promise.all([]);
};

router.get(
  '/admin/anlyticsCreepyCuties',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
  },
);

module.exports = router;
