const express = require('express');
const common = require('../../lib/common');
const numeral = require('numeral');
const stripe = require('stripe')(common.getPaymentConfig().secretKey);
const router = express.Router();

// The homepage of the site
// router.post('/checkout_action', (req, res, next) => {
//     let db = req.app.db;
//     let config = req.app.config;
//     let stripeConfig = common.getPaymentConfig();
//
//     // charge via stripe
//     stripe.charges.create({
//         amount: numeral(req.session.totalCartAmount).format('0.00').replace('.', ''),
//         currency: stripeConfig.stripeCurrency,
//         source: req.body.stripeToken,
//         description: stripeConfig.stripeDescription
//     }, (err, charge) => {
//         if(err){
//             console.info(err.stack);
//             req.session.messageType = 'danger';
//             req.session.message = 'Your payment has declined. Please try again';
//             req.session.paymentApproved = false;
//             req.session.paymentDetails = '';
//             res.redirect('/pay');
//             return;
//         }
//
//         // order status
//         let paymentStatus = 'Paid';
//         if(charge.paid !== true){
//             paymentStatus = 'Declined';
//         }
//
//         // paymentStatus = 'Shipped';
//
//         // new order doc
//         let orderDoc = {
//             customerId: req.session.customer._id,
//             orderPaymentId: charge.id,
//             orderPaymentGateway: 'Stripe',
//             orderPaymentMessage: charge.outcome.seller_message,
//             orderTotal: req.session.totalCartAmount,
//             orderEmail: req.body.shipEmail,
//             orderFirstname: req.body.shipFirstname,
//             orderLastname: req.body.shipLastname,
//             orderAddr1: req.body.shipAddr1,
//             orderAddr2: req.body.shipAddr2,
//             orderCountry: req.body.shipCountry,
//             orderState: req.body.shipState,
//             orderPostcode: req.body.shipPostcode,
//             orderPhoneNumber: req.body.shipPhoneNumber,
//             orderComment: req.body.orderComment,
//             orderStatus: paymentStatus,
//             // UDID: '6c0f8d495815139dfdb528d706ddc7177ac26b65',
//             // orderModel: 'MJAAA99999',
//             // orderEncryptedModel: 'f3y+AffkGpHx3MwDFH2zA/gVZDDy/PHITP3uY7S5fct0AHW5ZpvCcJj0fJi4SssmmCg29lQtL2aw0zjJW+hFUp6QHkmS3YP/K1qyZ8oJd9MH0Ge91/uULllJ5xtIpGTdY+RxC/hZbKXjFW3WrQPTMCwPShh3YVB+ZrkRnidcShM=',
//             orderDate: new Date(),
//             orderProducts: req.session.cart
//         };
//
//
//         for(let i = 0; i < orderDoc.orderProducts.length; i++){
//             let product = orderDoc.orderProducts[i];
//             product.qrcode = '/images/' + product.productModelNumber + '.jpeg';
//             //
//             // // let number = Math.floor(Math.random() * Math.floor(9999));
//             // // product.productModelNumber = 'MJ' + number;
//             // // product.productEncryptedModel = key.privateEncrypt(product.productModelNumber, 'utf8', 'base64');
//             // let qr_svg = QRCode.image(product.productEncryptedModel, {type: 'png'});
//             // qr_svg.pipe(require('fs').createWriteStream('./public/images/' + product.productModelNumber + '.png'));
//             // product.qrcode = '/images/' + product.productModelNumber + '.png';
//         }
//
//         // insert order into DB
//         db.orders.insert(orderDoc, (err, newDoc) => {
//             if(err){
//                 console.info(err.stack);
//             }
//
//             // get the new ID
//             let newId = newDoc.insertedIds['0'];
//
//             // add to lunr index
//             common.indexOrders(req.app)
//             .then(() => {
//                 // if approved, send email etc
//                 if(charge.paid === true){
//                     // set the results
//                     req.session.messageType = 'success';
//                     req.session.message = 'Your payment was successfully completed';
//                     req.session.paymentEmailAddr = newDoc.ops[0].orderEmail;
//                     req.session.paymentApproved = true;
//                     req.session.paymentDetails = '<p><strong>Order ID: </strong>' + newId + '</p><p><strong>Transaction ID: </strong>' + charge.id + '</p>';
//
//                     // set payment results for email
//                     let paymentResults = {
//                         message: req.session.message,
//                         messageType: req.session.messageType,
//                         paymentEmailAddr: req.session.paymentEmailAddr,
//                         paymentApproved: true,
//                         paymentDetails: req.session.paymentDetails
//                     };
//
//                     // clear the cart
//                     if(req.session.cart){
//                         req.session.cart = null;
//                         req.session.orderId = null;
//                         req.session.totalCartAmount = 0;
//                     }
//
//                     // send the email with the response
//                     // TODO: Should fix this to properly handle result
//                     common.sendEmail(req.session.paymentEmailAddr, 'Your payment with ' + config.cartTitle, common.getEmailTemplate(paymentResults));
//
//                     // redirect to outcome
//                     res.redirect('/payment/' + newId);
//                 }else{
//                     // redirect to failure
//                     req.session.messageType = 'danger';
//                     req.session.message = 'Your payment has declined. Please try again';
//                     req.session.paymentApproved = false;
//                     req.session.paymentDetails = '<p><strong>Order ID: </strong>' + newId + '</p><p><strong>Transaction ID: </strong>' + charge.id + '</p>';
//                     res.redirect('/payment/' + newId);
//                 }
//             });
//         });
//     });
// });

module.exports = router;
