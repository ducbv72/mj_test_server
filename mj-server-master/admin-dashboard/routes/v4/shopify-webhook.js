const express = require('express');
const Promise = require('promise');
const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime');
const crypto = require('crypto');
const { isValidAddress } = require('ethereumjs-util');

const common = require('../../../lib/common');
const {
  getProductFromShopifyById,
  mergeProductFromShopifyToSaveLocal,
} = require('../../../services/shopify');
const { mergeCreepyCuties } = require('../../../services/product');
dayjs.extend(relativeTime);
const { uploadTteNftMetadataToIpfs } = require('../../../services/web3');
const { TTE_NFT_VARIANTS } = require('../../../config/nft');

const router = express.Router();

router.post(
  '/api/v4/hook-product-creation-premium-worked',
  async (req, res) => {
    console.log('🎉 We got an order!');
    try {
      const body = JSON.stringify(req.body);
      console.log('Phew, it came from Shopify!');
      const productShopify = JSON.parse(body);
      await importProductFromShopify(req.app, productShopify, 'PremiumWorked');
      res.sendStatus(200);
    } catch (e) {
      console.log('Something went wrong:');
      console.log(e);
      res.sendStatus(403);
    }
  },
);

router.post('/api/v4/hook-product-creation', async (req, res) => {
  console.log('🎉 We got an order!');
  try {
    const body = JSON.stringify(req.body);
    console.log(body);

    console.log('Phew, it came from Shopify!');
    const productShopify = JSON.parse(body);
    await importProductFromShopify(req.app, productShopify, 'MightyJaxx');
    res.sendStatus(200);
  } catch (e) {
    console.log('Something went wrong:');
    console.log(e);
    res.sendStatus(403);
  }
});

router.post('/api/v4/hook-order-payment', async (req, res) => {
  console.log('🎉 An order has been paid!');

  try {
    const db = req.app.db;

    // We'll compare the hmac to our own hash
    const hmac = req.get('X-Shopify-Hmac-Sha256');
    const rawBody = req.rawBody;

    // Create a hash using the body and our private key
    const hash = crypto
      .createHmac('sha256', process.env.MJ_STORE_WEBHOOK_SECRET_KEY)
      .update(rawBody, 'utf8', 'hex')
      .digest('base64');

    if (hash !== hmac) {
      console.log('Danger! Not from Shopify!');
      return res.sendStatus(403);
    }

    const order = JSON.parse(JSON.stringify(req.body));
    const {
      id: orderId,
      customer: { id: customerId },
      note_attributes: noteAttributes,
      line_items: lineItems,
    } = order;
    let ethAddress = '';

    if (noteAttributes && noteAttributes?.length) {
      noteAttributes.forEach(({ value }) => {
        if (value && isValidAddress(value)) {
          ethAddress = value;
        }
      });
    }

    let skus = [];

    lineItems?.length
      ? lineItems.forEach(({ sku, quantity }) => {
          const result = Array(quantity ?? 1).fill(sku);
          skus = [...skus, ...result];
        })
      : null;

    console.log({ orderId, customerId, ethAddress, skus });

    if (!orderId || !customerId || !ethAddress || !skus) {
      console.log('Missing ethAddress in order -> No need to mint NFT');
      return res.sendStatus(200);
    }

    const VALID_TTE_SKUS = Object.keys(TTE_NFT_VARIANTS);
    const tteSkus = skus.filter((sku) => VALID_TTE_SKUS.includes(sku));

    if (!tteSkus.length) {
      console.log('Not TTE SKUs -> No need to mint NFT');
      return res.sendStatus(200);
    }

    // Fetch availableIds for the NFT names
    const [idTracker, tteNftDocument] = await Promise.all([
      db.tteNfts.findOne({ tokenIdTracker: true }),
      db.tteNfts.findOne({ orderId: orderId.toString() }),
    ]);

    db.tteNfts.insertOne({
      toBeDeleted: true,
      orderId: orderId.toString(),
      updatedAt: new Date(),
    });

    if (tteNftDocument && tteNftDocument?.updatedAt) {
      console.log(
        'tteNftDocument exists for that orderId -> No need to mint NFT',
      );
      return res.sendStatus(200);
    }

    let availableIds = idTracker?.availableIds;
    let _id = idTracker?._id;

    if (!_id) {
      // DB document that tracks IDs needs to be created
      const result = await generateAvailableIds(db);
      availableIds = result?.availableIds;
      _id = result?._id;
    }

    const promises = tteSkus.map(async (sku) => {
      // Generate a random ID for the NFT name
      const randomId =
        availableIds[Math.floor(Math.random() * availableIds.length)];
      availableIds = availableIds.filter((x) => x !== randomId);
      const nftName = `#${randomId}`;
      let tokenURI = '';

      try {
        tokenURI = await uploadTteNftMetadataToIpfs(nftName, sku);
      } catch {
        // Rate limit reached. A cron job will try again later on
      }

      const transactionHash = '';
      const tokenId = '';
      const status = '';
      const { nftType } = TTE_NFT_VARIANTS[sku];

      if (!nftType) {
        console.log('Invalid nftType:', nftType);
        return res.sendStatus(500);
      }

      // Save data to DB. A cron job will use that data to mint the NFT
      await db.tteNfts.insertOne({
        customerId: customerId.toString(),
        orderId: orderId.toString(),
        randomId,
        sku,
        nftName,
        ethAddress,
        lastTxHash: transactionHash,
        tokenId,
        status,
        tokenURI,
        updatedAt: new Date(),
      });
    });

    await Promise.all(promises);
    await Promise.all([
      db.tteNfts.updateOne({ _id }, { $set: { availableIds } }),
      db.tteNfts.deleteOne({
        toBeDeleted: true,
        orderId: orderId.toString(),
      }),
    ]);

    return res.sendStatus(200);
  } catch (e) {
    console.log('Something went wrong:');
    console.log(e);
    return res.sendStatus(403);
  }
});

const generateAvailableIds = async (db) => {
  const availableIds = [];

  for (let i = 1; i <= 1200; i++) {
    availableIds.push(i);
  }

  const { insertedId } = await db.tteNfts.insertOne({
    tokenIdTracker: true,
    availableIds,
    updatedAt: new Date(),
  });

  return { availableIds, _id: insertedId };
};

router.post('/api/v4/hook-product-creation-jason-freeny', async (req, res) => {
  console.log('🎉 We got an order!');
  // We'll compare the hmac to our own hash
  const hmac = req.get('X-Shopify-Hmac-Sha256');
  // Use raw-body to get the body (buffer)
  try {
    const body = JSON.stringify(req.body);
    console.log(body);
    // Create a hash using the body and our key
    // const hash = crypto
    //     .createHmac('sha256', Config.MJSharedSecretKey)
    //     .update(body, 'utf8', 'hex')
    //     .digest('base64')
    // console.log(body);
    // Compare our hash to Shopify's hash
    // if (hash === hmac) {
    // It's a match! All good
    console.log('Phew, it came from Shopify!');
    const productShopify = JSON.parse(body);
    await importProductFromShopify(req.app, productShopify, 'JasonFreeny');
    return res.sendStatus(200);
  } catch (e) {
    console.log('Something went wrong:');
    console.log(e);
    return res.sendStatus(403);
  }
});

const importProductFromShopify = async function (
  app,
  shopifyProductHook,
  storeName,
) {
  const db = app.db;
  const variants = shopifyProductHook.variants;
  const productShopify = await getProductFromShopifyById(shopifyProductHook.id);

  for (const item of variants) {
    console.log('Processing hook notification sku: ', item.sku);
    if (item.sku && item.sku.toString() !== '') {
      let productDoc = await db.products.findOne({
        $and: [{ productVariantSku: item.sku.toString() }],
      });
      if (!productDoc) {
        productDoc = {
          productPermalink: item.id.toString(),
          shopifyProductId: shopifyProductHook.id.toString(),
          udid: '',
          productStore: storeName,
          productModelNumber: '',
          productVariantId: item.id.toString(),
          productVariantSku: item.sku.toString(),
          productTitle: shopifyProductHook.title,
          productPrice: item.price,
          productDescription: shopifyProductHook.body_html,
          productPublished: 1,
          productTags: shopifyProductHook.tags,
          productOptions: '',
          productComment: 1,
          productAddedDate: shopifyProductHook.created_at
            ? shopifyProductHook.created_at
            : new Date(),
          productStock: item.inventory_quantity,
          productNew: 0,
          productTrending: 0,
          enableEditionNumber: 0,
          productFeature: 0,
          productRecommendation: 0,
          productPublishedAt: shopifyProductHook.published_at,
          productImageUrl: shopifyProductHook.image
            ? common.cleanHtml(shopifyProductHook.image.src)
            : '/images/default-image.png',
        };
      } else {
        productDoc.productVariantId = item.id.toString();
        productDoc.shopifyProductId = shopifyProductHook.id.toString();
        productDoc.productImageUrl = shopifyProductHook.image
          ? common.cleanHtml(shopifyProductHook.image.src)
          : '/images/default-image.png';
      }

      if (productShopify) {
        // merging data from shopify
        productDoc = await mergeProductFromShopifyToSaveLocal(
          productDoc,
          productShopify,
        );
      }

      //merge CreepyCuties
      productDoc = await mergeCreepyCuties(productDoc);

      if (productDoc._id && shopifyProductHook.id.toString() !== '') {
        if (productDoc.shopifyProductId === shopifyProductHook.id.toString()) {
          //save to log
          await common.saveToActionLog(
            db,
            'shopify_hook',
            'update_product',
            productDoc,
          );
          //update product
          await Promise.all([
            db.products.update(
              { _id: common.getId(productDoc._id) },
              { $set: productDoc },
              { multi: false },
            ),
          ]);
        } else {
          //send email report to admin about duplicate sku
          if (process.env.ADMIN_EMAIL !== '') {
            const subject = 'Duplicate SKU - ' + item.sku.toString();
            const body = `Hello,
It appears a duplicate of SKU: ${item.sku.toString()} has been created on Shopify.  We have blocked a duplicate record from being made on the admin dashboard.  Please check the admin dashboard to see if this was done by accident.  If the product first created then deleted and you are attempting to create a new product on Shopify, then you will need to follow the instructions below.
In order for the existing product on the admin dashboard to be updated with changes from Shopify, please update the Shopify product ID in the admin dashboard with the the following ID:

${shopifyProductHook.id.toString()}

Once updated, please proceed back to the product on Shopify and save the changes again.  This will update the product details on the admin dashboard.  Should you have any further questions, please reach out to Rob via Slack or at robert.deboer@mightyjaxx.com.
Thanks! `;
            console.log(process.env.ADMIN_EMAIL);
            common.sendEmail(process.env.ADMIN_EMAIL, '', subject, body, null);
          }
        }
      } else {
        // insert product
        await Promise.all([db.products.insert(productDoc)]);
        await common.saveToActionLog(
          db,
          'shopify_hook',
          'add_new_product',
          productDoc,
        );
        console.log(
          'save product: ' + shopifyProductHook.id.toString() + ' successfully',
        );
      }
    }
  }
};

module.exports = router;
