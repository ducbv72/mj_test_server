const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const fs = require('fs');
const router = express.Router();
const formidable = require('formidable');
const mime = require('mime-type/with-db');
const moment = require('moment');

router.get(
  '/admin/app-released',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    // get all results
    const [appRelease] = await Promise.all([
      db.appReleased.find({}).sort({ dateRelease: -1 }).toArray(),
    ]);
    let currentReleaseVersion = null;
    let nextReleaseVersion = null;
    if (appRelease.length > 0) {
      currentReleaseVersion = appRelease[0];
      let releaseDate = new moment(currentReleaseVersion.dateRelease);
      releaseDate = releaseDate.add(
        currentReleaseVersion.delayToRelease
          ? currentReleaseVersion.delayToRelease
          : 0,
        'hours',
      );
      if (moment.now().valueOf() < releaseDate.valueOf()) {
        nextReleaseVersion = appRelease[0];
        if (appRelease.length > 1) {
          currentReleaseVersion = appRelease[1];
        } else {
          currentReleaseVersion = null;
        }
      } else {
        nextReleaseVersion = null;
      }
    }
    res.render('manage_release/list_app_release', {
      title: 'app-released',
      results: appRelease,
      currentReleaseVersion: currentReleaseVersion,
      nextReleaseVersion: nextReleaseVersion,
      session: req.session,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
      menu: 'setting',
    });
  },
);

// insert form
router.get(
  '/admin/app-release/new',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('manage_release/app_release_new', {
      title: 'New Release',
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
      menu: 'setting',
    });
  },
);

// insert new product form action
router.post(
  '/admin/app-release/insert',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    if (!fs.existsSync('./public/uploads/appRelease/')) {
      fs.mkdirSync('./public/uploads/appRelease/');
    }
    const form = new formidable.IncomingForm();
    form.uploadDir = './public/uploads/appRelease/';
    form.parse(req, async (err, fields, files) => {
      if (err) {
        console.log(colors.red('Error Creating new release: ' + err));
        // keep the current stuff
        req.session.message = 'Error: Creating new release';
        req.session.messageType = 'danger';
        // redirect to insert
        res.redirect('/admin/app-release/new');
        return;
      }
      const mimeTypeApk = mime.lookup(files.file.name);
      if (!common.allowedAPKType.includes(mimeTypeApk)) {
        // Remove temp file
        fs.unlinkSync(files.file.path);
        // Redirect to error
        req.session.message =
          'File type not allowed or too large. Please try again.';
        req.session.messageType = 'danger';
        res.redirect('/admin/app-release/new');
        return;
      }

      let appFileName = '';
      if (files.file.size > 0) {
        const tmpPath = files.file.path;
        const newPath = form.uploadDir + files.file.name;
        appFileName = '/uploads/appRelease/' + files.file.name;
        fs.renameSync(tmpPath, newPath);
      }

      const doc = {
        versionName: common.cleanHtml(fields.versionName),
        versionCode: common.cleanHtml(fields.versionCode),
        noteRelease: common.cleanHtml(fields.noteRelease),
        dateRelease: new Date(),
        delayToRelease: fields.delayToRelease,
        appUrl: appFileName,
      };

      const [result] = await Promise.all([db.appReleased.insert(doc)]);

      res.redirect('/admin/app-release/edit/' + result.insertedIds[0]);
    });
  },
);

// render the editor
router.get(
  '/admin/app-release/edit/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    db.appReleased.findOne(
      { _id: common.getId(req.params.id) },
      (err, result) => {
        if (err) {
          console.info(err.stack);
        }
        res.render('manage_release/app_release_edit', {
          title: 'Edit Release',
          release: result,
          menu: 'setting',
          admin: true,
          session: req.session,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          config: req.app.config,
          editor: true,
          helpers: req.handlebars.helpers,
        });
      },
    );
  },
);

router.get(
  '/admin/store/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.redirect('/admin/stores');
  },
);

// Update an existing product form action
router.post(
  '/admin/app-release/update',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const form = new formidable.IncomingForm();
    if (!fs.existsSync('./public/uploads/appRelease/')) {
      fs.mkdirSync('./public/uploads/appRelease/');
    }
    form.uploadDir = './public/uploads/appRelease/';
    form.parse(req, async (err, fields, files) => {
      const [releaseItem] = await Promise.all([
        db.appReleased.findOne({ _id: common.getId(fields.releaseId) }),
      ]);
      if (!releaseItem) {
        req.session.message = 'Not found';
        req.session.messageType = 'danger';
        res.redirect('/admin/app-released');
      } else {
        let appFileName = '';
        if (files.file.size > 0) {
          const tmpPath = files.file.path;
          const newPath = form.uploadDir + files.file.name;
          appFileName = '/uploads/appRelease/' + files.file.name;
          fs.renameSync(tmpPath, newPath);
        }
        if (appFileName !== '') {
          releaseItem.appUrl = appFileName;
        }
        releaseItem.versionName = fields.versionName;
        releaseItem.versionCode = fields.versionCode;
        releaseItem.noteRelease = fields.noteRelease;
        releaseItem.delayToRelease = fields.delayToRelease;
        releaseItem.updated = new Date();
        await db.appReleased.update(
          { _id: common.getId(releaseItem._id) },
          { $set: releaseItem },
          false,
        );
        req.session.message = 'Successfully saved';
        req.session.messageType = 'success';
        res.redirect('/admin/app-released');
      }
    });
  },
);

// delete product
router.get(
  '/admin/app-release/delete/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    // remove the article
    db.appReleased.remove({ _id: common.getId(req.params.id) }, {}, (err) => {
      if (err) {
        console.info(err.stack);
      }
      req.session.message = 'Successfully deleted';
      req.session.messageType = 'success';
      res.redirect('/admin/app-released');
    });
  },
);

router.get('/admin/app-release/download-url', async (req, res) => {
  const db = req.app.db;
  const [appRelease] = await Promise.all([
    db.appReleased.find({}).sort({ dateRelease: -1 }).limit(1).toArray(),
  ]);
  if (appRelease.length === 1) {
    return res.redirect(appRelease[0].appUrl);
  } else {
    return res.status(400).json({
      code: 400,
      message: 'Not Found',
    });
  }
});

module.exports = router;
