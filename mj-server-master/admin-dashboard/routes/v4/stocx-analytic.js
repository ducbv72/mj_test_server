const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const router = express.Router();
const Promise = require('promise');
const Config = require('../../../config/ethereumConfig');
const csrf = require('csurf');
const csv = require('csvtojson');
const fs = require('fs');
const formidable = require('formidable');
const parse = require('url-parse');
const mime = require('mime-type/with-db');

router.get(
  '/admin/analytics/stockx',
  common.restrict,
  async (req, res, next) => {
    const db = req.app.db;
    const [stockReports] = await Promise.all([
      db.stockAnalytics
        .aggregate([
          {
            $project: {
              _id: 1,
              startDate: 1,
              endDate: 1,
              date: {
                $dateFromString: {
                  dateString: '$startDate',
                },
              },
            },
          },
          { $sort: { date: -1 } },
        ])
        .toArray(),
    ]);
    let reportId = req.param('id');
    if (stockReports.length > 0 && !reportId) {
      reportId = stockReports[0]._id;
    }
    let [stockReportDetail, stockReport] = [null, null];
    if (reportId == 'alltime') {
      [stockReportDetail, stockReport] = await Promise.all([
        db.stockDetailAnalytics
          .aggregate([
            {
              $lookup: {
                from: 'products',
                localField: 'sku',
                foreignField: 'productVariantSku',
                as: 'product',
              },
            },
            { $match: { product: { $ne: [] } } },
            {
              $group: {
                _id: {
                  sku: '$sku',
                  productTitle: '$productTitle',
                  product: '$product',
                },
                totalClick: { $sum: '$totalClick' },
              },
            },
          ])
          .toArray(),
        db.stockAnalytics
          .aggregate([
            {
              $group: {
                _id: null,
                totalAction: { $sum: '$totalAction' },
                totalClickGA: { $sum: '$totalClickGA' },
                totalEarn: { $sum: '$totalEarn' },
                totalActionEarn: { $sum: '$totalActionEarn' },
                totalPurchase: { $sum: '$totalPurchase' },
              },
            },
          ])
          .toArray(),
      ]);
    } else {
      [stockReportDetail, stockReport] = await Promise.all([
        db.stockDetailAnalytics
          .aggregate([
            {
              $lookup: {
                from: 'products',
                localField: 'sku',
                foreignField: 'productVariantSku',
                as: 'product',
              },
            },
            {
              $match: {
                stockReportId: common.getId(reportId),
              },
            },
          ])
          .toArray(),
        db.stockAnalytics.findOne({ _id: common.getId(reportId) }),
      ]);
    }
    res.render('stockx_analytics', {
      menu: 'analytic',
      title: 'Dashboard',
      stockReport: stockReport instanceof Array ? stockReport[0] : stockReport,
      stockReports: stockReports,
      stockReportDetail: stockReportDetail,
      referringUrl: req.header('Referer'),
      selectedId: reportId,
      config: req.app.config,
      session: req.session,
      editor: true,
      admin: true,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

// insert form
router.get(
  '/admin/stockx-analytics/new',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('stockx_analytics_new', {
      menu: 'analytic',
      title: 'Upload stockx report',
      session: req.session,
      storeName: common.clearSessionValue(req.session, 'storeName'),
      storeHandle: common.clearSessionValue(req.session, 'storeHandle'),
      isFeatureCollection: common.clearSessionValue(
        req.session,
        'isFeatureCollection',
      ),
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
    });
  },
);

router.post(
  '/admin/stockx-analytics/insert',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const form = new formidable.IncomingForm();
    form.uploadDir = './public/uploads/stores/';
    form.parse(req, async (err, fields, files) => {
      //delete report & detail
      const [result] = await Promise.all([
        db.stockAnalytics.remove({ startDate: fields.start_date }),
        db.stockDetailAnalytics.remove({ startDate: fields.start_date }),
      ]);
      if (files.file_ga.size > 0 && files.file_stockx.size > 0) {
        const mimeTypeGa = mime.lookup(files.file_ga.name);
        const mimeTypeStockx = mime.lookup(files.file_stockx.name);
        if (
          !common.allowedCSVType.includes(mimeTypeGa) ||
          files.file_ga.size > common.fileSizeLimit ||
          !common.allowedCSVType.includes(mimeTypeStockx) ||
          files.file_stockx.size > common.fileSizeLimit
        ) {
          // Remove temp file
          fs.unlinkSync(files.file_ga.path);
          // Redirect to error
          req.session.message =
            'File type not allowed or too large. Please try again.';
          req.session.messageType = 'danger';
          res.redirect('/admin/analytics/stockx');
          return;
        }
        const gaReports = await csv().fromFile(files.file_ga.path);
        const stockxReports = await csv().fromFile(files.file_stockx.path);
        const arrayDetail = [];
        let totalClickGA = 0;
        // process GA report
        for (let i = 0; i < gaReports.length; i++) {
          const item = gaReports[i];
          if (
            item['field2'] &&
            item['# ----------------------------------------'].startsWith('/')
          ) {
            const productUrl =
              item['# ----------------------------------------'];
            totalClickGA += parseInt(item['field2']);
            const parsed = parse(productUrl);
            const pathname = parsed.pathname;
            const handle = pathname.split('/').pop();
            const [product] = await Promise.all([
              db.products.findOne({ productUrl: handle.trim() }),
            ]);
            let foundItem = null;
            if (product) {
              foundItem = arrayDetail.find(
                (x) => x.sku === product.productVariantSku,
              );
            }
            if (!foundItem) {
              const newDoc = {
                startDate: fields.start_date,
                endDate: fields.end_date,
                productTitle: product ? product.productTitle : handle,
                sku: product ? product.productVariantSku : 'Unknown',
                totalClick: parseInt(item['field2']),
              };
              arrayDetail.push(newDoc);
            } else {
              foundItem.totalClick += parseInt(item['field2']);
            }
          }
        }

        // process stockx report
        let totalClickStock = 0;
        let totalPurchase = 0;
        let totalOther = 0;
        let totalEarn = 0;
        let totalActionEarn = 0;
        let totalAction = 0;
        for (let i = 0; i < stockxReports.length; i++) {
          const item = stockxReports[i];
          totalClickStock += parseInt(item.Clicks);
          if (parseFloat(item['Sale Amount']) > 0) {
            totalPurchase += parseFloat(item['Sale Amount']);
          }
          if (parseFloat(item['Other Earnings']) > 0) {
            totalOther += parseFloat(item['Other Earnings']);
          }
          if (parseFloat(item['Total Earnings']) > 0) {
            totalEarn += parseFloat(item['Total Earnings']);
          }
          if (parseFloat(item['Action Earnings']) > 0) {
            totalActionEarn += parseFloat(item['Action Earnings']);
          }
          if (parseInt(item['Actions']) > 0) {
            totalAction += parseFloat(item['Actions']);
          }
        }

        const newDoc = {
          startDate: fields.start_date,
          endDate: fields.end_date,
          totalAction: totalAction,
          totalClickGA: totalClickGA,
          totalClickStock: totalClickStock,
          totalPurchase: parseFloat(totalPurchase.toFixed(2)),
          totalActionEarn: parseFloat(totalActionEarn.toFixed(2)),
          totalOther: parseFloat(totalOther.toFixed(2)),
          totalEarn: parseFloat(totalEarn.toFixed(2)),
        };

        const [result] = await Promise.all([db.stockAnalytics.insert(newDoc)]);
        if (result) {
          arrayDetail.forEach((item) => {
            item.stockReportId = result.ops[0]._id;
          });
        }
        if (arrayDetail.length > 0) {
          const [result1] = await Promise.all([
            db.stockDetailAnalytics.insertMany(arrayDetail),
          ]);
        }

        req.session.message = 'Created Report';
        req.session.messageType = 'success';
        res.redirect('/admin/analytics/stockx');
      } else {
        req.session.message = 'Failed upload report';
        req.session.messageType = 'danger';
        res.redirect('/admin/analytics/stockx');
      }
    });
  },
);

module.exports = router;
