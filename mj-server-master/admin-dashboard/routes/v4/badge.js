const express = require('express');
const router = express.Router();
const common = require('../../../lib/common');
const Config = require('../../../config');
const mime = require('mime-type/with-db');
const multer = require('multer');
const path = require('path');
const sharp = require('sharp');
const fs = require('fs');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: Config.S3.accessKeyId,
  secretAccessKey: Config.S3.secretAccessKey,
});
const Jimp = require('jimp');

// DB Badge doc
/*const badge = {
  _id: ObjectId(''),
  createdDate: new Date(),
  badgeCategory: '',
  name: '',
  description: '',
  iconImage: '',
  unlockCriteria: '',
  matchingCriteria: ['', ''],
  scanCountRequired: 0,
  backgroundSkin: '',
  shelfSkin: '',
};

// DB user badge doc
const userBadge = {
  userId: '',
  badges: [
    {
      _id: '',
      name: '',
      unlockDate: new Date(),
      scanCount: 1,
    },
  ],
};*/
const storageBadge = multer.diskStorage({
  destination: function (req, file, cb) {
    const dir = 'public/uploads/badges-' + req.session.userId + '/';
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    cb(null, 'public/uploads/badges-' + req.session.userId + '/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const listUploadImage = multer({ storage: storageBadge });
// gray scale
async function grayImage(url) {
  const image = await Jimp.read(url);
  await image.grayscale();
  //image.writeAsync(`test/${Date.now()}_gray.png`);
  const folder = Config.S3.socialFeedFolder;
  const params = {
    Bucket: Config.S3.bucket, //Enter the bucket name that you created
    Key: `${folder}/${Date.now()}.jpg`, //filename to use on the cloud
    Body: await image.getBufferAsync(Jimp.MIME_PNG),
    ContentType: 'image/jpg',
    CacheControl: 'max-age=172800',
    ACL: 'public-read', //To make file publicly accessible through URL
  };
  const s3Upload = await s3.upload(params).promise();
  return s3Upload.Location;
}

router.get(
  '/admin/badges',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const [badges] = await Promise.all([
      db.badges
        .aggregate([
          {
            $project: {
              _id: '$_id',
              createdDate: '$createdDate',
              badgeCategory: '$badgeCategory',
              name: '$name',
              iconImage: '$iconImage',
            },
          },
        ])
        .sort({
          badgeCategory: -1,
          name: -1,
        })
        .toArray(),
    ]);

    res.render('badges', {
      title: 'Badges',
      menu: 'badge',
      session: req.session,
      badge_list: badges,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.get(
  '/admin/badges/delete/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const id = req.params.id;
    const [badge, userBadge] = await Promise.all([
      db.badges.findOne({ _id: common.getId(req.params.id) }),
      db.userBadges.findOne({
        badges: {
          $elemMatch: { id: req.params.id, unlockedDate: { $ne: null } },
        },
      }),
    ]);

    if (badge && !userBadge) {
      await Promise.all([
        badge.iconImage ? deleteS3ObjectDirect(badge.iconImage) : null,
        badge.backgroundSkin
          ? deleteS3ObjectDirect(badge.backgroundSkin)
          : null,
        badge.shelfSkin ? deleteS3ObjectDirect(badge.shelfSkin) : null,
      ]);

      await Promise.all([db.badges.deleteOne({ _id: common.getId(id) })]);

      req.session.message = 'Badge successfully deleted';
      req.session.messageType = 'success';
    } else {
      req.session.message =
        'This Badge have been awarded for user, can not delete it';
      req.session.messageType = 'danger';
    }

    res.redirect('/admin/badges');
  },
);

router.get(
  '/admin/badges/new',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    res.render('badge_new', {
      title: 'New Badge',
      menu: 'badge',
      session: req.session,
      badge: common.clearSessionValue(req.session, 'badge'),
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.get(
  '/admin/badges/edit/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const [badge] = await Promise.all([
      db.badges.findOne({ _id: common.getId(req.params.id) }),
    ]);

    if (!badge) {
      return res.redirect('/admin/badges');
    }

    res.render('badge_edit', {
      title: 'Edit Badge',
      menu: 'badge',
      session: req.session,
      badge: badge,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.post(
  '/admin/badges/insert',
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([
    { name: 'iconImage', maxCount: 1 },
    { name: 'backgroundSkin' },
    { name: 'shelfSkin' },
  ]),
  async (req, res) => {
    const db = req.app.db;
    const currentDate = new Date();

    const { iconImage, backgroundSkin, shelfSkin } = req.files;
    const folder = Config.S3.socialFeedFolder;

    const [checkIcon, checkBackground, checkShelf] = await Promise.all([
      iconImage ? checkFile(common, iconImage[0]) : [],
      backgroundSkin ? checkFile(common, backgroundSkin[0]) : [],
      shelfSkin ? checkFile(common, shelfSkin[0]) : [],
    ]);

    let errors = [];

    errors = checkIcon.concat(checkBackground, checkShelf);
    if (errors.length) {
      return { errors, result: null };
    }

    const [iconImageUrl] = await Promise.all([
      iconImage ? uploadFileToS3(iconImage[0], folder) : '',
    ]);

    const imgInActiveUrl = iconImage ? await grayImage(iconImageUrl) : '';
    const backgroundImageUrl = [];
    if (backgroundSkin) {
      for (let i = 0; i < backgroundSkin.length; i++) {
        const url = await uploadFileToS3(backgroundSkin[i], folder);
        backgroundImageUrl.push(url);
      }
    }
    const shelfImageUrl = [];
    if (shelfSkin) {
      for (let i = 0; i < shelfSkin.length; i++) {
        const url = await uploadFileToS3(shelfSkin[i], folder);
        shelfImageUrl.push(url);
      }
    }
    const badge = {
      createdDate: currentDate,
      badgeCategory: common.cleanHtml(req.body.badgeCategory),
      name: common.cleanHtml(req.body.badgeName),
      description: common.cleanHtml(req.body.badgeDescription),
      iconImage: iconImageUrl,
      unlockCriteria: req.body.earningMethod,
      matchingCriteria: req.body.matchingCriteria.replace(/(\s*,?\s*)*$/, ''),
      scanCountRequired: parseInt(req.body.scanCountRequired.trim()),
      backgroundSkin: backgroundImageUrl,
      shelfSkin: shelfImageUrl,
      iconImageInActive: imgInActiveUrl,
      totalAward: 0,
    };

    await Promise.all([db.badges.insert(badge)]);
    fs.rmSync('./public/uploads/badges-' + req.session.userId + '/', {
      recursive: true,
      force: true,
    });
    req.session.message = 'Successfully saved';
    req.session.messageType = 'success';
    res.redirect('/admin/badges');
  },
);

router.post(
  '/admin/badges/update',
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([
    { name: 'iconImage', maxCount: 1 },
    { name: 'backgroundSkin' },
    { name: 'shelfSkin' },
  ]),
  async (req, res) => {
    const db = req.app.db;
    const [badge, awarded] = await Promise.all([
      db.badges.findOne({ _id: common.getId(req.body.frmBadgeId) }),
      db.userBadges.findOne({
        badges: {
          $elemMatch: { id: req.body.frmBadgeId, unlockedDate: { $ne: null } },
        },
      }),
    ]);
    if (badge && !awarded) {
      badge.name = common.cleanHtml(req.body.badgeName);
      badge.description = common.cleanHtml(req.body.badgeDescription);
      badge.badgeCategory = common.cleanHtml(req.body.badgeCategory);
      badge.unlockCriteria = req.body.earningMethod;
      badge.matchingCriteria = req.body.matchingCriteria.replace(
        /(\s*,?\s*)*$/,
        '',
      );
      badge.scanCountRequired = parseInt(req.body.scanCountRequired.trim());

      const { iconImage, backgroundSkin, shelfSkin } = req.files;
      const folder = Config.S3.socialFeedFolder;

      const [checkIcon, checkBackground, checkShelf] = await Promise.all([
        iconImage ? checkFile(common, iconImage[0]) : [],
        backgroundSkin ? checkFile(common, backgroundSkin[0]) : [],
        shelfSkin ? checkFile(common, shelfSkin[0]) : [],
      ]);

      let errors = [];

      errors = checkIcon.concat(checkBackground, checkShelf);
      if (errors.length) {
        return { errors, result: null };
      }

      await Promise.all([
        iconImage ? deleteS3Object(iconImage, badge.iconImage) : null,
      ]);
      if (shelfSkin) {
        for (let i = 0; i < badge.shelfSkin.length; i++) {
          deleteS3ObjectDirect(badge.shelfSkin[i]);
        }
      }
      if (backgroundSkin) {
        for (const item of backgroundSkin) {
          deleteS3Object(item, badge.backgroundSkin);
        }
      }
      const [iconImageUrl] = await Promise.all([
        iconImage ? uploadFileToS3(iconImage[0], folder) : '',
      ]);
      const imgInActiveUrl = iconImage ? await grayImage(iconImageUrl) : '';
      const shelfImageUrl = [];
      //console.log(shelfSkin);
      if (shelfSkin) {
        for (let i = 0; i < shelfSkin.length; i++) {
          const file = shelfSkin[i];
          shelfImageUrl.push(await uploadFileToS3(file, folder));
        }
      }
      const backgroundSkinUrl = [];
      if (backgroundSkin) {
        for (let i = 0; i < backgroundSkin.length; i++) {
          const url = await uploadFileToS3(backgroundSkin[i], folder);
          backgroundSkinUrl.push(url);
        }
      }

      if (iconImage) {
        badge.iconImage = iconImageUrl;
        badge.iconImageInActive = imgInActiveUrl;
      }
      if (backgroundSkin) badge.backgroundSkin = backgroundSkinUrl;
      if (shelfSkin) badge.shelfSkin = shelfImageUrl;

      await Promise.all([
        db.badges.update(
          { _id: common.getId(req.body.frmBadgeId) },
          { $set: badge },
          { multi: false },
        ),
      ]);
      fs.rmSync('./public/uploads/badges-' + req.session.userId + '/', {
        recursive: true,
        force: true,
      });

      req.session.message = 'Successfully saved';
      req.session.messageType = 'success';
    } else {
      req.session.message =
        'This Badge have been awarded for user, can not update it';
      req.session.messageType = 'danger';
    }

    res.redirect('/admin/badges');
  },
);

const uploadFileToS3 = async (file, folder) => {
  const imgFormat = file.mimetype.slice(6).toString();
  const fileData = await sharp(file.path)
    .resize()
    .toFormat(imgFormat)
    .toBuffer();
  const params = {
    Bucket: Config.S3.bucket, //Enter the bucket name that you created
    Key: `${folder}/${Date.now()}${file.originalname}`, //filename to use on the cloud
    Body: fileData,
    ContentType: file.mimetype,
    CacheControl: 'max-age=172800',
    ACL: 'public-read', //To make file publicly accessible through URL
  };
  const s3Upload = await s3.upload(params).promise();
  // fs.unlinkSync(file.path);
  return s3Upload.Location;
};

const splitS3Path = (path) => {
  const url = new URL(path);
  const bucket = Config.S3.bucket;
  const key = url.pathname.substring(1);
  return { bucket, key };
};

const deleteS3ObjectDirect = async (oldPath) => {
  if (oldPath.length) {
    const { bucket, key } = splitS3Path(oldPath);
    await s3
      .deleteObjects({
        Bucket: bucket,
        Delete: { Objects: [{ Key: key }] },
      })
      .promise();
  }
};

const deleteS3Object = async (newData, oldPath) => {
  if (newData && oldPath.length) {
    const { bucket, key } = splitS3Path(oldPath);
    await s3
      .deleteObjects({
        Bucket: bucket,
        Delete: { Objects: [{ Key: key }] },
      })
      .promise();
  }
};

const checkFile = (common, file) => {
  const errors = [];
  // Get the mime type of the file
  const mimeType = mime.lookup(file.originalname);

  // Check for allowed mime type and file size
  if (file.size > common.fileSizeLimit) {
    errors.push({
      statusCode: 413,
      message: `File ${file.originalname} type too large. Please try again.`,
    });
  }

  if (!common.allowedMimeType.includes(mimeType)) {
    errors.push({
      statusCode: 415,
      message: `File ${file.originalname} type not allowed. Please try again.`,
    });
  }
  if (errors.length) {
    // Remove temp file
    fs.unlinkSync(file.path);
  }

  return errors;
};

module.exports = router;
