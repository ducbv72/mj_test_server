const express = require('express');
const common = require('../../../lib/common');
const fs = require('fs');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const ObjectId = require('mongodb').ObjectId;

const {
  getCommunityProfileForm,
  editCommunityProfile,
  insertCommunityProfile,
  deleteCommunityProfile,
  getPollCreateForm,
  insertProfilePoll,
} = require('../../../services/community-profile.service');

const storageBrandProfile = multer.diskStorage({
  destination: function (req, file, cb) {
    const dir = 'public/uploads/brandProfiles/';

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    cb(null, 'public/uploads/brandProfiles/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const listUploadImage = multer({ storage: storageBrandProfile });

//redirect to brand profiles
router.get(
  '/admin/community-profile',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    res.redirect('/admin/community-profile/type/brands');
  },
);

router.get(
  '/admin/community-profile/type/:profileType',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    const defineCase = req.params.profileType;
    const profileType =
      defineCase.charAt(0).toUpperCase() +
      defineCase.slice(1, defineCase.length - 1);
    // get the top results
    const results = await db.profiles
      .aggregate([
        {
          $match: {
            profileType: profileType,
          },
        },
        {
          $lookup: {
            from: 'polls',
            let: { id: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$profileId'] }],
                  },
                },
              },
            ],
            as: 'polls',
          },
        },
        {
          $lookup: {
            from: 'quizs',
            let: { id: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$profileId'] }],
                  },
                },
              },
            ],
            as: 'quizs',
          },
        },
        {
          $project: {
            profileName: 1,
            profileType: 1,
            cardImage: 1,
            logoImage: 1,
            bannerImage: 1,
            createdAt: 1,
            updatedAt: 1,
            canPublish: 1,
            isPublished: 1,
            order: 1,
            numOfPolls: { $size: '$polls' },
            numOfQuizs: { $size: '$quizs' },
            hasPolls: { $gt: [{ $size: '$polls' }, 0] },
            hasQuizs: { $gt: [{ $size: '$quizs' }, 0] },
          },
        },
        {
          $sort: { order: 1 },
        },
      ])
      .toArray();
    res.render('community_profile', {
      title: 'Community Profiles',
      results: results,
      session: req.session,
      admin: true,
      profileType: profileType,
      defineCase,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
      menu: 'profile',
    });
  },
);

// insert form
router.get(
  '/admin/community-profile/new',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const result = await getCommunityProfileForm(req, common);
    res.render('community_profile_new', result);
  },
);

router.post(
  '/admin/community-profile/insert',
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([
    { name: 'cardImage', maxCount: 1 },
    { name: 'logoImage', maxCount: 1 },
    { name: 'bannerImage', maxCount: 1 },
  ]),
  async (req, res) => {
    const { errors, data, profileType } = await insertCommunityProfile(
      req,
      common,
    );
    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect('/admin/community-profile/new');
    }

    req.session.message = 'Created!';
    req.session.messageType = 'success';

    return res.redirect(
      `/admin/community-profile/type/${profileType.toLowerCase()}s`,
    );
  },
);

router.get(
  '/admin/community-profile/edit/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const result = await getCommunityProfileForm(req, common);
    res.render('community_profile_new', result);
  },
);

router.post(
  '/admin/community-profile/edit/:id',
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([
    { name: 'cardImage', maxCount: 1 },
    { name: 'logoImage', maxCount: 1 },
    { name: 'bannerImage', maxCount: 1 },
  ]),
  async (req, res) => {
    const { errors, data } = await editCommunityProfile(req, common);

    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect(`/admin/community-profile/edit/${req.params.id}`);
    }

    req.session.message = 'Updated!';
    req.session.messageType = 'success';

    return res.redirect(
      `/admin/community-profile/type/${data.profileType.toLowerCase()}s`,
    );
  },
);

router.delete(
  `/admin/community-profile/delete/:id`,
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const { errors, data } = await deleteCommunityProfile(req);
    if (errors.length) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect(`/admin/community-profile`);
    }
    req.session.message = 'Deleted!';
    req.session.messageType = 'success';

    res.end();
  },
);

router.put(
  `/admin/community-profile/edit/update-status/:id`,
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const errors = [];
    const { id } = req.params;
    let { isPublished } = req.body;

    const profile = await db.profiles.findOne({ _id: ObjectId(id) });
    if (!profile) {
      res.status(400).json({ code: 404, message: 'This profile not found' });
    }

    const findPoll = await db.polls.findOne({ profileId: ObjectId(id) });

    if (findPoll && isPublished === 'false') {
      return res.status(400).json({
        message:
          'Brand Profile cannot be unpublished. It is linked to poll & quiz data',
      });
    }

    if (isPublished === 'true') isPublished = true;
    if (isPublished === 'false') isPublished = false;

    const updateProfile = await db.profiles.updateOne(
      { _id: ObjectId(id) },
      { $set: { isPublished } },
      {},
    );

    if (!updateProfile.result.n) {
      return errors.push({ code: 400, message: 'Updated fail' });
    }

    if (errors.length) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect(`/admin/community-profile`);
    }
    req.session.message = 'Updated!';
    req.session.messageType = 'success';
    res.status(200).json({ status: 'ok' });
  },
);

router.put(
  `/admin/community-profile/edit/update-orders/:profileType`,
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const { profileType } = req.params;
    let orders = req.body['orders[]'];
    orders = orders.map((e) => Number(e));
    const listCommunity = await db.profiles
      .aggregate([
        {
          $match: {
            profileType: profileType,
          },
        },
        {
          $project: {
            order: 1,
            _id: 1,
          },
        },
      ])
      .sort({ order: 1 })
      .toArray();
    const listOrders = listCommunity.map((e) => Number(e.order));
    for (let i = 0; i < orders.length; i++) {
      if (orders[i] !== listOrders[i]) {
        const j = listCommunity.findIndex((e) => e.order === orders[i]);
        await db.profiles.findOneAndUpdate(
          { _id: listCommunity[j]._id },
          { $set: { order: listOrders[i] } },
          {},
        );
      }
    }
    res.end();
  },
);

router.get(
  `/admin/community-profile/:id/create-poll`,
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const result = await getPollCreateForm(req, common);
    res.render('community_poll_new', result);
  },
);

router.post(
  `/admin/community-profile/:id/create-poll`,
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([{ name: 'bannerImage', maxCount: 1 }]),
  async (req, res) => {
    const { errors, data, profileType } = await insertProfilePoll(req, common);
    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect(
        `/admin/community-profile/${req.params.id}/create-poll`,
      );
    }

    req.session.message = 'Created!';
    req.session.messageType = 'success';

    return res.redirect(
      `/admin/community-profile/type/${profileType.toLowerCase()}s`,
    );
  },
);

module.exports = router;
