const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const router = express.Router();
const moment = require('moment-timezone');
const Config = require('../../../config/ethereumConfig');

router.get(
  '/admin/campaigns',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    // get the top results
    const [campaigns, totalParticipate, participateCounts] = await Promise.all([
      db.campaigns.find({}).toArray(),
      db.campaignParticipates.distinct('customerId'),
      db.campaignParticipates
        .aggregate([
          { $group: { _id: '$campaign_id', count: { $sum: '$scanCount' } } },
        ])
        .toArray(),
    ]);

    const participateCountsMap = participateCounts.reduce((map, obj) => {
      map[obj._id] = obj.count;
      return map;
    }, {});

    res.render('campaigns', {
      title: 'Campaigns',
      menu: 'campaign',
      results: campaigns,
      totalParticipate: totalParticipate.length,
      participateCountsMap: participateCountsMap,
      session: req.session,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.get(
  '/admin/campaign/filter/:search',
  common.restrict,
  common.checkAccess,
  (req, res, next) => {
    const db = req.app.db;
    const searchTerm = req.params.search;
    const storesIndex = req.app.storesIndex;

    const lunrIdArray = [];
    storesIndex.search(searchTerm).forEach((id) => {
      lunrIdArray.push(common.getId(id.ref));
    });

    // we search on the lunr indexes
    db.stores.find({ _id: { $in: lunrIdArray } }).toArray((err, results) => {
      if (err) {
        console.error(colors.red('Error searching', err));
      }
      res.render('campaigns', {
        title: 'Results',
        menu: 'campaign',
        results: results,
        admin: true,
        config: req.app.config,
        session: req.session,
        searchTerm: searchTerm,
        message: common.clearSessionValue(req.session, 'message'),
        messageType: common.clearSessionValue(req.session, 'messageType'),
        helpers: req.handlebars.helpers,
      });
    });
  },
);

// insert form
router.get(
  '/admin/campaign/new',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('campaign_new', {
      title: 'New Campaign',
      menu: 'campaign',
      session: req.session,
      campaignName: common.clearSessionValue(req.session, 'storeName'),
      isFeatureCollection: common.clearSessionValue(
        req.session,
        'isFeatureCollection',
      ),
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
    });
  },
);

// insert new product form action
router.post(
  '/admin/campaign/insert',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const currentDate = new Date();
    const campaign = {
      name: common.cleanHtml(req.body.name),
      description: common.cleanHtml(req.body.description),
      url: common.cleanHtml(req.body.url),
      // max_number_of_scans: common.cleanHtml(req.body.numberOfScanning),
      start_date: new Date(
        new moment(req.body.startDate + ':00').toISOString(),
      ),
      end_date: new Date(new moment(req.body.endDate + ':00').toISOString()),
      winner: '',
      id: currentDate.getTime(),
      isDisable: req.body.isDisable,
    };

    const scan_count = new moment(req.body.endDate + ':00').diff(
      new moment(req.body.startDate + ':00'),
      'days',
    );
    campaign.max_number_of_scans = scan_count > 0 ? scan_count + 1 : 0;
    await Promise.all([db.campaigns.insert(campaign)]);
    req.session.message = 'Successfully saved';
    req.session.messageType = 'success';
    res.redirect('/admin/campaigns');
  },
);

// render the editor
router.get(
  '/admin/campaign/edit/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    db.campaigns.findOne(
      { _id: common.getId(req.params.id) },
      (err, result) => {
        if (err) {
          console.info(err.stack);
        }
        res.render('campaign_edit', {
          title: 'Edit Campaign',
          menu: 'campaign',
          campaign: result,
          admin: true,
          session: req.session,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          config: req.app.config,
          editor: true,
          helpers: req.handlebars.helpers,
        });
      },
    );
  },
);

router.get(
  '/admin/campaign/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.redirect('/admin/campaigns');
  },
);

// Update an existing product form action
router.post(
  '/admin/campaign/update',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const [campaign] = await Promise.all([
      db.campaigns.findOne({ _id: common.getId(req.body.id) }),
    ]);

    if (campaign) {
      campaign.name = common.cleanHtml(req.body.name);
      campaign.description = common.cleanHtml(req.body.description);
      campaign.url = common.cleanHtml(req.body.url);
      campaign.max_number_of_scans = common.cleanHtml(
        req.body.numberOfScanning,
      );
      campaign.isDisable = common.checkboxBool(req.body.isDisable);
      campaign.start_date = new Date(
        new moment(req.body.startDate + ':00').toISOString(),
      );
      campaign.end_date = new Date(
        new moment(req.body.endDate + ':00').toISOString(),
      );
      campaign.winner = common.cleanHtml(req.body.winner);
      const scan_count = new moment(req.body.endDate + ':00').diff(
        new moment(req.body.startDate + ':00'),
        'days',
      );
      campaign.max_number_of_scans = scan_count > 0 ? scan_count + 1 : 0;
      const [result] = await Promise.all([
        db.campaigns.update(
          { _id: common.getId(req.body.id) },
          { $set: campaign },
          { multi: false },
        ),
      ]);
      req.session.message = 'Successfully saved';
      req.session.messageType = 'success';
      res.redirect('/admin/campaigns');
    } else {
      res.redirect('/admin/campaigns');
    }
  },
);

// delete product
router.get(
  '/admin/campaign/take-winner/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;

    const [campaign] = await Promise.all([
      db.campaigns.findOne({ _id: common.getId(req.params.id) }),
    ]);
    if (campaign) {
      const [count] = await Promise.all([
        db.campaignParticipates
          .aggregate([
            {
              $match: {
                $and: [{ campaign_id: campaign.id }],
              },
            },
            { $group: { _id: '$customerId', count: { $sum: '$scanCount' } } },
            { $sort: { count: -1 } },
          ])
          .limit(10)
          .toArray(),
      ]);
      if (count.length > 0) {
        const maxPeak = count.reduce((p, c) => (p.count > c.count ? p : c));
        const filteredUser = count.filter(
          (item) => item.count === maxPeak.count,
        );
        if (filteredUser.length > 0) {
          const winner =
            filteredUser[Math.floor(Math.random() * filteredUser.length)];
          const [customer] = await Promise.all([
            db.customers.findOne({ customerId: winner._id }),
          ]);
          if (customer) {
            campaign.winner = customer.userName;
            const [result] = await Promise.all([
              db.campaigns.update({ _id: campaign._id }, { $set: campaign }),
            ]);
            req.session.message = 'Got Winner!!!';
            req.session.messageType = 'success';
            res.redirect('/admin/campaign/edit/' + req.params.id);
          } else {
            req.session.message = 'Have not participate member';
            req.session.messageType = 'danger';
            res.redirect('/admin/campaign/edit/' + req.params.id);
          }
        } else {
          req.session.message = 'Have not participate member';
          req.session.messageType = 'danger';
          res.redirect('/admin/campaign/edit/' + req.params.id);
        }
      } else {
        req.session.message = 'Have not participate member';
        req.session.messageType = 'danger';
        res.redirect('/admin/campaign/edit/' + req.params.id);
      }
    } else {
      res.redirect('/admin/campaigns');
    }
  },
);

// delete product
router.get(
  '/admin/campaign/delete/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    // remove the article
    db.campaigns.remove(
      { _id: common.getId(req.params.id) },
      {},
      (err, numRemoved) => {
        if (err) {
          console.info(err.stack);
        }
        req.session.message = 'Campaign successfully deleted';
        req.session.messageType = 'success';
        res.redirect('/admin/campaigns');
      },
    );
  },
);

router.get(
  '/admin/campaign/participates/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const page = req.param('p') ? req.param('p') : 0;
    const [campaign, totalParticipate, participates] = await Promise.all([
      db.campaigns.findOne({ id: parseInt(req.params.id) }),
      db.campaignParticipates.distinct('customerId').length,
      db.campaignParticipates
        .aggregate([
          {
            $lookup: {
              from: 'customers', // other table name
              localField: 'customerId', // name of users table field
              foreignField: 'customerId', // name of userinfo table field
              as: 'customer_info', // alias for userinfo table
            },
          },
          { $unwind: '$customer_info' },
          {
            $match: {
              $and: [{ campaign_id: parseInt(req.params.id) }],
            },
          },
          {
            $project: {
              _id: 1,
              email: '$customer_info.email',
              userName: '$customer_info.userName',
              scanCount: 1,
            },
          },
          {
            $group: { _id: '$userName', count: { $sum: '$scanCount' } },
          },
        ])
        .skip(page * Config.productLitmitPerPage)
        .limit(Config.productLitmitPerPage)
        .sort({ count: -1 })
        .toArray(),
    ]);

    const totalPage = totalParticipate / Config.productLitmitPerPage;
    res.render('participates', {
      title: 'Participates',
      menu: 'campaign',
      campaign: campaign,
      participates: participates,
      admin: true,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      config: req.app.config,
      editor: true,
      helpers: req.handlebars.helpers,
      pagination: {
        page: page, // The current page the user is on
        pageCount: totalPage, // The total number of available pages
      },
    });
  },
);

router.get(
  '/admin/campaign/participate/new/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const [campaign, products] = await Promise.all([
      db.campaigns.findOne({ id: parseInt(req.params.id) }),
      db.products
        .find(
          { productVariantSku: { $exists: true, $ne: '' } },
          {
            projection: {
              productTitle: 1,
              productVariantSku: 1,
            },
          },
        )
        .sort({ productVariantSku: 1 })
        .toArray(),
    ]);
    res.render('participate_new', {
      title: 'Participates',
      menu: 'campaign',
      campaign: campaign,
      products: products,
      minDate: new moment(campaign.start_date).format('YYYY-MM-DD'),
      maxDate: new moment(campaign.end_date).format('YYYY-MM-DD'),
      admin: true,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      config: req.app.config,
      editor: true,
      helpers: req.handlebars.helpers,
    });
  },
);

// insert new product form action
router.post(
  '/admin/campaign/participate/insert',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;

    // find user with userName
    const [customer] = await Promise.all([
      db.customers.findOne({ userNameLower: req.body.userName.toLowerCase() }),
    ]);
    if (!customer) {
      req.session.message = 'User not found';
      req.session.messageType = 'danger';
      return res.redirect(
        '/admin/campaign/participate/new/' + req.body.campaignId,
      );
    } else {
      const currentDate = new Date();
      const newParticipate = {
        campaign_id: parseInt(req.body.campaignId),
        toy_id: req.body.sku,
        customerLocalId: customer._id,
        customerId: customer.customerId,
        scan_date: new moment(req.body.scanDate).format('DD-MM-YYYY'),
        createdAt: moment.now(),
        scanCount: parseInt(req.body.scanCount),
        udid: 'MANUAL_ADDED_' + new Date().getTime().toString(),
      };

      const [result] = await Promise.all([
        db.campaignParticipates.insert(newParticipate),
      ]);
      req.session.message = 'Successfully saved';
      req.session.messageType = 'success';
      res.redirect('/admin/campaign/participates/' + req.body.campaignId);
    }
  },
);

module.exports = router;
