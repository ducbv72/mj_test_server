const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const fs = require('fs');
const path = require('path');
const router = express.Router();
const Config = require('../../../config');
const mime = require('mime-type/with-db');
const multer = require('multer');
const { consoleSandbox } = require('@sentry/node/node_modules/@sentry/utils');
const upload = multer({ dest: 'public/uploads/' });
router.get('/admin/sports',common.restrict,common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    const [sportList] = await Promise.all([
      db.sports.find().sort({sortSport:1}).toArray(),
    ]);
    res.render('sports/sports', {
      title: 'Sports List',
      menu: 'mas',
      session: req.session,
      results: sportList,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);
router.post('/admin/sport/sortSport',common.restrict,common.checkAccess,(req, res) => {
    const db = req.app.db;
    var reqsortSport = req.body.listtempSortSport;
    var doc1 = reqsortSport[0];
    var doc2 = reqsortSport[1];
    db.sports.updateOne(
      { _id: common.getId(doc1._id) },
      { $set: {sortSport: doc1.sortSport }},
      (err, numReplaced) => {
        if (err) {
          console.error(colors.red('Failed to save tag: ' + err));
          req.session.message = 'Failed to save. Please try again';
          req.session.messageType = 'danger';
          res.redirect(
            '/admin/sports',
          );
        } else {
          req.session.message = 'Successfully saved';
          // req.session.messageType = 'success';
          // res.redirect('/admin/sports');
        }
      },
    );
    db.sports.updateOne(
      { _id: common.getId(doc2._id) },
      { $set: {sortSport: doc2.sortSport } },
      (err, numReplaced) => {
        if (err) {
          console.error(colors.red('Failed to save tag: ' + err));
          req.session.message = 'Failed to save. Please try again';
          req.session.messageType = 'danger';
          res.redirect(
            '/admin/sports',
          );
        } else {
          req.session.message = 'Successfully saved';
          req.session.messageType = 'success';
          res.redirect('/admin/sports');
        }
      },
    );
    
});
router.get('/admin/sport/new',common.restrict,common.checkAccess,(req, res) => {
    res.render('sports/sport_new', {
      title: 'Create sport',
      menu: 'mas',
      session: req.session,
      admin: true,
      config: req.app.config,
      helpers: req.handlebars.helpers,
    });
  },
);
// insert new sport form action
router.post('/admin/sport/insert',common.restrict,common.checkAccess,async (req, res) => {
    const db = req.app.db;
    const doc = {
      name: req.body.sportName,
      collections: req.body.lstcollection,
      createdAt: new Date(),
      updatedAt: new Date(),
    };
    db.sports.insert(doc, (err, newDoc) => {
      if (err) {
        req.session.message = 'New sport successfully created';
        req.session.messageType = 'success';
        res.redirect('/admin/sports');
      } else {
        req.session.message = 'New sport successfully created';
        req.session.messageType = 'success';
        res.redirect('/admin/sports');
      }
    });
  },
);
//Api upload file
router.post(
  '/admin/sport/upload-file',
  common.restrict,
  common.checkAccess,
  upload.single('upload_file'),
  async (req, res, next) => {
    const db = req.app.db;
    if (req.file) {
      const file = req.file;
      // Get the mime type of the file
      const mimeType = mime.lookup(file.originalname);

      // Check for allowed mime type and file size
      if (
        !common.allowedMimeType.includes(mimeType) ||
        file.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(file.path);
        res.status(400).json({
          code: 400,
          message: 'File type not allowed or too large. Please try again.',
        });
        return;
      }

      //upload bigger file
      const imageUrlS3 = await common.uploadFileToS3(
        file.path,
        Config.S3.exclusiveImageFolder,
      );
      if (req.body.src && req.body.src.length) {
        await common.deleteS3Object(imageUrlS3, req.body.src);
      }
      fs.unlinkSync(file.path);
      res.status(200).json({
        message: 'Upload image sucessfully',
        data: { imageUrl: imageUrlS3 },
      });
    } else {
      // Redirect to error
      res.status(400).json({
        code: 400,
        message: 'File upload error. Please select a file.',
      });
    }
  },
);
router.get('/admin/sport/delete/:id',common.restrict,common.checkAccess,(req, res) => {
    const db = req.app.db;
    db.sports.remove(
      { _id: common.getId(req.params.id) },
      {},
      (err, numRemoved) => {
        if (err) {
          console.error(colors.red('Failed to delete product: ' + err));
          req.session.message = 'Failed to save. Please try again';
          req.session.messageType = 'danger';
          res.redirect(
            '/admin/sport/edit/' + req.body.id,
          );
        }
        // redirect home
        req.session.message = 'Tag successfully deleted';
        req.session.messageType = 'success';
        res.redirect('/admin/sports');
      },
    );
  },
);
router.get('/admin/sport/edit/:id',common.restrict,common.checkAccess,async (req, res) => {
    const db = req.app.db;
    const result = await db.sports.findOne({
      _id: common.getId(req.params.id),
    });
    res.render('sports/sport_edit', {
      title: 'Edit Sport',
      menu: 'mas',
      result: result,
      session: req.session,
      admin: true,
      config: req.app.config,
      helpers: req.handlebars.helpers,
    });
  },
);
router.post('/admin/sport/update',common.restrict,common.checkAccess,(req, res) => {
    const db = req.app.db;
    const doc = {
      name: req.body.sportName,
      collections: req.body.lstcollection,
      updatedAt: new Date(),
    };
    db.sports.findOne(
      { _id: common.getId(req.body._id) },
      (err, data) => {
        if (err) {
          console.info(err.stack);
          req.session.message = 'Failed updating tag.';
          req.session.messageType = 'danger';
          res.redirect(
            '/admin/sport/edit/' + req.body._id,
          );
          return;
        }
        db.sports.updateOne(
          { _id: common.getId(req.body._id) },
          { $set: doc },
          (err, numReplaced) => {
            if (err) {
              console.error(colors.red('Failed to save tag: ' + err));
              req.session.message = 'Failed to save. Please try again';
              req.session.messageType = 'danger';
              res.redirect(
                '/admin/sport/edit/' + req.body._id,
              );
            } else {
              req.session.message = 'Successfully saved';
              req.session.messageType = 'success';
              res.redirect('/admin/sports');
            }
          },
        );
      },
    );
  },
);
module.exports = router;
