const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const router = express.Router();
const Promise = require('promise');
const Config = require('../../../config/ethereumConfig');
const csrf = require('csurf');
// setup route middlewares
const csrfProtection = csrf({ cookie: true });

router.get(
  '/admin/artist/filter',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const searchTerm = req.query.search;
    const [results, count] = await Promise.all([
      db.artists
        .find({
          $or: [{ artistName: new RegExp(searchTerm, 'i') }],
        })
        .toArray(),
    ]);
    res.render('artist/artists', {
      title: 'Artists',
      searchTerm: searchTerm,
      session: req.session,
      artists: results,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
    });
  },
);

router.get(
  '/admin/artists/:page',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const page = Number(req.params.page);
    const [results, count] = await Promise.all([
      db.artists
        .find({})
        .skip(page * Config.qrcodeLimitPerpage)
        .limit(Config.qrcodeLimitPerpage)
        .toArray(),
      db.artists.count({}),
    ]);
    const total = Math.ceil(count / Config.qrcodeLimitPerpage);
    const array = [];
    for (let i = 0; i < total; i++) {
      array.push(i + '');
    }
    res.render('artist/artists', {
      title: 'Artists',
      session: req.session,
      artists: results,
      total: total,
      page: parseInt(req.params.page),
      totalPages: array,
      count: count,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
      menu: 'artist',
    });
  },
);

// edit form
router.get(
  '/admin/artist/edit/:id',
  csrfProtection,
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    db.artists.findOne({ _id: common.getId(req.params.id) }, (err, result) => {
      if (err) {
        res.redirect('/admin/artists/0');
      }
      const arrayGallery = result.gallery ? result.gallery.join('\n') : '';
      res.render('artist/artist_edit', {
        title: 'Edit Artist',
        session: req.session,
        artist: result,
        arrayGallery: arrayGallery.trim(),
        message: common.clearSessionValue(req.session, 'message'),
        messageType: common.clearSessionValue(req.session, 'messageType'),
        editor: true,
        admin: true,
        helpers: req.handlebars.helpers,
        config: req.app.config,
        csrfToken: req.csrfToken(),
      });
    });
  },
);

// edit form
router.get(
  '/admin/artist/new/',
  csrfProtection,
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    res.render('artist/artist_new', {
      title: 'New Artist',
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
      csrfToken: req.csrfToken(),
      menu: 'artist',
    });
  },
);

// Update an existing product form action
router.post(
  '/admin/artist/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    db.artists.findOne({ _id: common.getId(req.body.id) }, (err, product) => {
      if (err) {
        console.info(err.stack);
        req.session.message = 'Failed updating artist.';
        req.session.messageType = 'danger';
        res.redirect('/admin/artist/edit/' + req.body.id);
        return;
      }
      const gallery = req.body.gallery ? req.body.gallery.split('\n') : '';
      const artistDoc = {
        articleId: common.cleanHtml(req.body.articleId),
        artistName: common.cleanHtml(req.body.artistName),
        artistImage: common.cleanHtml(req.body.artistImage),
        artistTitle: common.cleanHtml(req.body.artistTitle),
        artistDescription: common.cleanHtml(req.body.artistDescription),
        website: common.cleanHtml(req.body.website),
        instagram: common.cleanHtml(req.body.instagram),
        facebook: common.cleanHtml(req.body.facebook),
        twitter: common.cleanHtml(req.body.twitter),
        gallery: gallery,
      };
      db.artists.update(
        { _id: common.getId(req.body.id) },
        { $set: artistDoc },
        {},
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed to save product: ' + err));
            req.session.message = 'Failed to save. Please try again';
            req.session.messageType = 'danger';
            res.redirect('/admin/artist/edit/' + req.body.id);
          } else {
            req.session.message = 'Successfully saved';
            req.session.messageType = 'success';
            res.redirect('/admin/artist/edit/' + req.body.id);
          }
        },
      );
    });
  },
);

router.post(
  '/admin/artist/insert',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const gallery = req.body.gallery ? req.body.gallery.split('\n') : '';
    const artistDoc = {
      articleId: common.cleanHtml(req.body.articleId),
      artistName: common.cleanHtml(req.body.artistName),
      artistImage: common.cleanHtml(req.body.artistImage),
      artistTitle: common.cleanHtml(req.body.artistTitle),
      artistDescription: common.cleanHtml(req.body.artistDescription),
      website: common.cleanHtml(req.body.website),
      instagram: common.cleanHtml(req.body.instagram),
      facebook: common.cleanHtml(req.body.facebook),
      twitter: common.cleanHtml(req.body.twitter),
      gallery: gallery,
    };
    db.artists.insert(artistDoc, (err, newDoc) => {
      if (err) {
        console.error(colors.red('Failed to save product: ' + err));
        req.session.message = 'Failed to save. Please try again';
        req.session.messageType = 'danger';
        res.redirect('/admin/artist/artists/0');
      } else {
        req.session.message = 'Successfully saved';
        req.session.messageType = 'success';
        res.redirect('/admin/artists/0');
      }
    });
  },
);

router.get(
  '/admin/artist/insert',
  csrfProtection,
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.redirect('/admin/artists/0');
  },
);

router.get(
  '/admin/artist/delete/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const id = req.params.id;
    // remove the article
    db.artists.remove(
      { productId: common.getId(id) },
      {},
      (err, numRemoved) => {
        if (err) {
          console.info(err.stack);
          req.session.message = 'something wrong.';
          req.session.messageType = 'error';
          res.redirect('/admin/artists/0');
        }
        req.session.message = 'successfully deleted';
        req.session.messageType = 'success';
        res.redirect('/admin/artists/0');
      },
    );
  },
);

module.exports = router;
