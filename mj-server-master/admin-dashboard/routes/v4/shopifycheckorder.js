const express = require('express');
const router = express.Router();
const Promise = require('promise');
const { shopify } = require('../../../config/shopify');

router.post('/checkshopifyorder', async (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );

  const order_number = req.body.order_number;
  const email = req.body.email;
  let orderInfo = await Promise.all([
    shopify.order
      .list({ limit: 1, name: order_number, email: email })
      .then((orders) => {
        if (orders.length == 0) {
          res.status(200).json({
            msg: 'ok',
            code: 200,
            data: [],
          });
        } else {
          const order = orders[0];
          // var order = result[0];
          const orderDetail = {
            productName: order.line_items[0].name,
            orderNumber: order.name,
            email: order.email,
            orderPlaced: order.created_at,
            lastUpdated: order.updated_at,
            payment: order.financial_status,
            shipTo: order.shipping_address.country_code,
          };

          const shippingStatus = {};

          const billing = {
            name: order.billing_address.name,
            address: order.billing_address.address1,
            phoneNo: order.billing_address.phone,
          };

          const address = {
            name: order.billing_address.name,
            address: order.billing_address.address1,
            phoneNo: order.billing_address.phone,
          };

          const lineItems = [];

          const orderPayment = {
            lineItems: lineItems,
            subtotal_price: order.subtotal_price,
            total_price: order.total_price,
            shipping_price: order.total_shipping_price_set.presentment_money,
          };

          const data = {
            orderDetail: orderDetail,
            shippingStatus: shippingStatus,
            billing: billing,
            address: address,
            orderPayment: orderPayment,
          };

          return {
            data: data,
            order: order,
          };
        }
        // return orders;
      })
      .catch((error) => {
        console.log(error);
        res.status(500).json({
          msg: 'fail',
          code: 500,
          data: error,
        });
      }),
  ]);

  const db = req.app.db;
  orderInfo = orderInfo[0];
  if (orderInfo) {
    const items = orderInfo.order.line_items;
    for (let i = 0; i < items.length; i++) {
      const line = items[i];
      let productDb = await Promise.all([
        db.products.findOne({
          productVariantSku: line.sku,
        }),
      ]);
      productDb = productDb && productDb[0] ? productDb[0] : null;
      const item = {
        product: line.name,
        productUrl:
          productDb != null && productDb.zendeskUrl
            ? productDb.zendeskUrl
            : '#',
        price: parseFloat(line.price) - parseFloat(line.total_discount),
        quantity: line.quantity,
        total:
          (parseFloat(line.price) - parseFloat(line.total_discount)) *
          line.quantity,
      };
      orderInfo.data.orderPayment.lineItems.push(item);
    }
    res.status(200).json({
      msg: 'ok',
      code: 200,
      data: orderInfo.data,
    });
  }

  // const options = {
  //   url:
  //     'https://sosapp.sinelabs.com/api/orders/lookup/?callback=jQuery111009339838356295675_1610685075889&sos_sid=7010143&sos_oid=56874&sos_eml=rpbernabeiii@gmail.com',
  //   method: 'GET',
  // };
  // request(options, (error, response, body) => {
  //   if (!error && response.statusCode === 200) {
  //     console.log(body);
  //     //var data = body.replace()
  //     res.json(body);
  //   } else {
  //     res.json(error);
  //   }
  //});
});

module.exports = router;
