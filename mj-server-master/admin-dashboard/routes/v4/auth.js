const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const getUserFragment = require('../../../graphql/getCustomer').getUserFragment;
const Config = require('../../../config/settings.json');
const fetch = require('node-fetch');
const apiVersion = '2020-04';
const { MJStoreFrontAccessToken } = require('../../../config/shopify');

router.post('/auth/login', async (req, res) => {
  try {
    if (!req.body.token) {
      return res.status(403).json({ error: 'No token sent.' });
    }
    const db = req.app.db;
    const customerAccessToken = req.body.token;

    let config = req.app.config;
    if (process.env.NODE_ENV === 'test') {
      config = Config;
    }
    // Fetch customer info from Shopify GraphQL
    const shopifyResponse = await fetch(
      `${config.endPoint}api/${apiVersion}/graphql.json`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'X-Shopify-Storefront-Access-Token': MJStoreFrontAccessToken,
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query: `
          query{
            customer(customerAccessToken: "${customerAccessToken}"){
              ${getUserFragment()}
            }
            }
            `,
        }),
      },
    );

    const shopifyData = await shopifyResponse.json();

    if (!shopifyData.data.customer || !shopifyData.data.customer.email) {
      return res.status(403).json({ error: 'No email found.' });
    }
    const customer = shopifyData.data.customer;
    const buff = Buffer.from(customer.id, 'base64');
    const customer_url = buff.toString('ascii');
    const customerId = customer_url.split('/')[4];
    const [checkCustomer] = await Promise.all([
      db.customers.findOne({
        customerId: customerId.toString(),
      }),
    ]).catch((err) => console.log(err));

    if (!checkCustomer) {
      const newCustomer = {
        email: customer.email,
        firstName: customer.firstName,
        lastName: customer.lastName,
        customerId: customerId.toString(),
        userName: null,
        userNameLower: null,
        address1: '',
        address2: '',
        country: '',
        state: '',
        postcode: '',
        phone: customer.phone,
        password: bcrypt.hashSync(customer.email, 10),
        created: new Date(),
        feedPrivate: false,
        vaultPrivate: false,
      };
      const [result] = await Promise.all([db.customers.insert(newCustomer)]);
    }

    // Generate and return JWT token with email payload

    const token = jwt.sign(
      {
        email: shopifyData.data.customer.email,
        customerId: customerId.toString(),
        shopifyToken: customerAccessToken,
      },
      config.jwtSecret,
    );
    return res.status(200).json({ token: token });
  } catch (err) {
    return res.status(500).json({
      msg: 'Something went wrong',
      code: 500,
      data: err.message,
    });
  }
});

router.post('/auth/register', async (req, res) => {
  try {
    if (!req.body.token) {
      return res.status(400).json({ error: 'No token sent.' });
    }

    const db = req.app.db;
    const customerAccessToken = req.body.token;
    let config = req.app.config;
    if (process.env.NODE_ENV === 'test') {
      config = Config;
    }
    // Fetch customer info from Shopify GraphQL
    const shopifyResponse = await fetch(
      `${config.endPoint}api/${apiVersion}/graphql.json`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'X-Shopify-Storefront-Access-Token': MJStoreFrontAccessToken,
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query: `
            query{
                customer(customerAccessToken: "${customerAccessToken}"){
                    ${getUserFragment()}
                }
            }
            `,
        }),
      },
    );

    const shopifyData = await shopifyResponse.json();

    if (!shopifyData.data.customer.email) {
      return res.status(403).json({ error: 'No email found.' });
    }

    const customer = shopifyData.data.customer;
    const user_name = req.body.username ? req.body.username : '';
    const user_name_lower = req.body.username
      ? req.body.username.toLowerCase()
      : '';
    const buff = new Buffer(customer.id, 'base64');
    const customer_url = buff.toString('ascii');
    const customerId = customer_url.split('/')[4];

    const [checkCustomer] = await Promise.all([
      db.customers.findOne({
        $or: [
          { userNameLower: user_name_lower },
          { customerId: customerId.toString() },
        ],
      }),
    ]);

    if (checkCustomer) {
      return res.status(400).json({
        msg: 'Customer Exist',
        code: 400,
      });
    }

    const doc = {
      email: customer.email,
      firstName: customer.firstName,
      lastName: customer.lastName,
      address1: customer.address1,
      address2: customer.address2,
      country: customer.country,
      state: customer.state,
      postcode: customer.postcode,
      phone: customer.phone,
      password: bcrypt.hashSync(customer.email, 10),
      userName: user_name,
      userNameLower: user_name_lower,
      customerId: customerId.toString(),
      created: new Date(),
      feedPrivate: false,
      vaultPrivate: false,
    };
    // email is ok to be used.
    db.customers.insertOne(doc, (err) => {
      if (err) {
        throw new Error(err);
      }
      // Customer creation successful
      const token = jwt.sign(
        {
          email: customer.email,
          customerId: customerId.toString(),
          shopifyToken: customerAccessToken,
        },
        config.jwtSecret,
      );
      return res.status(200).json({ token });
    });
  } catch (err) {
    return res.status(500).json({
      msg: 'Something went wrong',
      code: 500,
      data: null,
    });
  }
});

/**
 * @swagger
 * /api/v4/generate-testing-token/{email}:
 *  get:
 *    tags:
 *      - Testing
 *    description: Generate token for endpoint testing purposes
 *    parameters:
 *    - in: path
 *      name: email
 *      schema:
 *        type: string
 *      example: 'dovu@mightyjaxx.com'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Not a valid email address
 */
router.get('/api/v4/generate-testing-token/:email', async (req, res) => {
  if (process.env.IGNORE_TOKEN !== 'true') {
    res.status(400).json({
      message: 'user not found',
      data: [],
    });
  }
  const config = req.app.config;
  const db = req.app.db;
  const regex = new RegExp(['^', req.params.email, '$'].join(''), 'i');
  const [user] = await Promise.all([
    db.customers.find({ email: regex }).toArray(),
  ]);
  if (user.length == 1) {
    const token = jwt.sign(
      {
        email: user[0].email,
        customerId: user[0].customerId,
        shopifyToken: '',
      },
      config.jwtSecret,
    );
    res.status(200).json({
      message: 'ok',
      token: token,
    });
  } else {
    res.status(400).json({
      message: 'user not found',
      data: [],
    });
  }
});

module.exports = router;
