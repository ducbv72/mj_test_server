/* eslint-disable no-trailing-spaces */
const express = require('express');
const router = express.Router();

// These is the customer facing routes
// router.get('/payment/:orderId', async (req, res, next) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });

// router.get('/checkout', async (req, res, next) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });

// router.get('/pay', async (req, res, next) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });

// router.get('/cartPartial', (req, res) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });

// show an individual product
router.get('/product/:id', (req, res) => {
  if (!req.session.user) {
    res.redirect('admin/login');
    // return;
  } else {
    res.redirect('admin/products');
    // return;
  }
});

// Updates a single product quantity
// router.post('/product/updatecart', (req, res, next) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });
//
// // Remove single product from cart
// router.post('/product/removefromcart', (req, res, next) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });
//
// // Totally empty the cart
// router.post('/product/emptycart', (req, res, next) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });

// Add item to cart
// router.post('/product/addtocart', (req, res, next) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });
//
// // search products
// router.get('/search/:searchTerm/:pageNum?', (req, res) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });
//
// // search products
// router.get('/category/:cat/:pageNum?', (req, res) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });
//
// // return sitemap
// router.get('/sitemap.xml', (req, res, next) => {
//     if(!req.session.user){
//         res.redirect('admin/login');
//         return;
//     }else{
//         res.redirect('admin/products');
//         return;
//     }
// });

router.get('/page/:pageNum', (req, res, next) => {
  if (!req.session.user) {
    res.redirect('admin/login');
    // return;
  } else {
    res.redirect('admin/products');
    // return;
  }
});

// The main entry point of the shop
router.get('/:page?', (req, res, next) => {
  if (!req.session.user) {
    res.redirect('admin/login');
    // return;
  } else {
    res.redirect('admin/products');
    // return;
  }
});

module.exports = router;
