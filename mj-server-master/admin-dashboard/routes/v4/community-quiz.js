const express = require('express');
const common = require('../../../lib/common');
const fs = require('fs');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const dayjs = require('dayjs');
const xl = require('excel4node');

const {
  getCommunityQuizForm,
  insertProfileQuiz,
  listQuiz,
  deleteCommunityQuiz,
  createQuizFromProfile,
  getUpdateQuizForm,
  updateCommunityQuiz,
} = require('../../../services/community-profile.service');
const { ObjectId } = require('mongodb');

const storageProfileQuiz = multer.diskStorage({
  destination: function (req, file, cb) {
    const dir = 'public/uploads/community-quiz/';

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    cb(null, 'public/uploads/community-quiz/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const listUploadImage = multer({ storage: storageProfileQuiz });

// insert form
router.get(
  '/admin/community-quiz/new',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const result = await getCommunityQuizForm(req, common);
    res.render('community_quiz_new', result);
  },
);

router.get(
  '/admin/community-profile/:profileId/create-quiz',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const result = await createQuizFromProfile(req, common);
    res.render('community_quiz_new', result);
  },
);

router.get(
  '/admin/community-quiz/new/getSKUs',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    try {
      const db = req.app.db;
      const search = req.query.search;
      const regex = new RegExp(search, 'i');
      const query = [
        { $project: { productVariantSku: 1 } },
        { $sort: { productVariantSku: -1 } },
        { $limit: 20 },
      ];
      if (search && search.length) {
        query.unshift({
          $match: {
            productVariantSku: {
              $regex: regex,
            },
          },
        });
      }
      const result = await db.products.aggregate(query).toArray();
      res.status(200).json(result);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },
);

router.get(
  '/admin/community-quiz/new/getProfileName',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    try {
      const db = req.app.db;
      const search = req.query.search;
      const regex = new RegExp(search, 'i');
      const query = [
        { $project: { profileName: 1, isPublished: 1 } },
        { $sort: { profileName: -1 } },
        { $limit: 20 },
      ];
      if (search && search.length) {
        query.unshift({
          $match: {
            profileName: {
              $regex: regex,
            },
          },
        });
      }
      const result = await db.profiles.aggregate(query).toArray();
      res.status(200).json(result);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },
);

router.post(
  '/admin/community-profile/create-quiz',
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([{ name: 'bannerImage', maxCount: 1 }]),
  async (req, res) => {
    const { errors, data } = await insertProfileQuiz(req, common);
    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect('/admin/community-profile/create-quiz');
    }

    req.session.message = 'Created!';
    req.session.messageType = 'success';

    return res.redirect(`/admin/community-profile/list-quizs`);
  },
);

router.get(
  '/admin/community-profile/list-quizs',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const { results } = await listQuiz(req);
    res.render('community_quiz_list', {
      menu: 'quiz',
      parentMenu: 'community-profile',
      results: results,
      session: req.session,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.get(
  '/admin/community-quiz/update/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const { result } = await getUpdateQuizForm(req, common);
    res.render('community_quiz_new', result);
  },
);

router.post(
  '/admin/community-quiz/update/:id',
  common.restrict,
  common.checkAccess,
  listUploadImage.fields([{ name: 'bannerImage', maxCount: 1 }]),
  async (req, res) => {
    const { errors, data } = await updateCommunityQuiz(req, common);
    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.redirect(`/admin/community-quiz/update/${req.params.id}`);
    }

    req.session.message = 'Updated!';
    req.session.messageType = 'success';

    res.redirect(`/admin/community-profile/list-quizs`);
  },
);

router.put(
  '/admin/community-quiz/update-status/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const query = {};
    const getQuizs = await db.quizs.findOne({ _id: ObjectId(req.params.id) });
    const quizExpiresOn = new Date(getQuizs.quizExpiresOn).getTime();
    const currentTime = new Date().getTime();
    query.isDraft = true;
    // if (getQuizs.ispublished && quizExpiresOn < currentTime) {
    //   query.isDraft = true;
    // } else {
    //   query.isDraft = true;
    //   query.ispublished = false;
    // }

    const updateStatus = await db.quizs.updateOne(
      { _id: ObjectId(req.params.id) },
      { $set: query },
    );
    if (!updateStatus.result.n) {
      return res.status(400).json({ message: 'Update status failed' });
    }
    res.status(200).json({ message: 'Updated!' });
  },
);

router.delete(
  '/admin/community-quiz/delete/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const { errors, data } = await deleteCommunityQuiz(req);
    if (errors.length || !data) {
      req.session.message = errors.map((e) => e.message).join(`\n`);
      req.session.messageType = 'danger';
      return res.end();
    }

    req.session.message = 'Deleted!';
    req.session.messageType = 'success';

    res.end();
  },
);

router.get(
  '/admin/community-quiz/export/:quiz',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    try {
      const db = req.app.db;
      const quizId = ObjectId(req.params.quiz);

      const [listQuestions, quiz] = await Promise.all([
        db.quizQuestions
          .aggregate([
            {
              $match: {
                quizId,
              },
            },
            {
              $unwind: '$quizAnswers',
            },
            {
              $lookup: {
                from: 'customers',
                let: { id: '$userId' },
                pipeline: [
                  { $match: { $expr: { $eq: ['$_id', '$$id'] } } },
                  { $project: { email: 1, userName: 1 } },
                ],
                as: 'user_detail',
              },
            },
            {
              $sort: { createdAt: 1 },
            },
          ])
          .toArray(),
        db.quizs.findOne({ _id: quizId }),
      ]);

      const getBrand = await db.profiles.findOne({ _id: quiz.profileId });

      if (!quiz || (quiz.isDraft && !quiz.ispublished)) {
        return res.status(400).json({
          message: 'Sorry, this quiz is currently unavailable',
          data: null,
        });
      }

      const quizStartOn = dayjs(quiz.quizStartOn)
        .format('DD/MM/YYYY-hh:mm A')
        .replace('-', ' ');
      const quizExpiresOn = dayjs(quiz.quizExpiresOn)
        .format('DD/MM/YYYY-hh:mm A')
        .replace('-', ' ');

      const getQuestion = quiz.questions.map((e) => {
        return {
          _id: e._id,
          questionNumber: e.questionNumber,
          questionName: `${e.questionNumber}. ${e.question}`,
          options: e.options,
          correctAnswer: e.options.find((o) => o.is_correct).key,
        };
      });

      const resultData = [];
      listQuestions.forEach((e, i) => {
        const qIndex = e.quizAnswers.questionNumber - 1;
        const response = getQuestion[qIndex].options.find(
          (o) => o.key === e.quizAnswers.nameOfAnswer,
        );
        const question = getQuestion[qIndex].questionName;

        return resultData.push({
          userEmail: e.user_detail[0].email,
          response: response.name,
          dateTime: dayjs(e.createdAt)
            .format('DD/MM/YYYY-hh:mm A')
            .replace('-', ' '),
          question,
        });
      });

      const trueAnswer = quiz.questions.map((e) => {
        return {
          questionNumber: e.questionNumber,
          correctAnswer: e.options.find((o) => o.is_correct).key,
        };
      });

      const overallResultsTally = [];

      const totalQuestionCount = listQuestions.filter(
        (q) => q.quizAnswers.questionNumber === 1,
      ).length;
      const totalQuestion = trueAnswer.length;
      for (let i = 0; i < totalQuestion; i++) {
        const findQuestion = getQuestion.find(
          (e) => e.questionNumber === trueAnswer[i].questionNumber,
        );
        const findCorectAnswerCount = listQuestions.filter(
          (q) =>
            q.quizAnswers.nameOfAnswer === trueAnswer[i].correctAnswer &&
            q.quizAnswers.questionNumber === trueAnswer[i].questionNumber,
        ).length;
        overallResultsTally.push({
          resultsTally: findQuestion.questionName,
          count: findCorectAnswerCount,
          rate: totalQuestionCount
            ? `${Math.round(
                (findCorectAnswerCount / totalQuestionCount) * 100,
              )}%`
            : '0%',
          rawRate: `${findCorectAnswerCount}/${totalQuestionCount}`,
        });
      }

      // Create a new instance of a Workbook class
      const wb = new xl.Workbook();

      const options = {
        pageSetup: {
          orientation: 'landscape',
          scale: 80,
        },
      };

      // Add Worksheets to the workbook
      const ws = wb.addWorksheet('Sheet 1', options);

      const font = 'Arial';

      // Create a reusable style
      const style = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          name: font,
        },

        alignment: {
          wrapText: true,
          vertical: 'top',
        },
      });

      const columnTitleStyle = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          bold: true,
          name: font,
        },

        alignment: {
          wrapText: true,
          vertical: 'center',
          horizontal: 'center',
        },
      });

      const columnStyleCenter = wb.createStyle({
        font: {
          color: '#000000',
          size: 10,
          name: font,
        },

        alignment: {
          wrapText: true,
          horizontal: 'center',
          vertical: 'center',
        },
      });

      const startRow = 2;
      const resultDataLength = resultData.length;
      const tab = 'CHAR(32)&CHAR(32)&';
      const space = 'CHAR(32)&';

      ws.column(1).setWidth(18);
      ws.column(2).setWidth(35);
      ws.column(3).setWidth(20);
      ws.column(4).setWidth(18);
      ws.column(6).setWidth(18);
      ws.column(7).setWidth(18);

      // COLUMN TITLE
      ws.cell(startRow - 1, 1)
        .string('User Email')
        .style(columnTitleStyle);
      ws.cell(startRow - 1, 2)
        .string('Question')
        .style(columnTitleStyle);
      ws.cell(startRow - 1, 3)
        .string('Response')
        .style(columnTitleStyle);
      ws.cell(startRow - 1, 4)
        .string('DateTime')
        .style(columnTitleStyle);

      for (let i = 0; i < resultDataLength; i++) {
        // FULLNAME - FIRST COLUMN
        ws.cell(startRow + i, 1)
          .string(resultData[i].userEmail)
          .style(style);

        // SECOND COLUMN
        ws.cell(startRow + i, 2)
          .string(resultData[i].question)
          .style(style);

        // THIRD COLUMN
        ws.cell(startRow + i, 3)
          .string(resultData[i].response)
          .style(columnStyleCenter);

        // FOURTH COLUMN
        ws.cell(startRow + i, 4)
          .string(resultData[i].dateTime)
          .style(columnStyleCenter);
      }

      const endOfResult = startRow + resultDataLength - 1;

      ws.cell(1, 1, endOfResult, 4).style({
        border: {
          top: {
            style: 'thin',
          },
          right: {
            style: 'thin',
          },
          bottom: {
            style: 'thin',
          },
          left: {
            style: 'thin',
          },
        },
      });

      ws.cell(startRow - 1, 1, startRow - 1, 4).style({
        border: {
          top: {
            style: 'thick',
          },
          right: {
            style: 'thick',
          },
          bottom: {
            style: 'thick',
          },
          left: {
            style: 'thick',
          },
        },
      });

      ws.cell(startRow - 1, 1, startRow - 1, 4).style({
        border: {
          top: {
            style: 'thick',
          },
          right: {
            style: 'thick',
          },
          bottom: {
            style: 'thick',
          },
          left: {
            style: 'thick',
          },
        },
      });

      ws.cell(endOfResult, 1, endOfResult, 4).style({
        border: {
          bottom: {
            style: 'thick',
          },
        },
      });

      ws.cell(1, 4, endOfResult, 4).style({
        border: {
          right: {
            style: 'thick',
          },
        },
      });

      ws.cell(1, 1, endOfResult, 1).style({
        border: {
          left: {
            style: 'thick',
          },
        },
      });

      const newRow = startRow + resultDataLength + 4;

      const resultLenght = overallResultsTally.length;

      // COLUMN TITLE
      ws.cell(newRow - 1, 2)
        .string('Results Tally')
        .style(columnTitleStyle);
      ws.cell(newRow - 1, 3)
        .string('No. of correct answers/ No. of respondents')
        .style(columnTitleStyle);
      ws.cell(newRow - 1, 4)
        .string('% of correct answers')
        .style(columnTitleStyle);

      for (let i = 0; i < resultLenght; i++) {
        // FULLNAME - FIRST COLUMN
        ws.cell(newRow + i, 2)
          .string(overallResultsTally[i].resultsTally)
          .style(style);

        // SECOND COLUMN
        ws.cell(newRow + i, 3)
          .string(overallResultsTally[i].rawRate)
          .style(columnStyleCenter);

        // THIRD COLUMN
        ws.cell(newRow + i, 4)
          .string(overallResultsTally[i].rate)
          .style(columnStyleCenter);
      }

      ws.cell(newRow - 1, 2, resultLenght + newRow - 1, 4).style({
        border: {
          top: {
            style: 'thin',
          },
          right: {
            style: 'thin',
          },
          bottom: {
            style: 'thin',
          },
          left: {
            style: 'thin',
          },
        },
      });

      ws.cell(1, 6, 2, 7).style({
        border: {
          top: {
            style: 'thick',
          },
          right: {
            style: 'thick',
          },
          bottom: {
            style: 'thick',
          },
          left: {
            style: 'thick',
          },
        },
      });

      ws.cell(1, 6).string('Quiz Starts On').style(columnTitleStyle);
      ws.cell(2, 6).string('Quiz Expires On').style(columnTitleStyle);
      ws.cell(1, 7).string(quizStartOn).style(columnStyleCenter);
      ws.cell(2, 7).string(quizExpiresOn).style(columnStyleCenter);

      const exportFileName = `${dayjs().format('YYYY.MM.DD')}_${
        getBrand.profileName
      }_${quiz.title}.xlsx`;
      await wb.write(exportFileName, res);
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  },
);
module.exports = router;
