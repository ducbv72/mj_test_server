const express = require('express');
const colors = require('colors');
const router = express.Router();
const Promise = require('promise');
const dayjs = require('dayjs');
const { shopify } = require('../../../config/shopify');
// setup route middlewares
const tradegecko = require('./tradegecko-api');

router.post('/webhooks', async (req, res) => {
  // Call your action on the request here
  const payload = req.body;

  const processItem = async () => {
    const [stock] = await Promise.all([
      tradegecko
        .sync_product_stock(payload.resource_url)
        .then(async function (data) {
          const variant = JSON.parse(data).variant;
          let stock_on_hand = parseInt(variant.stock_on_hand);
          // eslint-disable-next-line use-isnan
          if (isNaN(stock_on_hand)) {
            stock_on_hand = 0;
          }

          let logDocs = {
            object_id: payload.object_id,
            event: payload.event,
            createdDate: new Date(),
            resource_url: payload.resource_url,
            variant_id: variant.id,
            product_id: variant.product_id,
            variant_sku: variant.sku,
          };

          if (payload.event === 'variant.stock_level_update') {
            const location = variant.locations.find(
              (v) => v.location_id === 55730,
            );

            if (location) {
              // const socket_emit = await socketEmit(variant, location);

              // if (socket_emit) {
              //   let productSoldOutDate = dayjs()
              //     .subtract(1, 'day')
              //     .format('YYYY-MM-DD');
              //   let productSoldOutTime = dayjs()
              //     .subtract(1, 'day')
              //     .format('HH:mm:ss');
              //
              //   if (!socket_emit.is_sold_out) {
              //     productSoldOutDate = dayjs()
              //       .add(100, 'year')
              //       .format('YYYY-MM-DD');
              //     productSoldOutTime = dayjs()
              //       .add(100, 'year')
              //       .format('HH:mm:ss');
              //   }
              //
              //   const productUpdateData = {
              //     productSoldOutDate,
              //     productSoldOutTime,
              //     productSoldOutStatus: String(socket_emit.is_sold_out),
              //   };
              //
              //   await updateProductDb(req, variant.sku, productUpdateData);
              //
              //   logDocs = {
              //     ...logDocs,
              //     socket_emit,
              //   };
              // }
            }
          }

          await addWebhookLog(req, logDocs);
          await verifySku(req, variant.sku, stock_on_hand);
        })
        .catch((error) => {
          console.error(colors.red('Error: ', error));
        }),
    ]);
  };

  await processItem();

  res.status(200).end(); // Responding is important
});

const socketEmit = async (tradegecko_variant, tradegecko_location) => {
  const query = `
    {
      productVariants(first:1, query:"sku:${tradegecko_variant.sku}") {
        edges {
          node {
            id
            inventoryItem {
              id
              inventoryLevels(first: 1) {
                edges {
                  node {
                    available
                    location {
                      id
                    }
                  }
                }
              }
            }
            product {
              id
            }
          }
        }
      }
    }
  `;

  let socket_emit_data;

  await shopify.graphql(query).then(async (product_variant) => {
    if (product_variant.productVariants.edges.length) {
      const variant =
        product_variant.productVariants.edges[0].node.id.split('/');
      const variant_id = variant[variant.length - 1];
      const product =
        product_variant.productVariants.edges[0].node.product.id.split('/');
      const product_id = product[product.length - 1];
      const inventory_item =
        product_variant.productVariants.edges[0].node.inventoryItem.id.split(
          '/',
        );
      const inventory_item_id = inventory_item[inventory_item.length - 1];
      const location =
        product_variant.productVariants.edges[0].node.inventoryItem.inventoryLevels.edges[0].node.location.id.split(
          '/',
        );
      const shopify_available = Number(
        product_variant.productVariants.edges[0].node.inventoryItem
          .inventoryLevels.edges[0].node.available,
      );
      const location_id = location[location.length - 1];

      if (
        tradegecko_location &&
        tradegecko_location.stock_on_hand > 0 &&
        tradegecko_location.committed >= 0 &&
        tradegecko_location.available <= 0 &&
        !tradegecko_variant.keep_selling
      ) {
        // Update product available in shopify
        await shopify.inventoryLevel.set({
          location_id,
          inventory_item_id,
          available: 0,
        });

        socket_emit_data = {
          product_id,
          variant_id,
          is_sold_out: true,
          available: tradegecko_location.available,
        };
      }

      if (
        tradegecko_location &&
        tradegecko_location.available > 0 &&
        shopify_available <= 0
      ) {
        // Update product available in shopify
        await shopify.inventoryLevel.set({
          location_id,
          inventory_item_id,
          available: tradegecko_location.available,
        });

        socket_emit_data = {
          product_id,
          variant_id,
          is_sold_out: false,
          available: tradegecko_location.available,
        };
      }

      if (global.socket && socket_emit_data) {
        global.socket.emit('product_update', socket_emit_data);
      }
    }
  });
  // .catch((err) => console.error(err));

  return socket_emit_data;
};

// Get product by product variant sku
const getProductDbBySku = async (req, sku) => {
  const db = req.app.db;
  const [product] = await Promise.all([
    db.products.findOne({ productVariantSku: sku }),
  ]);

  return product;
};

// Update product soldout datetime by product variant sku
const updateProductDb = async (req, sku, productData) => {
  const db = req.app.db;

  const product = await getProductDbBySku(req, sku);

  if (!product) {
    return;
  }

  await Promise.all([
    db.products.updateOne(
      {
        productVariantSku: sku,
      },
      {
        $set: productData,
      },
    ),
  ]);
};

const addWebhookLog = async (req, logDocs) => {
  const db = req.app.db;
  await new Promise((resolve) => {
    db.tradegeckoWebhookLog.insertOne(logDocs, (err, newDoc) => {
      if (err) {
        console.error(colors.red('Failed to save product: ' + err));
        req.session.message = 'Failed to save. Please try again';
        req.session.messageType = 'danger';
      } else {
        req.session.message = 'Successfully saved';
        req.session.messageType = 'success';
      }

      resolve();
    });
  });
};

const verifySku = async (req, sku, stock_on_hand) => {
  const db = req.app.db;
  await new Promise((resolve) => {
    db.products.findOne({ productVariantSku: sku }, (err, product) => {
      if (err) {
        console.info(err.stack);
        req.session.message = 'Failed updating product.';
        req.session.messageType = 'danger';
        return;
      }

      const productDoc = {
        stock_on_hand: stock_on_hand,
      };

      db.products.updateOne(
        { productVariantSku: sku },
        { $set: productDoc },
        {},
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed to save product: ' + err));
            req.session.message = 'Failed to save. Please try again';
            req.session.messageType = 'danger';
          }

          resolve();
        },
      );
    });
  });
};

module.exports = router;
