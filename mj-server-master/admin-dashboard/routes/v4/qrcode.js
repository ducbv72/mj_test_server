const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const fs = require('fs');
const router = express.Router();
const Promise = require('promise');
const ethereumConfig = require('../../../config/ethereumConfig');
const Config = require('../../../config');
const randomstring = require('randomstring');
const csrf = require('csurf');
// setup route middlewares
const csrfProtection = csrf({ cookie: true });
const json2csv = require('json2csv').parse;
const csvParser = require('csv-parser');
const formidable = require('formidable');
const crypto = require('crypto');
const Buffer = require('buffer').Buffer;
const XLSX = require('xlsx');
const mime = require('mime-type/with-db');
const encryptedString = require('../../../services/encryptedString');
const multer = require('multer');
const { FORMERR } = require('dns');
const upload = multer({ dest: 'public/uploads/' });
const csvToJson = require('csvtojson');
const { stripIgnoredCharacters } = require('graphql');
const { string } = require('hardhat/internal/core/params/argumentTypes');
const { Code } = require('bson');
const schedule = require('node-schedule');

router.get(
  '/admin/qrcode/filter/:search',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const searchTerm = req.params.search;
    const productsIndex = req.app.productsIndex;

    const lunrIdArray = [];
    productsIndex.search(searchTerm).forEach((id) => {
      lunrIdArray.push(common.getId(id.ref));
    });

    // we search on the lunr indexes
    db.qrcodes.find({ _id: { $in: lunrIdArray } }).toArray((err, results) => {
      if (err) {
        console.error(colors.red('Error searching', err));
      }
      res.render('products', {
        title: 'Results',
        menu: 'product',
        results: results,
        admin: true,
        config: req.app.config,
        session: req.session,
        searchTerm: searchTerm,
        message: common.clearSessionValue(req.session, 'message'),
        messageType: common.clearSessionValue(req.session, 'messageType'),
        helpers: req.handlebars.helpers,
      });
    });
  },
);

// insert form
router.get(
  '/admin/qrcode/new/:id',
  csrfProtection,
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    db.products.findOne({ _id: common.getId(req.params.id) }, (err, result) => {
      if (err) {
        res.redirect('/admin/products/0');
      }
      res.render('qrcode_new', {
        title: 'New Qrcode',
        menu: 'product',
        session: req.session,
        product: result,
        productTitle: common.clearSessionValue(req.session, 'productTitle'),
        productVariantSku: common.clearSessionValue(
          req.session,
          'productVariantSku',
        ),
        productId: common.clearSessionValue(req.session, 'productId'),
        numberOfItem: common.clearSessionValue(req.session, 'numberOfItem'),
        message: common.clearSessionValue(req.session, 'message'),
        messageType: common.clearSessionValue(req.session, 'messageType'),
        editor: true,
        admin: true,
        helpers: req.handlebars.helpers,
        config: req.app.config,
        csrfToken: req.csrfToken(),
      });
    });
  },
);

router.get(
  '/admin/qrcode/view/:id/:page',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const page = Number(req.params.page);
    db.products.findOne(
      { _id: common.getId(req.params.id) },
      async (err, product) => {
        if (err) {
          res.redirect('/admin/products/0');
        }

        const [results, count, countUdid] = await Promise.all([
          db.qrcodes
            .find(
              { productId: common.getId(req.params.id) },
              {
                projection: {
                  encrytedMessage: 1,
                  udid: 1,
                  password: 1,
                },
              },
            )
            .skip(page * ethereumConfig.qrcodeLimitPerpage)
            .limit(ethereumConfig.qrcodeLimitPerpage)
            .toArray(),
          db.qrcodes.count({ productId: common.getId(req.params.id) }),
          db.qrcodes.count({
            $and: [
              { productId: common.getId(req.params.id) },
              { udid: { $ne: '' } },
              { udid: { $ne: null } },
            ],
          }),
        ]);
        const total = count / ethereumConfig.qrcodeLimitPerpage;
        const array = [];
        for (let i = 0; i < total; i++) {
          array.push(i + '');
        }

        res.render('qrcodes', {
          title: 'Qrcodes',
          menu: 'product',
          session: req.session,
          product: product,
          qrcodes: results,
          total: total,
          page: req.params.page,
          totalPages: array,
          count: count,
          countUdid: countUdid,
          countNotUUID: count - countUdid,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          editor: true,
          admin: true,
          helpers: req.handlebars.helpers,
          config: req.app.config,
        });
      },
    );
  },
);

router.get(
  '/admin/qrcode/insert',
  csrfProtection,
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.redirect('/admin/products/0');
  },
);

// insert new qrcode form action
router.post(
  '/admin/qrcode/insert',
  csrfProtection,
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const numberOfItem = req.body.numberOfItem;
    if (numberOfItem < 0) {
      req.session.message = null;
      req.session.messageType = 'error';
      res.redirect('/admin/qrcode/view/' + req.body.id + '/0');
    } else {
      db.products.findOne({ _id: common.getId(req.body.id) }, (err, result) => {
        if (err || !result) {
          req.session.message = result ? 'product not found' : err;
          req.session.messageType = 'error';
          res.redirect('/admin/qrcode/view/' + req.body.id + '/0');
        } else {
          // let key = ursa.createPrivateKey(fs.readFileSync(Config.ServerPrivateKey));
          // let crt = ursa.createPublicKey(fs.readFileSync(Config.ServerPublicKey));
          const key = fs.readFileSync(Config.ServerPrivateKey);
          db.qrcodes
            .find(
              { productId: common.getId(req.params.id) },
              {
                projection: {
                  encryptedMessage: 1,
                },
              },
            )
            .toArray(async (err, existedArray) => {
              const qrcodeCount = existedArray.length;
              const total = qrcodeCount + parseInt(numberOfItem);
              const arrayNewItem = [];
              const arrayqrcode = [];
              for (let i = qrcodeCount; i < total; i++) {
                const randomString = randomstring.generate(5);
                const randomPassword = crypto.randomBytes(4).toString('hex');
                // let randomPack = randomstring.generate(2);
                const stringToEncryt =
                  result.productVariantSku + '-' + i + '-' + randomString;
                const encryptedMessage = crypto
                  .privateEncrypt(key, Buffer.from(stringToEncryt))
                  .toString('base64');
                // key.privateEncrypt(stringToEncryt, 'utf8', 'base64');
                if (
                  existedArray.indexOf(encryptedMessage) === -1 &&
                  arrayNewItem.indexOf(encryptedMessage) === -1
                ) {
                  arrayNewItem.push(encryptedMessage);
                  arrayqrcode.push({
                    shopifyProductId: result.shopifyProductId,
                    productId: result._id,
                    productVariantId: result.productVariantId,
                    productVariantSku: result.productVariantSku,
                    indexItem: i,
                    encrytedMessage: encryptedMessage,
                    productImage: result.productImageUrl,
                    qrcodeImage: null,
                    nfcUdid: null,
                    password: common.checkboxBool(req.body.generatePassword)
                      ? randomPassword
                      : null,
                    // packString: common.checkboxBool(req.body.generatePassword) ? randomPack : null,
                    udidCount: 0,
                    randomString: randomString,
                  });
                }
              }
              if (arrayNewItem.length > 0) {
                const [newqrcode] = await Promise.all([
                  db.qrcodes.insertMany(arrayqrcode),
                ]);
                console.log(
                  'save qrcode ' + arrayNewItem.length + ' successfully',
                );
              }
              res.redirect('/admin/qrcode/view/' + req.body.id + '/0');
            });
        }
      });
    }
  },
);

router.get(
  '/admin/qrcode/reset-udid/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const id = req.params.id;
    db.qrcodes.findOne({ _id: common.getId(id) }, async (err, qrcode) => {
      if (qrcode) {
        qrcode.udid = null;
        qrcode.udidCount = 0;
        const [qrcodeUpdate] = await Promise.all([
          db.qrcodes.update({ _id: qrcode._id }, { $set: qrcode }, {}),
        ]);
        req.session.message = 'Reset successfully';
        req.session.messageType = 'success';
        res.redirect('/admin/qrcode/view/' + qrcode.productId + '/0');
      } else {
        res.redirect('/admin/products/0');
      }
    });
  },
);

router.get(
  '/admin/qrcode/delete-all-qrcode/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const id = req.params.id;
    // remove the article
    db.qrcodes.remove(
      { productId: common.getId(id) },
      {},
      (err, numRemoved) => {
        if (err) {
          console.info(err.stack);
          req.session.message = 'something wrong.';
          req.session.messageType = 'error';
          res.redirect('/admin/qrcode/view/' + id + '/0');
        }
        req.session.message = 'successfully deleted';
        req.session.messageType = 'success';
        res.redirect('/admin/qrcode/view/' + id + '/0');
      },
    );
  },
);

router.get(
  '/admin/qrcode/reset-all-udid/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const id = req.params.id;
    db.qrcodes.update(
      { productId: common.getId(id) },
      { $set: { udid: null } },
      { multi: true },
      (err, numReplaced) => {
        if (err) {
          console.info(err.stack);
          req.session.message = 'something wrong.';
          req.session.messageType = 'error';
          res.redirect('/admin/qrcode/view/' + id + '/0');
        }
        req.session.message = 'successfully deleted';
        req.session.messageType = 'success';
        res.redirect('/admin/qrcode/view/' + id + '/0');
      },
    );
  },
);

router.get(
  '/admin/qrcode/export-qrcode-csv/:id/:from/:to',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const from = Number(req.params.from);
    const to = Number(req.params.to);
    db.products.findOne({ _id: common.getId(req.params.id) }, (err, result) => {
      if (err) {
        res.redirect('/admin/products/0');
      }
      db.qrcodes
        .find(
          {
            $and: [
              { productId: common.getId(req.params.id) },
              {
                $or: [
                  { udid: '' },
                  { udid: null },
                  { udid: { $exists: false } },
                ],
              },
            ],
          },
          {
            projection: {
              encrytedMessage: 1,
              udid: 1,
              password: 1,
            },
          },
        )
        .skip(from)
        .limit(to - from)
        .toArray(async (err, existedArray) => {
          let newArrayOfObj = [];
          if (existedArray.length > 0) {
            const item = existedArray[0];
            if (item.udid === undefined) {
              item.udid = '';
            }
            if (item.password === undefined) {
              item.password = '';
            }
            newArrayOfObj = existedArray.map(
              ({ encrytedMessage, udid, password }) => ({
                encryptedMessage: encrytedMessage,
                udid: udid,
                password: password,
              }),
            );
            res.xls(result.productVariantSku + '.xlsx', newArrayOfObj);
          } else {
            req.session.message = 'something wrong.';
            req.session.messageType = 'error';
            res.redirect(
              'admin/qrcode/view/' + common.getId(req.params.id) + '/0',
            );
          }
        });
    });
  },
);

// render the editor
router.get(
  '/admin/qrcode/import/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    db.products.findOne(
      { _id: common.getId(req.params.id) },
      (err, product) => {
        if (err) {
          console.info(err.stack);
        }
        res.render('qrcode_upload_csv', {
          title: 'Qrcode import',
          menu: 'product',
          admin: true,
          product: product,
          session: req.session,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          config: req.app.config,
          editor: true,
          helpers: req.handlebars.helpers,
        });
      },
    );
  },
);

// Update an existing product form action
router.post(
  '/admin/qrcode/import',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const form = new formidable.IncomingForm();
    form.uploadDir = './public/uploads/stores/';
    form.parse(req, async (err, fields, files) => {
      const mimeType = mime.lookup(files.file.name);
      if (
        !common.allowedCSVType.includes(mimeType) ||
        files.file.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(files.file.path);
        // Redirect to error
        req.session.message =
          'File type not allowed or too large. Please try again.';
        req.session.messageType = 'danger';
        res.redirect('/admin/qrcode/import/' + fields.product_id);
        return;
      }

      const [product] = await Promise.all([
        db.products.findOne({ _id: common.getId(fields.product_id) }),
      ]);
      if (product) {
        if (files.file.size > 0) {
          // import csv
          if (fields.fileType === '0') {
            const tmpPath = files.file.path;
            fs.createReadStream(tmpPath)
              .on('error', () => {
                req.session.message = 'Failed import csv.';
                req.session.messageType = 'danger';
                res.redirect('/admin/qrcode/import/' + fields.product_id);
              })
              .pipe(csvParser())
              .on('data', async (row) => {
                const [checkQrcode] = await Promise.all([
                  db.qrcodes.findOne({
                    encrytedMessage: row['encryptedMessage'],
                  }),
                ]);
                if (checkQrcode) {
                  const [result] = await Promise.all([
                    db.qrcodes.update(
                      { encrytedMessage: row['encryptedMessage'] },
                      { $set: { udid: row['udid'] } },
                      {},
                    ),
                  ]);
                } else {
                  // get last index of qrcode
                  const [lastIndex] = await Promise.all([
                    db.qrcodes
                      .findOne({
                        productVariantSku: product.productVariantSku,
                      })
                      .sort({ indexItem: -1 }),
                  ]);
                  let nextIndex = 1;
                  if (lastIndex) {
                    nextIndex = lastIndex.indexItem;
                  }
                  const newDoc = {
                    shopifyProductId: product.shopifyProductId,
                    productId: product._id,
                    productVariantId: product.productVariantId,
                    productVariantSku: product.productVariantSku,
                    indexItem: nextIndex,
                    encrytedMessage: row['encryptedMessage'],
                    productImage: product.productImageUrl,
                    qrcodeImage: null,
                    nfcUdid: null,
                    password: row['password'],
                    udidCount: 0,
                    randomString: row['password'],
                    udid: row['udid'],
                  };
                  const [result1] = await Promise.all([
                    db.qrcodes.insert(newDoc),
                  ]);
                }
              })
              .on('end', () => {
                req.session.message = 'Imported';
                req.session.messageType = 'success';
                res.redirect('/admin/qrcode/view/' + fields.product_id + '/0');
              });
          } else if (fields.fileType === '1') {
            const tmpPath = files.file.path;
            const workbook = XLSX.readFile(tmpPath); // ./assets is where your relative path directory where excel file is, if your excuting js file and excel file in same directory just igore that part
            const sheet_name_list = workbook.SheetNames; // SheetNames is an ordered list of the sheets in the workbook
            const data = XLSX.utils.sheet_to_json(
              workbook.Sheets[sheet_name_list[0]],
            ); // if you have multiple sheets
            const i = 0;
            console.log('total row: ' + data.length);
            for (let i = 0; i < data.length; i++) {
              const row = data[i];
              if (
                // eslint-disable-next-line no-prototype-builtins
                !row.hasOwnProperty('encryptedMessage') ||
                row['encryptedMessage'].length === 0
              ) {
                continue;
              }
              i++;
              console.log('processed row: ' + i);
              const [result] = await Promise.all([
                db.qrcodes.update(
                  { encrytedMessage: row['encryptedMessage'] },
                  { $set: { udid: row['udid'] } },
                  {},
                ),
              ]);
            }
            req.session.message = 'Imported';
            req.session.messageType = 'success';
            res.redirect('/admin/qrcode/view/' + fields.product_id + '/0');
          } else {
            req.session.message = 'File does not supported';
            req.session.messageType = 'danger';
            res.redirect('/admin/qrcode/import/' + fields.product_id);
          }
        } else {
          req.session.message = 'Failed import csv/xlsx';
          req.session.messageType = 'danger';
          res.redirect('/admin/qrcode/import/' + fields.product_id);
        }
      } else {
        req.session.message = 'Product not found';
        req.session.messageType = 'danger';
        res.redirect('/admin/qrcode/import/' + fields.product_id);
      }
    });
  },
);

router.get(
  '/admin/qrcode/filter/:id/:search',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const searchTerm = req.params.search;
    db.products.findOne(
      { _id: common.getId(req.params.id) },
      async (err, product) => {
        if (err) {
          res.redirect('/admin/products/0');
        }

        const [results, count] = await Promise.all([
          db.qrcodes
            .find(
              {
                $and: [
                  { productId: common.getId(req.params.id) },
                  {
                    $or: [
                      { encrytedMessage: new RegExp(searchTerm, 'i') },
                      { udid: new RegExp(searchTerm, 'i') },
                    ],
                  },
                ],
              },
              {
                projection: {
                  encrytedMessage: 1,
                  udid: 1,
                },
              },
            )
            .toArray(),
          db.qrcodes.count({ productId: common.getId(req.params.id) }),
        ]);
        res.render('qrcodes', {
          title: 'Qrcodes',
          menu: 'product',
          session: req.session,
          product: product,
          qrcodes: results,
          count: count,
          totalPages: 0,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          editor: true,
          admin: true,
          helpers: req.handlebars.helpers,
          config: req.app.config,
        });
      },
    );
  },
);

router.get(
  '/admin/qrcode/update-udid/:qrcodeId',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    db.qrcodes.findOne(
      { _id: common.getId(req.params.qrcodeId) },
      async (err, qrcode) => {
        if (err || !qrcode) {
          req.session.message = 'Item not found';
          req.session.messageType = 'danger';
          res.redirect('/admin/products/0');
        }

        const [product] = await Promise.all([
          db.products.findOne({ _id: common.getId(qrcode.productId) }),
        ]);

        res.render('qrcodes_edit', {
          title: 'Qrcodes Update',
          menu: 'product',
          session: req.session,
          qrcode: qrcode,
          product: product,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          editor: true,
          admin: true,
          helpers: req.handlebars.helpers,
          config: req.app.config,
        });
      },
    );
  },
);

router.post(
  '/admin/qrcode/update-udid',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const udid = req.param('newUdid');
    if (!udid) {
      req.session.message = 'Item not found';
      req.session.messageType = 'danger';
      res.redirect('/admin/qrcode/update-udid/' + req.param('qrcodeId'));
    }
    db.qrcodes.findOne(
      { _id: common.getId(req.param('qrcodeId')) },
      async (err, qrcode) => {
        if (err || !qrcode) {
          req.session.message = 'Item not found';
          req.session.messageType = 'danger';
          res.redirect('/admin/qrcode/view/' + req.param('productId') + '/0');
        }
        qrcode.udid = udid;
        const [qrcodeUpdate] = await Promise.all([
          db.qrcodes.update({ _id: qrcode._id }, { $set: qrcode }, {}),
        ]);
        req.session.message = 'Reset successfully';
        req.session.messageType = 'success';
        res.redirect('/admin/qrcode/update-udid/' + req.param('qrcodeId'));
      },
    );
  },
);

router.get(
  '/admin/qrcode/send-csv-to-manufacture',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    // list out all sku product
    const [productSkus] = await Promise.all([
      db.products
        .find(
          {},
          {
            projection: {
              _id: 1,
              productVariantSku: 1,
            },
          },
        )
        .toArray(),
    ]);

    res.render('send_encrypted_string', {
      title: 'Send Encrypted String',
      menu: 'product',
      productSkus: JSON.stringify(productSkus),
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
    });
  },
);

router.post(
  '/admin/qrcode/send-csv-to-manufacture',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const emailCC = req.body.emailCC;
    let limit = Number(req.body.count);
    if (limit === 0) {
      limit = 5000;
    }
    db.products.findOne(
      { _id: common.getId(req.body.product_id) },
      (err, result) => {
        if (err) {
          res.redirect('/admin/products/0');
        }
        db.qrcodes
          .find(
            {
              $and: [
                { productId: common.getId(req.body.product_id) },
                {
                  $or: [
                    { udid: '' },
                    { udid: null },
                    { udid: { $exists: false } },
                  ],
                },
              ],
            },
            {
              projection: {
                encrytedMessage: 1,
                udid: 1,
              },
            },
          )
          .limit(limit)
          .toArray(async (err, existedArray) => {
            let newArrayOfObj = [];
            if (existedArray.length > 0) {
              const item = existedArray[0];
              if (item.udid === undefined) {
                item.udid = '';
              }
              newArrayOfObj = existedArray.map(({ encrytedMessage, udid }) => ({
                encryptedMessage: encrytedMessage,
                udid: udid,
              }));
            }
            const csvString = json2csv(newArrayOfObj);
            const fileName = result.productVariantSku + '.csv';
            const path = '/tmp/' + fileName;
            fs.writeFile(path, csvString, (err, data) => {
              if (err) {
                console.log(err);
                res.status(400).json({
                  msg: 'Something went wrong',
                  code: 400,
                  data: [],
                });
              } else {
                console.log('file Created');
                const attachments = [
                  {
                    // file on disk as an attachment
                    filename: fileName,
                    path: path,
                  },
                ];
                common.sendEmail(
                  req.app.config.emailManufacture,
                  emailCC,
                  req.body.subject,
                  req.body.content,
                  attachments,
                );
                res.status(200).json({
                  msg: 'Email has been sent',
                  code: 200,
                  data: [],
                });
              }
            });
          });
      },
    );
  },
);

router.get(
  '/admin/qrcode/count/:sku',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    // list out all sku product
    const [total, pending, send, completed] = await Promise.all([
      db.qrcodes.count({ productVariantSku: req.params.sku }),
      db.qrcodes.count({
        $and: [
          { productVariantSku: req.params.sku },
          { isSent: true },
          {
            $or: [{ udid: '' }, { udid: null }],
          },
        ],
      }),
      db.qrcodes.count({
        $and: [
          { productVariantSku: req.params.sku },
          {
            $or: [{ isSent: { $exists: false } }, { isSent: false }],
          },
          {
            $or: [{ udid: '' }, { udid: null }],
          },
        ],
      }),
      db.qrcodes.count({
        $and: [
          { productVariantSku: req.params.sku },
          {
            $and: [
              { udid: { $exists: true } },
              { udid: { $ne: '' } },
              { udid: { $ne: null } },
            ],
          },
        ],
      }),
    ]);
    res.status(200).json({
      msg: 'ok',
      code: 200,
      data: {
        total: total,
        pending: pending,
        send: send,
        completed: completed,
      },
    });
  },
);

router.post(
  '/admin/qrcode/create-new-encrypted-string',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const numberOfItem = req.body.numberOfItem;
    if (numberOfItem < 0) {
      res.status(400).json({
        msg: 'Invalid number',
        code: 400,
        data: [],
      });
    } else {
      const [result] = await Promise.all([
        db.products.findOne({ productVariantSku: req.body.sku }),
      ]);
      if (!result) {
        res.status(400).json({
          msg: 'Invalid number',
          code: 400,
          data: [],
        });
        return;
      }
      const key = fs.readFileSync(Config.ServerPrivateKey);
      // let key = ursa.createPrivateKey(fs.readFileSync(Config.ServerPrivateKey));
      // let crt = ursa.createPublicKey(fs.readFileSync(Config.ServerPublicKey));
      const [existedArray] = await Promise.all([
        db.qrcodes
          .find(
            { productId: common.getId(req.params.id) },
            {
              projection: {
                encryptedMessage: 1,
              },
            },
          )
          .toArray(),
      ]);
      const qrcodeCount = existedArray.length;
      const total = qrcodeCount + parseInt(numberOfItem);
      const arrayNewItem = [];
      const arrayqrcode = [];
      for (let i = qrcodeCount; i < total; i++) {
        const randomString = randomstring.generate(5);
        const randomPassword = crypto.randomBytes(4).toString('hex');
        // let randomPack = randomstring.generate(2);
        const stringToEncryt =
          result.productVariantSku + '-' + i + '-' + randomString;
        const encryptedMessage = crypto
          .privateEncrypt(key, Buffer.from(stringToEncryt))
          .toString('base64');
        if (
          existedArray.indexOf(encryptedMessage) === -1 &&
          arrayNewItem.indexOf(encryptedMessage) === -1
        ) {
          arrayNewItem.push(encryptedMessage);
          arrayqrcode.push({
            shopifyProductId: result.shopifyProductId,
            productId: result._id,
            productVariantId: result.productVariantId,
            productVariantSku: result.productVariantSku,
            indexItem: i,
            encrytedMessage: encryptedMessage,
            productImage: result.productImageUrl,
            qrcodeImage: null,
            nfcUdid: null,
            isSent: false,
            password: common.checkboxBool(req.body.generatePassword)
              ? randomPassword
              : null,
            isForrealLeatherKeyfob: req.body.isForrealLeatherKeyfob,
            udidCount: 0,
            randomString: randomString,
          });
        }
      }
      if (arrayNewItem.length > 0) {
        const [newqrcode] = await Promise.all([
          db.qrcodes.insertMany(arrayqrcode),
        ]);
        console.log('save qrcode ' + arrayNewItem.length + ' successfully');
      }

      const [totalQrcode, pending, send, completed] = await Promise.all([
        db.qrcodes.count({ productId: result._id }),
        db.qrcodes.count({
          $and: [
            { productId: result._id },
            { isSent: { $exists: true } },
            { isSent: true },
            {
              $or: [{ udid: '' }, { udid: null }],
            },
          ],
        }),
        db.qrcodes.count({
          $and: [
            { productId: result._id },
            {
              $or: [{ isSent: { $exists: false } }, { isSent: false }],
            },
            {
              $or: [{ udid: '' }, { udid: null }],
            },
          ],
        }),
        db.qrcodes.count({
          $and: [
            { productId: result._id },
            {
              $and: [
                { udid: { $exists: true } },
                { udid: { $ne: '' } },
                { udid: { $ne: null } },
              ],
            },
          ],
        }),
      ]);
      res.status(200).json({
        msg: 'ok',
        code: 200,
        data: {
          total: totalQrcode,
          pending: pending,
          send: send,
          completed: completed,
        },
      });
    }
  },
);

router.post(
  '/admin/qrcode/send-bulk-csv-to-manufacture',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const arraySku = req.body.arraySku;

    const newItem = {
      adminId: common.getId(req.session.userId),
      creator: req.session.user,
      arraySku: JSON.parse(arraySku),
      isSent: false,
      emailManufacture: req.body.emailManufacture,
      subject: req.body.subject,
      emailCC: req.body.emailCC,
      content: req.body.content,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    const [restult] = await Promise.all([
      db.sendEmailToManufacture.insertOne(newItem),
    ]);

    res.status(200).json({
      msg: 'System is processing. Please wait for a while.',
      code: 200,
      data: [],
    });
  },
);

router.get(
  '/admin/qrcode/nfc-udid-request',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const skuAdded = req.param('sku');
    // list out all sku product
    const [productSkus] = await Promise.all([
      db.products
        .find(
          {},
          {
            projection: {
              _id: 1,
              productVariantSku: 1,
            },
          },
        )
        .toArray(),
    ]);

    let dataAddedSku = null;

    if (skuAdded) {
      const [totalQrcode, pending, send, completed] = await Promise.all([
        db.qrcodes.count({ productVariantSku: skuAdded }),
        db.qrcodes.count({
          $and: [
            { productVariantSku: skuAdded },
            { isSent: { $exists: true } },
            { isSent: true },
            {
              $or: [{ udid: '' }, { udid: null }],
            },
          ],
        }),
        db.qrcodes.count({
          $and: [
            { productVariantSku: skuAdded },
            {
              $or: [{ isSent: { $exists: false } }, { isSent: false }],
            },
            {
              $or: [{ udid: '' }, { udid: null }],
            },
          ],
        }),
        db.qrcodes.count({
          $and: [
            { productVariantSku: skuAdded },
            {
              $and: [
                { udid: { $exists: true } },
                { udid: { $ne: '' } },
                { udid: { $ne: null } },
              ],
            },
          ],
        }),
      ]);

      dataAddedSku = {
        total: totalQrcode,
        pending: pending,
        send: send,
        completed: completed,
        sku: skuAdded,
      };
    }

    res.render('nfc_udid_request', {
      title: 'NFC UDID Request',
      menu: 'product',
      productSkus: JSON.stringify(productSkus),
      dataAddedSku: dataAddedSku ? JSON.stringify(dataAddedSku) : '',
      isEnableSEndEmailV2:
        process.env.ENABLE_CRONJOB_SEND_EMAIL_V2 == 'true' ? true : false,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
    });
  },
);

router.post(
  '/admin/qrcode/batch-upload-edition-number',
  common.restrict,
  common.checkAccess,
  upload.single('upload_file'),
  async (req, res) => {
    const db = req.app.db;
    const productId = req.body.productId;
    if (!productId) {
      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
    }
    const file = req.file;
    if (file) {
      // Get the mime type of the file
      const mimeType = mime.lookup(file.originalname);

      // Check for allowed mime type and file size
      if (
        !common.allowedCSVType.includes(mimeType) ||
        file.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(file.path);
        res.status(400).json({
          code: 400,
          message: 'File type not allowed or too large. Please try again.',
        });
        return;
      }
    }

    const product = await db.products.findOne({ _id: common.getId(productId) });
    if (!product) {
      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
    }
    const result = await encryptedString.generateEditionNumber(
      db,
      req.file,
      product,
    );

    const countEditionNumber = await encryptedString.countEditionNumber(
      db,
      product.productVariantSku,
    );

    if (result.status) {
      res.status(200).json({
        code: 200,
        message: 'Successfully updated',
        data: {
          countEditionNumber: countEditionNumber,
        },
      });
    } else {
      res.status(400).json({
        code: 400,
        message: result.message,
      });
    }
  },
);

//Check exist data for gameAndShiftCode
router.get('/api/v4/getGameAndShiftCodeBySku/:sku', async (req, res) => {
  const db = req.app.db;
  const sku = req.params.sku;
  const [data] = await Promise.all([
    db.gameAndShiftCode.findOne({ productVariantSku: sku }),
  ]);
  return res.status(200).json({
    code: 200,
    result: data,
  });
});

//Check exists data for voucherCodes
router.get('/api/v4/getVoucherCodeBySku/:sku', async (req, res) => {
  const db = req.app.db;
  const sku = req.params.sku;
  const [data] = await Promise.all([
    db.voucherCodes.findOne({ productVariantSku: sku }),
  ]);
  return res.status(200).json({
    code: 200,
    result: data,
  });
});

async function checkTypeCode(code, sku) {
  const listShiftCodeSku = [
    'LAB-21DFDWMBSOG49',
    'LAB-21DFDWMMMPOG52',
    'LAB-21DFDWMMROG50',
    'LAB-21DFDWMSKOG51',
    'LAB-21DFDWMTTOG47',
    'LAB-21WBSSATOG40',
  ];

  const listGameCode = ['LAB-21GBWLTTBSOG42', 'LAB-21GBWLTTBSOG54'];
  //check is code game
  if (code.length === 23 && listGameCode.includes(sku)) {
    const userKeyRegExp =
      /^[A-Z0-9]{5}\-[A-Z0-9]{5}\-[A-Z0-9]{5}\-[A-Z0-9]{5}?$/;
    const valid = userKeyRegExp.test(code);
    if (valid) {
      return 'GameCode';
    }
  } else if (code.length === 29 && listShiftCodeSku.includes(sku)) {
    const userKeyRegExp =
      /^[A-Z0-9]{5}\-[A-Z0-9]{5}\-[A-Z0-9]{5}\-[A-Z0-9]{5}\-[A-Z0-9]{5}?$/;
    const valid = userKeyRegExp.test(code);
    if (valid) {
      return 'ShiftCode';
    }
  } else {
    if (sku === 'LAB-21GBWLTTBSOG42') {
      return 'GameCodeValid';
    } else if (sku === 'LAB-21WBSSATOG40') {
      return 'ShiftCodeValid';
    }
  }
  return 'InvalidCode';
}

async function generateCodeGames(file, db, product) {
  let countCodesUploaded = 0;
  if (file) {
    //Read file data
    const data = await csvToJson().fromFile(file.path);
    const codes = [];
    const emails = [];
    const listGameCode = ['LAB-21GBWLTTBSOG42', 'LAB-21GBWLTTBSOG54'];
    let foundCodeValid = false;

    for (let i = 0; i < data.length; i++) {
      const row = data[i];
      if (
        listGameCode.includes(product.productVariantSku) &&
        !row.hasOwnProperty('email')
      ) {
        return {
          status: false,
          message: 'Key of column email is invalid, valid is "email"',
        };
      }

      if (!row.hasOwnProperty('Code')) {
        return {
          status: false,
          message: 'Key of column code is invalid, valid is "Code"',
        };
      }

      codes.push(row['Code']);
      emails.push(row['email']);
      if (!row.hasOwnProperty('Code') || row['Code'] === '') {
        foundCodeValid = true;
      }
    }

    if (foundCodeValid) {
      return {
        status: false,
        message: 'Code must have not null or empty',
      };
    }

    //toFindGameCodeDuplicates
    const toFindGameCodeDuplicates = (codes) =>
      codes.filter((item, index) => codes.indexOf(item) !== index);
    const duplicateValue = toFindGameCodeDuplicates(codes);

    if (duplicateValue.length > 0) {
      return {
        status: false,
        message: 'Duplicate Code',
      };
    }

    // insert data from list data of file upload
    let docs = [];
    for (let j = 0; j < data.length; j++) {
      const row = data[j];
      let doc = {
        productVariantSku: product.productVariantSku,
        udid: null,
        code: row['Code'],
        status: 'Unallocated', //1.Unallocated, 2.Allocated , 3.Expired
        type: 'GameCode', //1.GameCode, 2.ShiftCode
      };

      if (row.hasOwnProperty('Code') && row['Code'] !== '') {
        const checkFormatCode = await checkTypeCode(
          row['Code'],
          product.productVariantSku,
        );
        switch (checkFormatCode) {
          case 'GameCode':
            doc.type = 'GameCode';
            doc.email = row['email'];
            doc.status = 'Unallocated';
            break;
          case 'ShiftCode':
            doc.type = 'ShiftCode';
            doc.status = 'Unallocated';
            doc.email = null;
            break;
          case 'InvalidCode':
            return {
              status: false,
              message: 'Code format invalid',
            };
          case 'GameCodeValid':
            return {
              status: false,
              message: 'Game code is invalid, please check format',
            };
          case 'ShiftCodeValid':
            return {
              status: false,
              message: 'Shift code is invalid, please check format',
            };
          default:
            return {
              status: false,
              message: 'Code format invalid',
            };
        }
      }

      docs.push(doc);
      countCodesUploaded++;
    }

    if (docs.length > 0) {
      await db.gameAndShiftCode.deleteMany({
        productVariantSku: product.productVariantSku,
        email: { $in: [null, ''] },
      });

      const listGameCode = await db.gameAndShiftCode
        .find({
          productVariantSku: product.productVariantSku,
        })
        .toArray();
      const listInsertCode = [];
      docs.forEach((doc) => {
        if (!listGameCode.some((e) => e.code === doc.code)) {
          listInsertCode.push(doc);
        }
      });
      await db.gameAndShiftCode.insertMany(listInsertCode);
    }

    return {
      status: true,
      countCodesUploaded: countCodesUploaded,
      message: '',
    };
  }

  return {
    status: false,
    message: 'File is invalid',
  };
}

async function generateVoucherCodes(file, db, product) {
  if (file) {
    //Read file data
    const data = await csvToJson().fromFile(file.path);
    const emails = [];
    const skus = [];
    const skusEachEmail = [];
    const errorEmail = [];
    const accept4InchsProd = [
      'LAB-21DFDWMBSOG49',
      'LAB-21DFDWMMMPOG52',
      'LAB-21DFDWMMROG50',
      'LAB-21DFDWMSKOG51',
      'LAB-21DFDWMTTOG47',
    ];
    const accept8InchsProd = 'LAB-21GBWLTTBSOG42';
    for (let i = 0; i < data.length; i++) {
      const row = data[i];
      if (!row.hasOwnProperty('email')) {
        return {
          status: false,
          message: 'Key of column email is invalid, valid is "email"',
        };
      }
      if (!row.hasOwnProperty('sku')) {
        return {
          status: false,
          message: 'Key of column sku is invalid, valid is "sku"',
        };
      }
    }

    data.forEach((e) => {
      skus.push(e.sku);
      emails.push(e.email);
    });

    const counts = {};
    emails.forEach(function (x) {
      counts[x] = (counts[x] || 0) + 1;
    });
    if (!Object.values(counts).every((e) => e >= 2 && e % 2 == 0)) {
      return {
        status: false,
        message:
          'Emails in the CSV file may be duplicated or need to be assigned 2 sub-vouchers',
      };
    }

    if (!skus.every((e) => accept4InchsProd.includes(e))) {
      return {
        status: false,
        message: 'SKUs in the CSV do not match with previously uploaded SKUs',
      };
    }
    // insert data from list data of file upload
    const listEmails = [...new Set(emails)];
    listEmails.forEach((e) => {
      const listSkus = data.filter((i) => i.email === e);
      for (let i = 0; i < listSkus.length; i += 2) {
        if (listSkus[i].sku === listSkus[i + 1].sku) {
          errorEmail.push(e);
        }
      }
    });
    if (errorEmail.length) {
      return {
        status: false,
        message: 'SKUs with same email in the CSV cannot duplicate',
      };
    }

    const docs = [];
    const countEmail = listEmails.length;
    if (countEmail > 0) {
      const listProducts = await db.voucherCodes
        .find({ email: { $in: emails } })
        .toArray();
      for (let j = 0; j < countEmail; j++) {
        const email = listEmails[j];

        const sku = data.filter((e) => e.email === email).map((e) => e.sku);
        if (sku.length > 2) {
          for (let i = 0; i < sku.length; i += 2) {
            const doc = {
              productVariantSku: product.productVariantSku,
              email: email,
              udid: '',
              createdAt: new Date(),
              status: 'Unallocated',
              unlockVoucherWithSKU: sku.slice(i, i + 2),
            };
            docs.push(doc);
          }
        } else {
          const doc = {
            productVariantSku: product.productVariantSku,
            email: email,
            udid: '',
            createdAt: new Date(),
            status: 'Unallocated',
            unlockVoucherWithSKU: sku,
          };
          docs.push(doc);
        }
      }
      await db.voucherCodes.deleteMany({ email: { $in: emails } });
      await db.voucherCodes.insertMany(docs);
    }

    return {
      status: true,
      countCodesUploaded: emails.length / 2,
      message: 'File had been uploaded',
    };
  }

  return {
    status: false,
    message: 'File is invalid',
  };
}

router.post(
  '/admin/qrcode/upload-game-shift-codes',
  common.restrict,
  common.checkAccess,
  upload.single('upload_file'),
  async (req, res) => {
    const db = req.app.db;
    const productId = req.body.productId;
    if (!productId) {
      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
    }
    const file = req.file;
    if (file) {
      // Get the mime type of the file
      const mimeType = mime.lookup(file.originalname);

      // Check for allowed mime type and file size
      if (
        !common.allowedCSVType.includes(mimeType) ||
        file.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(file.path);
        res.status(400).json({
          code: 400,
          message: 'File type not allowed or too large. Please try again.',
        });
        return;
      }
    }
    const product = await db.products.findOne({ _id: common.getId(productId) });
    if (!product) {
      //Remove temp file
      fs.unlinkSync(file.path);

      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
    }

    const result = await generateCodeGames(req.file, db, product);

    if (result) {
      //Remove temp file
      fs.unlinkSync(file.path);
    }

    if (result.status) {
      common.Schedule(
        req.app,
        'gameAndShiftCode',
        'expiryDate',
        { status: 'Expired' },
        'gameAndShiftCode',
        { type: 'ShiftCode', status: 'Unallocated' },
      );

      res.status(200).json({
        code: 200,
        message: 'Updated',
        data: {
          countCodesUploaded: result.countCodesUploaded,
        },
      });
    } else {
      res.status(400).json({
        code: 400,
        message: result.message,
      });
    }
  },
);
// api for test shift code
router.get('/api/v4/getShiftCode/:udid', async (req, res) => {
  const data = [
    {
      _id: '623ad625f2147208700f9dc1',
      productVariantSku: 'LAB-21WBSSATOG40',
      udid: '049F25727F6380',
      code: 'SCWBT-KJHQE-6ZJR3-VWD8F-XVA5S',
      status: 'Allocated',
      type: 'ShiftCode',
    },
  ];
  res.status(200).json({
    code: 200,
    data: [],
  });
});
router.post(
  '/admin/qrcode/upload-voucher-codes',
  common.restrict,
  common.checkAccess,
  upload.single('upload_file'),
  async (req, res) => {
    const db = req.app.db;
    const productId = req.body.productId;
    if (!productId) {
      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
    }
    const file = req.file;
    if (file) {
      // Get the mime type of the file
      const mimeType = mime.lookup(file.originalname);

      // Check for allowed mime type and file size
      if (
        !common.allowedCSVType.includes(mimeType) ||
        file.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(file.path);
        res.status(400).json({
          code: 400,
          message: 'File type not allowed or too large. Please try again.',
        });
        return;
      }
    }
    const product = await db.products.findOne({ _id: common.getId(productId) });
    if (!product) {
      //Remove temp file
      fs.unlinkSync(file.path);

      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
    }
    const result = await generateVoucherCodes(req.file, db, product);

    if (result) {
      //Remove temp file
      fs.unlinkSync(file.path);
    }

    if (result.status) {
      res.status(200).json({
        code: 200,
        message: 'Updated',
        data: {
          countCodesUploaded: result.countCodesUploaded,
        },
      });
    } else {
      res.status(400).json({
        code: 400,
        message: result.message,
      });
    }
  },
);

router.post(
  '/admin/qrcode/verify-edition-number',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const udid = await db.qrcodes.findOne({
      $and: [
        { productVariantSku: req.body.sku },
        { editionNumber: req.body.newEditionNumber },
      ],
    });
    if (udid) {
      res.status(200).json({
        code: 200,
        message: {
          used: true,
        },
      });
    } else {
      res.status(200).json({
        code: 200,
        message: {
          used: false,
        },
      });
    }
  },
);

router.post(
  '/admin/qrcode/reset-edition-number',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const productId = req.body.productId;

    const product = await db.products.findOne({ _id: common.getId(productId) });

    if (!product) {
      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
      return;
    }
    await db.qrcodes.update(
      { productVariantSku: product.productVariantSku },
      { $set: { editionNumber: null } },
      { multi: true },
    );

    await db.orders.update(
      { sku: product.productVariantSku },
      { $set: { editionNumber: null } },
      { multi: true },
    );

    res.status(200).json({
      code: 200,
      message: 'Successfully updated',
    });
  },
);

router.post(
  '/admin/scan-to-unlock-code/reset-code',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const productId = req.body.productId;
    const product = await db.products.findOne({ _id: common.getId(productId) });
    if (!product) {
      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
      return;
    }

    await Promise.all([
      db.gameAndShiftCode.deleteMany(
        {
          productVariantSku: product.productVariantSku,
          status: { $ne: 'Allocated' },
          $and: [{ udid: { $eq: '' } }, { email: { $eq: '' } }],
        },
        (err) => {
          if (err) {
            res.status(400).json({ code: 400, message: err, data: null });
            return;
          }
        },
      ),
    ]);

    res.status(200).json({
      code: 200,
      message: 'Successfully updated',
    });
  },
);

router.post(
  '/admin/scan-to-unlock-voucher-code/reset-code',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const productId = req.body.productId;
    const product = await db.products.findOne({ _id: common.getId(productId) });

    if (!product) {
      res.status(400).json({
        code: 400,
        message: 'Product not found',
      });
      return;
    }

    await Promise.all([
      db.voucherCodes.deleteMany(
        { productVariantSku: product.productVariantSku },
        (err) => {
          if (err) {
            res.status(400).json({ code: 400, message: err, data: null });
            return;
          }
        },
      ),
    ]);

    res.status(200).json({
      code: 200,
      message: 'Successfully updated',
    });
  },
);

module.exports = router;
