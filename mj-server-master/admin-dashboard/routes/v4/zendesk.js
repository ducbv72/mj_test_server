const express = require('express');
const router = express.Router();
const Promise = require('promise');
const fetch = require('node-fetch');

/**
 * @swagger
 * /api/v4/zendesksync:
 *  post:
 *    tags:
 *      - Zendesk
 *    description: Retrieve MJ's Zendesk Information
 *    responses:
 *      '200':
 *        description: A successful response
 */

router.post('/api/v4/zendesksync', async (req, res) => {
  const db = req.app.db;
  let nextPage =
    'https://mightyjaxx.zendesk.com/api/v2/help_center/en-us/sections/360000589673/articles.json?page=0&per_page=50';
  const header = 'Title' + '\t' + 'Id' + '\t' + 'Url' + '\n';
  let content = header;
  while (nextPage != null) {
    const request = await fetch(nextPage);
    const result = await request.json();
    nextPage = result.next_page;
    const articles = result.articles;

    for (const i in articles) {
      const filter = articles[i].title;
      const [product] = await Promise.all([
        db.products.findOne({
          productTitle: { $regex: new RegExp(filter, 'i') },
        }),
      ]);

      if (product) {
        product.zendeskUrl = articles[i].html_url;
        await Promise.all([
          db.products.updateOne(
            { _id: product._id },
            { $set: product },
            {},
            false,
          ),
        ]);
        continue;
      } else {
        content += articles[i].title + '\t';
        content += articles[i].id + '\t';
        content += articles[i].html_url + '\t';
        content += '\n';
      }
    }
  }
  res.setHeader('Content-Type', 'application/vnd.openxmlformats');
  res.setHeader(
    'Content-Disposition',
    'attachment; filename=zendesksyncresult.xls',
  );
  res.write(new Buffer([0xff, 0xfe]));
  // res.write(iconv.convert(content));
  res.end();
});

module.exports = router;
