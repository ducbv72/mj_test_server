const express = require('express');
const common = require('../../../lib/common');
const router = express.Router();
const ethereumConfig = require('../../../config/ethereumConfig');
const Config = require('../../../config');
const { base64encode, base64decode } = require('nodejs-base64');
const { shopify } = require('../../../config/shopify');
const adminFirebase = require('firebase-admin');
const moment = require('moment-timezone');

router.get(
  '/admin/settings/send-notification',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    // get the top results
    db.products
      .find({})
      .sort({ productAddedDate: -1 })
      .toArray((err, topResults) => {
        if (err) {
          console.info(err.stack);
        }
        res.render('notification/notification', {
          title: 'Send Notification',
          products: topResults,
          session: req.session,
          admin: true,
          config: req.app.config,
          message: common.clearSessionValue(req.session, 'message'),
          messageType: common.clearSessionValue(req.session, 'messageType'),
          helpers: req.handlebars.helpers,
          menu: 'notification',
        });
      });
  },
);

router.post(
  '/admin/settings/send-notification',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    try {
      let b2bChannel = 'Nyansum';
      let topicName = Config.MJ_CHANNEL;
      const notificationId = req.body.notificationId;
      if (process.env.IGNORE_TOKEN === 'true') {
        topicName = Config.MJ_STAGING_CHANNEL;
        b2bChannel = 'Nyansum-staging';
      }
      if (req.body.B2BItem === 'on') {
        topicName = b2bChannel;
      }
      if (req.body.emails) {
        topicName = 'Private channel';
      }

      const payLoad = req.body.payload ? req.body.payload : '{}';
      const productLocalId =
        req.body.productId !== '-1' ? req.body.productId : null;
      const dataPrepared = await prepareMessageContent(
        db,
        req.body.emails,
        req.body.title,
        req.body.content,
        payLoad,
        productLocalId,
        topicName,
      );
      if (!dataPrepared.result) {
        req.session.message = dataPrepared.message;
        req.session.messageType = 'danger';
        if (notificationId) {
          res.redirect('/admin/notification/edit/' + notificationId);
        } else {
          res.redirect('/admin/settings/send-notification');
        }
        return;
      }

      const resultData = await sendNotificationToEmails(
        req.session.user,
        db,
        req.body.emails,
        productLocalId,
        req.body.notificationId,
        req.body.scheduleTime,
        req.body.title,
        payLoad,
        topicName,
        req.body.B2BItem === 'on',
        dataPrepared.data,
      );
      console.log(resultData);
      if (resultData.result) {
        req.session.message = resultData.message;
        req.session.messageType = 'success';
        if (notificationId) {
          res.redirect('/admin/notification/edit/' + notificationId);
        } else {
          res.redirect('/admin/settings/scheduled/notification');
        }
      } else {
        req.session.message = resultData.message;
        req.session.messageType = 'danger';
        if (notificationId) {
          res.redirect('/admin/notification/edit/' + notificationId);
        } else {
          res.redirect('/admin/settings/send-notification');
        }
      }
    } catch (error) {
      console.log('Error sending message:' + error);
      req.session.message = 'Something went wrong ' + error.message;
      req.session.messageType = 'danger';
      res.redirect('/admin/settings/send-notification');
    }
  },
);

router.get(
  '/admin/settings/scheduled/notification',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    // get the top results
    const db = req.app.db;
    let page = Number(req.query.page);
    if (!page || page <= 0) page = 1;
    const skip = (page - 1) * Number(ethereumConfig.productLitmitPerPage);
    const [results, count] = await Promise.all([
      db.scheduleNotifications
        .find({})
        .sort({ scheduleTime: -1 })
        .skip(skip)
        .limit(ethereumConfig.productLitmitPerPage)
        .toArray(),
      db.scheduleNotifications.count({}),
    ]);
    const navigationUrl =
      process.env.APP_URL + '/admin/settings/scheduled/notification';
    res.render('notification/notification_scheduled', {
      title: 'Schedule Notification',
      notifications: results,
      page: page,
      currentPage: page,
      nextPage: Number(page) < count ? Number(page) + 1 : count,
      backPage: Number(page) > 0 ? Number(page) - 1 : 0,
      totalPage: Math.ceil(count / ethereumConfig.productLitmitPerPage) - 1,
      navigationUrl: navigationUrl,
      session: req.session,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
      menu: 'notification',
    });
  },
);

//edit notification
router.get(
  '/admin/notification/edit/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const [notification, products] = await Promise.all([
      db.scheduleNotifications.findOne({ _id: common.getId(req.params.id) }),
      db.products
        .find(
          {},
          {
            projection: {
              shopifyProductId: 1,
              productTitle: 1,
              productVariantSku: 1,
            },
          },
        )
        .sort({
          productAddedDate: -1,
        })
        .toArray(),
    ]);
    if (notification) {
      const notificationData = JSON.parse(notification.content);
      let content = null;
      let data = null;
      if (Array.isArray(notificationData)) {
        content =
          notificationData.length > 0 ? notificationData[0].notification : null;
        data = notificationData.length > 0 ? notificationData[0].data : null;
      } else {
        content = notificationData ? notificationData.notification : null;
        data = notificationData ? notificationData.data : null;
      }
      const productId = data
        ? base64decode(data.productId).replace('gid://shopify/Product/', '')
        : null;

      res.render('notification/notification_edit', {
        title: 'Send Notification',
        notificationTitle: content ? content.title : '',
        notificationBody: content ? content.body : '',
        notification: notification,
        productId: productId,
        products: products,
        session: req.session,
        admin: true,
        config: req.app.config,
        message: common.clearSessionValue(req.session, 'message'),
        messageType: common.clearSessionValue(req.session, 'messageType'),
        helpers: req.handlebars.helpers,
        menu: 'notification',
      });
    } else {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/settings/scheduled/notification');
    }
  },
);

// delete notification
router.get(
  '/admin/notification/delete/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    // remove the a notification
    await db.scheduleNotifications.remove(
      { _id: common.getId(req.params.id) },
      {},
    );
    req.session.message = 'Notification successfully deleted';
    req.session.messageType = 'success';
    res.redirect('/admin/settings/scheduled/notification');
  },
);

/**
 * prepare data before sending notification
 * @param database
 * @param emails
 * @param title
 * @param content
 * @param payload
 * @param productLocalId
 * @param topic
 * @returns {Promise<{result: boolean, errorMessage: string}|{result: boolean, data: *[], errorMessage: string}>}
 */
async function prepareMessageContent(
  database,
  emails,
  title,
  content,
  payload,
  productLocalId,
  topic,
) {
  try {
    const payloadData = JSON.parse(payload);
    payloadData.productId = '';
    const messages = [];
    if (!title) {
      return {
        result: false,
        message: 'Title is empty',
      };
    }
    if (productLocalId) {
      const productLocal = await database.products.findOne({
        _id: common.getId(productLocalId),
      });
      if (!productLocal) {
        return {
          result: false,
          message: 'Product not found',
        };
      }
      const shopifyProduct = await shopify.product.get(
        Number(productLocal.shopifyProductId),
      );
      if (!shopifyProduct) {
        return {
          result: false,
          message: 'Product is not exist on shopify',
        };
      }
      payloadData.productId = base64encode(
        `gid://shopify/Product/${productLocal.shopifyProductId}`,
      );
      if (productLocal?.productVariantSku) {
        payloadData.productVariantSku = productLocal.productVariantSku;
      }
      payloadData.type = 'Product';
    } else {
      payloadData.type = 'General';
    }
    if (emails) {
      // get emails from our server
      const arrayEmail = emails.split(',');
      for (let i = 0; i < arrayEmail.length; i++) {
        const [user] = await Promise.all([
          database.customers.findOne({
            email: {
              $regex: `^${arrayEmail[i].trim().toLowerCase()}$`,
              $options: 'i',
            },
          }),
        ]);
        if (
          user &&
          user.email.toLowerCase().trim() === arrayEmail[i].trim().toLowerCase()
        ) {
          messages.push({
            notification: {
              title: title,
              body: content,
            },
            data: payloadData,
            topic: user.customerId,
          });
        }
      }
    } else {
      messages.push({
        notification: {
          title: title,
          body: content,
        },
        data: payloadData,
        topic: topic,
      });
    }
    if (messages.length === 0) {
      return {
        result: false,
        message: 'There is no receiver',
      };
    }
    return {
      result: true,
      message: '',
      data: messages,
    };
  } catch (e) {
    console.log(e);
    return {
      result: false,
      message: 'Something went wrong',
    };
  }
}

/**
 * sending notification to user
 * @param userName
 * @param database
 * @param emails
 * @param productLocalId
 * @param notificationId
 * @param scheduleTime
 * @param title
 * @param payload
 * @param topic
 * @param isB2BItem
 * @param messages
 * @returns {Promise<boolean>}
 */
async function sendNotificationToEmails(
  userName,
  database,
  emails,
  productLocalId,
  notificationId,
  scheduleTime,
  title,
  payload,
  topic,
  isB2BItem,
  messages,
) {
  const productLocal = productLocalId
    ? await database.products.findOne({ _id: common.getId(productLocalId) })
    : null;

  const notification = {
    receiverEmail: emails,
    isB2BItem,
    content: JSON.stringify(messages),
    topic,
    payload,
    productId: productLocal ? productLocal.shopifyProductId : null,
    productVariantSku: productLocal ? productLocal.productVariantSku : null,
    productLocalId,
    title,
    scheduleTime: common.convertTimezone(new Date(), 'Asia/Singapore'),
    isSent: true,
    sendDate: new Date(),
    status: 'success',
    updatedAt: new Date(),
  };

  if (scheduleTime) {
    const scheduleNotification = {
      ...notification,
      createdBy: userName,
      scheduleTime: common.convertTimezone(
        new Date(moment(scheduleTime, 'DD/MM/YYYY hh:mm A').toISOString()),
        'Asia/Singapore',
      ),
      sendDate: null,
      isSent: false,
      status: 'pending',
    };
    await common.saveToActionLog(
      database,
      userName,
      'send notification',
      scheduleNotification,
    );
    if (notificationId) {
      await Promise.all([
        database.scheduleNotifications.update(
          { _id: common.getId(notificationId) },
          { $set: scheduleNotification },
          { multi: false },
        ),
      ]);
      return {
        result: true,
        message: 'Notification has been updated',
      };
    } else {
      scheduleNotification.createdAt = new Date();
      database.scheduleNotifications.insert(scheduleNotification);
      return {
        result: true,
        message: 'Notification has been scheduled',
      };
    }
  } else {
    // send right away and update old notification to done
    // save log action
    try {
      await adminFirebase.messaging().sendAll(messages);
      if (notificationId) {
        await Promise.all([
          database.scheduleNotifications.update([
            { _id: common.getId(notificationId) },
            {
              $set: {
                isSent: true,
                result: 'success',
                scheduleTime: common.convertTimezone(
                  new Date(),
                  'Asia/Singapore',
                ),
              },
            },
            { multi: false },
          ]),
        ]);
      } else {
        notification.createdAt = new Date();
        database.scheduleNotifications.insert(notification);
      }
      await common.saveToActionLog(
        database,
        userName,
        'send notification',
        notification,
      );
      return {
        result: true,
        message: 'Message has been sent successful',
      };
    } catch (ex) {
      console.log(ex);
      if (notificationId) {
        await Promise.all([
          database.scheduleNotifications.update([
            { _id: common.getId(notificationId) },
            {
              $set: {
                isSent: true,
                result: 'failure',
                scheduleTime: common.convertTimezone(
                  new Date(),
                  'Asia/Singapore',
                ),
              },
            },
            { multi: false },
          ]),
        ]);
      }
      notification.status = 'failure';
      await common.saveToActionLog(
        database,
        userName,
        'send notification',
        notification,
      );
      return {
        result: false,
        message: 'Error sending message ' + ex?.message,
      };
    }
  }
}

module.exports = router;
