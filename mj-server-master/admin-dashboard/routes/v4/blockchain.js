const _ = require('lodash');
const ethWallet = require('ethereumjs-wallet');
const ethUtil = require('ethereumjs-util');
const Tx = require('ethereumjs-tx');
const Web3 = require('web3');
const Config = require('../../../config/ethereumConfig');
const web3 = new Web3(new Web3.providers.HttpProvider(process.env.EtherNet));
let currentNonce = 0;

module.exports = {
  strToHash: (message) => {
    return web3.utils.sha3(message);
  },

  ethCall: (contract, func, account, params) => {
    return contract.func.call(params, { from: account }, (err, value) => {
      return { error: err, value: value };
    });
  },

  ethSignMessage: (private_key, message) => {
    const msg = web3.utils.sha3(message);
    const wallet = ethWallet.fromPrivateKey(Buffer.from(private_key, 'hex'));
    const signed = ethUtil.ecsign(
      ethUtil.toBuffer(msg),
      wallet.getPrivateKey(),
    );
    const combined = Buffer.concat([
      Buffer.from(signed.r),
      Buffer.from(signed.s),
      Buffer.from([signed.v]),
    ]);
    return combined.toString('hex');
  },

  ethRawTx: (type, data, from, to) => {
    const wallet = ethWallet.fromPrivateKey(
      Buffer.from(Config.SignKeyDefault, 'hex'),
    );
    const gasPrice = web3.utils.toWei(Config.gasDeploy, Config.gasUnit);
    const gasPriceHex = web3.utils.toHex(gasPrice);
    let gasLimitHex;
    if (type === 'contract') {
      gasLimitHex = web3.utils.toHex(Config.gasLimitContract);
    } else {
      gasLimitHex = web3.utils.toHex(Config.gasLimitFunction);
    }

    let nonce = web3.eth.getTransactionCount(Config.accountDefault, 'pending');
    if (currentNonce !== 0 && nonce === currentNonce) {
      nonce++;
    }

    console.log('Current Nonce: ' + currentNonce + ' Nonce: ' + nonce);

    currentNonce = nonce;
    const nonceHex = web3.utils.toHex(nonce);

    const rawTx = {
      nonce: nonceHex,
      gasPrice: gasPriceHex,
      gasLimit: gasLimitHex,
      data: data,
      from: Config.accountDefault,
    };
    if (to !== undefined && to.length !== 0) {
      rawTx.to = to;
    }
    const tx = new Tx(rawTx);
    tx.sign(Buffer(wallet.getPrivateKey(), 'hex'));
    return tx.serialize();
  },

  ethSendRawTransaction: (serialized, callback) => {
    web3.eth.sendRawTransaction(
      '0x' + serialized.toString('hex'),
      (err, hash) => {
        console.log(err, hash);
        callback(err, hash);
      },
    );
  },
};
