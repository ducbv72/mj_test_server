const _ = require('lodash');
const express = require('express');
const common = require('../../../lib/common');
const router = express.Router();
const Config = require('../../../config/ethereumConfig');

router.get(
  '/admin/settings/scheduled/emails',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    res.redirect('/admin/settings/scheduled/emails/0');
  },
);

router.get(
  '/admin/settings/scheduled/send-email/:page',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    res.redirect('/admin/settings/scheduled/emails/0');
  },
);

router.get(
  '/admin/settings/scheduled/emails/:page',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    // get the top results
    const db = req.app.db;
    const page = Number(req.params.page);
    const [results, count] = await Promise.all([
      db.sendEmailToManufacture
        .find({})
        .sort({ updatedAt: -1 })
        .skip(page * Config.productLitmitPerPage)
        .limit(Config.productLitmitPerPage)
        .toArray(),
      db.sendEmailToManufacture.count({}),
    ]);
    const total = count / Config.productLitmitPerPage;
    const array = [];
    for (let i = 0; i < total; i++) {
      array.push(i + '');
    }
    res.render('notification/email_scheduled', {
      title: 'Schedule Email',
      menu: 'notification',
      emails: results,
      total: total,
      page: req.params.page,
      totalPages: array,
      count: count,
      session: req.session,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

// delete product
router.get(
  '/admin/email/delete/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    // remove the article
    db.sendEmailToManufacture.remove(
      { _id: common.getId(req.params.id) },
      {},
      (err, numRemoved) => {
        if (err) {
          console.info(err.stack);
        }
        req.session.message = 'Item successfully deleted';
        req.session.messageType = 'success';
        res.redirect('/admin/settings/scheduled/emails/0');
      },
    );
  },
);

module.exports = router;
