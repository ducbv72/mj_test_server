const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const rimraf = require('rimraf');
const fs = require('fs');
const { Parser } = require('json2csv');
const router = express.Router();
const Config = require('../../../config');
const ethereumConfig = require('../../../config/ethereumConfig');
const uuidv4 = require('uuid').v4;
const schedule = require('node-schedule');
const { shopify } = require('../../../config/shopify');
const {
  checkProductShopify,
  checkProductStoreFront,
} = require('../../../services/shopify');
const csvParser = require('csv-parser');
const formidable = require('formidable');
const { Schedule } = require('../../../lib/common');
const getNft = require('../../../mobile/controllers/nft/getNft');
const encryptedString = require('../../../services/encryptedString');
const { mergeCreepyCuties } = require('../../../services/product');

const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: Config.S3.accessKeyId,
  secretAccessKey: Config.S3.secretAccessKey,
  region: Config.S3UdidRequest.region,
});
const createMetafield = (shopifyId, metafieldName, value) => {
  shopify.metafield
    .create({
      key: metafieldName,
      value: value,
      value_type: 'string',
      namespace: 'information',
      owner_resource: 'product',
      owner_id: shopifyId,
    })
    .then(
      (metafield) => console.log(metafield),
      (err) => console.error(err),
    );
};
// update metafields
const updateMetafield = (shopifyId, metafieldId, value) => {
  const query = `mutation($input: ProductInput!) {
    productUpdate(input: $input) {
      product {
        metafields(first: 100) {
          edges {
            node {
              namespace
              key
              value
            }
          }
        }
      }
    }
  }`;

  const variables = {
    input: {
      id: `gid://shopify/Product/${shopifyId}`,
      metafields: [
        {
          id: metafieldId,
          value: value,
        },
      ],
    },
  };

  shopify.graphql(query, variables);
};
// check metafield available or not, return metafieldId or false
const checkAvailable = async (shopifyId, metafieldName) => {
  const query = `query {
    product(id: "gid://shopify/Product/${shopifyId}") {
      metafield(namespace: "information", key: "${metafieldName}") {
        key
        value
        id
      }
    }
  }`;
  const response = await shopify.graphql(query);
  if (response.product.metafield) {
    return response.product.metafield.id;
  } else return false;
};
function formatDate(oldDate) {
  return oldDate.toString().split('-').reverse().join('-');
}
router.get('/admin/shopify/:id', async (req, res) => {
  const check = await checkProductShopify(req.params.id);
  if (check == true) {
    res.status(200).json({ message: 'found it' });
  } else res.status(404).json({ message: 'not found' });
});
router.get(
  '/admin/product_upcomming',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    res.render('product_upcomming', {
      title: 'Upcomming Products',
      menu: 'product_upcomming',
      session: req.session,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.get(
  '/admin/products/:page',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    const page = Number(req.params.page);
    const [products, count] = await Promise.all([
      db.products
        .aggregate([
          {
            $project: {
              _id: '$_id',
              productTitle: '$productTitle',
              productVariantSku: '$productVariantSku',
              productNew: '$productNew',
              productNotMatchShopify: '$productNotMatchShopify',
              productNotMatchMobile: '$productNotMatchMobile',
              productRecommendation: '$productRecommendation',
              productFeature: '$productFeature',
              productImageUrl: '$productImageUrl',
              productArtist: '$productArtist',
              isShowStockxBanner: '$isShowStockxBanner',
              productTitleOnApp: '$productTitleOnApp',
              enableEditionNumber: '$enableEditionNumber',
              productLaunchVipStatus: '$productLaunchVipStatus',
              productTags: { $split: ['$productTags', ','] },
              productStock: '$productStock',
              totalQrcode: '$totalQrcode',
              completedQrcode: '$completedQrcode',
              pendingQrcode: '$pendingQrcode',
              productSoldOutDate: {
                $cond: [
                  {
                    $eq: ['$productSoldOutStatus', true],
                  },
                  {
                    $concat: [
                      '$productSoldOutDate',
                      ' ',
                      { $ifNull: ['$productSoldOutTime', ''] },
                    ],
                  },
                  null,
                ],
              },
              currentDateTime: new Date(),
              stock_on_hand: '$stock_on_hand',
            },
          },
        ])
        .sort({
          productRecommendation: -1,
          productFeature: -1,
          productAddedDate: -1,
        })
        .skip(page * ethereumConfig.productLitmitPerPage)
        .limit(ethereumConfig.productLitmitPerPage)
        .toArray(),
      db.products.count({}),
    ]);

    const total = count / ethereumConfig.productLitmitPerPage;
    const array = [];
    for (let i = 0; i < total; i++) {
      array.push(i);
    }
    res.render('products', {
      title: 'Cart',
      menu: 'product',
      top_results: products,
      total: total,
      page: page,
      totalPages: array,
      count: count,
      session: req.session,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.get(
  '/admin/products/',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    res.redirect('/admin/products/0');
  },
);

router.get(
  '/admin/products/filter/:search',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    const searchTerm = req.params.search;
    const searchTermEEscape = req.params.search.replace(
      /[.*+?^${}()|[\]\\]/g,
      '\\$&',
    );
    // we search on the lunr indexes
    const [products] = await Promise.all([
      db.products
        .aggregate([
          {
            $match: {
              $or: [
                { productTitle: new RegExp(searchTermEEscape, 'i') },
                { productVariantSku: new RegExp(searchTermEEscape, 'i') },
                { productArtist: new RegExp(searchTermEEscape, 'i') },
              ],
            },
          },
          {
            $project: {
              _id: '$_id',
              productTitle: '$productTitle',
              productVariantSku: '$productVariantSku',
              productNew: '$productNew',
              productNotMatchShopify: '$productNotMatchShopify',
              productNotMatchMobile: '$productNotMatchMobile',
              productRecommendation: '$productRecommendation',
              productFeature: '$productFeature',
              productImageUrl: '$productImageUrl',
              productArtist: '$productArtist',
              isShowStockxBanner: '$isShowStockxBanner',
              productTitleOnApp: '$productTitleOnApp',
              enableEditionNumber: '$enableEditionNumber',
              productLaunchVipStatus: '$productLaunchVipStatus',
              productTags: { $split: ['$productTags', ','] },
              productStock: '$productStock',
              totalQrcode: '$totalQrcode',
              completedQrcode: '$completedQrcode',
              pendingQrcode: '$pendingQrcode',
              productSoldOutDate: {
                $cond: [
                  {
                    $eq: ['$productSoldOutStatus', true],
                  },
                  {
                    $concat: [
                      '$productSoldOutDate',
                      ' ',
                      { $ifNull: ['$productSoldOutTime', ''] },
                    ],
                  },
                  null,
                ],
              },
            },
          },
        ])
        .sort({
          productRecommendation: -1,
          productFeature: -1,
          productAddedDate: -1,
        })
        .toArray(),
    ]);

    res.render('products', {
      title: 'Results',
      menu: 'product',
      top_results: products,
      admin: true,
      config: req.app.config,
      session: req.session,
      searchTerm: searchTerm,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
      keySearch: searchTerm,
    });
  },
);

router.post(
  '/admin/product/sync',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    shopify.product.get(req.body.id).then((product) => {
      if (product.id.toString()) {
        const variants = product.variants;
        variants.forEach(async (item) => {
          if (item.sku) {
            const [count] = await Promise.all([
              db.products.count({ productVariantSku: item.sku }),
            ]);

            let doc = {
              productPermalink: item.id.toString(),
              shopifyProductId: product.id.toString(),
              productUrl: product.handle,
              udid: '',
              productModelNumber: '',
              productVariantId: item.id.toString(),
              productVariantSku: item.sku.toString(),
              productTitle: product.title + ' - ' + item.title,
              productPrice: item.price,
              productDescription: common.cleanHtml(product.body_html),
              productPublished: 1,
              productTags: product.tags,
              productOptions: '',
              productComment: 1,
              productAddedDate: new Date(),
              productStock: item.inventory_quantity,
              productNew: 0,
              productTrending: 0,
              enableEditionNumber: 0,
              productFeature: 0,
              productRecommendation: 0,
              productPublishedAt: product.published_at,
              productImageUrl: product.image
                ? common.cleanHtml(product.image.src)
                : '/images/default-image.png',
            };
            //merge CreepyCuties
            doc = await mergeCreepyCuties(doc);

            if (count === 0) {
              const [newDoc] = await Promise.all([db.products.insert(doc)]);
            } else {
              await Promise.all([
                db.products.updateOne(
                  { shopifyProductId: req.body.id },
                  { $set: doc },
                ),
              ]);
            }
          }
        });
        req.session.message = 'Successfully saved';
        req.session.messageType = 'success';
        res.redirect('/admin/products/0');
      }
    });
  },
);

// insert form
router.get(
  '/admin/product/sync',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('product_sync', {
      title: 'Sync product',
      menu: 'product',
      session: req.session,
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
    });
  },
);

router.get(
  '/admin/product/new',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('product_new', {
      title: 'New product',
      menu: 'product',
      session: req.session,
      productTitle: common.clearSessionValue(req.session, 'productTitle'),
      productTitleOnApp: common.clearSessionValue(
        req.session,
        'productTitleOnApp',
      ),
      shopifyProductId: common.clearSessionValue(
        req.session,
        'shopifyProductId',
      ),
      productVariantId: common.clearSessionValue(
        req.session,
        'productVariantId',
      ),
      productVariantSku: common.clearSessionValue(
        req.session,
        'productVariantSku',
      ),
      udid: common.clearSessionValue(req.session, 'productUDID'),
      productModelNumber: common.clearSessionValue(
        req.session,
        'productModelNumber',
      ),
      productDescription: common.clearSessionValue(
        req.session,
        'productDescription',
      ),
      productSpecification: common.clearSessionValue(
        req.session,
        'productSpecification',
      ),
      productPrice: common.clearSessionValue(req.session, 'productPrice'),
      productPermalink: common.clearSessionValue(
        req.session,
        'productPermalink',
      ),
      productNew: common.clearSessionValue(req.session, 'productNew'),
      productTrending: common.clearSessionValue(req.session, 'productTrending'),
      enableEditionNumber: common.clearSessionValue(
        req.session,
        'enableEditionNumber',
      ),
      productFeature: common.clearSessionValue(req.session, 'productFeature'),
      productImageUrl: common.clearSessionValue(req.session, 'productImageUrl'),
      productRecommendation: common.clearSessionValue(
        req.session,
        'productRecommendation',
      ),
      productIsHomePopup: common.clearSessionValue(
        req.session,
        'productIsHomePopup',
      ),
      productPublishedAt: common.clearSessionValue(
        req.session,
        'productPublishedAt',
      ),
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
      zendeskUrl: common.clearSessionValue(req.session, 'zendeskUrl'),
    });
  },
);

// insert new product form action
router.post(
  '/admin/product/insert',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    let doc = {
      productPermalink: uuidv4(),
      udid: req.body.frmProductUDID,
      productModelNumber: common.cleanHtml(req.body.frmProductModel),
      productVariantId: common.cleanHtml(req.body.frmProductVariantId),
      productVariantSku: common.cleanHtml(req.body.frmProductVariantSku),
      productTitle: req.body.frmProductTitle,
      shopifyProductId: req.body.frmShopifyProductId,
      productPrice: req.body.frmProductPrice,
      productDescription: common.cleanHtml(req.body.frmProductDescription),
      productSpecification: common.cleanHtml(req.body.frmProductSpecification),
      productPublished: new Date(),
      productTitleOnApp: req.body.frmProductTitleOnApp,
      Permalink: new Date().getTime().toString(),
      productTags: '',
      productOptions: common.cleanHtml(''),
      productComment: common.checkboxBool(req.body.frmProductComment),
      productAddedDate: new Date(),
      productStock: req.body.frmProductStock
        ? parseInt(req.body.frmProductStock)
        : null,
      productNew: common.checkboxBool(req.body.frmProductIsNew),
      productRecommendation: common.checkboxBool(req.body.frmProductIsNew),
      productTrending: common.checkboxBool(req.body.frmProductIsTrending),
      enableEditionNumber: common.checkboxBool(req.body.frmenableEditionNumber),
      productFeature: common.checkboxBool(req.body.frmProductIsFeature),
      productIsHomePopup: common.checkboxBool(req.body.frmProductIsHomePopup),
      productArtist: req.body.frmProductArtist,
      productPublishedAt: true,
      productLaunchDate: req.body.frmProductLaunchDate,
      productStore: req.body.frmProductStore,
      productLaunchTime: req.body.frmProductLaunchTime,
      productLaunchColor: '#FFCD00',
      productLaunchStatus: req.body.frmProductLaunchStatus,
      productLaunchVipStatus: req.body.frmProductLaunchVipStatus,
      productSoldOutDate: req.body.frmProductSoldOutDate,
      productSoldOutTime: req.body.frmProductSoldOutTime,
      productSoldOutStatus: req.body.frmProductSoldOutStatus,
      stockxLink: req.body.frmProductStockXUrl
        ? req.body.frmProductStockXUrl.toLowerCase()
        : '',
      stockxSlug: req.body.frmProductStockXUrl
        ? req.body.frmProductStockXUrl
            .toLowerCase()
            .replace('https://stockx.com/', '')
        : '',
      productOrder: req.body.frmProductOrder,
      isShowStockxBanner: common.checkboxBool(req.body.frmIsShowStockxBanner),
      zendeskUrl: req.body.frmZendeskUrl,
      productCrossSellStatus: common.checkboxBool(
        req.body.frmProductCrossSellStatus,
      ),
      productCrossSellContent: req.body.frmProductCrossSellContent,
      productCrossSellContentText: req.body.frmProductCrossSellContentText,
    };

    //merge CreepyCuties
    doc = await mergeCreepyCuties(doc);
    // verify sku
    const [product1] = await Promise.all([
      db.products.findOne({
        $and: [
          {
            productVariantSku: common.cleanHtml(req.body.frmProductVariantSku),
          },
        ],
      }),
    ]);

    if (product1) {
      console.log(colors.red('This SKU has been used please enter other sku.'));
      // keep the current stuff
      req.session.productTitle = req.body.frmProductTitle;
      req.session.shopifyProductId = req.body.frmShopifyProductId;
      req.session.productVariantId = req.body.frmProductVariantId;
      req.session.productVariantSku = req.body.frmProductVariantSku;
      req.session.udid = req.body.frmProductUDID;
      req.session.productDescription = req.body.frmProductDescription;
      req.session.productSpecification = req.body.frmProductSpecification;
      req.session.productPrice = req.body.frmProductPrice;
      // req.session.productPermalink = req.body.frmProductPermalink;
      req.session.productOptJson = req.body.productOptJson;
      req.session.productComment = common.checkboxBool(
        req.body.frmProductComment,
      );
      req.session.productNew = common.checkboxBool(req.body.frmProductIsNew);
      req.session.enableEditionNumber = common.checkboxBool(
        req.body.frmenableEditionNumber,
      );
      req.session.productTrending = common.checkboxBool(
        req.body.frmProductIsTrending,
      );
      req.session.productFeature = common.checkboxBool(
        req.body.frmProductIsFeature,
      );
      req.session.productRecommendation = common.checkboxBool(
        req.body.frmProductIsNew,
      );
      req.session.productIsHomePopup = common.checkboxBool(
        req.body.frmProductIsHomePopup,
      );
      req.session.productPublishedAt = req.body.productPublishedAt;
      req.session.productStock = req.body.frmProductStock
        ? parseInt(req.body.frmProductStock)
        : null;
      req.session.productArtist = req.body.frmProductArtist;
      req.session.productTitleOnApp = common.cleanHtml(
        req.body.frmProductTitleOnApp,
      );
      req.session.productLaunchDate = req.body.frmProductLaunchDate;
      req.session.productStore = req.body.frmProductStore;
      req.session.productLaunchTime = req.body.frmProductLaunchTime;
      req.session.productLaunchColor = '#FFCD00';
      req.session.productLaunchStatus = req.body.frmProductLaunchStatus;
      req.session.productLaunchVipStatus = req.body.frmProductLaunchVipStatus;
      req.session.productSoldOutDate = req.body.frmProductSoldOutDate;
      req.session.productSoldOutTime = req.body.frmProductSoldOutTime;
      req.session.productSoldOutStatus = req.body.frmProductSoldOutStatus;
      req.session.productStockUrl = req.body.frmProductStockXUrl;
      req.session.productOrder = req.body.frmProductOrder;
      req.session.isShowStockxBanner = common.checkboxBool(
        req.body.frmIsShowStockxBanner,
      );
      req.session.message = 'This SKU has been used please enter other sku.';
      req.session.messageType = 'danger';
      req.session.productCrossSellStatus = common.checkboxBool(
        req.body.frmProductCrossSellStatus,
      );
      req.session.productCrossSellContent = req.body.frmProductCrossSellContent;
      req.session.productCrossSellContentText =
        req.body.frmProductCrossSellContentText;

      // redirect to insert
      res.redirect('/admin/product/new');
    } else {
      await common.saveToActionLog(db, req.user, 'insert_product', doc);

      let checkShopify = true;
      if (req.body.frmShopifyProductId) {
        checkShopify = await checkProductShopify(req.body.frmShopifyProductId);
      } else {
        doc.productNotMatchShopify = true;
      }

      if (!checkShopify) {
        req.session.productTitle = req.body.frmProductTitle;
        req.session.shopifyProductId = req.body.frmShopifyProductId;
        req.session.productVariantId = req.body.frmProductVariantId;
        req.session.productVariantSku = req.body.frmProductVariantSku;
        req.session.udid = req.body.frmProductUDID;
        req.session.productDescription = req.body.frmProductDescription;
        req.session.productSpecification = req.body.frmProductSpecification;
        req.session.productPrice = req.body.frmProductPrice;
        // req.session.productPermalink = req.body.frmProductPermalink;
        req.session.productOptJson = req.body.productOptJson;
        req.session.productComment = common.checkboxBool(
          req.body.frmProductComment,
        );
        req.session.productNew = common.checkboxBool(req.body.frmProductIsNew);
        req.session.enableEditionNumber = common.checkboxBool(
          req.body.frmenableEditionNumber,
        );
        req.session.productTrending = common.checkboxBool(
          req.body.frmProductIsTrending,
        );
        req.session.productFeature = common.checkboxBool(
          req.body.frmProductIsFeature,
        );
        req.session.productRecommendation = common.checkboxBool(
          req.body.frmProductIsNew,
        );
        req.session.productIsHomePopup = common.checkboxBool(
          req.body.frmProductIsHomePopup,
        );
        req.session.productPublishedAt = req.body.productPublishedAt;
        req.session.productStock = req.body.frmProductStock
          ? parseInt(req.body.frmProductStock)
          : null;
        req.session.productArtist = req.body.frmProductArtist;
        req.session.productTitleOnApp = common.cleanHtml(
          req.body.frmProductTitleOnApp,
        );
        req.session.productLaunchDate = req.body.frmProductLaunchDate;
        req.session.productStore = req.body.frmProductStore;
        req.session.productLaunchTime = req.body.frmProductLaunchTime;
        req.session.productLaunchColor = '#FFCD00';
        req.session.productLaunchStatus = req.body.frmProductLaunchStatus;
        req.session.productLaunchVipStatus = req.body.frmProductLaunchVipStatus;
        req.session.productSoldOutDate = req.body.frmProductSoldOutDate;
        req.session.productSoldOutTime = req.body.frmProductSoldOutTime;
        req.session.productSoldOutStatus = req.body.frmProductSoldOutStatus;
        req.session.productStockUrl = req.body.frmProductStockXUrl;
        req.session.productOrder = req.body.frmProductOrder;
        req.session.isShowStockxBanner = common.checkboxBool(
          req.body.frmIsShowStockxBanner,
        );
        req.session.message =
          'Save failed.  There is no matching product on Shopify.';
        req.session.messageType = 'danger';
        // redirect to insert
        res.redirect('/admin/product/new');
      } else {
        db.products.insert(doc, (err, newDoc) => {
          if (err) {
            console.log(colors.red('Error inserting document: ' + err));
            // keep the current stuff
            req.session.productTitle = req.body.frmProductTitle;
            req.session.shopifyProductId = req.body.frmShopifyProductId;
            req.session.productVariantId = req.body.frmProductVariantId;
            req.session.productVariantSku = req.body.frmProductVariantSku;
            req.session.udid = req.body.frmProductUDID;
            req.session.productDescription = req.body.frmProductDescription;
            req.session.productSpecification = req.body.frmProductSpecification;
            req.session.productPrice = req.body.frmProductPrice;
            // req.session.productPermalink = req.body.frmProductPermalink;
            req.session.productOptJson = req.body.productOptJson;
            req.session.productComment = common.checkboxBool(
              req.body.frmProductComment,
            );
            req.session.productNew = common.checkboxBool(
              req.body.frmProductIsNew,
            );
            req.session.productTrending = common.checkboxBool(
              req.body.frmProductIsTrending,
            );
            req.session.enableEditionNumber = common.checkboxBool(
              req.body.frmenableEditionNumber,
            );
            req.session.productFeature = common.checkboxBool(
              req.body.frmProductIsFeature,
            );
            req.session.productRecommendation = common.checkboxBool(
              req.body.frmProductIsNew,
            );
            req.session.productIsHomePopup = common.checkboxBool(
              req.body.frmProductIsHomePopup,
            );
            req.session.productPublishedAt = req.body.productPublishedAt;
            req.session.productStock = req.body.frmProductStock
              ? parseInt(req.body.frmProductStock)
              : null;
            req.session.productArtist = req.body.frmProductArtist;
            req.session.productTitleOnApp = common.cleanHtml(
              req.body.frmProductTitleOnApp,
            );
            req.session.productLaunchDate = req.body.frmProductLaunchDate;
            req.session.productStore = req.body.frmProductStore;
            req.session.productLaunchTime = req.body.frmProductLaunchTime;
            req.session.productLaunchColor = '#FFCD00';
            req.session.productLaunchStatus = req.body.frmProductLaunchStatus;
            req.session.productLaunchVipStatus =
              req.body.frmProductLaunchVipStatus;
            req.session.productSoldOutDate = req.body.frmProductSoldOutDate;
            req.session.productSoldOutTime = req.body.frmProductSoldOutTime;
            req.session.productSoldOutStatus = req.body.frmProductSoldOutStatus;
            req.session.productStockxUrl = req.body.frmProductStockXUrl;
            req.session.isShowStockxBanner = common.checkboxBool(
              req.body.frmIsShowStockxBanner,
            );
            req.session.productOrder = req.body.frmProductOrder;
            req.session.message = 'Error: Inserting product';
            req.session.messageType = 'danger';

            // redirect to insert
            res.redirect('/admin/product/new');
          } else {
            // get the new ID
            const newId = newDoc.insertedIds[0];
            const shopifyId = req.body.frmShopifyProductId;
            if (req.body.frmProductLaunchStatus == 'true') {
              -createMetafield(
                shopifyId,
                'launch_countdown',
                req.body.fullDateLaunch,
              );

              //  create launch countdown vip
              if (req.body.frmProductLaunchVipStatus == 'true') {
                createMetafield(shopifyId, 'launch_vip_access', 'yes');
                createMetafield(
                  shopifyId,
                  'launch_countdown_vip',
                  req.body.fullDateLaunchVip,
                );
              }
              if (req.body.frmProductLaunchVipStatus == 'false') {
                const fullDay = '01-01-1977 00:00';
                createMetafield(shopifyId, 'launch_vip_access', 'no');
                createMetafield(shopifyId, 'launch_countdown_vip', fullDay);
              }
            }
            if (req.body.frmProductLaunchStatus == 'false') {
              const fullDay = '01-01-1977 00:00';

              createMetafield(shopifyId, 'launch_countdown', fullDay);
              createMetafield(shopifyId, 'launch_vip_access', 'no');
              createMetafield(shopifyId, 'launch_countdown_vip', fullDay);
            }
            // create countdown
            if (req.body.frmProductSoldOutStatus == 'true') {
              createMetafield(shopifyId, 'countdown', req.body.fullDateSoldOut);
            }
            if (req.body.frmProductSoldOutStatus == 'false') {
              const fullDay = '01-01-1977 00:00';

              createMetafield(shopifyId, 'countdown', fullDay);
            }
            // add to lunr index
            // common.indexProducts(req.app).then(() => {
            req.session.message = 'New product successfully created';
            req.session.messageType = 'success';

            // redirect to new doc
            res.redirect('/admin/product/edit/' + newId);
            //});
          }
        });
      }
    }
  },
);

// render the editor
// render the editor
router.get(
  '/admin/product/edit/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const product = await db.products.findOne({
      _id: common.getId(req.params.id),
    });

    const countEditionNumber = await encryptedString.countEditionNumber(
      db,
      product.productVariantSku,
    );
    const options = {};
    res.render('product_edit', {
      title: 'Edit product',
      menu: 'product',
      result: product,
      countEditionNumber: countEditionNumber,
      options: options,
      admin: true,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      config: req.app.config,
      editor: true,
      helpers: req.handlebars.helpers,
    });
  },
);

// Update an existing product form action
router.post(
  '/admin/product/update',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const product = await db.products.findOne({
      _id: common.getId(req.body.frmProductId),
    });

    let productDoc = {
      productTitle: req.body.frmProductTitle,
      shopifyProductId: req.body.frmShopifyProductId,
      productUrl: product.productUrl,
      udid: common.cleanHtml(req.body.frmProductUDID),
      productModelNumber: common.cleanHtml(req.body.frmProductModel),
      productVariantId: common.cleanHtml(req.body.frmProductVariantId),
      productVariantSku: common.cleanHtml(req.body.frmProductVariantSku),
      productDescription: common.cleanHtml(req.body.frmProductDescription),
      productSpecification: common.cleanHtml(req.body.frmProductSpecification),
      productPublished: req.body.frmProductPublished,
      productPrice: req.body.frmProductPrice,
      productPermalink: req.body.frmProductPermalink,
      productOrder: req.body.frmProductOrder,
      productTags: common.cleanHtml(req.body.frmProductTags),
      productOptions: common.cleanHtml(req.body.productOptJson),
      productComment: common.checkboxBool(req.body.frmProductComment),
      productStock: req.body.frmProductStock
        ? parseInt(req.body.frmProductStock)
        : null,
      productNew: common.checkboxBool(req.body.frmProductIsNew),
      productTrending: common.checkboxBool(req.body.frmProductIsTrending),
      enableEditionNumber: common.checkboxBool(req.body.frmenableEditionNumber),
      productFeature: common.checkboxBool(req.body.frmProductIsFeature),
      productRecommendation: common.checkboxBool(req.body.frmProductIsNew),
      productIsHomePopup: common.checkboxBool(req.body.frmProductIsHomePopup),
      productImageUrl: product.productImageUrl,
      productImageUrlS3: product.productImageUrlS3,
      productPublishedAt: req.body.productPublishedAt,
      productBackgroundImage: product.productBackgroundImage,
      productBackgroundImageS3: product.productBackgroundImageS3,
      productLaunchDate: req.body.frmProductLaunchDate,
      productStore: req.body.frmProductStore,
      productLaunchTime: req.body.frmProductLaunchTime,
      productLaunchColor: '#FFCD00',
      productLaunchStatus: req.body.frmProductLaunchStatus,
      productLaunchVipStatus: req.body.frmProductLaunchVipStatus,
      productSoldOutDate: req.body.frmProductSoldOutDate,
      productSoldOutTime: req.body.frmProductSoldOutTime,
      productSoldOutStatus: req.body.frmProductSoldOutStatus,
      productArtist: req.body.frmProductArtist,
      productTitleOnApp: common.cleanHtml(req.body.frmProductTitleOnApp),
      stockxLink: req.body.frmProductStockXUrl
        ? req.body.frmProductStockXUrl.toLowerCase()
        : '',
      stockxSlug: req.body.frmProductStockXUrl
        ? req.body.frmProductStockXUrl
            .toLowerCase()
            .replace('https://stockx.com/', '')
        : '',
      isShowStockxBanner: common.checkboxBool(req.body.frmIsShowStockxBanner),
      zendeskUrl: req.body.frmZendeskUrl,
      shopifyData: product.shopifyData ? product.shopifyData : '',
      productCrossSellStatus: common.checkboxBool(
        req.body.frmProductCrossSellStatus,
      ),
      productCrossSellContent: req.body.frmProductCrossSellContent,
      productCrossSellContentText: req.body.frmProductCrossSellContentText,
    };

    //Save data for Assets File
    if (req.body.imageAddingData) {
      const imageAssetsFile = JSON.parse(req.body.imageAddingData);
      productDoc.productAssetsFile = [];
      imageAssetsFile.forEach(function (e) {
        const obj = {
          fileId: e.fileId,
          assetsFileName: e.assetsFileName,
          assetsFileUrl: e.assetsFileUrl,
          allowDownload: e.allowDownload,
          assetsFileThumbUrl: e.assetsFileThumbUrl,
        };
        productDoc.productAssetsFile.push(obj);
      });
    }

    //merge CreepyCuties
    productDoc = await mergeCreepyCuties(productDoc);

    // check this product on shopify if available allow update
    let checkShopify = false;
    let checkStoreFront = false;
    if (req.body.frmShopifyProductId) {
      [checkShopify, checkStoreFront] = await Promise.all([
        checkProductShopify(req.body.frmShopifyProductId),
        checkProductStoreFront(req.body.frmShopifyProductId),
      ]);
    }
    if (
      (!checkStoreFront || !checkShopify) &&
      (common.checkboxBool(req.body.frmProductIsNew) ||
        common.checkboxBool(req.body.frmProductIsFeature))
    ) {
      req.session.message =
        'Save failed.  There is no matching product on Shopify.  Check the Shopify Product ID';
      req.session.messageType = 'danger';
      res.redirect('/admin/product/edit/' + req.body.frmProductId);
    } else {
      await common.saveToActionLog(db, req.user, 'update_product', productDoc);
      const shopifyId = req.body.frmShopifyProductId;
      const [
        checkLaunchCountdown,
        checkLaunchCountdownVip,
        checkCountdown,
        checkLaunchVipAccess,
      ] = await Promise.all([
        checkAvailable(shopifyId, 'launch_countdown'),
        checkAvailable(shopifyId, 'launch_countdown_vip'),
        checkAvailable(shopifyId, 'countdown'),
        checkAvailable(shopifyId, 'launch_vip_access'),
      ]);
      // check and update launch countdown
      if (req.body.frmProductLaunchStatus == 'true') {
        if (checkLaunchCountdown) {
          updateMetafield(
            shopifyId,
            checkLaunchCountdown,
            req.body.fullDateLaunch,
          );
        } else {
          createMetafield(
            shopifyId,
            'launch_countdown',
            req.body.fullDateLaunch,
          );
        }
        // check and update launch countdown vip
        if (req.body.frmProductLaunchVipStatus == 'true') {
          if (checkLaunchVipAccess) {
            updateMetafield(shopifyId, checkLaunchVipAccess, 'yes');
          } else {
            createMetafield(shopifyId, 'launch_vip_access', 'yes');
          }
          if (checkLaunchCountdownVip) {
            updateMetafield(
              shopifyId,
              checkLaunchCountdownVip,
              req.body.fullDateLaunchVip,
            );
          } else {
            createMetafield(
              shopifyId,
              'launch_countdown_vip',
              req.body.fullDateLaunchVip,
            );
          }
        }
        if (req.body.frmProductLaunchVipStatus == 'false') {
          if (checkLaunchVipAccess) {
            updateMetafield(shopifyId, checkLaunchVipAccess, 'no');
          } else {
            createMetafield(shopifyId, 'launch_vip_access', 'no');
          }
          const fullDay = '01-01-1977 00:00';
          if (checkLaunchCountdownVip) {
            updateMetafield(shopifyId, checkLaunchCountdownVip, fullDay);
          } else {
            createMetafield(shopifyId, 'launch_countdown_vip', fullDay);
          }
        }
      }
      if (req.body.frmProductLaunchStatus == 'false') {
        const fullDay = '01-01-1977 00:00';
        if (checkLaunchCountdown) {
          updateMetafield(shopifyId, checkLaunchCountdown, fullDay);
        } else {
          createMetafield(shopifyId, 'launch_countdown', fullDay);
        }

        // take no in vip early and update it to default date
        if (checkLaunchVipAccess) {
          updateMetafield(shopifyId, checkLaunchVipAccess, 'no');
        } else {
          createMetafield(shopifyId, 'launch_vip_access', 'no');
        }
        if (checkLaunchCountdownVip) {
          updateMetafield(shopifyId, checkLaunchCountdownVip, fullDay);
        } else {
          createMetafield(shopifyId, 'launch_countdown_vip', fullDay);
        }
      }

      // check and update countdown
      if (req.body.frmProductSoldOutStatus == 'true') {
        if (checkCountdown) {
          updateMetafield(shopifyId, checkCountdown, req.body.fullDateSoldOut);
        } else {
          createMetafield(shopifyId, 'countdown', req.body.fullDateSoldOut);
        }
      }
      if (req.body.frmProductSoldOutStatus == 'false') {
        const fullDay = '01-01-1977 00:00';
        if (checkCountdown) {
          updateMetafield(shopifyId, checkCountdown, fullDay);
        } else {
          createMetafield(shopifyId, 'countdown', fullDay);
        }
      }

      //update current product
      await db.products.update(
        { _id: common.getId(req.body.frmProductId) },
        { $set: productDoc },
        { multi: false },
      );

      //update product and all variant
      await db.products.update(
        { shopifyProductId: req.body.frmShopifyProductId },
        {
          $set: {
            productLaunchDate: req.body.frmProductLaunchDate,
            productLaunchTime: req.body.frmProductLaunchTime,
            productLaunchColor: '#FFCD00',
            productLaunchStatus: req.body.frmProductLaunchStatus,
            productLaunchVipStatus: req.body.frmProductLaunchVipStatus,
            productSoldOutDate: req.body.frmProductSoldOutDate,
            productSoldOutTime: req.body.frmProductSoldOutTime,
            productSoldOutStatus: req.body.frmProductSoldOutStatus,
            productNotMatchShopify: !checkShopify,
            productNotMatchMobile: !checkStoreFront,
          },
        },
        { multi: true },
      );
      req.session.message = 'Successfully saved';
      req.session.messageType = 'success';
      res.redirect('/admin/product/edit/' + req.body.frmProductId);
    }
  },
);

// delete product
router.get(
  '/admin/product/delete/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    db.products.remove(
      { _id: common.getId(req.params.id) },
      {},
      (err, numRemoved) => {
        if (err) {
          console.info(err.stack);
        }
        // delete any images and folder
        rimraf('public/uploads/' + req.params.id, (err) => {
          if (err) {
            console.info(err.stack);
          }

          // remove the index
          common.indexProducts(req.app).then(() => {
            // redirect home
            req.session.message = 'Product successfully deleted';
            req.session.messageType = 'success';
            res.redirect('/admin/products/0');
          });
        });
      },
    );
  },
);

// update the published state based on an ajax call from the frontend
router.post(
  '/admin/product/published_state',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    db.products.update(
      { _id: common.getId(req.body.id) },
      { $set: { productPublished: req.body.state } },
      { multi: false },
      (err, numReplaced) => {
        if (err) {
          console.error(
            colors.red('Failed to update the published state: ' + err),
          );
          res.status(400).json('Published state not updated');
        } else {
          res.status(200).json('Published state updated');
        }
      },
    );
  },
);

// set as main product image
router.post(
  '/admin/product/setasmainimage',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    // update the productImage to the db
    db.products.update(
      { _id: common.getId(req.body.product_id) },
      { $set: { productImage: req.body.productImage } },
      { multi: false },
      (err, numReplaced) => {
        if (err) {
          res.status(400).json({
            message: 'Unable to set as main image. Please try again.',
          });
        } else {
          res.status(200).json({ message: 'Main image successfully set' });
        }
      },
    );
  },
);

router.get('/admin/products/list', (req, res, next) => {
  const db = req.app.db;
  // get the top results
  db.products
    .find({})
    .sort({ productAddedDate: -1 })
    .toArray((err, topResults) => {
      if (err) {
        res.status(400).json({ message: 'not found', data: [] });
      }
      res.status(200).json({ message: 'ok', data: topResults });
    });
});

router.get(
  '/admin/product/pull-product-shopify/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    db.products.findOne(
      { shopifyProductId: req.params.id },
      (err, productdb) => {
        if (err || !productdb) {
          console.error(colors.red('Product not found'));
          req.session.message = 'Product not found';
          req.session.messageType = 'danger';
          res.redirect('/admin/products/0');
        }
        shopify.product.get(req.params.id).then((product) => {
          if (product.id.toString()) {
            const variants = product.variants;
            variants.forEach(async (item) => {
              const [productSku] = await Promise.all([
                db.products.findOne({ productVariantSku: item.sku }),
              ]);
              if (productdb) {
                const doc = {
                  productPermalink: item.id.toString(),
                  shopifyProductId: product.id.toString(),
                  productUrl: product.handle,
                  udid: '',
                  productModelNumber: '',
                  productVariantId: item.id.toString(),
                  productVariantSku: item.sku.toString(),
                  productTitle: product.title + ' - ' + item.title,
                  productPrice: item.price,
                  productDescription: common.cleanHtml(product.body_html),
                  productPublished: 1,
                  productTags: product.tags,
                  productOptions: '',
                  productComment: 1,
                  productAddedDate: new Date(),
                  productStock: item.inventory_quantity,
                  productNew: 0,
                  productTrending: 0,
                  enableEditionNumber: 0,
                  productFeature: 0,
                  productRecommendation: 0,
                  productPublishedAt: product.published_at,
                  productImageUrl: product.image
                    ? common.cleanHtml(product.image.src)
                    : '/images/default-image.png',
                  productBackgroundImage: productSku.productBackgroundImage,
                  productBackgroundImageS3: productSku.productBackgroundImageS3,
                  productOrder: productSku.productOrder,
                  productTitleOnApp: productSku.productTitleOnApp,
                  productArtist: productSku.productArtist,
                  productLaunchDate: productSku.productLaunchDate,
                  productStore: productSku.productStore,
                  productLaunchTime: productSku.productLaunchTime,
                  productLaunchColor: productSku.productLaunchColor,
                  productLaunchStatus: productSku.productLaunchStatus,
                  productLaunchVipStatus: productSku.productLaunchVipStatus,
                  productSoldOutDate: productSku.productSoldOutDate,
                  productSoldOutTime: productSku.productSoldOutTime,
                  productSoldOutStatus: productSku.productSoldOutStatus,
                  productSpecification: productSku.productSpecification,
                  isShowStockxBanner: productSku.isShowStockxBanner,
                  stockxLink: productSku.stockxLink,
                  stockxSlug: productSku.stockxSlug,
                };
                const [newDoc] = await Promise.all([
                  db.products.update(
                    { shopifyProductId: product.id.toString() },
                    {
                      $set: doc,
                    },
                    { multi: false },
                  ),
                ]);
              } else {
                let doc = {
                  productPermalink: item.id.toString(),
                  shopifyProductId: product.id.toString(),
                  productUrl: product.handle,
                  udid: '',
                  productModelNumber: '',
                  productVariantId: item.id.toString(),
                  productVariantSku: item.sku.toString(),
                  productTitle: product.title + ' - ' + item.title,
                  productPrice: item.price,
                  productDescription: common.cleanHtml(product.body_html),
                  productPublished: 1,
                  productTags: product.tags,
                  productOptions: '',
                  productComment: 1,
                  productAddedDate: new Date(),
                  productStock: item.inventory_quantity,
                  productNew: 0,
                  productTrending: 0,
                  enableEditionNumber: 0,
                  productFeature: 0,
                  productRecommendation: 0,
                  isShowStockxBanner: 0,
                  productPublishedAt: product.published_at,
                  productImageUrl: product.image
                    ? common.cleanHtml(product.image.src)
                    : '/images/default-image.png',
                };

                //merge CreepyCuties
                doc = await mergeCreepyCuties(doc);

                const [newDoc] = await Promise.all([db.products.insert(doc)]);
              }
            });
            req.session.message = 'Successfully saved';
            req.session.messageType = 'success';
            res.redirect('/admin/products/0');
            console.log(
              'save product: ' + product.id.toString() + ' successfully',
            );
            // let productDoc = {
            //     productPermalink: product.id.toString(),
            //     shopifyProductId: product.id.toString(),
            //     udid: '',
            //     productModelNumber: product.id.toString(),
            //     productVariantId: product.variants[0].id.toString(),
            //     productVariantSku: product.variants[0].sku.toString(),
            //     productTitle: (product.title),
            //     productPrice: product.variants[0].price,
            //     productDescription: common.cleanHtml(product.body_html),
            //     productPublished: 1,
            //     productTags: product.tags,
            //     productOptions: '',
            //     productComment: 1,
            //     productAddedDate: new Date(),
            //     productStock: product.variants.inventory_quantity,
            //     productNew: productdb.productNew,
            //     productTrending: productdb.productTrending,
            //     productFeature: productdb.productFeature,
            //     productRecommendation: productdb.productRecommendation,
            //     productPublishedAt: product.published_at,
            //     productImageUrl: product.image ? common.cleanHtml(product.image.src) : '/images/default-image.png'
            // };
            //
            // db.products.update({shopifyProductId: product.id.toString()}, {$set: productDoc}, {}, (err, numReplaced) => {
            //     if(err){
            //         console.error(colors.red('Failed to save product: ' + err));
            //         req.session.message = 'Failed to save. Please try again';
            //         req.session.messageType = 'danger';
            //         res.redirect('/admin/products');
            //     }else{
            //         // Update the index
            //         req.session.message = 'Successfully saved';
            //         req.session.messageType = 'success';
            //         res.redirect('/admin/products/');
            //     }
            // });
          }
        });
      },
    );
  },
);

router.get(
  '/admin/product/toogle/:id/:property/:value',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const id = req.params.id;
    const property = req.params.property;
    let value = req.params.value;
    if (value === 'false') {
      value = true;
    } else {
      value = false;
    }

    if (property === 'productRecommendation') {
      db.products.update(
        { productVariantId: id },
        { $set: { productRecommendation: value } },
        {},
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed to save product: ' + err));
            req.session.message = 'Failed to save. Please try again';
            req.session.messageType = 'danger';
            res.redirect('/admin/products/0');
          } else {
            // Update the index
            req.session.message = 'Successfully saved';
            req.session.messageType = 'success';
            res.redirect('/admin/products/0');
          }
        },
      );
    } else if (property === 'productFeature') {
      db.products.update(
        { productVariantId: id },
        { $set: { productFeature: value } },
        {},
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed to save product: ' + err));
            req.session.message = 'Failed to save. Please try again';
            req.session.messageType = 'danger';
            res.redirect('/admin/products/0');
          } else {
            // Update the index
            req.session.message = 'Successfully saved';
            req.session.messageType = 'success';
            res.redirect('/admin/products/0');
          }
        },
      );
    } else if (property === 'productTrending') {
      db.products.update(
        { productVariantId: id },
        { $set: { productTrending: value } },
        {},
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed to save product: ' + err));
            req.session.message = 'Failed to save. Please try again';
            req.session.messageType = 'danger';
            res.redirect('/admin/products/0');
          } else {
            // Update the index
            req.session.message = 'Successfully saved';
            req.session.messageType = 'success';
            res.redirect('/admin/products/0');
          }
        },
      );
    } else if (property === 'productNew') {
      db.products.update(
        { productVariantId: id },
        { $set: { productNew: value, productRecommendation: value } },
        {},
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed to save product: ' + err));
            req.session.message = 'Failed to save. Please try again';
            req.session.messageType = 'danger';
            res.redirect('/admin/products/0');
          } else {
            // Update the index
            req.session.message = 'Successfully saved';
            req.session.messageType = 'success';
            res.redirect('/admin/products/0');
          }
        },
      );
    }
  },
);

// Import product from excel file
router.get(
  '/admin/product/import',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    res.render('product_upload_excel', {
      menu: 'product',
      title: 'Import Products',
      admin: true,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      config: req.app.config,
      editor: true,
      helpers: req.handlebars.helpers,
    });
  },
);

// Export Order Edition By SKU
router.get(
  '/admin/product/export/:SKU',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const SKU = req.params.SKU;
    const [products] = await Promise.all([
      db.orders
        .aggregate([
          {
            $match: {
              editionNumber: {
                $exists: true,
              },
              sku: {
                $eq: SKU,
              },
            },
          },
          {
            $project: {
              _id: 0,
              udid: 1,
              // 'sku': 1,
              editionNumber: 1,
            },
          },
        ])
        .sort({
          editionNumber: -1,
        })
        .toArray(),
    ]);

    if (!products.length > 0) {
      req.session.message = 'This product does not have a edition number.';
      req.session.messageType = 'danger';
      res.redirect(`/admin/products/filter/${SKU}`);
    }
    const fields = [];
    for (const field of Object.keys(products[0])) {
      fields.push({
        label: field,
        value: field,
      });
    }
    return downloadResource(
      res,
      `editionnumber_${SKU}_datetime.csv`,
      fields,
      products,
    );
  },
);

const downloadResource = (res, fileName, fields, data) => {
  const json2csv = new Parser({ fields });
  const csv = json2csv.parse(data);
  res.header('Content-Type', 'text/csv');
  res.attachment(fileName);
  return res.send(csv);
};
//   const convertCamelCaseToTitle = (field) => {
//     const firstLetter = field.charAt(0).toUpperCase();
//     field = field.replace(/^(.)/, firstLetter);
//     return field.replace(/([a-z])([A-Z])/g, "$1 $2");
//   };
//   const convertTitleToCamelCase = (str) => {
//     return str
//         .replace(/\s(.)/g, function(a) {
//             return a.toUpperCase();
//         })
//         .replace(/\s/g, '')
//         .replace(/^(.)/, function(b) {
//             return b.toLowerCase();
//         });
// }

router.post(
  '/admin/product/import/:SKU',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const SKU = req.params.SKU;
    const form = new formidable.IncomingForm();
    form.uploadDir = './public/uploads/product/';
    if (!fs.existsSync(form.uploadDir)) {
      fs.mkdirSync(form.uploadDir);
    }
    form.parse(req, (err, fields, files) => {
      if (files.myfile.size > 0) {
        const tmpPath = files.myfile.path;
        let message = '<ul>';
        let count = 0;
        const data = [];
        const fileName = files.myfile.name;
        const sku = fileName.substring(
          fileName.indexOf('_') + 1,
          fileName.lastIndexOf('_'),
        );
        fs.createReadStream(tmpPath)
          .on('error', () => {
            req.session.message = 'Failed import csv.';
            req.session.messageType = 'danger';
            res.redirect(`/admin/products/filter/${SKU}`);
          })
          .pipe(csvParser())
          .on('data', async (row) => {
            count++;
            data.push(row);
          })
          .on('end', async () => {
            const [countDoc] = await Promise.all([
              db.orders.countDocuments({
                sku,
                editionNumber: { $exists: true },
              }),
            ]);
            if (sku === req.params.SKU) {
              if (countDoc === count) {
                message;
              } else {
                message +=
                  '<li>Total number of UDIDs in the uploaded file does not match the total number of UDIDs for a given SKU in the database.</li>' +
                  '\n';
              }
              for (const item of data) {
                if (Number.isInteger(Number(item.editionNumber))) {
                  message;
                } else {
                  message +=
                    `<li>editionNumber ${item.editionNumber} is not an integer.</li>` +
                    '\n';
                }
                const [orders, editionNumber] = await Promise.all([
                  db.orders
                    .find({
                      udid: item.udid,
                      sku: sku,
                      editionNumber: { $exists: true },
                    })
                    .toArray(),
                  db.orders
                    .find({ editionNumber: item.editionNumber })
                    .toArray(),
                ]);
                if (item.udid) {
                  if (orders.length === 1 && orders[0].sku === sku) {
                    message;
                  } else {
                    message +=
                      `<li>The uploaded UDIDs ${item.udid} don’t match the UDIDs in the searched SKU. </li>` +
                      '\n';
                  }
                  if (orders.length) {
                    if (
                      !editionNumber.length ||
                      (editionNumber.length === 1 &&
                        editionNumber[0].udid === item.udid)
                    ) {
                      message;
                    } else if (item.editionNumber === '') {
                      message +=
                        `<li>There are missing editionNumber.</li>` + '\n';
                    } else {
                      message +=
                        `<li>There are duplicated edition numbers: ${item.editionNumber}.</li>` +
                        '\n';
                    }
                  }
                } else {
                  message += `<li>There are missing UDIDs. </li>` + '\n';
                }
              }
            } else {
              message +=
                '<li>Your file is not valid with this sku, please import extracly file.</li>' +
                '\n';
            }
            message += '</ul>';
            if (message === '<ul></ul>') {
              for (const item of data) {
                const [newDoc] = await Promise.all([
                  db.orders.update(
                    { udid: common.getId(item.udid) },
                    { $set: { editionNumber: item.editionNumber } },
                    {},
                  ),
                ]);
              }
              req.session.messageType = 'success';
              res
                .status(200)
                .json({ message: 'Successfully imported the file' });
            } else {
              req.session.messageType = 'danger';
              res.status(400).json({ message });
            }
          });
      }
    });
  },
);

router.post(
  '/admin/product/import',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const form = new formidable.IncomingForm();
    form.uploadDir = './public/uploads/product/';
    if (!fs.existsSync(form.uploadDir)) {
      fs.mkdirSync(form.uploadDir);
    }
    form.parse(req, (err, fields, files) => {
      if (files.file.size > 0) {
        const tmpPath = files.file.path;
        let message = '';
        let count = 0;
        fs.createReadStream(tmpPath)
          .on('error', () => {
            req.session.message = 'Failed import csv.';
            req.session.messageType = 'danger';
            res.redirect('/admin/product/import/');
          })
          .pipe(csvParser())
          .on('data', async (row) => {
            count++;
            if (row['uuid'].length > 0) {
              if (row['sku']) {
                const [product1] = await Promise.all([
                  db.products.findOne({ productVariantSku: row['sku'] }),
                ]);
                if (product1) {
                  const url =
                    row['Links'].toLowerCase().indexOf('https://') > -1
                      ? row['Links']
                      : 'https://' + row['Links'];
                  const slug = url.replace('https://stockx.com/', '');
                  product1.stockxUUID = row['uuid'];
                  product1.stockxSlug = slug;
                  product1.stockxLink = url;
                  const [newDoc] = await Promise.all([
                    db.products.update(
                      { _id: common.getId(product1._id) },
                      { $set: product1 },
                      {},
                      false,
                    ),
                  ]);
                } else {
                  message += 'Row - ' + count + ' sku not found' + '\n';
                }
              } else {
                message += 'Row - ' + count + " Don't have sku" + '\n';
              }
            }
          })
          .on('end', () => {
            if (message === '') {
              req.session.message = 'Imported';
              req.session.messageType = 'success';
              res.redirect('/admin/products/0');
            } else {
              req.session.message = message;
              req.session.messageType = 'danger';
              res.redirect('/admin/products/0');
            }
          });
      } else {
        req.session.message = 'Failed import csv.';
        req.session.messageType = 'danger';
        res.redirect('/admin/product/import/');
      }
    });
  },
);

router.post(
  '/admin/upload_upcomming_product_picture_temp',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    const form = new formidable.IncomingForm();
    form.uploadDir = './public/uploads/upcomming-product/';
    if (!fs.existsSync(form.uploadDir)) {
      fs.mkdirSync(form.uploadDir);
    }
    form.parse(req, (err, fields, files) => {
      if (files.file.size > 0) {
        const tmpPath = files.file.path;
        const message = '';
        const count = 0;
        fs.createReadStream(tmpPath).on('error', () => {
          res.status(400).json({
            message: 'Invalid file upload',
          });
        });
        res.status(200).json({
          message: 'ok',
          data: {
            url: tmpPath,
          },
        });
      } else {
        res.status(400).json({
          message: 'Invalid file upload',
        });
      }
    });
  },
);

router.post(
  '/admin/createOrUpdateUpcommingProduct',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const data = req.body;
    if (data.id && data.id != null && data.id != '') {
      db.upcommingProducts.update(
        { _id: common.getId(data.id) },
        {
          $set: {
            title: data.title,
            active: data.active,
            description: data.description,
            dropDate: new Date(data.dropDate),
            pictureUrl: data.pictureUrl,
            url: data.url,
          },
        },
        {},
        (err, numReplaced) => {
          if (err) {
            console.error(colors.red('Failed to save product: ' + err));
            res.status(400).json({
              message: 'Invalid data',
            });
          } else {
            common.Schedule(
              req.app,
              'upcommingProducts',
              'dropDate',
              { active: false },
              'upcommingProducts',
            );
            res.status(200).json({
              message: 'ok',
              data: {
                id: data.id,
              },
            });
          }
        },
      );
    } else {
      if (schedule.scheduledJobs['upcommingProducts']) {
        schedule.scheduledJobs['upcommingProducts'].cancel();
      }
      Schedule(
        req.app,
        'upcommingProducts',
        'dropDate',
        { active: false },
        'upcommingProducts',
      );
      db.upcommingProducts.insert(
        {
          title: data.title,
          active: data.active,
          description: data.description,
          dropDate: new Date(data.dropDate),
          pictureUrl: data.pictureUrl,
          url: data.url,
          count: 0,
        },
        (err, newDoc) => {
          if (err) {
            console.log(colors.red('Error inserting document: ' + err));
            // keep the current stuff
            res.status(400).json({
              message: 'Invalid data',
            });
          } else {
            // get the new ID
            const newId = newDoc.insertedIds[0];
            res.status(200).json({
              message: 'ok',
              data: {
                id: newId,
              },
            });
          }
        },
      );
    }
  },
);

router.get(
  '/admin/nfc-chip-inventory',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;

    // Total NFC chips available in the NFC factory
    const [totalChipAvailable] = await Promise.all([
      db.qrcodes.find({
        $and: [
          { udid: { $ne: null } },
          { udid: { $ne: '' } },
          { udid: { $exists: true } },
        ],
      }),
    ]);

    // Total leather keyfobs available in the NFC factory (inventory)
    const [totalLeatherKeyfobsAvailable] = await Promise.all([
      db.qrcodes.find({
        $and: [
          { udid: { $ne: null } },
          { udid: { $ne: '' } },
          { udid: { $exists: true } },
        ],
      }),
    ]);
    const page = req.params.page ? req.params.page : 0;
    // Table breakdown by SKU of available encrypted strings (not attached to UDID)
    const [skuByQuantity, totalSku] = await Promise.all([
      db.orders
        .aggregate([
          {
            $match: {
              $and: [
                { udid: { $ne: null } },
                { udid: { $ne: '' } },
                { udid: { $exists: true } },
              ],
            },
          },
          { $group: { _id: '$sku', count: { $sum: 1 } } },
        ])
        .skip(page * ethereumConfig.qrcodeLimitPerpage)
        .limit(ethereumConfig.qrcodeLimitPerpage)
        .toArray(),
      db.orders.distinct('sku').length,
    ]);

    const total = totalSku / ethereumConfig.qrcodeLimitPerpage;
    const array = [];
    for (let i = 0; i < total; i++) {
      array.push(i + '');
    }

    res.render('nfc_chip_inventory', {
      title: 'NFC Chip Inventory',
      menu: 'setting',
      admin: true,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      config: req.app.config,
      editor: true,
      helpers: req.handlebars.helpers,
    });
  },
);

/**
 * @swagger
 * /api/v4/getUpcommingProducts:
 *  get:
 *    security: []
 *    tags:
 *      - Product
 *    description: Get list of active upcomming products
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/api/v4/getUpcommingProducts', async (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  const db = req.app.db;
  const [data] = await Promise.all([
    db.upcommingProducts
      .find({
        $and: [{ active: true }],
      })
      .sort({ dropDate: 1 })
      .toArray(),
  ]);
  res.status(200).json({
    message: 'ok',
    code: 200,
    data: data,
  });
});

/**
 * @swagger
 * /api/v4/getAllUpcommingProducts:
 *  get:
 *    security: []
 *    tags:
 *      - Product
 *    description: Get all list upcomming products
 *    parameters:
 *    - in: query
 *      name: filter
 *      schema:
 *      type: string
 *      enum:
 *            - All
 *            - Active
 *            - InActive
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/api/v4/getAllUpcommingProducts', async (req, res) => {
  const db = req.app.db;
  const filter = req.query.filter ? req.query.filter : 'Active';
  let condition = {};
  switch (filter) {
    case 'Inactive':
      condition = {
        active: false,
      };
      break;
    case 'All':
      condition = {};
      break;
    case 'Active':
      condition = {
        active: true,
      };
      break;
  }
  const [data] = await Promise.all([
    db.upcommingProducts.find(condition).sort({ dropDate: 1 }).toArray(),
  ]);
  res.status(200).json({
    message: 'ok',
    code: 200,
    data: data,
  });
});

/**
 * @swagger
 * /api/v4/countUpcommingInterest/{id}:
 *  get:
 *    security: []
 *    tags:
 *      - Product
 *    description: Get number of people interested in upcoming product
 *    parameters:
 *    - in: path
 *      name: id
 *      required: true
 *      schema:
 *      type: string
 *      example: '602e465f0fc90e42b1b6f294'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid data
 */
router.get('/api/v4/countUpcommingInterest/:id', async (req, res) => {
  const db = req.app.db;
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  const id = req.params.id;
  const [data] = await Promise.all([
    db.upcommingProducts.findOne({ _id: common.getId(id) }),
  ]);
  if (!data) {
    return res.status(400).json({ code: 400, message: 'Not found' });
  }
  if (!data.count) {
    data.count = 0;
  }
  data.count = data.count + 1;

  const numReplaced = await db.upcommingProducts.update(
    { _id: common.getId(id) },
    {
      $set: {
        count: data.count,
      },
    },
    {},
  );
  if (!numReplaced) {
    return res.status(400).json({
      message: 'Invalid data',
    });
  }
  return res.status(200).json({
    code: 200,
    message: 'ok',
  });
});

/**
 * @swagger
 * /api/v4/deleteUpcommingProducts/{id}:
 *  delete:
 *    security: []
 *    tags:
 *      - Product
 *    description: Delete an upcoming product
 *    parameters:
 *    - in: path
 *      name: id
 *      required: true
 *      schema:
 *      type: string
 *      example: '602e465f0fc90e42b1b6f294'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Not found
 */
router.delete('/api/v4/deleteUpcommingProducts/:id', async (req, res) => {
  const db = req.app.db;
  const id = req.params.id;
  const [data] = await Promise.all([
    db.upcommingProducts.findOne({ _id: common.getId(id) }),
  ]);
  if (!data) {
    return res.status(400).json({ code: 400, message: 'Not found' });
  }
  await Promise.all([
    db.upcommingProducts.deleteOne({ _id: common.getId(id) }),
  ]);
  res.status(200).json({
    message: 'ok',
    code: 200,
  });
});

router.post(
  '/admin/product/set-size-edition-number',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const maxEditionNumber = req.body.maxEditionNumber;
    const productId = req.body.productId;
    if (!maxEditionNumber || maxEditionNumber < 0) {
      res.status(400).json({
        message: 'Please enter whole number',
        data: null,
      });
      return;
    }

    const product = await db.products.findOne({ _id: common.getId(productId) });
    if (!product) {
      res.status(400).json({
        message: 'Product not found',
        data: null,
      });
      return;
    }
    product.maxEditionNumber = Number(maxEditionNumber);
    product.enableEditionNumber = Number(maxEditionNumber) > 0;
    await db.products.update(
      { _id: common.getId(productId) },
      { $set: product },
      {},
    );

    res.status(200).json({
      message: 'Successfully updated',
      data: null,
    });
  },
);

router.get('/admin/listDataForGoodies/:id', async (req, res) => {
  const db = req.app.db;
  const numberGoodies = req.query.pageSize || 10;
  const product = await db.products.findOne({
    _id: common.getId(req.params.id),
  });

  if (!product) {
    return res
      .status(400)
      .json({ code: 400, message: 'No product found', data: [] });
  }

  let listDataGoodies = [];
  if (product.productAssetsFile) {
    listDataGoodies = product.productAssetsFile;
  }

  const [data] = await Promise.all([
    listDataGoodies.slice(0, parseInt(numberGoodies)),
  ]);
  const result = {
    _id: common.getId(req.params.id),
    productAssetsFile: data,
    totalCount: listDataGoodies.length,
  };
  res.status(200).json({
    message: 'ok',
    code: 200,
    data: result,
  });
});

router.post('/admin/product/updateAssetsFileUpload', async (req, res) => {
  const db = req.app.db;
  const product = await db.products.find({});
  if (!product) {
    return res
      .status(400)
      .json({ code: 400, message: 'No product found', data: [] });
  }
  const [resultUpdate] = await Promise.all([
    db.products.update(
      {},
      {
        $unset: {
          productAssetsFile: [],
        },
      },
      { multi: true },
    ),
  ]);
  return res
    .status(200)
    .json({ code: 200, message: 'Update AssetsFileUpload Success', data: [] });
});

const splitS3Path = (path) => {
  const url = new URL(path);
  const bucket = Config.S3.bucket;
  const key = url.pathname.substr(1);
  return { bucket, key };
};

const deleteS3Object = async (path) => {
  const { bucket, key } = splitS3Path(path);
  await s3
    .deleteObjects({
      Bucket: bucket,
      Delete: { Objects: [{ Key: key }] },
    })
    .promise();
};

router.post('/admin/deleteDataForGoodies', async (req, res) => {
  try {
    const db = req.app.db;
    const product = await db.products.findOne({
      _id: common.getId(req.body.id),
    });

    if (!product) {
      return res
        .status(400)
        .json({ code: 400, message: 'No product found', data: [] });
    }
    const [obj] = await Promise.all([
      db.products.findOne(
        { _id: common.getId(req.body.id) },
        { productAssetsFile: { $elemMatch: { fileId: req.body.fileId } } },
        { multi: true },
      ),
    ]);
    // get urlFile for delete file to S3
    const path = obj.productAssetsFile[0].assetsFileUrl;
    if (path) {
      deleteS3Object(path);
    }
    const [data] = await Promise.all([
      db.products.update(
        { _id: common.getId(req.body.id) },
        { $pull: { productAssetsFile: { fileId: req.body.fileId } } },
        { multi: true },
      ),
    ]);
    return res.status(200).json({
      message: 'Delete file successfully',
      code: 200,
      data: [],
    });
  } catch (e) {
    console.log('Errors: ', e);
    return res.status(400).json({
      message: e,
      code: 400,
    });
  }
});

router.get('/admin/products/nft/:tokenId', getNft);

module.exports = router;
