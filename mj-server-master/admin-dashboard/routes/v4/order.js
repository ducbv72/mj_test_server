const _ = require('lodash');
const express = require('express');
const common = require('../../../lib/common');
const router = express.Router();
const Config = require('../../../config/ethereumConfig');
const {
  searchCertificate,
  registerOwnerShip,
} = require('../../../services/certificate');
const NodeCache = require('node-cache');
const MJCache = new NodeCache();

// Show orders
router.get(
  '/admin/certificates',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    let page = req.query.page;
    const keyword = req.query.keyword ? req.query.keyword : '';
    const filterType = req.query.filterType ? req.query.filterType : 'username';
    const sortType = req.query.sortType ? req.query.sortType : -1;
    if (!page || page <= 0) page = 1;
    const skip = (page - 1) * Number(Config.productLitmitPerPage);
    const [certificates, count] = await searchCertificate(
      db,
      keyword,
      filterType,
      sortType,
      skip,
    );
    let countCertificate = 0;
    let countNumberOfUserRegister = 0;
    let countChipped = 0;
    // MJCache.flushAll();
    if (MJCache.has('countCertificate')) {
      countCertificate = MJCache.get('countCertificate');
      countNumberOfUserRegister = MJCache.get('countNumberOfUserRegister');
      countChipped = MJCache.get('countChipped');
    } else {
      [countCertificate, countNumberOfUserRegister, countChipped] =
        await Promise.all([
          db.orders.countDocuments({
            orderCertNumber: { $exists: true },
          }),
          db.orders.distinct('orderEmail', {
            orderCertNumber: { $exists: true },
          }),
          db.qrcodes.countDocuments({
            $and: [{ udid: { $ne: '' } }, { udid: { $ne: null } }],
          }),
        ]);
      countNumberOfUserRegister = countNumberOfUserRegister.length;
      MJCache.set('countCertificate', countCertificate, 600);
      MJCache.set('countNumberOfUserRegister', countNumberOfUserRegister, 600);
      MJCache.set('countChipped', countChipped, 600);
    }
    const navigationUrl =
      process.env.APP_URL +
      '/admin/certificates?keyword=' +
      keyword +
      '&filterType=' +
      filterType +
      '&sortType=' +
      sortType;

    res.render('certificate/all_certificates', {
      menu: 'order',
      page: page,
      currentPage: page,
      nextPage: Number(page) < count ? Number(page) + 1 : count,
      backPage: Number(page) > 0 ? Number(page) - 1 : 0,
      totalPage: Math.ceil(count / Config.productLitmitPerPage) - 1,
      countCertificate: countCertificate,
      countChipped: countChipped,
      countNumberOfUserRegister: countNumberOfUserRegister,
      keyword: keyword,
      filterType: filterType,
      sortType: sortType,
      title: 'Cart',
      results: certificates,
      navigationUrl: navigationUrl,
      admin: true,
      config: req.app.config,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

// render the editor
router.get(
  '/admin/certificate/detail/:udid',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;

    const [udid, certificate] = await Promise.all([
      db.qrcodes.findOne({ udid: req.params.udid }),
      db.orders.findOne({
        $and: [
          { udid: req.params.udid },
          { orderCertNumber: { $exists: true } },
          { orderCertNumber: { $ne: '' } },
          { udid: { $ne: '' } },
          { udid: { $ne: null } },
          { orderCertNumber: { $ne: null } },
        ],
      }),
    ]);

    if (!udid) {
      req.session.message = 'UDID not found';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificates');
      return;
    }
    const product = await db.products.findOne({
      _id: common.getId(udid.productId),
    });

    res.render('certificate/certificate_detail', {
      title: 'View certificate',
      menu: 'order',
      udid: udid,
      certificate: certificate,
      product: product,
      config: req.app.config,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
    });
  },
);

router.post(
  '/admin/certificate/change-ownership',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const certificateId = req.body.certificateId;
    const udid = req.body.udid;
    const newOwner = req.body.newOwner;
    if (!certificateId && !udid) {
      req.session.message = 'Not Found 1';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificates');
      return;
    }

    const certificate = await db.orders.findOne({
      _id: common.getId(certificateId),
    });

    const udidItem = await db.qrcodes.findOne({
      udid: udid,
    });

    if (!certificate && !udidItem) {
      req.session.message = 'Not Found 2';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificates');
      return;
    }

    if (!newOwner) {
      req.session.message = 'Please enter username';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificate/detail/' + udid);
      return;
    }

    const customer = await db.customers.findOne({
      userNameLower: newOwner.toLowerCase(),
    });

    if (!customer) {
      req.session.message = 'Username not found';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificate/detail/' + udid);
      return;
    }
    const result = await registerOwnerShip(
      db,
      req.session.user,
      customer,
      certificate,
      udidItem,
    );
    if (result) {
      req.session.message = 'Successfully saved';
      req.session.messageType = 'success';
      res.redirect('/admin/certificate/detail/' + udid);
    } else {
      req.session.message = 'Something went wrong';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificate/detail/' + udid);
    }
  },
);

router.get(
  '/admin/certificate/summary',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    // remove the article
    const [summary, countNumberOfUserRegister, countNumberRegistered] =
      await Promise.all([
        db.orders
          .aggregate([
            {
              $match: { 'orderProducts.orderSmartContract': { $exists: true } },
            },
            { $group: { _id: '$sku', count: { $sum: 1 } } },
          ])
          .sort({ count: -1 })
          .toArray(),
        db.orders.distinct('orderEmail', {
          'orderProducts.orderSmartContract': { $exists: true },
        }),
        db.orders.count({
          'orderProducts.orderSmartContract': { $exists: true },
        }),
      ]);

    res.render('certificate/certificate_summary', {
      title: 'Certification summary',
      menu: 'order',
      summary: summary,
      countNumberOfUserRegister: countNumberOfUserRegister.length,
      countNumberRegistered: countNumberRegistered,
      admin: true,
      config: req.app.config,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.get(
  '/admin/certificate/reset-certificate/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const certificateId = req.params.id;
    if (!certificateId) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificates');
      return;
    }

    const certificate = await db.orders.findOne({
      _id: common.getId(certificateId),
    });
    if (!certificate) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificates');
      return;
    }
    await db.orders.remove({ _id: common.getId(req.params.id) }, {});
    await common.saveToActionLog(
      db,
      req.session.user,
      'remove ownership',
      certificate,
    );

    req.session.message = 'Successfully saved';
    req.session.messageType = 'success';
    res.redirect('/admin/certificate/detail/' + certificate.udid);
  },
);

router.post(
  '/admin/certificate/change-edition-number',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const udidId = req.body.udidId;
    if (!udidId) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificates');
      return;
    }

    const udidObject = await db.qrcodes.findOne({
      _id: common.getId(udidId),
    });
    if (!udidObject) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificates');
      return;
    }
    // validate unique edition number
    const udidObjectDuplicateEditionNumber = await db.qrcodes.findOne({
      $and: [
        { _id: { $ne: udidObject._id } },
        { productVariantSku: udidObject.productVariantSku },
        { editionNumber: req.body.newEditionNumber },
      ],
    });

    console.log(udidObjectDuplicateEditionNumber);

    if (udidObjectDuplicateEditionNumber) {
      req.session.message = 'Edition number has been taken';
      req.session.messageType = 'danger';
      res.redirect('/admin/certificate/detail/' + udidObject.udid);
      return;
    }

    await db.qrcodes.update(
      { _id: common.getId(udidObject._id) },
      { $set: { editionNumber: req.body.newEditionNumber } },
      { multi: false },
    );

    await db.orders.update(
      { udid: udidObject.udid },
      { $set: { editionNumber: req.body.newEditionNumber } },
      { multi: false },
    );
    udidObject.editionNumber = req.body.newEditionNumber;
    await common.saveToActionLog(
      db,
      req.session.user,
      'change edition number',
      udidObject,
    );
    console.log(udidObject);
    req.session.message = 'Successfully saved';
    req.session.messageType = 'success';
    res.redirect('/admin/certificate/detail/' + udidObject.udid);
  },
);

module.exports = router;
