const express = require('express');
const common = require('../../../lib/common');
const colors = require('colors');
const fs = require('fs');
const path = require('path');
const router = express.Router();
const Config = require('../../../config');

const mime = require('mime-type/with-db');

const multer = require('multer');

const upload = multer({ dest: 'public/uploads/' });

router.get(
  '/admin/exclusive-sale',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    const pageOptions = {
      page: parseInt(req.query.page) || 1,
      limit: parseInt(req.query.limit) || 10,
    };
    const page = Number(req.query.page) || 1;

    const [exclusiveSaleList, count] = await Promise.all([
      db.exclusiveSale
        .find()
        .skip((pageOptions.page - 1) * pageOptions.limit)
        .limit(pageOptions.limit)
        .sort({
          createdAt: -1,
        })
        .toArray(),
      db.exclusiveSale.count(),
    ]);

    const totalPage = Math.ceil(count / pageOptions.limit);
    const totalPageArr = [];
    for (let i = 1; i <= totalPage; i++) {
      totalPageArr.push(i);
    }

    res.render('exclusive_sales/exclusive_sale_list', {
      title: 'Exclusive Sale Management',
      menu: 'product',
      session: req.session,
      results: exclusiveSaleList,
      totalPage: totalPage,
      currentPage: page,
      totalPageArr: totalPageArr,
      count: count,
      editor: true,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.get(
  '/admin/exclusive-sale/new',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    res.render('exclusive_sales/exclusive_sale_new', {
      title: 'New Tag',
      menu: 'product',
      session: req.session,
      admin: true,
      config: req.app.config,
      helpers: req.handlebars.helpers,
    });
  },
);

// insert new product form action
router.post(
  '/admin/exclusive-sale/insert',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const doc = {
      titleTag: req.body.frmTitleTag,
      productTag: req.body.frmProductTag,
      customerTag: req.body.frmCustomerTag.filter((a) => a), //  Remove the empty, null, undefined values in the array.
      description: req.body.frmDescription,
      bannerText: req.body.frmBannerText,
      status: common.checkboxBool(req.body.frmStatus),
      imgUrl: req.body.frmImgUrl,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    db.exclusiveSale.insert(doc, (err, newDoc) => {
      if (err) {
        req.session.message = 'New tag successfully created';
        req.session.messageType = 'success';
        res.redirect('/admin/exclusive-sale');
      } else {
        req.session.message = 'New tag successfully created';
        req.session.messageType = 'success';
        res.redirect('/admin/exclusive-sale');
      }
    });
  },
);

router.get(
  '/admin/exclusive-sale/edit/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const result = await db.exclusiveSale.findOne({
      _id: common.getId(req.params.id),
    });

    res.render('exclusive_sales/exclusive_sale_edit', {
      title: 'Edit Tag',
      menu: 'product',
      result: result,
      customerTagArr: result.customerTag,
      session: req.session,
      admin: true,
      config: req.app.config,
      helpers: req.handlebars.helpers,
    });
  },
);

router.post(
  '/admin/exclusive-sale/update',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;

    const doc = {
      titleTag: req.body.frmTitleTag,
      productTag: req.body.frmProductTag,
      customerTag: req.body.frmCustomerTag.filter((a) => a), //  Remove the empty, null, undefined values in the array.
      description: req.body.frmDescription,
      bannerText: req.body.frmBannerText,
      status: common.checkboxBool(req.body.frmStatus),
      imgUrl: req.body.frmImgUrl,
      updatedAt: new Date(),
    };

    db.exclusiveSale.findOne(
      { _id: common.getId(req.body.frmExclusiveSaleId) },
      (err, data) => {
        if (err) {
          console.info(err.stack);
          req.session.message = 'Failed updating tag.';
          req.session.messageType = 'danger';
          res.redirect(
            '/admin/exclusive-sale/edit/' + req.body.frmExclusiveSaleId,
          );
          return;
        }
        db.exclusiveSale.updateOne(
          { _id: common.getId(req.body.frmExclusiveSaleId) },
          { $set: doc },
          (err, numReplaced) => {
            if (err) {
              console.error(colors.red('Failed to save tag: ' + err));
              req.session.message = 'Failed to save. Please try again';
              req.session.messageType = 'danger';
              res.redirect(
                '/admin/exclusive-sale/edit/' + req.body.frmExclusiveSaleId,
              );
            } else {
              req.session.message = 'Successfully saved';
              req.session.messageType = 'success';
              res.redirect('/admin/exclusive-sale');
            }
          },
        );
      },
    );
  },
);

router.get(
  '/admin/exclusive-sale/delete/:id',
  common.restrict,
  common.checkAccess,
  (req, res) => {
    const db = req.app.db;
    db.exclusiveSale.remove(
      { _id: common.getId(req.params.id) },
      {},
      (err, numRemoved) => {
        if (err) {
          console.error(colors.red('Failed to delete product: ' + err));
          req.session.message = 'Failed to save. Please try again';
          req.session.messageType = 'danger';
          res.redirect(
            '/admin/exclusive-sale/edit/' + req.body.frmExclusiveSaleId,
          );
        }
        // redirect home
        req.session.message = 'Tag successfully deleted';
        req.session.messageType = 'success';
        res.redirect('/admin/exclusive-sale?page=1');
      },
    );
  },
);

// upload the file
router.post(
  '/admin/exlusive-sale/upload-file',
  common.restrict,
  common.checkAccess,
  upload.single('upload_file'),
  async (req, res, next) => {
    const db = req.app.db;
    if (req.file) {
      const file = req.file;
      // Get the mime type of the file
      const mimeType = mime.lookup(file.originalname);

      // Check for allowed mime type and file size
      if (
        !common.allowedMimeType.includes(mimeType) ||
        file.size > common.fileSizeLimit
      ) {
        // Remove temp file
        fs.unlinkSync(file.path);
        res.status(400).json({
          code: 400,
          message: 'File type not allowed or too large. Please try again.',
        });
        return;
      }

      //upload bigger file
      const imageUrlS3 = await common.uploadFileToS3(
        file.path,
        Config.S3.exclusiveImageFolder,
      );
      if (req.body.src && req.body.src.length) {
        await common.deleteS3Object(imageUrlS3, req.body.src);
      }
      fs.unlinkSync(file.path);
      res.status(200).json({
        message: 'Upload image sucessfully',
        data: { imageUrl: imageUrlS3 },
      });
    } else {
      // Redirect to error
      res.status(400).json({
        code: 400,
        message: 'File upload error. Please select a file.',
      });
    }
  },
);

module.exports = router;
