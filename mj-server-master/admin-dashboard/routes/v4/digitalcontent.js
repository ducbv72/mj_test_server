const express = require('express');
const common = require('../../../lib/common');
const router = express.Router();

router.get(
  '/admin/digital-content/manage-digital-content',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    // get the top results
    const digitalContentGroupTypes = await db.digitalContentGroupType
      .find({})
      .toArray();

    res.render('digital_content/list_group_type', {
      title: 'Manage Digital Content',
      menu: 'digital-content',
      digitalContentGroupTypes: digitalContentGroupTypes,
      session: req.session,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.post(
  '/admin/digital-content/digital-content-new',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const newGroup = {
      groupName: req.body.groupName,
      description: req.body.description,
      createdBy: req.session.user,
      updatedAt: new Date(),
      createdAt: new Date(),
    };

    const newDoc = await db.digitalContentGroupType.insert(newGroup);

    await common.saveToActionLog(
      db,
      req.session.user,
      'create digital content',
      newGroup,
    );

    req.session.message = 'Added';
    req.session.messageType = 'success';
    res.redirect(
      '/admin/digital-content/digital-content-edit/' + newDoc.ops[0]._id,
    );
  },
);

router.get(
  '/admin/digital-content/digital-content-edit/:id',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    // get the top results
    const [groupItem, digitalContents] = await Promise.all([
      db.digitalContentGroupType.findOne({ _id: common.getId(req.params.id) }),
      db.digitalContents
        .find({ groupId: common.getId(req.params.id) })
        .toArray(),
    ]);
    // sort by Id
    digitalContents.sort(function (a, b) {
      try {
        return parseInt(a.toyId) - parseInt(b.toyId);
      } catch (ex) {
        return a.toyId - b.toyId;
      }
    });
    res.render('digital_content/group_edit', {
      title: 'Digital contents',
      menu: 'digital-content',
      groupItem: groupItem,
      digitalContents: digitalContents,
      session: req.session,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

router.post(
  '/admin/digital-content/digital-content-edit',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const groupItem = await db.digitalContentGroupType.findOne({
      _id: common.getId(req.body.groupItemId),
    });

    if (!groupItem) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/digital-content/manage-digital-content');
      return;
    }
    groupItem.groupName = req.body.groupName;
    groupItem.description = req.body.description;
    groupItem.lastUpdatedBy = req.session.user;
    groupItem.updatedAt = new Date();

    await db.digitalContentGroupType.update(
      { _id: common.getId(req.body.groupItemId) },
      { $set: groupItem },
      { multi: false },
    );

    await common.saveToActionLog(
      db,
      req.session.user,
      'update digital content',
      groupItem,
    );

    req.session.message = 'Updated';
    req.session.messageType = 'success';
    res.redirect(
      '/admin/digital-content/digital-content-edit/' + req.body.groupItemId,
    );
  },
);

router.get(
  '/admin/digital-content/delete/:id',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;

    const [groupItem, digitalContents] = await Promise.all([
      db.digitalContentGroupType.findOne({ _id: common.getId(req.params.id) }),
      db.digitalContents
        .find({
          $and: [
            { groupId: common.getId(req.params.id) },
            {
              $and: [
                { productSkus: { $ne: [] } },
                { productSkus: { $ne: null } },
              ],
            },
          ],
        })
        .toArray(),
    ]);

    if (digitalContents.length > 0) {
      req.session.message = 'Please unlink all products with character';
      req.session.messageType = 'danger';
      res.redirect('/admin/digital-content/manage-digital-content');
      return;
    }

    await Promise.all([
      db.digitalContentGroupType.remove(
        { _id: common.getId(req.params.id) },
        {},
      ),
      db.digitalContents.remove({ groupId: common.getId(req.params.id) }, {}),
    ]);

    await common.saveToActionLog(
      db,
      req.session.user,
      'delete digital content',
      groupItem,
    );

    req.session.message = 'Deleted';
    req.session.messageType = 'success';
    res.redirect('/admin/digital-content/manage-digital-content');
  },
);

router.post(
  '/admin/digital-content/character-insert',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const groupItem = await db.digitalContentGroupType.findOne({
      _id: common.getId(req.body.groupItemId),
    });
    if (!groupItem) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/digital-content/manage-digital-content');
      return;
    }
    const newItem = {
      itemId: new Date().getTime().toString(),
      toyId: common.cleanHtml(req.body.toyId),
      toyName: common.cleanHtml(req.body.toyName),
      buttonText: common.cleanHtml(req.body.buttonText),
      buttonColor: common.cleanHtml(req.body.buttonColor),
      textColor: common.cleanHtml(req.body.textColor),
      contentUrlOrScreen: common.cleanHtml(req.body.contentUrlOrScreen),
      isHostedInApp: common.checkboxBool(req.body.isHostedInApp),
      description: common.cleanHtml(req.body.description),
      groupId: groupItem._id,
      productSkus: [],
      createdBy: req.session.user,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    groupItem.lastUpdatedBy = req.session.user;
    groupItem.updatedAt = new Date();

    groupItem.lastUpdatedBy = req.session.user;
    groupItem.updatedAt = new Date();
    await Promise.all([
      db.digitalContentGroupType.update(
        { _id: groupItem._id },
        { $set: groupItem },
        { multi: false },
      ),
      [db.digitalContents.insert(newItem)],
    ]);

    await common.saveToActionLog(
      db,
      req.session.user,
      'add new character',
      newItem,
    );
    req.session.message = 'Added';
    req.session.messageType = 'success';
    res.redirect(
      '/admin/digital-content/digital-content-edit/' + req.body.groupItemId,
    );
  },
);

router.post(
  '/admin/digital-content/character-update',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const digitalItem = await db.digitalContents.findOne({
      _id: common.getId(req.body.digitalItemId),
    });

    if (!digitalItem) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/digital-content/manage-digital-content');
      return;
    }

    const groupItem = await db.digitalContentGroupType.findOne({
      _id: common.getId(digitalItem.groupId),
    });

    if (!groupItem) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/digital-content/manage-digital-content');
      return;
    }

    digitalItem.toyId = common.cleanHtml(req.body.toyId);
    digitalItem.toyName = common.cleanHtml(req.body.toyName);
    digitalItem.buttonText = common.cleanHtml(req.body.buttonText);
    digitalItem.buttonColor = common.cleanHtml(req.body.buttonColor);
    digitalItem.textColor = common.cleanHtml(req.body.textColor);
    digitalItem.contentUrlOrScreen = common.cleanHtml(
      req.body.contentUrlOrScreen,
    );
    digitalItem.isHostedInApp = common.checkboxBool(req.body.isHostedInApp);
    digitalItem.description = common.cleanHtml(req.body.description);
    digitalItem.lastUpdatedBy = req.session.user;
    digitalItem.updatedAt = new Date();
    if (!digitalItem.productSkus) {
      digitalItem.productSkus = [];
    }
    groupItem.lastUpdatedBy = req.session.user;
    groupItem.updatedAt = new Date();
    await Promise.all([
      db.digitalContents.update(
        { _id: common.getId(req.body.digitalItemId) },
        { $set: digitalItem },
        { multi: false },
      ),
      db.digitalContentGroupType.update(
        { _id: groupItem._id },
        { $set: groupItem },
        { multi: false },
      ),
    ]);

    await common.saveToActionLog(
      db,
      req.session.user,
      'update character',
      digitalItem,
    );

    req.session.message = 'Updated';
    req.session.messageType = 'success';
    res.redirect(
      '/admin/digital-content/digital-content-edit/' + digitalItem.groupId,
    );
  },
);

router.get(
  '/admin/delete-character/:groupItemId/:itemId',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const digitalItem = await db.digitalContents.findOne({
      _id: common.getId(req.params.itemId),
    });
    await Promise.all([
      db.digitalContents.remove({ _id: common.getId(req.params.itemId) }, {}),
    ]);

    await common.saveToActionLog(
      db,
      req.session.user,
      'delete character',
      digitalItem,
    );
    req.session.message = 'Deleted';
    req.session.messageType = 'success';
    res.redirect(
      '/admin/digital-content/digital-content-edit/' + req.params.groupItemId,
    );
  },
);

router.get(
  '/admin/all-characters',
  common.restrict,
  common.checkAccess,
  async (req, res, next) => {
    const db = req.app.db;
    // get the top results
    let digitalContents = null;
    let groupItem = null;
    if (req.query.id) {
      [groupItem, digitalContents] = await Promise.all([
        db.digitalContentGroupType.findOne({
          _id: common.getId(req.query.id),
        }),
        db.digitalContents
          .find({
            $and: [
              { groupId: common.getId(req.query.id) },
              {
                $and: [
                  { productSkus: { $exists: true, $not: { $size: 0 } } },
                  { productSkus: { $ne: null } },
                ],
              },
            ],
          })
          .toArray(),
      ]);
      // sort by Id
      digitalContents.sort(function (a, b) {
        return parseInt(a.toyId) - parseInt(b.toyId);
      });
    }
    const allGroupType = await db.digitalContentGroupType.find({}).toArray();

    res.render('digital_content/digital_contents', {
      title: 'Digital contents',
      menu: 'digital-content',
      digitalContents: digitalContents,
      allGroupType: allGroupType,
      groupItem: groupItem,
      session: req.session,
      admin: true,
      config: req.app.config,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      helpers: req.handlebars.helpers,
    });
  },
);

// insert form
router.get(
  '/admin/digital-content/new-character',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    let digitalContents = null;
    let groupItem = null;
    if (req.query.id) {
      [digitalContents, groupItem] = await Promise.all([
        db.digitalContents
          .find({
            $and: [
              { groupId: common.getId(req.query.id) },
              {
                $or: [
                  { productSkus: { $eq: [] } },
                  { productSkus: { $eq: null } },
                ],
              },
            ],
          })
          .toArray(),
        db.digitalContentGroupType.findOne({ _id: common.getId(req.query.id) }),
      ]);
    }

    const allGroupType = await db.digitalContentGroupType.find({}).toArray();

    res.render('digital_content/digital_content_new', {
      title: 'New Game Character',
      menu: 'digital-content',
      allGroupType: allGroupType,
      digitalContents: digitalContents,
      groupItem: groupItem,
      digitalContentsString: JSON.stringify(digitalContents),
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      editor: true,
      admin: true,
      helpers: req.handlebars.helpers,
      config: req.app.config,
    });
  },
);

// render the editor
router.get(
  '/admin/character/edit/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;

    const digitalContent = await db.digitalContents.findOne({
      _id: common.getId(req.params.id),
    });

    if (!digitalContent) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/all-characters');
      return;
    }

    const groupItem = await db.digitalContentGroupType.findOne({
      _id: common.getId(digitalContent.groupId),
    });

    if (!groupItem) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/all-characters');
      return;
    }

    res.render('digital_content/digital_content_edit', {
      title: 'Edit Digital Content',
      menu: 'digital-content',
      digitalContent: digitalContent,
      groupItem: groupItem,
      admin: true,
      session: req.session,
      message: common.clearSessionValue(req.session, 'message'),
      messageType: common.clearSessionValue(req.session, 'messageType'),
      config: req.app.config,
      editor: true,
      helpers: req.handlebars.helpers,
    });
  },
);

router.post(
  '/admin/character/link-product',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const digitalContent = await db.digitalContents.findOne({
      _id: common.getId(req.body.digitalContentId),
    });

    if (!digitalContent) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/all-characters');
      return;
    }

    const groupItem = await db.digitalContentGroupType.findOne({
      _id: common.getId(digitalContent.groupId),
    });

    if (!groupItem || !req.body.productSku) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/all-characters');
      return;
    }

    if (req.body.productSku) {
      const skus = req.body.productSku.split(',');
      if (skus.length === 0) {
        req.session.message = 'SKU not found';
        req.session.messageType = 'danger';
        if (
          digitalContent.productSkus &&
          digitalContent.productSkus.length > 0
        ) {
          res.redirect(
            '/admin/digital-content/new-character?id=' + groupItem._id,
          );
        } else {
          res.redirect('/admin/character/edit/' + digitalContent._id);
        }
        return;
      }
      for (let i = 0; i < skus.length; i++) {
        const product = await db.products.findOne({
          productVariantSku: common.cleanHtml(skus[i].trim()),
        });
        if (!product) {
          req.session.message = 'SKU not found';
          req.session.messageType = 'danger';
          res.redirect(
            '/admin/digital-content/new-character?id=' + groupItem._id,
          );
          return;
        }
      }
    }

    const productSkus = req.body.productSku.split(',');
    // validate sku
    for (let i = 0; i < productSkus.length; i++) {
      const duplicateSku = await db.digitalContents.findOne({
        productSkus: { $elemMatch: { $eq: productSkus[i].trim() } },
      });
      if (
        duplicateSku &&
        duplicateSku.toString()._id !== digitalContent.toString()._id
      ) {
        req.session.message = 'SKU has been linked';
        req.session.messageType = 'danger';
        if (
          digitalContent.productSkus &&
          digitalContent.productSkus.length > 0
        ) {
          res.redirect('/admin/character/edit/' + digitalContent._id);
        } else {
          res.redirect(
            '/admin/digital-content/new-character?id=' + groupItem._id,
          );
        }
        return;
      }
    }
    digitalContent.productSkus = common
      .cleanHtml(req.body.productSku)
      .replace(' ', '')
      .split(',');
    digitalContent.isDisable = common.checkboxBool(req.body.isDisable);
    digitalContent.linkedBy = req.session.user;
    digitalContent.updatedAt = new Date();

    groupItem.lastUpdatedBy = req.session.user;
    groupItem.updatedAt = new Date();
    await Promise.all([
      db.digitalContentGroupType.update(
        { _id: groupItem._id },
        { $set: groupItem },
        { multi: false },
      ),
      db.digitalContents.update(
        { _id: common.getId(req.body.digitalContentId) },
        { $set: digitalContent },
        { multi: false },
      ),
    ]);

    await common.saveToActionLog(
      db,
      req.session.user,
      'linking character to sku',
      digitalContent,
    );

    req.session.message = 'Linked';
    req.session.messageType = 'success';
    res.redirect('/admin/all-characters?id=' + groupItem._id);
  },
);

router.get(
  '/admin/character/unlink-product/:id',
  common.restrict,
  common.checkAccess,
  async (req, res) => {
    const db = req.app.db;
    const digitalContent = await db.digitalContents.findOne({
      _id: common.getId(req.params.id),
    });

    if (!digitalContent) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/all-characters');
      return;
    }

    const groupItem = await db.digitalContentGroupType.findOne({
      _id: common.getId(digitalContent.groupId),
    });

    if (!groupItem) {
      req.session.message = 'Not Found';
      req.session.messageType = 'danger';
      res.redirect('/admin/all-characters');
      return;
    }

    digitalContent.productSkus = [];
    digitalContent.isDisable = false;
    digitalContent.unlinkedBy = req.session.user;
    digitalContent.updatedAt = new Date();

    groupItem.lastUpdatedBy = req.session.user;
    groupItem.updatedAt = new Date();
    await Promise.all([
      db.digitalContentGroupType.update(
        { _id: groupItem._id },
        { $set: groupItem },
        { multi: false },
      ),
      db.digitalContents.update(
        { _id: common.getId(req.params.id) },
        { $set: digitalContent },
        { multi: false },
      ),
    ]);

    await common.saveToActionLog(
      db,
      req.session.user,
      'unlinking character to sku',
      digitalContent,
    );

    req.session.message = 'Unlinked';
    req.session.messageType = 'success';
    res.redirect('/admin/all-characters?id=' + groupItem._id);
  },
);

module.exports = router;
