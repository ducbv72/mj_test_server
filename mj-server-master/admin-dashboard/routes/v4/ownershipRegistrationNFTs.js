const express = require('express');
const Sentry = require('@sentry/node');

const common = require('../../../lib/common');
const logger = require('../../../logger');
const {
  getTokenURIForOwnershipRegistration,
  burnNftForOwnershipRegistration,
} = require('../../../services/web3');

const router = express.Router();

const burnNft = async (req, res) => {
  try {
    if (!req.session.isAdmin) {
      req.session.message = 'Access denied.';
      req.session.messageType = 'danger';

      return res.redirect('/admin/users');
    }

    const tokenId = req.params.id;
    const db = req.app.db;

    if (
      tokenId === null ||
      tokenId === undefined ||
      typeof tokenId !== 'number'
    ) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_parameters',
      });
    }

    // Check token ID exists
    const tokenURI = await getTokenURIForOwnershipRegistration(tokenId);

    if (!tokenURI) {
      return res.status(500).json({
        status: 'failed',
        error_message: 'nonexistent_token_id',
      });
    }

    // Burn NFT
    const transactionHash = await burnNftForOwnershipRegistration(tokenId);

    if (!transactionHash) {
      return res.status(500).json({
        status: 'failed',
        error_message: 'invalid_tx_hash',
      });
    }

    // Update database to indicate the NFT is burned
    const query = { tokenId };
    const newData = {
      $set: {
        lastTxHash: transactionHash,
        isBurned: true,
        updatedAt: new Date(),
      },
    };
    await db.forrealNfts.updateOne(query, newData);

    const payload = {
      transactionHash,
      isBurned: true,
    };
    return res.status(200).json(payload);
  } catch (error) {
    logger.error(error);
    Sentry.captureException(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

router.get(
  '/admin/nft/ownership-registration/delete/:id',
  common.restrict,
  common.checkAccess,
  burnNft,
);

module.exports = router;
