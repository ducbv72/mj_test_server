const Config = require('../../../config');
const Promise = require('promise');
const request = require('request');
module.exports = {
  sync_product_stock: (url) => {
    return new Promise((resolve, reject) => {
      const headers = {
        Accept: 'application/json',
        Authorization: 'Bearer ' + Config.tradeGeckoToken,
      };
      const options = {
        url: url,
        method: 'GET',
        headers: headers,
      };
      request(options, (error, response, body) => {
        if (!error && response.statusCode === 200) {
          resolve(body);
        } else {
          reject(error);
        }
      });
    });
  },

  get_product_by_sku: (sku) => {
    return new Promise((resolve, reject) => {
      const headers = {
        Accept: 'application/json',
        Authorization: 'Bearer ' + Config.tradeGeckoToken,
      };
      const options = {
        url: 'https://api.tradegecko.com/variants?sku=' + sku,
        method: 'GET',
        headers: headers,
      };
      request(options, (error, response, body) => {
        if (!error && response.statusCode === 200) {
          resolve(body);
        } else {
          reject(error);
        }
      });
    });
  },
};
