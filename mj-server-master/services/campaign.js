const moment = require('moment');

const campaignHandle = async (db, user, product, certificate) => {
  const campaign = await db.campaigns.findOne({
    $and: [
      { isDisable: { $ne: 'true' } },
      { end_date: { $gte: new Date() } },
      { start_date: { $lte: new Date() } },
    ],
  });

  if (campaign) {
    const [participateCampaign, userScanedToday] = await Promise.all([
      db.campaignParticipates.findOne({
        $and: [
          { campaign_id: campaign.id },
          { udid: certificate.udid },
          {
            scan_date: moment(moment.now()).format('DD-MM-YYYY'),
          },
        ],
      }),
      db.campaignParticipates.findOne({
        $and: [
          { campaign_id: campaign.id },
          {
            customerId: user.customerId,
          },
          {
            scan_date: moment(moment.now()).format('DD-MM-YYYY'),
          },
        ],
      }),
    ]);

    if (!participateCampaign && !userScanedToday) {
      const newParticipate = {
        campaign_id: campaign.id,
        toy_id: product.productVariantSku,
        customerLocalId: user._id,
        customerId: user.customerId,
        scan_date: moment(moment.now()).format('DD-MM-YYYY'),
        createdAt: moment.now(),
        udid: certificate.udid,
        scanCount: 1,
      };

      await db.campaignParticipates.insert(newParticipate);
    }
  }
};

module.exports = {
  campaignHandle,
};
