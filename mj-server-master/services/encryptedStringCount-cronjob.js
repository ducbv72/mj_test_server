const Promise = require('promise');

module.exports = {
  update_encrypted_string_count_export:
    async function update_encrypted_string_count_export(app) {
      const db = app.db;
      const [products] = await Promise.all([
        db.products
          .find(
            {
              productVariantSku: { $ne: '' },
            },
            { productVariantSku: 1, _id: 1 },
          )
          .toArray(),
      ]);

      for (let i = 0; i < products.length; i++) {
        const item = products[i];
        const [itemPending, itemCompleted, itemTotal] = await Promise.all([
          db.qrcodes.count({
            $and: [
              { productId: item._id },
              { isSent: true },
              {
                $or: [{ udid: { $eq: null } }, { udid: { $eq: '' } }],
              },
            ],
          }),
          db.qrcodes.count({
            $and: [{ productId: item._id }, { udid: { $exists: true } }],
          }),
          db.qrcodes.count({ productId: item._id }),
        ]);

        db.products.update(
          { _id: item._id },
          {
            $set: {
              totalQrcode: itemTotal,
              completedQrcode: itemCompleted,
              pendingQrcode: itemPending,
            },
          },
          { multi: false },
        );
      }
    },
};
