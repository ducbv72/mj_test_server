const {
  availableSkus,
  voucherSku,
  listShiftCodeSku,
} = require('../config/scanToUnlockSku');

const scanToUnLockHandle = async (db, certificate) => {
  if (availableSkus.includes(certificate.sku)) {
    const gameAndShiftCodeData = {
      udid: certificate.udid,
      productVariantSku: certificate.sku,
      status: 'Unallocated',
      type: 'GameCode',
      email: certificate.orderEmail,
    };

    const checkGameCode = await db.gameAndShiftCode.findOne({
      productVariantSku: certificate.sku,
      email: certificate.orderEmail,
      udid: null,
    });
    if (checkGameCode) {
      await db.gameAndShiftCode.update(
        {
          productVariantSku: certificate.sku,
          email: certificate.orderEmail,
          udid: null,
        },
        { $set: gameAndShiftCodeData },
        { upsert: false },
      );
    } else {
      await db.gameAndShiftCode.update(
        {
          productVariantSku: certificate.sku,
          $or: [{ email: null }, { email: '' }],
          udid: null,
        },
        { $set: gameAndShiftCodeData },
        { upsert: false },
      );
    }
  }

  if (certificate.sku === voucherSku) {
    await db.voucherCodes.updateOne(
      {
        productVariantSku: certificate.sku,
        email: certificate.orderEmail,
        udid: '',
      },
      { $set: { udid: certificate.udid } },
      {},
    );
  }

  if (listShiftCodeSku.includes(certificate.sku)) {
    await db.gameAndShiftCode.update(
      {
        productVariantSku: certificate.sku,
        email: null,
        udid: null,
      },
      {
        $set: {
          productVariantSku: certificate.sku,
          email: certificate.orderEmail,
          udid: certificate.udid,
          status: 'Unallocated',
          type: 'ShiftCode',
        },
      },
      { upsert: false },
    );
  }
};

module.exports = {
  scanToUnLockHandle,
};
