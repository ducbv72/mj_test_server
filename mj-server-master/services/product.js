const moment = require('moment-timezone');
const common = require('../lib/common');
const { uploadOwnershipDataToIPFS } = require('./web3');
const ObjectId = require('mongodb').ObjectId;
const ipfsDag = require('ipfs-dag');
const decodeBase64 = (string) => {
  const buff = Buffer.from(string, 'base64');
  const customer_url = buff.toString('ascii');
  const shopifyId = customer_url.split('/')[4];
  return shopifyId.toString('base64');
};

const encodeBase64 = (string) => {
  const buffer = Buffer.from(String(string));
  return buffer.toString('base64');
};

const productQuery = () => `
        createdAt
        description
        descriptionHtml
        handle
        id
        title      
        tags     
        onlineStoreUrl           
        images(first: 100){
        edges{
            node{
            originalSrc
            transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
            }
        }
        }
        productType
        options{
        id
        name
        values
        }
        priceRange{
        maxVariantPrice{
            amount
            currencyCode
        }
        minVariantPrice{
            amount
            currencyCode
        }
        }
        variants(first: 100){
            edges{
                node{
                compareAtPrice
                id
                image{
                    originalSrc
                    transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
                }
                price
                sku
                selectedOptions{
                    name
                    value
                }
                title
                weight
                weightUnit
                }
            }
        }
`;

const parseProductFromServer = (product) => {
  return {
    id: encodeBase64(`gid://shopify/Product/` + product.shopifyProductId),
    shopifyProductId: product.shopifyProductId,
    title: product.productTitle,
    isFeature: product.productFeature,
    isNewest: product.productNew,
    isTrending: product.productTrending,
    isRecommend: product.productRecommendation,
    backgroundImageUrl: product.productBackgroundImageS3,
    productLaunchColor: product.productLaunchColor,
    productLaunchDate: product.productLaunchDate,
    productLaunchTime: product.productLaunchTime
      ? product.productLaunchTime
      : '00:00:00',
    productLaunchStatus: product.productLaunchStatus == 'true',
    productLaunchVipStatus: product.productLaunchVipStatus == 'true',
    productSoldOutDate: product.productSoldOutDate,
    productSoldOutTime: product.productSoldOutTime
      ? product.productSoldOutTime
      : '00:00:00',
    productSoldOutStatus: product.productSoldOutStatus == 'true',
    artistName: product.productArtist,
    titleOnapp: product.productTitleOnApp,
    sku: product.productVariantSku,
    currentDateUTC: moment.tz('Asia/Singapore').format('YYYY-MM-DD HH:mm:ss'),
    productCrossSellStatus: product.productCrossSellStatus,
    productCrossSellContent: encodeBase64(
      `gid://shopify/Product/` + product.productCrossSellContent,
    ),
    productCrossSellContentText: product.productCrossSellContentText,
  };
};

const parseProductShopifyWithServer = (product, findProduct) => {
  product.productLaunchColor = findProduct.productLaunchColor;
  product.productLaunchDate = findProduct.productLaunchDate;
  product.productLaunchStatus = findProduct.productLaunchStatus;
  product.productLaunchTime = findProduct.productLaunchTime;
  product.productLaunchVipStatus = findProduct.productLaunchVipStatus;
  product.productSoldOutDate = findProduct.productSoldOutDate;
  product.productSoldOutStatus = findProduct.productSoldOutStatus;
  product.productSoldOutTime = findProduct.productSoldOutTime;
  product.allowVIPEarlyAccess = findProduct.productLaunchVipStatus;
  product.backgroundImageUrl = findProduct.backgroundImageUrl;
  product.titleOnapp = findProduct.titleOnapp;
  product.timeGettingEvent = moment.utc().format('YYYY-MM-DD HH:mm:ss');
  product.productCrossSellStatus = findProduct.productCrossSellStatus;
  product.productCrossSellContent = findProduct.productCrossSellContent;
  product.productCrossSellContentText = findProduct.productCrossSellContentText;

  if (findProduct.productLaunchDate) {
    product.launchDateColor = findProduct.productLaunchColor;
  }

  if (findProduct.currentDateUTC) {
    product.currentDateUTC = moment
      .utc(findProduct.currentDateUTC)
      .format('YYYY-MM-DD HH:mm:ss');
  }

  if (!product.artistName && findProduct.artistName) {
    product.artistName = findProduct.artistName
      ? findProduct.artistName
      : product.title;
    product.title = findProduct.title;
  }

  if (findProduct.productLaunchStatus && findProduct.productLaunchDate) {
    product.availableForSale = false;
    product.productLaunchTime;
    product.launchDate =
      !findProduct.productLaunchDate || !findProduct.productLaunchTime
        ? undefined
        : moment(
            findProduct.productLaunchDate + ' ' + findProduct.productLaunchTime,
            'YYYY-MM-DD HH:mm:ss',
          )
            .tz('America/New_York')
            .toISOString();
    product.soldOutDate = undefined;
  }

  if (findProduct.productSoldOutStatus && findProduct.productSoldOutDate) {
    product.soldOutDate =
      !findProduct.productSoldOutDate || !findProduct.productSoldOutTime
        ? undefined
        : moment(
            findProduct.productSoldOutDate +
              ' ' +
              findProduct.productSoldOutTime,
            'YYYY-MM-DD HH:mm:ss',
          )
            .tz('Asia/Singapore')
            .toISOString();
    product.launchDate = undefined;
  }

  // set event variant
  if (!!findProduct.productSoldOutDate || !!findProduct.productLaunchDate) {
    product.availableForSale = findProduct.availableForSale;
    product.eventForVariantSkus = product.eventForVariantSkus
      ? product.eventForVariantSkus.concat(
          product.variants.edges.map((item) => ({
            sku: item.node.sku,
            isSoldOut: false,
          })) || [],
        )
      : product.variants.edges.map((item) => ({
          sku: item.node.sku,
          isSoldOut: false,
        }));
  }
  // end set

  if (product.currentDateUTC) {
    if (
      (product.launchDate &&
        moment(product.launchDate).isAfter(moment(product.currentDateUTC)) &&
        product.productLaunchStatus) ||
      !product.productSoldOutStatus
    ) {
      product.soldOutDate = undefined;
    }
    if (
      (product.soldOutDate &&
        moment(product.soldOutDate).isAfter(moment(product.currentDateUTC)) &&
        product.productSoldOutStatus) ||
      !product.productLaunchStatus
    ) {
      product.launchDate = undefined;
    }
  }
  return product;
};

const prepareProductDataToReturn = (product, compressData) => {
  (product.id = encodeBase64(
    `gid://shopify/Product/` + product.shopifyProductId,
  )),
    (product.title = product.productTitle);
  product.isFeature = product.productFeature;
  product.isNewest = product.productNew;
  product.isTrending = product.productTrending;
  product.isRecommend = product.productRecommendation;
  product.backgroundImageUrl = product.productBackgroundImageS3;
  product.productLaunchTime = product.productLaunchTime
    ? product.productLaunchTime
    : '00:00:00';
  product.productLaunchStatus = product.productLaunchStatus === 'true';
  product.productLaunchVipStatus = product.productLaunchVipStatus === 'true';
  product.productSoldOutTime = product.productSoldOutTime
    ? product.productSoldOutTime
    : '00:00:00';
  product.productSoldOutStatus = product.productSoldOutStatus === 'true';
  product.artistName = product.productArtist;
  product.titleOnapp = product.productTitleOnApp;
  product.sku = product.productVariantSku;
  product.currentDateUTC = moment
    .tz('Asia/Singapore')
    .format('YYYY-MM-DD HH:mm:ss');
  product.timeGettingEvent = moment.utc().format('YYYY-MM-DD HH:mm:ss');
  product.descriptionHtml = product.productDescription;
  product.tags = product.productTags?.split(',');
  product.handle = product.productHandle;
  product.title = product.productTitle;
  if (product.currentDateUTC) {
    product.currentDateUTC = moment
      .utc(product.currentDateUTC)
      .format('YYYY-MM-DD HH:mm:ss');
  }

  if (!product.artistName && product.artistName) {
    product.artistName = product.artistName
      ? product.artistName
      : product.title;
  }

  if (product.productLaunchStatus && product.productLaunchDate) {
    product.availableForSale = false;
    product.productLaunchTime;
    product.launchDate =
      !product.productLaunchDate || !product.productLaunchTime
        ? undefined
        : moment(
            product.productLaunchDate + ' ' + product.productLaunchTime,
            'YYYY-MM-DD HH:mm:ss',
          )
            .tz('America/New_York')
            .toISOString();
    product.soldOutDate = undefined;
  }

  if (product.productSoldOutStatus && product.productSoldOutDate) {
    product.soldOutDate =
      !product.productSoldOutDate || !product.productSoldOutTime
        ? undefined
        : moment(
            product.productSoldOutDate + ' ' + product.productSoldOutTime,
            'YYYY-MM-DD HH:mm:ss',
          )
            .tz('Asia/Singapore')
            .toISOString();
    product.launchDate = undefined;
  }
  // set event variant
  if (!!product.productSoldOutDate || !!product.productLaunchDate) {
    product.eventForVariantSkus = product.eventForVariantSkus
      ? product.eventForVariantSkus.concat(
          product.variants.edges.map((item) => ({
            sku: item.node.sku,
            isSoldOut: false,
          })) || [],
        )
      : product.variants.edges.map((item) => ({
          sku: item.node.sku,
          isSoldOut: false,
        }));
  }
  // end set

  if (product.currentDateUTC) {
    if (
      (product.launchDate &&
        moment(product.launchDate).isAfter(moment(product.currentDateUTC)) &&
        product.productLaunchStatus) ||
      !product.productSoldOutStatus
    ) {
      product.soldOutDate = undefined;
    }
    if (
      (product.soldOutDate &&
        moment(product.soldOutDate).isAfter(moment(product.currentDateUTC)) &&
        product.productSoldOutStatus) ||
      !product.productLaunchStatus
    ) {
      product.launchDate = undefined;
    }
  }
  delete product.productVariantSku;
  delete product.productPermalink;
  delete product.udid;
  delete product.productStore;
  delete product.availableForSale;
  delete product.isRecommend;
  delete product.isTrending;
  delete product.isNewest;
  delete product.isFeature;
  delete product.vendor;
  delete product.createdAt;
  delete product.productType;
  delete product.options;
  delete product.productAssetsFile;
  delete product.enableEditionNumber;
  delete product.stock_on_hand;
  delete product.stockxSlug;
  delete product.stockxLink;
  delete product.productUrl;
  delete product.productIsHomePopup;
  delete product.isShowStockxBanner;
  delete product.totalQrcode;
  delete product.pendingQrcode;
  delete product.completedQrcode;
  delete product.productBackgroundImageS3;
  delete product.productBackgroundImage;
  delete product.productImageUrl;
  delete product.productPublishedAt;
  delete product.productFeature;
  delete product.productTrending;
  delete product.productNew;
  delete product.productAddedDate;
  delete product.productStock;
  delete product.productComment;
  delete product.productOptions;
  delete product.productPublished;
  delete product.productVariantId;
  delete product.productRecommendation;
  delete product.productEditionNumber;
  delete product.productModelNumber;
  delete product._id;
  delete product.handle;
  delete product.productOrder;
  delete product.productDescription;
  delete product.productHandle;
  delete product.productTags;
  if (compressData) {
    delete product.eventForVariantSkus;
    delete product.productSpecification;
    delete product.zendeskUrl;
    delete product.variants;
    delete product.descriptionHtml;
    delete product.description;
    delete product.productDescription;
    delete product.handle;
    delete product.backgroundImageUrl;
    delete product.productBackgroundImageUrlS3;
    delete product.productImageUrlS3;
  }
  return product;
};

const mergeCreepyCuties = async (productDoc) => {
  const ccProduct = await common.getCreepyCutiesProductsInfo(
    productDoc.productVariantSku,
  );

  if (ccProduct) {
    productDoc.productAltarOptions = ccProduct.productAltarOptions;

    if (!productDoc.productImageUrl) {
      productDoc.productImageUrl = ccProduct.productImageUrl;
    }

    if (!productDoc.productImageUrlS3) {
      productDoc.productImageUrlS3 = ccProduct.productImageUrlS3;
    }

    if (!productDoc.productBackgroundImageS3) {
      productDoc.productBackgroundImageS3 = ccProduct.productBackgroundImageS3;
    }
  }
  return productDoc;
};

const handleCertificate = async (
  db,
  user,
  product,
  customerShopify,
  certificate,
  encrypted,
) => {
  const currentDate = new Date();
  let datePublish = new Date(
    product.productPublishedAt
      ? product.productPublishedAt
      : product.productAddedDate,
  );
  if (datePublish == null) {
    datePublish = currentDate;
  }
  const certificateNumber =
    'MJMP' +
    datePublish.getFullYear() +
    Math.ceil(currentDate.getTime() / 1000).toString();
  
  //temporary generate data which is allow showing correctly on mobile. we will refractory it later on server/mobile
  const transactionHash = new ipfsDag.Node(certificateNumber).multihash;
  const tokenId = '';
  const status = '';
  const {
    productTitle: productName,
    productVariantSku: productSKU,
    udid: UUID,
  } = product;
  let tokenURI = '';

  try {
    tokenURI = await uploadOwnershipDataToIPFS(productName, productSKU, UUID);
  } catch {
    // Rate limit reached. A cron job will try again later on
  }

  await Promise.all([
    db.orders.update(
      {
        'orderProducts.productEncryptedModel': encrypted,
      },
      {
        $set: {
          orderUserName: user.userName,
          orderEmail: customerShopify.email,
          orderFirstname: customerShopify.first_name
            ? customerShopify.first_name
            : '',
          orderLastname: customerShopify.last_name
            ? customerShopify.last_name
            : '',
          'orderProducts.$.blockHash': transactionHash,
          orderCertNumber: certificateNumber,
          'orderProducts.$.productCertNumber': certificateNumber,
          orderBlockHash: transactionHash,
          orderSmartContract: transactionHash,
          'orderProducts.$.orderSmartContract': transactionHash,
          'orderProducts.$.smartContract': transactionHash,
          customerId: customerShopify.id.toString(),
        },
      },
    ),
    // Save data to DB. A cron job will use that data to mint the NFT
    db.forrealNfts.insert({
      customerId: customerShopify.id.toString(),
      orderId: certificate._id,
      lastTxHash: '',
      tokenId,
      status,
      tokenURI,
      updatedAt: new Date(),
      productName,
      productSKU,
      UUID,
    }),
  ]);

  // reload certificate data
  return await db.orders.findOne({ _id: ObjectId(certificate._id) });
};

module.exports = {
  productQuery,
  parseProductFromServer,
  parseProductShopifyWithServer,
  decodeBase64,
  prepareProductDataToReturn,
  mergeCreepyCuties,
  handleCertificate,
  encodeBase64,
};
