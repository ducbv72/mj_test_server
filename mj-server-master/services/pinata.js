const axios = require('axios');
const FormData = require('form-data');

const { PINATA } = require('../config/nft');

const { KEY, SECRET } = PINATA;

const pinJSONToIPFS = async (JSONBody) => {
  const url = 'https://api.pinata.cloud/pinning/pinJSONToIPFS';

  const response = await axios.post(url, JSONBody, {
    headers: {
      pinata_api_key: KEY,
      pinata_secret_api_key: SECRET,
    },
  });

  const { IpfsHash } = response.data;

  if (!IpfsHash || typeof IpfsHash !== 'string') {
    throw new Error('Invalid IpfsHash');
  }

  return `ipfs://${IpfsHash}`;
};

const pinFileToIPFS = async (file, filename) => {
  const url = `https://api.pinata.cloud/pinning/pinFileToIPFS`;
  const metadata = JSON.stringify({
    name: filename,
  });
  const data = new FormData();

  data.append('file', file);
  data.append('pinataMetadata', metadata);

  const response = await axios.post(url, data, {
    maxBodyLength: 'Infinity', // this is needed to prevent axios from erroring out with large files
    headers: {
      'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
      pinata_api_key: KEY,
      pinata_secret_api_key: SECRET,
    },
  });

  const { IpfsHash } = response.data;

  if (!IpfsHash || typeof IpfsHash !== 'string') {
    throw new Error('Invalid IpfsHash');
  }

  return `ipfs://${IpfsHash}`;
};

module.exports = {
  pinJSONToIPFS,
  pinFileToIPFS,
};
