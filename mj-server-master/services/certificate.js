const Config = require('../config/ethereumConfig');
const common = require('../lib/common');
const ipfsDag = require('ipfs-dag');
const { mergeCreepyCuties } = require('./product');

const searchCertificate = async (db, keyword, filterType, sortType, skip) => {
  let certificates = [];
  let count = 0;
  console.log(filterType);
  switch (filterType) {
    case 'udid':
      [certificates, count] = await filterBySkuOrUdidCondition(
        db,
        keyword,
        sortType,
        filterType,
        skip,
      );
      break;
    case 'sku':
      [certificates, count] = await filterBySkuOrUdidCondition(
        db,
        keyword,
        sortType,
        filterType,
        skip,
      );
      break;
    case 'email':
      [certificates, count] = await filterByEmailOrUseNameCondition(
        db,
        keyword,
        sortType,
        filterType,
        skip,
      );
      break;
    default:
    case 'username':
      [certificates, count] = await filterByEmailOrUseNameCondition(
        db,
        keyword,
        sortType,
        filterType,
        skip,
      );
      break;
  }

  return [certificates, count];
};

const registerOwnerShip = async (db, user, customer, certificate, udidItem) => {
  if (certificate) {
    if (!certificate.oldOwner) {
      certificate.oldOwner = [];
    }

    await common.saveToActionLog(db, user, 'change ownership', certificate);

    certificate.oldOwner.push({
      userName: certificate.orderUserName,
      email: certificate.orderEmail,
      customerId: certificate.customerId,
    });
    certificate.customerId = customer.customerId;
    certificate.orderEmail = customer.email;
    certificate.firstName = customer.firstName;
    certificate.lastName = customer.lastName;
    certificate.orderUserName = customer.userName;

    await db.orders.update(
      { _id: common.getId(certificate._id) },
      { $set: certificate },
      { multi: false },
    );
    return true;
  }

  let product = await db.products.findOne({
    productVariantSku: udidItem.productVariantSku,
  });
  if (!product) {
    return false;
  }
  product.qrcode = udidItem.qrcodeImage;
  product.udid = udidItem.udid;
  product.password = udidItem.password ? udidItem.password : '';
  product.productModelNumber = product.productVariantSku;
  product.productEncryptedModel = udidItem.encrytedMessage;

  const currentDate = new Date();
  let datePublish = new Date(
    product.productPublishedAt
      ? product.productPublishedAt
      : product.productAddedDate,
  );
  if (datePublish == null) {
    datePublish = currentDate;
  }
  const certificateNumber =
    'MJMP' +
    datePublish.getFullYear() +
    Math.ceil(currentDate.getTime() / 1000).toString();
  const dataToHash =
    certificateNumber + ',' + customer.email + ',' + product.productVariantSku;
  const blockHash = new ipfsDag.Node(dataToHash).multihash;
  product.blockHash = blockHash;
  product.orderSmartContract = blockHash;
  product.smartContract = blockHash;
  product.productCertNumber = certificateNumber;
  product.recipientImage = '';

  //merge CreepyCuties
  product = await mergeCreepyCuties(product);

  console.log(product);

  const certificateDoc = {
    shopifyOrderId: new Date().getTime().toString(),
    customerId_encryted: customer.customerId,
    customerId: customer.customerId,
    orderEmail: customer.email,
    orderFirstname: customer.firstName,
    orderLastname: customer.lastName,
    orderUserName: customer.userName,
    orderAddr1: '',
    orderAddr2: '',
    orderCountry: '',
    orderState: '',
    orderStatus: 'Paid',
    orderOffline: false,
    orderPostcode: '',
    orderPhoneNumber: '',
    orderComment: '',
    orderDate: new Date(),
    orderProducts: [product],
    line_id: '',
    shopifyProductId: '',
    variant_id: product.productVariantId,
    title: product.productTitle,
    variant_title: product.productTitle,
    product_name: product.productTitle,
    sku: product.productVariantSku,
    encrytedMessage: udidItem.encrytedMessage,
    udid: udidItem.udid,
    editionNumber: udidItem.editionNumber,
    orderSmartContract: blockHash,
    orderBlockHash: blockHash,
    orderCertNumber: certificateNumber,
  };

  await db.orders.insert(certificateDoc);
  await common.saveToActionLog(db, user, 'register ownership', certificateDoc);
  return true;
};

async function filterBySkuOrUdidCondition(
  db,
  keyword,
  sortType,
  filterType,
  skip,
) {
  let certificates = [];
  let count = 0;
  let condition = {};
  switch (filterType) {
    case 'udid':
      condition = [
        { udid: keyword },
        { productVariantSku: { $ne: null } },
        { productVariantSku: { $ne: '' } },
      ];
      break;
    case 'sku':
      condition = [
        { productVariantSku: keyword },
        { udid: { $ne: null } },
        { udid: { $ne: '' } },
      ];
      break;
  }

  [certificates, count] = await Promise.all([
    db.qrcodes
      .aggregate([
        {
          $match: {
            $and: condition,
          },
        },
        {
          $lookup: {
            from: 'orders',
            let: { udid: '$udid' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ['$udid', '$$udid'] },
                      { $ne: ['$orderCertNumber', null] },
                      { $ne: ['$orderCertNumber', ''] },
                    ],
                  },
                },
              },
              {
                $project: {
                  orderDate: '$orderDate',
                  orderCertNumber: '$orderCertNumber',
                  orderUserName: '$orderUserName',
                  orderEmail: '$orderEmail',
                },
              },
            ],
            as: 'certificate',
          },
        },
        {
          $project: {
            udid: '$udid',
            sku: '$productVariantSku',
            orderCertNumber: { $first: '$certificate.orderCertNumber' },
            orderUserName: { $first: '$certificate.orderUserName' },
            orderEmail: { $first: '$certificate.orderEmail' },
            orderDate: { $first: '$certificate.orderDate' },
          },
        },
      ])
      .sort({ orderDate: Number(sortType), sku: 1, orderUserName: 1 })
      .skip(skip)
      .limit(Config.productLitmitPerPage)
      .toArray(),
    db.qrcodes.count({ $and: condition }),
  ]);
  return [certificates, count];
}

async function filterByEmailOrUseNameCondition(
  db,
  keyword,
  sortType,
  filterType,
  skip,
) {
  let certificates = [];
  let count = 0;
  const conditions = [
    { orderCertNumber: { $exists: true } },
    { orderCertNumber: { $ne: '' } },
    { udid: { $ne: '' } },
    { udid: { $ne: null } },
    { orderCertNumber: { $ne: null } },
  ];
  switch (filterType) {
    case 'username':
      conditions.push({ orderUserName: new RegExp(keyword, 'i') });
      break;
    case 'email':
      conditions.push({ orderEmail: new RegExp(keyword, 'i') });
      break;
  }
  [certificates, count] = await Promise.all([
    db.orders
      .find(
        {
          $and: conditions,
        },
        {
          projection: {
            udid: 1,
            sku: 1,
            orderUserName: 1,
            orderDate: 1,
            orderEmail: 1,
            orderCertNumber: 1,
          },
        },
      )
      .sort({ orderDate: Number(sortType) })
      .skip(skip)
      .limit(Config.productLitmitPerPage)
      .toArray(),
    db.orders.countDocuments({
      $and: conditions,
    }),
  ]);
  return [certificates, count];
}

module.exports = {
  searchCertificate,
  registerOwnerShip,
};
