const Sentry = require('@sentry/node');

const {
  mintNftForOwnershipRegistration,
  getTokenURIForOwnershipRegistration,
  checkTransactionReceiptStatus,
  resetNonceForreal,
  uploadOwnershipDataToIPFS,
} = require('./web3');

const retryMintNFTs = async (nftArray, db) => {
  const length = nftArray.length || 0;

  for (let index = 0; index < length; index++) {
    try {
      const _id = nftArray[index]?._id;
      const productName = nftArray[index]?.productName;
      const productSKU = nftArray[index]?.productSKU;
      const UUID = nftArray[index]?.UUID;
      let tokenURI = nftArray[index]?.tokenURI;

      if (!tokenURI && productName && productSKU && UUID) {
        try {
          tokenURI = await uploadOwnershipDataToIPFS(
            productName,
            productSKU,
            UUID,
          );
        } catch {
          // Rate limit reached. Try again later
        }
      }

      if (!tokenURI || typeof tokenURI !== 'string' || !_id) {
        console.log('Invalid tokenURI or _id');
        continue;
      }

      const { tokenId, transactionHash } =
        await mintNftForOwnershipRegistration(tokenURI);

      if (!transactionHash || !tokenId) {
        console.log('Invalid transactionHash or tokenId');
        continue;
      }

      const result = {
        status: 'success',
        lastTxHash: transactionHash,
        tokenId,
        retryMintCronjob: true,
        updatedAt: new Date(),
        tokenURI,
      };

      await db.forrealNfts.updateOne({ _id }, { $set: result });
    } catch (error) {
      console.log('Error inside for loop:', error);
      Sentry.captureException(error);
    }
  }
};

const retryMintForrealNFTCronjob = async (app) => {
  const db = app.db;

  const nftsWithUnvalidatedTxs = await db.forrealNfts
    .find({
      validatedTx: { $ne: true },
      status: 'success',
    })
    .toArray();

  console.log({ nftsWithUnvalidatedTxsLength: nftsWithUnvalidatedTxs.length });

  const promises = nftsWithUnvalidatedTxs.map(
    async ({ _id, lastTxHash }, index) => {
      if (!lastTxHash) return;

      const isValidTx = await checkTransactionReceiptStatus(lastTxHash, index);
      console.log({
        isValidTx,
        remaining: nftsWithUnvalidatedTxs.length - index - 1,
      });

      if (isValidTx === true) {
        const result = {
          validatedTx: true,
          updatedAt: new Date(),
        };

        await db.forrealNfts.updateOne({ _id }, { $set: result });
      } else if (isValidTx === false) {
        const result = {
          status: 'error',
          updatedAt: new Date(),
        };

        await db.forrealNfts.updateOne({ _id }, { $set: result });
      } else if (isValidTx === null) {
        // Not able to validate the tx at this time, check later
      }
    },
  );

  await Promise.all(promises);

  const nftsWithErrors = await db.forrealNfts
    .find({
      status: { $ne: 'success' },
    })
    .toArray();

  console.log({ nftsWithErrorsLength: nftsWithErrors.length });

  await retryMintNFTs(nftsWithErrors, db);
  resetNonceForreal();

  console.log('retryMintForrealNFTCronjob -> Done 🎉');
};

module.exports = retryMintForrealNFTCronjob;
