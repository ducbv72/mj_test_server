const { shopify } = require('../config/shopify');
const queryCustomerByPhone = `
  {
    customers(first: 1, query:"phone:{phone}") {
      edges {
        node {
          id
          email
        }
      }
    }
  }
`;

const queryCustomerByEmail = `
  {
    customers(first: 1, query:"email:{email}") {
      edges {
        node {
          id
          email
          defaultAddress {
            id
          }
        }
      }
    }
  }
`;

const queryCustomerFullByEmail = `
  {
    customers(first: 1, query:"email:{email}") {
      edges {
        node {
          id
          email
          addresses {
            id
            firstName
            lastName
            company
            address1
            address2
            city
            country
            province
            zip
            phone
            name
            provinceCode
            countryCodeV2

          }
          defaultAddress {
            id
            firstName
            lastName
            company
            address1
            address2
            city
            country
            province
            zip
            phone
            name
            provinceCode
            countryCodeV2
          }
        }
      }
    }
  }
`;

// Check valid password
const checkPasswordValid = (body) => {
  if (!body.password) {
    return { code: 400, message: 'Please enter your password' };
  }

  if (!body.password_confirmation) {
    return {
      code: 400,
      message: 'Please enter your password confirmation',
    };
  }

  const passwordRegex =
    /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

  if (
    !passwordRegex.test(body.password) ||
    !passwordRegex.test(body.password_confirmation)
  ) {
    return {
      code: 400,
      message:
        'Password must contain at least 8 characters, 1 number, 1 lower, 1 uppercase letters and 1 special characters',
    };
  }

  if (body.password !== body.password_confirmation) {
    return {
      code: 400,
      message: 'Password confirm does not match password',
    };
  }

  return { code: 200 };
};

// Check valid data
const checkValidCreateCustomer = (body) => {
  if (!body.first_name) {
    return { code: 400, message: 'Please enter your first name' };
  }

  if (!body.last_name) {
    return { code: 400, message: 'Please enter your last name' };
  }

  if (!body.email) {
    return { code: 400, message: 'Please enter your email' };
  }

  const emailRegexp =
    /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

  if (!emailRegexp.test(body.email)) {
    return { code: 400, message: 'Please enter a valid email address' };
  }

  const valid_password = checkPasswordValid(body);

  if (valid_password.code !== 200) {
    return valid_password;
  }

  return { code: 200 };
};

// Create shopify customer
// shopify is store that created the customer (shopify)
// shopify_create is store that need to create the customer to sync data (shopify_create)
const createShopifyCustomer = async (shopify, shopify_create, body) => {
  const valid = checkValidCreateCustomer(body);

  if (valid.code !== 200) {
    return valid;
  }

  let shopify_customer_exist = false;
  let shopify_create_customer_exist = false;

  await shopify_create
    .graphql(queryCustomerByEmail.replace('{email}', body.email))
    .then(async (customer) => {
      if (customer.customers.edges.length) {
        shopify_create_customer_exist = true;
      }
    });

  await shopify
    .graphql(queryCustomerByEmail.replace('{email}', body.email))
    .then(async (customer) => {
      if (customer.customers.edges.length) {
        shopify_customer_exist = true;
      }
    });

  if (shopify_customer_exist || shopify_create_customer_exist) {
    return {
      code: 400,
      message: 'This email address is already being used',
    };
  }

  let document = {};

  await shopify_create.customer
    .create(body)
    .then(() => {
      document = { code: 200, message: 'Create customer success' };
    })
    .catch((err) => {
      console.log(err);
      document = { code: 404, message: err.message };
    });

  return document;
};

// Update shopify customer
// shopify is store that need to update
// body is data from webhook
const updateShopifyCustomerAddress = async (shopify, body) => {
  let document = {};
  const new_customer_address = body.addresses.map((v) => {
    return JSON.stringify({
      first_name: v.first_name || '',
      last_name: v.last_name || '',
      company: v.company || '',
      address1: v.address1 || '',
      address2: v.address2 || '',
      city: v.city || '',
      province: v.province || '',
      country: v.country || '',
      zip: v.zip || '',
      phone: v.phone || '',
      name: v.name || '',
      province_code: v.province_code || '',
      country_code: v.country_code || '',
      default: v.default,
    });
  });

  await shopify
    .graphql(queryCustomerFullByEmail.replace('{email}', body.email))
    .then(async (customer) => {
      if (customer.customers.edges.length) {
        const _customer = customer.customers.edges[0].node;
        const customer_id = _customer.id.split('/');
        const old_customer_address = _customer.addresses.map((v) => {
          const address_gid = v.id.split('/');
          const address_id = address_gid[address_gid.length - 1].split('?');
          const is_default =
            _customer.defaultAddress && v.id === _customer.defaultAddress.id;

          return {
            address_id: Number(address_id[0]),
            data: JSON.stringify({
              first_name: v.firstName || '',
              last_name: v.lastName || '',
              company: v.company || '',
              address1: v.address1 || '',
              address2: v.address2 || '',
              city: v.city || '',
              province: v.province || '',
              country: v.country || '',
              zip: v.zip || '',
              phone: v.phone || '',
              name: v.name || '',
              province_code: v.provinceCode || '',
              country_code: v.countryCodeV2 || '',
              default: is_default,
            }),
          };
        });

        let new_address_change;
        let old_address_change;

        for (let address of new_customer_address) {
          if (!old_customer_address.map((v) => v.data).includes(address)) {
            new_address_change = JSON.parse(address);
            break;
          }
        }

        for (let address of old_customer_address) {
          if (!new_customer_address.includes(address.data)) {
            old_address_change = address;
            break;
          }
        }

        if (!new_address_change && !old_address_change) {
          document = { code: 200, data: 'Success' };
        }

        if (new_address_change && !old_address_change) {
          delete new_address_change.default;
          await shopify.customerAddress
            .create(
              Number(customer_id[customer_id.length - 1]),
              new_address_change,
            )
            .then(() => {
              console.log('success');
              document = { code: 200, message: 'Create address success' };
            })
            .catch((err) => {
              console.log(err);
              document = { code: 400, message: 'Create address failed' };
            });
        }

        if (!new_address_change && old_address_change) {
          await shopify.customerAddress
            .delete(
              Number(customer_id[customer_id.length - 1]),
              old_address_change.address_id,
            )
            .then(() => {
              console.log('success');
              document = { code: 200, message: 'Delete address success' };
            })
            .catch((err) => {
              console.log(err);
              document = { code: 400, message: 'Delete address failed' };
            });
        }

        if (new_address_change && old_address_change) {
          delete new_address_change.default;
          await shopify.customerAddress
            .update(
              Number(customer_id[customer_id.length - 1]),
              old_address_change.address_id,
              new_address_change,
            )
            .then(() => {
              console.log('success');
              document = { code: 200, message: 'Update address success' };
            })
            .catch((err) => {
              console.log(err);
              document = { code: 400, message: 'Update address failed' };
            });
        }

        return;
      }

      document = { code: 404, message: 'Customer not found' };
    });

  return document;
};

// Get customer
// shopify is store that need to find customer
const getCustomer = async (shopify, email) => {
  let customer = {};

  await shopify
    .graphql(queryCustomerByEmail.replace('{email}', email))
    .then((_customer) => {
      if (_customer.customers.edges.length) {
        const customer_id = _customer.customers.edges[0].node.id.split('/');
        let address_id = null;

        if (_customer.customers.edges[0].node.defaultAddress) {
          const address_gid =
            _customer.customers.edges[0].node.defaultAddress.id.split('/');
          address_id = address_gid[address_gid.length - 1].split('?');
        }

        customer = {
          customer_id: Number(customer_id[customer_id.length - 1]),
          address_id: address_id ? Number(address_id[0]) : null,
          email,
        };
      }
    });

  return customer;
};

// Update shopify customer profile address
// shopify is store that need to update customer address
// customer is person that update address
const updateShopifyCustomerProfileAddress = async (shopify, customer, body) => {
  const customerAddress = {};

  if (body.country !== undefined) {
    customerAddress.country = body.country;
  }

  if (body.country_code !== undefined) {
    customerAddress.country_code = body.country_code;
  }

  if (customer.address_id) {
    if (Object.keys(customerAddress).length > 0) {
      await shopify.customerAddress
        .update(customer.customer_id, customer.address_id, customerAddress)
        .catch((err) => {
          console.log(5, err);
          return err.message;
        });
    }
  } else {
    const _customerAddress = { ...customerAddress };

    if (body.first_name !== undefined) {
      _customerAddress.first_name = body.first_name;
    }

    if (body.last_name !== undefined) {
      _customerAddress.last_name = body.last_name;
    }

    if (Object.keys(_customerAddress).length > 0) {
      await shopify.customerAddress
        .create(customer.customer_id, _customerAddress)
        .catch((err) => {
          console.log(3, err);
          return err.message;
        });
    }
  }
};

// Change password shopify customer
// shopify is store that need to update customer password
const changePasswordShopifyCustomer = async (shopify, body) => {
  let document = {};

  await shopify
    .graphql(queryCustomerByEmail.replace('{email}', body.email))
    .then(async (customer) => {
      if (customer.customers.edges.length) {
        let customer_id = customer.customers.edges[0].node.id.split('/');
        customer_id = Number(customer_id[customer_id.length - 1]);

        return await shopify.customer
          .update(customer_id, {
            password: body.password,
            password_confirmation: body.password_confirmation,
          })
          .then(() => {
            document = { code: 200, message: 'Change password success' };
          })
          .catch((err) => {
            console.log(err);
            document = { code: 400, message: 'Change password failed' };
          });
      }

      document = { code: 404, message: 'Customer not found' };
    });

  return document;
};

// Get customer by email or phone number
const getCustomerByEmailOrPhone = async (shopify, body) => {
  if (!body.email && !body.phone) {
    return false;
  }

  let is_customer_exist = false;

  if (body.email && body.email !== body.old_email) {
    await shopify
      .graphql(queryCustomerByEmail.replace('{email}', body.email))
      .then(async (_customer) => {
        if (_customer.customers.edges.length) {
          is_customer_exist = true;
        }
      });

    if (is_customer_exist) {
      return { field: 'Email' };
    }
  }

  if (body.phone) {
    await shopify
      .graphql(queryCustomerByPhone.replace('{phone}', body.phone))
      .then(async (_customer) => {
        if (_customer.customers.edges.length) {
          if (_customer.customers.edges[0].node.email !== body.old_email) {
            is_customer_exist = true;
          }
        }
      });

    if (is_customer_exist) {
      return { field: 'Phone number' };
    }
  }

  return is_customer_exist;
};

module.exports = {
  checkPasswordValid,
  createShopifyCustomer,
  updateShopifyCustomerAddress,
  getCustomer,
  updateShopifyCustomerProfileAddress,
  changePasswordShopifyCustomer,
  getCustomerByEmailOrPhone,
};
