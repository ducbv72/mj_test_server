const common = require('../lib/common');
const Promise = require('promise');
const fs = require('fs');
const json2xls = require('json2xls');
const Config = require('../config');
const archiver = require('archiver');
const adminFirebase = require('firebase-admin');

module.exports = {
  send_email_export: async function send_email_export(app) {
    const db = app.db;
    const [itemSendEmail] = await Promise.all([
      db.sendEmailToManufacture.findOne({ isSent: false }),
    ]);
    if (itemSendEmail) {
      const arraySku = itemSendEmail.arraySku;
      const arrPromise = [];
      const attachments = [];
      let i = 0;
      if (arraySku && arraySku.length > 0) {
        arraySku.forEach((skuitem) => {
          const singlePromise = new Promise(async (resolve) => {
            const [existedArray] = await Promise.all([
              db.qrcodes
                .find(
                  {
                    $and: [
                      {
                        $or: [{ isSent: false }, { isSent: null }],
                      },
                      { productVariantSku: skuitem.sku },
                      {
                        $or: [
                          { udid: '' },
                          { udid: null },
                          { udid: { $exists: false } },
                        ],
                      },
                    ],
                  },
                  {
                    projection: {
                      encrytedMessage: 1,
                      udid: 1,
                      password: 1,
                    },
                  },
                )
                .limit(parseInt(skuitem.send))
                .toArray(),
            ]);
            let newArrayOfObj = [];
            if (existedArray.length > 0) {
              const item = existedArray[0];
              if (item.udid === undefined) {
                item.udid = '';
              }
              if (item.password === undefined) {
                item.password = '';
              }
              newArrayOfObj = existedArray.map(
                ({ encrytedMessage, udid, password }) => ({
                  encryptedMessage: encrytedMessage,
                  udid: udid,
                  password: password,
                }),
              );
              const dataXLSX = json2xls(newArrayOfObj);
              const fileName = skuitem.sku + '.xlsx';
              const path = '/tmp/' + fileName;
              fs.writeFileSync(path, dataXLSX, 'binary');
              attachments.push({
                filename: fileName,
                path: path,
              });
              // update item become isSent
              const encryptedStrings = existedArray.map(
                (a) => a.encrytedMessage,
              );
              const [resultUpdate] = await Promise.all([
                db.qrcodes.update(
                  { encrytedMessage: { $in: encryptedStrings } },
                  {
                    $set: {
                      isSent: true,
                    },
                  },
                  { multi: true },
                ),
              ]);
            }
            resolve(i);
          });
          // run all promise
          arrPromise.push(singlePromise);
          i++;
        });
        Promise.all(arrPromise.map((p) => p.catch((e) => e)))
          .then(() => {
            common.sendEmail(
              itemSendEmail.emailManufacture,
              itemSendEmail.emailCC,
              itemSendEmail.subject,
              itemSendEmail.content,
              attachments,
            );
            console.log('send email done');
            db.sendEmailToManufacture.update(
              { _id: itemSendEmail._id },
              { $set: { isSent: true } },
              { multi: false },
              (err) => {
                if (err) {
                  console.log('something went wrong');
                } else {
                  console.log('updated complete');
                }
              },
            );
          }) // 1,Error: 2,3
          .catch((e) => console.log(e));
      }
    }
  },

  send_email_export_v2: async function send_email_export(app) {
    const db = app.db;
    const [itemSendEmail] = await Promise.all([
      db.sendEmailToManufacture.findOne({ isSent: false }),
    ]);
    if (itemSendEmail) {
      const arraySku = itemSendEmail.arraySku;
      const arrPromise = [];
      let i = 0;
      const currentDate = new Date();
      const zipFolderName = 'email_' + currentDate.valueOf();
      const zipFileName = zipFolderName + '.zip';
      //make a folder to save all xlsx file
      const dirZip = '/tmp/' + zipFolderName;
      if (!fs.existsSync(dirZip)) {
        fs.mkdirSync(dirZip);
      }
      if (arraySku && arraySku.length > 0) {
        arraySku.forEach((skuitem) => {
          const singlePromise = new Promise(async (resolve) => {
            const [existedArray] = await Promise.all([
              db.qrcodes
                .find(
                  {
                    $and: [
                      {
                        $or: [{ isSent: false }, { isSent: null }],
                      },
                      { productVariantSku: skuitem.sku },
                      {
                        $or: [
                          { udid: '' },
                          { udid: null },
                          { udid: { $exists: false } },
                        ],
                      },
                    ],
                  },
                  {
                    projection: {
                      encrytedMessage: 1,
                      udid: 1,
                      password: 1,
                    },
                  },
                )
                .limit(parseInt(skuitem.send))
                .toArray(),
            ]);
            let newArrayOfObj = [];
            if (existedArray.length > 0) {
              const item = existedArray[0];
              if (item.udid === undefined) {
                item.udid = '';
              }
              if (item.password === undefined) {
                item.password = '';
              }
              newArrayOfObj = existedArray.map(
                ({ encrytedMessage, udid, password }) => ({
                  encryptedMessage: encrytedMessage,
                  udid: udid,
                  password: password,
                }),
              );
              const dataXLSX = json2xls(newArrayOfObj);
              const currentDate = new Date();
              const fileName =
                skuitem.sku + '_' + currentDate.valueOf() + '.xlsx';
              const path = dirZip + '/' + fileName;
              fs.writeFileSync(path, dataXLSX, 'binary');

              // update item become isSent
              const encryptedStrings = existedArray.map(
                (a) => a.encrytedMessage,
              );
              const [resultUpdate] = await Promise.all([
                db.qrcodes.update(
                  { encrytedMessage: { $in: encryptedStrings } },
                  {
                    $set: {
                      isSent: true,
                    },
                  },
                  { multi: true },
                ),
              ]);
            }
            resolve(i);
          });
          // run all promise
          arrPromise.push(singlePromise);
          i++;
        });
        Promise.all(arrPromise.map((p) => p.catch((e) => e)))
          .then(async (results) => {
            try {
              // add url into email
              const zip = await zipDirectory(dirZip, '/tmp/' + zipFileName);

              if (fs.existsSync('/tmp/' + zipFileName)) {
                // upload file to s3
                const urlZipFileS3 = await common.uploadUdidRequestFileToS3(
                  '/tmp/' + zipFileName,
                  zipFileName,
                );
                //generate url with file management
                const downloadUrl =
                  process.env.FILE_MANAGEMENT_URL + zipFileName;

                const emailContent = itemSendEmail.content.replace(
                  Config.KEY_ZIP_DOWNLOAD_URL,
                  downloadUrl,
                );
                common.sendEmail(
                  itemSendEmail.emailManufacture,
                  itemSendEmail.emailCC,
                  itemSendEmail.subject,
                  emailContent,
                );
                console.log('send email done');
                db.sendEmailToManufacture.update(
                  { _id: itemSendEmail._id },
                  {
                    $set: {
                      isSent: true,
                      status: 'success',
                      downloadUrl: downloadUrl,
                      urlZipFileS3: urlZipFileS3,
                      fileName: zipFileName,
                    },
                  },
                  { multi: false },
                  (err, numReplaced) => {
                    if (err) {
                      console.log('something went wrong');
                    } else {
                      console.log('updated complete');
                    }
                  },
                );
              } else {
                db.sendEmailToManufacture.update(
                  { _id: itemSendEmail._id },
                  {
                    $set: {
                      isSent: true,
                      status: 'fail',
                      errorMessage: 'send fail. cannot find zip file',
                    },
                  },
                  { multi: false },
                  (err, numReplaced) => {
                    if (err) {
                      console.log('something went wrong');
                    } else {
                      console.log('updated complete');
                    }
                  },
                );
                console.log('send fail. cannot find zip file');
              }
            } catch (e) {
              console.log(e);
            }
          }) // 1,Error: 2,3
          .catch((e) => console.log(e));
      }
    }
  },

  check_schedule_notification: async function check_schedule_notification(app) {
    const db = app.db;

    try {
      const [notification] = await Promise.all([
        db.scheduleNotifications.findOne({
          $and: [{ isSent: false }, { scheduleTime: { $lte: new Date() } }],
        }),
      ]);
      if (notification) {
        const messages = JSON.parse(notification.content);
        if (!Array.isArray(messages)) {
          notification.isSent = true;
          notification.status = 'failure';
          notification.sendDate = new Date();
          await Promise.all([
            db.scheduleNotifications.update(
              { _id: notification._id },
              { $set: notification },
              {},
            ),
            common.saveToActionLog(
              db,
              'System',
              'send notification',
              notification,
            ),
          ]);

          console.log('send notification: ' + notification._id);
          return;
        }
        try {
          await adminFirebase.messaging().sendAll(messages);
          notification.isSent = true;
          notification.status = 'success';
          notification.sendDate = new Date();
          await Promise.all([
            db.scheduleNotifications.update(
              { _id: notification._id },
              { $set: notification },
              {},
            ),
          ]);
          common.saveToActionLog(
            db,
            notification.createdBy,
            'send notification',
            notification,
          );
          console.log('send notification: ' + notification._id);
        } catch (error) {
          console.log('error sending message:', error);
          notification.isSent = true;
          notification.status = 'fail';
          notification.error = 'error';
          notification.sendDate = new Date();
          await Promise.all([
            db.scheduleNotifications.update(
              { _id: notification._id },
              { $set: notification },
              {},
            ),
          ]);

          common.saveToActionLog(
            db,
            notification.createdBy,
            'send notification',
            notification,
          );
          console.log(
            'send notification failure: ' +
              notification._id +
              ' ' +
              error.message,
          );
        }
      } else {
        console.log('no schedule notification');
      }
    } catch (ex) {
      console.log(ex);
    }
  },
};

/**
 * @param {String} source
 * @param {String} out
 * @returns {Promise}
 */
function zipDirectory(source, out) {
  const archive = archiver('zip', { zlib: { level: 9 } });
  const stream = fs.createWriteStream(out);
  return new Promise((resolve, reject) => {
    archive
      .directory(source, false)
      .on('error', (err) => reject(err))
      .pipe(stream);
    stream.on('close', () => resolve());
    archive.finalize();
  });
}
