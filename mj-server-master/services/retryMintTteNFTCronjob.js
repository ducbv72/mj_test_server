const Sentry = require('@sentry/node');
const Web3 = require('web3');

const { TTE_NFT_VARIANTS } = require('../config/nft');

const {
  mintTteNft,
  checkTransactionReceiptStatus,
  uploadTteNftMetadataToIpfs,
  resetNonceTte,
} = require('./web3');

const retryMintNFTs = async (nftArray, db) => {
  const length = nftArray.length || 0;

  for (let index = 0; index < length; index++) {
    try {
      const _id = nftArray[index]?._id;

      const ethAddress = Web3.utils.toChecksumAddress(
        nftArray[index]?.ethAddress,
      );
      const sku = nftArray[index]?.sku;
      const nftName = nftArray[index]?.nftName;
      const quantity = 1;
      let tokenURI = nftArray[index]?.tokenURI;

      // Check if NFT name has been used before
      const docsWithSameNftName = await db.tteNfts
        .find({
          nftName,
        })
        .toArray();

      if (docsWithSameNftName.length > 1) {
        console.log('Error: NFT name has been used before!');
        continue;
      }

      if (!tokenURI && nftName && sku) {
        try {
          tokenURI = await uploadTteNftMetadataToIpfs(nftName, sku);
        } catch {
          // Rate limit reached. Try again later
        }
      }

      if (!tokenURI || !_id || !ethAddress || !sku) {
        console.log('Invalid tokenURI, _id, sku or ethAddress');
        continue;
      }

      const { nftType } = TTE_NFT_VARIANTS[sku];

      if (!nftType) {
        console.log('Invalid nftType');
        continue;
      }

      const { tokenId, transactionHash } = await mintTteNft(
        quantity,
        ethAddress,
        tokenURI,
        nftType,
      );

      if (!transactionHash || !tokenId) {
        console.log('Invalid transactionHash or tokenId');
        continue;
      }

      const result = {
        status: 'success',
        lastTxHash: transactionHash,
        tokenId,
        retryMintCronjob: true,
        updatedAt: new Date(),
        tokenURI,
      };

      await db.tteNfts.updateOne({ _id }, { $set: result });
    } catch (error) {
      console.log('Error inside for loop:', error);
      Sentry.captureException(error);
    }
  }
};

const retryMintTteNFTCronjob = async (app) => {
  const db = app.db;

  const nftsWithUnvalidatedTxs = await db.tteNfts
    .find({
      validatedTx: { $ne: true },
      status: 'success',
    })
    .toArray();

  console.log({
    nftsWithUnvalidatedTxsLength: nftsWithUnvalidatedTxs.length,
  });

  const promises = nftsWithUnvalidatedTxs.map(
    async ({ _id, lastTxHash }, index) => {
      if (!lastTxHash) return;

      const isValidTx = await checkTransactionReceiptStatus(lastTxHash, index);
      console.log({
        isValidTx,
        remaining: nftsWithUnvalidatedTxs.length - index - 1,
      });

      if (isValidTx === true) {
        const result = {
          validatedTx: true,
          updatedAt: new Date(),
        };

        await db.tteNfts.updateOne({ _id }, { $set: result });
      } else if (isValidTx === false) {
        const result = {
          status: 'error',
          updatedAt: new Date(),
        };

        await db.tteNfts.updateOne({ _id }, { $set: result });
      } else if (isValidTx === null) {
        // Not able to validate the tx at this time, check later
      }
    },
  );

  await Promise.all(promises);

  const nftsToBeMinted = await db.tteNfts
    .find({
      status: { $ne: 'success' },
      tokenIdTracker: { $ne: true },
      toBeDeleted: { $ne: true },
    })
    .toArray();

  console.log({ ['Length of nftsToBeMinted']: nftsToBeMinted.length });

  await retryMintNFTs(nftsToBeMinted, db);
  resetNonceTte();

  console.log('retryMintTteNFTCronjob -> Done 🎉');
};

module.exports = retryMintTteNFTCronjob;
