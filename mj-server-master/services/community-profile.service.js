const ObjectId = require('mongodb').ObjectId;
const Config = require('../config');
const AWS = require('aws-sdk');
const mime = require('mime-type/with-db');
const sharp = require('sharp');
const fs = require('fs');
const dayjs = require('dayjs');
const utc = require('dayjs/plugin/utc');
dayjs.extend(utc);
const s3 = new AWS.S3({
  accessKeyId: Config.S3.accessKeyId,
  secretAccessKey: Config.S3.secretAccessKey,
});

const checkFile = (common, file) => {
  const errors = [];
  // Get the mime type of the file
  const mimeType = mime.lookup(file.originalname);

  // Check for allowed mime type and file size
  if (file.size > common.fileSizeLimit) {
    errors.push({
      statusCode: 413,
      message: `File ${file.originalname} type too large. Please try again.`,
    });
  }

  if (!common.allowedMimeType.includes(mimeType)) {
    errors.push({
      statusCode: 415,
      message: `File ${file.originalname} type not allowed. Please try again.`,
    });
  }
  if (errors.length) {
    // Remove temp file
    fs.unlinkSync(file.path);
  }

  return errors;
};

const uploadFileToS3 = async (file, folder) => {
  let x = new Date().getTime();
  const fileData = await sharp(file.path).resize().toFormat('jpeg').toBuffer();
  x = new Date().getTime();
  const params = {
    Bucket: Config.S3.bucket, //Enter the bucket name that you created
    Key: `${folder}/${Date.now()}.jpg`, //filename to use on the cloud
    Body: fileData,
    ContentType: 'image/png',
    CacheControl: 'max-age=172800',
    ACL: 'public-read', //To make file publicly accessible through URL
  };
  const s3Upload = await s3.upload(params).promise();
  fs.unlinkSync(file.path);
  return s3Upload.Location;
};

const splitS3Path = (path) => {
  const url = new URL(path);
  const bucket = Config.S3.bucket;
  const key = url.pathname.substr(1);
  return { bucket, key };
};

const deleteS3Object = async (newData, oldPath) => {
  if (newData && oldPath.length) {
    const { bucket, key } = splitS3Path(oldPath);
    await s3
      .deleteObjects({
        Bucket: bucket,
        Delete: { Objects: [{ Key: key }] },
      })
      .promise();
  }
};

const getCommunityProfileForm = async (req, common) => {
  const db = req.app.db;

  let result;
  let title;
  let actions;
  let canChangeToDraft = true;

  result = {
    profileName: '',
    profileType: '',
    cardImage: '',
    logoImage: '',
    bannerImage: '',
  };
  title = 'New Community Profile';
  actions = {
    endpoint: '/admin/community-profile/insert',
  };

  if (req.params.id) {
    result = await db.profiles.findOne({ _id: ObjectId(req.params.id) });
    const findPoll = await db.polls.findOne({ profileId: result._id });
    canChangeToDraft = findPoll ? false : true;
    title = 'Update Community Profile';
    actions = {
      endpoint: `/admin/community-profile/edit/${req.params.id}`,
    };
  }

  return {
    title,
    menu: 'profile',
    parentMenu: 'community-profile',
    result,
    canChangeToDraft,
    actions,
    session: req.session,
    editor: true,
    admin: true,
    config: req.app.config,
    message: common.clearSessionValue(req.session, 'message'),
    messageType: common.clearSessionValue(req.session, 'messageType'),
    helpers: req.handlebars.helpers,
  };
};

const insertCommunityProfile = async (req, common) => {
  const db = req.app.db;
  let errors = [];
  const { profileName, profileType, isPublish } = req.body;

  const checkProfileName = await db.profiles.findOne({ profileName });
  if (checkProfileName) {
    errors.push({
      statusCode: 400,
      message: 'This profile name has exists',
    });
    return { errors, result: null };
  }

  const { cardImage, logoImage, bannerImage } = req.files;
  const folder = Config.S3.profileImageFolder;
  let isPublished = false;
  let canPublish = false;

  const [checkBanner, checkCard, checkLogo] = await Promise.all([
    bannerImage ? checkFile(common, bannerImage[0]) : [],
    cardImage ? checkFile(common, cardImage[0]) : [],
    logoImage ? checkFile(common, logoImage[0]) : [],
  ]);

  errors = checkBanner.concat(checkCard, checkLogo);
  if (errors.length) {
    return { errors, result: null };
  }

  const [cardImageUrl, logoImageUrl, bannerImageUrl] = await Promise.all([
    cardImage ? uploadFileToS3(cardImage[0], folder) : '',
    logoImage ? uploadFileToS3(logoImage[0], folder) : '',
    bannerImage ? uploadFileToS3(bannerImage[0], folder) : '',
  ]);

  if (isPublish === 'true') isPublished = true;
  if (isPublish === 'false') isPublished = false;

  if (profileName && profileType && cardImage && logoImage && bannerImage) {
    canPublish = true;
  }

  const doc = {
    profileName: profileName ? profileName : '',
    profileType: profileType ? profileType : 'brands',
    cardImage: cardImageUrl,
    logoImage: logoImageUrl,
    bannerImage: bannerImageUrl,
    createdAt: new Date(),
    updatedAt: new Date(),
    canPublish,
    isPublished,
    order: Date.now(),
  };
  const addBrandProfile = await db.profiles.insertOne(doc);
  if (!addBrandProfile.result.n) {
    errors.push({
      statusCode: 400,
      message: 'Inserted failed, please try again!',
    });
  }

  return { errors, data: addBrandProfile, profileType };
};

const editCommunityProfile = async (req, common) => {
  const db = req.app.db;
  const id = req.params.id;
  let isPublished;
  let canPublish = false;
  let errors = [];
  const folder = Config.S3.profileImageFolder;
  const { profileName, profileType, isPublish } = req.body;

  const checkProfileName = await db.profiles.findOne({
    profileName,
    _id: { $nin: [ObjectId(id)] },
  });

  if (checkProfileName) {
    errors.push({
      statusCode: 400,
      message: 'This profile name has exists',
    });
    return { errors, result: null };
  }

  const { cardImage, logoImage, bannerImage } = req.files;
  const getProfile = await db.profiles.findOne({
    _id: ObjectId(id),
  });

  const [checkBanner, checkCard, checkLogo] = await Promise.all([
    bannerImage ? checkFile(common, bannerImage[0]) : [],
    cardImage ? checkFile(common, cardImage[0]) : [],
    logoImage ? checkFile(common, logoImage[0]) : [],
  ]);

  errors = checkBanner.concat(checkCard, checkLogo);
  if (errors.length) {
    return { errors, result: null };
  }

  await Promise.all([
    cardImage ? deleteS3Object(cardImage, getProfile.cardImage) : null,
    logoImage ? deleteS3Object(logoImage, getProfile.logoImage) : null,
    bannerImage ? deleteS3Object(bannerImage, getProfile.bannerImage) : null,
  ]);

  const [cardImageUrl, logoImageUrl, bannerImageUrl] = await Promise.all([
    cardImage ? uploadFileToS3(cardImage[0], folder) : getProfile.cardImage,
    logoImage ? uploadFileToS3(logoImage[0], folder) : getProfile.logoImage,
    bannerImage
      ? uploadFileToS3(bannerImage[0], folder)
      : getProfile.bannerImage,
  ]);

  const newProfileName = profileName ? profileName : getProfile.profileName;
  const newProfileType = profileType ? profileType : getProfile.profileType;

  if (isPublish === 'true') isPublished = true;
  if (isPublish === 'false') isPublished = false;
  if (
    newProfileName.length &&
    newProfileType.length &&
    cardImageUrl.length &&
    logoImageUrl.length &&
    bannerImageUrl.length
  ) {
    canPublish = true;
  }

  const updateBrandProfile = {
    profileName: newProfileName,
    profileType: newProfileType,
    cardImage: cardImageUrl,
    logoImage: logoImageUrl,
    bannerImage: bannerImageUrl,
    isPublished,
    canPublish,
    updatedAt: new Date(),
    createdAt: getProfile.createdAt,
    order: getProfile.order,
  };

  const updateProfile = await db.profiles.updateOne(
    { _id: getProfile._id },
    { $set: updateBrandProfile },
    {},
  );

  if (!updateProfile.result.n) {
    errors.push({
      statusCode: 400,
      message: 'Updated failed, please try again!',
    });
  }

  return { errors, data: updateBrandProfile };
};

const deleteCommunityProfile = async (req) => {
  const errors = [];
  try {
    const db = req.app.db;
    const id = req.params.id;
    const getProfile = await db.profiles.findOne({
      _id: ObjectId(id),
    });
    const deleteProfile = await db.profiles.deleteOne({
      _id: getProfile._id,
    });
    if (!deleteProfile.result.n) {
      errors.push({ code: 400, message: 'Delete failed' });
      return { errors, result: null };
    }
    await Promise.all([
      getProfile.cardImage ? deleteS3Object(true, getProfile.cardImage) : null,
      getProfile.logoImage ? deleteS3Object(true, getProfile.logoImage) : null,
      getProfile.bannerImage
        ? deleteS3Object(true, getProfile.bannerImage)
        : null,
    ]);
    return { errors, data: { profileType: getProfile.profileType } };
  } catch (error) {
    errors.push({ statusCode: 500, message: error.message });
    return { errors };
  }
};

const getPollCreateForm = async (req, common) => {
  const db = req.app.db;
  const id = req.params.id;
  const formTitle = 'Create poll';

  const getProfile = await db.profiles.findOne({ _id: ObjectId(id) });

  const profiles = await db.profiles
    .find({ _id: { $nin: [getProfile._id] } })
    .sort({ updatedAt: -1 })
    .toArray();

  return {
    title: 'Community polls',
    formTitle,
    menu: 'profile',
    parentMenu: 'community-profile',
    profiles,
    getProfile,
    profileStatus: getProfile.isPublished ? getProfile.isPublished : 'false',
    profileId: getProfile._id,
    showSaveDraft: true,
    showPublish: true,
    session: req.session,
    editor: true,
    admin: true,
    config: req.app.config,
    message: common.clearSessionValue(req.session, 'message'),
    messageType: common.clearSessionValue(req.session, 'messageType'),
    helpers: req.handlebars.helpers,
  };
};

/*************************** Quiz Form ****************************/
const getCommunityQuizForm = (req, common) => {
  return {
    menu: 'quiz',
    parentMenu: 'community-profile',
    session: req.session,
    showSaveDraft: true,
    showPublish: true,
    editor: true,
    admin: true,
    config: req.app.config,
    message: common.clearSessionValue(req.session, 'message'),
    messageType: common.clearSessionValue(req.session, 'messageType'),
    helpers: req.handlebars.helpers,
  };
};

const createQuizFromProfile = async (req, common) => {
  const db = req.app.db;
  const profileId = req.params.profileId;
  const getProfile = await db.profiles.findOne({ _id: ObjectId(profileId) });
  const result = {};
  result.profileId = getProfile.profileId;
  result.profileName = getProfile.profileName;

  return {
    menu: 'quiz',
    parentMenu: 'community-profile',
    result: result,
    session: req.session,
    isProfilePublic: getProfile.isPublished,
    showSaveDraft: true,
    showPublish: true,
    editor: true,
    admin: true,
    config: req.app.config,
    message: common.clearSessionValue(req.session, 'message'),
    messageType: common.clearSessionValue(req.session, 'messageType'),
    helpers: req.handlebars.helpers,
  };
};

const insertProfileQuiz = async (req, common) => {
  const db = req.app.db;
  const errors = [];
  const {
    profileName,
    title,
    availableSkus,
    quizStartOn,
    quizExpiresOn,
    questions,
    ispublished,
  } = req.body;
  let { statusBreakDown, isDraft, canPublish, profileId } = req.body;
  const { bannerImage } = req.files;
  const folder = Config.S3.quizsImageFolder;

  if (profileName.length && !profileId) {
    const getProfile = await db.profiles.findOne({ profileName });
    profileId = getProfile._id;
  }

  const checkBanner = bannerImage ? checkFile(common, bannerImage[0]) : [];

  if (checkBanner.length) {
    errors.push(checkBanner);
    return { errors, result: null };
  }

  if (new Date(quizStartOn).getTime() > new Date(quizExpiresOn).getTime()) {
    errors.push({
      statusCode: 400,
      message:
        'Inserted failed, "Quiz starts on" greater than "Quiz expires on" please try again!',
    });
    return { errors, result: null };
  }

  const [bannerImageUrl] = await Promise.all([
    bannerImage ? uploadFileToS3(bannerImage[0], folder) : '',
  ]);

  if (statusBreakDown === 'true') statusBreakDown = true;
  if (statusBreakDown === 'false' || !statusBreakDown) statusBreakDown = false;
  if (isDraft === 'true') isDraft = true;
  if (isDraft === 'false' || !isDraft) isDraft = false;
  if (canPublish === 'true') canPublish = true;
  if (canPublish === 'false' || !canPublish) canPublish = false;

  const doc = {
    profileId: profileId ? ObjectId(profileId) : '',
    title,
    statusBreakDown,
    quizStartOn: quizStartOn.length ? new Date(quizStartOn) : '',
    quizExpiresOn: quizExpiresOn.length ? new Date(quizExpiresOn) : '',
    isDraft,
    ispublished: ispublished ? ispublished : false,
    availableSkus:
      typeof availableSkus === 'string' ? [availableSkus] : availableSkus,
    questions: questions.map((e, i) => {
      e.questionNumber = i + 1;
      e.question = e[Object.keys(e)[0]];
      e.options = [
        {
          key: Object.keys(e.options)[0],
          name: Object.values(e.options)[0],
          is_correct: e[Object.keys(e)[2]] === '1',
        },
        {
          key: Object.keys(e.options)[1],
          name: Object.values(e.options)[1],
          is_correct: e[Object.keys(e)[2]] === '2',
        },
        {
          key: Object.keys(e.options)[2],
          name: Object.values(e.options)[2],
          is_correct: e[Object.keys(e)[2]] === '3',
        },
        {
          key: Object.keys(e.options)[3],
          name: Object.values(e.options)[3],
          is_correct: e[Object.keys(e)[2]] === '4',
        },
      ];
      e._id = new ObjectId();
      delete e[Object.keys(e)[0]];
      return e;
    }),
    statusOfQuiz: isDraft ? 'Draft' : '',
    bannerImage: bannerImageUrl,
    canPublish,
    createdAt: new Date(),
    updatedAt: new Date(),
  };

  const addProfileQuiz = await db.quizs.insertOne(doc);

  if (!addProfileQuiz.result.n) {
    errors.push({
      statusCode: 400,
      message: 'Inserted failed, please try again!',
    });
  }

  return { errors, data: addProfileQuiz.ops[0] };
};

const getUpdateQuizForm = async (req, common) => {
  let hideSaveDraftButton = false;
  let showExportCSV = false;
  let showSaveDraft = false;
  let showUnpublish = false;
  let showPublish = false;
  let checkEditQuestion = true;
  let isDisableForm = false;
  const isUpdateForm = true;
  const db = req.app.db;
  const [quiz, question] = await Promise.all([
    db.quizs
      .aggregate([
        { $match: { _id: ObjectId(req.params.id) } },
        {
          $lookup: {
            from: 'profiles',
            localField: 'profileId',
            foreignField: '_id',
            as: 'profile',
          },
        },
      ])
      .toArray(),
    db.quizQuestions.findOne({ quizId: ObjectId(req.params.id) }),
  ]);
  const result = quiz[0];
  const quizStartOn = new Date(result.quizStartOn).getTime();
  const quizExpiresOn = new Date(result.quizExpiresOn).getTime();
  const currentTime = new Date().getTime();

  if (quizStartOn && quizExpiresOn) {
    if (
      //Active case
      quizStartOn < currentTime &&
      currentTime < quizExpiresOn &&
      !result.isDraft &&
      result.ispublished
    ) {
      if (question) {
        checkEditQuestion = false;
      }
      showUnpublish = true;
      showPublish = true;
    } else if (
      result.isDraft &&
      result.ispublished &&
      quizExpiresOn < currentTime
    ) {
      showExportCSV = true;
      isDisableForm = true;
    } else if (
      !result.isDraft &&
      result.ispublished &&
      quizExpiresOn < currentTime
    ) {
      showExportCSV = true;
      showUnpublish = true;
      isDisableForm = true;
    } else if (
      !result.isDraft &&
      result.ispublished &&
      quizStartOn > currentTime
    ) {
      if (question) {
        checkEditQuestion = false;
      }
      showUnpublish = true;
      showPublish = true;
    } else if (result.isDraft && !result.ispublished) {
      checkEditQuestion = true;
      showSaveDraft = true;
      showPublish = true;
    } else if (result.isDraft && result.canPublish && result.ispublished) {
      //Unpublish
      if (question) {
        checkEditQuestion = false;
      }
      showExportCSV = true;
      isDisableForm = true;
    }
  } else {
    checkEditQuestion = true;
    showSaveDraft = true;
    showPublish = true;
  }

  result.quizStartOn = result.quizStartOn
    ? dayjs(result.quizStartOn.toISOString().slice(0, 16))
        .utc('z')
        .local()
        .format()
        .slice(0, 16)
    : '';
  result.quizExpiresOn = result.quizExpiresOn
    ? dayjs(result.quizExpiresOn.toISOString().slice(0, 16))
        .utc('z')
        .local()
        .format()
        .slice(0, 16)
    : '';

  if (!result.bannerImage.length) {
    delete result.bannerImage;
  }

  if (!result.questions.length) {
    delete result.questions;
  }

  if (!result || !result.profile.length) {
    delete result.profileName;
    delete result.profileId;
  }

  if (result.profile.length) {
    result.profileName = result.profile[0].profileName;
  }

  const data = {
    menu: 'quiz',
    parentMenu: 'community-profile',
    result: result,
    questions: result.questions,
    hideSaveDraftButton,
    showExportCSV,
    isUpdateForm,
    checkEditQuestion,
    showSaveDraft,
    showUnpublish,
    showPublish,
    isProfilePublic: result.profile.length
      ? result.profile[0].isPublished
      : false,
    isDisableForm,
    session: req.session,
    editor: true,
    admin: true,
    config: req.app.config,
    message: common.clearSessionValue(req.session, 'message'),
    messageType: common.clearSessionValue(req.session, 'messageType'),
    helpers: req.handlebars.helpers,
  };

  return { result: data };
};

const updateCommunityQuiz = async (req, common) => {
  const db = req.app.db;
  const id = req.params.id;
  let errors = [];

  const {
    profileName,
    title,
    availableSkus,
    quizStartOn,
    quizExpiresOn,
    questions,
    checkEditQuestion,
  } = req.body;

  let getProfile;
  let { statusBreakDown, isDraft, canPublish, ispublished } = req.body;
  const { bannerImage } = req.files;
  const folder = Config.S3.quizsImageFolder;

  if (profileName) {
    getProfile = await db.profiles.findOne({ profileName });
  }

  if (new Date(quizStartOn).getTime() > new Date(quizExpiresOn).getTime()) {
    errors.push({
      statusCode: 400,
      message:
        'Inserted failed, "Quiz starts on" greater than "Quiz expires on" please try again!',
    });
    return { errors, result: null };
  }

  const checkQuizExist = await db.quizs.findOne({
    _id: ObjectId(id),
  });

  if (!checkQuizExist) {
    errors.push({
      statusCode: 400,
      message: 'This quiz not exists',
    });
    return { errors, result: null };
  }
  if (checkQuizExist.ispublished === true) {
    ispublished = true;
  }

  const [checkBanner] = await Promise.all([
    bannerImage ? checkFile(common, bannerImage[0]) : [],
  ]);

  errors = checkBanner;
  if (errors.length) {
    return { errors, result: null };
  }

  await Promise.all([
    bannerImage
      ? deleteS3Object(bannerImage, checkQuizExist.bannerImage)
      : null,
  ]);

  const [bannerImageUrl] = await Promise.all([
    bannerImage
      ? uploadFileToS3(bannerImage[0], folder)
      : checkQuizExist.bannerImage,
  ]);

  if (statusBreakDown === 'true') statusBreakDown = true;
  if (statusBreakDown === 'false' || !statusBreakDown) statusBreakDown = false;
  if (isDraft === 'true') isDraft = true;
  if (isDraft === 'false' || !isDraft) isDraft = false;
  if (canPublish === 'true') canPublish = true;
  if (canPublish === 'false' || !canPublish) canPublish = false;
  const updateQuiz = {
    profileId: profileName.length ? getProfile._id : '',
    title,
    statusBreakDown,
    quizStartOn: quizStartOn.length ? new Date(quizStartOn) : '',
    quizExpiresOn: quizExpiresOn.length ? new Date(quizExpiresOn) : '',
    isDraft,
    ispublished: ispublished ? ispublished : false,
    availableSkus:
      typeof availableSkus === 'string' ? [availableSkus] : availableSkus,
    statusOfQuiz: isDraft ? 'Draft' : '',
    bannerImage: bannerImageUrl,
    canPublish,
    createdAt: new Date(),
    updatedAt: new Date(),
  };
  if (checkEditQuestion === 'true') {
    updateQuiz.questions = questions.map((e, i) => {
      e.questionNumber = i + 1;
      e.question = e[`title-${i + 1}`];
      e.options = [
        {
          key: Object.keys(e.options)[0],
          name: Object.values(e.options)[0],
          is_correct: e[`question-${i + 1}`] === '1',
        },
        {
          key: Object.keys(e.options)[1],
          name: Object.values(e.options)[1],
          is_correct: e[`question-${i + 1}`] === '2',
        },
        {
          key: Object.keys(e.options)[2],
          name: Object.values(e.options)[2],
          is_correct: e[`question-${i + 1}`] === '3',
        },
        {
          key: Object.keys(e.options)[3],
          name: Object.values(e.options)[3],
          is_correct: e[`question-${i + 1}`] === '4',
        },
      ];
      e._id = e._id ? e._id : new ObjectId();
      delete e[`title-${i + 1}`];
      delete e[`question-${i + 1}`];
      return e;
    });
  }

  const updateResult = await db.quizs.updateOne(
    { _id: checkQuizExist._id },
    { $set: updateQuiz },
    {},
  );

  if (!updateResult.result.n) {
    errors.push({
      statusCode: 400,
      message: 'Updated failed, please try again!',
    });
  }

  return { errors, data: updateResult };
};

const listQuiz = async (req) => {
  const query = [
    {
      $lookup: {
        from: 'profiles',
        localField: 'profileId',
        foreignField: '_id',
        as: 'profile',
      },
    },
    {
      $sort: {
        quizStartOn: 1,
      },
    },
  ];
  if (req.query.profileId) {
    query.unshift({ $match: { profileId: ObjectId(req.query.profileId) } });
  }
  const db = req.app.db;
  const results = await db.quizs.aggregate(query).toArray();
  for (let i = 0; i < results.length; i++) {
    const getStartOn = new Date(results[i].quizStartOn).getTime();
    const getExpiresOn = new Date(results[i].quizExpiresOn).getTime();
    const currentTime = new Date().getTime();
    results[i].quizStartOn = results[i].quizStartOn.toString().length
      ? new Date(results[i].quizStartOn).toString().slice(0, 31)
      : 'NA';
    if (!results[i].isDraft && getExpiresOn < currentTime) {
      results[i].statusOfQuiz = 'Expired';
    }

    if (results[i].isDraft && !results[i].ispublished) {
      results[i].statusOfQuiz = 'Draft';
    }

    if (results[i].isDraft && results[i].canPublish && results[i].ispublished) {
      results[i].statusOfQuiz = 'Unpublished';
    }

    if (
      !results[i].isDraft &&
      getStartOn < currentTime &&
      getExpiresOn > currentTime
    ) {
      results[i].statusOfQuiz = 'Active';
    }

    if (!results[i].isDraft && getStartOn > currentTime) {
      results[i].statusOfQuiz = 'Incoming';
    }

    if (!results[i].profile.length) {
      results[i].statusOfQuiz = 'Draft';
      results[i].profileId = '';
      results[i].profileName = '';
    } else {
      results[i].profileName = results[i].profile[0].profileName;
    }
  }

  return { results };
};

const deleteCommunityQuiz = async (req) => {
  const errors = [];
  try {
    const db = req.app.db;
    const id = req.params.id;
    const [getQuiz, checkAnswer] = await Promise.all([
      db.quizs.findOne({
        _id: ObjectId(id),
      }),
      db.quizQuestions.findOne({
        quizId: ObjectId(id),
      }),
    ]);
    if (checkAnswer) {
      errors.push({ code: 400, message: 'Delete failed' });
      return { errors, result: null };
    }
    const [deleteQuiz] = await Promise.all([
      db.quizs.deleteOne({
        _id: getQuiz._id,
      }),
    ]);
    if (!deleteQuiz.result.n) {
      errors.push({ code: 400, message: 'Delete failed' });
      return { errors, result: null };
    }
    await Promise.all([
      getQuiz.bannerImage ? deleteS3Object(true, getQuiz.bannerImage) : null,
    ]);
    return { errors, data: getQuiz };
  } catch (error) {
    errors.push({ statusCode: 500, message: error.message });
    return { errors };
  }
};

/*************************** Poll Form ****************************/
const listPolls = async (req) => {
  const db = req.app.db;
  // get the top results
  const query = req.query.profileId
    ? {
        profileId: ObjectId(req.query.profileId),
      }
    : {};
  const results = await db.polls
    .aggregate([
      {
        $match: query,
      },
      {
        $lookup: {
          from: 'profiles',
          localField: 'profileId',
          foreignField: '_id',
          as: 'profile',
        },
      },
      {
        $limit: 100,
      },
      {
        $sort: { pollStartOn: -1 },
      },
    ])
    .toArray();
  results.forEach(async (result) => {
    if (result.profile.length) {
      result.profileName = result.profile[0].profileName;
    }
    const pollStartOn = new Date(result.pollStartOn).getTime();
    const pollExpiresOn = new Date(result.pollExpiresOn).getTime();
    const currentTime = new Date().getTime();
    if (pollStartOn && pollExpiresOn) {
      if (
        //active
        pollStartOn < currentTime &&
        currentTime < pollExpiresOn &&
        !result.isDraft &&
        result.isPublished
      ) {
        result.statusOfPoll = 'Active';
      } else if (
        //ubpublished
        result.isDraft &&
        result.isPublished
      ) {
        result.statusOfPoll = 'Unpublished';
      } else if (
        //expired
        !result.isDraft &&
        result.isPublished &&
        pollExpiresOn < currentTime
      ) {
        result.statusOfPoll = 'Expired';
      } else if (
        //incoming
        !result.isDraft &&
        result.isPublished &&
        pollStartOn > currentTime
      ) {
        result.statusOfPoll = 'Incoming';
      } else if (result.isDraft && !result.isPublished) {
        result.statusOfPoll = 'Draft';
      }
    } else {
      result.statusOfPoll = 'Draft';
    }
  });

  return { results };
};

const insertProfilePoll = async (req, common) => {
  const db = req.app.db;
  const errors = [];
  const {
    profileId,
    question,
    option1,
    option2,
    option3,
    option4,
    pollStartOn,
    pollExpiresOn,
    isPublished,
  } = req.body;
  let { statusBreakDown, isDraft } = req.body;
  const { bannerImage } = req.files;
  const folder = Config.S3.pollsImageFolder;

  const checkBanner = bannerImage ? checkFile(common, bannerImage[0]) : [];

  if (checkBanner.length) {
    errors.push(checkBanner);
    return { errors, result: null };
  }

  if (new Date(pollStartOn).getTime() > new Date(pollExpiresOn).getTime()) {
    errors.push({
      statusCode: 400,
      message:
        'Inserted failed, "Poll starts on" greater than "Poll expires on" please try again!',
    });
    return { errors, result: null };
  }

  const [bannerImageUrl] = await Promise.all([
    bannerImage ? uploadFileToS3(bannerImage[0], folder) : '',
  ]);

  if (statusBreakDown === 'true') statusBreakDown = true;
  if (statusBreakDown === 'false' || !statusBreakDown) statusBreakDown = false;
  if (isDraft === 'true') isDraft = true;
  if (isDraft === 'false' || !isDraft) isDraft = false;

  const doc = {
    profileId: profileId ? ObjectId(profileId) : '',
    question,
    statusBreakDown,
    pollStartOn: pollStartOn.length ? new Date(pollStartOn) : '',
    pollExpiresOn: pollExpiresOn.length ? new Date(pollExpiresOn) : '',
    isDraft,
    isPublished: isPublished ? isPublished : false,
    statusOfPoll: isDraft ? 'Draft' : '',
    bannerImage: bannerImageUrl,
    options: [
      {
        key: 'option1',
        name: option1,
      },
      {
        key: 'option2',
        name: option2,
      },
      {
        key: 'option3',
        name: option3,
      },
      {
        key: 'option4',
        name: option4,
      },
    ],
    createdAt: new Date(),
    updatedAt: new Date(),
  };

  const addProfilePoll = await db.polls.insertOne(doc);

  if (!addProfilePoll.result.n) {
    errors.push({
      statusCode: 400,
      message: 'Inserted failed, please try again!',
    });
  }

  return { errors, data: addProfilePoll.ops[0] };
};

const deleteCommunityPoll = async (req) => {
  const errors = [];
  try {
    const db = req.app.db;
    const id = req.params.id;
    const getPoll = await db.polls.findOne({
      _id: ObjectId(id),
    });
    const [deletePoll, deleteAnswer] = await Promise.all([
      db.polls.deleteOne({
        _id: getPoll._id,
      }),
      db.userPolls.deleteMany({
        pollId: getPoll._id,
      }),
    ]);
    if (!deletePoll.result.n) {
      errors.push({ code: 400, message: 'Delete failed' });
      return { errors, result: null };
    }
    await Promise.all([
      getPoll.bannerImage ? deleteS3Object(true, getPoll.bannerImage) : null,
    ]);
    return { errors, data: getPoll };
  } catch (error) {
    errors.push({ statusCode: 500, message: error.message });
    return { errors };
  }
};

const getUpdatePollForm = async (req, common) => {
  let showExportCSV = false;
  let showSaveDraft = false;
  let showUnpublish = false;
  let showPublish = false;
  let isDisableForm = false;
  let formTitle = '';
  const isUpdateForm = true;
  const db = req.app.db;
  const [poll, profiles] = await Promise.all([
    db.polls
      .aggregate([
        { $match: { _id: ObjectId(req.params.id) } },
        {
          $lookup: {
            from: 'profiles',
            localField: 'profileId',
            foreignField: '_id',
            as: 'profile',
          },
        },
      ])
      .toArray(),
    db.profiles
      .find(
        {},
        {
          projection: {
            profileId: 1,
            profileName: 1,
            isPublished: 1,
          },
        },
      )
      .sort({
        createdAt: -1,
      })
      .limit(100)
      .toArray(),
  ]);
  const result = poll[0];
  const pollStartOn = new Date(result.pollStartOn).getTime();
  const pollExpiresOn = new Date(result.pollExpiresOn).getTime();
  const currentTime = new Date().getTime();

  if (pollStartOn && pollExpiresOn) {
    if (
      //active
      pollStartOn < currentTime &&
      currentTime < pollExpiresOn &&
      !result.isDraft &&
      result.isPublished
    ) {
      showUnpublish = true;
      showPublish = true;
      formTitle = 'Edit active poll';
    } else if (
      //ubpublished
      result.isDraft &&
      result.isPublished
    ) {
      showExportCSV = true;
      isDisableForm = true;
      formTitle = 'Unpublished poll';
    } else if (
      //expired
      !result.isDraft &&
      result.isPublished &&
      pollExpiresOn < currentTime
    ) {
      showExportCSV = true;
      showUnpublish = true;
      isDisableForm = true;
      formTitle = 'Expired poll';
    } else if (
      //incoming
      !result.isDraft &&
      result.isPublished &&
      pollStartOn > currentTime
    ) {
      showUnpublish = true;
      showPublish = true;
      formTitle = 'Edit active poll';
    } else if (result.isDraft && !result.isPublished) {
      showSaveDraft = true;
      showPublish = true;
      formTitle = 'Edit draft poll';
    } else if (result.statusOfPoll === 'Draft') {
      showSaveDraft = true;
      showPublish = true;
      formTitle = 'Edit draft poll';
    }
  } else {
    showSaveDraft = true;
    showPublish = true;
    formTitle = 'Edit draft poll';
  }

  result.pollStartOn = result.pollStartOn
    ? dayjs(result.pollStartOn.toISOString().slice(0, 16))
        .utc('z')
        .local()
        .format()
        .slice(0, 16)
    : '';
  result.pollExpiresOn = result.pollExpiresOn
    ? dayjs(result.pollExpiresOn.toISOString().slice(0, 16))
        .utc('z')
        .local()
        .format()
        .slice(0, 16)
    : '';

  if (!result.bannerImage.length) {
    delete result.bannerImage;
  }

  if (!result || !result.profile.length) {
    delete result.profileId;
  }

  if (result.profile.length) {
    result.profileName = result.profile[0].profileName;
  }

  const data = {
    menu: 'poll',
    parentMenu: 'community-profile',
    result: result,
    formTitle,
    profiles,
    profileStatus: result.profile[0]?.isPublished,
    profileId: result.profileId,
    showExportCSV,
    isUpdateForm,
    showSaveDraft,
    showUnpublish,
    showPublish,
    isProfilePublic: result.profile.length
      ? result.profile[0].isPublished
      : false,
    isDisableForm,
    session: req.session,
    editor: true,
    admin: true,
    config: req.app.config,
    message: common.clearSessionValue(req.session, 'message'),
    messageType: common.clearSessionValue(req.session, 'messageType'),
    helpers: req.handlebars.helpers,
  };

  return { result: data };
};

const updateCommunityPoll = async (req, common) => {
  const db = req.app.db;
  const id = req.params.id;
  let errors = [];

  const {
    profileId,
    question,
    option1,
    option2,
    option3,
    option4,
    pollStartOn,
    pollExpiresOn,
  } = req.body;

  let { statusBreakDown, isDraft, isPublished } = req.body;
  const { bannerImage } = req.files;
  const folder = Config.S3.pollsImageFolder;

  if (new Date(pollStartOn).getTime() > new Date(pollExpiresOn).getTime()) {
    errors.push({
      statusCode: 400,
      message:
        'Inserted failed, "Poll starts on" greater than "Poll expires on" please try again!',
    });
    return { errors, result: null };
  }

  const checkPollExist = await db.polls.findOne({
    _id: ObjectId(id),
  });

  if (!checkPollExist) {
    errors.push({
      statusCode: 400,
      message: 'This poll not exists',
    });
    return { errors, result: null };
  }
  if (
    checkPollExist.isPublished === true ||
    checkPollExist.isPublished === 'true'
  ) {
    isPublished = 'true';
  }

  const [checkBanner] = await Promise.all([
    bannerImage ? checkFile(common, bannerImage[0]) : [],
  ]);

  errors = checkBanner;
  if (errors.length) {
    return { errors, result: null };
  }

  await Promise.all([
    bannerImage
      ? deleteS3Object(bannerImage, checkPollExist.bannerImage)
      : null,
  ]);

  const [bannerImageUrl] = await Promise.all([
    bannerImage
      ? uploadFileToS3(bannerImage[0], folder)
      : checkPollExist.bannerImage,
  ]);

  if (statusBreakDown === 'true') statusBreakDown = true;
  if (statusBreakDown === 'false' || !statusBreakDown) statusBreakDown = false;
  if (isDraft === 'true') isDraft = true;
  if (isDraft === 'false' || !isDraft || isDraft[0] === 'false')
    isDraft = false;
  const updatePoll = {
    profileId: profileId ? ObjectId(profileId) : '',
    question,
    statusBreakDown,
    pollStartOn: pollStartOn.length ? new Date(pollStartOn) : '',
    pollExpiresOn: pollExpiresOn.length ? new Date(pollExpiresOn) : '',
    isDraft,
    isPublished: isPublished === 'true' ? true : false,
    options: [
      {
        key: 'option1',
        name: option1,
      },
      {
        key: 'option2',
        name: option2,
      },
      {
        key: 'option3',
        name: option3,
      },
      {
        key: 'option4',
        name: option4,
      },
    ],
    statusOfPoll: isDraft ? 'Draft' : '',
    bannerImage: bannerImageUrl,
    updatedAt: new Date(),
  };

  const updateResult = await db.polls.updateOne(
    { _id: checkPollExist._id },
    { $set: updatePoll },
    {},
  );

  if (!updateResult.result.n) {
    errors.push({
      statusCode: 400,
      message: 'Updated failed, please try again!',
    });
  }

  return { errors, data: updateResult };
};

module.exports = {
  getCommunityProfileForm,
  editCommunityProfile,
  deleteCommunityProfile,
  insertCommunityProfile,
  insertProfilePoll,
  getCommunityQuizForm,
  insertProfileQuiz,
  listQuiz,
  deleteCommunityQuiz,
  getUpdateQuizForm,
  updateCommunityQuiz,
  createQuizFromProfile,
  listPolls,
  deleteCommunityPoll,
  getUpdatePollForm,
  updateCommunityPoll,
  getPollCreateForm,
};
