const axios = require('axios');

const facebookConfig = require('../config/facebook');

const getFacebookAccessToken = async (fbCode, redirectUri) => {
  const { appId, appSecret } = facebookConfig;
  const url = `https://graph.facebook.com/v9.0/oauth/access_token?client_id=${appId}&redirect_uri=${redirectUri}&client_secret=${appSecret}&code=${fbCode}`;

  const response = await axios.get(url);
  const { access_token: accessToken } = response.data;

  return accessToken;
};

module.exports = {
  getFacebookAccessToken,
};
