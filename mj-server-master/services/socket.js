const request = require('request');
const Config = require('../config');

exports.socket = async () => {
  // await createWebhook();
  global.io.on('connection', function (socket) {
    global.socket = socket;
    console.log(`Device with ID ${socket.id} connected`);
  });

  global.io.on('disconnect', (socket) => {
    console.log(`Device with ID ${socket.id} disconnected`);
  });
};

const getListWebhooks = async (headers) => {
  const options = {
    url: 'https://api.tradegecko.com/webhooks',
    method: 'GET',
    headers,
  };

  const data = new Promise((resolve) => {
    request(options, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        const data = JSON.parse(body);
        resolve(data.webhooks);
      } else {
        resolve([]);
      }
    });
  });

  return data;
};

const createWebhook = async () => {
  let address = 'localhost:' + process.env.PORT;

  if (process.env.NODE_ENV === 'development') {
    address = 'https://mjdev.abcsoft.vn';
  }

  if (process.env.NODE_ENV === 'production') {
    address = 'https://mightyjaxx.technology';
  }

  address += '/webhooks';

  const headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + Config.tradeGeckoToken,
  };

  const body = {
    webhook: {
      address,
      event: 'variant.stock_level_update',
    },
  };

  const webhooks = await getListWebhooks(headers);

  const is_webhook_exist = webhooks.find(
    (v) => v.event === body.webhook.event && v.address === body.webhook.address,
  );

  if (!is_webhook_exist) {
    await new Promise((resolve, reject) => {
      const options = {
        url: 'https://api.tradegecko.com/webhooks',
        method: 'POST',
        body: JSON.stringify(body),
        headers: headers,
      };

      request(options, (error, response, body) => {
        resolve(JSON.parse(body));
      });
    });
  }
};
