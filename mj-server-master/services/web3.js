const Web3 = require('web3');
const fs = require('fs');
const { randomUUID } = require('crypto');
const Sentry = require('@sentry/node');
const { recoverPersonalSignature } = require('eth-sig-util');
const axios = require('axios');

const {
  forrealAbi,
  mjContractAbi,
  FORREAL_CONTRACT_ADDRESS,
  ALCHEMY_KEY_POLYGON,
  PRIVATE_KEY_POLYGON,
  PRIVATE_KEY_TTE,
  PUBLIC_KEY_TTE,
  TTE_NFT_VARIANTS,
  tteAbi,
  TTE_CONTRACT_ADDRESS,
} = require('../config/nft');
const { getUSDPrice } = require('./coingecko');
const { pinFileToIPFS, pinJSONToIPFS } = require('./pinata');

const { PUBLIC_KEY, NFT_CONTRACT_ADDRESS, PRIVATE_KEY } = process.env;

const web3 = new Web3(new Web3.providers.HttpProvider(process.env.EtherNet));
const options = {
  keepAlive: true,
  withCredentials: false,
  timeout: 5 * 1000, // ms
};
const web3Polygon = new Web3(
  new Web3.providers.HttpProvider(ALCHEMY_KEY_POLYGON, options),
);

const initWeb3 = () =>
  web3.eth
    .getBlock('latest')
    .catch((error) => console.log('Error at initWeb3:', error));

const isValidEthAddress = (address) => web3.utils.isAddress(address);

const getApiUrl = () => process.env.APP_URL || 'https://mightyjaxx.technology';

const getTxFee = async (toAddress) => {
  const nftContract = new web3.eth.Contract(
    mjContractAbi,
    NFT_CONTRACT_ADDRESS,
    {
      from: PUBLIC_KEY,
    },
  );

  const defaultTokenURI = `${getApiUrl()}/admin/products/nft/1`;

  const [gasPriceInGwei, gasLimitInGwei] = await Promise.all([
    web3.eth.getGasPrice(),
    nftContract.methods.mintNFT(toAddress, defaultTokenURI).estimateGas(),
  ]);

  // The total cost of a transaction is the product of the gas limit and gas price
  const transactionFeeInGwei = gasLimitInGwei * parseFloat(gasPriceInGwei);
  const transactionFeeEth = web3.utils.fromWei(
    `${transactionFeeInGwei}`,
    'ether',
  );

  return transactionFeeEth;
};

const mintNftToAddress = async (toAddress, feePaidByCustomer, onSuccess) => {
  const fromAddress = PUBLIC_KEY;
  const nftContract = new web3.eth.Contract(
    mjContractAbi,
    NFT_CONTRACT_ADDRESS,
    {
      from: fromAddress,
    },
  );
  const defaultTokenURI = `${getApiUrl()}/admin/products/nft/1`;
  const [ethPriceInUsd, gasPriceInGwei, tokenCount] = await Promise.all([
    getUSDPrice('ethereum'),
    web3.eth.getGasPrice(),
    nftContract.methods.mintNFT(toAddress, defaultTokenURI).call(),
  ]);

  const tokenURI = `${getApiUrl()}/admin/products/nft/${tokenCount}`;

  const txFeeInEth = (feePaidByCustomer / ethPriceInUsd).toFixed(12);
  const transactionFeeInGwei = web3.utils.toWei(`${txFeeInEth}`, 'ether');

  console.log({ txFeeInEth, feePaidByCustomer });

  const gas = parseInt(transactionFeeInGwei / gasPriceInGwei);

  const tx = {
    from: fromAddress,
    to: NFT_CONTRACT_ADDRESS,
    gas,
    value: 0,
    data: nftContract.methods.mintNFT(toAddress, tokenURI).encodeABI(),
  };

  const signedTx = await web3.eth.accounts.signTransaction(tx, PRIVATE_KEY);
  web3.eth.sendSignedTransaction(signedTx.rawTransaction, (err, hash) => {
    if (!err) {
      console.log(
        'The hash of your transaction is: ',
        hash,
        "\nCheck Alchemy's Mempool to view the status of your transaction!",
      );
      onSuccess(hash, tokenURI);
      return;
    }

    console.log('Something went wrong when submitting your transaction:', err);
  });

  return signedTx;
};

// Upload the TTE (Thailand Toy Exhibition) NFT metadata to IPFS
const uploadTteNftMetadataToIpfs = async (name, productSKU) => {
  if (!name || !productSKU) {
    throw new Error('Invalid parameters');
  }

  const { image, variantName } = TTE_NFT_VARIANTS[productSKU];

  // Upload JSON metadata to IPFS
  const json = {
    description: '',
    name,
    image,
    attributes: [
      {
        trait_type: 'Product Name',
        value: name,
      },
      {
        trait_type: 'Product SKU',
        value: productSKU,
      },
      {
        trait_type: 'Variant',
        value: variantName,
      },
    ],
  };

  const tokenURI = await pinJSONToIPFS(json);

  if (!tokenURI) {
    throw new Error('invalid_token_uri');
  }

  return tokenURI;
};

const uploadOwnershipDataToIPFS = async (productName, productSKU, UUID) => {
  if (!productName || !productSKU || !UUID) {
    throw new Error('Invalid parameters');
  }

  const UPLOAD_CERTIFICATE_IMAGE = false; // Disabled for now
  let imageCID = '';

  if (UPLOAD_CERTIFICATE_IMAGE) {
    // Upload certificate image to IPFS
    const path = `${__dirname}/example.png`;
    const file = fs.createReadStream(path);
    const fileName = randomUUID();
    const fileExt = '.png';

    imageCID = await pinFileToIPFS(file, `${fileName}.${fileExt}`);
    if (!imageCID) {
      throw new Error('invalid_image_cid');
    }
  }

  // Upload JSON metadata to IPFS
  const json = {
    description: productName,
    name: productName,
    attributes: [
      {
        trait_type: 'Product Name',
        value: productName,
      },
      {
        trait_type: 'Product SKU',
        value: productSKU,
      },
      {
        trait_type: 'UUID',
        value: UUID,
      },
    ],
  };

  if (imageCID) {
    json.image = imageCID;
  }

  const tokenURI = await pinJSONToIPFS(json);

  if (!tokenURI) {
    throw new Error('invalid_token_uri');
  }

  return tokenURI;
};

let nonce = null;

const resetNonceForreal = () => {
  nonce = null;
};

const mintNftForOwnershipRegistration = async (tokenURI) => {
  const fromAddress = PUBLIC_KEY;
  const toAddress = PUBLIC_KEY;
  const nftContract = new web3Polygon.eth.Contract(
    forrealAbi,
    FORREAL_CONTRACT_ADDRESS,
    {
      from: fromAddress,
    },
  );

  const [gasLimitInGwei, latestTokenId, latestNonce] = await Promise.all([
    nftContract.methods
      .safeMint(toAddress, tokenURI)
      .estimateGas({ from: fromAddress }),
    nftContract.methods._tokenIdCounter().call({ from: fromAddress }),
    web3Polygon.eth.getTransactionCount(PUBLIC_KEY, 'latest'), // get latest nonce
  ]);

  if (!nonce || latestNonce > nonce) {
    nonce = latestNonce;
  } else {
    nonce = parseInt(nonce) + 1;
  }

  // Set a higher gasLimit to increase the likelihood of the transaction being successful
  const multiplier = 2;
  const gasLimit = gasLimitInGwei * multiplier;

  // Add an upper limit to gasLimit
  const maxGas = 2000000;
  const gas = String(Math.min(gasLimit, maxGas));

  const tx = {
    from: fromAddress,
    to: FORREAL_CONTRACT_ADDRESS,
    gas,
    value: 0,
    nonce,
    data: nftContract.methods.safeMint(toAddress, tokenURI).encodeABI(),
  };
  const signedTx = await web3Polygon.eth.accounts.signTransaction(
    tx,
    PRIVATE_KEY_POLYGON,
  );

  const tokenId = String(parseInt(latestTokenId) + nonce - latestNonce);

  const txDataForSentry = {
    ...tx,
    tokenURI,
    tokenId,
  };
  const transactionHash = await sendSignedTransaction(
    signedTx,
    txDataForSentry,
  );

  if (!transactionHash) {
    throw new Error('invalid_tx_hash');
  }

  return { tokenId, transactionHash };
};

let nonceTte = null;

const resetNonceTte = () => {
  nonceTte = null;
};

const mintTteNft = async (quantity, toAddress, tokenURI, nftType) => {
  const fromAddress = PUBLIC_KEY_TTE;
  const nftContract = new web3Polygon.eth.Contract(
    tteAbi,
    TTE_CONTRACT_ADDRESS,
    {
      from: fromAddress,
    },
  );

  const [gasLimitInGwei, latestTokenId, latestNonce] = await Promise.all([
    nftContract.methods
      .safeMint(quantity, toAddress, tokenURI, nftType)
      .estimateGas({ from: fromAddress }),
    nftContract.methods._tokenIdCounter().call({ from: fromAddress }),
    web3Polygon.eth.getTransactionCount(fromAddress, 'latest'), // get latest nonce
  ]);

  if (!nonceTte || latestNonce > nonceTte) {
    nonceTte = latestNonce;
  } else {
    nonceTte = parseInt(nonceTte) + 1;
  }

  // Set a higher gasLimit to increase the likelihood of the transaction being successful
  const multiplier = 2;
  const gasLimit = gasLimitInGwei * multiplier;

  // Add an upper limit to gasLimit
  const maxGas = 2000000;
  const gas = String(Math.min(gasLimit, maxGas));

  const tx = {
    from: fromAddress,
    to: TTE_CONTRACT_ADDRESS,
    gas,
    value: 0,
    nonce: nonceTte,
    data: nftContract.methods
      .safeMint(quantity, toAddress, tokenURI, nftType)
      .encodeABI(),
  };
  const signedTx = await web3Polygon.eth.accounts.signTransaction(
    tx,
    PRIVATE_KEY_TTE,
  );

  const tokenId = String(parseInt(latestTokenId) + nonceTte - latestNonce);

  const txDataForSentry = {
    ...tx,
    tokenURI,
    tokenId,
  };
  const transactionHash = await sendSignedTransaction(
    signedTx,
    txDataForSentry,
  );

  if (!transactionHash) {
    throw new Error('invalid_tx_hash');
  }

  return { tokenId, transactionHash };
};

const burnNftForOwnershipRegistration = async (tokenId) => {
  const fromAddress = PUBLIC_KEY;
  const nftContract = new web3Polygon.eth.Contract(
    forrealAbi,
    FORREAL_CONTRACT_ADDRESS,
    {
      from: fromAddress,
    },
  );
  const gasLimitInGwei = await nftContract.methods
    .burn(tokenId)
    .estimateGas({ from: fromAddress });

  const tx = {
    from: fromAddress,
    to: FORREAL_CONTRACT_ADDRESS,
    gas: String(gasLimitInGwei),
    value: 0,
    data: nftContract.methods.burn(tokenId).encodeABI(),
  };

  const signedTx = await web3Polygon.eth.accounts.signTransaction(
    tx,
    PRIVATE_KEY_POLYGON,
  );
  const txDataForSentry = {
    ...tx,
    tokenId,
  };
  const transactionHash = await sendSignedTransaction(
    signedTx,
    txDataForSentry,
  );

  return transactionHash;
};

const getTokenURIForOwnershipRegistration = async (tokenId) => {
  try {
    const fromAddress = PUBLIC_KEY;
    const nftContract = new web3Polygon.eth.Contract(
      forrealAbi,
      FORREAL_CONTRACT_ADDRESS,
      {
        from: fromAddress,
      },
    );
    const tokenURI = await nftContract.methods
      .tokenURI(tokenId)
      .call({ from: fromAddress });

    return tokenURI;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const getTokenURIForTteNft = async (tokenId) => {
  try {
    const fromAddress = PUBLIC_KEY_TTE;
    const nftContract = new web3Polygon.eth.Contract(
      tteAbi,
      TTE_CONTRACT_ADDRESS,
      {
        from: fromAddress,
      },
    );

    const tokenURI = await nftContract.methods
      .tokenURI(tokenId)
      .call({ from: fromAddress });

    return tokenURI;
  } catch (error) {
    console.log(error);
    return null;
  }
};

const sendSignedTransaction = (signedTx = {}, txDataForSentry) =>
  new Promise((resolve, reject) =>
    web3Polygon.eth.sendSignedTransaction(
      signedTx.rawTransaction,
      (err, hash) => {
        if (!err) {
          console.log(
            'The hash of your transaction is: ',
            hash,
            "\nCheck Alchemy's Mempool to view the status of your transaction!",
          );
          return resolve(hash);
        }

        console.log(
          'Something went wrong when submitting your transaction:',
          err,
        );
        Sentry.captureException({ err, txDataForSentry });

        return reject(txDataForSentry);
      },
    ),
  );

const isValidSignature = (address, signature, messageToSign) => {
  if (!address || typeof address !== 'string' || !signature || !messageToSign) {
    return false;
  }

  const signingAddress = recoverPersonalSignature({
    data: messageToSign,
    sig: signature,
  });

  if (!signingAddress || typeof signingAddress !== 'string') {
    return false;
  }

  return signingAddress.toLowerCase() === address.toLowerCase();
};

const getEtherscanKey = () => {
  const getRandomInt = (max) => {
    return Math.floor(Math.random() * max);
  };

  const keys = [
    'VE7KDN36SF9MQHV7Q49ZUD1QGZ953ZBAN8',
    'J4VG2TPXUUP1CVXVMK4XB6UCXK2NI5VU86',
  ];

  const randomNum = getRandomInt(keys.length);

  return keys[randomNum];
};

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

const checkTransactionReceiptStatus = async (txHash, index) => {
  try {
    await sleep(500 * index);

    const key = getEtherscanKey();

    const { data } = await axios.get(
      `https://api.polygonscan.com/api?module=transaction&action=gettxreceiptstatus&txhash=${txHash}&apikey=${key}`,
    );

    const result = data?.result;

    if (result === 'Max rate limit reached') {
      return null;
    }
    const isValidTx = data?.result?.status === '1' || false;

    return isValidTx;
  } catch (error) {
    console.log('Error:', error);
    return null;
  }
};

module.exports = {
  initWeb3,
  getTxFee,
  isValidEthAddress,
  mintNftToAddress,
  mintNftForOwnershipRegistration,
  burnNftForOwnershipRegistration,
  getTokenURIForOwnershipRegistration,
  uploadOwnershipDataToIPFS,
  uploadTteNftMetadataToIpfs,
  mintTteNft,
  isValidSignature,
  getEtherscanKey,
  checkTransactionReceiptStatus,
  getTokenURIForTteNft,
  resetNonceTte,
  resetNonceForreal,
};
