const Config = require('../config/ethereumConfig');
const fs = require('fs');
const csvParser = require('csv-parser');
const { promises } = require('fs');
const csvToJson = require('csvtojson');
const common = require('../lib/common');
const { isNumber } = require('lodash');

async function generateEditionNumber(db, file, product) {
  if (file) {
    const arrayEditionNumber = await csvToJson().fromFile(file.path);
    const countEditionNumberExisted = await countEditionNumber(
      db,
      product.productVariantSku,
    );
    const total = countEditionNumberExisted + arrayEditionNumber.length;
    if (total > product.maxEditionNumber) {
      return {
        status: false,
        message: 'Exceed the maximum value',
      };
    }

    const udids = [];
    const validationUdids = [];
    const editionNumbers = [];
    let foundNotInt = false;
    let foundExceedMaxium = false;

    arrayEditionNumber.forEach((item) => {
      udids.push(item.UDID);
      editionNumbers.push(item['Edition Number']);
      if (!common.isInt(item['Edition Number']) || !item['Edition Number']) {
        foundNotInt = true;
      } else {
        if (parseInt(item['Edition Number']) > product.maxEditionNumber) {
          foundExceedMaxium = true;
        }
      }
    });

    if (foundNotInt) {
      return {
        status: false,
        message: 'Invalid edition number',
      };
    }
    if (foundExceedMaxium) {
      return {
        status: false,
        message: 'Exceed the maximum value',
      };
    }

    const toFindEditionNumberDuplicates = (editionNumbers) =>
      editionNumbers.filter(
        (item, index) => editionNumbers.indexOf(item) !== index,
      );
    const duplicateValue = toFindEditionNumberDuplicates(editionNumbers);

    const toFindUDIDNumberDuplicates = (editionNumbers) =>
      editionNumbers.filter(
        (item, index) => editionNumbers.indexOf(item) !== index,
      );
    const duplicateUDIDValue = toFindUDIDNumberDuplicates(editionNumbers);
    if (duplicateValue.length > 0 || duplicateUDIDValue.length > 0) {
      return {
        status: false,
        message: 'Duplicate Edition Number Or UDID',
      };
    }

    const validationItems = await db.qrcodes
      .find(
        {
          $and: [
            { udid: { $in: udids } },
            { productVariantSku: product.productVariantSku },
          ],
        },
        {
          projection: {
            udid: 1,
          },
        },
      )
      .toArray();

    validationItems.forEach((item) => {
      validationUdids.push(item.udid);
    });
    if (validationUdids.length === 0) {
      return {
        status: false,
        message: 'UDIDs in the CSV do not match',
      };
    }
    const difference = udids.filter((x) => !validationUdids.includes(x));
    if (difference.length > 0) {
      return {
        status: false,
        message: 'UDIDs in the CSV do not match with previously uploaded UDIDs',
      };
    }

    for (let i = 0; i < arrayEditionNumber.length; i++) {
      const editionNumber = arrayEditionNumber[i];
      await Promise.all([
        db.qrcodes.update(
          { udid: editionNumber.UDID },
          { $set: { editionNumber: editionNumber['Edition Number'] } },
          {},
        ),
        db.orders.update(
          { udid: editionNumber.UDID },
          { $set: { editionNumber: editionNumber['Edition Number'] } },
          {},
        ),
      ]);
    }
  }

  return {
    status: true,
    message: '',
  };
}

async function countEditionNumber(db, sku) {
  const countEditionNumber = await db.qrcodes.count({
    $and: [
      { productVariantSku: sku },
      { editionNumber: { $exists: true, $ne: '' } },
      { editionNumber: { $exists: true, $ne: null } },
    ],
  });
  return countEditionNumber;
}

module.exports = {
  generateEditionNumber,
  countEditionNumber,
};
