const axios = require('axios');

const getUSDPrice = async (coinId = 'ethereum') => {
  const coingeckoUrl = `https://api.coingecko.com/api/v3/simple/price?ids=${coinId}&vs_currencies=usd&include_24hr_change=true`;

  const res = await axios.get(coingeckoUrl);
  const { data } = res;
  const currentUsdPrice = (data && data[coinId] && data[coinId].usd) || 0;

  return currentUsdPrice;
};

module.exports = { getUSDPrice };
