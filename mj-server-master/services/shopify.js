const Multipassify = require('multipassify');
const axios = require('axios');
const uuid = require('uuid-random');
const { shopify } = require('../config/shopify');
const {
  MJStoreFrontAccessToken,
  multipassSecret,
  storeUrl,
  MJStoreFrontAdmin,
} = require('../config/shopify');
const {
  productQuery,
  decodeBase64,
  parseProductShopifyWithServer,
  parseProductFromServer,
  prepareProductDataToReturn,
} = require('./product');
const common = require('../lib/common');
const {encodeBase64} = require("../lib/common");

const storefrontHeaders = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
  'X-Shopify-Storefront-Access-Token': MJStoreFrontAccessToken,
};

const adminHeaders = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
  'X-Shopify-Access-Token': MJStoreFrontAdmin,
};

const apiVersion = '2021-01';
const shopifySite = 'https://mightyjaxx.myshopify.com';
const storefrontBaseURL = `${shopifySite}/api/${apiVersion}/graphql.json`;
const adminBaseURL = `${shopifySite}/admin/api/${apiVersion}/graphql.json`;

const storefrontInstance = axios.create({
  baseURL: storefrontBaseURL,
  timeout: 5000,
  headers: storefrontHeaders,
});

const adminInstance = axios.create({
  baseURL: adminBaseURL,
  timeout: 10000,
  headers: adminHeaders,
});

const queryCustomer = async (customerAccessToken, fields) => {
  const query = `query customer($customerAccessToken: String!) {
    customer(customerAccessToken: $customerAccessToken) ${fields}
  }`;

  const variables = {
    customerAccessToken,
  };
  const params = {
    query,
    variables,
  };

  const response = await storefrontInstance.post(null, params);
  const { data, errors } = response.data;
  if (errors && errors.length) {
    throw new Error(JSON.stringify(errors));
  }

  const customer = (data && data.customer) || null;

  return customer;
};

// check a product have shopify?
const checkProductShopify = async (shopifyId) => {
  const query = `query {
    product(id: "gid://shopify/Product/${shopifyId}") {
      id
      title
    }
  }`;
  const response = await shopify.graphql(query);
  console.log(response.product);
  if (response.product) {
    return true;
  } else return false;
};
// check a product have in store front?
const checkProductStoreFront = async (shopifyId) => {
  const query = `query{
    node(id: "gid://shopify/Product/${shopifyId}"){
      ... on Product{
        id
       }
      }
      } `;
  const response = await storefrontInstance.post(null, { query: query });
  if (response.data.data.node) {
    return true;
  } else return false;
};

const customerAccessTokenCreateWithMultipass = async (multipassToken) => {
  const query = `mutation customerAccessTokenCreateWithMultipass($multipassToken: String!) {
    customerAccessTokenCreateWithMultipass(multipassToken: $multipassToken) {
      customerAccessToken {
        accessToken
        expiresAt
      }
      customerUserErrors {
        code
        field
        message
      }
    }
  }`;
  const variables = {
    multipassToken,
  };
  const params = {
    query,
    variables,
  };

  const response = await storefrontInstance.post(null, params);
  const { data, errors } = response.data;

  if (errors && errors.length) {
    throw new Error(JSON.stringify(errors));
  }

  return data.customerAccessTokenCreateWithMultipass;
};

const customerUpdate = async (updates) => {
  const query = `mutation customerUpdate($input: CustomerInput!) {
    customerUpdate(input: $input) {
      customer {
        tags
      }
      userErrors {
        field
        message
      }
    }
  }`;
  const variables = {
    input: updates,
  };
  const params = {
    query,
    variables,
  };

  const response = await adminInstance.post(null, params);
  const { data, errors } = response.data;

  if (errors && errors.length) {
    throw new Error(JSON.stringify(errors));
  }

  const userErrors =
    (data && data.customerUpdate && data.customerUpdate.userErrors) || [];
  const customer =
    (data && data.customerUpdate && data.customerUpdate.customer) || [];

  if (userErrors && userErrors.length) {
    throw new Error(JSON.stringify(userErrors));
  }

  return customer.tags;
};

const queryCustomerById = async (id, fields) => {
  const query = `query customer($id: ID!) {
    customer(id: $id) ${fields}
  }`;

  const variables = {
    id: `gid://shopify/Customer/${id}`,
  };
  const params = {
    query,
    variables,
  };

  const response = await adminInstance.post(null, params);
  const { data, errors } = response.data;

  if (errors && errors.length) {
    throw new Error(JSON.stringify(errors));
  }

  const userErrors = (data && data.customer && data.customer.userErrors) || [];

  if (userErrors && userErrors.length) {
    throw new Error(JSON.stringify(userErrors));
  }

  const customer = (data && data.customer) || null;

  return customer;
};

const generateMultipassToken = (customerData) => {
  // Construct the Multipassify encoder
  const multipassify = new Multipassify(multipassSecret);
  return multipassify.encode(customerData);
};

const defaultRedirectToPath = 'account/login/multipass/';

const generateMultipassUrl = (path = defaultRedirectToPath, token) => {
  return `${storeUrl}${path}${token}`;
};

const getAccessTokenFromSocialProfile = async (
  fullName,
  email,
  emailVerified,
  socialMethod,
) => {
  const fullNameArray = (fullName && fullName.split(' ')) || [];
  const firstName = (fullNameArray && fullNameArray[0]) || '';
  const lastName = (fullNameArray && fullNameArray[1]) || '';

  const customerData = {
    ...(firstName && { first_name: firstName }),
    ...(lastName && { last_name: lastName }),
    email,
    verified_email: emailVerified,
    created_at: new Date().toISOString(),
  };

  const multipassToken = generateMultipassToken(customerData);
  const storefrontRes = await customerAccessTokenCreateWithMultipass(
    multipassToken,
  );

  const accessToken =
    (storefrontRes &&
      storefrontRes.customerAccessToken &&
      storefrontRes.customerAccessToken.accessToken) ||
    '';

  if (!accessToken) {
    throw new Error('Invalid access token');
  }

  const newTags = ['social_login', socialMethod];
  const fields = `{
      tags,
      id,
    }`;

  const { tags: existingTags, id: customerId } = await queryCustomer(
    accessToken,
    fields,
  );

  let hasTermsToken = false;

  if (existingTags && existingTags.length) {
    hasTermsToken =
      existingTags.filter((tag) => tag.includes('terms_token')).length > 0;
  }

  if (!hasTermsToken) {
    newTags.push(`terms_token_${uuid()}`);
  }

  const tags = [...existingTags, ...newTags];
  // Remove duplicate values
  const uniqTags = [...new Set(tags)];

  await customerUpdate({
    tags: uniqTags,
    id: customerId,
  });

  const result = { accessToken, customerData, tags: uniqTags, customerId };

  return result;
};

const nyansumTag = async (req, limit = 1) => {
  // let pageSize = req.query.pageSize || 10;
  // if (pageSize > 10 || pageSize < 0) {
  //   pageSize = 10;
  // }
  const pageSize = 250;
  let nextCursor = req.query.nextCursor;

  if (nextCursor === 'null' || nextCursor === 'undefined') {
    nextCursor = null;
  }
  const nextPage = nextCursor
    ? `first:${pageSize}, after:"${nextCursor}"`
    : `first:${pageSize}`;
  const query = `{
    products(${nextPage}, query:"tag:${req.query.productTag}"){
      edges{
        cursor
        node{
        createdAt
        description
        descriptionHtml
        handle
        id
        title
        tags
        onlineStoreUrl
        images(first: 100){
          edges{
            node{
              originalSrc
              transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
            }
          }
        }
        productType
        options{
          id
          name
          values
        }
        priceRange{
          maxVariantPrice{
            amount
            currencyCode
          }
          minVariantPrice{
            amount
            currencyCode
          }
        }
        variants(first: 100){
          edges{
            node{
          compareAtPrice
          id
          image{
            originalSrc
            transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
          }
          price
          sku
          selectedOptions{
            name
            value
          }
          title
          weight
          weightUnit
          currentlyNotInStock
            }
          }
        }

        }
      }
      pageInfo{
        hasNextPage
      }
    }
  }`;
  const params = {
    query,
  };

  const response = await storefrontInstance.post(null, params);
  const { data, errors } = response.data;

  if (errors && errors.length) {
    throw new Error(JSON.stringify(errors));
  }
  return data;
};

const productTransform = async (productRaw) => {
  let { products } = productRaw;
  products.edges.map((e) => (e.node.cursor = e.cursor));
  products = products.edges.map((e) => e.node);
  for (let i = 0; i < products.length; i++) {
    const imageSources = products[i].images.edges.length
      ? products[i].images.edges.map((e) => Object.values(e.node)[1])
      : [];
    let listVariants = products[i].variants.edges.map((e) => e.node);

    listVariants = listVariants.map((e) => {
      e.optionValues = e.selectedOptions;
      e.imageSrc = e.image ? e.image.transformedSrc : null;
      delete e.selectedOptions;
      delete e.image;
      return e;
    });
    products[i] = {
      cursor: products[i].cursor,
      allowVIPEarlyAccess: undefined,
      artistName: '',
      available: undefined,
      availableForSale: products[i].availableForSale
        ? products[i].availableForSale
        : undefined,
      backgroundImageUrl: undefined,
      createdAt: products[i].createdAt,
      description: products[i].description,
      descriptionHtml: products[i].descriptionHtml,
      handle: products[i].handle,
      id: products[i].id,
      imageSources: imageSources ? imageSources : [],
      isProductForVIP: null,
      launchDateColor: null,
      lowestPrice: products[i].priceRange.minVariantPrice.amount,
      onlineStoreUrl: products[i].onlineStoreUrl,
      options: products[i].options,
      productType: products[i].productType,
      selections: products[i].selections ? products[i].selections : null,
      shopifyProductId: products[i].id,
      sku: undefined,
      tags: products[i].tags,
      thumbnailSrc: products[i].images.edges.length
        ? products[i].images.edges[0].node.transformedSrc
        : '',
      title: products[i].title,
      titleOnApp: '',
      variants: listVariants ? listVariants : [],
      outOfStock: null,
      price: products[i].priceRange.minVariantPrice.amount,
    };
  }
  return {
    result: products,
    pageInfo: productRaw['products']['pageInfo'],
  };
};

const getProductTagById = async (req, shopifyProductId) => {
  const query = `{
  products(first: 1, query: "id:${shopifyProductId}") {
    edges {
      node {
        id
        tags
      }
    }
  }
  }`;
  const params = {
    query,
  };

  const response = await storefrontInstance.post(null, params);
  const { data, errors } = response.data;

  if (errors && errors.length) {
    throw new Error(JSON.stringify(errors));
  }
  return data;
};

const getProductFromShopifyById = async (shopifyIdBase64) => {
  const query = `
      query{
        node(id: "${shopifyIdBase64}"){
          ... on Product{
            ${productQuery()}
          }
        }
      }
      `;
  const shopifyResponse = await adminInstance.post(null, { query: query });
  if (!shopifyResponse) {
    return null;
  }
  const { data } = shopifyResponse;
  if (data.errors && data.errors.length) {
    return null;
  }
  return data.data.node;
};

const mergeProductFromShopifyToSaveLocal = async (
  productDB,
  productShopify,
) => {
  console.log(productShopify);
  if (productShopify) {
    productDB.variants = productShopify.variants;
    productDB.productHandle = productShopify.handle;
    productDB.productTitle = productShopify.title;
    productDB.options = productShopify.options;
    productDB.images = productShopify.images;
    productDB.productType = productShopify.product_type;
    productDB.priceRange = productShopify.priceRange;
    productDB.onlineStoreUrl = productShopify.onlineStoreUrl;
    productDB.productTags = productShopify.tags?.join(',');
    productDB.productDescription = productShopify.descriptionHtml;
    productDB.vendor = productShopify.vendor;
    productDB.createdAt = productShopify.createdAt;
  }
  return productDB;
};

const getProductByIds = async (listIdBase64, db) => {
  let data = [];
  // easy way is forach and take one by one product
  for (const idBase64 of listIdBase64) {
    const productDB = await getProductById(idBase64, db, true);
    if (productDB) data.push(productDB);
  }
  return data;
};

const getProductById = async (idBase64, db, compressData) => {
  const dbProduct = await db.products.findOne({
    shopifyProductId: decodeBase64(idBase64),
  });

  return await getProductDetail(dbProduct, db, compressData);
};

const getProductBySku = async (sku, db) => {
  const dbProduct = await db.products.findOne({
    productVariantSku: sku,
  });

  return await getProductDetail(dbProduct, db);
};

const getProductDetail = async (dbProduct, db, compressData) => {
  if (!dbProduct) {
    return null;
  }

  if (process.env.ENABLE_LOAD_DATA_LOCAL_DB === 'true') {
    // product is missing field from shopify then sync it again
    if (!dbProduct.priceRange || !dbProduct.variants) {
      const productShopify = await getProductFromShopifyById(
        encodeBase64('gid://shopify/Product/' + dbProduct.shopifyProductId),
      );
      // Check if id is invalid
      if (!productShopify) {
        return null;
      }
      //merge shopify product
      dbProduct = await mergeProductFromShopifyToSaveLocal(
        dbProduct,
        productShopify,
      );
      //save to log
      common.saveToActionLog(
        db,
        'system_update_missing_fields',
        'update_product',
        dbProduct,
      );
      await db.products.update(
        { _id: common.getId(dbProduct._id) },
        { $set: dbProduct },
        { multi: false },
      );
    }
    return prepareProductDataToReturn(dbProduct, compressData);
  } else {
    const productShopify = await getProductFromShopifyById(
      encodeBase64('gid://shopify/Product/' + dbProduct.shopifyProductId),
    );
    // Check if id is invalid
    if (!productShopify) {
      return null;
    }

    let findProduct =
      dbProduct && Object.keys(dbProduct).length
        ? parseProductFromServer(dbProduct)
        : null;

    if (findProduct) {
      findProduct = parseProductShopifyWithServer(productShopify, findProduct);
    }
    return findProduct;
  }
};

module.exports = {
  getAccessTokenFromSocialProfile,
  customerAccessTokenCreateWithMultipass,
  generateMultipassUrl,
  queryCustomerById,
  generateMultipassToken,
  queryCustomer,
  customerUpdate,
  nyansumTag,
  productTransform,
  getProductTagById,
  storefrontInstance,
  getProductFromShopifyById,
  mergeProductFromShopifyToSaveLocal,
  checkProductShopify,
  checkProductStoreFront,
  getProductById,
  getProductBySku,
  getProductByIds,
};
