const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const fetch = require('node-fetch');
const getUserFragment = require('../graphql/getCustomer').getUserFragment;
const { MJStoreFrontAccessToken } = require('../config/shopify');

const registerCustomerInDB = async (
  db,
  customerAccessToken,
  config,
  facebookId,
) => {
  const apiVersion = '2020-04';

  const shopifyResponse = await fetch(
    `${config.endPoint}api/${apiVersion}/graphql.json`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'X-Shopify-Storefront-Access-Token': MJStoreFrontAccessToken,
        Accept: 'application/json',
      },
      body: JSON.stringify({
        query: `
            query{
                customer(customerAccessToken: "${customerAccessToken}"){
                    ${getUserFragment()}
                }
            }
            `,
      }),
    },
  );
  const shopifyData = await shopifyResponse.json();

  if (!shopifyData.data.customer.email) {
    throw new Error('No email found.');
  }

  const customer = shopifyData.data.customer;

  const buff = Buffer.from(customer.id, 'base64');
  const customer_url = buff.toString('ascii');
  const customerId = customer_url.split('/')[4];

  const [checkCustomer] = await Promise.all([
    db.customers.findOne({
      $or: [{ customerId: customerId.toString() }],
    }),
  ]);

  if (checkCustomer) {
    // Customer Exists
    return;
  }

  const doc = {
    ...customer,
    ...(facebookId && { facebookId }),
    password: bcrypt.hashSync(customer.email, 10),
    customerId: customerId.toString(),
    created: new Date(),
    feedPrivate: false,
    vaultPrivate: false,
  };

  // email is ok to be used.
  await db.customers.insertOne(doc);

  // Customer creation successful
  const token = jwt.sign(
    {
      email: customer.email,
      customerId: customerId.toString(),
      shopifyToken: customerAccessToken,
    },
    config.jwtSecret,
  );
  return token;
};

module.exports = {
  registerCustomerInDB,
};
