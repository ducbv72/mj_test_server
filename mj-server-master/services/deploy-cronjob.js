const BlockChain = require('../admin-dashboard/routes/v4/blockchain');
const ethereumConfig = require('../config/ethereumConfig');
const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(process.env.EtherNet));
const Promise = require('promise');
const MerkleTools = require('merkle-tools');
const adminFirebase = require('firebase-admin');

module.exports = {
  check_deploy_registration: async function check_deploy_registration(app) {
    const db = app.db;
    try {
      const [orders] = await Promise.all([
        db.orders
          .find({
            $and: [
              { 'orderProducts.blockHash': { $exists: true } },
              { 'orderProducts.orderSmartContract': { $exists: false } },
            ],
          })
          .limit(10)
          .toArray(),
      ]);
      if (!orders) return;
      for (let i = 0; i < orders.length; i++) {
        const order = orders[i];
        for (let j = 0; j < order.orderProducts.length; j++) {
          const product = order.orderProducts[j];
          // let receipt = web3.eth.getTransactionReceipt(product.blockHash);
          // let certNumber = Math.floor(1000 + Math.random() * 9000);
          // if(receipt){
          // generate certificate
          let datePublish = new Date(
            order.orderProducts[0].productPublishedAt
              ? order.orderProducts[0].productPublishedAt
              : order.orderProducts[0].productAddedDate,
          );
          if (datePublish == null) {
            datePublish = new Date();
          }
          // let certificateNumber = 'MJMP' + datePublish.getFullYear() + (Math.ceil(currentDate.getTime() / 1000)).toString();
          await Promise.all([
            db.orders.update(
              {
                'orderProducts.productEncryptedModel':
                  product.productEncryptedModel,
              },
              {
                $set: {
                  'orderProducts.$.orderSmartContract': product.blockHash,
                  'orderProducts.$.smartContract': product.blockHash,
                  // 'orderProducts.$.productCertNumber': certificateNumber,
                  orderDate: new Date(),
                },
              },
              { multi: false },
            ),
          ]);
          const payload = {};
          payload.orderId = order.shopifyOrderId.toString();
          payload.productId = product.shopifyProductId.toString();
          payload.orderSmartContract = product.blockHash;
          payload.productCertNumber = product.productCertNumber;
          payload.type = 'RegisterBLC';
          const message = {
            notification: {
              title: 'MightyJaxx',
              body: 'Registration of ownership successful. Click here to view certificate of authenticity',
            },
            data: payload,
          };
          const options = {
            priority: 'high',
          };
          adminFirebase
            .messaging()
            .sendToTopic(order.customerId.toString(), message, options)
            .then((response) => {
              console.log(response);
            })
            .catch((error) => {
              console.log('Error sending message:', error);
            });
          // }
        }
      }
    } catch (ex) {
      console.log(ex);
    }
  },

  check_deploy_merkle_registration:
    async function check_deploy_merkle_registration(app) {
      const db = app.db;
      try {
        const [merkleroots] = await Promise.all([
          db.merkleroots
            .find({
              $and: [
                { blockHash: { $exists: true } },
                { smartContract: { $exists: false } },
              ],
            })
            .limit(100)
            .toArray(),
        ]);
        if (!merkleroots) {
          console.log('All item has been deployed');
          return;
        }
        for (let i = 0; i < merkleroots.length; i++) {
          const item = merkleroots[0];
          const receipt = web3.eth.getTransactionReceipt(item.blockHash);
          console.log(receipt);
          if (receipt) {
            // update merketRoot With SmartContract
            await Promise.all([
              db.merkleroots.update(
                { blockHash: item.blockHash },
                {
                  $set: {
                    smartContract: receipt.contractAddress,
                    deployedDate: new Date(),
                  },
                },
                { multi: false },
              ),
            ]);
          }
        }
      } catch (ex) {
        console.log(ex);
      }
    },

  check_deploy_change_owner: async function check_deploy_change_owner(app) {
    const db = app.db;
    try {
      const [orders] = await Promise.all([
        db.orders
          .find({
            $and: [
              { 'orderProducts.blockHashChange': { $exists: true } },
              { 'orderProducts.changeOwnerDone': false },
            ],
          })
          .limit(10)
          .toArray(),
      ]);
      if (!orders) return;
      for (let i = 0; i < orders.length; i++) {
        const order = orders[i];
        for (let j = 0; j < order.orderProducts.length; j++) {
          const product = order.orderProducts[j];
          const oldCustomer = order.customerId;
          // let receipt = web3.eth.getTransactionReceipt(product.blockHash);
          // if(receipt){
          await Promise.all([
            db.orders.update(
              {
                'orderProducts.productEncryptedModel':
                  product.productEncryptedModel,
              },
              {
                $set: {
                  'orderProducts.$.changeOwnerDone': true,
                  orderEmail: product.newOwnerEmail,
                  orderFirstname: '',
                  orderLastname: '',
                  customerId: order.newCustomerId,
                },
              },
              { multi: false },
            ),
          ]);
          const payload = {};
          payload.orderId = order.shopifyOrderId.toString();
          payload.productId = product.shopifyProductId.toString();
          payload.newOwnerEmail = product.newOwnerEmail;
          payload.type = 'ChangeOwner';
          const message = {
            notification: {
              title: 'MightyJaxx',
              body: 'Your request has been applied',
            },
            data: payload,
          };
          const options = {
            priority: 'high',
          };
          adminFirebase
            .messaging()
            .sendToTopic(oldCustomer, message, options)
            .then((response) => {
              console.log(response);
            })
            .catch((error) => {
              console.log('Error sending message:', error);
            });
        }
      }
    } catch (ex) {
      console.log(ex);
    }
  },

  deploy_registered_toy_today: async function deploy_registered_toy_today(app) {
    const db = app.db;
    const [orders] = await Promise.all([
      db.orders
        .find({
          $and: [
            { merkleRootId: { $exists: false } },
            { orderBlockHash: { $exists: true } },
          ],
        })
        .limit(100)
        .toArray(),
    ]);
    if (orders.length > 1) {
      const arrayHash = [];
      orders.forEach(async (order) => {
        if (order.orderBlockHash.length > 0 && order.orderBlockHash) {
          arrayHash.push(order.orderBlockHash);
        }
      });
      if (arrayHash.length > 0) {
        const dataInput = arrayHash.map((x) => new Buffer(x, 'hex'));
        const merkleTools = new MerkleTools();
        merkleTools.addLeaves(dataInput);
        merkleTools.makeTree();
        const root = merkleTools.getMerkleRoot();
        if (root) {
          const merkle_root = root.toString('hex');
          console.log(merkle_root);
          const Sol_MJSmartContractABI =
            ethereumConfig.Sol_MJSmartContractABI_v2;
          const Sol_MJSmartContractData =
            ethereumConfig.Sol_MJSmartContractData_v2;
          const FRRegistrationContract = web3.eth.contract(
            Sol_MJSmartContractABI,
          );
          const payloadData = FRRegistrationContract.new.getData(merkle_root, {
            data: Sol_MJSmartContractData,
          });
          const serialized = BlockChain.ethRawTx(
            'contract',
            payloadData,
            ethereumConfig.accountDefault,
            '',
          );
          BlockChain.ethSendRawTransaction(serialized, async (err, hash) => {
            if (hash) {
              const newMerkleRoot = {
                merkleRootHash: merkle_root,
                abi: ethereumConfig.Sol_MJSmartContractABI_v2,
                data: ethereumConfig.Sol_MJSmartContractData_v2,
                arrayHash: arrayHash,
                blockHash: hash,
                createdDate: new Date(),
              };
              const [result1] = await Promise.all([
                db.merkleroots.insertOne(newMerkleRoot),
              ]);
              for (let i = 0; i < orders.length; i++) {
                const order = orders[0];
                order.merkleRootId = result1.ops[0]._id;
                await Promise.all([
                  db.orders.update({ _id: order._id }, { $set: order }, {}),
                ]);
              }
            } else {
              console.log('Deploy fail. There is something wrong');
            }
          });
        } else {
          console.log('Deploy fail. There is something wrong');
        }
      } else {
        console.log('Nothing to deploy!!!');
      }
    } else {
      console.log('Nothing to deploy!!!');
    }
  },
};
