const _ = require('lodash');
const uglifycss = require('uglifycss');
const colors = require('colors');
const lunr = require('lunr');
const cheerio = require('cheerio');
const fs = require('fs');
const path = require('path');
const glob = require('glob');
const async = require('async');
const nodemailer = require('nodemailer');
const sanitizeHtml = require('sanitize-html');
const escape = require('html-entities').AllHtmlEntities;
const mkdirp = require('mkdirp');
const ObjectId = require('mongodb').ObjectId;
const schedule = require('node-schedule');
const csvToJson = require('csvtojson');
const Config = require('../config');
const AWS = require('aws-sdk');
const crypto = require('crypto');
const sharp = require('sharp');
const s3 = new AWS.S3({
  accessKeyId: Config.S3.accessKeyId,
  secretAccessKey: Config.S3.secretAccessKey,
  region: Config.S3UdidRequest.region,
});

require('dotenv').config();
const restrictedRoutesAdmin = [];
const restrictedRoutesMarketting = [
  { route: '/admin/users', response: 'redirect' },
  { route: '/admin/user/edit/:id', response: 'redirect' },
  { route: '/admin/user/new', response: 'redirect' },
  { route: '/admin/user/delete/:id', response: 'redirect' },
  { route: '/admin/user/update', response: 'redirect' },

  { route: '/admin/sports', response: 'redirect' },
  { route: '/admin/sport/edit/:id', response: 'redirect' },
  { route: '/admin/sport/new', response: 'redirect' },
  { route: '/admin/sport/delete/:id', response: 'redirect' },
  { route: '/admin/sport/update', response: 'redirect' },

  { route: '/admin/user/insert', response: 'redirect' },
  { route: '/admin/qrcode/send-csv-to-manufacture', response: 'redirect' },
  { route: '/admin/qrcode/nfc-udid-request', response: 'redirect' },
  { route: '/admin/settings/update', response: 'redirect' },
  { route: '/admin/blockchains/:page', response: 'redirect' },
  { route: '/admin/settings', response: 'redirect' },
  { route: '/admin/settings/update', response: 'redirect' },
  { route: '/admin/email/delete/:id', response: 'redirect' },
  { route: '/admin/qrcode/filter/:search', response: 'redirect' },
  { route: '/admin/qrcode/new/:id', response: 'redirect' },
  { route: '/admin/qrcode/view/:id/:page', response: 'redirect' },
  { route: '/admin/qrcode/insert', response: 'redirect' },
  { route: '/admin/qrcode/reset-udid/:id', response: 'redirect' },
  { route: '/admin/qrcode/delete-all-qrcode/:id', response: 'redirect' },
  { route: '/admin/qrcode/reset-all-udid/:id/:id', response: 'redirect' },
  {
    route: '/admin/qrcode/export-qrcode-csv/:id/:from/:to',
    response: 'redirect',
  },
  { route: '/admin/qrcode/import/:id', response: 'redirect' },
  { route: '/admin/qrcode/import', response: 'redirect' },
  { route: '/admin/qrcode/filter/:id/:search', response: 'redirect' },
  { route: '/admin/qrcode/update-udid/:qrcodeId', response: 'redirect' },
  { route: '/admin/qrcode/update-udid', response: 'redirect' },
  { route: '/admin/qrcode/send-csv-to-manufacture', response: 'redirect' },
  { route: '/admin/qrcode/count/:sku', response: 'redirect' },
  { route: '/admin/qrcode/create-new-encrypted-string', response: 'redirect' },
  { route: '/admin/email/delete/:id', response: 'redirect' },
  // disable order/certificate
  { route: '/admin/orders', response: 'redirect' },
  { route: '/admin/orders/:page', response: 'redirect' },
  { route: '/admin/order/summary', response: 'redirect' },
  { route: '/admin/app-released', response: 'redirect' },
  { route: '/admin/app-release/new', response: 'redirect' },
  { route: '/admin/app-release/insert', response: 'redirect' },
  { route: '/admin/app-release/edit/:id', response: 'redirect' },
  { route: '/admin/app-release/edit/:id', response: 'redirect' },
  { route: '/admin/app-release/update', response: 'redirect' },
  { route: '/admin/app-release/delete/:id', response: 'redirect' },
  { route: '/admin/product/new', response: 'redirect' },
  { route: '/admin/product/insert', response: 'redirect' }
];

const restrictedRoutesProduction = [
  // Cannot access ‘Stores’, ‘Certificates’, ‘Artist', ‘Notification’, ‘Blockchain Transactions’ and ‘Settings’ page
  { route: '/admin/users', response: 'redirect' },
  { route: '/admin/user/edit/:id', response: 'redirect' },
  { route: '/admin/user/new', response: 'redirect' },
  { route: '/admin/user/delete/:id', response: 'redirect' },
  { route: '/admin/user/update', response: 'redirect' },
  { route: '/admin/user/insert', response: 'redirect' },
  { route: '/admin/settings/update', response: 'redirect' },
  { route: '/admin/blockchains/:page', response: 'redirect' },
  { route: '/admin/settings', response: 'redirect' },
  { route: '/admin/settings/update', response: 'redirect' },
  { route: '/admin/email/delete/:id', response: 'redirect' },
  { route: '/admin/stores', response: 'redirect' },
  { route: '/admin/store/filter/:search', response: 'redirect' },
  { route: '/admin/store/new', response: 'redirect' },
  { route: '/admin/store/insert', response: 'redirect' },
  { route: '/admin/store/edit/:id', response: 'redirect' },
  { route: '/admin/store/update', response: 'redirect' },
  { route: '/admin/store/delete/:id', response: 'redirect' },
  { route: '/admin/stores/list', response: 'redirect' },
  { route: '/admin/artist/filter/:id/:search', response: 'redirect' },
  { route: '/admin/artists/:page', response: 'redirect' },
  { route: '/admin/artist/edit/:id', response: 'redirect' },
  { route: '/admin/artist/new/', response: 'redirect' },
  { route: '/admin/artist/update', response: 'redirect' },
  { route: '/admin/artist/insert', response: 'redirect' },
  { route: '/admin/artist/delete/:id', response: 'redirect' },
  { route: '/admin/settings/notification', response: 'redirect' },
  { route: '/admin/settings/send-notification', response: 'redirect' },
  { route: '/admin/settings/scheduled/notification', response: 'redirect' },
  {
    route: '/admin/settings/scheduled/notification/:page',
    response: 'redirect',
  },
  { route: '/admin/notification/delete/:id', response: 'redirect' },
  { route: '/admin/settings/scheduled/emails', response: 'redirect' },
  { route: '/admin/settings/scheduled/send-email/:page', response: 'redirect' },
  { route: '/admin/settings/scheduled/emails/:page', response: 'redirect' },
  { route: '/admin/notification/edit/:id', response: 'redirect' },
  { route: '/admin/email/delete/:id', response: 'redirect' },
  { route: '/admin/orders/:page', response: 'redirect' },
  { route: '/admin/order/summary', response: 'redirect' },
  { route: '/admin/app-release/new', response: 'redirect' },
  { route: '/admin/app-release/insert', response: 'redirect' },
  { route: '/admin/app-release/edit/:id', response: 'redirect' },
  { route: '/admin/app-release/edit/:id', response: 'redirect' },
  { route: '/admin/app-release/update', response: 'redirect' },
  { route: '/admin/app-release/delete/:id', response: 'redirect' },
  { route: '/admin/product/delete/:id', response: 'redirect' },
  { route: '/admin/community-poll/insert', response: 'redirect' },
  { route: '/admin/community-poll/delete/:id', response: 'redirect' },
  { route: '/admin/community-poll/update/:id', response: 'redirect' },
  { route: '/admin/community-profile/list-polls', response: 'redirect' },
  { route: '/admin/community-profile', response: 'redirect' },
  { route: '/admin/community-profile', response: 'redirect' },
  { route: '/admin/community-profile/type/:profileType', response: 'redirect' },
  { route: '/admin/community-profile/new', response: 'redirect' },
  { route: '/admin/community-profile/insert', response: 'redirect' },
  { route: '/admin/community-profile/edit/:id', response: 'redirect' },
  { route: '/admin/community-profile/delete/:id', response: 'redirect' },
  {
    route: '/admin/community-profile/edit/update-status/:id',
    response: 'redirect',
  },
  {
    route: '/admin/community-profile/edit/update-orders/:profileType',
    response: 'redirect',
  },
  { route: '/admin/community-profile/:id/create-poll', response: 'redirect' },
  { route: '/admin/exclusive-sale', response: 'redirect' },
  { route: '/admin/exclusive-sale/new', response: 'redirect' },
  { route: '/admin/exclusive-sale/insert', response: 'redirect' },
  { route: '/admin/exclusive-sale/edit/:id', response: 'redirect' },
  { route: '/admin/exclusive-sale/update', response: 'redirect' },
  { route: '/admin/exclusive-sale/delete/:id', response: 'redirect' },
  { route: '/admin/exlusive-sale/upload-file', response: 'redirect' },
];

const restrictedRoutesCustomerService = [
  // cannot access ‘Send Encrypted String’ page, Cannot access ‘Stores’, ‘Certificates’, ‘Artist', ‘Notification’, ‘Blockchain Transactions’ and ‘Settings’ page
  { route: '/er/delete/:id', response: 'redirect' },
  { route: '/admin/user/admin/users', response: 'redirect' },
  { route: '/admin/user/edit/:id', response: 'redirect' },
  { route: '/admin/user/new', response: 'redirect' },
  { route: '/admin/usupdate', response: 'redirect' },
  { route: '/admin/user/insert', response: 'redirect' },
  { route: '/admin/qrcode/send-csv-to-manufacture', response: 'redirect' },
  { route: '/admin/qrcode/nfc-udid-request', response: 'redirect' },
  { route: '/admin/settings/update', response: 'redirect' },
  { route: '/admin/blockchains/:page', response: 'redirect' },
  { route: '/admin/settings', response: 'redirect' },
  { route: '/admin/settings/update', response: 'redirect' },
  { route: '/admin/email/delete/:id', response: 'redirect' },
  { route: '/admin/stores', response: 'redirect' },
  { route: '/admin/store/filter/:search', response: 'redirect' },
  { route: '/admin/store/new', response: 'redirect' },
  { route: '/admin/store/insert', response: 'redirect' },
  { route: '/admin/store/edit/:id', response: 'redirect' },
  { route: '/admin/store/update', response: 'redirect' },
  { route: '/admin/store/delete/:id', response: 'redirect' },
  { route: '/admin/stores/list', response: 'redirect' },
  { route: '/admin/artist/filter/:id/:search', response: 'redirect' },
  { route: '/admin/artists/:page', response: 'redirect' },
  { route: '/admin/artist/edit/:id', response: 'redirect' },
  { route: '/admin/artist/new/', response: 'redirect' },
  { route: '/admin/artist/update', response: 'redirect' },
  { route: '/admin/artist/insert', response: 'redirect' },
  { route: '/admin/artist/delete/:id', response: 'redirect' },
  { route: '/admin/settings/notification', response: 'redirect' },
  { route: '/admin/settings/send-notification', response: 'redirect' },
  { route: '/admin/settings/scheduled/notification', response: 'redirect' },
  {
    route: '/admin/settings/scheduled/notification/:page',
    response: 'redirect',
  },
  { route: '/admin/notification/delete/:id', response: 'redirect' },
  { route: '/admin/notification/edit/:id', response: 'redirect' },
  { route: '/admin/settings/scheduled/emails', response: 'redirect' },
  { route: '/admin/settings/scheduled/send-email/:page', response: 'redirect' },
  { route: '/admin/settings/scheduled/emails/:page', response: 'redirect' },
  { route: '/admin/email/delete/:id', response: 'redirect' },
  { route: '/admin/orders', response: 'redirect' },
  { route: '/admin/orders/:page', response: 'redirect' },
  { route: '/admin/order/summary', response: 'redirect' },
  { route: '/admin/app-release/new', response: 'redirect' },
  { route: '/admin/app-release/insert', response: 'redirect' },
  { route: '/admin/app-release/edit/:id', response: 'redirect' },
  { route: '/admin/app-release/edit/:id', response: 'redirect' },
  { route: '/admin/app-release/update', response: 'redirect' },
  { route: '/admin/app-release/delete/:id', response: 'redirect' },
  { route: '/admin/product/new', response: 'redirect' },
  { route: '/admin/product/insert', response: 'redirect' },
  { route: '/admin/product/delete/:id', response: 'redirect' },
  { route: '/admin/community-poll/insert', response: 'redirect' },
  { route: '/admin/community-poll/delete/:id', response: 'redirect' },
  { route: '/admin/community-poll/update/:id', response: 'redirect' },
  { route: '/admin/community-profile/list-polls', response: 'redirect' },
  { route: '/admin/community-profile', response: 'redirect' },
  { route: '/admin/community-profile', response: 'redirect' },
  { route: '/admin/community-profile/type/:profileType', response: 'redirect' },
  { route: '/admin/community-profile/new', response: 'redirect' },
  { route: '/admin/community-profile/insert', response: 'redirect' },
  { route: '/admin/community-profile/edit/:id', response: 'redirect' },
  { route: '/admin/community-profile/delete/:id', response: 'redirect' },
  {
    route: '/admin/community-profile/edit/update-status/:id',
    response: 'redirect',
  },
  {
    route: '/admin/community-profile/edit/update-orders/:profileType',
    response: 'redirect',
  },
  { route: '/admin/community-profile/:id/create-poll', response: 'redirect' },
  { route: '/admin/exclusive-sale', response: 'redirect' },
  { route: '/admin/exclusive-sale/new', response: 'redirect' },
  { route: '/admin/exclusive-sale/insert', response: 'redirect' },
  { route: '/admin/exclusive-sale/edit/:id', response: 'redirect' },
  { route: '/admin/exclusive-sale/update', response: 'redirect' },
  { route: '/admin/exclusive-sale/delete/:id', response: 'redirect' },
  { route: '/admin/exlusive-sale/upload-file', response: 'redirect' },
];
//Allowed mime types for product video
exports.allowedVideoType = ['video/mp4'];

// Allowed mime types for product images
exports.allowedMimeType = [
  'image/jpeg',
  'image/png',
  'image/gif',
  'image/bmp',
  'image/webp',
  'image/jpg',
  'image/heic',
];

// Allowed mime types for csv
exports.allowedCSVType = ['text/csv'];

// Allowed mime types for apk
exports.allowedAPKType = ['application/vnd.android.package-archive'];

// Allowed mime types for csv/xlsx
exports.allowedCSVAndExcelType = [
  'text/csv',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
];

// Allowed mime types for xlsx
exports.allowedExcelType =
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

exports.fileSizeLimit = 10485760;

// common functions
exports.restrict = (req, res, next) => {
  if (process.env.NODE_ENV === 'test') {
    return next();
  }
  exports.checkLogin(req, res, next);
};

exports.checkLogin = (req, res, next) => {
  // if not protecting we check for public pages and don't checkLogin
  if (req.session.needsSetup === true) {
    res.redirect('/admin/setup');
    return;
  }

  if (req.session.user) {
    next();
    return;
  }
  res.redirect('/admin/login');
};

exports.cleanHtml = (html) => {
  return sanitizeHtml(html);
};

exports.mongoSanitize = (param) => {
  if (param instanceof Object) {
    for (const key in param) {
      if (/^\$/.test(key)) {
        delete param[key];
      }
    }
  }
  return param;
};

exports.checkboxBool = (param) => {
  if (param && param === 'on') {
    return true;
  }
  return false;
};

// Middleware to check for admin access for certain route
exports.checkAccess = (req, res, next) => {
  if (process.env.NODE_ENV === 'test') {
    return next();
  }
  let routeList = [];
  switch (req.session.groupRole) {
    case 'marketing':
      routeList = restrictedRoutesMarketting;
      break;
    case 'production':
      routeList = restrictedRoutesProduction;
      break;
    case 'customer_service':
      routeList = restrictedRoutesCustomerService;
      break;
    case 'admin':
      routeList = restrictedRoutesAdmin;
      break;
  }
  const routeCheck = _.find(routeList, { route: req.route.path });

  // If the user is not an admin and route is restricted, show message and redirect to /admin
  if (req.session.isAdmin === false && routeCheck) {
    if (routeCheck.response === 'redirect') {
      req.session.message = 'Unauthorised. Please refer to administrator.';
      req.session.messageType = 'danger';
      res.redirect('/admin');
      return;
    }
    if (routeCheck.response === 'json') {
      res
        .status(400)
        .json({ message: 'Unauthorised. Please refer to administrator.' });
    }
  } else {
    next();
  }
};

exports.showCartCloseBtn = (page) => {
  let showCartCloseButton = true;
  if (page === 'checkout' || page === 'pay') {
    showCartCloseButton = false;
  }

  return showCartCloseButton;
};

// adds products to sitemap.xml
exports.addSitemapProducts = (req, res, cb) => {
  const db = req.app.db;

  const config = exports.getConfig();
  const hostname = config.baseUrl;

  db.products.find({ productPublished: 'true' }).toArray((err, products) => {
    const posts = [];
    if (err) {
      cb(null, posts);
    }
    async.eachSeries(
      products,
      (item, callback) => {
        const post = {};
        let url = item._id;
        if (item.productPermalink) {
          url = item.productPermalink;
        }
        post.url = hostname + '/' + url;
        post.changefreq = 'weekly';
        post.priority = 0.7;
        posts.push(post);
        callback(null, posts);
      },
      () => {
        cb(null, posts);
      },
    );
  });
};

exports.clearSessionValue = (session, sessionVar) => {
  let temp;
  if (session) {
    temp = session[sessionVar];
    session[sessionVar] = null;
  }
  return temp;
};

exports.updateTotalCartAmount = (req, res) => {
  const config = exports.getConfig();

  req.session.totalCartAmount = 0;

  _(req.session.cart).forEach((item) => {
    req.session.totalCartAmount =
      req.session.totalCartAmount + item.totalItemPrice;
  });

  // under the free shipping threshold
  if (req.session.totalCartAmount < config.freeShippingAmount) {
    req.session.totalCartAmount =
      req.session.totalCartAmount + parseInt(config.flatShipping);
    req.session.shippingCostApplied = true;
  } else {
    req.session.shippingCostApplied = false;
  }
};

exports.checkDirectorySync = (directory) => {
  try {
    fs.statSync(directory);
  } catch (e) {
    try {
      fs.mkdirSync(directory);
    } catch (err) {
      mkdirp.sync(directory); // error : directory & sub directories to be newly created
    }
  }
};

exports.getThemes = () => {
  return fs
    .readdirSync(path.join(__dirname, '../', 'views', 'themes'))
    .filter((file) =>
      fs
        .statSync(
          path.join(path.join(__dirname, '../', 'views', 'themes'), file),
        )
        .isDirectory(),
    );
};

exports.getImages = (dir, req, res, callback) => {
  const db = req.app.db;

  db.products.findOne({ _id: exports.getId(dir) }, (err, product) => {
    if (err) {
      console.error(colors.red('Error getting images', err));
    }
    if (product) {
      // loop files in /public/uploads/
      glob(
        'public/uploads/' + product.productPermalink + '/**',
        { nosort: true },
        (er, files) => {
          // sort array
          files.sort();

          // declare the array of objects
          const fileList = [];

          // loop these files
          for (let i = 0; i < files.length; i++) {
            // only want files
            if (fs.lstatSync(files[i]).isDirectory() === false) {
              // declare the file object and set its values
              const file = {
                id: i,
                path: files[i].substring(6),
              };
              if (product.productBackgroundImage === files[i].substring(6)) {
                file.productImage = true;
              }
              // push the file object into the array
              fileList.push(file);
            }
          }
          callback(fileList);
        },
      );
    } else {
      callback([]);
    }
  });
};

exports.getConfigFilename = () => {
  const filename = path.join(__dirname, '../config', 'settings-local.json');
  if (fs.existsSync(filename)) {
    return filename;
  }
  return path.join(__dirname, '../config', 'settings.json');
};

exports.getConfig = () => {
  const config = JSON.parse(
    fs.readFileSync(exports.getConfigFilename(), 'utf8'),
  );
  config.customCss =
    typeof config.customCss !== 'undefined'
      ? escape.decode(config.customCss)
      : null;
  config.footerHtml =
    typeof config.footerHtml !== 'undefined'
      ? escape.decode(config.footerHtml)
      : null;
  config.googleAnalytics =
    typeof config.googleAnalytics !== 'undefined'
      ? escape.decode(config.googleAnalytics)
      : null;

  // set the environment for files
  config.env = '.min';
  if (
    process.env.NODE_ENV === 'development' ||
    process.env.NODE_ENV === undefined
  ) {
    config.env = '';
  }

  // setup theme
  config.themeViews = '';
  if (typeof config.theme === 'undefined' || config.theme === '') {
    config.theme = 'Cloth'; // Default to Cloth theme
  }

  config.themeViews = '../views/themes/' + config.theme + '/';

  // if db set to mongodb override connection with MONGODB_CONNECTION_STRING env var
  if (process.env.NODE_ENV !== 'test') {
    config.databaseConnectionString =
      process.env.MONGODB_CONNECTION_STRING || config.databaseConnectionString;
  } else {
    config.databaseConnectionString = process.env.MONGODB_TESTING_CONNECTION;
  }

  return config;
};

exports.getPaymentConfig = () => {
  const siteConfig = this.getConfig();

  let config = [];
  if (
    fs.existsSync(
      path.join(__dirname, '../config/' + siteConfig.paymentGateway + '.json'),
    )
  ) {
    config = JSON.parse(
      fs.readFileSync(
        path.join(
          __dirname,
          '../config/' + siteConfig.paymentGateway + '.json',
        ),
        'utf8',
      ),
    );
  }

  return config;
};

exports.updateConfig = (fields) => {
  const settingsFile = exports.getConfig();

  _.forEach(fields, (value, key) => {
    settingsFile[key] = value;
    if (key === 'customCss_input') {
      settingsFile['customCss'] = escape.encode(uglifycss.processString(value));
    }
    if (key === 'footerHtml_input') {
      const footerHtml =
        typeof value !== 'undefined' || value === ''
          ? escape.encode(value)
          : '';
      settingsFile['footerHtml'] = footerHtml;
    }
    if (key === 'googleAnalytics_input') {
      const googleAnalytics =
        typeof value !== 'undefined' ? escape.encode(value) : '';
      settingsFile['googleAnalytics'] = googleAnalytics;
    }
  });

  // delete settings
  delete settingsFile['customCss_input'];
  delete settingsFile['footerHtml_input'];
  delete settingsFile['googleAnalytics_input'];

  if (fields['emailSecure'] === 'on') {
    settingsFile['emailSecure'] = true;
  } else {
    settingsFile['emailSecure'] = false;
  }

  if (!fields['menuEnabled']) {
    settingsFile['menuEnabled'] = false;
  } else {
    settingsFile['menuEnabled'] = true;
  }

  if (fields['emailPort']) {
    settingsFile['emailPort'] = parseInt(fields['emailPort']);
  }

  if (fields['flatShipping']) {
    settingsFile['flatShipping'] = parseInt(fields['flatShipping']);
  }

  if (fields['freeShippingAmount']) {
    settingsFile['freeShippingAmount'] = parseInt(fields['freeShippingAmount']);
  }

  if (fields['productsPerRow']) {
    settingsFile['productsPerRow'] = parseInt(fields['productsPerRow']);
  }

  if (fields['productsPerPage']) {
    settingsFile['productsPerPage'] = parseInt(fields['productsPerPage']);
  }

  // write file
  try {
    fs.writeFileSync(
      exports.getConfigFilename(),
      JSON.stringify(settingsFile, null, 4),
    );
    return true;
  } catch (exception) {
    return false;
  }
};

exports.getMenu = (db) => {
  return db.menu.findOne({});
};

// creates a new menu item
exports.newMenu = (req, res) => {
  const db = req.app.db;
  return exports
    .getMenu(db)
    .then((menu) => {
      // if no menu present
      if (!menu) {
        menu = {};
        menu.items = [];
      }
      const newNav = {
        title: req.body.navMenu,
        link: req.body.navLink,
        order: Object.keys(menu.items).length + 1,
      };

      menu.items.push(newNav);
      return db.menu
        .updateOne({}, { $set: { items: menu.items } }, { upsert: true })
        .then(() => {
          return true;
        });
    })
    .catch((err) => {
      console.log('Error creating new menu', err);
      return false;
    });
};

// delete a menu item
exports.deleteMenu = (req, res, menuIndex) => {
  const db = req.app.db;
  return exports
    .getMenu(db)
    .then((menu) => {
      // Remove menu item
      menu.items.splice(menuIndex, 1);
      return db.menu
        .updateOne({}, { $set: { items: menu.items } }, { upsert: true })
        .then(() => {
          return true;
        });
    })
    .catch(() => {
      return false;
    });
};

// updates and existing menu item
exports.updateMenu = (req, res) => {
  const db = req.app.db;
  return exports
    .getMenu(db)
    .then((menu) => {
      // find menu item and update it
      const menuIndex = _.findIndex(menu.items, ['title', req.body.navId]);
      menu.items[menuIndex].title = req.body.navMenu;
      menu.items[menuIndex].link = req.body.navLink;
      return db.menu
        .updateOne({}, { $set: { items: menu.items } }, { upsert: true })
        .then(() => {
          return true;
        });
    })
    .catch(() => {
      return false;
    });
};

exports.sortMenu = (menu) => {
  if (menu && menu.items) {
    menu.items = _.sortBy(menu.items, 'order');
    return menu;
  }
  return {};
};

// orders the menu
exports.orderMenu = (req, res) => {
  const db = req.app.db;
  return exports
    .getMenu(db)
    .then((menu) => {
      // update the order
      for (let i = 0; i < req.body.navId.length; i++) {
        _.find(menu.items, ['title', req.body.navId[i]]).order = i;
      }
      return db.menu
        .updateOne({}, { $set: { items: menu.items } }, { upsert: true })
        .then(() => {
          return true;
        });
    })
    .catch(() => {
      return false;
    });
};

exports.getEmailTemplate = (result) => {
  const config = this.getConfig();

  const template = fs.readFileSync(
    path.join(__dirname, '../public/email_template.html'),
    'utf8',
  );

  $ = cheerio.load(template);
  $('#brand').text(config.cartTitle);
  $('#paymentResult').text(result.message);
  if (result.paymentApproved === true) {
    $('#paymentResult').addClass('text-success');
  } else {
    $('#paymentResult').addClass('text-danger');
  }
  $('#paymentMessage').text(
    'Thanks for shopping with us. We hope you will shop with us again soon.',
  );
  $('#paymentDetails').html(result.paymentDetails);

  return $.html();
};

exports.sendEmail = (to, cc, subject, body, attachments) => {
  const config = this.getConfig();

  const emailSettings = {
    host: config.emailHost,
    port: config.emailPort,
    secure: config.emailSecure,
    auth: {
      user: config.emailUser,
      pass: config.emailPassword,
    },
  };

  // outlook needs this setting
  if (config.emailHost === 'smtp-mail.outlook.com') {
    emailSettings.tls = { ciphers: 'SSLv3' };
  }

  const transporter = nodemailer.createTransport(emailSettings);

  const mailOptions = {
    from: 'Forreal Technology<' + config.imapUser + '>', // sender address
    cc: cc,
    to: to, // list of receivers
    subject: subject, // Subject line
    text: body, // html body,
    attachments: attachments,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.error(colors.red(error));
    }
    return true;
  });
};

exports.sendReplyEmail = (to, subject, messageId, body) => {
  const config = this.getConfig();

  const emailSettings = {
    host: config.emailHost,
    port: config.emailPort,
    secure: config.emailSecure,
    auth: {
      user: config.emailUser,
      pass: config.emailPassword,
    },
  };

  // outlook needs this setting
  if (config.emailHost === 'smtp-mail.outlook.com') {
    emailSettings.tls = { ciphers: 'SSLv3' };
  }

  const transporter = nodemailer.createTransport(emailSettings);

  const mailOptions = {
    inReplyTo: messageId,
    references: [messageId],
    from: 'Forreal Technology<' + config.imapUser + '>', // sender address
    to: to,
    cc: process.env.CC_IMPORT_CSV_EMAIL,
    // inReplyTo: getlastCommentArrayInReplyTo,
    // references: getlastCommentArrayInReplyTo,
    // headers: {
    //     inReplyTo: messageId,
    //     replyTo: to,
    //     references: [messageId]
    //     // inReplyTo: getlastCommentArrayInReplyTo,
    //     // references: getlastCommentArrayInReplyTo,
    // },
    subject: subject,
    text: body,
    generateTextFromHTML: true,
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.error(colors.red(error));
    }
    return true;
  });
};

// gets the correct type of index ID
exports.getId = (id) => {
  if (id) {
    if (id.length !== 24) {
      return id;
    }
  }
  return ObjectId(id);
};

exports.getData = (req, page, query) => {
  const db = req.app.db;
  const config = exports.getConfig();
  const numberProducts = config.productsPerPage ? config.productsPerPage : 6;

  let skip = 0;
  if (page > 1) {
    skip = (page - 1) * numberProducts;
  }

  if (!query) {
    query = {};
  }

  query['productPublished'] = 'true';

  // Run our queries
  return Promise.all([
    db.products
      .find(query)
      .skip(skip)
      .limit(parseInt(numberProducts))
      .toArray(),
    db.products.count(query),
  ])
    .then((result) => {
      const returnData = { data: result[0], totalProducts: result[1] };
      return returnData;
    })
    .catch((err) => {
      throw new Error('Error retrieving products');
    });
};

exports.indexProducts = (app) => {
  // index all products in lunr on startup
  return new Promise((resolve, reject) => {
    app.db.products.find({}).toArray((err, productsList) => {
      if (err) {
        console.error(colors.red(err.stack));
        reject(err);
      }

      // setup lunr indexing
      const productsIndex = lunr(function () {
        this.field('productTitle', { boost: 10 });
        this.field('productTags', { boost: 5 });
        this.field('productDescription');

        const lunrIndex = this;

        // add to lunr index
        productsList.forEach((product) => {
          const doc = {
            productTitle: product.productTitle,
            productTags: product.productTags,
            productDescription: product.productDescription,
            id: product._id,
          };
          lunrIndex.add(doc);
        });
      });

      app.productsIndex = productsIndex;
      console.log(colors.cyan('- Product indexing complete'));
      resolve();
    });
  });
};

exports.indexCustomers = (app) => {
  // index all products in lunr on startup
  return new Promise((resolve, reject) => {
    app.db.customers.find({}).toArray((err, customerList) => {
      if (err) {
        console.error(colors.red(err.stack));
        reject(err);
      }

      // setup lunr indexing
      const customersIndex = lunr(function () {
        this.field('email', { boost: 10 });
        this.field('name', { boost: 5 });
        this.field('phone');

        const lunrIndex = this;

        // add to lunr index
        customerList.forEach((customer) => {
          const doc = {
            email: customer.email,
            name: `${customer.firstName} ${customer.lastName}`,
            phone: customer.phone,
            id: customer._id,
          };
          lunrIndex.add(doc);
        });
      });

      app.customersIndex = customersIndex;
      console.log(colors.cyan('- Customer indexing complete'));
      resolve();
    });
  });
};

exports.indexOrders = (app, cb) => {
  // index all orders in lunr on startup
  return new Promise((resolve, reject) => {
    app.db.orders.find({}).toArray((err, ordersList) => {
      if (err) {
        console.error(colors.red('Error setting up products index: ' + err));
        reject(err);
      }

      // setup lunr indexing
      const ordersIndex = lunr(function () {
        this.field('orderEmail', { boost: 10 });
        this.field('orderLastname', { boost: 5 });
        this.field('orderPostcode');

        const lunrIndex = this;

        // add to lunr index
        ordersList.forEach((order) => {
          const doc = {
            orderLastname: order.orderLastname,
            orderEmail: order.orderEmail,
            orderPostcode: order.orderPostcode,
            id: order._id,
          };
          lunrIndex.add(doc);
        });
      });

      app.ordersIndex = ordersIndex;
      console.log(colors.cyan('- Order indexing complete'));
      resolve();
    });
  });
};

// start indexing products and orders
exports.runIndexing = (app) => {
  console.info(colors.yellow('Setting up indexes..'));

  return Promise.all([
    exports.indexProducts(app),
    exports.indexOrders(app),
    exports.indexCustomers(app),
  ]).catch((err) => {
    process.exit(2);
  });
};

exports.dropTestData = (db) => {
  Promise.all([db.products.drop(), db.users.drop(), db.customers.drop()])
    .then((err) => {
      return Promise.resolve();
    })
    .catch((err) => {
      console.log('Error dropping test data', err);
    });
};

exports.sampleData = (app) => {
  const db = app.db;

  db.products.count().then((products) => {
    if (products !== 0) {
      return Promise.resolve();
    }

    console.log('Inserting sample data');
    const testData = fs.readFileSync('./bin/testdata.json', 'utf-8');
    const jsonData = JSON.parse(testData);

    // Add sample datanpm install --global yarn
    return Promise.all([
      db.products.insertMany(fixProductDates(jsonData.products)),
      db.menu.insertOne(jsonData.menu),
    ]);
  });
};

exports.testData = async (app) => {
  const db = app.db;
  const testData = fs.readFileSync('./bin/testdata.json', 'utf-8');
  const jsonData = JSON.parse(testData);

  await Promise.all([
    db.users.remove({}, {}),
    db.customers.remove({}, {}),
    db.products.remove({}, {}),
    db.menu.remove({}, {}),
  ]);

  await Promise.all([
    db.users.insertMany(jsonData.users),
    db.customers.insertMany(jsonData.customers),
    db.products.insertMany(fixProductDates(jsonData.products)),
    db.menu.insertOne(jsonData.menu),
  ]);
};

exports.convertTimezone = (date, ianatz) => {
  // suppose the date is 12:00 UTC
  const invdate = new Date(
    date.toLocaleString('en-US', {
      timeZone: ianatz,
    }),
  );

  // then invdate will be 07:00 in Toronto
  // and the diff is 5 hours
  const diff = date.getTime() - invdate.getTime();

  // so 12:00 in Toronto is 17:00 UTC
  return new Date(date.getTime() + diff);
};

exports.uploadFileToS3 = async (fileLocation, folderName, isPoll) => {
  let fileData;
  //upload
  if (isPoll) {
    fileData = await sharp(fileLocation).resize().toFormat('jpeg').toBuffer();
  } else {
    fileData = fs.readFileSync(fileLocation);
  }
  const params = {
    Bucket: Config.S3.bucket, //Enter the bucket name that you created
    Key: `${folderName}/${Date.now()}.jpg`, //filename to use on the cloud
    Body: fileData,
    ContentType: 'image/png',
    CacheControl: 'max-age=172800',
    ACL: 'public-read', //To make file publicly accessible through URL
  };
  const s3Upload = await s3
    .upload(params, function (error, data) {
      if (error) {
        console.log(error);
        throw error;
      }
      console.log(`File was Uploaded Successfully. ${data.Location}`);
    })
    .promise();
  return s3Upload.Location;
};

const splitS3Path = (path) => {
  const url = new URL(path);
  const bucket = Config.S3.bucket;
  const key = url.pathname.substr(1);
  return { bucket, key };
};

exports.deleteS3Object = async (newData, oldPath) => {
  if (newData && oldPath.length) {
    const { bucket, key } = splitS3Path(oldPath);
    await s3
      .deleteObjects({
        Bucket: bucket,
        Delete: { Objects: [{ Key: key }] },
      })
      .promise();
  }
};

exports.uploadVideoToS3 = async (fileLocation, folderName, isPoll) => {
  let fileData;
  //upload
  if (isPoll) {
    fileData = await sharp(fileLocation).resize().toFormat('jpeg').toBuffer();
  } else {
    fileData = fs.readFileSync(fileLocation);
  }
  const params = {
    Bucket: Config.S3.bucket, //Enter the bucket name that you created
    Key: `${folderName}/${Date.now()}.mp4`, //filename to use on the cloud
    Body: fileData,
    ContentType: 'video',
    CacheControl: 'max-age=172800',
    ACL: 'public-read', //To make file publicly accessible through URL
  };
  console.log(folderName, fileLocation);
  const s3Upload = await s3
    .upload(params, function (error, data) {
      if (error) {
        console.log(error);
        throw error;
      }
      console.log(`File was Uploaded Successfully. ${data.Location}`);
    })
    .promise();
  return s3Upload.Location;
};

exports.uploadUdidRequestFileToS3 = async (fileLocation, fileName) => {
  //upload
  const fileData = fs.readFileSync(fileLocation);
  const params = {
    Bucket: Config.S3UdidRequest.bucket, //Enter the bucket name that you created
    Key: `${Config.S3UdidRequest.folder}/${fileName}`, //filename to use on the cloud
    Body: fileData,
    ContentType: 'application/zip',
    CacheControl: 'max-age=172800',
    ACL: 'public-read', //To make file publicly accessible through URL
  };

  const s3Upload = await s3
    .upload(params, function (error, data) {
      if (error) {
        throw error;
      }
      console.log(`File was Uploaded Successfully. ${data.Location}`);
    })
    .promise();
  return s3Upload.Location;
};

// Adds current date to product added date when smashing into DB
function fixProductDates(products) {
  let index = 0;
  products.forEach((product) => {
    products[index].productAddedDate = new Date();
    index++;
  });
  return products;
}

// Import NFT product from file csv
exports.importNftProducts = (app) => {
  const db = app.db;
  db.nftProducts.count().then((count) => {
    if (!count) {
      csvToJson()
        .fromFile('bin/NyansumNFTDatasheet-NFTLongList.csv')
        .then((data) => {
          const randomNumber = (min, max) => {
            return Math.floor(Math.random() * (max - min + 1)) + min;
          };

          const nftProducts = data.map((product) => {
            const _product = {
              ...product,
              characterNumber: Number(product.characterNumber),
              hp: randomNumber(Number(product.hpMin), Number(product.hpMax)),
              lv: randomNumber(Number(product.lvMin), Number(product.lvMax)),
              attack1: randomNumber(
                Number(product.attack1Min),
                Number(product.attack1Max),
              ),
              attack2: randomNumber(
                Number(product.attack2Min),
                Number(product.attack2Max),
              ),
              hpMax: Number(product.hpMax),
              lvMax: Number(product.lvMax),
              attack1Max: Number(product.attack1Max),
              attack2Max: Number(product.attack2Max),
            };

            delete _product.hpMin;
            delete _product.lvMin;
            delete _product.attack1Min;
            delete _product.attack2Min;

            return _product;
          });

          db.nftProducts.insertMany(nftProducts);
        });
    }
  });
};

exports.decryptDataFromClient = (encryptedString, ivString) => {
  const iv = Buffer.from(ivString, 'hex');
  const key = Buffer.from(Config.cryptoHexKey, 'hex');
  const encryptedText = Buffer.from(encryptedString, 'base64');
  const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
  let decrypted = decipher.update(encryptedText, 'hex', 'utf8');
  decrypted += decipher.final('utf8');
  return decrypted;
};

exports.encryptDataFromClient = (text) => {
  const iv = crypto.randomBytes(16);
  const key = Buffer.from(Config.cryptoHexKey, 'hex');
  const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
  // Updating text
  let encrypted = cipher.update(text);
  // Using concatenation
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  console.log(iv.toString('hex'));
  console.log(encrypted.toString('base64'));
  return {
    iv: iv.toString('hex'),
    encryptedMessage: encrypted.toString('base64'),
  };
};

/**
 * @description         Create an timer with collection and this field what contain timer
 * @param {*}           app         The express() function is a top-level function exported by the express module.
 * @param {Collection}  collection  This collection you wants to schedule an event to fire
 * @param {*}           field       Field is an field in the collection where contain timer
 * @param {Object}      updateField Fields need to update when time out
 * @param {String}      scheduleName Name for this job
 * ---
 * @note                Date-time to be specified in ISO 8601 format (<iso-date-string>)
 */
const Schedule = async (
  app,
  collection,
  field,
  updateField,
  scheduleName,
  query,
) => {
  //Check have exist and update cronjob
  if (schedule.scheduledJobs[scheduleName]) {
    schedule.scheduledJobs[scheduleName].cancel();
  }

  const db = app.db;
  let logDB;
  let queryDB = { [field]: { $gt: new Date() } };
  if (query) {
    queryDB = Object.assign(queryDB, query);
  }
  const [fireEvent] = await db[collection]
    .find(queryDB)
    .sort({ [field]: 1 })
    .limit(1)
    .toArray();
  logDB = `Find event \n `;
  if (fireEvent) {
    console.log(`upcoming event start: ${fireEvent[field]}`);
    logDB += `upcoming event start: ${fireEvent[field]} \n `;
    const date = new Date(fireEvent[field]).getDate();
    const month = new Date(fireEvent[field]).getMonth();
    const year = new Date(fireEvent[field]).getFullYear();
    const hour = new Date(fireEvent[field]).getHours();
    const minute = new Date(fireEvent[field]).getMinutes();
    const second = new Date(fireEvent[field]).getSeconds();
    const rule = new schedule.RecurrenceRule();
    rule.date = date;
    rule.month = month;
    rule.year = year;
    rule.hour = hour;
    rule.minute = minute;
    rule.second = second;
    return schedule.scheduleJob(scheduleName, rule, async function () {
      logDB += `Found event \n `;
      db[collection].updateMany(
        { [field]: { $eq: fireEvent[field] } },
        { $set: updateField },
        {},
      );
      Schedule(app, collection, field, updateField, scheduleName);
    });
  } else if (!fireEvent && !schedule.scheduledJobs[scheduleName]) {
    logDB += `There are no upcoming events. \n `;
    console.log('There are no upcoming events.');
  } else if (!fireEvent && schedule.scheduledJobs[scheduleName]) {
    logDB += `There are no upcoming events. \n `;
    console.log('There are no upcoming events.');
    schedule.scheduledJobs[scheduleName].cancel();
  }
};

exports.saveToActionLog = async (db, userName, action, data) => {
  const log = {
    userName: userName,
    action: action,
    data: data,
    createAt: new Date(),
  };

  return await Promise.all([db.actionLog.insert(log)]);
};

exports.getCreepyCutiesProductsInfo = async (productVariantSku) => {
  const rawData = await fs.readFileSync(
    './bin/creepyCutiesProducts.json',
    'utf-8',
  );
  const creepyCutiesProducts = JSON.parse(rawData);
  return creepyCutiesProducts.find(
    (v) => v.productVariantSku === productVariantSku,
  );
};

exports.isInt = (value) => {
  const x = parseFloat(value);
  return !isNaN(value) && (x | 0) === x;
};

exports.dbInitCollection = (db) => {
  db.users = db.collection('users');
  db.products = db.collection('products');
  db.orders = db.collection('orders');
  db.pages = db.collection('pages');
  db.menu = db.collection('menu');
  db.customers = db.collection('customers');
  db.registrations = db.collection('registrations');
  db.variants = db.collection('variants');
  db.qrcodes = db.collection('qrcodes');
  db.forrealNfts = db.collection('forrealNfts');
  db.tteNfts = db.collection('tteNfts');
  db.stores = db.collection('stores');
  db.sports = db.collection('sports');
  db.artists = db.collection('artists');
  db.scheduleNotifications = db.collection('scheduleNotifications');
  db.merkleroots = db.collection('merkleroots');
  db.sendEmailToManufacture = db.collection('sendEmailToManufacture');
  db.campaigns = db.collection('campaigns');
  db.campaignParticipates = db.collection('campaignParticipates');
  db.digitalContents = db.collection('digitalContents');
  db.stockAnalytics = db.collection('stockAnalytics');
  db.stockDetailAnalytics = db.collection('stockDetailAnalytics');
  db.userSettings = db.collection('userSettings');
  db.tradegeckoWebhookLog = db.collection('tradegeckoWebhookLog');
  db.upcommingProducts = db.collection('upcommingProducts');
  db.nftProducts = db.collection('nftProducts');
  db.appReleased = db.collection('appReleased');
  db.digitalContentGroupType = db.collection('digitalContentGroupType');
  db.actionLog = db.collection('actionLog');
  db.profiles = db.collection('profiles');
  db.polls = db.collection('polls');
  db.userPolls = db.collection('userPolls');
  db.analyticsCreepyCuties = db.collection('analyticsCreepyCuties');
  db.exclusiveSale = db.collection('exclusiveSale');
  db.quizs = db.collection('quizs');
  db.quizQuestions = db.collection('quizQuestions');
  db.quizAnswers = db.collection('quizAnswers');
  db.gameAndShiftCode = db.collection('gameAndShiftCode');
  db.voucherCodes = db.collection('voucherCodes');
  db.badges = db.collection('badges');
  db.userBadges = db.collection('userBadges');
  db.web3Auth = db.collection('web3Auth');
};

exports.decodeBase64 = (string) => {
  const buff = Buffer.from(string, 'base64');
  const customer_url = buff.toString('ascii');
  const shopifyId = customer_url.split('/')[4];
  return shopifyId.toString('base64');
};

exports.encodeBase64 = (string) => {
  const buffer = Buffer.from(String(string));
  return buffer.toString('base64');
};

module.exports.Schedule = Schedule;

const makeId = (length) => {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;

  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
};

module.exports.makeId = makeId;

const randomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

module.exports.randomNumber = randomNumber;
