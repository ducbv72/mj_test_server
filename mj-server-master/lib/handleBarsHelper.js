const numeral = require('numeral');
const handlebars = require('express-handlebars');
const moment = require('moment');
const url = require('url');
const { clone, isArray } = require('lodash');
exports.handlebarsHelper = (config) => {
  return handlebars.create({
    helpers: {
      perRowClass: function (numProducts) {
        if (parseInt(numProducts) === 1) {
          return 'col-md-12 col-xl-12 col m12 xl12 product-item';
        }
        if (parseInt(numProducts) === 2) {
          return 'col-md-6 col-xl-6 col m6 xl6 product-item';
        }
        if (parseInt(numProducts) === 3) {
          return 'col-md-4 col-xl-4 col m4 xl4 product-item';
        }
        if (parseInt(numProducts) === 4) {
          return 'col-md-3 col-xl-3 col m3 xl3 product-item';
        }

        return 'col-md-6 col-xl-6 col m6 xl6 product-item';
      },
      menuMatch: function (title, search) {
        if (!title || !search) {
          return '';
        }
        if (title.toLowerCase().startsWith(search.toLowerCase())) {
          return 'class="navActive"';
        }
        return '';
      },
      getTheme: function (view) {
        return `themes/${config.theme}/${view}`;
      },
      formatAmount: function (amt) {
        if (amt) {
          return numeral(amt).format('0.00');
        }
        return '0.00';
      },
      amountNoDecimal: function (amt) {
        if (amt) {
          return handlebars.helpers.formatAmount(amt).replace('.', '');
        }
        return handlebars.helpers.formatAmount(amt);
      },
      getStatusColor: function (status) {
        switch (status) {
          case 'Paid':
            return 'success';
          case 'Approved':
            return 'success';
          case 'Approved - Processing':
            return 'success';
          case 'Failed':
            return 'danger';
          case 'Completed':
            return 'success';
          case 'Shipped':
            return 'success';
          case 'Pending':
            return 'warning';
          default:
            return 'danger';
        }
      },
      checkProductOptions: function (opts) {
        if (opts) {
          return 'true';
        }
        return 'false';
      },
      currencySymbol: function (value) {
        if (typeof value === 'undefined' || value === '') {
          return '$';
        }
        return value;
      },
      objectLength: function (obj) {
        if (obj) {
          return Object.keys(obj).length;
        }
        return 0;
      },
      checkedState: function (state) {
        if (state === 'true' || state === true) {
          return 'checked';
        }
        return '';
      },
      selectState: function (state, value) {
        if (state && value) {
          if (state.toString() === value.toString()) {
            return 'selected';
          }
        } else {
          if (state === value) {
            return 'selected';
          }
        }
        return '';
      },
      isNull: function (value, options) {
        if (typeof value === 'undefined' || value === '') {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      toLower: function (value) {
        if (value) {
          return value.toLowerCase();
        }
        return null;
      },
      formatDate: function (date, format) {
        if (date) {
          return moment(date).tz('Asia/Singapore').format(format);
        }

        return moment().tz('Asia/Singapore').format(format);
      },
      ifCond: function (v1, operator, v2, options) {
        switch (operator) {
          case '==':
            return v1 === v2 ? options.fn(this) : options.inverse(this);
          case '!=':
            return v1 !== v2 ? options.fn(this) : options.inverse(this);
          case '===':
            return v1 === v2 ? options.fn(this) : options.inverse(this);
          case '<':
            return v1 < v2 ? options.fn(this) : options.inverse(this);
          case '<=':
            return v1 <= v2 ? options.fn(this) : options.inverse(this);
          case '>':
            return v1 > v2 ? options.fn(this) : options.inverse(this);
          case '>=':
            return v1 >= v2 ? options.fn(this) : options.inverse(this);
          case '&&':
            return v1 && v2 ? options.fn(this) : options.inverse(this);
          case '||':
            return v1 || v2 ? options.fn(this) : options.inverse(this);
          case 'in':
            // eslint-disable-next-line no-case-declarations
            const arrayValue = v2 ? v2.toString().split('|') : [];
            return arrayValue.includes(v1)
              ? options.fn(this)
              : options.inverse(this);
          default:
            return options.inverse(this);
        }
      },
      isAnAdmin: function (value, options) {
        if (value === 'true' || value === true) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      isAnSuper: function (value, options) {
        if (value === 'true' || value === true) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      valueFromMap: function (key, values) {
        if (!values) return '0';
        return values[key];
      },
      checkedMenuActive: function (menu, activeMenu) {
        if (menu === activeMenu) {
          return ' active';
        }
        return '';
      },
      AddIntvalue: function (int1, int2) {
        return parseInt(int1) + parseInt(int2);
      },
      Divide: function (float1, float2) {
        if (parseFloat(float2) == 0) return 0;
        return (parseFloat(float1) / parseFloat(float2)).toFixed(2);
      },
      ifArrayHasData: function (array, options) {
        if (array.length > 0) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      getImageFromProduct: function (products, options) {
        if (products instanceof Array) {
          if (products.length > 0) {
            return products[0].productImageUrl;
          } else {
            return '/images/default-image.png';
          }
        } else {
          if (products) {
            return products.productImageUrl;
          } else {
            return '/images/default-image.png';
          }
        }
      },
      getProductId: function (products, options) {
        if (products instanceof Array) {
          if (products.length > 0) {
            return products[0]._id;
          } else {
            return '';
          }
        } else {
          if (products) {
            return products._id;
          } else {
            return '';
          }
        }
      },
      fixFormatCurrency: function (floatNumber) {
        return floatNumber.toFixed(2);
      },
      ifContainProductTag: function (array, value) {
        const trimmedArray = [];
        array.forEach(function (val, i) {
          trimmedArray.push(val.trim());
        });
        return trimmedArray.includes(value);
      },
      checkStringIsDateTimeAndNotEmpty: function (string) {
        if (string && !isNaN(Date.parse(string))) {
          return true;
        }
        return false;
      },
      parseDateTime: function (string) {
        return Date.parse(string);
      },
      convertStringToBool: function (string) {
        if (string != undefined && string.toLowerCase() == 'true') {
          return true;
        }
        return false;
      },
      isRequired: function (param) {
        if (param && (param === 'on' || param === true)) {
          return 'required';
        } else {
          return '';
        }
      },
      mappingKeyAndReturnValue(object, keyName, inputType) {
        // eslint-disable-next-line no-prototype-builtins
        if (object.hasOwnProperty(keyName)) {
          if (inputType !== 'checkbox') {
            console.log(object[keyName]);
            return object[keyName];
          } else {
            if (
              object[keyName] &&
              (object[keyName] === 'on' || object[keyName] === true)
            ) {
              return 'checked';
            } else {
              return '';
            }
          }
        } else {
          return '';
        }
      },
      showPaginationAtBottom(currentPage, totalPage, limitPageShow, url) {
        if (totalPage < 1) return '';
        const previousPage =
          parseInt(currentPage) > 0 ? parseInt(currentPage) - 1 : 0;
        const nextPage =
          parseInt(currentPage) < parseInt(totalPage)
            ? parseInt(currentPage) + 1
            : parseInt(totalPage);
        const CurrentPageToShow = parseInt((currentPage - 1) / limitPageShow);
        const lastPageToShow = parseInt(totalPage / limitPageShow);
        const urlPage = new URL(url);
        //back button
        urlPage.searchParams.append('page', previousPage);
        const backButton =
          parseInt(currentPage) === 1
            ? `<div class="btn-group"><button class="button-navigation-left button-disable" ><i class="fa fa-caret-left navigation-text-selected"></i></button>`
            : `<div class="btn-group"> <a href="${urlPage.toString()}"> <button class="button-navigation-left"><i class="fa fa-caret-left navigation-text-selected"></i></button></a>`;

        //middle button
        let pageMidButton = '';
        const toShowpage =
          CurrentPageToShow === lastPageToShow
            ? totalPage % limitPageShow
            : limitPageShow;
        if (CurrentPageToShow > 0) {
          urlPage.searchParams.set('page', 1);
          pageMidButton += `<a href="${urlPage.toString()}"><button class="button-navigation">${1}</button></a>`;
          pageMidButton += '...';
        }

        for (let i = 1; i <= toShowpage; i++) {
          const pageText = CurrentPageToShow * parseInt(limitPageShow) + i;
          urlPage.searchParams.set('page', pageText);
          const className =
            parseInt(pageText) === parseInt(currentPage)
              ? 'button-navigation-selected'
              : 'button-navigation';
          pageMidButton += `<a href="${urlPage.toString()}"><button class="${className}">${pageText}</button></a>`;
        }

        if (lastPageToShow !== CurrentPageToShow && lastPageToShow > 1) {
          urlPage.searchParams.set('page', totalPage);
          pageMidButton += '...';
          pageMidButton += `<a href="${urlPage.toString()}"><button class="button-navigation">${totalPage}</button></a>`;
        }

        //next button
        urlPage.searchParams.set('page', nextPage);
        const nextButton =
          parseInt(currentPage) === parseInt(totalPage)
            ? `<button class="button-navigation-right button-disable"><i class="fa fa-caret-right navigation-text-selected"></i></button>`
            : `<a href="${urlPage.toString()}"> <button class="button-navigation-right"><i class="fa fa-caret-right navigation-text-selected"></i></button></a></div>`;
        return backButton + pageMidButton + nextButton;
      },
      json: function (json) {
        return JSON.stringify(json);
      },
      arrayToString: function (array) {
        if (isArray(array)) {
          return array.join(',');
        } else {
          return '';
        }
      },
      ifCompare: function (operand_1, operator, operand_2, options) {
        const operators = {
            '==': function (l, r) {
              return l == r;
            },
            '!=': function (l, r) {
              return l != r;
            },
            '>': function (l, r) {
              return Number(l) > Number(r);
            },
            '||': function (l, r) {
              return l || r;
            },
            '&&': function (l, r) {
              return l && r;
            },
            '%': function (l, r) {
              return l % r === 0;
            },
          },
          result = operators[operator](operand_1, operand_2);
        if (result) return options.fn(this);
        else return options.inverse(this);
      },
      showPaginationExclusiveSaleBottom: function (currentPage, totalPage) {
        let htmlRaw = `<div class='col-sm-12 exclusive-sale-pagination'><nav><ul class='pagination'>`;
        for (let i = 1; i <= totalPage; i++) {
          // if (i > 2) {
          //   htmlRaw += `<li class='exlusive-sale-pagination-three-dots'><a class='page-link'>...</a></li>`;
          //   htmlRaw += `<li class='exlusive-sale-pagination-page-num'><a class='page-link' href='/admin/exclusive-sale?page=${totalPage}'>${totalPage}</a></li>`;
          //   break;
          // }
          if (i == currentPage) {
            htmlRaw += `<li class='exlusive-sale-pagination-page-num-active'><a class='page-link' href='/admin/exclusive-sale?page=${i}'>${i}</a></li>`;
          } else {
            htmlRaw += `<li class='exlusive-sale-pagination-page-num'><a class='page-link' href='/admin/exclusive-sale?page=${i}'>${i}</a></li>`;
          }
        }
        if (currentPage < totalPage) {
          htmlRaw += `<li class='exlusive-sale-pagination-page-next'><a class='page-link' href='/admin/exclusive-sale?page=${
            currentPage + 1
          }'>Next</a></li>`;
        }
        htmlRaw += `</ul></nav></div>`;
        return htmlRaw;
      },
      selectedPageExclusiveSaleTop: function (currentPage, totalPage) {
        let htmlRaw =
          '<select class="select-page-exclusive-sale" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">';
        for (let i = 1; i <= totalPage; i++) {
          if (i == currentPage) {
            htmlRaw += `<option value='/admin/exclusive-sale?page=${i}' selected>Page ${i}</option>`;
          } else {
            htmlRaw += `<option value='/admin/exclusive-sale?page=${i}'>Page ${i}</option>`;
          }
        }
        htmlRaw += `</select>`;
        return htmlRaw;
      },
      ifAndCond: function (val1, val2, options) {
        if (val1 & val2) {
          return options.fn(this);
        } else {
          return options.inverse(this);
        }
      },
      ifOrCond: function (val1, val2, options) {
        if (val1 || val2) {
          return options.fn(this);
        } else {
          return options.inverse(this);
        }
      },
    },
  });
};
