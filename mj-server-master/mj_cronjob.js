const MongoClient = require('mongodb').MongoClient;
const mongodbUri = require('mongodb-uri');
const cron = require('node-cron');
const common = require('./lib/common');
const sendEmailCrondjob = require('./services/sendemail-cronjob');
const DeployCronjob = require('./services/deploy-cronjob');
const UpdateEncryptedStringCountCronjob = require('./services/encryptedStringCount-cronjob');
const retryMintForrealNFTCronjob = require('./services/retryMintForrealNFTCronjob');
const retryMintTteNFTCronjob = require('./services/retryMintTteNFTCronjob');
const express = require('express');
const notifier = require('mail-notifier');
const stream = require('stream');
const fs = require('fs');
const csv = require('csvtojson');
const sleep = require('system-sleep');
const colors = require('colors');
const { adminFirebase } = require('./config/firebase');

const app = express();

// get config
const config = common.getConfig();

const option = {
  socketTimeoutMS: 9000000,
  keepAlive: true,
  reconnectTries: 30000,
};
const sgTimezone = 'Asia/Singapore';

MongoClient.connect(config.databaseConnectionString, option, (err, client) => {
  // On connection error we display then exit
  if (err) {
    console.log(colors.red('Error connecting to MongoDB: ' + err));
    process.exit(2);
  }

  // select DB
  const dbUriObj = mongodbUri.parse(config.databaseConnectionString);
  let db;
  if (process.env.ENABLE_CRONJOB === 'true') {
    db = client.db(dbUriObj.database);
    // setup the collections
    common.dbInitCollection(db);

    // add db to app for routes
    app.dbClient = client;
    app.db = db;
    app.config = config;
    app.port = app.get('8000');
    if (process.env.ENABLE_READ_EMAIL === 'true') {
      // listen email
      console.log('starting imap');
      const imap = {
        user: config.imapUser,
        password: config.imapPassword,
        host: config.imapHost,
        port: 993, // imap port
        tls: true, // use secure connection
        markSeen: true,
        tlsOptions: { rejectUnauthorized: false },
      };

      const n = notifier(imap);
      n.on('end', () => {
        // session closed
        console.log('session closed');
        n.start();
      })
        .on('mail', (mail) => {
          const emailAddress = mail.from;
          let correctEmail = false;
          console.log(emailAddress);
          console.log(config.emailManufacture);
          emailAddress.forEach((item) => {
            if (
              item.address.toLowerCase() ===
              config.emailManufacture.toLowerCase()
            ) {
              correctEmail = true;
            }
          });
          if (correctEmail) {
            console.log('start import: ' + new Date().toISOString());
            if (mail.attachments) {
              console.log('have attached');
              const arrPromise = [];
              let i = 0;
              mail.attachments.forEach((attachment) => {
                const singlePromise = new Promise(async (resolve, reject) => {
                  const index = i;
                  try {
                    const date = new Date();
                    const bufferStream = new stream.Transform();
                    bufferStream.push(attachment.content);
                    let filePath =
                      '/tmp/' +
                      date.getTime().toString() +
                      '_' +
                      attachment.generatedFileName;
                    filePath = filePath.replace(/ /g, '_');
                    const output = fs.createWriteStream(filePath);
                    bufferStream.pipe(output);
                    sleep(2);
                    console.log(filePath);
                    const arrayString = await csv().fromFile(filePath);
                    const arrayUpdate = [];
                    for (let i = 0; i < arrayString.length; i++) {
                      const item = arrayString[i];
                      arrayUpdate.push({
                        updateOne: {
                          filter: {
                            $and: [
                              { encrytedMessage: item.encryptedMessage },
                              {
                                $or: [
                                  { udid: '' },
                                  { udid: null },
                                  { udid: { $exists: false } },
                                ],
                              },
                            ],
                          },
                          update: { $set: { udid: item.udid } },
                        },
                      });
                    }
                    const [result] = await Promise.all([
                      db.qrcodes.bulkWrite(arrayUpdate),
                    ]);
                  } catch (e) {
                    console.log(e);
                  }
                  resolve(index);
                });
                arrPromise.push(singlePromise);
                i++;
              });

              Promise.all(arrPromise.map((p) => p.catch((e) => e)))
                .then((results) => {
                  console.log(results);
                  console.log(mail.messageId);
                  // send email success to admin
                  const body = 'File has been imported';
                  common.sendReplyEmail(
                    mail.from,
                    mail.subject,
                    mail.messageId,
                    body,
                  );
                  console.log('end import: ' + new Date().toISOString());
                }) // 1,Error: 2,3
                .catch((e) => console.log(e));
            }
          }
        })
        .start();
    }
    console.log('staring job check registration');
    cron.schedule('*/20 * * * *', () => {
      DeployCronjob.check_deploy_registration(app);
      console.log('check registration');
    });

    console.log('staring job send emails');

    cron.schedule('*/10 * * * *', () => {
      if (process.env.ENABLE_CRONJOB_SEND_EMAIL_V2 === 'true') {
        sendEmailCrondjob.send_email_export_v2(app);
      } else {
        sendEmailCrondjob.send_email_export(app);
      }
      console.log('send emails');
    });

    cron.schedule(
      '*/5 * * * *',
      async () => {
        console.log(`Running a job every 5 mins:`);
        console.log('- Retry to mint Forreal NFTs with error status');

        await retryMintForrealNFTCronjob(app);
      },
      {
        timezone: sgTimezone,
      },
    );

    cron.schedule(
      '*/15 * * * *',
      async () => {
        console.log(`Running a job every 15 mins:`);
        console.log('- Mint TTE NFTs');

        await retryMintTteNFTCronjob(app);
      },
      {
        timezone: sgTimezone,
      },
    );

    console.log('staring job updated encrypted string count');
    cron.schedule('0 * * * *', () => {
      UpdateEncryptedStringCountCronjob.update_encrypted_string_count_export(
        app,
      );
      console.log('updated encrypted string count');
    });
  } else {
    console.log('Job not enabled');
  }
});

module.exports = app;
