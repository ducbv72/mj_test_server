const listStores = [
  {
    _id: '1',
    name: 'The World of Popiki',
    handle: 'world-of-popiki-1',
    image: 'image',
    isFeatureCollection: true,
    image_bigger: 'image_bigger',
    order: 3,
  },
  {
    _id: '2',
    name: 'In Stock',
    handle: 'mj-vault',
    image: 'image',
    isFeatureCollection: true,
    image_bigger: 'image_bigger',
    order: 4,
    isHideStore: true,
  },
];

const shopifyProduct = (hasNextPage = false) => ({
  collectionByHandle: {
    products: {
      edges: [
        {
          cursor: 'cursor',
          node: {
            createdAt: '2021-12-01T03:58:17Z',
            description: 'description',
            descriptionHtml: 'descriptionHtml',
            handle: 'f1-2021-charles-leclerc-premium-edition',
            vendor: 'F1 and YARMS',
            status: 'ACTIVE',
            artistName: 'artistName',
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzE=',
            title: 'F1 2021: Charles Leclerc  (Premium Edition)',
            tags: [
              'Artist_Danil Yad',
              'F12021',
              'F1Collector',
              'Mighty AllStars',
              'New Arrivals',
              'NTWRK_MARKETPLACE',
              'premium-edition',
            ],
            onlineStoreUrl: 'onlineStoreUrl',
            images: {
              edges: [
                {
                  node: {
                    originalSrc: 'originalSrc',
                    src: 'src',
                  },
                },
                {
                  node: {
                    originalSrc: 'originalSrc',
                    transformedSrc: 'transformedSrc',
                  },
                },
              ],
            },
            productType: 'Collectible - Art Figure',
            options: [
              {
                id: 'gid://shopify/ProductOption/1',
                name: 'Size',
                values: ['Default'],
              },
              {
                id: 'gid://shopify/ProductOption/2',
                name: 'Color',
                values: ['Default'],
              },
            ],
            priceRange: {
              maxVariantPrice: {
                amount: '120000.0',
                currencyCode: 'USD',
              },
              minVariantPrice: {
                amount: '120000.0',
                currencyCode: 'USD',
              },
            },
            variants: {
              edges: [
                {
                  node: {
                    compareAtPrice: null,
                    id: 'gid://shopify/ProductVariant/39619049685126',
                    image: null,
                    price: '1200000.00',
                    sku: 'MAS-SKU0',
                    selectedOptions: [
                      {
                        name: 'Size',
                        value: 'Default',
                      },
                      {
                        name: 'Color',
                        value: 'Default',
                      },
                    ],
                    title: 'Default / Default',
                    weight: 0,
                    weightUnit: 'GRAMS',
                  },
                },
              ],
            },
          },
        },
        {
          cursor: 'cursor',
          node: {
            createdAt: '2021-12-01T03:58:24Z',
            description: 'description',
            descriptionHtml: 'descriptionHtml',
            handle: 'f1-2021-fernando-alonso-premium-edition',
            vendor: 'F1 and YARMS',
            status: 'ACTIVE',
            id: 'gid://shopify/Product/2',
            title: 'F1 2021: Fernando Alonso  (Premium Edition)',
            tags: [
              'Artist_Danil Yad',
              'F12021',
              'F1Collector',
              'Mighty AllStars',
              'New Arrivals',
              'NTWRK_MARKETPLACE',
              'premium-edition',
            ],
            onlineStoreUrl: 'onlineStoreUrl',
            images: {
              edges: [],
            },
            productType: 'Collectible - Art Figure',
            options: [
              {
                id: 'gid://shopify/ProductOption/1',
                name: 'Size',
                values: ['Default'],
              },
            ],
            priceRange: {
              maxVariantPrice: {
                amount: '120000.0',
                currencyCode: 'USD',
              },
              minVariantPrice: {
                amount: '120000.0',
                currencyCode: 'USD',
              },
            },
            variants: {
              edges: [
                {
                  node: {
                    compareAtPrice: null,
                    id: 'gid://shopify/ProductVariant/1',
                    image: null,
                    price: '1200.00',
                    sku: 'MAS-SKU1',
                    selectedOptions: [
                      {
                        name: 'Size',
                        value: 'Default',
                      },
                    ],
                    title: 'Default',
                    weight: 0,
                    weightUnit: 'GRAMS',
                  },
                },
              ],
            },
          },
        },
        {
          cursor: 'cursor',
          node: {
            createdAt: '2021-12-01T03:58:24Z',
            description: 'description',
            descriptionHtml: 'descriptionHtml',
            handle: 'f1-2021-fernando-alonso-premium-edition',
            vendor: 'F1 and YARMS',
            status: 'ACTIVE',
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzI=',
            title: 'F1 2021: Fernando Alonso  (Premium Edition)',
            tags: [
              'Artist_Danil Yad',
              'F12021',
              'F1Collector',
              'Mighty AllStars',
              'New Arrivals',
              'NTWRK_MARKETPLACE',
              'premium-edition',
            ],
            onlineStoreUrl: 'onlineStoreUrl',
            images: {
              edges: [],
            },
            productType: 'Collectible - Art Figure',
            options: [
              {
                id: 'gid://shopify/ProductOption/1',
                name: 'Size',
                values: ['Default'],
              },
            ],
            priceRange: {
              maxVariantPrice: {
                amount: '120000.0',
                currencyCode: 'USD',
              },
              minVariantPrice: {
                amount: '120000.0',
                currencyCode: 'USD',
              },
            },
            variants: {
              edges: [
                {
                  node: {
                    compareAtPrice: null,
                    id: 'gid://shopify/ProductVariant/1',
                    image: null,
                    price: '1200.00',
                    sku: 'MAS-SKU1',
                    selectedOptions: [
                      {
                        name: 'Size',
                        value: 'Default',
                      },
                    ],
                    title: 'Default',
                    weight: 0,
                    weightUnit: 'GRAMS',
                  },
                },
              ],
            },
          },
        },
        {
          cursor: 'cursor',
          node: {
            createdAt: '2021-12-01T03:58:24Z',
            description: 'description',
            descriptionHtml: 'descriptionHtml',
            handle: 'f1-2021-fernando-alonso-premium-edition',
            vendor: 'F1 and YARMS',
            status: 'ACTIVE',
            id: 'gid://shopify/Product/3',
            title: 'F1 2021: Fernando Alonso  (Premium Edition)',
            tags: [
              'Artist_Danil Yad',
              'F12021',
              'F1Collector',
              'Mighty AllStars',
              'New Arrivals',
              'NTWRK_MARKETPLACE',
              'premium-edition',
            ],
            onlineStoreUrl: 'onlineStoreUrl',
            images: {
              edges: [],
            },
            productType: 'Collectible - Art Figure',
            options: [
              {
                id: 'gid://shopify/ProductOption/1',
                name: 'Size',
                values: ['Default'],
              },
            ],
            priceRange: {
              maxVariantPrice: {
                amount: '120000.0',
                currencyCode: 'USD',
              },
              minVariantPrice: {
                amount: '120000.0',
                currencyCode: 'USD',
              },
            },
            variants: {
              edges: [
                {
                  node: {
                    compareAtPrice: null,
                    id: 'gid://shopify/ProductVariant/1',
                    image: null,
                    price: '1200.00',
                    sku: 'MAS-SKU1',
                    selectedOptions: [
                      {
                        name: 'Size',
                        value: 'Default',
                      },
                    ],
                    title: 'Default',
                    weight: 0,
                    weightUnit: 'GRAMS',
                  },
                },
              ],
            },
          },
        },
      ],
      pageInfo: { hasNextPage: hasNextPage },
    },
  },
});

const dbProducts = [
  {
    _id: '1',
    productPermalink: '1',
    shopifyProductId: '1',
    udid: '',
    productStore: 'MightyJaxx',
    productModelNumber: '',
    productVariantId: '1',
    productVariantSku: 'MAS-SKU0',
    productTitle: 'F1 2021: Daniel Ricciardo (Premium Edition)',
    productPrice: '1200.00',
    productDescription: 'productDescription',
    productPublished: 1,
    productTags: 'F12021, F1Collector, NTWRK_MARKETPLACE, premium-edition',
    productOptions: '',
    productComment: 1,
    productAddedDate: '2021-10-26T09:07:00.120Z',
    productStock: 150,
    productNew: 0,
    productTrending: 0,
    productEditionNumber: 0,
    productFeature: 0,
    productRecommendation: 0,
    productPublishedAt: '2021-10-15T21:46:04+08:00',
    currentDateUTC: '2021-10-15T21:46:04+08:00',
    productImageUrl: 'productImageUrl',
    productBackgroundImage: null,
    productBackgroundImageS3: 'productBackgroundImageS3',
    productLaunchColor: 'productLaunchColor',
    productLaunchDate: '2022-10-15',
    productLaunchStatus: 'true',
    productLaunchTime: '21:46:04',
    productLaunchVipStatus: null,
    productOrder: 2,
    productSoldOutDate: null,
    productSoldOutStatus: 'false',
    productSoldOutTime: null,
    productSpecification: null,
    completedQrcode: 0,
    pendingQrcode: 0,
    totalQrcode: 0,
    stock_on_hand: 0,
    productArtist: 'artistName',
  },
  {
    _id: '3',
    productPermalink: '3',
    shopifyProductId: '3',
    udid: '',
    productStore: 'MightyJaxx',
    productModelNumber: '',
    productVariantId: '1',
    productVariantSku: 'MAS-SKU0',
    productTitle: 'F1 2021: Daniel Ricciardo (Premium Edition)',
    productPrice: '1200.00',
    productDescription: 'productDescription',
    productPublished: 1,
    productTags: 'F12021, F1Collector, NTWRK_MARKETPLACE, premium-edition',
    productOptions: '',
    productComment: 1,
    productAddedDate: '2021-10-26T09:07:00.120Z',
    productStock: 150,
    productNew: 0,
    productTrending: 0,
    productEditionNumber: 0,
    productFeature: 0,
    productRecommendation: 0,
    productPublishedAt: '2021-10-15T21:46:04+08:00',
    currentDateUTC: '2021-10-15T21:46:04+08:00',
    productImageUrl: 'productImageUrl',
    productBackgroundImage: null,
    productBackgroundImageS3: 'productBackgroundImageS3',
    productLaunchColor: null,
    productLaunchDate: '2022-10-15',
    productLaunchStatus: 'true',
    productLaunchTime: '21:46:04',
    productLaunchVipStatus: null,
    productOrder: 2,
    productSoldOutDate: null,
    productSoldOutStatus: 'true',
    productSoldOutTime: null,
    productSpecification: null,
    completedQrcode: 0,
    pendingQrcode: 0,
    totalQrcode: 0,
    stock_on_hand: 0,
    productArtist: 'artistName',
  },
  {
    _id: '2',
    productPermalink: '2',
    shopifyProductId: '2',
    udid: '',
    productStore: 'MightyJaxx',
    productModelNumber: '',
    productVariantId: '2',
    productVariantSku: 'MAS-SKU1',
    productTitle: 'F1 2021: Sebastian Vettel (Premium Edition)',
    productPrice: '1200.00',
    productDescription: 'productDescription',
    productPublished: 1,
    productTags: 'F12021, F1Collector, NTWRK_MARKETPLACE, premium-edition',
    productOptions: '',
    productComment: 1,
    productAddedDate: '2021-10-26T09:24:36.173Z',
    productStock: 150,
    productNew: 0,
    productTrending: 0,
    productEditionNumber: 0,
    productFeature: 0,
    productRecommendation: 0,
    productPublishedAt: '2021-10-15T21:46:34+08:00',
    productImageUrl: 'productImageUrl',
    productBackgroundImage: 'productBackgroundImage',
    productBackgroundImageS3: null,
    productLaunchColor: null,
    productLaunchDate: null,
    productLaunchStatus: 'false',
    productLaunchTime: null,
    productLaunchVipStatus: null,
    productOrder: null,
    productSoldOutDate: '2021-10-16',
    productSoldOutStatus: 'true',
    productSoldOutTime: '21:46:04',
    productSpecification: null,
    completedQrcode: 0,
    pendingQrcode: 0,
    totalQrcode: 0,
    stock_on_hand: 0,
    productArtist: 'artistName',
  },
];

const storefrontProducts = {
  data: {
    products: {
      edges: [
        {
          cursor: 'cursor==',
          node: {
            createdAt: '2020-09-02T02:41:23Z',
            description: 'description',
            descriptionHtml: 'descriptionHtml',
            handle: 'xxray-plus-mr-potato-head',
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzE=',
            title: '4D XXRAY Mr Potato Head',
            tags: [
              'All Artists',
              'Jason Freeny',
              'Out of stock',
              'XXRAY',
              'XXRAY PLUS',
            ],
            onlineStoreUrl: 'onlineStoreUrl',
            images: {
              edges: [
                {
                  node: {
                    originalSrc: 'originalSrc',
                    src: 'transformedSrc',
                  },
                },
              ],
            },
            productType: 'Collectible - Art Figure',
            options: [
              {
                id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0T3B0aW9uLzY2NDkzNzQzMTA1MzQ=',
                name: 'Size',
                values: ['Default'],
              },
            ],
            priceRange: {
              maxVariantPrice: {
                amount: '139.0',
                currencyCode: 'USD',
              },
              minVariantPrice: {
                amount: '139.0',
                currencyCode: 'USD',
              },
            },
            variants: {
              edges: [
                {
                  node: {
                    compareAtPrice: null,
                    id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zNDc2MDg3OTk2NDI5NA==',
                    image: {
                      originalSrc: 'originalSrc',
                      transformedSrc: 'transformedSrc',
                    },
                    price: '139.00',
                    sku: 'LND-20HBXPPHOG19',
                    selectedOptions: [
                      {
                        name: 'Size',
                        value: 'Default',
                      },
                    ],
                    title: 'Default',
                    weight: 0,
                    weightUnit: 'GRAMS',
                  },
                },
              ],
            },
          },
        },
        {
          cursor: 'cursor',
          node: {
            createdAt: '2021-07-26T03:23:22Z',
            description: 'description',
            descriptionHtml: 'descriptionHtml',
            handle:
              'a-wood-awakening-chill-out-porcelain-by-juce-gace-gold-chrome-edition',
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzI=',
            title:
              'A Wood Awakening Chill-Out: Porcelain by Juce Gace (Gold Chrome Edition)',
            tags: [
              'All Artists',
              'Artist_Juce Gace',
              'Category_Premium Art Toys',
              'exclusive',
              'Material_Others',
              'Out of stock',
            ],
            onlineStoreUrl: 'onlineStoreUrl',
            images: {
              edges: [
                {
                  node: {
                    originalSrc: 'originalSrc',
                    transformedSrc: 'transformedSrc',
                  },
                },
              ],
            },
            productType: 'Collectible - Art Figure',
            options: [
              {
                id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0T3B0aW9uLzg0ODU1Nzc0NTc3OTg=',
                name: 'Size',
                values: ['Default'],
              },
            ],
            priceRange: {
              maxVariantPrice: {
                amount: '2999999.0',
                currencyCode: 'USD',
              },
              minVariantPrice: {
                amount: '2999999.0',
                currencyCode: 'USD',
              },
            },
            variants: {
              edges: [
                {
                  node: {
                    compareAtPrice: null,
                    id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zOTQyODgyMjEwNjI0Ng==',
                    image: {
                      originalSrc: 'originalSrc',
                      transformedSrc: 'transformedSrc',
                    },
                    price: '2999999.00',
                    sku: 'ORG-21JGAWAPGCE30',
                    selectedOptions: [
                      {
                        name: 'Size',
                        value: 'Default',
                      },
                    ],
                    title: 'Default',
                    weight: 0,
                    weightUnit: 'GRAMS',
                  },
                },
              ],
            },
          },
        },
        {
          cursor: 'cursor',
          node: {
            createdAt: '2021-07-26T03:23:22Z',
            description: 'description',
            descriptionHtml: 'descriptionHtml',
            handle:
              'a-wood-awakening-chill-out-porcelain-by-juce-gace-gold-chrome-edition',
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzM=',
            title:
              'A Wood Awakening Chill-Out: Porcelain by Juce Gace (Gold Chrome Edition)',
            tags: [
              'All Artists',
              'Artist_Juce Gace',
              'Category_Premium Art Toys',
              'exclusive',
              'Material_Others',
              'Out of stock',
            ],
            onlineStoreUrl: 'onlineStoreUrl',
            images: {
              edges: [],
            },
            productType: 'Collectible - Art Figure',
            options: [
              {
                id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0T3B0aW9uLzg0ODU1Nzc0NTc3OTg=',
                name: 'Size',
                values: ['Default'],
              },
            ],
            priceRange: {
              maxVariantPrice: {
                amount: '2999999.0',
                currencyCode: 'USD',
              },
              minVariantPrice: {
                amount: '2999999.0',
                currencyCode: 'USD',
              },
            },
            variants: {
              edges: [
                {
                  node: {
                    compareAtPrice: null,
                    id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zOTQyODgyMjEwNjI0Ng==',
                    image: {
                      originalSrc: 'originalSrc',
                      transformedSrc: 'transformedSrc',
                    },
                    price: '2999999.00',
                    sku: 'ORG-21JGAWAPGCE30',
                    selectedOptions: [
                      {
                        name: 'Size',
                        value: 'Default',
                      },
                    ],
                    title: 'Default',
                    weight: 0,
                    weightUnit: 'GRAMS',
                  },
                },
              ],
            },
          },
        },
      ],
    },
  },
};

const dbProductNotFindProd = [
  {
    _id: '1',
    productPermalink: '1',
    shopifyProductId: '1',
    udid: '',
    productStore: 'MightyJaxx',
    productModelNumber: '',
    productVariantId: '1',
    productVariantSku: 'MAS-SKU1',
    productTitle: 'F1 2021: Sebastian Vettel (Premium Edition)',
    productPrice: '1200.00',
    productDescription: 'productDescription',
    productPublished: 1,
    productTags: 'F12021, F1Collector, NTWRK_MARKETPLACE, premium-edition',
    productOptions: '',
    productComment: 1,
    productAddedDate: '2021-10-26T09:24:36.173Z',
    productStock: 150,
    productNew: 0,
    productTrending: 0,
    productEditionNumber: 0,
    productFeature: 0,
    productRecommendation: 0,
    productPublishedAt: '2021-10-15T21:46:34+08:00',
    productImageUrl: 'productImageUrl',
    productBackgroundImage: null,
    productBackgroundImageS3: null,
    productLaunchColor: null,
    productLaunchDate: null,
    productLaunchStatus: null,
    productLaunchTime: null,
    productLaunchVipStatus: null,
    productOrder: null,
    productSoldOutDate: null,
    productSoldOutStatus: null,
    productSoldOutTime: null,
    productSpecification: null,
    completedQrcode: 0,
    pendingQrcode: 0,
    totalQrcode: 0,
    stock_on_hand: 0,
  },
  {
    _id: '4',
    productPermalink: '4',
    shopifyProductId: '4',
    udid: '',
    productStore: 'MightyJaxx',
    productModelNumber: '',
    productVariantId: '4',
    productVariantSku: 'MAS-SKU1',
    productTitle: 'F1 2021: Sebastian Vettel (Premium Edition)',
    productPrice: '1200.00',
    productDescription: 'productDescription',
    productPublished: 1,
    productTags: 'F12021, F1Collector, NTWRK_MARKETPLACE, premium-edition',
    productOptions: '',
    productComment: 1,
    productAddedDate: '2021-10-26T09:24:36.173Z',
    productStock: 150,
    productNew: 0,
    productTrending: 0,
    productEditionNumber: 0,
    productFeature: 0,
    productRecommendation: 0,
    productPublishedAt: '2021-10-15T21:46:34+08:00',
    productImageUrl: 'productImageUrl',
    productBackgroundImage: null,
    productBackgroundImageS3: null,
    productLaunchColor: null,
    productLaunchDate: null,
    productLaunchStatus: 'false',
    productLaunchTime: null,
    productLaunchVipStatus: null,
    productOrder: null,
    productSoldOutDate: null,
    productSoldOutStatus: 'false',
    productSoldOutTime: null,
    productSpecification: null,
    completedQrcode: 0,
    pendingQrcode: 0,
    totalQrcode: 0,
    stock_on_hand: 0,
  },
];

const storefrontProductsReturnNodes = {
  data: {
    nodes: [
      {
        createdAt: '2020-09-02T02:41:23Z',
        description: 'description',
        descriptionHtml: 'descriptionHtml',
        handle: 'xxray-plus-mr-potato-head',
        id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzE=',
        title: '4D XXRAY Mr Potato Head',
        tags: [
          'All Artists',
          'Jason Freeny',
          'Out of stock',
          'XXRAY',
          'XXRAY PLUS',
        ],
        onlineStoreUrl: 'onlineStoreUrl',
        images: {
          edges: [
            {
              node: {
                originalSrc: 'originalSrc',
                src: 'transformedSrc',
              },
            },
          ],
        },
        productType: 'Collectible - Art Figure',
        options: [
          {
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0T3B0aW9uLzY2NDkzNzQzMTA1MzQ=',
            name: 'Size',
            values: ['Default'],
          },
        ],
        priceRange: {
          maxVariantPrice: {
            amount: '139.0',
            currencyCode: 'USD',
          },
          minVariantPrice: {
            amount: '139.0',
            currencyCode: 'USD',
          },
        },
        variants: {
          edges: [
            {
              node: {
                compareAtPrice: null,
                id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zNDc2MDg3OTk2NDI5NA==',
                image: {
                  originalSrc: 'originalSrc',
                  transformedSrc: 'transformedSrc',
                },
                price: '139.00',
                sku: 'LND-20HBXPPHOG19',
                selectedOptions: [
                  {
                    name: 'Size',
                    value: 'Default',
                  },
                ],
                title: 'Default',
                weight: 0,
                weightUnit: 'GRAMS',
              },
            },
          ],
        },
      },
      {
        createdAt: '2021-07-26T03:23:22Z',
        description: 'description',
        descriptionHtml: 'descriptionHtml',
        handle:
          'a-wood-awakening-chill-out-porcelain-by-juce-gace-gold-chrome-edition',
        id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzI=',
        title:
          'A Wood Awakening Chill-Out: Porcelain by Juce Gace (Gold Chrome Edition)',
        tags: [
          'All Artists',
          'Artist_Juce Gace',
          'Category_Premium Art Toys',
          'exclusive',
          'Material_Others',
          'Out of stock',
        ],
        onlineStoreUrl: 'onlineStoreUrl',
        images: {
          edges: [
            {
              node: {
                originalSrc: 'originalSrc',
                transformedSrc: 'transformedSrc',
              },
            },
          ],
        },
        productType: 'Collectible - Art Figure',
        options: [
          {
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0T3B0aW9uLzg0ODU1Nzc0NTc3OTg=',
            name: 'Size',
            values: ['Default'],
          },
        ],
        priceRange: {
          maxVariantPrice: {
            amount: '2999999.0',
            currencyCode: 'USD',
          },
          minVariantPrice: {
            amount: '2999999.0',
            currencyCode: 'USD',
          },
        },
        variants: {
          edges: [
            {
              node: {
                compareAtPrice: null,
                id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zOTQyODgyMjEwNjI0Ng==',
                image: {
                  originalSrc: 'originalSrc',
                  transformedSrc: 'transformedSrc',
                },
                price: '2999999.00',
                sku: 'ORG-21JGAWAPGCE30',
                selectedOptions: [
                  {
                    name: 'Size',
                    value: 'Default',
                  },
                ],
                title: 'Default',
                weight: 0,
                weightUnit: 'GRAMS',
              },
            },
          ],
        },
      },
      {
        createdAt: '2021-07-26T03:23:22Z',
        description: 'description',
        descriptionHtml: 'descriptionHtml',
        handle:
          'a-wood-awakening-chill-out-porcelain-by-juce-gace-gold-chrome-edition',
        id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzM=',
        title:
          'A Wood Awakening Chill-Out: Porcelain by Juce Gace (Gold Chrome Edition)',
        tags: [
          'All Artists',
          'Artist_Juce Gace',
          'Category_Premium Art Toys',
          'exclusive',
          'Material_Others',
          'Out of stock',
        ],
        onlineStoreUrl: 'onlineStoreUrl',
        images: {
          edges: [],
        },
        productType: 'Collectible - Art Figure',
        options: [
          {
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0T3B0aW9uLzg0ODU1Nzc0NTc3OTg=',
            name: 'Size',
            values: ['Default'],
          },
        ],
        priceRange: {
          maxVariantPrice: {
            amount: '2999999.0',
            currencyCode: 'USD',
          },
          minVariantPrice: {
            amount: '2999999.0',
            currencyCode: 'USD',
          },
        },
        variants: {
          edges: [
            {
              node: {
                compareAtPrice: null,
                id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zOTQyODgyMjEwNjI0Ng==',
                image: {
                  originalSrc: 'originalSrc',
                  transformedSrc: 'transformedSrc',
                },
                price: '2999999.00',
                sku: 'ORG-21JGAWAPGCE30',
                selectedOptions: [
                  {
                    name: 'Size',
                    value: 'Default',
                  },
                ],
                title: 'Default',
                weight: 0,
                weightUnit: 'GRAMS',
              },
            },
          ],
        },
      },
    ],
    node: {
      createdAt: '2020-09-02T02:41:23Z',
      description: 'test',
      descriptionHtml: 'descriptionHtml',
      handle: 'xxray-plus-mr-potato-head',
      id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzE=',
      title: '4D XXRAY Mr Potato Head',
      tags: [
        'All Artists',
        'Jason Freeny',
        'Out of stock',
        'XXRAY',
        'XXRAY PLUS',
      ],
      onlineStoreUrl: 'onlineStoreUrl',
      images: {
        edges: [
          {
            node: {
              originalSrc: 'originalSrc',
              src: 'transformedSrc',
            },
          },
        ],
      },
      productType: 'Collectible - Art Figure',
      options: [
        {
          id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0T3B0aW9uLzY2NDkzNzQzMTA1MzQ=',
          name: 'Size',
          values: ['Default'],
        },
      ],
      priceRange: {
        maxVariantPrice: {
          amount: '139.0',
          currencyCode: 'USD',
        },
        minVariantPrice: {
          amount: '139.0',
          currencyCode: 'USD',
        },
      },
      variants: {
        edges: [
          {
            node: {
              compareAtPrice: null,
              id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zNDc2MDg3OTk2NDI5NA==',
              image: {
                originalSrc: 'originalSrc',
                transformedSrc: 'transformedSrc',
              },
              price: '139.00',
              sku: 'SKU',
              selectedOptions: [
                {
                  name: 'Size',
                  value: 'Default',
                },
              ],
              title: 'Default',
              weight: 0,
              weightUnit: 'GRAMS',
            },
          },
        ],
      },
    },
    productByHandle: {
      createdAt: '2020-09-02T02:41:23Z',
      description: 'test',
      descriptionHtml: 'descriptionHtml',
      handle: 'xxray-plus-mr-potato-head',
      id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzE=',
      title: '4D XXRAY Mr Potato Head',
      tags: [
        'All Artists',
        'Jason Freeny',
        'Out of stock',
        'XXRAY',
        'XXRAY PLUS',
      ],
      onlineStoreUrl: 'onlineStoreUrl',
      images: {
        edges: [
          {
            node: {
              originalSrc: 'originalSrc',
              src: 'transformedSrc',
            },
          },
        ],
      },
      productType: 'Collectible - Art Figure',
      options: [
        {
          id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0T3B0aW9uLzY2NDkzNzQzMTA1MzQ=',
          name: 'Size',
          values: ['Default'],
        },
      ],
      priceRange: {
        maxVariantPrice: {
          amount: '139.0',
          currencyCode: 'USD',
        },
        minVariantPrice: {
          amount: '139.0',
          currencyCode: 'USD',
        },
      },
      variants: {
        edges: [
          {
            node: {
              compareAtPrice: null,
              id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zNDc2MDg3OTk2NDI5NA==',
              image: {
                originalSrc: 'originalSrc',
                transformedSrc: 'transformedSrc',
              },
              price: '139.00',
              sku: 'SKU',
              selectedOptions: [
                {
                  name: 'Size',
                  value: 'Default',
                },
              ],
              title: 'Default',
              weight: 0,
              weightUnit: 'GRAMS',
            },
          },
        ],
      },
    },
  },
};

module.exports = {
  listStores,
  shopifyProduct,
  dbProducts,
  storefrontProducts,
  dbProductNotFindProd,
  storefrontProductsReturnNodes,
};
