const exclusiveTags = {
  _id: '1',
  titleTag: 'Test',
  productTag: 'Test',
  customerTag: ['Test'],
  description: 'This is description for this tag',
  status: false,
  imgUrl: 'imgUrl',
  createdAt: '2021-11-28T06:48:01.916+0000',
  updatedAt: '2022-11-02T08:30:28.698+0000',
  bannerText: 'Test',
};

const nyamsumTag = {
  data: {
    products: {
      edges: [
        {
          cursor: 'cursor',
          node: {
            createdAt: '2016-02-20T10:23:22Z',
            description: 'description',
            descriptionHtml: 'descriptionHtml',
            handle: 'handle',
            id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzUzOTUzMjY0NjU=',
            title: 'title',
            tags: ['exclusive', 'TRGTEST'],
            onlineStoreUrl: 'onlineStoreUrl',
            images: {
              edges: [
                {
                  node: {
                    originalSrc: 'originalSrc',
                    transformedSrc: 'transformedSrc',
                  },
                },
              ],
            },
            productType: 'productType',
            options: [
              {
                id: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzUzOTUzMjY0NjU=',
                name: 'Color',
                values: ['Yellow'],
              },
            ],
            priceRange: {
              maxVariantPrice: {
                amount: '00.0',
                currencyCode: 'USD',
              },
              minVariantPrice: {
                amount: '00.0',
                currencyCode: 'USD',
              },
            },
            variants: {
              edges: [
                {
                  node: {
                    compareAtPrice: null,
                    id: 'id',
                    image: {
                      originalSrc: 'originalSrc',
                      transformedSrc: 'transformedSrc',
                    },
                    price: '00.00',
                    sku: 'SKU',
                    selectedOptions: [
                      {
                        name: 'Color',
                        value: 'Yellow',
                      },
                    ],
                    title: 'Yellow',
                    weight: 0.5,
                    weightUnit: 'KILOGRAMS',
                    currentlyNotInStock: false,
                  },
                },
              ],
            },
          },
        },
      ],
      pageInfo: {
        hasNextPage: false,
      },
    },
  },
};

module.exports = { exclusiveTags, nyamsumTag };
