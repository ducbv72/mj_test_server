const jwt = require('jsonwebtoken');
const sub = '1';
const email = 'tester@test.com';
const email_verified = true;
const full_name = 'tester';
const terms_token = 'terms_token';
const id = Buffer.from('gid://shopify/Product/1').toString('base64');

// Apple
const appleConstants = {
  appleAccount: {
    email,
    sub,
    email_verified,
  },
  id,
};
appleConstants.identityTokenFail = jwt.sign({ sub, email_verified }, 'secret');
appleConstants.identityToken = jwt.sign(appleConstants.appleAccount, 'secret');

// Facebook
const facebookConstants = {
  facebookAccount: {
    email,
    sub,
    email_verified,
  },
  id,
};

//Google
const googleConstants = {
  googleAccount: {
    email,
    sub,
    email_verified,
  },
  googleParams: {
    full_name,
    access_token: 'google_valid_token',
    user_id: '1',
  },
  id,
};

//AcceptTnC
const acceptTnC = {
  email,
  id: 'gid://shopify/Customer/1',
  terms_token: terms_token,
  customer_id: sub,
};

//Get Transaction Fee
const getTransactionFee = {
  fee: 1,
  ethPriceInUsd: 0.1,
  address: '0x5aAeb6053F3E94C9b9A09f33669435E7Ef1BeAed'
};

module.exports = {
  appleConstants,
  facebookConstants,
  googleConstants,
  acceptTnC,
  getTransactionFee,
};
