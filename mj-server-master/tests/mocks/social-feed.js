const { user } = require('./vaultFeatures');
const tags = [
  {
    _id: '1',
    name: '#VIETNAM',
  },
  {
    _id: '2',
    name: '#hash_tag',
  },
  {
    _id: '606eb7604d6f8f3578916094',
    name: '#123abc',
  },
];
const searchTags = [
  {
    _id: '1',
    name: '#VIETNAM',
  },
  {
    _id: '2',
    name: '#hash_tag',
  },
];

const post = {
  _id: '20af6aa9e6f0e73dccbfff23',
  username: 'Tester',
  post_id: 1,
  caption: 'testCaption',
  image_post: 'image_post_img',
  user_tag: '#Test',
  posted_date: '2020-05-27T09:47:21.911+0000',
  last_updated_date: '2020-06-15T17:47:06+08:00',
  like_count: 2,
  isRemove: true,
  customer_id: '1',
};

const trending = [];

const genPost = (limit) => {
  for (let i = 0; i < limit; i++) {
    const newPost = post;
    newPost.post_id = i;
    trending.push(newPost);
  }
  return trending;
};

const follows = [
  {
    _id: '1',
    username: 'Tester1',
    user_followed: 'Tester2',
    following_datetime: '2020-07-19T07:59:35Z',
  },
  {
    _id: '2',
    username: 'Tester1',
    user_followed: 'Tester3',
    following_datetime: '2020-07-19T08:00:07Z',
  },
];

const followsResult = [
  {
    _id: '1',
    username: 'Tester',
    post_id: 1,
    caption: 'like',
    image_post: 'image_post',
    user_tag: '#Test',
    posted_date: '2020-06-22T04:57:25.296Z',
    last_updated_date: '2020-07-01T16:12:42+08:00',
    like_count: 1,
    isRemove: false,
    customer_id: '1',
  },
  {
    _id: '2',
    username: 'Tester',
    post_id: 2,
    caption: 'agd',
    image_post: 'image_post',
    user_tag: '#Test',
    posted_date: '2020-07-05T03:22:23.296Z',
    last_updated_date: '2020-07-05T03:22:23.296Z',
    like_count: 0,
    isRemove: false,
  },
];

const userFollow = {
  _id: 1,
  email: 'tester@test.vn',
  firstName: 'tester',
  lastName: 'test',
  customerId: '1',
  userName: 'Tester',
  userNameLower: 'test',
  phone: null,
  password: '$2a$10$0RkbU9OyH8nqX0GSKe.t4OgBdsgMU.n7CYYaEQj0VYb1nq4nChRtO',
  feedPrivate: false,
  vaultPrivate: false,
};

const userFollowed = {
  _id: 2,
  email: 'tester2@test.vn',
  firstName: 'tester',
  lastName: 'test',
  customerId: '1',
  userName: 'Tester2',
  userNameLower: 'test2',
  phone: null,
  password: '$2a$10$0RkbU9OyH8nqX0GSKe.t4OgBdsgMU.n7CYYaEQj0VYb1nq4nChRtO',
  feedPrivate: false,
  vaultPrivate: false,
};

const blogFromShopify = (handle) => [
  {
    id: 1,
    handle: handle,
    title: 'News',
    updated_at: '2020-08-04T20:00:02+08:00',
    commentable: 'no',
    feedburner: null,
    feedburner_location: null,
    created_at: '2010-11-16T20:02:19+08:00',
    template_suffix: null,
    tags: 'Test, Tags',
    admin_graphql_api_id: 'gid://shopify/OnlineStoreBlog/1',
  },
];
const article = (id) => ({
  id: id,
  title: 'title',
  created_at: '2021-08-03T11:06:34+08:00',
  body_html: 'body description',
  blog_id: id,
  author: 'Gregory Govin',
  user_id: 1,
  published_at: '2021-08-04T20:00:02+08:00',
  updated_at: '2021-08-04T20:00:02+08:00',
  summary_html: '',
  template_suffix: '',
  handle: 'handle',
  tags: '',
  admin_graphql_api_id: 'gid://shopify/OnlineStoreArticle/1',
  image: {
    created_at: '2020-08-03T11:18:04+08:00',
    alt: '',
    width: 1070,
    height: 712,
    src: 'https://cdn.shopify.com/s/files/1/0701/0143/articles/1',
  },
});

const articleFromShopify = (limit) => {
  const listArticle = [];
  for (let i = 0; i < limit; i++) {
    listArticle.push(article(i));
  }
  return listArticle;
};

const tagsInstance = {
  tags,
  searchTags,
};

const liker = {
  _id: '620890e1c9bc7a86bd4e60360',
  post_id: 1,
  like_date: '2020-04-28T15:26:20+08:00',
  liker_customer_id: '1',
};

const listFollowing = [
  {
    username: 'tester@test.com',
    followed_date: '2022-05-07T04:02:23.062Z',
  },
  {
    username: 'tester2@test.vn',
    followed_date: '2022-06-01T04:02:23.062Z',
  },
];

const listPersons = [
  {
    username: 'Tester',
    post_id: 1,
    caption: 'Test post',
    image_post: 'image_post',
    user_tag: '#Test',
    posted_date: '2020-05-27T09:50:12.262Z',
    last_updated_date: '2020-05-31T16:26:08+08:00',
    like_count: 1,
    isRemove: false,
    posts: [post],
  },
  {
    username: 'Tester',
    post_id: 1,
    caption: 'caption',
    image_post: 'image_post',
    user_tag: '#Test',
    posted_date: '2020-05-27T09:53:58.727Z',
    last_updated_date: '2020-06-01T15:28:03+08:00',
    like_count: 2,
    isRemove: false,
    posts: [post],
  },
];

const listLikers = {
  username: 'DiepNguyen',
  posts: {
    post_id: 1,
    likers: [
      {
        id: '60b5dfdad2ac375d89e6edc0',
      },
      {
        id: '60b5e183d2ac375d89e6edc5',
      },
      {
        id: '60b8984de8103419fb4abc28',
      },
      {
        id: '60dd851d26b0cd1d539855ae',
        username: 'DiepNguyen',
      },
    ],
  },
};

module.exports = {
  tagsInstance,
  user,
  post,
  genPost,
  follows,
  followsResult,
  userFollow,
  userFollowed,
  blogFromShopify,
  articleFromShopify,
  liker,
  listFollowing,
  listLikers,
  listPersons,
};
