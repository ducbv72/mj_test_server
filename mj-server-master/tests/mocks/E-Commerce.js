const edge = (num) => ({
  cursor: num,
  node: {
    id: `gid://shopify/Order/${num}`,
    name: `#${num}`,
    email: 'tester@test.com',
    phone: null,
    updatedAt: '2020-02-20T02:20:02Z',
    createdAt: '2020-02-20T02:20:02Z',
    tags: ['In Stock'],
    fullyPaid: true,
    totalPriceSet: {
      presentmentMoney: { amount: '0.00', currencyCode: 'USD' },
      shopMoney: { amount: '0.00', currencyCode: 'USD' },
    },
  },
});
const genDataFromModel = (num, model) => {
  const result = [];
  for (let index = 0; index <= num; index++) {
    if (index === 0) continue;
    result.push(model(index));
  }
  return result;
};

const orders = (numOfOrders, next = false) => {
  if (numOfOrders > 10) {
    numOfOrders = 10;
  }
  return {
    customers: {
      edges: [
        {
          node: {
            id: 'gid://shopify/Customer/1',
            displayName: 'Tester',
            orders: {
              pageInfo: { hasNextPage: next, hasPreviousPage: false },
              edges: genDataFromModel(numOfOrders, edge),
            },
          },
        },
      ],
    },
  };
};

const orderProduct = (id) => ({
  _id: id,
  productPermalink: id,
  shopifyProductId: id,
  udid: 'udid',
  orderSmartContract: 'orderSmartContract',
  productModelNumber: `SKU-${id}`,
  productVariantId: id,
  productVariantSku: `SKU-${id}`,
  productTitle: 'Test',
  productPrice: '0.00',
  productDescription: 'productDescription',
  productPublished: 'true',
  productTags: 'tags',
  productOptions: '',
  productComment: false,
  productTrending: false,
  productFeature: false,
  productRecommendation: false,
  productPublishedAt: null,
  productArtist: '',
  productLaunchColor: '#000000',
  productLaunchDate: '',
  productLaunchStatus: '0',
  productLaunchTime: '',
  productLaunchVipStatus: '0',
  productOrder: '9999',
  productSoldOutDate: '',
  productSoldOutStatus: '0',
  productSoldOutTime: '',
  productTitleOnApp: '',
  isShowStockxBanner: false,
  product_url: 'rivet-wars-the-mountaineer',
  productUrl: 'rivet-wars-the-mountaineer',
  qrcode: null,
  password: '',
  recipientImage: '',
  changeOwnerDone: true,
  newOwnerEmail: 'tester@test.com',
  newOwnerFirstName: 'Tester',
  newOwnerId: `${id}`,
  newOwnerLastName: '',
  productEncryptedModel: 'encrytedMessage',
});

const order = (num) => ({
  id: num,
  admin_graphql_api_id: `gid://shopify/Order/${num}`,
  confirmed: true,
  encrytedMessage: 'encrytedMessage',
  orderProducts: [
    {
      productImageUrl: 'productImageUrl',
      productImageUrlS3: 'productImageUrlS3',
      productBackgroundImageS3: 'productBackgroundImageS3',
    },
  ],
  variant_id: num,
  customerId: '1',
  shopifyOrderId: '1',
  contact_email: 'tester@test.com',
  currency: 'USD',
  current_subtotal_price: '0.00',
  current_total_discounts: '0.00',
  current_total_duties_set: null,
  current_total_price: '0.00',
  current_total_tax: '0.00',
  customer_locale: 'en',
  email: 'tester@test.com',
  financial_status: 'paid',
  fulfillment_status: null,
  gateway: null,
  landing_site: null,
  landing_site_ref: null,
  location_id: null,
  sku: `SKU-${num}`,
  name: `#${num}`,
  note: '',
  note_attributes: [],
  number: num,
  order_number: num,
  original_total_duties_set: null,
  payment_gateway_names: [],
  phone: null,
  presentment_currency: 'USD',
  processing_method: 'manual',
  reference: null,
  referring_site: null,
  source_identifier: null,
  source_name: 'shopify_draft_order',
  source_url: null,
  subtotal_price: '0.00',
  tags: 'Pre Order',
  tax_lines: [],
  taxes_included: true,
  test: true,
  token: 'token',
  total_discounts: '0.00',
  total_line_items_price: '0.00',
  total_outstanding: '0.00',
  total_price: '0.00',
  total_price_usd: '0.00',
  total_tax: '0.00',
  total_tip_received: '0.00',
  total_weight: 0,
  user_id: num,
  billing_address: {
    first_name: 'test ',
    address1: 'test',
    phone: null,
    city: 'test',
    zip: 'test',
    province: null,
    country: 'test',
    last_name: 'test',
    address2: 'test',
    company: null,
    name: 'test',
    country_code: 'test',
    province_code: null,
  },
  customer: {
    id: num,
    email: 'tester@test.com',
    accepts_marketing: false,
    first_name: 'tester',
    last_name: 'tester',
    state: 'enabled',
    total_spent: '0.00',
    note: null,
    verified_email: true,
    multipass_identifier: null,
    tax_exempt: false,
    phone: null,
    tags: 'test, tester',
    currency: 'USD',
    marketing_opt_in_level: null,
    tax_exemptions: [],
    admin_graphql_api_id: `gid://shopify/Order/${num}`,
  },
  discount_applications: [],
  fulfillments: [],
  orderCertNumber: 2,
});

const product = (num, slug = []) => {
  const result = {
    _id: num,
    shopifyProductId: num,
    productVariantSku: `SKU-${num}`,
    productTitle: `Product test ${num}`,
    productPrice: '0.00',
    productNew: true,
    productArtist: 'test',
    productLaunchDate: '2020-02-20',
    productLaunchStatus: 'true',
    productLaunchTime: '16:50:00',
    productLaunchVipStatus: 'false',
    productSoldOutDate: '2020-02-20',
    productSoldOutStatus: 'true',
    productSoldOutTime: '16:53:00',
    stockxSlug: slug,
    isShowStockxBanner: true,
    productTags: 'tags, test',
  };
  if (num === 2) {
    result.productBackgroundImageS3 = 's3url';
    result.productBackgroundImage = 'url';
    result.productNew = false;
  }
  return result;
};
const prodductB2B = (id) => ({
  cursor: id,
  node: {
    description: 'description',
    descriptionHtml: 'description',
    handle: 'handle',
    id: id,
    title: 'title',
    tags: ['tags'],
    onlineStoreUrl: 'https://mightyjaxx.com/products/test',
    images: {
      edges: [
        {
          node: {
            originalSrc: 'originalSrc',
            transformedSrc: 'transformedSrc',
          },
        },
      ],
    },
    productType: 'Type',
    options: [
      {
        id: id,
        name: 'Title',
        values: ['Default Title'],
      },
    ],
    priceRange: {
      maxVariantPrice: {
        amount: '0.0',
        currencyCode: 'USD',
      },
      minVariantPrice: {
        amount: '0.0',
        currencyCode: 'USD',
      },
    },
    variants: {
      edges: [
        {
          node: {
            compareAtPrice: null,
            id: id,
            image: {
              originalSrc: 'originalSrc',
              transformedSrc: 'transformedSrc',
            },
            price: '0.00',
            sku: 'TEST',
            selectedOptions: [
              {
                name: 'Title',
                value: 'Default Title',
              },
            ],
            title: 'Default Title',
            weight: 1,
            weightUnit: 'TEST',
            currentlyNotInStock: false,
          },
        },
      ],
    },
  },
});

const productsB2B = (numOfProd, nextPage) => ({
  data: {
    products: {
      edges: genDataFromModel(numOfProd, prodductB2B),
      pageInfo: {
        hasNextPage: nextPage,
      },
    },
  },
});

const store = (id) => ({
  _id: id,
  name: 'Test',
  handle: 'test',
  image: '/uploads/stores/test.png',
  isFeatureCollection: true,
  image_bigger: '/uploads/stores/test.jpg',
});

const artist = (id) => ({
  _id: id,
  shopifyProductId: id,
  productArtist: 'Artist',
  productTitleOnApp: 'Artist',
});

const getProducts = (arg) => {
  if (arg.productFeature === true) {
    return [product(1), product(2)];
  }
  if (arg.productRecommendation === true) {
    return [product(1), product(2)];
  }
  if (arg.productLaunchStatus === 'true') {
    return [product(1)];
  }
  if (arg.productSoldOutStatus === 'true') {
    return [product(2)];
  }
  if (arg['$or'].length === 2) {
    return [artist(1)];
  }
};

const ordersConstant = {
  orders,
  order,
};

/**
 *
 * @param {Boolean} check_tag
 * @returns customer's tags
 */
const shopifyTagExclusiveSale = (check_tag) => {
  if (check_tag) {
    return { tags: 'tags, exclusive-mobile-nyansum' };
  }
  return { tags: 'tags' };
};

const stores = [store(1)];

module.exports = {
  ordersConstant,
  getProducts,
  stores,
  product,
  shopifyTagExclusiveSale,
  productsB2B,
  orderProduct,
};
