const productAltar = (id, order) => ({
  _id: `${id}`,
  shopifyOrderId: `${id}`,
  customerId_encryted: '1',
  customerId: '1',
  orderEmail: 'Tester@test.com',
  orderFirstname: 'Tester',
  orderLastname: 'Tester',
  orderStatus: 'Paid',
  orderProducts: {
    _id: `${id}`,
    productPermalink: `${id}`,
    shopifyProductId: `${id}`,
    udid: 'udid',
    productStore: 'MightyJaxx',
    productVariantId: `${id}`,
    productVariantSku: `SKU-${id}`,
    productTitle: `productTitle-${id}`,
    productPrice: `${id}.00`,
    productPublished: 'true',
    productImageUrl: `productImageUrl-${id}`,
    productAltarOptions: {
      collection: 'Creepy Cuties',
      order: order ? order : -1,
      productImageCards: [
        `productImageCards-${id}0`,
        `productImageCards-${id}0`,
      ],
    },
  },
});

const productAltars = [];

for (let i = 0; i < 11; i++) {
  if (i === 2 || i === 6) {
    productAltars.push(productAltar(`${i}000000603b3fd496c12bbbb`, i));
  }
  productAltars.push(productAltar(i));
}

const order = {
  _id: '1',
  shopifyOrderId: '1',
  customerId_encryted: '1',
  customerId: '1',
  orderEmail: 'tester@test.com',
  orderFirstname: 'Tester',
  orderLastname: 'Tester',
  orderProducts: [
    {
      _id: '1',
      productPermalink: '1',
      shopifyProductId: '1',
      udid: 'udid',
      productStore: 'MightyJaxx',
      productModelNumber: '1',
      productVariantId: '1',
      productVariantSku: 'SKU-1',
      productTitle: 'productTitle',
      productPrice: '0.00',
      productDescription: 'productDescription',
      productTags: 'shoelace-exclude',
      productOptions: '',
      productImageUrl: 'productImageUrl',
    },
  ],
  udid: 'udid',
};

module.exports = { productAltar, productAltars, order };
