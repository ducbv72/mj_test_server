const sendEmailToManufacture = [
  {
    _id: '611420c4ebeb531151dfdd61',
    adminId: '5cbcade2a266285daed56d67',
    creator: 'admin@mj.com',
    arraySku: [
      {
        id: 'JFMAOZ',
        sku: 'JFMAOZ',
        total: 69,
        pending: 38,
        send: 8,
        completed: 23,
        empty: 0,
      },
    ],
    isSent: true,
    emailManufacture: 'dovuthanh2018@gmail.com',
    subject: 'testing item',
    emailCC: 'dovuthanh2018@gmail.com',
    content: '{ZIP_FILE_DOWNLOAD_URL} ',
    createdAt: '2021-08-11T19:11:00.650+0000',
    updatedAt: '2021-08-11T19:11:00.650+0000',
    downloadUrl:
      'https://file.staging.mightyjaxx.technology/admin/download/email_1628715121925.zip',
    fileName: 'email_1628715121925.zip',
    status: 'success',
    urlZipFileS3:
      'https://udid-request.s3.ap-southeast-1.amazonaws.com/files/email_1628715121925.zip',
  },
];

module.exports = { sendEmailToManufacture };
