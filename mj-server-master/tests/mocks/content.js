const artist = (id) => ({
  _id: id,
  artistName: 'Tester',
  artistImage: 'imageUrl',
  articleId: '1',
});
const artists = [artist(1), artist(2)];

module.exports = { artists, artist };
