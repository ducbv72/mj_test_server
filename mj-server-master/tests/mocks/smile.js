const listProduct = {
  points_products: [
    {
      reward: {
        value: 1,
      },
    },
    {
      reward: {
        value: null,
      },
    },
  ],
};

const smileInfo = (id) => {
  return { customers: [{ vip_tier_id: id }] };
};

const updateEmail = {
  email: 'diepnn4@abcsoft.vn',
  firstName: 'tester',
  lastName: 'test',
  customerId: '5148595290246',
  userName: 'sonnguyenhoang2312@gmail.com',
  userNameLower: 'sonnguyenhoang2312@gmail.com',
};

const getInfoShopify = [
  {
    id: 1,
    email: 'tester@test.vn',
    accepts_marketing: true,
    first_name: 'tester',
    last_name: 'test',
    orders_count: 0,
    state: 'enabled',
    total_spent: '0.00',
    tags: 'tags, tag',
    admin_graphql_api_id: 'gid://shopify/Customer/5148595290246',
  },
];

const points_purchase = {
  id: 1,
  customer_id: '1',
  points_product_id: 1,
  points_spent: 1,
  created_at: '2020-06-05T11:32:05.545Z',
  updated_at: '2020-06-05T11:32:05.545Z',
  fulfilled_reward: {
    id: 1,
    name: '$5 off',
    image_url: 'image_url',
    source_description: 'Spent 1 Mighty Coins',
    code: '1',
    created_at: '2020-06-05T11:32:05.537Z',
    updated_at: '2020-06-05T11:32:05.537Z',
    used_at: null,
  },
  reward_fulfillment: {
    id: 1,
    name: '$5 off',
    image_url: 'image_url',
    source_description: 'Spent 1 Mighty Coins',
    code: '1',
    created_at: '2020-06-05T11:32:05.537Z',
    updated_at: '2020-06-05T11:32:05.537Z',
    used_at: null,
  },
};

const updateEmailConstant = {
  updateEmail,
  getInfoShopify,
  points_purchase,
};

const customerSmileIo = {
  customers: [
    {
      id: 1,
      first_name: 'Test',
      last_name: 'Test',
      email: 'tester@test.com',
      date_of_birth: null,
      points_balance: 10,
      referral_url: 'http://i.refs.cc/4gsEA67p',
      state: 'member',
      vip_tier_id: 1,
      vip_tier_expires_at: '2020-12-31T23:59:59.999Z',
      vip_tier_expires_at_formatted: 'December 31, 2020',
      vip_metric: '30',
      created_at: '2020-05-05T10:27:59.256Z',
      updated_at: '2020-08-02T03:13:57.738Z',
      state_changed_at: '2020-05-05T10:27:59.255Z',
      account_id: 1,
      date_of_birth_updated_at: null,
      authentication_token: 'authentication_token',
      vip_tier: {
        id: 1,
        account_id: 1,
        reward_program_id: 1,
        name: 'Captain',
        range_minimum: 30000,
        milestone: 30000,
        image_url: 'image_url',
        image_svg: null,
        customized_image_url: 'image_url',
        milestone_description: 'Earn 30,000 Mighty Coins',
        members_count: 1,
        created_at: '2010-02-26T03:55:17.235Z',
        updated_at: '2020-07-19T08:32:40.437Z',
      },
    },
  ],
};

const reward_programs = {
  reward_programs: [
    {
      id: 1,
      account_id: 1,
      reward_program_definition_id: 1,
      type: 'points',
      name: 'Points',
      is_enabled: true,
      launched_at: '2018-02-21T01:58:48.671Z',
      last_enabled_at: '2018-03-02T04:54:16.678Z',
      requires_upgrade_to_enable: false,
      created_at: '2018-02-21T01:58:24.137Z',
      updated_at: '2021-07-28T15:43:16.919Z',
      points_label_singular: 'Mighty Coin',
      customized_points_label_singular: 'Mighty Coin',
      default_points_label_singular: 'Point',
      points_label_plural: 'Mighty Coins',
      customized_points_label_plural: 'Mighty Coins',
      default_points_label_plural: 'Points',
      points_expiry_is_installed: true,
      points_expiry_is_enabled: true,
      points_expiry_inactive_floor_date: '2021-07-28T15:43:16.914+00:00',
      points_expiry_interval: 'days',
      points_expiry_interval_count: 180,
      requires_upgrade_to_enable_points_expiry: false,
      points_expiry_is_available: true,
    },
  ],
};
const pointTransaction = {
  points_transactions: [
    {
      id: 1,
      customer_id: 1,
      description: 'Shared on Facebook',
      points_change: 100,
      internal_note: null,
      created_at: '2021-12-08T02:34:38.645Z',
      updated_at: '2021-12-08T02:34:38.645Z',
    },
    {
      id: 2,
      customer_id: 1,
      description: 'Celebrated a birthday',
      points_change: 500,
      internal_note: null,
      created_at: '2021-11-08T06:07:01.860Z',
      updated_at: '2021-11-08T06:07:01.860Z',
    },
    {
      id: 3,
      customer_id: 1,
      description: 'Shared on Facebook',
      points_change: 100,
      internal_note: null,
      created_at: '2021-11-04T14:17:17.838Z',
      updated_at: '2021-11-04T14:17:17.838Z',
    },
    {
      id: 4,
      customer_id: 1,
      description: 'Points spent on $5 off',
      points_change: -1000,
      internal_note: null,
      created_at: '2021-10-21T11:06:37.056Z',
      updated_at: '2021-10-21T11:06:37.056Z',
    },
  ],
  metadata: {
    next_cursor: 'next_cursor',
    previous_cursor: null,
  },
};
module.exports = {
  listProduct,
  smileInfo,
  updateEmailConstant,
  points_purchase,
  customerSmileIo,
  reward_programs,
  pointTransaction,
};
