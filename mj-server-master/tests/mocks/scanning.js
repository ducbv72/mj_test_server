const { ordersConstant, orderProduct, product } = require('./E-Commerce');
const { order } = ordersConstant;
const { campaign, campaignParticipates } = require('./vaultFeatures');

const order_id = 3;
const user = {
  _id: `${order_id}`,
  email: 'tester2@test.vn',
  firstName: 'tester',
  lastName: 'test',
  customerId: `${order_id}`,
  userName: 'Tester',
  userNameLower: 'test',
  phone: null,
  password: '$2a$10$0RkbU9OyH8nqX0GSKe.t4OgBdsgMU.n7CYYaEQj0VYb1nq4nChRtO',
  feedPrivate: false,
  vaultPrivate: false,
};

const getInfoShopify = [
  {
    id: 2,
    email: 'tester2@test.vn',
    accepts_marketing: true,
    first_name: 'tester',
    last_name: 'test',
    orders_count: 0,
    state: 'enabled',
    total_spent: '0.00',
    tags: 'tags, tag',
    admin_graphql_api_id: 'gid://shopify/Customer/5148595290246',
  },
];

const files = {
  file: {
    fieldname: 'files',
    originalname: 'image.jpg',
    encoding: '7bit',
    mimetype: 'image/jpeg',
    destination: 'destination/',
    filename: 'filename',
    path: 'file_path',
    size: 475,
  },
};
const fields = {
  customer_id: '1',
  encrypted: 'encrytedMessage',
};

const newOrder = order(order_id);
newOrder.orderProducts = [orderProduct(1), orderProduct(2)];

const accountShopify = {
  accounts: [{ name: 'Mighty Jaxx', id: 1 }],
};

const customerShopify = {
  customers: [{ id: 1 }],
};

const shopifyProductVariants = {
  productVariants: {
    edges: [
      {
        node: {
          price: 1,
          product: {
            tags: ['F12021', 'tag'],
          },
        },
      },
    ],
  },
};
const qrcode = {
  _id: '1',
  shopifyProductId: '1',
  productId: '1',
  productVariantId: '1',
  productVariantSku: 'SKU-1',
  indexItem: 8,
  encrytedMessage: 'encrytedMessage',
  productImage: 'productImage',
  qrcodeImage: null,
  nfcUdid: null,
  password: null,
  udidCount: 0,
  randomString: 'AAA7a',
  udid: 'udid',
};

const orderWithSameId = order(1);

module.exports = {
  user,
  newOrder,
  getInfoShopify,
  order_id,
  files,
  fields,
  accountShopify,
  customerShopify,
  shopifyProductVariants,
  qrcode,
  campaign,
  campaignParticipates,
  product,
  orderWithSameId,
};
