const user = {
  _id: 1,
  email: 'tester@test.vn',
  firstName: 'tester',
  lastName: 'test',
  customerId: '1',
  userName: 'Tester',
  userNameLower: 'test',
  phone: null,
  password: '$2a$10$0RkbU9OyH8nqX0GSKe.t4OgBdsgMU.n7CYYaEQj0VYb1nq4nChRtO',
  feedPrivate: false,
  vaultPrivate: false,
};
const userSetting = {
  _id: 1,
  customerId: '1',
  settingName: 'library.list.sorting',
  value: 'value',
};

const campaign = {
  name: 'Scan to Win!',
  description: '',
  url: '',
  start_date: '2020-20-20T20:02:20.000+0000',
  end_date: '2020-22-20T20:20:00.000+0000',
  winner: '',
  id: 1,
  isDisable: false,
  max_number_of_scans: 32,
};

const campaignParticipates = {
  _id: '1',
  campaign_id: 1,
  toy_id: 'SKU-1',
  udid: 'uuid',
  customerLocalId: '1',
  customerId: '1',
  scan_date: '31-01-2021',
  createdAt: 1645105557329,
  scanCount: 1,
};

const digitalContent = (isTrueSKU) => ({
  _id: '1',
  digitalContentType: 'test',
  toyId: '1',
  toyName: 'Test',
  productSku: isTrueSKU ? 'SKU-1' : 'NOT SKU',
  isDisable: null,
  createdAt: '2020-20-15T04:28:29.979+0000',
});

const qrcode = (isCount = true) => ({
  _id: '1',
  shopifyProductId: '1',
  productId: '1',
  productVariantId: '1',
  productVariantSku: 'SKU-1',
  indexItem: 1,
  encrytedMessage: 'encrytedMessage',
  productImage: 'productImage',
  qrcodeImage: null,
  nfcUdid: null,
  password: null,
  udidCount: isCount ? 1 : 0,
  randomString: 'oFPxU',
  udid: 'valid_udid',
});
module.exports = {
  user,
  userSetting,
  campaign,
  campaignParticipates,
  digitalContent,
  qrcode,
};
