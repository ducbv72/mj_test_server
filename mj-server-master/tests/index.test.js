/**
 * @describe Define all dependence for test environment
 */
const fs = require('fs');
const { MongoClient } = require('mongodb');
const app = require('../mj_admin_dashboard');
const config = require('../config');
const common = require('../lib/common');
const request = require('supertest');
const agent = request.agent(app);
const jwt = require('jsonwebtoken');
const path = require('path');
const nock = require('nock');

/**
 * @describe Define mock data
 */
const rawSettingTestData = fs.readFileSync(
  path.join(process.cwd(), 'config/settings.json'),
  'utf-8',
);
const setting = JSON.parse(rawSettingTestData);

/**
 * @describe Define test file
 */
const Smile = require('./Suite/Smile');
const SocialFeed = require('./Suite/Social-feed');
const Account = require('./Suite/Account');
const ProductModule = require('./Suite/Products');
const Website = require('./Suite/Website');
const ProductAltar = require('./Suite/ProductAltar');
const SocialSignIn = require('./Suite/SocialSignin');
const Home = require('./Suite/Home');
const FileManagement = require('./Suite/FileManagement');
const Artist = require('./Suite/Artist');
const Campaigns = require('./Suite/Campaigns');
const Vip = require('./Suite/Vip');
const Community = require('./Suite/Community');
const ExclusiveSale = require('./Suite/Exclusive-sale');
const StoreModule = require('./Suite/Stores');

/**
 * @describe Define global variable
 */
let db;
let connection;
const token = jwt.sign(
  {
    email: 'tester@test.com',
    customerId: 1,
    shopifyToken: 'access_token',
  },
  setting.jwtSecret,
);

const spyFindCustomer = () => {
  jest.spyOn(app.db.customers, 'find').mockImplementation((arg) => ({
    toArray: () => [
      {
        _id: 1,
        email: 'tester@test.vn',
        firstName: 'tester',
        lastName: 'test',
        customerId: '1',
        userName: 'Tester',
        userNameLower: 'test',
        tags: 'exclusive-mobile-nyansum',
        phone: null,
        password:
          '$2a$10$0RkbU9OyH8nqX0GSKe.t4OgBdsgMU.n7CYYaEQj0VYb1nq4nChRtO',
        feedPrivate: false,
        vaultPrivate: false,
      },
    ],
  }));
};
//Initial setup for all test file
beforeAll(async () => {
  connection = await MongoClient.connect(
    process.env.MONGODB_TESTING_CONNECTION,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  );
  db = await connection.db('testingdb');
  common.dbInitCollection(db);
  app.dbClient = connection;
  app.db = db;
  app.token = token;
});
beforeEach(() => {
  jest.clearAllMocks();
  nock.cleanAll();
  spyFindCustomer();
});
afterEach(() => {
  jest.clearAllMocks();
  nock.cleanAll();
});
// Remove all file when test completed
afterAll((done) => {
  fs.unlinkSync('globalConfig.json');
  fs.unlinkSync('.env');
  connection.close(done);
});

/************** Test *****************/
/************** Test *****************/
/************** Test *****************/

describe('Account module', () => {
  Account(agent, app);
});

describe('Artist module', () => {
  Artist(agent, app);
});

describe('Campaigns module', () => {
  Campaigns(agent, app);
});

describe('Community module', () => {
  Community(agent, app);
});

describe('Exclusive Sale module', () => {
  ExclusiveSale(agent, app);
});

describe('Home module', () => {
  Home(agent, app);
});

describe('File management module', () => {
  FileManagement(agent, app);
});

describe('Social Sign In', () => {
  SocialSignIn(agent, app);
});

describe('Product Module', () => {
  ProductModule(agent, app);
});

describe('Testing Smile Vip', () => {
  Smile(agent, app);
});

describe('Testing Website', () => {
  Website(agent, config, app);
});

describe('Product Altar', () => {
  ProductAltar(agent, app);
});

describe('Stores Module', () => {
  StoreModule(agent, app);
});

describe('Social Feed', () => {
  SocialFeed(agent, app);
});
describe('Vip Module', () => {
  Vip(agent, app);
});
