const { productAltars, order } = require('../../mocks/productAltar');
const deleteProductAltarById = (agent, app, fn) => {
  const {
    spyOrdersAggregate,
    spyOrdersFindOne,
    spyOrdersUpdateOne,
    spyOrdersFindOneError,
  } = fn;

  test('should return success response', async () => {
    const order_id = '602e465f0fc90e42b1b6f294';
    spyOrdersAggregate(productAltars);
    spyOrdersFindOne(order);
    spyOrdersUpdateOne(order);
    const res = await agent
      .delete(`/api/v4/products/altar/${order_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.msg).toBe('Success');
    expect(res.body.data).toBeObject();
  });

  test('should return error with message: "Not found"', async () => {
    const order_id = '602e465f0fc90e42b1b6f294';
    spyOrdersFindOne(null);
    const res = await agent
      .delete(`/api/v4/products/altar/${order_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Not found');
  });

  test('should return catch error', async () => {
    const order_id = '602e465f0fc90e42b1b6f294';
    spyOrdersFindOneError();
    const res = await agent
      .delete(`/api/v4/products/altar/${order_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.msg).toBe('Test Catch Error');
  });
};
module.exports = deleteProductAltarById;
