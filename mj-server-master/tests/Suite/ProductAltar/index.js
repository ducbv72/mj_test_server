const GetProductsAltar = require('./getProductsAltar');
const DeleteProductAltarById = require('./deleteProductAltarById');

const ProductAltar = (agent, app) => {
  const spyOrdersAggregate = (data) => {
    jest.spyOn(app.db.orders, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyOrdersAggregateError = () => {
    jest.spyOn(app.db.orders, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => Promise.reject(new Error('Test Catch Error')),
      };
    });
  };

  const spyOrdersFindOne = (data, id, otherData) => {
    jest.spyOn(app.db.orders, 'findOne').mockImplementation((arg) => {
      if (arg._id.toString() === id) {
        return otherData;
      }
      return data;
    });
  };

  const spyOrdersUpdateOne = (data) => {
    jest.spyOn(app.db.orders, 'updateOne').mockImplementation(() => {
      return data;
    });
  };

  //Get products altar
  //GET /api/v4/products/altar
  describe('Get products altar', () => {
    GetProductsAltar(agent, app, spyOrdersAggregate);
  });
};

module.exports = ProductAltar;
