const { productAltars } = require('../../mocks/productAltar');
const getProductsAltar = (agent, app, fn) => {
  test('should return success response', async () => {
    fn(productAltars);
    const res = await agent
      .get('/api/v4/products/altar')
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Success');
    expect(res.body.data).toBeObject();
  });

  test('should return success response (case query type = card)', async () => {
    fn(productAltars);
    const res = await agent
      .get('/api/v4/products/altar?type=card')
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Success');
    expect(res.body.data).toBeObject();
  });

  test('should return catch error', async () => {
    fn(null);
    const res = await agent
      .get('/api/v4/products/altar')
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
  });
};

module.exports = getProductsAltar;
