const { userFollow, listPersons } = require('../../mocks/social-feed');

const listPerson = (agent, app, fn) => {
  const { spyCustomersFindOne, spyCustomersFindOneError, spyLikersAggregate } =
    fn;
  const username = 'Tester';
  const failUsername = 'Failed';

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/feed/${username}/posts/following`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    spyCustomersFindOne(userFollow);
    spyLikersAggregate(listPersons);
    const res = await agent
      .get(`/api/v4/feed/${username}/posts/following?limit=10`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
  });

  it('It should return invalid type', async () => {
    const res = await agent
      .get(`/api/v4/feed/${username}/posts/following?limit=a`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(412);
    expect(res.body.message).toBeString().toEqual('Validation failed');
  });

  it('It should return error with message: "Test Catch Error"', async () => {
    spyCustomersFindOneError();
    const res = await agent
      .get(`/api/v4/feed/${username}/posts/following?limit=10`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.error.message).toBeString().toEqual('Test Catch Error');
  });

  it('It should return user not found', async () => {
    spyCustomersFindOne(null);
    const res = await agent
      .get(`/api/v4/feed/${failUsername}/posts/following?limit=10`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.message).toBeString().toEqual('User not found');
  });
};
module.exports = listPerson;
