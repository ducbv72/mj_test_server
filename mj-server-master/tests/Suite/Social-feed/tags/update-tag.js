const updateTag = (agent, app, fn) => {
  const { spyTagsFindOneAndUpdate } = fn;
  const testId = '216eb7604d6f8f3578916094'; //ObjectId generate
  const name = '#Test';
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.put(`/api/v4/feed/tags/${testId}`);
    expect(res.status).toEqual(401);
  });

  // send the token - should respond with a 200
  it('It response with a updated tag', async () => {
    spyTagsFindOneAndUpdate({ _id: testId, name });
    const res = await agent
      .put(`/api/v4/feed/tags/${testId}`)
      .auth(app.token, { type: 'bearer' })
      .send({ name });

    expect(res.body.code).toEqual(200);
    expect(res.body.data.name).toBeString().toEqual(`${name}`);
  });

  // error: `The name cannot be empty or more than 40 characters long`
  it('Should return tag name error (case name length = 0)', async () => {
    const res = await agent
      .put(`/api/v4/feed/tags/${testId}`)
      .auth(app.token, { type: 'bearer' })
      .send({ name: '' });
    expect(res.status).toEqual(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('The name cannot be empty or more than 40 characters long');
  });

  // error: `The name cannot be empty or more than 40 characters long`
  it('Should return tag name error (case not send name)', async () => {
    const res = await agent
      .put(`/api/v4/feed/tags/${testId}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('The name cannot be empty or more than 40 characters long');
  });

  // error: `The name cannot be empty or more than 40 characters long`
  it('Should return tag name error (case length > 40 cchars)', async () => {
    const name = '#RYtM0B7AJdHyfYKfboxgZH0MX07wpXboT4FHfMQ5';
    const res = await agent
      .put(`/api/v4/feed/tags/${testId}`)
      .auth(app.token, { type: 'bearer' })
      .send({ name });

    expect(res.status).toEqual(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('The name cannot be empty or more than 40 characters long');
  });

  //error: Should return hashtag contain special character error
  it('Should return hashtag contain special character error', async () => {
    const name = '#has$_tag';
    const res = await agent
      .put(`/api/v4/feed/tags/${testId}`)
      .auth(app.token, { type: 'bearer' })
      .send({ name });
    expect(res.status).toEqual(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('Hashtag will only include letters, numbers & underscores.');
  });

  //error: Should return update fail
  it('Should return update fail', async () => {
    spyTagsFindOneAndUpdate(null);
    const res = await agent
      .put(`/api/v4/feed/tags/6066c29b8fec73064df49a1b`)
      .auth(app.token, { type: 'bearer' })
      .send({ name });
    expect(res.status).toEqual(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('Update failed, something wrong or item was not be found');
  });
};

module.exports = updateTag;
