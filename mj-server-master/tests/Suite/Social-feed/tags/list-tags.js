const { tags, searchTags } = require('../../../mocks/social-feed').tagsInstance;
const listTags = (agent, app, fn) => {
  const { spyTagsAggregate } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get('/api/v4/feed/tags');
    expect(res.status).toEqual(401);
  });

  // send the token - should respond with a 200
  it('It response with JSON', async () => {
    spyTagsAggregate(tags);
    const res = await agent
      .get('/api/v4/feed/tags')
      .auth(app.token, { type: 'bearer' });

    expect(res.status).toEqual(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data.tags).toBeArray();
  });

  it('It should return search with tags', async () => {
    spyTagsAggregate(searchTags);
    const searchKey = 'a';
    const res = await agent
      .get(`/api/v4/feed/tags?limit=${searchTags.length}&search=${searchKey}`)
      .auth(app.token, { type: 'bearer' });

    expect(res.status).toEqual(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data.tags).toBeArray();
    expect(res.body.data.tags.length).toBeLessThanOrEqual(searchTags.length);
  });

  it('It should return limit tags', async () => {
    spyTagsAggregate(tags);
    const res = await agent
      .get(`/api/v4/feed/tags?limit=${tags.length}`)
      .auth(app.token, { type: 'bearer' });

    expect(res.status).toEqual(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data.tags).toBeArray();
    expect(res.body.data.tags.length).toBeLessThanOrEqual(tags.length);
  });

  it('It should return fail when limit is not a number', async () => {
    spyTagsAggregate(tags);
    const res = await agent
      .get('/api/v4/feed/tags?limit=a')
      .auth(app.token, { type: 'bearer' });

    expect(res.status).toEqual(412);
    expect(res.body.message).toBeString().toEqual('Validation failed');
  });
};

module.exports = listTags;
