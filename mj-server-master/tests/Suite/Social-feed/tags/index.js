const CreateTag = require('./create-tag');
const ListTags = require('./list-tags');
const SpecificTag = require('./specific-tag');
const UpdateTag = require('./update-tag');
const DeleteTag = require('./delete-tag');

const tags = (agent, app) => {
  const spyTagsInsertOne = (data) => {
    jest.spyOn(app.db.tags, 'insertOne').mockImplementationOnce(() => data);
  };

  const spyTagsFindOne = (data) => {
    jest.spyOn(app.db.tags, 'findOne').mockImplementationOnce(() => data);
  };

  const spyTagsDeleteOne = (data) => {
    jest.spyOn(app.db.tags, 'deleteOne').mockImplementationOnce(() => data);
  };

  const spyTagsFindOneAndUpdate = (data) => {
    jest
      .spyOn(app.db.tags, 'findOneAndUpdate')
      .mockImplementationOnce(() => ({ value: data }));
  };

  const spyTagsAggregate = (data) => {
    jest.spyOn(app.db.tags, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };
  //Create new tag
  //POST /api/v4/feed/tags
  describe('Create new tag', () => {
    CreateTag(agent, app, { spyTagsInsertOne });
  });

  //Get all tags
  //GET /api/v4/feed/tags
  describe('Get all tags', () => {
    ListTags(agent, app, { spyTagsAggregate });
  });

  //Get tag by id
  //GET /api/v4/feed/tags/:id
  describe(`Get tag by id`, () => {
    SpecificTag(agent, app, { spyTagsFindOne });
  });

  //Update specific tag
  //PUT /api/v4/feed/tags/:id
  describe(`Update specific tag`, () => {
    UpdateTag(agent, app, { spyTagsFindOneAndUpdate });
  });

  //Delete a specific tag
  //DELETE /api/v4/feed/tags/:id
  describe(`Delete a specific tag`, () => {
    DeleteTag(agent, app, { spyTagsDeleteOne });
  });
};

module.exports = tags;
