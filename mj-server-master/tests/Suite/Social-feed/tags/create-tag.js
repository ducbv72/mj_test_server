const createTag = (agent, app, fn) => {
  const { spyTagsInsertOne } = fn;
  const name = '#Test';
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post('/api/v4/feed/tags');
    expect(res.status).toEqual(401);
  });

  // send the token - should respond with a 200
  it('It response with new tag', async () => {
    spyTagsInsertOne({ ops: [{ name, _id: '1' }] });
    const res = await agent
      .post('/api/v4/feed/tags')
      .auth(app.token, { type: 'bearer' })
      .send({ name });
    expect(res.status).toEqual(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(1);
  });

  // error: `The name cannot be empty or more than 40 characters long`
  it('Should return tag name error', async () => {
    const res = await agent
      .post(`/api/v4/feed/tags`)
      .auth(app.token, { type: 'bearer' })
      .send({ name: '' });
    expect(res.status).toEqual(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('The name cannot be empty or more than 40 characters long');
  });

  // error: `The name cannot be empty or more than 40 characters long`
  it('Should return tag name error', async () => {
    const res = await agent
      .post(`/api/v4/feed/tags`)
      .auth(app.token, { type: 'bearer' });

    expect(res.status).toEqual(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('The name cannot be empty or more than 40 characters long');
  });

  // error: `The name cannot be empty or more than 40 characters long`
  it('Should return tag name error', async () => {
    const name = '#RYtM0B7AJdHyfYKfboxgZH0MX07wpXboT4FHfMQ5';
    const res = await agent
      .post(`/api/v4/feed/tags`)
      .auth(app.token, { type: 'bearer' })
      .send({ name });

    expect(res.status).toEqual(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('The name cannot be empty or more than 40 characters long');
  });

  //error: Should return hashtag contain special character error
  it('Should return error when tag contain special character', async () => {
    const name = '#has$_tag';
    const res = await agent
      .post('/api/v4/feed/tags')
      .auth(app.token, { type: 'bearer' })
      .send({ name });

    expect(res.status).toEqual(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message)
      .toBeString()
      .toEqual('Hashtag will only include letters, numbers & underscores.');
  });
};

module.exports = createTag;
