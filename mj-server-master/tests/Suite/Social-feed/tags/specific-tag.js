const { tags } = require('../../../mocks/social-feed').tagsInstance;

const specificTag = (agent, app, fn) => {
  const { spyTagsFindOne } = fn;
  const testId = '216eb7604d6f8f3578916094';
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/feed/tags/${testId}`);

    expect(res.status).toEqual(401);
  });

  // send the token - should respond with a 200
  it('It response with a tag', async () => {
    spyTagsFindOne(tags[0]);
    const res = await agent
      .get(`/api/v4/feed/tags/${testId}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.body.code).toEqual(200);
    expect(res.body.item._id).toBeString().toEqual(`${tags[0]._id}`);
  });

  it('It response with error of id type', async () => {
    const res = await agent
      .get(`/api/v4/feed/tags/1234`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.error.message).toEqual(
      'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters',
    );
  });

  it('It response with message: "Item not found"', async () => {
    spyTagsFindOne(null);
    const res = await agent
      .get(`/api/v4/feed/tags/6066c29b8fec73064df49a1b`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.message).toEqual('Item not found');
  });
};

module.exports = specificTag;
