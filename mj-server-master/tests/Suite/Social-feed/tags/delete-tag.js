const deleteTag = (agent, app, fn) => {
  const { spyTagsDeleteOne } = fn;
  const testId = '216eb7604d6f8f3578916094'; //ObjectId generate
  const result = (n) => ({ result: { n: 1 } });
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.delete(`/api/v4/feed/tags/${testId}`);

    expect(res.status).toEqual(401);
  });

  // Catch err
  it('It response with error of id type', async () => {
    const res = await agent
      .delete(`/api/v4/feed/tags/1234`)
      .auth(app.token, { type: 'bearer' });

    expect(res.status).toBe(400);
    expect(res.body.error.message).toEqual(
      'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters',
    );
  });

  // Item not found
  it('It response with message: "Item not found"', async () => {
    spyTagsDeleteOne(result(false));
    const res = await agent
      .delete(`/api/v4/feed/tags/6066c29b8fec73064df49a1b`)
      .auth(app.token, { type: 'bearer' });

    expect(res.status).toBe(400);
    expect(res.body.message).toEqual('Cannot found this tag');
  });

  // Success delete item
  it('It response with a tag', async () => {
    spyTagsDeleteOne(result(true));
    const res = await agent
      .delete(`/api/v4/feed/tags/${testId}`)
      .auth(app.token, { type: 'bearer' });

    expect(res.body.code).toEqual(200);
    expect(res.body.message).toBeString().toEqual(`Deleted`);
  });
};
module.exports = deleteTag;
