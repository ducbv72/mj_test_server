const {
  blogFromShopify,
  articleFromShopify,
} = require('../../mocks/social-feed');

const listArtist = (agent, app, fn) => {
  const { spyShopifyBlog, spyShopifyArticle, spyShopifyBlogError } = fn;
  const limit = 10;
  const since_id = 'since_id'; //next page in shopify-api-node lib

  it('It should return artist', async () => {
    spyShopifyBlog(blogFromShopify('artists'));
    spyShopifyArticle(articleFromShopify(limit));
    const res = await agent.get(`/api/v4/feed/artists?limit=${limit}`);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeArray();
    expect(res.body.data.length).toBeLessThanOrEqual(limit);
  });

  it('It should return next page', async () => {
    spyShopifyBlog(blogFromShopify('artists'));
    spyShopifyArticle(articleFromShopify(limit));
    const res = await agent.get(
      `/api/v4/feed/artists?limit=${limit}&since_id=${since_id}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeArray();
    expect(res.body.data.length).toBeLessThanOrEqual(10);
  });

  it('Should return error', async () => {
    spyShopifyBlog([]);
    const res = await agent.get(`/api/v4/feed/artists?limit=${limit}`);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray();
    expect(res.body.data.length).toBe(0);
  });

  it('It should return invalid type', async () => {
    const res = await agent.get(`/api/v4/feed/artists?limit=a`);
    expect(res.status).toBe(412);
    expect(res.body.message).toBeString().toEqual('Validation failed');
  });

  it('It should return catch error', async () => {
    spyShopifyBlogError();
    const res = await agent.get(`/api/v4/feed/artists`);
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
    expect(res.body.message).toBeString().toEqual('Internal Error Server');
  });
};
module.exports = listArtist;
