const { user, post } = require('../../mocks/social-feed');
const reportPost = (agent, app, fn) => {
  const {
    spyCustomersFindOne,
    spyReportsInsert,
    spyPostsAggregate,
    spyReportsInsertError,
  } = fn;
  const body = {
    post_id: 1,
    image_post: 'image_post_img',
    username: 'Tester',
  };
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post('/api/v4/feed/reports');
    expect(res.status).toEqual(401);
  });

  // Cannot found user
  it('It should return error with message: "User not found"', async () => {
    spyCustomersFindOne(null);
    const res = await agent
      .post('/api/v4/feed/reports')
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toEqual(400);
    expect(res.body.message).toBeString().toEqual('User not found');
  });

  // Report not found post
  it('It should return error with message: "Not found"', async () => {
    spyCustomersFindOne(user);
    spyPostsAggregate([null]);
    const res = await agent
      .post('/api/v4/feed/reports')
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toEqual(400);
    expect(res.body.error.message).toBeString().toEqual('Not found');
  });

  // Report success
  it('It should return success response', async () => {
    spyCustomersFindOne(user);
    spyPostsAggregate([post]);
    spyReportsInsert(post);
    const res = await agent
      .post('/api/v4/feed/reports')
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toEqual(200);
    expect(res.body.data).toBeString().toEqual('Reported');
  });

  it('It should return error with message: "Test Catch Error"', async () => {
    spyCustomersFindOne(user);
    spyPostsAggregate([post]);
    spyReportsInsertError();
    const res = await agent
      .post('/api/v4/feed/reports')
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(400);
    expect(res.body.error.message).toBeString().toEqual('Test Catch Error');
  });
};
module.exports = reportPost;
