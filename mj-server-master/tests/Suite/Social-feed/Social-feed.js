/* eslint-disable no-undef */
const SocialFeed = (agent, mockData, app) => {
  //Create new post
  //POST /api/v4/feed/posts
  describe(`Create new post`, () => {
    // token not being sent - should respond with a 401
    it('It should require authorization', async () => {
      const res = await agent.post('/api/v4/feed/posts');
      expect(res.status).toEqual(401);
    });

    // Should return new post, case: normal jpg
    it('It should return new post', async () => {
      const filePath = `${process.cwd()}\\test\\image\\anh-dep-sapa-46.jpg`;

      const checkFile = fs.existsSync(filePath);
      if (!checkFile) {
        throw new Error('file does not exist');
      }
      const res = await agent
        .post('/api/v4/feed/posts')
        .auth(app.token, { type: 'bearer' })
        .attach('files', filePath)
        .field('caption', mockData.posts.newPost.data.caption)
        .field('user_tag', mockData.posts.newPost.data.user_tag);

      expect(res.status).toBe(200);
      expect(res.body.statusCode).toBe(200);
      expect(res.body.data[0].caption).toBe(
        mockData.posts.newPost.data.caption,
      );
      expect(res.body.data[0].user_tag).toBe(
        mockData.posts.newPost.data.user_tag,
      );
    }, 15000);

    // Should return new post, case heic
    // it('It should return new post when upload heic file', async () => {
    //   const filePath = `${process.cwd()}\\test\\image\\image1.heic`;

    //   const checkFile = fs.existsSync(filePath);
    //   if (!checkFile) {
    //     throw new Error('file does not exist');
    //   }
    //   const res = await agent
    //     .post('/api/v4/feed/posts')
    //     .auth(app.token, { type: 'bearer' })
    //     .attach('files', filePath)
    //     .field('caption', mockData.posts.newPost.data.caption)
    //     .field('user_tag', mockData.posts.newPost.data.user_tag);

    //   expect(res.status).toBe(200);
    //   expect(res.body.statusCode).toBe(200);
    //   expect(res.body.data[0].caption).toBe(
    //     mockData.posts.newPost.data.caption,
    //   );
    //   expect(res.body.data[0].user_tag).toBe(
    //     mockData.posts.newPost.data.user_tag,
    //   );
    // }, 50000);

    // Should return error params required
    it('It should return params required error', async () => {
      const filePath = `${process.cwd()}\\test\\image\\anh-dep-sapa-46.jpg`;

      const checkFile = fs.existsSync(filePath);
      if (!checkFile) {
        throw new Error('file does not exist');
      }
      const res = await agent
        .post('/api/v4/feed/posts')
        .auth(app.token, { type: 'bearer' })
        .attach('files', filePath)
        .field('caption', mockData.posts.newPost.data.caption);
      expect(res.status).toBe(400);
      expect(res.body.statusCode).toBe(400);
      expect(res.body.error.message)
        .toBeString()
        .toEqual('Required parameter not found');
    });

    // Should return error mime type
    it('It should return mime type not accept', async () => {
      const filePath = `${process.cwd()}\\test\\image\\testExt.doc`;

      const checkFile = fs.existsSync(filePath);
      if (!checkFile) {
        throw new Error('file does not exist');
      }
      const res = await agent
        .post('/api/v4/feed/posts')
        .auth(app.token, { type: 'bearer' })
        .attach('files', filePath)
        .field('caption', mockData.posts.newPost.data.caption)
        .field('user_tag', mockData.posts.newPost.data.user_tag);
      expect(res.status).toBe(415);
      expect(res.body.statusCode).toBe(415);
      expect(res.body.error.message)
        .toBeString()
        .toEqual('File type not allowed. Please try again.');
    });

    // Should return error file type too large
    it('It should return file type too large', async () => {
      const filePath = `${process.cwd()}\\test\\image\\processed.jpeg`;

      const checkFile = fs.existsSync(filePath);
      if (!checkFile) {
        throw new Error('file does not exist');
      }
      const res = await agent
        .post('/api/v4/feed/posts')
        .auth(app.token, { type: 'bearer' })
        .attach('files', filePath)
        .field('caption', mockData.posts.newPost.data.caption)
        .field('user_tag', mockData.posts.newPost.data.user_tag);
      expect(res.status).toBe(413);
      expect(res.body.statusCode).toBe(413);
      expect(res.body.error.message)
        .toBeString()
        .toEqual('File type too large. Please try again.');
    });
  });
};
module.exports = SocialFeed;
