const { listLikers } = require('../../mocks/social-feed');

const getListLikers = (agent, app, fn) => {
  const { spyPostsAggregate, spyPostsAggregateError } = fn;

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get('/api/v4/feed/posts/1/likers');
    expect(res.status).toEqual(401);
  });

  it('It should return users liked', async () => {
    spyPostsAggregate([listLikers]);
    const res = await agent
      .get('/api/v4/feed/posts/9/likers')
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.data.feed).toStrictEqual(listLikers);
  });

  it('It should return error with message: "Test Catch Error"', async () => {
    spyPostsAggregateError();
    const res = await agent
      .get('/api/v4/feed/posts/5/likers')
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.error.message).toEqual('Test Catch Error');
  });
};
module.exports = getListLikers;
