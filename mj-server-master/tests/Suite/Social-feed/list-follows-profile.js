const { userFollow, listFollowing } = require('../../mocks/social-feed');

const followsProfile = (agent, app, fn) => {
  const { spyCustomersFindOne, spyCustomersFindOneError, spyFollowsAggregate } =
    fn;
  const username = 'Tester';
  const failUsername = 'Failed';

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(
      `/api/v4/feed/customer/${username}/follow?limit=10&is_following=true`,
    );
    expect(res.status).toEqual(401);
  });

  //Should return success response
  it('Should return success response', async () => {
    spyCustomersFindOne(userFollow);
    spyFollowsAggregate(listFollowing);
    const res = await agent
      .get(
        `/api/v4/feed/customer/${username}/follow?limit=10&is_following=true`,
      )
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data.feed.username).toEqual(username);
    expect(res.body.data.feed.following).toBeArray();
    expect(res.body.data.feed.following.length).toBeGreaterThan(0);
  });

  it('Should return success response (case is_following = false)', async () => {
    spyCustomersFindOne(userFollow);
    spyFollowsAggregate(listFollowing);
    const res = await agent
      .get(
        `/api/v4/feed/customer/${username}/follow?limit=10&is_following=false`,
      )
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data.feed.username).toEqual(username);
    expect(res.body.data.feed.followed).toBeArray();
  });

  it('Should return error with message: "Required parameter not found"', async () => {
    spyCustomersFindOne(userFollow);
    const res = await agent
      .get(
        `/api/v4/feed/customer/${username}/follow?limit=10&is_following=string`,
      )
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.error.message).toEqual('Required parameter not found');
  });

  it('Should return invalid type error', async () => {
    const res = await agent
      .get(
        `/api/v4/feed/customer/${username}/follow?limit=string&is_following=true`,
      )
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(412);
    expect(res.body.message).toEqual('Validation failed');
  });

  it('Should return catch error', async () => {
    spyCustomersFindOneError();
    const res = await agent
      .get(
        `/api/v4/feed/customer/${username}/follow?limit=10&is_following=true`,
      )
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.error.message).toEqual('Test Catch Error');
  });

  it('Should return error with message: "User not found"', async () => {
    spyCustomersFindOne(null);
    const res = await agent
      .get(
        `/api/v4/feed/customer/${failUsername}/follow?limit=10&is_following=true`,
      )
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.message).toEqual('User not found');
  });
};
module.exports = followsProfile;
