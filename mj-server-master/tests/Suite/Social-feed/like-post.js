const { post, liker } = require('../../mocks/social-feed');

const likePost = (agent, app, fn) => {
  const {
    spyPostsFindOne,
    spyLikersFindOne,
    spyLikersInsert,
    spyLikersDeleteOne,
    spyLikersCountDocuments,
    spyPostsFindOneAndUpdate,
    spyPostsFindOneError,
  } = fn;
  const body = {
    like: true,
    user_tag: '#Test',
    caption: 'caption',
  };
  const post_id = 1;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.put(`/api/v4/feed/posts/${post_id}`);
    expect(res.status).toEqual(401);
  });

  it('It should return error with message: "Not found"', async () => {
    spyPostsFindOne(null);
    const res = await agent
      .put(`/api/v4/feed/posts/${post_id}`)
      .auth(app.token, { type: 'bearer' })
      .send({ ...body });
    expect(res.status).toEqual(404);
    expect(res.body.code).toEqual(404);
    expect(res.body.error.message).toBe('Not found');
  });

  it('It should return error with message: "Already exist"', async () => {
    spyPostsFindOne(post);
    spyLikersFindOne(liker);
    const res = await agent
      .put(`/api/v4/feed/posts/${post_id}`)
      .auth(app.token, { type: 'bearer' })
      .send({ ...body });
    expect(res.status).toEqual(404);
    expect(res.body.code).toEqual(404);
    expect(res.body.error.message).toBe('Already exist');
  });

  it('It should return success response', async () => {
    spyPostsFindOne(post);
    spyLikersFindOne(null);
    spyLikersInsert(liker);
    spyLikersCountDocuments(1);
    spyPostsFindOneAndUpdate({ value: post });
    spyLikersFindOne(liker);
    const res = await agent
      .put(`/api/v4/feed/posts/${post_id}`)
      .auth(app.token, { type: 'bearer' })
      .send({ ...body });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeObject();
  });

  it('It should return success response (case like is false)', async () => {
    spyPostsFindOne(post);
    spyLikersFindOne(null);
    spyLikersDeleteOne({});
    spyLikersCountDocuments(1);
    spyPostsFindOneAndUpdate({ value: post });
    spyLikersFindOne(liker);
    const res = await agent
      .put(`/api/v4/feed/posts/${post_id}`)
      .auth(app.token, { type: 'bearer' })
      .send({ like: false });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeObject();
  });

  it('It should return catch error', async () => {
    spyPostsFindOneError()
    const res = await agent
      .put(`/api/v4/feed/posts/${post_id}`)
      .auth(app.token, { type: 'bearer' })
      .send({ ...body });
    expect(res.status).toEqual(400);
    expect(res.body.error.message).toBe('Test Catch Error');
  });

  it('It should return type invalid', async () => {
    const res = await agent
      .put('/api/v4/feed/posts/a')
      .auth(app.token, { type: 'bearer' })
      .send({ ...body });
    expect(res.status).toEqual(412);
    expect(res.body.message).toBeString().toBe('Validation failed');
  });
};
module.exports = likePost;
