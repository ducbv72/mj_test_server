const listVideos = (agent, app, fn) => {
  it('Should return all video', async () => {
    const res = await agent.get(`/api/v4/feed/videos`);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray();
  });
  it('Should return feature video', async () => {
    const res = await agent.get(`/api/v4/feed/videos?type=feature`);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray();
  });
  it('Should return unboxx video', async () => {
    const res = await agent.get(`/api/v4/feed/videos?type=unboxx`);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray();
  });

  it('Should return other video', async () => {
    const res = await agent.get(`/api/v4/feed/videos?type=aaaaa`);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray();
  });
};
module.exports = listVideos;
