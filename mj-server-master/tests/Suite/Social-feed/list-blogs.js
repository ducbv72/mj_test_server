const {
  blogFromShopify,
  articleFromShopify,
} = require('../../mocks/social-feed');

const listBlogs = (agent, app, fn) => {
  const limit = 10;
  const { spyShopifyBlog, spyShopifyArticle, spyShopifyBlogError } = fn;

  it('Should return all blogs', async () => {
    spyShopifyBlog(blogFromShopify('news'));
    spyShopifyArticle(articleFromShopify(limit));
    const res = await agent.get(`/api/v4/feed/blogs`);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray();
    expect(res.body.data.length).toBeLessThanOrEqual(limit);
  });

  it('Should return blogs with paginate', async () => {
    spyShopifyBlog(blogFromShopify('news'));
    spyShopifyArticle(articleFromShopify(limit));
    const res = await agent.get(
      `/api/v4/feed/blogs?limit=${limit}&since_id=since_id`,
    );
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray();
    expect(res.body.data.length).toBeLessThanOrEqual(limit);
  });

  it('Should return error', async () => {
    spyShopifyBlog([]);
    const res = await agent.get(`/api/v4/feed/blogs?limit=${limit}`);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray();
    expect(res.body.data.length).toBe(0);
  });

  it('Should return error validate fail', async () => {
    const res = await agent.get(`/api/v4/feed/blogs?limit=a`);
    expect(res.status).toBe(412);
    expect(res.body.message).toBeString().toEqual('Validation failed');
  });

  it('Should return error bad request', async () => {
    spyShopifyBlogError();
    const res = await agent.get(`/api/v4/feed/blogs?limit=10`);
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
    expect(res.body.message).toBeString().toEqual('Internal Error Server');
  });
};
module.exports = listBlogs;
