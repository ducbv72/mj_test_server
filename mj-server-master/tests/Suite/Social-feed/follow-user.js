const { userFollow, userFollowed } = require('../../mocks/social-feed');
const followUser = (agent, app, fn) => {
  const {
    spyFollowsFindOneAndDelete,
    spyFollowsInsertOne,
    spyCustomersFindOne,
    spyCustomersFindOneError,
  } = fn;

  const result = {
    username: userFollow.userName,
    user_followed: userFollowed.userName,
    following_datetime: '2020-08-05T08:57:52.999Z',
    _id: '210ba8105a2111120f1721bc',
    is_following: true,
  };
  const username = result.username;
  const user_followed = result.user_followed;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.put(
      '/api/v4/feed/sonnguyenhoang2312@gmail.com/follow/DiepNguyen',
    );
    expect(res.status).toEqual(401);
  });

  // Should follow user
  it('It should return json with status 200', async () => {
    spyFollowsFindOneAndDelete(null);
    spyFollowsInsertOne(result);
    spyCustomersFindOne(userFollow);
    spyCustomersFindOne(userFollowed);
    const res = await agent
      .put(`/api/v4/feed/${username}/follow/${user_followed}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data.username).toEqual(username);
    expect(res.body.data.user_followed).toEqual(user_followed);
  });

  it('It should return json with status 200', async () => {
    spyFollowsFindOneAndDelete(result);
    spyFollowsInsertOne(result);
    spyCustomersFindOne(userFollow);
    spyCustomersFindOne(userFollowed);
    const res = await agent
      .put(`/api/v4/feed/${username}/follow/${user_followed}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data.username).toEqual(username);
    expect(res.body.data.user_followed).toEqual(user_followed);
  });

  // Should return user not found
  it('Should return user not found', async () => {
    spyCustomersFindOne(null);
    spyCustomersFindOne(null);
    const res = await agent
      .put(`/api/v4/feed/${username}/follow/${user_followed}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.statusCode).toEqual(400);
    expect(res.body.message).toEqual('User not found');
  });

  it('Should return catch error', async () => {
    spyCustomersFindOneError();
    const res = await agent
      .put(`/api/v4/feed/${username}/follow/${user_followed}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.error.message).toEqual('Test Catch Error');
  });
};
module.exports = followUser;
