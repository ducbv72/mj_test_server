const { genPost } = require('../../mocks/social-feed');

const personalFeed = (agent, app, fn) => {
  const { spyPostsAggregate, spyPostsAggregateError } = fn;
  const limit = 5;
  const body = {
    username: 'Tester',
    limit,
  };
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post('/api/v4/feed/user/posts');
    expect(res.status).toEqual(401);
  });

  it('Should return trending post', async () => {
    spyPostsAggregate(genPost(limit));
    const res = await agent
      .post('/api/v4/feed/user/posts')
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(200);
    expect(res.body.data.feed.posts).toBeArray();
  });

  it('Should return error with message: "Test Catch Error"', async () => {
    spyPostsAggregateError();
    const res = await agent
      .post('/api/v4/feed/user/posts')
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(400);
    expect(res.body.error.message).toBe('Test Catch Error');
  });

  it('Should return error limit type', async () => {
    body.limit = 'a';
    const res = await agent
      .post('/api/v4/feed/user/posts')
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(412);
    expect(res.body.message).toBeString().toEqual('Validation failed');
  });
};
module.exports = personalFeed;
