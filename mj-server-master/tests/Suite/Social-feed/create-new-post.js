const { post } = require('../../mocks/social-feed');
const fs = require('fs');

const createPost = async (agent, app, fn) => {
  const { spyPostsAggregate, spyS3Upload, spyPostsInsertOne } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post('/api/v4/feed/posts');
    expect(res.status).toEqual(401);
  });

  // Should return new post, case: normal jpg
  it('It should return new post', async () => {
    const filePath = `${process.cwd()}\\tests\\mocks\\images\\test.jpg`;
    spyPostsAggregate([post]);
    spyS3Upload('S3URL');
    spyPostsInsertOne({ ops: post });
    const checkFile = fs.existsSync(filePath);
    if (!checkFile) {
      throw new Error('file does not exist');
    }
    const res = await agent
      .post('/api/v4/feed/posts')
      .auth(app.token, { type: 'bearer' })
      .attach('files', filePath)
      .field('caption', 'caption')
      .field('user_tag', '#Test');

    expect(res.status).toBe(200);
    expect(res.body.statusCode).toBe(200);
    expect(res.body.data).toStrictEqual(post);
  });

  // Should return new post, case heic
  it('It should return new post when upload heic file', async () => {
    const filePath = `${process.cwd()}\\tests\\mocks\\images\\testHeicFile.heic`;
    spyPostsAggregate([post]);
    spyS3Upload('S3URL');
    spyPostsInsertOne({ ops: post });
    const checkFile = fs.existsSync(filePath);
    if (!checkFile) {
      throw new Error('file does not exist');
    }
    const res = await agent
      .post('/api/v4/feed/posts')
      .auth(app.token, { type: 'bearer' })
      .attach('files', filePath)
      .field('caption', 'caption')
      .field('user_tag', '#Test');

    expect(res.status).toBe(200);
    expect(res.body.statusCode).toBe(200);
    expect(res.body.data).toStrictEqual(post);
  });

  // Should return error params required
  it('It should return params required error', async () => {
    const filePath = `${process.cwd()}\\tests\\mocks\\images\\test.jpg`;

    const checkFile = fs.existsSync(filePath);
    if (!checkFile) {
      throw new Error('file does not exist');
    }
    const res = await agent
      .post('/api/v4/feed/posts')
      .auth(app.token, { type: 'bearer' })
      .attach('files', filePath)
      .field('caption', 'caption');
    expect(res.status).toBe(200);
    expect(res.body.statusCode).toBe(400);
    expect(res.body.error.message)
      .toBeString()
      .toEqual('Required parameter not found');
  });

  // Should return error mime type
  it('It should return mime type not accept', async () => {
    const filePath = `${process.cwd()}\\tests\\mocks\\images\\testErrorFile.doc`;
    spyPostsAggregate([post]);
    spyS3Upload('S3URL');
    spyPostsInsertOne({ ops: post });

    const checkFile = fs.existsSync(filePath);
    if (!checkFile) {
      throw new Error('file does not exist');
    }
    const res = await agent
      .post('/api/v4/feed/posts')
      .auth(app.token, { type: 'bearer' })
      .attach('files', filePath)
      .field('caption', 'caption')
      .field('user_tag', '#Test');

    expect(res.body.statusCode).toBe(415);
    expect(res.body.error.message)
      .toBeString()
      .toEqual('File type not allowed. Please try again.');
  });

  // Should return error file type too large
  it('It should return file type too large', async () => {
    const filePath = `${process.cwd()}\\tests\\mocks\\images\\testFileSize.jpeg`;
    spyPostsAggregate([post]);
    spyS3Upload('S3URL');
    spyPostsInsertOne({ ops: post });

    const checkFile = fs.existsSync(filePath);
    if (!checkFile) {
      throw new Error('file does not exist');
    }
    const res = await agent
      .post('/api/v4/feed/posts')
      .auth(app.token, { type: 'bearer' })
      .attach('files', filePath)
      .field('caption', 'caption')
      .field('user_tag', '#Test');

    expect(res.status).toBe(200);
    expect(res.body.statusCode).toBe(413);
    expect(res.body.error.message)
      .toBeString()
      .toEqual('File type too large. Please try again.');
  });
};
module.exports = createPost;
