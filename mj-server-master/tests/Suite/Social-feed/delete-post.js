const deletePost = (agent, app, fn) => {
  const { spyPostFindOneAndDelete } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.delete('/api/v4/feed/posts/1');
    expect(res.status).toEqual(401);
  });

  it('It should return success', async () => {
    spyPostFindOneAndDelete({ value: { username: 'Tester' } });
    const res = await agent
      .delete('/api/v4/feed/posts/1')
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.data.deleted).toBeTruthy();
  });

  it('It should return error', async () => {
    spyPostFindOneAndDelete(null);
    const res = await agent
      .delete('/api/v4/feed/posts/10')
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.error.message)
      .toBeString()
      .toEqual("Cannot read property 'username' of null");
  });

  it('It should return type invalid', async () => {
    const res = await agent
      .delete('/api/v4/feed/posts/aaaaa')
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(412);
    expect(res.body.message).toBeString().toEqual('Validation failed');
  });
};
module.exports = deletePost;
