const ListBlogs = require('./list-blogs');
const ListVideos = require('./list-videos');
const ListArtist = require('./list-artists');

const Shopify = require('shopify-api-node');
const AWS = require('aws-sdk');

const SocialFeed = (agent, app) => {
  const spyS3Upload = (data) => {
    jest.spyOn(AWS.S3.prototype, 'upload').mockImplementationOnce(() => {
      return {
        promise: () => data,
      };
    });
  };

  const spyS3UploadError = () => {
    jest.spyOn(AWS.S3.prototype, 'upload').mockImplementationOnce(() => {
      return {
        promise: () => Promise.reject(new Error('Test Catch Error')),
      };
    });
  };

  const spyCustomersFindOne = (data) => {
    jest.spyOn(app.db.customers, 'findOne').mockImplementationOnce(() => data);
  };

  const spyPostsInsertOne = (data) => {
    jest.spyOn(app.db.posts, 'insertOne').mockImplementationOnce(() => data);
  };

  const spyPostsFindOne = (data) => {
    jest.spyOn(app.db.posts, 'findOne').mockImplementationOnce(() => data);
  };

  const spyPostsFindOneError = () => {
    jest
      .spyOn(app.db.posts, 'findOne')
      .mockImplementationOnce(() =>
        Promise.reject(new Error('Test Catch Error')),
      );
  };

  const spyPostsFindOneAndUpdate = (data) => {
    jest
      .spyOn(app.db.posts, 'findOneAndUpdate')
      .mockImplementationOnce(() => data);
  };

  const spyLikersFindOne = (data) => {
    jest.spyOn(app.db.likers, 'findOne').mockImplementationOnce(() => data);
  };

  const spyLikersInsert = (data) => {
    jest.spyOn(app.db.likers, 'insert').mockImplementationOnce(() => data);
  };

  const spyLikersDeleteOne = (data) => {
    jest.spyOn(app.db.likers, 'deleteOne').mockImplementationOnce(() => data);
  };

  const spyLikersCountDocuments = (data) => {
    jest
      .spyOn(app.db.likers, 'countDocuments')
      .mockImplementationOnce(() => data);
  };

  const spyShopifyBlog = (data) => {
    jest.spyOn(Shopify.prototype.blog, 'list').mockImplementationOnce(() => {
      return data;
    });
  };

  const spyShopifyBlogError = () => {
    jest
      .spyOn(Shopify.prototype.blog, 'list')
      .mockImplementationOnce(() =>
        Promise.reject(new Error('Test Catch Error')),
      );
  };

  const spyShopifyArticle = (data) => {
    jest.spyOn(Shopify.prototype.article, 'list').mockImplementationOnce(() => {
      return data;
    });
  };

  const spyCustomersFindOneError = () => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementationOnce(() =>
        Promise.reject(new Error('Test Catch Error')),
      );
  };

  const spyReportsInsert = (data) => {
    jest
      .spyOn(app.db.reports, 'insert')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyFollowsFind = (data) => {
    jest.spyOn(app.db.follows, 'find').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyFollowsInsertOne = (data) => {
    jest.spyOn(app.db.follows, 'insertOne').mockImplementationOnce(() => {
      return {
        ops: [data],
      };
    });
  };
  const spyFollowsFindOneAndDelete = (data) => {
    jest
      .spyOn(app.db.follows, 'findOneAndDelete')
      .mockImplementationOnce(() => {
        return {
          value: data,
        };
      });
  };

  const spyPostFindOneAndDelete = (data) => {
    jest.spyOn(app.db.posts, 'findOneAndDelete').mockImplementationOnce(() => {
      return {
        value: data,
      };
    });
  };

  const spyReportsInsertError = () => {
    jest
      .spyOn(app.db.reports, 'insert')
      .mockImplementationOnce(() =>
        Promise.reject(new Error('Test Catch Error')),
      );
  };

  const spyPostsAggregate = (data) => {
    jest.spyOn(app.db.posts, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyFollowsAggregate = (data) => {
    jest.spyOn(app.db.follows, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyLikersAggregate = (data) => {
    jest.spyOn(app.db.likers, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyPostsAggregateError = () => {
    jest.spyOn(app.db.posts, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => Promise.reject(new Error('Test Catch Error')),
      };
    });
  };

  const spyPostsFind = (data) => {
    jest.spyOn(app.db.posts, 'find').mockImplementation(() => {
      return {
        limit: () => ({
          sort: () => ({
            project: () => ({
              toArray: () => data,
            }),
          }),
        }),
      };
    });
  };

  const spyPostsFindError = () => {
    jest.spyOn(app.db.posts, 'find').mockImplementation(() => {
      return {
        limit: () => ({
          sort: () => ({
            project: () => ({
              toArray: () => Promise.reject(new Error('Test Catch Error')),
            }),
          }),
        }),
      };
    });
  };

  //Get blogs
  //GET /api/v4/feed/blogs
  describe(`Get blogs from feed`, () => {
    ListBlogs(agent, app, {
      spyShopifyBlog,
      spyShopifyArticle,
      spyShopifyBlogError,
    });
  });

  //Get videos
  //GET /api/v4/feed/videos
  describe(`Get videos from feed`, () => {
    ListVideos(agent, app);
  });

  //Get artists from shopify
  //GET /api/v4/feed/artists
  describe(`Get artists from shopify`, () => {
    ListArtist(agent, app, {
      spyShopifyBlog,
      spyShopifyArticle,
      spyShopifyBlogError,
    });
  });
};
module.exports = SocialFeed;
