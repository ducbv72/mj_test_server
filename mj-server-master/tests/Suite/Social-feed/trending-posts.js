const { genPost } = require('../../mocks/social-feed');
const trendingPosts = (agent, app, fn) => {
  const { spyPostsFind, spyPostsFindError } = fn;
  const limit = 5;
  it('Should return trending post', async () => {
    spyPostsFind(genPost(limit));
    const res = await agent.get(`/api/v4/feed/trending/posts?limit=${limit}`);
    expect(res.status).toBe(200);
    expect(res.body.data.trending.posts).toBeArray();
    expect(res.body.data.trending.posts.length).toBeLessThanOrEqual(10);
  });

  it('Should return error limit type', async () => {
    const res = await agent.get('/api/v4/feed/trending/posts?limit=aaa');
    expect(res.status).toBe(412);
    expect(res.body.message).toBeString().toEqual('Validation failed');
  });

  it('Should return error with message: "Test Catch Error"', async () => {
    spyPostsFindError();
    const res = await agent.get(`/api/v4/feed/trending/posts?limit=${limit}`);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.error.message).toBeString().toEqual('Test Catch Error');
  });
};
module.exports = trendingPosts;
