const { follows, followsResult } = require('../../mocks/social-feed');
const latestPosts = (agent, app, fn) => {
  const { spyFollowsFind, spyPostsAggregate, spyPostsAggregateError } = fn;
  // Should return latest social feed posts
  it('Should return latest social feed posts (case no username)', async () => {
    spyPostsAggregate(followsResult);
    const res = await agent.get(
      '/api/v4/feed/posts?post_id=1&user_tag=%23123abc&search=DiepNguyen&post_ids=2,5&limit=10',
    );
    expect(res.status).toEqual(200);
    expect(res.body.data.posts).toBeArray();
  });

  it('Should return latest social feed posts', async () => {
    spyFollowsFind(follows);
    spyPostsAggregate(followsResult);
    const res = await agent.get(
      '/api/v4/feed/posts?username=DiepNguyen&post_id=1&user_tag=%23123abc&search=DiepNguyen&post_ids=2,5&limit=10',
    );
    expect(res.status).toEqual(200);
    expect(res.body.data.posts).toBeArray();
  });

  it('Should return error with message: "Test Catch Error"', async () => {
    spyPostsAggregateError()
    const res = await agent.get('/api/v4/feed/posts');
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.error.message).toBe('Test Catch Error');
  });

  // Should return data type error
  it('Should return data type error', async () => {
    const res = await agent.get(
      '/api/v4/feed/posts?username=DiepNguyen&post_id=14&user_tag=%23123abc&search=DiepNguyen&post_ids=14&limit=aaaa',
    );
    expect(res.status).toEqual(412);
    expect(res.body.message).toBeString().toEqual('Validation failed');
  });
};
module.exports = latestPosts;
