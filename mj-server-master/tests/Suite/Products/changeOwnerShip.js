const {
  user,
  newOrder,
  order_id,
  getInfoShopify,
} = require('../../mocks/scanning');
const changeOwnerShip = (agent, app, fn) => {
  const {
    spyCustomersFindOne,
    spyOrdersFindOne,
    spyOrdersUpdate,
    spyShopifyCustomerSearch,
    spyAdminFirebase,
    spyOrdersFind,
  } = fn;

  const data = {
    order_id,
    newOwnerUserName: 'Tester_2',
    store_font_key: 'store_font_key',
  };

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/change-owner-ship`);
    expect(res.status).toEqual(401);
  });

  it('Should return update ownership', async () => {
    spyCustomersFindOne(user);
    spyShopifyCustomerSearch(getInfoShopify);
    spyOrdersFindOne(order_id, newOrder);
    spyOrdersUpdate(newOrder);
    spyAdminFirebase('Message To Topic');
    spyOrdersFind([]);
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('Your request has been applied');
  });

  it('Should return error with message invalid parameters', async () => {
    const dataWithNoUsername = {
      order_id: data.order_id,
      store_font_key: data.store_font_key,
    };
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(dataWithNoUsername);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Invalid Parameters');
    expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  });

  it('Should return error message (case User Not Found)', async () => {
    spyCustomersFindOne(null);
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('User Not Found');
  });

  it('Should return error message (case id diffrent)', async () => {
    spyCustomersFindOne({ _id: 1, customerId: '1' });
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('Invalid Parameters');
  });

  it('Should return error response (Email not found)', async () => {
    spyCustomersFindOne(user);
    spyShopifyCustomerSearch(null);
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('Email not found');
  });

  it('Should return error response (User not found)', async () => {
    spyCustomersFindOne(user);
    spyShopifyCustomerSearch([]);
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('User not found');
  });

  it('Should return error response (User not found customer = undefined)', async () => {
    spyCustomersFindOne(user);
    spyShopifyCustomerSearch([undefined]);
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('User not found');
  });

  it('Should return error response (case Certificate Not Found)', async () => {
    spyCustomersFindOne(user);
    spyShopifyCustomerSearch(getInfoShopify);
    spyOrdersFindOne(null, null);
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('Certificate Not Found');
  });

  it('Should return error response (case Not allowed)', async () => {
    spyCustomersFindOne(user);
    spyShopifyCustomerSearch(getInfoShopify);
    spyOrdersFindOne(order_id, { customerId: '2' });
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(403);
    expect(res.body.code).toBe(403);
    expect(res.body.message).toBe('Not allowed');
  });

  it('Should return error response (case Invalid Parameters result.orderEmail === customer.email)', async () => {
    spyCustomersFindOne(user);
    spyShopifyCustomerSearch(getInfoShopify);
    spyOrdersFindOne(order_id, {
      customerId: '1',
      orderEmail: 'tester2@test.vn',
    });
    const res = await agent
      .post(`/api/v4/change-owner-ship`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('Invalid Parameters');
  });
};

module.exports = changeOwnerShip;
