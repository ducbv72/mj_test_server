const { digitalContent } = require('../../mocks/vaultFeatures');
const { product } = require('../../mocks/E-Commerce');

const getProductByToyId = (agent, fn) => {
  const { spyDigitalContentsFindOne, spyProductsFindOne } = fn;

  it('Should return success response with product', async () => {
    spyDigitalContentsFindOne(digitalContent(true));
    spyProductsFindOne(product(1).productVariantSku, product(1));
    const res = await agent.get(`/api/v4/product-by-toy-id/1`);
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toStrictEqual(product(1));
  });

  it('Should return error product not found "no digitalContents"', async () => {
    spyDigitalContentsFindOne(null);
    spyProductsFindOne(product(1).productVariantSku, product(1));
    const res = await agent.get(`/api/v4/product-by-toy-id/1`);
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('not found');
    expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  });

  it('Should return error product not found "no product"', async () => {
    spyDigitalContentsFindOne(digitalContent(false));
    spyProductsFindOne();
    const res = await agent.get(`/api/v4/product-by-toy-id/1`);
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('not found');
    expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  });
};

module.exports = getProductByToyId;
