const getToyCertificate = (agent, app, fn) => {
  const order_id = '1';
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/get-certificate-by-order-id`);
    expect(res.status).toEqual(401);
  });

  it('Should return success response', async () => {
    fn(order_id, {});
    const res = await agent
      .get(`/api/v4/get-certificate-by-order-id?order_id=${order_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeObject();
  });

  it('Should return order not found', async () => {
    fn();
    const res = await agent
      .get(`/api/v4/get-certificate-by-order-id?order_id=${order_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe(
      'This id type is not true or has been delete from database',
    );
  });
};

module.exports = getToyCertificate;
