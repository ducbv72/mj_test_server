const { gameAndShiftCode } = require('../../mocks/products');

const codeForProduct = (agent, app, fn) => {
  const udid = 'UDID';
  const { spyGameAndShiftCodeFind } = fn;

  test('should return success response', async () => {
    spyGameAndShiftCodeFind(gameAndShiftCode);
    const res = await agent.get(`/api/v4/getCodesForProduct/${udid}`);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toStrictEqual(gameAndShiftCode);
  });

  test('should return error response (case not found game/shift code)', async () => {
    spyGameAndShiftCodeFind(null);
    const res = await agent.get(`/api/v4/getCodesForProduct/${udid}`);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Not found');
  });
};

module.exports = codeForProduct;
