const { newOrder } = require('../../mocks/scanning');

const exclusiveSale = (agent, app, fn) => {
  const SKUofCCProduct = 'LND-21CCWOBB13-BEG';
  const { spyOrdersFind, spyOrdersUpdateMany } = fn;

  it('It should return success response', async () => {
    spyOrdersFind([newOrder]);
    spyOrdersUpdateMany(true);
    const res = await agent.get(`/api/v4/update-cc-product/${SKUofCCProduct}`);
    expect(res.status).toEqual(200);
    expect(res.body.message).toEqual('Updated!');
  });

  it('It should return error response (case not found product)', async () => {
    spyOrdersFind([]);
    spyOrdersUpdateMany(true);
    const res = await agent.get(`/api/v4/update-cc-product/${SKUofCCProduct}`);
    expect(res.status).toEqual(400);
    expect(res.body.message).toEqual('Your product is not a cc product');
  });

  it('It should return error response (case not update product failed)', async () => {
    spyOrdersFind([newOrder]);
    spyOrdersUpdateMany(false);
    const res = await agent.get(`/api/v4/update-cc-product/${SKUofCCProduct}`);
    expect(res.status).toEqual(400);
    expect(res.body.message).toEqual('Updated failed!');
  });
};

module.exports = exclusiveSale;
