const { user, newOrder, fields, files } = require('../../mocks/scanning');

const registerToEther = (agent, app, fn) => {
  const { spyCustomersFindOne, spyOrdersFindOne, spyFormidable } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/register-to-ether`);
    expect(res.status).toEqual(401);
  });

  it('Should return register ownership - Second scan', async () => {
    spyCustomersFindOne(user);
    spyOrdersFindOne(null, newOrder, fields.encrypted);
    spyFormidable(null, fields, files);
    const res = await agent
      .post(`/api/v4/register-to-ether`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('This product has been registered');
  });
};

module.exports = registerToEther;
