const { shopifyTagExclusiveSale } = require('../../mocks/E-Commerce');

const subscribeB2B = (agent, app, fn) => {
  const { spyShopifyCustomer, spyAdminFirebase, spyAdminFirebaseError } = fn;
  const registrationTokens = 'registrationTokens';
  const successMessage = 'Subscribed To Channels';
  const errorMessage = 'Error To Subscribe This Channels';
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/exclusive-sale/subscribe`);
    expect(res.status).toEqual(401);
  });

  it('It should return error user not have exclusive-mobile-nyansum tag', async () => {
    spyShopifyCustomer(shopifyTagExclusiveSale(false));
    const res = await agent
      .post(`/api/v4/exclusive-sale/subscribe`)
      .auth(app.token, { type: 'bearer' })
      .send({ registrationTokens });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.message).toEqual(
      'This customer does not include exclusive-mobile-nyansum tag.',
    );
  });

  it('It should return successfully subscribed', async () => {
    spyShopifyCustomer(shopifyTagExclusiveSale(true));
    spyAdminFirebase(successMessage);
    const res = await agent
      .post(`/api/v4/exclusive-sale/subscribe`)
      .auth(app.token, { type: 'bearer' })
      .send({ registrationTokens });
    expect(res.body.code).toEqual(200);
    expect(res.body.message).toEqual(
      'Successfully subscribed to topic: Nyansum-test',
    );
  }, 100000);

  it('It should return error with message error subscribed', async () => {
    spyAdminFirebase(errorMessage);
    const res = await agent
      .post(`/api/v4/exclusive-sale/subscribe`)
      .auth(app.token, { type: 'bearer' });
    expect(res.body.code).toEqual(400);
    expect(res.body.message).toEqual(
      `Error subscribing to topic: Nyansum-test`,
    );
  });

  it('It should return Internal Error Server', async () => {
    spyAdminFirebaseError(errorMessage);
    const res = await agent
      .post(`/api/v4/exclusive-sale/subscribe`)
      .auth(app.token, { type: 'bearer' });
    expect(res.body.code).toEqual(500);
    expect(res.body.message).toEqual(`Internal Error Server`);
  });
};

module.exports = subscribeB2B;
