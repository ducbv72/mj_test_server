const {
  storefrontProductsReturnNodes,
  dbProducts,
} = require('../../mocks/stores');

const productByIds = (agent, app, fn) => {
  const { nockShopifyProduct, nockShopifyProductError, spyProductFind } = fn;
  const ids =
    'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzE=,Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzI=';
  const errorMessage = 'Test Catch Error';

  it('Should return success response', async () => {
    nockShopifyProduct(storefrontProductsReturnNodes);
    spyProductFind(dbProducts);
    const res = await agent.get(`/api/v4/products/byIds/${ids}`);
    expect(res.status).toBe(200);
    expect(res.body.message).toBeString().toBe('Success');
    expect(res.body.data).toBeArray();
  });

  // it('Should return error response (case cannot find products by ids)', async () => {
  //   // nockShopifyProduct(null);
  //   // spyProductFind(spyProductFind);
  //   const res = await agent.get(`/api/v4/products/byIds/${ids}`);
  //   expect(res.status).toBe(200);
  //   expect(res.body.data).toBeObject();
  // });

  // it('Should return error response (case cannot find products by ids)', async () => {
  //   // nockShopifyProduct({ data: { nodes: null } });
  //   // spyProductFind(spyProductFind);
  //   const res = await agent.get(`/api/v4/products/byIds/${ids}`);
  //   expect(res.status).toBe(200);
  //   expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  // });
  //
  // it('Should return error response (case errors have exists)', async () => {
  //   // nockShopifyProduct({ errors: [{ message: 'Test Error' }] });
  //   // spyProductFind(spyProductFind);
  //   const res = await agent.get(`/api/v4/products/byIds/${ids}`);
  //   expect(res.status).toBe(400);
  //   expect(res.body.code).toBe(400);
  //   expect(res.body.message).toBe('Invalid global id');
  // });

  it('Should return error response (case ids is null)', async () => {
    const res = await agent.get(`/api/v4/products/byIds/null`);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Success');
    expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  });

  // it('Should return error response (case ids is null)', async () => {
  //   nockShopifyProductError(errorMessage);
  //   spyProductFind(spyProductFind);
  //   const res = await agent.get(`/api/v4/products/byIds/${ids}`);
  //   expect(res.status).toBe(400);
  //   expect(res.body.code).toBe(400);
  //   expect(res.body.message).toBe('Test Catch Error');
  // });
};

module.exports = productByIds;
