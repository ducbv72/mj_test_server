const {
  shopifyTagExclusiveSale,
  productsB2B,
} = require('../../mocks/E-Commerce');

const exclusiveSale = (agent, app, fn) => {
  const aggregateData = [{ productArtist: 'Artist' }];
  const limit = 1;
  const {
    spyShopifyCustomer,
    nockShopifyProduct,
    nockShopifyProductError,
    spyAggregateProducts,
  } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/exclusive-sale`);
    expect(res.status).toEqual(401);
  });

  it('It should return error user not have exclusive-mobile-nyansum tag', async () => {
    spyShopifyCustomer(shopifyTagExclusiveSale(false));
    const res = await agent
      .get(`/api/v4/exclusive-sale?limit=${limit}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.message).toEqual(
      'This customer does not include exclusive-mobile-nyansum tag.',
    );
  });

  it('It should return B2B product', async () => {
    spyShopifyCustomer(shopifyTagExclusiveSale(true));
    nockShopifyProduct(productsB2B(limit, true));
    spyAggregateProducts(aggregateData);
    const res = await agent
      .get(`/api/v4/exclusive-sale?limit=${limit}&nextCursor=null`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.statusCode).toEqual(200);
    expect(res.body.result.result).toBeArray().toBeArrayOfSize(limit);
  });

  it('It should return B2B product not found', async () => {
    spyShopifyCustomer(shopifyTagExclusiveSale(true));
    nockShopifyProduct(productsB2B(limit, true));
    spyAggregateProducts([null]);
    const res = await agent
      .get(`/api/v4/exclusive-sale?limit=${limit}&nextCursor=null`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(404);
    expect(res.body.message).toEqual('Product not found');
  });

  it('It should return error user not have exclusive-mobile-nyansum tag', async () => {
    const message = 'Test Catch Error';
    spyShopifyCustomer(shopifyTagExclusiveSale(true));
    spyAggregateProducts(aggregateData);
    nockShopifyProductError(message);
    const res = await agent
      .get(`/api/v4/exclusive-sale`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.statusCode).toEqual(500);
    expect(res.body.message).toEqual('Internal Error Server');
    expect(res.body.error_message).toEqual(message);
  });
};

module.exports = exclusiveSale;
