const { storefrontProductsReturnNodes } = require('../../mocks/stores');
const { product } = require('../../mocks/E-Commerce');

const productById = (agent, app, fn) => {
  const { nockShopifyProduct, nockShopifyProductError, spyProductsFindOne } =
    fn;
  const id = 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzE=';
  const errorMessage = 'Test Catch Error';
  const SKU = 'SKU';

  it('Should return success response', async () => {
    nockShopifyProduct(storefrontProductsReturnNodes);
    spyProductsFindOne(SKU, product(1));
    const res = await agent.get(`/api/v4/product/byId/${id}`);
    expect(res.status).toBe(200);
    expect(res.body.message).toBeString().toBe('Success');
    expect(res.body.data).toBeObject();
    expect(res.body.data.description).toBe('test');
  });

  it('Should return success response (case Invalid global id)', async () => {
    nockShopifyProduct(storefrontProductsReturnNodes);
    spyProductsFindOne(SKU, {});
    const res = await agent.get(`/api/v4/product/byId/${id}`);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Invalid global id');
  });
};

module.exports = productById;
