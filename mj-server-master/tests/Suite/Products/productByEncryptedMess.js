const {
  newOrder,
  order_id,
  accountShopify,
  customerShopify,
  shopifyProductVariants,
  qrcode,
  campaign,
  campaignParticipates,
  product,
  getInfoShopify,
  orderWithSameId,
} = require('../../mocks/scanning');
const createRecordInToyDB = (agent, app, fn) => {
  const body = {
    order_id,
    encrypted: 'encrytedMessage',
    udid: 'udid',
  };
  const {
    spyOrdersFindOne,
    spyOrdersUpdate,
    spyQrcodesFindOne,
    spyCampaignsFindOne,
    spyCampaignParticipatesFindOne,
    spyCampaignParticipatesInsert,
    spyProductsFindOne,
    spyQrcodesFind,
    spyOrdersInsert,
    nockSmileAccount,
    nockSmileCustomer,
    spyShopifyGrapql,
    nockSmilePointTransaction,
    spyShopifyProductVariant,
    spyShopifyCustomerGet,
    spyOrdersUpdateOne,
  } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/product-by-encrypted-message`);
    expect(res.status).toEqual(401);
  });

  it('Should return create record in toy database', async () => {
    spyOrdersFindOne(order_id, newOrder);
    nockSmileAccount(accountShopify);
    nockSmileCustomer(customerShopify);
    spyShopifyProductVariant({ price: 1 });
    spyShopifyGrapql(shopifyProductVariants);
    nockSmilePointTransaction({});
    spyOrdersUpdate(newOrder);
    const res = await agent
      .post(`/api/v4/product-by-encrypted-message`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeObject();
  });

  it('Should return create record in toy database (case body not includes order_id)', async () => {
    spyOrdersFindOne(null, newOrder, body.encrypted);
    spyQrcodesFindOne(qrcode);
    spyProductsFindOne(null, product(1));
    spyShopifyCustomerGet(getInfoShopify);
    spyOrdersInsert(newOrder);
    //Mocks coinAwark
    nockSmileAccount(accountShopify);
    nockSmileCustomer(customerShopify);
    spyShopifyProductVariant({ price: 1 });
    spyShopifyGrapql(shopifyProductVariants);
    nockSmilePointTransaction({});
    const res = await agent
      .post(`/api/v4/product-by-encrypted-message`)
      .auth(app.token, { type: 'bearer' })
      .send({ encrypted: body.encrypted, udid: body.udid });
    expect(res.status).toBe(200);
    // expect(res.body.message).toBe('ok');
    // expect(res.body.data).toBeObject();
  });

  it('Should return error response (send order_id and not find order)', async () => {
    spyOrdersFindOne(null, null);
    const res = await agent
      .post(`/api/v4/product-by-encrypted-message`)
      .auth(app.token, { type: 'bearer' })
      .send({
        encrypted: body.encrypted,
        udid: body.udid,
        order_id: 'order_id',
      });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Product not found');
    expect(res.body.data).toBeNull();
  });

  it('Should return create record in toy database (case body not includes body)', async () => {
    const res = await agent
      .post(`/api/v4/product-by-encrypted-message`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Not found');
    expect(res.body.data).toBeNull();
  });

  it('Should return success res (case body not includes order_id and order not found)', async () => {
    spyOrdersFindOne();
    spyQrcodesFindOne(qrcode);
    spyCampaignsFindOne(campaign);
    spyCampaignParticipatesFindOne(campaignParticipates);
    spyProductsFindOne(null, product(1));
    //Mocks coinAwark
    nockSmileAccount(accountShopify);
    nockSmileCustomer(customerShopify);
    spyShopifyProductVariant({ price: 1 });
    spyShopifyGrapql(shopifyProductVariants);
    spyShopifyCustomerGet(getInfoShopify);
    nockSmilePointTransaction({});
    const res = await agent
      .post(`/api/v4/product-by-encrypted-message`)
      .auth(app.token, { type: 'bearer' })
      .send({ encrypted: body.encrypted, udid: body.udid });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeObject();
  });

  it('Should return error response (case found order, qrcode but order updated fail)', async () => {
    spyOrdersFindOne(null, orderWithSameId, body.encrypted);
    spyQrcodesFindOne(qrcode);
    spyCampaignsFindOne(campaign);
    spyCampaignParticipatesFindOne(null);
    spyCampaignParticipatesFindOne(null);
    spyCampaignParticipatesInsert('ok');
    spyProductsFindOne(null, product(2));
    spyOrdersUpdateOne(null);
    const res = await agent
      .post(`/api/v4/product-by-encrypted-message`)
      .auth(app.token, { type: 'bearer' })
      .send({ encrypted: body.encrypted, udid: body.udid });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Product not found');
  });

  it('Should return error response (case found order but not found qrcode)', async () => {
    spyOrdersFindOne(null, newOrder, body.encrypted);
    spyQrcodesFindOne(null);
    const res = await agent
      .post(`/api/v4/product-by-encrypted-message`)
      .auth(app.token, { type: 'bearer' })
      .send({ encrypted: body.encrypted, udid: body.udid });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Product not found');
  });
};

module.exports = createRecordInToyDB;
