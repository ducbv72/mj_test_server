const { productCC } = require('../../mocks/products');
const ObjectId = require('mongodb').ObjectId;

const changeLocation = (agent, app, fn) => {
  const id = new ObjectId();
  const { spyOrdersAggregate, spyOrdersUpdateOne, spyOrdersAggregateError } =
    fn;
  const body = {
    productAltar: {
      data: {
        location: 2,
        orderIdNew: '0000000603b3fd496c12bbbb',
        orderIdOld: '0000000203b3fd496c12bbbd',
        order: 20,
        order_id: id.toString(),
      },
    },
  };
  test('should return need authentication', async () => {
    const res = await agent.post(`/api/v4/products/altar/change-location`);
    expect(res.status).toBe(401);
  });

  test('should return success response', async () => {
    productCC[9]._id = id;
    spyOrdersAggregate(productCC);
    spyOrdersUpdateOne({ result: { n: 1 } });
    const res = await agent
      .post(`/api/v4/products/altar/change-location`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Success');
  });

  test('should return catch error', async () => {
    spyOrdersAggregateError();
    const res = await agent
      .post(`/api/v4/products/altar/change-location`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Test Catch Error');
  });
};

module.exports = changeLocation;
