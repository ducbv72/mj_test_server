const { product } = require('../../mocks/E-Commerce');
const { shopifyGraphql } = require('../../mocks/products');

const getSpecificProd = (agent, fn) => {
  const SKU = 'SKU';
  const {
    spyProductsFindOne,
    nockShopifyProduct,
    nockShopifyProductError,
    spyExclusiveSaleCount,
  } = fn;

  // it('Should return success response', async () => {
  //   spyProductsFindOne(SKU, product(1));
  //   nockShopifyProduct(shopifyGraphql);
  //   spyExclusiveSaleCount(2);
  //   const res = await agent.get(`/api/v4/product/${SKU}`);
  //   expect(res.status).toBe(200);
  //   expect(res.body.code).toBe(200);
  //   expect(res.body.message).toBeString().toEqual('Product found');
  // });

  // it('Should return product not found', async () => {
  //   spyProductsFindOne();
  //   const res = await agent.get('/api/v4/product/SKUU');
  //   expect(res.status).toBe(404);
  //   expect(res.body.code).toBe(404);
  //   expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  //   expect(res.body.message).toBeString().toEqual('No product found');
  // });

  // it('Should return error response (case catch error)', async () => {
  //   spyProductsFindOne(SKU, product(1));
  //   nockShopifyProductError('Test Catch Error');
  //   const res = await agent.get(`/api/v4/product/${SKU}`);
  //   expect(res.status).toBe(500);
  //   expect(res.body.code).toBe(500);
  //   expect(res.body.message).toBe('Internal Server Error');
  //   expect(res.body.error_message).toBe('Test Catch Error');
  // });
};
module.exports = getSpecificProd;
