const Shopify = require('shopify-api-node');
const admin = require('firebase-admin');
const formidable = require('formidable');
const nock = require('nock');
const StockXAPI = require('stockx-api');
const common = require('../../../lib/common');

const GetToyCertificate = require('./getToyCertificate');
const GetSpecificProd = require('./getSpecificProd');
const GetProductByToyId = require('./getProductByToyId');
const CreateRecordInToyDB = require('./productByEncryptedMess');
const ChangeOwnerShip = require('./changeOwnerShip');
const RegisterToEther = require('./registerToEther');
const StockXbySku = require('./stockXbySku');
const ExclusiveSale = require('./exclusive-sale');
const SubscribeB2B = require('./subscribe-b2b');
const UnsubscribeB2B = require('./unsubscribe-b2b');
const UpdateCCProduct = require('./updateCCProduct');
const ProductByIds = require('./productByIds');
const ProductById = require('./productById');
const ProductByHandle = require('./productByHandle');
const ChangeLocation = require('./changeLocation');
const GetVoucherCodes = require('./getVoucherCodes');
const GetCodeForProduct = require('./codeForProduct');
const ScanToUnlockCode = require('./scanToUnlockCode');
const ScanToUnlockVoucherCode = require('./scanToUnlockVoucherCode');

const ProductModule = (agent, app) => {
  const spyOrdersFindOne = (id, data, encrypt) => {
    jest.spyOn(app.db.orders, 'findOne').mockImplementation((arg) => {
      if (arg.shopifyOrderId === id) {
        return data;
      }
      if (arg['orderProducts.productEncryptedModel'] === encrypt) {
        return data;
      }
      if (arg['$and'][0]['$or'][0].encrytedMessage === encrypt) {
        return data;
      }
      return data;
    });
  };

  const spyDigitalContentsFindOne = (data) => {
    jest.spyOn(app.db.digitalContents, 'findOne').mockImplementationOnce(() => {
      return data;
    });
  };

  const spyExclusiveSaleCount = (data) => {
    jest.spyOn(app.db.exclusiveSale, 'count').mockImplementationOnce(() => {
      return data;
    });
  };

  const nockShopifyProduct = (data, status = 200) => {
    nock('https://mightyjaxx.myshopify.com/api/2021-01/graphql.json')
      .persist()
      .post(/.*/)
      .reply(status, data);
  };

  const nockShopifyProductError = (message) => {
    nock('https://mightyjaxx.myshopify.com/api/2021-01/graphql.json')
      .post(/.*/)
      .replyWithError(message);
  };

  const spyCustomersFindOne = (data) => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyCustomersFindOneError = () => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementationOnce(() =>
        Promise.reject(new Error('Test Catch Error')),
      );
  };

  const spyFormidable = (err, fields, files) => {
    jest.spyOn(formidable, 'IncomingForm').mockImplementationOnce(() => {
      return {
        parse: async (req, callback) => {
          return callback(err, fields, files);
        },
      };
    });
  };

  const spyShopifyCustomerSearch = (data) => {
    jest
      .spyOn(Shopify.prototype.customer, 'search')
      .mockImplementation(() => Promise.resolve(data));
  };

  const spyCommonFunction = (data) => {
    jest
      .spyOn(common, 'Schedule')
      .mockImplementation(() => Promise.resolve(data));
  };

  const spyOrdersFind = (data) => {
    jest.spyOn(app.db.orders, 'find').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyOrdersUpdate = (data) => {
    jest.spyOn(app.db.orders, 'update').mockImplementation(() => {
      return data;
    });
  };

  const spyOrdersUpdateMany = (isTrue = false) => {
    jest.spyOn(app.db.orders, 'updateMany').mockImplementation(() => {
      return { result: { n: +isTrue } };
    });
  };

  const spyOrdersUpdateOne = (data) => {
    jest.spyOn(app.db.orders, 'updateOne').mockImplementation(() => {
      return data;
    });
  };

  const spyOrdersInsert = (data) => {
    jest.spyOn(app.db.orders, 'insert').mockImplementation((arg) => {
      return data;
    });
  };

  const spyQrcodesFindOne = (data) => {
    jest.spyOn(app.db.qrcodes, 'findOne').mockImplementation((arg) => {
      return data;
    });
  };

  const spyCampaignsFindOne = (data) => {
    jest.spyOn(app.db.campaigns, 'findOne').mockImplementationOnce(() => {
      return data;
    });
  };

  const spyCampaignParticipatesFindOne = (data) => {
    jest
      .spyOn(app.db.campaignParticipates, 'findOne')
      .mockImplementationOnce(() => {
        return data;
      });
  };

  const spyCampaignParticipatesInsert = (data) => {
    jest
      .spyOn(app.db.campaignParticipates, 'insert')
      .mockImplementationOnce(() => {
        return data;
      });
  };

  const spyQrcodesFind = (data) => {
    jest.spyOn(app.db.qrcodes, 'find').mockImplementationOnce(() => {
      return data;
    });
  };

  const spyProductsFindOne = (sku, data) => {
    jest.spyOn(app.db.products, 'findOne').mockImplementation((arg) => {
      if (arg.productVariantSku === sku) {
        return data;
      }
      if (Object.keys(arg)[0] === '_id') {
        return data;
      }
      if (arg.shopifyProductId) {
        return data;
      }
      return data;
    });
  };

  const nockSmileAccount = (data) => {
    nock('https://api.smile.io/v1/accounts').get(/.*/).reply(200, data);
  };

  const nockSmileCustomer = (data) => {
    nock('https://api.smile.io/v1/customers/search').get(/.*/).reply(200, data);
  };

  const nockSmilePointTransaction = (data) => {
    nock('https://api.smile.io/v1/points_transactions')
      .post(/.*/)
      .reply(200, data);
  };

  const spyShopifyGrapql = (data) => {
    jest
      .spyOn(Shopify.prototype, 'graphql')
      .mockImplementation(() => Promise.resolve(data));
  };

  const spyShopifyProductVariant = (data) => {
    jest
      .spyOn(Shopify.prototype.productVariant, 'get')
      .mockImplementation(() => Promise.resolve(data));
  };

  const spyShopifyCustomerGet = (data) => {
    jest
      .spyOn(Shopify.prototype.customer, 'get')
      .mockImplementation(() => Promise.resolve(data));
  };

  const spyStockXapi = (data, isError, errorMessage) => {
    jest.spyOn(StockXAPI.prototype, 'searchProducts').mockImplementation(() => {
      if (isError) {
        return Promise.reject(new Error(errorMessage));
      }
      return data;
    });
  };

  //Defined evironment for all suite
  const spyAggregateProducts = (data) => {
    jest.spyOn(app.db.products, 'aggregate').mockImplementation(() => {
      return {
        toArray: () => {
          return data;
        },
      };
    });
  };

  const spyShopifyGrapqlError = () => {
    jest
      .spyOn(Shopify.prototype, 'graphql')
      .mockImplementation(() => Promise.reject(new Error('Test Catch Error')));
  };

  const spyShopifyOrder = (data) => {
    jest
      .spyOn(Shopify.prototype.order, 'get')
      .mockImplementation((num) => Promise.resolve(data(num)));
  };

  const spyShopifyOrderError = () => {
    jest
      .spyOn(Shopify.prototype.order, 'get')
      .mockImplementation(() => Promise.reject(new Error('Test Catch Error')));
  };

  const spyShopifyCustomer = (data) => {
    jest
      .spyOn(Shopify.prototype.customer, 'get')
      .mockImplementation(() => data);
  };

  const spyAdminFirebase = (message) => {
    jest.spyOn(admin, 'messaging').mockImplementationOnce(() => ({
      subscribeToTopic: (token) => {
        if (token) {
          return Promise.resolve(message);
        }
        return Promise.reject(message);
      },
      unsubscribeFromTopic: (token) => {
        if (token) {
          return Promise.resolve(message);
        }
        return Promise.reject(message);
      },
      sendToTopic: () => {
        return Promise.resolve(message);
      },
    }));
  };

  const spyAdminFirebaseError = (message) => {
    jest.spyOn(admin, 'messaging').mockImplementationOnce(() => ({
      subscribeToTopic: () => {
        throw new Error(message);
      },
      unsubscribeFromTopic: () => {
        throw new Error(message);
      },
    }));
  };

  const spyProductFind = (data) => {
    jest.spyOn(app.db.products, 'find').mockImplementation(() => ({
      toArray: () => data,
    }));
  };

  const spyVoucherCodesFind = (data) => {
    jest.spyOn(app.db.voucherCodes, 'find').mockImplementation(() => ({
      toArray: () => data,
    }));
  };

  /**
   *
   * @param {Array} data Array
   */
  const spyGameAndShiftCodeFind = (data) => {
    jest.spyOn(app.db.gameAndShiftCode, 'find').mockImplementation(() => ({
      toArray: () => data,
    }));
  };

  /**
   *
   * @param {Object} data Object
   */
  const spyGameAndShiftCodeFindOne = (data) => {
    jest
      .spyOn(app.db.gameAndShiftCode, 'findOne')
      .mockImplementation(() => data);
  };

  const spyVoucherCodesFindOne = (data) => {
    jest.spyOn(app.db.voucherCodes, 'findOne').mockImplementation(() => data);
  };

  const spyGameAndShiftCodeUpdate = (isTrue = true) => {
    jest.spyOn(app.db.gameAndShiftCode, 'update').mockImplementation(() => {
      return { result: { n: +isTrue } };
    });
  };

  const spyVoucherCodesUpdate = (isTrue = true) => {
    jest.spyOn(app.db.voucherCodes, 'update').mockImplementation(() => {
      return { result: { n: +isTrue } };
    });
  };

  const spyOrdersAggregate = (data) => {
    jest.spyOn(app.db.orders, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyOrdersAggregateError = () => {
    jest.spyOn(app.db.orders, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => Promise.reject(new Error('Test Catch Error')),
      };
    });
  };

  //Get toy's certificate based on toy's first scanning ID
  //GET /api/v4/get-certificate-by-order-id
  describe("Get toy's certificate based on toy's first scanning ID", () => {
    GetToyCertificate(agent, app, spyOrdersFindOne);
  });

  //Get a specific product's information based on SKU
  //GET /api/v4/product/:sku
  describe(`Get a specific product's information based on SKU`, () => {
    GetSpecificProd(agent, {
      spyProductsFindOne,
      nockShopifyProduct,
      nockShopifyProductError,
      spyExclusiveSaleCount,
    });
  });

  //Get digital content associated with specific product based on toy's first scanning ID
  //GET /api/v4/product-by-toy-id/:id
  describe(`Get digital content associated with specific product based on toy's first scanning ID`, () => {
    GetProductByToyId(agent, {
      spyDigitalContentsFindOne,
      spyProductsFindOne,
    });
  });

  //Send chip data to create record in toy database
  //POST /api/v4/product-by-encrypted-message
  describe('Send chip data to create record in toy database', () => {
    CreateRecordInToyDB(agent, app, {
      spyOrdersFindOne,
      spyOrdersUpdate,
      spyQrcodesFindOne,
      spyCampaignsFindOne,
      spyCampaignParticipatesFindOne,
      spyCampaignParticipatesInsert,
      spyProductsFindOne,
      spyQrcodesFind,
      spyOrdersInsert,
      nockSmileAccount,
      nockSmileCustomer,
      spyShopifyGrapql,
      nockSmilePointTransaction,
      spyShopifyProductVariant,
      spyShopifyCustomerGet,
      spyOrdersUpdateOne,
    });
  });

  //Send new username to database to update ownership
  //POST /api/v4/change-owner-ship
  describe('Send new username to database to update ownership', () => {
    ChangeOwnerShip(agent, app, {
      spyCustomersFindOne,
      spyOrdersFindOne,
      spyOrdersUpdate,
      spyShopifyCustomerSearch,
      spyAdminFirebase,
      spyOrdersFind,
    });
  });

  //Send encrypted string to register ownership - Second scan
  //POST /api/v4/register-to-ether
  describe('Send encrypted string to register ownership - Second scan', () => {
    // RegisterToEther(agent, app, {
    //   spyCustomersFindOne,
    //   spyOrdersFindOne,
    //   spyFormidable,
    // });
  });

  // Get product stockx by sku
  // GET /api/v4/product-stockx-by-sku/:token/:sku
  describe(`Get product stockx by sku`, () => {
    StockXbySku(agent, app, {
      spyStockXapi,
      spyProductsFindOne,
    });
  });

  //Get a product from shopify with an exclusive sale tag
  //GET /api/v4/exclusive-sale
  describe('Get a product from shopify with an exclusive sale tag', () => {
    ExclusiveSale(agent, app, {
      spyShopifyCustomer,
      nockShopifyProduct,
      nockShopifyProductError,
      spyAggregateProducts,
    });
  });

  //Add users with a specific exclusive sale tag to firebase for notifications
  // POST /api/v4/exclusive-sale/subscribe
  describe('Add users with a specific exclusive sale tag to firebase for notifications', () => {
    SubscribeB2B(agent, app, {
      spyShopifyCustomer,
      spyAdminFirebase,
      spyAdminFirebaseError,
    });
  });

  //Unsubscribe specific exclusive sale users from firebase
  // POST /api/v4/exclusive-sale/unsubscribe
  describe('Unsubscribe specific exclusive sale users from firebase', () => {
    UnsubscribeB2B(agent, app, {
      spyShopifyCustomer,
      spyAdminFirebase,
      spyAdminFirebaseError,
    });
  });

  //Update creepy cuties info when update json
  //GET /api/v4/update-cc-product/:sku
  describe('Update creepy cuties info when update json', () => {
    UpdateCCProduct(agent, app, {
      spyOrdersFind,
      spyOrdersUpdateMany,
    });
  });

  //Get list products by array of id
  //GET /api/v4/products/byIds/:ids
  describe('Get list products by array of id', () => {
    ProductByIds(agent, app, {
      nockShopifyProduct,
      nockShopifyProductError,
      spyProductFind,
    });
  });

  //Get products by id
  //GET /api/v4/product/byId/:id
  describe('Get products by id', () => {
    ProductById(agent, app, {
      nockShopifyProduct,
      nockShopifyProductError,
      spyProductsFindOne,
    });
  });

  //Get list products by handle
  //GET /api/v4/product/byHandle/:handle
  describe('Get list products by handle', () => {
    ProductByHandle(agent, app, {
      nockShopifyProduct,
      nockShopifyProductError,
      spyProductsFindOne,
    });
  });

  //Product altar change location
  //POST /api/v4/products/altar/change-location
  describe('Product altar change location', () => {
    ChangeLocation(agent, app, {
      spyOrdersAggregate,
      spyOrdersUpdateOne,
      spyOrdersAggregateError,
    });
  });

  // //Api for get voucher codes
  // //GET /api/v4/getVoucherCodesForProduct/:udid
  // describe('Api for get voucher codes', () => {
  //   GetVoucherCodes(agent, app, {
  //     spyVoucherCodesFind,
  //   });
  // });

  // //Api for scan to unlock game/shift codes
  // //GET /api/v4/getCodesForProduct/:udid
  // describe('Api for scan to unlock game/shift codes', () => {
  //   GetCodeForProduct(agent, app, {
  //     spyGameAndShiftCodeFind,
  //   });
  // });

  // //Api for scan to unlock codes
  // //POST /api/v4/scan-to-unlock-code
  // describe('Api for scan to unlock codes', () => {
  //   ScanToUnlockCode(agent, app, {
  //     spyCustomersFindOne,
  //     spyGameAndShiftCodeFindOne,
  //     spyGameAndShiftCodeUpdate,
  //     spyCommonFunction,
  //     spyCustomersFindOneError,
  //   });
  // });

  // //Api for scan to unlock voucher codes
  // //POST /api/v4/scan-to-unlock-voucher-code
  // describe('Api for scan to unlock voucher codes', () => {
  //   ScanToUnlockVoucherCode(agent, app, {
  //     spyCustomersFindOne,
  //     spyVoucherCodesFindOne,
  //     spyVoucherCodesUpdate,
  //     spyCommonFunction,
  //     spyCustomersFindOneError,
  //   });
  // });
};

module.exports = ProductModule;
