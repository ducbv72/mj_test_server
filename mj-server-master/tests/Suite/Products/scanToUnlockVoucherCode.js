const { voucherCode } = require('../../mocks/products');
const { user } = require('../../mocks/scanning');
const ObjectId = require('mongodb').ObjectId;

const scanToUnlockVoucherCode = (agent, app, fn) => {
  const data = { id: new ObjectId() };
  const {
    spyCustomersFindOne,
    spyVoucherCodesFindOne,
    spyVoucherCodesUpdate,
    spyCommonFunction,
    spyCustomersFindOneError,
  } = fn;

  test('It Should require authorization', async () => {
    const res = await agent.post(`/api/v4/scan-to-unlock-voucher-code`);
    expect(res.status).toEqual(401);
  });

  test('Should return success response', async () => {
    spyCustomersFindOne(user);
    spyVoucherCodesFindOne(voucherCode[0]);
    spyVoucherCodesUpdate(true);
    spyVoucherCodesFindOne(voucherCode[0]);
    spyCommonFunction('ok');
    const res = await agent
      .post(`/api/v4/scan-to-unlock-voucher-code`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('Unlock Code Success');
    expect(res.body.data).toStrictEqual([voucherCode[0]]);
  });

  test('Should return error response (case customer not found)', async () => {
    spyCustomersFindOne(null);
    spyVoucherCodesFindOne(voucherCode[0]);
    const res = await agent
      .post(`/api/v4/scan-to-unlock-voucher-code`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(401);
    expect(res.body.message).toBe(
      'Unauthorised. Please refer to administrator.',
    );
    expect(res.body.data).toBeNull();
  });

  test('Should return error response (case cannot found game and shift code)', async () => {
    spyCustomersFindOne(user);
    spyVoucherCodesFindOne(null);
    spyCommonFunction('ok');
    const res = await agent
      .post(`/api/v4/scan-to-unlock-voucher-code`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('Code Not Found');
    expect(res.body.data).toBeNull();
  });

  test('Should return error response (case update game and shift code failed)', async () => {
    spyCustomersFindOne(user);
    spyVoucherCodesFindOne(voucherCode[0]);
    spyVoucherCodesUpdate(false);
    const res = await agent
      .post(`/api/v4/scan-to-unlock-voucher-code`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Update failed');
    expect(res.body.data).toBeNull();
  });

  test('Should return error response (case status is not unallocated)', async () => {
    spyCustomersFindOne(user);
    spyVoucherCodesFindOne(voucherCode[1]);
    const res = await agent
      .post(`/api/v4/scan-to-unlock-voucher-code`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('Code has been unlocked or expired');
    expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  });

  test('Should return error response (case catch error)', async () => {
    spyCustomersFindOneError();
    spyVoucherCodesFindOne(voucherCode[1]);
    const res = await agent
      .post(`/api/v4/scan-to-unlock-voucher-code`)
      .auth(app.token, { type: 'bearer' })
      .send(data);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(500);
    expect(res.body.message).toBe('Internal server error');
  });
};

module.exports = scanToUnlockVoucherCode;
