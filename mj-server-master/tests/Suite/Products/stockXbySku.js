const { product } = require('../../mocks/E-Commerce');
const config = require('../../../config');

const stockXbySku = (agent, app, fn) => {
  const SKU = 'SKU';
  const { spyStockXapi, spyProductsFindOne } = fn;

  it('Should return success response', async () => {
    spyProductsFindOne(SKU, product(1, [{ id: '1' }]));
    spyStockXapi([
      {
        id: '1',
        product: 'product',
        url: 'url',
        market: { lowestAsk: 'lowestAsk' },
      },
    ]);
    const res = await agent.get(
      `/api/v4/product-stockx-by-sku/${config.stockxToken}/SKU`,
    );
    expect(res.status).toBe(200);
    expect(res.body.msg).toBeString().toBe('Product found');
    expect(res.body.data).toBeObject();
  });

  it('Should return unauthorized', async () => {
    const res = await agent.get(
      `/api/v4/product-stockx-by-sku/stockxToken/SKU`,
    );
    expect(res.status).toBe(401);
    expect(res.body.msg)
      .toBeString()
      .toBe('Unauthorised. Please refer to administrator.');
    expect(res.body.data).toBe(null);
  });

  it('Should return sku not found', async () => {
    spyProductsFindOne();
    const res = await agent.get(
      `/api/v4/product-stockx-by-sku/${config.stockxToken}/SKU`,
    );
    expect(res.status).toBe(404);
    expect(res.body.msg).toBeString().toBe('Product not found');
    expect(res.body.data).toBe(null);
  });

  it('Should return not show stockX banner', async () => {
    spyProductsFindOne(SKU, product(1));
    spyStockXapi([{ id: '1', product: 'product', url: 'url' }]);
    const res = await agent.get(
      `/api/v4/product-stockx-by-sku/${config.stockxToken}/SKU`,
    );
    expect(res.status).toBe(404);
    expect(res.body.msg).toBeString().toBe('Product not found');
    expect(res.body.data).toBe(null);
  });

  it('Should return Product not found (stockX cannot find item)', async () => {
    spyProductsFindOne(SKU, product(1, [{ id: '1' }]));
    spyStockXapi([]);
    const res = await agent.get(
      `/api/v4/product-stockx-by-sku/${config.stockxToken}/SKU`,
    );
    expect(res.status).toBe(404);
    expect(res.body.msg).toBe('Product not found');
    expect(res.body.data).toBe(null);
  });

  it('Should return catch error block', async () => {
    const errorMessage = 'No products found';
    spyProductsFindOne(SKU, product(1, [{ id: '1' }]));
    spyStockXapi(null, true, errorMessage);
    const res = await agent.get(
      `/api/v4/product-stockx-by-sku/${config.stockxToken}/SKU`,
    );
    expect(res.status).toBe(500);
    expect(res.body.msg).toBeString().toBe('Internal Server Error');
    expect(res.body.error_message).toBe(errorMessage);
  });
};

module.exports = stockXbySku;
