const { voucherCode } = require('../../mocks/products');

const getVoucherCodes = (agent, app, fn) => {
  const udid = 'UDID';
  const { spyVoucherCodesFind } = fn;

  test('should return success response', async () => {
    spyVoucherCodesFind(voucherCode);
    const res = await agent.get(`/api/v4/getVoucherCodesForProduct/${udid}`);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toStrictEqual(voucherCode);
  });

  test('should return error response (case not found voucher code)', async () => {
    spyVoucherCodesFind(null);
    const res = await agent.get(`/api/v4/getVoucherCodesForProduct/${udid}`);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Not found');
  });
};

module.exports = getVoucherCodes;
