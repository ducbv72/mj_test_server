const { storefrontProductsReturnNodes } = require('../../mocks/stores');
const { product } = require('../../mocks/E-Commerce');

const productByHandle = (agent, app, fn) => {
  const { nockShopifyProduct, nockShopifyProductError, spyProductsFindOne } =
    fn;
  const handle = 'all';
  const errorMessage = 'Test Catch Error';
  const SKU = 'SKU';

  it('Should return success response', async () => {
    nockShopifyProduct(storefrontProductsReturnNodes);
    spyProductsFindOne(SKU, product(1));
    const res = await agent.get(`/api/v4/product/byHandle/${handle}`);
    expect(res.status).toBe(200);
    expect(res.body.message).toBeString().toBe('Success');
    expect(res.body.data).toBeObject();
    expect(res.body.data.description).toBe('test');
  });

  it('Should return success response (case dbProduct is empty object)', async () => {
    nockShopifyProduct(storefrontProductsReturnNodes);
    spyProductsFindOne(SKU, {});
    const res = await agent.get(`/api/v4/product/byHandle/${handle}`);
    expect(res.status).toBe(200);
    expect(res.body.message).toBeString().toBe('Success');
    expect(res.body.data).toBeObject();
    expect(res.body.data.description).toBe('test');
  });

  it('Should return error response (case cannot find products by ids)', async () => {
    nockShopifyProduct(null);
    const res = await agent.get(`/api/v4/product/byHandle/${handle}`);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeObject();
  });

  it('Should return error response (case cannot find products by ids)', async () => {
    nockShopifyProduct({ data: { node: null } });
    const res = await agent.get(`/api/v4/product/byHandle/${handle}`);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Success');
    expect(res.body.data).toBeObject();
  });

  it('Should return error response (case ids is null)', async () => {
    nockShopifyProductError(errorMessage);
    const res = await agent.get(`/api/v4/product/byHandle/${handle}`);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Test Catch Error');
  });
};

module.exports = productByHandle;
