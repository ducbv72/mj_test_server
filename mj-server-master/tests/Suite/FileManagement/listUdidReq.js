const { sendEmailToManufacture } = require('../../mocks/file-management');

const listUdidReq = (agent, app, fn) => {
  const count = 1;
  const {
    spySendEmailToManufactureCount,
    spySendEmailToManufactureAggregate,
    spySendEmailToManufactureCountError,
  } = fn;

  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/list-udid_requests`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    spySendEmailToManufactureCount(count);
    spySendEmailToManufactureAggregate(sendEmailToManufacture);
    const res = await agent
      .get(
        `/api/v4/list-udid_requests?page=1&email=tester@test.com&limit=10&from=22-10-2021&to=22-02-2022`,
      )
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data.totalCount).toBe(count);
    expect(res.body.data.files).toStrictEqual(sendEmailToManufacture);
  });

  it('It should return success response (case no from in query)', async () => {
    spySendEmailToManufactureCount(count);
    spySendEmailToManufactureAggregate(sendEmailToManufacture);
    const res = await agent
      .get(
        `/api/v4/list-udid_requests?page=1&email=tester@test.com&limit=10&to=22-02-2022`,
      )
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data.totalCount).toBe(count);
    expect(res.body.data.files).toStrictEqual(sendEmailToManufacture);
  });

  it('It should return error response (case catch error)', async () => {
    spySendEmailToManufactureCountError();
    spySendEmailToManufactureAggregate(sendEmailToManufacture);
    const res = await agent
      .get(`/api/v4/list-udid_requests?limit=10`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
    expect(res.body.message).toBe('Test Catch Error');
  });
};

module.exports = listUdidReq;
