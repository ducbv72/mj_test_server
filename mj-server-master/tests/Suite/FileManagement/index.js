const ListUdidReq = require('./listUdidReq');

const Home = (agent, app) => {
  const spySendEmailToManufactureCount = (data) => {
    jest
      .spyOn(app.db.sendEmailToManufacture, 'count')
      .mockImplementation(() => data);
  };

  const spySendEmailToManufactureCountError = () => {
    jest
      .spyOn(app.db.sendEmailToManufacture, 'count')
      .mockImplementation(() => Promise.reject(new Error('Test Catch Error')));
  };

  const spySendEmailToManufactureAggregate = (data) => {
    jest
      .spyOn(app.db.sendEmailToManufacture, 'aggregate')
      .mockImplementation(() => ({
        sort: () => ({
          skip: () => ({ limit: () => ({ toArray: () => data }) }),
        }),
      }));
  };

  // Get list-udid_requests
  // GET /api/v4/list-udid_requests/
  describe('Get list-udid_requests', () => {
    ListUdidReq(agent, app, {
      spySendEmailToManufactureCount,
      spySendEmailToManufactureAggregate,
      spySendEmailToManufactureCountError,
    });
  });
};

module.exports = Home;
