const result = {
  characterNumber: '1',
  series: '1',
  rarity: '1',
  cateristics: '1',
  flavorProfile: '1',
  hpMax: '100',
  lvMax: '100',
  attack1Name: 'attack1Name',
  attack1Text: 'attack1Text',
  attack1Max: 'attack1Max',
  attack2Name: 'attack2Name',
  attack2Text: 'attack2Text',
  attack2Max: 'attack2Max',
  nftName: 'nftName',
  characterOriginText: 'characterOriginText',
  imageLink: 'imageLink',
};

const result2 = {
  nftName: 'nftName',
  characterOriginText: 'characterOriginText',
  imageLink: 'imageLink',
};
const getNft = (agent, fn) => {
  const { spyNftProductsFindOne, spyNftProductsFindOneError } = fn;
  const tokenId = '1';
  test('should return success response', async () => {
    spyNftProductsFindOne(result);
    const res = await agent.get(`/admin/products/nft/${tokenId}`);
    expect(res.status).toBe(200);
    expect(res.body.name).toBe(result.nftName);
    expect(res.body.description).toBe(result.characterOriginText);
    expect(res.body.attributes).toBeArray();
  });

  test('should return success response (case resule 2)', async () => {
    spyNftProductsFindOne(result2);
    const res = await agent.get(`/admin/products/nft/${tokenId}`);
    expect(res.status).toBe(200);
    expect(res.body.name).toBe(result.nftName);
    expect(res.body.description).toBe(result.characterOriginText);
    expect(res.body.attributes).toBeArray();
  });

  test('should return error with message: "Product not found."', async () => {
    spyNftProductsFindOne(null);
    const res = await agent.get(`/admin/products/nft/${tokenId}`);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Product not found.');
  });

  test('should return error with catch error', async () => {
    spyNftProductsFindOneError();
    const res = await agent.get(`/admin/products/nft/${tokenId}`);
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });
};

module.exports = getNft;
