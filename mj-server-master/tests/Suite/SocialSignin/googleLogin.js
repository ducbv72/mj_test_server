const { googleAccount, googleParams, id } =
  require('../../mocks/SocialLogin').googleConstants;
const { OAuth2Client } = require('google-auth-library');

const googleLogin = (agent, getAccessToken) => {
  const { access_token, full_name, user_id } = googleParams;
  const { email, email_verified, sub } = googleAccount;
  const spy = (result) => {
    jest
      .spyOn(OAuth2Client.prototype, 'verifyIdToken')
      .mockImplementationOnce(() =>
        Promise.resolve({
          getPayload: () => {
            return result;
          },
        }),
      );
  };

  it('Should return login success', async () => {
    getAccessToken(access_token, id, email);
    spy(googleAccount);
    const res = await agent.get(
      `/api/v4/google/access-token?id_token=${access_token}&full_name=${full_name}&user_id=${user_id}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.status).toBe('success');
    expect(res.body.accessToken).toBe(access_token);
    expect(res.body.customerData).toBeObject();
    expect(res.body.tags).toBeArray();
  });

  it('Should return error with message: "Details do not match"', async () => {
    getAccessToken(access_token, id, email);
    spy(googleAccount);
    const res = await agent.get(
      `/api/v4/google/access-token?id_token=${access_token}&full_name=${full_name}&user_id=2`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('Should return error with message: "Email not found"', async () => {
    getAccessToken(access_token, id, email);
    spy({ email_verified, sub });
    const res = await agent.get(
      `/api/v4/google/access-token?id_token=${access_token}&full_name=${full_name}&user_id=${user_id}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('Should return error with message: "invalid_parameters"', async () => {
    const access_token = '';
    const res = await agent.get(
      `/api/v4/google/access-token?id_token=${access_token}&full_name=${full_name}&user_id=${user_id}`,
    );
    expect(res.status).toBe(400);
    expect(res.body.status).toBe('failed');
    expect(res.body.error_message).toBe('invalid_parameters');
  });
};
module.exports = googleLogin;
