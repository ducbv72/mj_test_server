const Web3 = require('web3');
const web3 = new Web3();
const { ethPriceInUsd, fee, address } =
  require('../../mocks/SocialLogin').getTransactionFee;

const spyWeb3 = () => {
  jest.spyOn(web3.utils, 'fromWei').mockImplementationOnce(() => fee);
};

const spyWeb3Error = () => {
  jest.spyOn(web3.utils, 'fromWei').mockImplementationOnce(() => {
    throw new Error('Case Error');
  });
};
const getTransactionFee = (agent, getCoinGecko) => {
  it('Should return success response', async () => {
    spyWeb3();
    getCoinGecko(ethPriceInUsd);
    const res = await agent.get(`/api/v4/transaction-fee?address=${address}`);
    expect(res.status).toBe(200);
    expect(res.body.status).toBe('success');
    expect(res.body.tx_fee_in_usd).toBe(
      parseFloat(ethPriceInUsd * fee).toFixed(2),
    );
  });

  it('Should return error with message: "invalid_parameters"', async () => {
    const address = 'invalid_address';
    const res = await agent.get(`/api/v4/transaction-fee?address=${address}`);
    expect(res.status).toBe(400);
    expect(res.body.status).toBe('failed');
    expect(res.body.error_message).toBe('invalid_parameters');
  });

  it('Should return error with message: "Internal Server Error"', async () => {
    spyWeb3Error();
    const res = await agent.get(`/api/v4/transaction-fee?address=${address}`);
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });
};
module.exports = getTransactionFee;
