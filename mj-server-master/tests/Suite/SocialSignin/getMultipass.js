const nock = require('nock');
const { email, id, terms_token } = require('../../mocks/SocialLogin').acceptTnC;

const getMultipassUrlFromFacebook = (agent, nockfn) => {
  const { getAccessToken, getMePathFB, getIdPathFB } = nockfn;
  const code = terms_token;
  const path = '';
  const fbTokenResponse = () => {
    nock('https://graph.facebook.com/v9.0/oauth/access_token')
      .get(/.*/)
      .reply(200, { access_token: 'access_token' });
  };
  it('Should return success get multipass', async () => {
    fbTokenResponse();
    getMePathFB(' ');
    getIdPathFB(email);
    getAccessToken(code, id, email);
    const res = await agent.get(
      `/api/v4/facebook/multipass?code=${code}&redirect_to_path=${path}`,
    );
    console.log(res.body);
  });

  it('Should return success get multipass (tags is empty array)', async () => {
    const tags = [];
    fbTokenResponse();
    getMePathFB('');
    getIdPathFB(email);
    getAccessToken(code, id, email, tags);
    const res = await agent.get(
      `/api/v4/facebook/multipass?code=${code}&redirect_to_path=${path}`,
    );
    console.log(res.body);
  });

  it('Should return error with message: "Email not found"', async () => {
    fbTokenResponse();
    getMePathFB();
    getIdPathFB();
    getAccessToken(code, id, email);
    const res = await agent.get(
      `/api/v4/facebook/multipass?code=${code}&redirect_to_path=${path}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('Should return error with message: "Invalid access token"', async () => {
    fbTokenResponse();
    getMePathFB();
    getIdPathFB(email);
    getAccessToken('', id, email);
    const res = await agent.get(
      `/api/v4/facebook/multipass?code=${code}&redirect_to_path=${path}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('It should return invalid parameters', async () => {
    const code = '';
    fbTokenResponse();
    getMePathFB();
    getIdPathFB(email);
    getAccessToken('', id, email);
    const res = await agent.get(
      `/api/v4/facebook/multipass?code=${code}&redirect_to_path=${path}`,
    );
    expect(res.status).toBe(400);
    expect(res.body.status).toBe('failed');
    expect(res.body.error_message).toBe('invalid_parameters');
  });
};
module.exports = getMultipassUrlFromFacebook;
