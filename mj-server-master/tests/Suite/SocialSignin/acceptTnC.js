const appleSignin = require('apple-signin-auth');
const { customer_id, email, id, terms_token } =
  require('../../mocks/SocialLogin').acceptTnC;

const acceptTnC = (agent, getAccessToken) => {
  it('Should return terms and conditions', async () => {
    getAccessToken(terms_token, id, email);
    const res = await agent.get(
      `/api/v4/terms-and-conditions?terms_token=${terms_token}&customer_id=${customer_id}&email=${email}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.status).toBe('success');
  });

  it('Should return error with message: "Invalid customerId"', async () => {
    const id = 'invalid_id';
    getAccessToken(terms_token, id, email);
    const res = await agent.get(
      `/api/v4/terms-and-conditions?terms_token=${terms_token}&customer_id=${customer_id}&email=${email}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('Should return error with message: "Invalid termsToken"', async () => {
    const terms_token = 'invalid_token';
    getAccessToken(terms_token, id, email);
    const res = await agent.get(
      `/api/v4/terms-and-conditions?terms_token=${terms_token}&customer_id=${customer_id}&email=${email}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('Should return error with message: "invalid_parameters"', async () => {
    const terms_token = '';
    getAccessToken(terms_token, id, email);
    const res = await agent.get(
      `/api/v4/terms-and-conditions?terms_token=${terms_token}&customer_id=${customer_id}&email=${email}`,
    );
    expect(res.status).toBe(400);
    expect(res.body.status).toBe('failed');
    expect(res.body.error_message).toBe('invalid_parameters');
  });
};
module.exports = acceptTnC;
