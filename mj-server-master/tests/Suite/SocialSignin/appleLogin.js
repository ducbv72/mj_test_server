const appleSignin = require('apple-signin-auth');
const { appleConstants } = require('../../mocks/SocialLogin');

const appleLogin = (agent, getAccessToken, spyFindCustomer) => {
  const appleTestToken = appleConstants.identityToken;
  const full_name = 'tester';
  const email = appleConstants.appleAccount.email;
  const id = appleConstants.id;
  const token = 'token';
  const spy = (sub) => {
    jest
      .spyOn(appleSignin, 'verifyIdToken')
      .mockImplementationOnce(() => Promise.resolve({ sub }));
  };

  it('Get social sign in token for Apple account', async () => {
    spyFindCustomer()
    getAccessToken(token, id, email);
    spy('1');
    const res = await agent.get(
      `/api/v4/apple/access-token?identity_token=${appleTestToken}&full_name=${full_name}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.status).toBe('success');
    expect(res.body.accessToken).toBeDefined();
    expect(res.body.customerData.email).toBe(email);
  });

  it('Should return error with message: "Details do not match"', async () => {
    spy('2');
    const res = await agent.get(
      `/api/v4/apple/access-token?identity_token=${appleTestToken}&full_name=${full_name}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('Should return error with message: "Email not found"', async () => {
    spy('1');
    const res = await agent.get(
      `/api/v4/apple/access-token?identity_token=${appleConstants.identityTokenFail}&full_name=${full_name}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('Should return error with message: "Failed to verify identityToken"', async () => {
    const appleTestToken = 'invalid_token';
    const res = await agent.get(
      `/api/v4/apple/access-token?identity_token=${appleTestToken}&full_name=${full_name}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('Should return error with message: "invalid_parameters"', async () => {
    const appleTestToken = '';
    const res = await agent.get(
      `/api/v4/apple/access-token?identity_token=${appleTestToken}&full_name=${full_name}`,
    );
    expect(res.status).toBe(400);
    expect(res.body.status).toBe('failed');
    expect(res.body.error_message).toBe('invalid_parameters');
  });
};
module.exports = appleLogin;
