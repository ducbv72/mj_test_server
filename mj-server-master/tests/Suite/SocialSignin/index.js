const AppleLogin = require('./appleLogin');
const FacebookLogin = require('./facebookLogin');
const GoogleLogin = require('./googleLogin');
const AcceptTnC = require('./acceptTnC');
const GetMultipassUrlFromFacebook = require('./getMultipass');
const GetTransactionFee = require('./getTransactionFee');
// const GetNft = require('./getNft');
const nock = require('nock');

const SocialLogin = (agent, app) => {
  const getAccessToken = (token, id, email, tags) => {
    nock('https://mightyjaxx.myshopify.com/api/2021-01/graphql.json')
      .persist()
      .post(/.*/)
      .reply(200, {
        data: {
          customerAccessTokenCreateWithMultipass: {
            customerAccessToken: {
              accessToken: token,
              customerData: 'data',
              tags: ['tags'],
            },
          },
          customer: {
            id,
            tags: tags ? tags : ['tags', 'terms_token'],
            email,
          },
        },
      });
  };

  const spyFindCustomer = (email) => {
    jest.spyOn(app.db.customers, 'findOne').mockImplementationOnce((arg) => {
      if (arg.facebookId === '1') {
        return { email: email };
      }
      if (arg['$or'][0].customerId === '1') {
        return {};
      }
      return null;
    });
  };

  const spyNftProductsFindOne = (data) => {
    jest.spyOn(app.db.nftProducts, 'findOne').mockImplementation(() => {
      return data;
    });
  };

  const spyNftProductsFindOneError = () => {
    jest.spyOn(app.db.nftProducts, 'findOne').mockImplementation(() => {
      return Promise.reject(new Error('Test Catch Error'));
    });
  };

  const getMePathFB = (name = 'tester', data) => {
    nock('https://graph.facebook.com/me')
      .get(/.*/)
      .reply(200, data ? data : { name, id: '1' });
  };

  const getCoinGecko = (num = 0) => {
    nock('https://api.coingecko.com/api/v3/simple/price')
      .get(/.*/)
      .reply(200, {
        ethereum: {
          usd: num,
        },
      });
  };

  const getIdPathFB = (email) => {
    nock('https://graph.facebook.com/1').get(/.*/).reply(200, { email });
  };

  //Get social sign in token for Apple account
  //GET /api/v4/apple/access-token
  describe(`Get social sign in token for Apple account`, () => {
    AppleLogin(agent, getAccessToken, spyFindCustomer);
  });

  //Get social sign in token for Facebook account
  //GET /api/v4/facebook/access-token
  describe(`Get social sign in token for Facebook account`, () => {
    FacebookLogin(agent, app, {
      getAccessToken,
      getMePathFB,
      getIdPathFB,
      spyFindCustomer,
    });
  });

  //Get social sign in token for Google account
  //GET /api/v4/google/access-token
  describe(`Get social sign in token for Google account`, () => {
    GoogleLogin(agent, getAccessToken);
  });

  //Get terms and conditions
  //GET /api/v4/terms-and-conditions
  describe(`Get terms and conditions`, () => {
    AcceptTnC(agent, getAccessToken);
  });

  //Get Transaction Fee
  //GET /api/v4/transaction-fee
  describe(`Get transaction fee`, () => {
    GetTransactionFee(agent, getCoinGecko);
  });

  //Get Multipass Url From Facebook
  //GET /api/v4/facebook/multipass
  describe(`Get Multipass Url From Facebook`, () => {
    GetMultipassUrlFromFacebook(agent, {
      getAccessToken,
      getMePathFB,
      getIdPathFB,
    });
  });

  //Get nft
  //GET /admin/products/nft/:tokenId
  // describe(`Get nft`, () => {
  //   GetNft(agent, { spyNftProductsFindOne, spyNftProductsFindOneError });
  // });
};

module.exports = SocialLogin;
