const nock = require('nock');
const { id, facebookAccount } =
  require('../../mocks/SocialLogin').facebookConstants;

const facebookLogin = (agent, app, nockfn) => {
  const { getAccessToken, getMePathFB, getIdPathFB, spyFindCustomer } = nockfn;
  const token = 'fb_valid_token';
  const email = facebookAccount.email;

  it('Should return error with message "facebook_email_not_found"', async () => {
    getMePathFB();
    getIdPathFB();
    spyFindCustomer();
    getAccessToken(token, id, email);

    const res = await agent.get(`/api/v4/facebook/access-token?token=${token}`);
    expect(res.status).toBe(400);
    expect(res.body.status).toBe('error');
    expect(res.body.error).toBe('facebook_email_not_found');
    expect(res.body.message).toBe('facebook_email_not_found');
  });

  it('Should return error with message "facebook_id_not_found"', async () => {
    getMePathFB(null, {});
    getIdPathFB();
    spyFindCustomer();
    getAccessToken(token, id, email);

    const res = await agent.get(`/api/v4/facebook/access-token?token=${token}`);
    expect(res.status).toBe(400);
    expect(res.body.status).toBe('error');
    expect(res.body.error).toBe('facebook_id_not_found');
    expect(res.body.message).toBe('facebook_id_not_found');
  });

  it('It should login success with facebook account (email in database)', async () => {
    getMePathFB();
    getIdPathFB(email);
    spyFindCustomer(email);
    getAccessToken(token, id, email);
    await app.db
      .collection('customers')
      .findOneAndUpdate({ email }, { $set: { facebookId: '1' } }, {});

    const res = await agent.get(
      `/api/v4/facebook/access-token?token=${token}&email=${email}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.status).toBe('success');
    expect(res.body.accessToken).toBeDefined();
    expect(res.body.customerData.email).toBe(email);
  });

  it('It should login success with facebook account', async () => {
    getMePathFB();
    getIdPathFB(email);
    getAccessToken(token, id, email);

    const res = await agent.get(
      `/api/v4/facebook/access-token?token=${token}&email=${email}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.status).toBe('success');
    expect(res.body.accessToken).toBeDefined();
    expect(res.body.customerData.email).toBe(email);
  });

  it('It should return message: "Internal Server Error"', async () => {
    const res = await agent.get(
      `/api/v4/facebook/access-token?token=${token}&email=${email}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.error).toBe('server_error');
    expect(res.body.message).toBe('Internal Server Error');
  });

  it('It should return invalid parameters', async () => {
    const res = await agent.get(`/api/v4/facebook/access-token`);
    expect(res.status).toBe(400);
    expect(res.body.status).toBe('failed');
    expect(res.body.error_message).toBe('invalid_parameters');
  });
};

module.exports = facebookLogin;
