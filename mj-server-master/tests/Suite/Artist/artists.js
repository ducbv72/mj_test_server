/* eslint-disable no-undef */
const { artists } = require('../../mocks/content');
const Artists = (agent, app, fn) => {
  const { spyArtistFind } = fn;

  //Get all artists from database
  //GET /api/v4/artists
  describe('Get all artists from database', () => {
    it('Should return success response', async () => {
      spyArtistFind(artists);
      const res = await agent
        .get('/api/v4/artists')
        .auth(app.token, { type: 'bearer' });
      expect(res.status).toBe(200);
      expect(res.body.message).toBe('ok');
      expect(res.body.data).toBeArray().toBeArrayOfSize(artists.length);
    });
  });
};
module.exports = Artists;
