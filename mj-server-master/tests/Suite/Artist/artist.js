const Artist = (agent, app, fn) => {
  const articleId = 1;
  const { spyArtistFindOne } = fn;

  //Get artist by id from database
  //GET /api/v4/artist/:id
  describe('Get artist by id from database', () => {
    it('Should return success response', async () => {
      spyArtistFindOne(articleId);
      const res = await agent
        .get(`/api/v4/artist/${articleId}`)
        .auth(app.token, { type: 'bearer' });
      expect(res.status).toBe(200);
      expect(res.body.message).toBe('ok');
      expect(res.body.data).toBeObject();
      expect(res.body.data.articleId).toBe(`${articleId}`);
    });

    it('Should return artist not found', async () => {
      const res = await agent
        .get(`/api/v4/artist/28230058099`)
        .auth(app.token, { type: 'bearer' });
      expect(res.status).toBe(404);
      expect(res.body.code).toBe(404);
      expect(res.body.message).toBe('Cannot find artist');
    });
  });
};
module.exports = Artist;
