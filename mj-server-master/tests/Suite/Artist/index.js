const { artist } = require('../../mocks/content');
const Artist = require('./artist');
const Artists = require('./artists');

const ArtistModule = (agent, app) => {
  const spyArtistFind = (data) => {
    jest.spyOn(app.db.artists, 'find').mockImplementationOnce((arg) => {
      if (Object.keys(arg).length === 0) {
        return {
          toArray: () => data,
        };
      }
      return null;
    });
  };

  const spyArtistFindOne = (id) => {
    jest.spyOn(app.db.artists, 'findOne').mockImplementationOnce((arg) => {
      if (arg.articleId === id.toString()) {
        return artist(id);
      }
      return null;
    });
  };

  // Get list of artists
  // GET /api/v4/artists
  describe('Get list of artists', () => {
    Artist(agent, app, { spyArtistFindOne });
  });
  //Get details of artists by id
  //GET /api/v4/artist/:id
  describe('Get details of artists by id', () => {
    Artists(agent, app, { spyArtistFind });
  });
};

module.exports = ArtistModule;
