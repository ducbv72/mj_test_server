const CommunityProfile = require('./CommunityProfile');
const CommunityPoll = require('./CommunityPoll');
const CommunityQuiz = require('./CommunityQuiz');

const CommunityModule = (agent, app) => {
  describe('Community Profile', () => {
    CommunityProfile(agent, app);
  });

  describe('Community Poll Management', () => {
    CommunityPoll(agent, app);
  });

  describe('Community Poll Management', () => {
    CommunityQuiz(agent, app);
  });
};

module.exports = CommunityModule;
