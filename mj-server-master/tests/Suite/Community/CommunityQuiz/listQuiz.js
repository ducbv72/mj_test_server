const { user } = require('../../../mocks/vaultFeatures');
const { orders, quizQuestions, quizs } = require('../../../mocks/community');
const ObjectId = require('mongodb').ObjectId;

const listQuiz = (agent, app, fn) => {
  const {
    spyOrdersFind,
    spyCustomersFindOne,
    spyQuizQuestionsFind,
    spyQuizsAggregate,
    spyQuizsAggregateError,
    spyQuizsUpdateOne,
  } = fn;

  const id = new ObjectId();

  it('Should return success response', async () => {
    spyOrdersFind(orders);
    spyCustomersFindOne(user);
    spyQuizQuestionsFind(quizQuestions);
    spyQuizsAggregate(quizs);
    spyQuizsUpdateOne();
    const res = await agent.get(
      `/api/v4/community-quiz/list-quizs?customerId=${id}&profileId=${id}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.result).toBeArray().toBeArrayOfSize(1);
  });

  it('Should return error response (case catch error)', async () => {
    spyOrdersFind(orders);
    spyCustomersFindOne(user);
    spyQuizQuestionsFind(quizQuestions);
    spyQuizsAggregateError();
    const res = await agent.get(
      `/api/v4/community-quiz/list-quizs?customerId=${id}&profileId=${id}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.message).toBe('Test Catch Error');
  });
};

module.exports = listQuiz;
