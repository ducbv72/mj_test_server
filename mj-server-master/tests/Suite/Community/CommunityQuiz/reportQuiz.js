const { quizs, quizAnswers } = require('../../../mocks/community');
const ObjectId = require('mongodb').ObjectId;

const id = new ObjectId();

const reportQuiz = (agent, app, fn) => {
  const { spyQuizsFindOne, spyQuizAnswersFind, spyQuizsFindOneError } = fn;

  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/community-quiz/quiz-report/${id}`);
    expect(res.status).toEqual(401);
  });

  it('Should return success response', async () => {
    spyQuizsFindOne(quizs[0]);
    spyQuizAnswersFind(quizAnswers);
    const res = await agent
      .get(`/api/v4/community-quiz/quiz-report/${id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.result).toBeArray().toBeArrayOfSize(3);
  });

  it('Should return error response (case catch error)', async () => {
    spyQuizsFindOneError();
    spyQuizAnswersFind(quizAnswers);
    const res = await agent
      .get(`/api/v4/community-quiz/quiz-report/${id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(500);
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.body.error_message).toBe('Test Catch Error');
  });
};

module.exports = reportQuiz;
