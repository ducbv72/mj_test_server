const ListQuiz = require('./listQuiz');
const RetrieveAnswer = require('./retrieveAnswer');
const SaveAnswer = require('./saveAnswer');
const ReportQuiz = require('./reportQuiz');

const CommunityProfile = (agent, app) => {
  const spyCustomersFindOne = (data) => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyQuizsUpdateOne = () => {
    jest
      .spyOn(app.db.quizs, 'updateOne')
      .mockImplementationOnce(() => Promise.resolve({ result: { n: 1 } }));
  };

  const spyQuizAnswersUpdateOne = () => {
    jest
      .spyOn(app.db.quizAnswers, 'updateOne')
      .mockImplementationOnce(() => Promise.resolve({ result: { n: 1 } }));
  };

  const spyQuizAnswersInsertOne = () => {
    jest
      .spyOn(app.db.quizQuestions, 'insertOne')
      .mockImplementationOnce(() =>
        Promise.resolve({ result: { n: 1 }, ops: [{ message: 'inserted' }] }),
      );
  };

  const spyQuizAnswersInsertOneError = () => {
    jest
      .spyOn(app.db.quizQuestions, 'insertOne')
      .mockImplementationOnce(() => Promise.resolve({ result: { n: 0 } }));
  };

  const spyQuizsFindOne = (data) => {
    jest.spyOn(app.db.quizs, 'findOne').mockImplementationOnce(() => data);
  };

  const spyQuizsFindOneError = () => {
    jest
      .spyOn(app.db.quizs, 'findOne')
      .mockImplementationOnce(() =>
        Promise.reject({ message: 'Test Catch Error' }),
      );
  };

  const spyOrdersFind = (data) => {
    jest
      .spyOn(app.db.orders, 'find')
      .mockImplementationOnce(() => ({ toArray: () => Promise.resolve(data) }));
  };

  const spyOrdersFindError = () => {
    jest.spyOn(app.db.orders, 'find').mockImplementationOnce(() => ({
      toArray: () => Promise.reject({ message: 'Test Catch Error' }),
    }));
  };

  const spyQuizQuestionsFind = (data) => {
    jest
      .spyOn(app.db.quizQuestions, 'find')
      .mockImplementationOnce(() => ({ toArray: () => Promise.resolve(data) }));
  };

  const spyQuizAnswersFind = (data) => {
    jest
      .spyOn(app.db.quizAnswers, 'find')
      .mockImplementationOnce(() => ({ toArray: () => Promise.resolve(data) }));
  };

  const spyQuizQuestionsFindOne = (data) => {
    jest
      .spyOn(app.db.quizQuestions, 'findOne')
      .mockImplementationOnce(() => data);
  };

  const spyQuizQuestionsFindOneError = () => {
    jest
      .spyOn(app.db.quizQuestions, 'findOne')
      .mockImplementationOnce(() =>
        Promise.reject({ message: 'Test Catch Error' }),
      );
  };

  const spyQuizsAggregate = (data) => {
    jest.spyOn(app.db.quizs, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyQuizsAggregateError = () => {
    jest.spyOn(app.db.quizs, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => Promise.reject({ message: 'Test Catch Error' }),
      };
    });
  };

  // API get list active quizs
  // GET /api/v4/community-quiz/list-quizs
  describe('API get list active quizs', () => {
    ListQuiz(agent, app, {
      spyOrdersFind,
      spyCustomersFindOne,
      spyQuizQuestionsFind,
      spyQuizsAggregate,
      spyQuizsAggregateError,
      spyQuizsUpdateOne,
    });
  });

  // API retrieve an answer
  // GET /api/v4/community-quiz/retrieve-answer
  describe('API retrieve an answer', () => {
    RetrieveAnswer(agent, app, {
      spyQuizQuestionsFindOne,
      spyQuizQuestionsFindOneError,
    });
  });

  // Create new answer for quiz
  // POST /api/v4/community-quiz/answers
  describe('Create new answer for quiz', () => {
    SaveAnswer(agent, app, {
      spyOrdersFind,
      spyQuizsFindOne,
      spyQuizQuestionsFindOne,
      spyQuizAnswersUpdateOne,
      spyQuizAnswersInsertOne,
      spyQuizAnswersInsertOneError,
      spyOrdersFindError,
    });
  });

  //API get report all answer
  //GET /api/v4/community-quiz/quiz-report/:quizId
  describe('API get report all answer', () => {
    ReportQuiz(agent, app, {
      spyQuizsFindOne,
      spyQuizAnswersFind,
      spyQuizsFindOneError,
    });
  });
};

module.exports = CommunityProfile;
