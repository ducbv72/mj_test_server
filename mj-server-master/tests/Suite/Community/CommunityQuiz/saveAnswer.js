const {
  examAnswer,
  orders,
  quizs,
  quizQuestion,
} = require('../../../mocks/community');
const ObjectId = require('mongodb').ObjectId;

const id = new ObjectId();

const saveAnswer = (agent, app, fn) => {
  const {
    spyOrdersFind,
    spyQuizsFindOne,
    spyQuizQuestionsFindOne,
    spyQuizAnswersUpdateOne,
    spyQuizAnswersInsertOne,
    spyQuizAnswersInsertOneError,
    spyOrdersFindError,
  } = fn;

  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/community-quiz/answers`);
    expect(res.status).toEqual(401);
  });

  it('Should return error (case already completed the quiz)', async () => {
    spyOrdersFind(orders);
    spyQuizsFindOne(quizs[0]);
    spyQuizQuestionsFindOne(quizQuestion);
    const res = await agent
      .post(`/api/v4/community-quiz/answers`)
      .send(examAnswer(id))
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.message).toBe("You've already completed the quiz!");
  });

  it('Should return error (case update failed)', async () => {
    spyOrdersFind(orders);
    spyQuizsFindOne(quizs[0]);
    spyQuizQuestionsFindOne(null);
    spyQuizAnswersUpdateOne();
    spyQuizAnswersInsertOneError();
    const res = await agent
      .post(`/api/v4/community-quiz/answers`)
      .send(examAnswer(id))
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('Update failed, please try again!');
  });

  it('Should return success response', async () => {
    spyOrdersFind(orders);
    spyQuizsFindOne(quizs[0]);
    spyQuizQuestionsFindOne(null);
    spyQuizAnswersUpdateOne();
    spyQuizAnswersInsertOne();
    const res = await agent
      .post(`/api/v4/community-quiz/answers`)
      .send(examAnswer(id))
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.result.message).toBe('inserted');
  });

  it('Should return success response', async () => {
    spyOrdersFindError();
    spyQuizsFindOne(quizs[0]);
    spyQuizQuestionsFindOne(null);
    const res = await agent
      .post(`/api/v4/community-quiz/answers`)
      .send(examAnswer(id))
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(500);
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.body.error_message).toBe('Test Catch Error');
  });
};

module.exports = saveAnswer;
