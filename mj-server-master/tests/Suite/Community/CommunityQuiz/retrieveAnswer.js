const { quizQuestion } = require('../../../mocks/community');

const retrieveAnswer = (agent, app, fn) => {
  const { spyQuizQuestionsFindOne, spyQuizQuestionsFindOneError } = fn;

  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/community-quiz/retrieve-answer`);
    expect(res.status).toEqual(401);
  });

  it('Should return success response', async () => {
    spyQuizQuestionsFindOne(quizQuestion);
    const res = await agent
      .get(`/api/v4/community-quiz/retrieve-answer`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.result).toBeObject();
  });

  it('Should return error response (user not answer this quiz)', async () => {
    spyQuizQuestionsFindOne(null);
    const res = await agent
      .get(`/api/v4/community-quiz/retrieve-answer`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('User not answer this quiz');
  });

  it('Should return error response (case catch error)', async () => {
    spyQuizQuestionsFindOneError();
    const res = await agent
      .get(`/api/v4/community-quiz/retrieve-answer`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(500);
    expect(res.body.message).toBe('Internal Server Error');
    expect(res.body.error_message).toBe('Test Catch Error');
  });
};

module.exports = retrieveAnswer;
