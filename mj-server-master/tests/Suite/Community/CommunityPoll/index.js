const pollInfo = require('./getPollInfo');
const insertUserPoll = require('./insertUserPoll');

const CommunityPoll = (agent, app) => {
  const spyCustomersFindOne = (data) => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyUserPollsFindOne = (data) => {
    jest
      .spyOn(app.db.userPolls, 'findOne')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyUserPollsFindOneError = () => {
    jest
      .spyOn(app.db.userPolls, 'findOne')
      .mockImplementationOnce(() =>
        Promise.reject({ message: 'Test Catch Error' }),
      );
  };

  const spyUserPollsInsert = (data) => {
    jest
      .spyOn(app.db.userPolls, 'insert')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyUserPollsInsertError = () => {
    jest
      .spyOn(app.db.userPolls, 'insert')
      .mockImplementationOnce(() =>
        Promise.reject({ message: 'Test Catch Error' }),
      );
  };

  const spyPollsFindOne = (data) => {
    jest
      .spyOn(app.db.polls, 'findOne')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyUserPollsFind = (data) => {
    jest
      .spyOn(app.db.userPolls, 'find')
      .mockImplementationOnce(() => ({ toArray: () => Promise.resolve(data) }));
  };

  const spyPollsAggregate = (data) => {
    jest.spyOn(app.db.polls, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyPollsAggregateError = () => {
    jest.spyOn(app.db.polls, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => Promise.reject({ message: 'Test Catch Error' }),
      };
    });
  };

  //API get information vote for polls management
  //GET /api/v4/polls/:pollId/
  describe('API get information vote for polls management', () => {
    pollInfo(agent, app, {
      spyCustomersFindOne,
      spyUserPollsFind,
      spyUserPollsFindOne,
      spyPollsAggregate,
      spyPollsAggregateError,
    });
  });

  // Store customer's answer
  // POST /api/v4/userPoll/insert
  describe("Store customer's answer", () => {
    insertUserPoll(agent, app, {
      spyUserPollsFindOne,
      spyCustomersFindOne,
      spyPollsFindOne,
      spyUserPollsInsert,
      spyUserPollsFindOneError,
      spyUserPollsInsertError,
    });
  });
};

module.exports = CommunityPoll;
