const { user } = require('../../../mocks/vaultFeatures');

const { poll, userPoll } = require('../../../mocks/community');
const ObjectId = require('mongodb').ObjectId;

const insertUserPoll = (agent, app, fn) => {
  const body = {
    pollId: new ObjectId().toString(),
    nameOfAnswer: [{ key: 'option1', name: 'option1' }],
  };

  const {
    spyUserPollsFindOne,
    spyCustomersFindOne,
    spyPollsFindOne,
    spyUserPollsInsert,
    spyUserPollsFindOneError,
    spyUserPollsInsertError,
  } = fn;

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/userPoll/insert`);
    expect(res.status).toEqual(401);
  });

  it('Should return success response', async () => {
    spyUserPollsFindOne(null);
    spyCustomersFindOne(user);
    spyPollsFindOne(poll());
    spyUserPollsInsert({ result: { n: 1 } });
    const res = await agent
      .post(`/api/v4/userPoll/insert`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBe('Success!');
  });

  it('Should return error response (case body not contains required field)', async () => {
    const res = await agent
      .post(`/api/v4/userPoll/insert`)
      .auth(app.token, { type: 'bearer' })
      .send({ nameOfAnswer: [{ key: null, name: 'option1' }] });
    expect(res.status).toBe(400);
    expect(res.body.statusCode).toBe(400);
    expect(res.body.message).toBe('Key or Name Is not valid');
  });

  it('Should return error response (case user already vote)', async () => {
    spyUserPollsFindOne(userPoll);
    const res = await agent
      .post(`/api/v4/userPoll/insert`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('User already vote');
  });

  it('Should return error response (case user not found)', async () => {
    spyUserPollsFindOne(null);
    spyCustomersFindOne(null);
    const res = await agent
      .post(`/api/v4/userPoll/insert`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(404);
    expect(res.body.statusCode).toBe(404);
    expect(res.body.message).toBe('User not found');
  });

  it('Should return error response (case Poll Not found or Expired)', async () => {
    spyUserPollsFindOne(null);
    spyCustomersFindOne(user);
    spyPollsFindOne(null);
    const res = await agent
      .post(`/api/v4/userPoll/insert`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.error.message).toBe('Poll Not found or Expired');
  });

  it('Should return error response (case catch error)', async () => {
    spyUserPollsFindOneError();
    const res = await agent
      .post(`/api/v4/userPoll/insert`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(500);
    expect(res.body.error.message).toBe('Test Catch Error');
  });

  it('Should return error response (case insert error)', async () => {
    spyUserPollsFindOne(null);
    spyCustomersFindOne(user);
    spyPollsFindOne(poll());
    spyUserPollsInsertError();
    const res = await agent
      .post(`/api/v4/userPoll/insert`)
      .auth(app.token, { type: 'bearer' })
      .send(body);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Test Catch Error');
  });
};

module.exports = insertUserPoll;
