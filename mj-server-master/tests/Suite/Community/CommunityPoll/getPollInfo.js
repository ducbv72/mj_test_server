const { user } = require('../../../mocks/vaultFeatures');
const {
  poll,
  userPoll,
  userPollWithPollId,
} = require('../../../mocks/community');
const ObjectId = require('mongodb').ObjectId;

const getPollInfo = (agent, app, fn) => {
  const {
    spyCustomersFindOne,
    spyUserPollsFind,
    spyUserPollsFindOne,
    spyPollsAggregate,
    spyPollsAggregateError,
  } = fn;

  const id = new ObjectId();

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/polls/${id}`);
    expect(res.status).toEqual(401);
  });

  it('Should return success response', async () => {
    spyCustomersFindOne(user);
    spyUserPollsFindOne(userPoll);
    spyPollsAggregate([poll()]);
    spyUserPollsFind(userPollWithPollId);
    const res = await agent
      .get(`/api/v4/polls/${id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.userPoll).toStrictEqual(userPoll);
  });

  it('Should return error response (case customer not found', async () => {
    spyCustomersFindOne(null);
    const res = await agent
      .get(`/api/v4/polls/${id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.statusCode).toBe(400);
    expect(res.body.message).toEqual('User not found');
  });

  it('Should return error response (case poll not found', async () => {
    spyCustomersFindOne(user);
    spyUserPollsFindOne(userPoll);
    spyPollsAggregate([null]);
    const res = await agent
      .get(`/api/v4/polls/${id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toEqual('Poll not found');
  });

  it('Should return catch error', async () => {
    spyCustomersFindOne(user);
    spyUserPollsFindOne(userPoll);
    spyPollsAggregateError();
    const res = await agent
      .get(`/api/v4/polls/${id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(500);
    expect(res.body.message).toBe('Test Catch Error');
  });
};

module.exports = getPollInfo;
