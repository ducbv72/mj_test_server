const GetCommunityProfiles = require('./getCommunityProfiles');
const GetCommunityProfileById = require('./getCommunityProfileById');

const CommunityProfile = (agent, app) => {
  const spyCustomersFindOne = (data) => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyProfilesAggregate = (data) => {
    jest.spyOn(app.db.profiles, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyProfilesAggregateError = () => {
    jest.spyOn(app.db.profiles, 'aggregate').mockImplementationOnce(() => {
      return {
        toArray: () => Promise.reject({ message: 'Test Catch Error' }),
      };
    });
  };

  // Get active community profiles for mobile app
  // GET /api/v4/community-profile
  describe('Get active community profiles for mobile app', () => {
    GetCommunityProfiles(agent, app, {
      spyCustomersFindOne,
      spyProfilesAggregate,
      spyProfilesAggregateError,
    });
  });

  // Get active community profile by id for mobile app
  // GET /api/v4/community-profile/:id
  describe('Get active community profile by id for mobile app', () => {
    GetCommunityProfileById(agent, app, {
      spyCustomersFindOne,
      spyProfilesAggregate,
      spyProfilesAggregateError,
    });
  });
};

module.exports = CommunityProfile;
