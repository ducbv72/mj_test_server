const { user } = require('../../../mocks/vaultFeatures');
const { profile } = require('../../../mocks/community');
const getCommunityProfiles = (agent, app, fn) => {
  const {
    spyCustomersFindOne,
    spyProfilesAggregate,
    spyProfilesAggregateError,
  } = fn;
  const profilesData = [profile(1), profile(2)];

  it('Should return success response (case customer logged)', async () => {
    spyCustomersFindOne(user);
    spyProfilesAggregate(profilesData);
    const res = await agent.get(
      `/api/v4/community-profile?limit=5&user=1&profileType=Brand`,
    );
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeArray().toBeArrayOfSize(profilesData.length);
  });

  it('Should return success response (case headers not contains authorization)', async () => {
    spyProfilesAggregate(profilesData);
    const res = await agent.get(`/api/v4/community-profile`);
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeArray().toBeArrayOfSize(profilesData.length);
  });

  it('Should return catch error', async () => {
    spyProfilesAggregateError();
    const res = await agent.get(`/api/v4/community-profile`);
    expect(res.status).toBe(500);
    expect(res.body.message).toBe('Test Catch Error');
  });
};

module.exports = getCommunityProfiles;
