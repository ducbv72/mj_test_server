const { userWithDiscountCodeUsed, newCustomer } = require('../../mocks/vip');
const discountCodeUsed = (agent, fn) => {
  const { spyCustomersFindOne, spyCustomersFindOneError } = fn;
  const token =
    'N2IyMjYzNzU3Mzc0NmY2ZDY1NzI0OTY0MjIzYTIyMzEyMjJjMjI2NTZkNjE2OTZjMjIzYTIyNzQ2NTczNzQ2NTcyNDA3NDY1NzM3NDJlNjM2ZjZkMjI3ZA==';
  const failToken =
    'N2IyMjYzNzU3Mzc0NmY2ZDY1NzI0OTY0MjIzYTIyMzMzMTMxMzAzMTM0MzQzODM2MzgzNDM4MzYyMjJjMjI2NTZkNjE2OTZjMjIzYTIyNjQ2Zjc2NzU0MDZkNjk2NzY4NzQ3OTZhNjE3ODc4MmU2MzZmNmQyMjdk';
  const email = 'tester@test.com';
  it('Should return success response', async () => {
    spyCustomersFindOne(userWithDiscountCodeUsed);
    const res = await agent.get(
      `/api/v4/discount-code-used/${token}?email=${email}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.data).toBeObject();
    expect(res.body.data.discountCodes)
      .toBeArray()
      .toBeArrayOfSize(userWithDiscountCodeUsed.appliedDiscountCodes.length);
  });

  it('It should return success response (case user not contains appliedDiscountCodes)', async () => {
    spyCustomersFindOne(newCustomer);
    const res = await agent.get(
      `/api/v4/discount-code-used/${token}?email=${email}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.data).toBeObject();
    expect(res.body.data.discountCodes).toBeArray();
  });

  it('Should return error response (case email not match)', async () => {
    spyCustomersFindOne(userWithDiscountCodeUsed);
    const res = await agent.get(
      `/api/v4/discount-code-used/${failToken}?email=${email}`,
    );
    expect(res.status).toBe(403);
    expect(res.body.code).toBe(403);
    expect(res.body.message).toBe('Forbidden');
    expect(res.body.data).toBe(null);
  });

  it('Should return error response (case user not found)', async () => {
    spyCustomersFindOne(null);
    const res = await agent.get(
      `/api/v4/discount-code-used/${token}?email=${email}`,
    );
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('User not found');
    expect(res.body.data).toBe(null);
  });

  it('Should return error response (case not send email in query params)', async () => {
    const res = await agent.get(`/api/v4/discount-code-used/${token}`);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Invalid Parameters');
  });

  it('Should return error response (case catch error)', async () => {
    spyCustomersFindOneError();
    const res = await agent.get(
      `/api/v4/discount-code-used/${token}?email=${email}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
    expect(res.body.message).toBe('Internal Error Server');
  });
};

module.exports = discountCodeUsed;
