const { customerSmileIo, points_transactions } = require('../../mocks/vip');

const transactionHistory = (agent, app, fn) => {
  const {
    nockCustomersSearch,
    nockpointsTransactions,
    nockpointsTransactionsError,
  } = fn;

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/vip/transaction-history`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    nockCustomersSearch(customerSmileIo);
    nockpointsTransactions({ points_transactions });
    const res = await agent
      .get(`/api/v4/vip/transaction-history`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toStrictEqual(points_transactions);
  });

  it('It should return error response (case customer not found)', async () => {
    nockCustomersSearch(null);
    const res = await agent
      .get(`/api/v4/vip/transaction-history?page=1&z=1`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(404);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toEqual('User not found');
  });

  it('It should return error response (case catch error)', async () => {
    nockCustomersSearch(customerSmileIo);
    nockpointsTransactionsError('Test Catch Error');
    const res = await agent
      .get(`/api/v4/vip/transaction-history`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(500);
    expect(res.body.code).toEqual(500);
    expect(res.body.message).toEqual('Internal Error Server');
  });
};

module.exports = transactionHistory;
