const { customerSmileIo } = require('../../mocks/vip');

const changeBirthday = (agent, app, fn) => {
  const { nockCustomersSearch, nockCustomerUpdate, nockCustomersUpdateError } =
    fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/vip/change-birthday`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    nockCustomersSearch(customerSmileIo);
    nockCustomerUpdate({ customer: customerSmileIo });
    const res = await agent
      .post(`/api/v4/vip/change-birthday`)
      .send({ date_of_birth: '2019/02/31' })
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toStrictEqual(customerSmileIo);
  });

  it('It should return error response (case birthday invalid)', async () => {
    const res = await agent
      .post(`/api/v4/vip/change-birthday`)
      .send({ date_of_birth: '31-02-1991' })
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.message).toEqual('Invalid parameters');
  });

  it('It should return error response (case user not found)', async () => {
    nockCustomersSearch(null);
    const res = await agent
      .post(`/api/v4/vip/change-birthday`)
      .send({ date_of_birth: '2019/02/31' })
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(404);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toEqual('User not found');
  });

  it('It should return error response (case catch error)', async () => {
    nockCustomersSearch(customerSmileIo);
    nockCustomersUpdateError('Test Catch Error');
    const res = await agent
      .post(`/api/v4/vip/change-birthday`)
      .send({ date_of_birth: '2019/02/31' })
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(500);
    expect(res.body.code).toEqual(500);
    expect(res.body.message).toEqual('Internal Error Server');
  });
};

module.exports = changeBirthday;
