const { customerSmileIo, activityInfo } = require('../../mocks/vip');

const smileActivities = (agent, app, fn) => {
  const {
    nockCustomersSearch,
    nockActivitiesInsert,
    nockActivitiesInsertError,
  } = fn;

  const data = { activity_token: 'activity_token' };

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/vip/smile-activities`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    nockCustomersSearch(customerSmileIo);
    nockActivitiesInsert({ activity: activityInfo });
    const res = await agent
      .post(`/api/v4/vip/smile-activities`)
      .send(data)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toStrictEqual(activityInfo);
  });

  it('It should return error response (case not send activity)', async () => {
    nockCustomersSearch(customerSmileIo);
    nockActivitiesInsert({ activity: activityInfo });
    const res = await agent
      .post(`/api/v4/vip/smile-activities`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.code).toEqual(400);
    expect(res.body.message).toEqual('Invalid parameters');
  });

  it('It should return error response (case customer not found)', async () => {
    nockCustomersSearch(null);
    nockActivitiesInsert({ activity: activityInfo });
    const res = await agent
      .post(`/api/v4/vip/smile-activities`)
      .send(data)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(404);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toEqual('User not found');
  });

  it('It should return error response (case customer not found)', async () => {
    nockCustomersSearch(customerSmileIo);
    nockActivitiesInsertError('Test Catch Error');
    const res = await agent
      .post(`/api/v4/vip/smile-activities`)
      .send(data)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(500);
    expect(res.body.code).toEqual(500);
    expect(res.body.message).toEqual('Internal Error Server');
  });
};

module.exports = smileActivities;
