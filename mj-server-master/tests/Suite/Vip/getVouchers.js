const {
  customerSmileIo,
  reward_fulfillments,
  customerFindOne,
  newCustomer,
} = require('../../mocks/vip');

const getVouchers = (agent, app, fn) => {
  const {
    nockCustomersSearch,
    spyCustomersFindOne,
    nockRewardFulfillments,
    spyCustomersFindOneError,
  } = fn;

  const data = { activity_token: 'activity_token' };

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/vip/vouchers`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOne(customerFindOne);
    const res = await agent
      .get(`/api/v4/vip/vouchers`)
      .send(data)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeArray();
  });

  it('It should return success response (case user not contains appliedDiscountCodes)', async () => {
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOne(newCustomer);
    const res = await agent
      .get(`/api/v4/vip/vouchers`)
      .send(data)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeArray();
  });

  it('It should return error response (case customer not found)', async () => {
    nockCustomersSearch(null);
    const res = await agent
      .get(`/api/v4/vip/vouchers`)
      .send(data)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(404);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toBe('User not found');
  });

  it('It should return error response (case test catch error)', async () => {
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOneError();
    const res = await agent
      .get(`/api/v4/vip/vouchers`)
      .send(data)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(500);
    expect(res.body.code).toEqual(500);
    expect(res.body.message).toBe('Internal Error Server');
  });
};

module.exports = getVouchers;
