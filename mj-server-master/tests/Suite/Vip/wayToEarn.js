const {
  customerSmileIo,
  rewardPrograms,
  customer_activity_rules,
  referral_rewards,
} = require('../../mocks/vip');

const wayToEarn = (agent, app, fn) => {
  const {
    nockCustomersSearch,
    nockRewardPrograms,
    nockCustomerActivityRules,
    nockReferralRewards,
    nockCustomersSearchError,
  } = fn;

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/vip/ways-to-earn`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    nockCustomersSearch(customerSmileIo);
    nockRewardPrograms(rewardPrograms);
    nockCustomerActivityRules({ customer_activity_rules });
    nockReferralRewards({ referral_rewards });
    const res = await agent
      .get(`/api/v4/vip/ways-to-earn`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data.customer_activity_rules).toBeArray();
    expect(res.body.data.referral_rewards).toBeObject();
    expect(res.body.data.referral_rewards.id).toBe(1);
  });

  it('It should return error response (case customer not found)', async () => {
    nockCustomersSearch({ customers: [] });
    nockRewardPrograms(rewardPrograms);
    nockCustomerActivityRules({ customer_activity_rules });
    nockReferralRewards({ referral_rewards });
    const res = await agent
      .get(`/api/v4/vip/ways-to-earn`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(404);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toBe('User not found');
  });

  it('It should return error response (case customer not found)', async () => {
    nockCustomersSearchError('Test Catch Error');
    nockRewardPrograms(rewardPrograms);
    nockCustomerActivityRules({ customer_activity_rules });
    nockReferralRewards({ referral_rewards });
    const res = await agent
      .get(`/api/v4/vip/ways-to-earn`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(500);
    expect(res.body.code).toEqual(500);
    expect(res.body.message).toBe('Internal Error Server');
  });
};

module.exports = wayToEarn;
