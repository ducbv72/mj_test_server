const {
  customerSmileIo,
  reward_fulfillments,
  discountCode,
  customerFindOne,
} = require('../../mocks/vip');

const pastVouchers = (agent, app, fn) => {
  const {
    nockCustomersSearch,
    spyCustomersFindOne,
    nockRewardFulfillments,
    spyCustomersFindOneError,
    spyShopifyDiscountCode,
    spyShopifyDiscountCodeError,
  } = fn;

  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/vip/past-vouchers`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOne(customerFindOne);
    spyShopifyDiscountCode({ ...discountCode });
    const res = await agent
      .get(`/api/v4/vip/past-vouchers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(2);
  });

  it('It should return success response', async () => {
    reward_fulfillments[0].code = '5a4f75b636e44';
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOne(customerFindOne);
    spyShopifyDiscountCode({ ...discountCode });
    const res = await agent
      .get(`/api/v4/vip/past-vouchers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(2);
  });

  it('It should return error response (shopify error)', async () => {
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOne(customerFindOne);
    spyShopifyDiscountCodeError();
    const res = await agent
      .get(`/api/v4/vip/past-vouchers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(2);
  });

  it('It should return error response (customer not found)', async () => {
    nockCustomersSearch(null);
    const res = await agent
      .get(`/api/v4/vip/past-vouchers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(404);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toBe('User not found');
  });

  it('It should return error response (case catch error)', async () => {
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOneError();
    const res = await agent
      .get(`/api/v4/vip/past-vouchers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(500);
    expect(res.body.code).toEqual(500);
    expect(res.body.message).toBe('Internal Error Server');
  });

  it('It should return success response (case code not match)', async () => {
    reward_fulfillments[0].code = '5a4f75b636e33';
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOne(customerFindOne);
    spyShopifyDiscountCode({ ...discountCode });
    const res = await agent
      .get(`/api/v4/vip/past-vouchers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(2);
  });

  it('It should return success response (case not found appliedDiscountCodes)', async () => {
    const newCustomer = customerFindOne;
    delete newCustomer.appliedDiscountCodes;
    nockCustomersSearch(customerSmileIo);
    nockRewardFulfillments({
      reward_fulfillments,
    });
    spyCustomersFindOne(newCustomer);
    spyShopifyDiscountCode({ ...discountCode });
    const res = await agent
      .get(`/api/v4/vip/past-vouchers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(2);
  });
};

module.exports = pastVouchers;
