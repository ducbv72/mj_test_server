const nock = require('nock');
const Shopify = require('shopify-api-node');

const DiscountCodeUsed = require('./discountCodeUsed');
const WayToEarn = require('./wayToEarn');
const ChangeBirthday = require('./changeBirthday');
const SmileActivities = require('./smileActivities');
const ListVouchers = require('./getVouchers');
const PastVouchers = require('./pastVouchers');
const TransactionHistory = require('./transactionHistory');

const VipModule = (agent, app) => {
  const spyCustomersFindOne = (data) => {
    jest.spyOn(app.db.customers, 'findOne').mockImplementation(() => data);
  };

  const spyCustomersFindOneError = () => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementation(() => Promise.reject(new Error('Test Catch Error')));
  };

  const nockCustomersSearch = (data) => {
    nock('https://api.smile.io/v1/customers/search').get(/.*/).reply(200, data);
  };

  const nockRewardFulfillments = (data) => {
    nock('https://api.smile.io/v1/reward_fulfillments')
      .get(/.*/)
      .reply(200, data);
  };

  const nockRewardPrograms = (data) => {
    nock('https://api.smile.io/v1/reward_programs').get(/.*/).reply(200, data);
  };

  const nockCustomerActivityRules = (data) => {
    nock('https://platform.smile.io/v1/customer_activity_rules')
      .get(/.*/)
      .reply(200, data);
  };

  const nockCustomerUpdate = (data) => {
    nock('https://platform.smile.io/v1/customers/1').put(/.*/).reply(200, data);
  };

  const nockActivitiesInsert = (data) => {
    nock('https://platform.smile.io/v1/activities').post(/.*/).reply(200, data);
  };

  const nockActivitiesInsertError = (message) => {
    nock('https://platform.smile.io/v1/activities')
      .post(/.*/)
      .replyWithError(message);
  };

  const nockCustomersSearchError = (message) => {
    nock('https://mightyjaxx.myshopify.com/api/2021-01/graphql.json')
      .get(/.*/)
      .replyWithError(message);
  };

  const nockpointsTransactions = (data) => {
    nock('https://api.smile.io/v1/points_transactions')
      .get(/.*/)
      .reply(200, data);
  };

  const nockpointsTransactionsError = (message) => {
    nock('https://api.smile.io/v1/points_transactions')
      .get(/.*/)
      .replyWithError(message);
  };

  const nockCustomersUpdateError = (message) => {
    nock('https://platform.smile.io/v1/customers/1')
      .put(/.*/)
      .replyWithError(message);
  };

  const nockReferralRewards = (data) => {
    nock('https://api.smile.io/v1/referral_rewards').get(/.*/).reply(200, data);
  };

  const spyShopifyDiscountCode = (data) => {
    jest
      .spyOn(Shopify.prototype.discountCode, 'lookup')
      .mockImplementation(() => data);
  };

  const spyShopifyDiscountCodeError = () => {
    jest
      .spyOn(Shopify.prototype.discountCode, 'lookup')
      .mockImplementation(() => Promise.reject(new Error('Test Catch Error')));
  };

  // Get redeemed discount codes for a customer by token
  // GET /api/v4/discount-code-used/:token
  describe('Get redeemed discount codes for a customer by token', () => {
    DiscountCodeUsed(agent, { spyCustomersFindOne, spyCustomersFindOneError });
  });

  // Get ways-to-earn
  // GET /api/v4/vip/ways-to-earn
  describe('Get ways to earn point in smile.io', () => {
    WayToEarn(agent, app, {
      nockCustomersSearch,
      nockRewardPrograms,
      nockCustomerActivityRules,
      nockReferralRewards,
      nockCustomersSearchError,
    });
  });

  // Update customer date of birthday
  // POST /api/v4/vip/change-birthday
  describe('Update customer date of birthday', () => {
    ChangeBirthday(agent, app, {
      nockCustomersSearch,
      nockCustomerUpdate,
      nockCustomersUpdateError,
    });
  });

  // Create an smile activity
  // POST /api/v4/vip/smile-activities
  describe('Create an smile activity', () => {
    SmileActivities(agent, app, {
      nockCustomersSearch,
      nockActivitiesInsert,
      nockActivitiesInsertError,
    });
  });

  // Get vouchers for a specific user
  // GET /api/v4/vip/vouchers
  describe('Get vouchers for a specific user', () => {
    ListVouchers(agent, app, {
      nockCustomersSearch,
      spyCustomersFindOne,
      nockRewardFulfillments,
      spyCustomersFindOneError,
    });
  });

  // Get vouchers that have been used by a specific user
  // GET /api/v4/vip/past-vouchers
  describe('Get vouchers that have been used by a specific user', () => {
    PastVouchers(agent, app, {
      nockCustomersSearch,
      nockRewardFulfillments,
      spyCustomersFindOne,
      spyCustomersFindOneError,
      spyShopifyDiscountCode,
      spyShopifyDiscountCodeError,
    });
  });

  // Get point earning history for a specific user
  // GET /api/v4/vip/transaction-history
  describe('Get point earning history for a specific user', () => {
    TransactionHistory(agent, app, {
      nockCustomersSearch,
      nockpointsTransactions,
      nockpointsTransactionsError,
    });
  });
};

module.exports = VipModule;
