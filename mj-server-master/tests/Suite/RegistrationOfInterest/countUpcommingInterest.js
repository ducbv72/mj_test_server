const countUpcommingInterest = (agent, app, fn) => {
  const {
    spyUpcommingProductsFindOne,
    upcommingProductsItem,
    spyUpcommingProductsUpdate,
  } = fn;
  const upcommingProductId = 1;
  it('Should return upcomming product', async () => {
    spyUpcommingProductsFindOne(upcommingProductsItem[0]);
    spyUpcommingProductsUpdate(upcommingProductsItem[0]);
    const res = await agent
      .get(`/api/v4/countUpcommingInterest/${upcommingProductId}`)
      .set('Access-Control-Allow-Origin', '*')
      .set(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept',
      );
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
  });

  it('Should return upcomming product not found', async () => {
    spyUpcommingProductsFindOne(null);
    const res = await agent
      .get(`/api/v4/countUpcommingInterest/2`)
      .set('Access-Control-Allow-Origin', '*')
      .set(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept',
      );
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('Not found');
  });

  it('Should return error with message: "Invalid data"', async () => {
    const dataNotContainCount = upcommingProductsItem[0];
    delete dataNotContainCount.count;
    spyUpcommingProductsFindOne(dataNotContainCount);
    spyUpcommingProductsUpdate(null);
    const res = await agent
      .get(`/api/v4/countUpcommingInterest/2`)
      .set('Access-Control-Allow-Origin', '*')
      .set(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept',
      );
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('Invalid data');
  });
};

module.exports = countUpcommingInterest;
