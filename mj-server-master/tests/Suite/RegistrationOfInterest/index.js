const GetUpcommingProducts = require('./getUpcommingProducts');
const GetAllUpcommingProducts = require('./getAllUpcommingProducts');
const CountUpcommingInterest = require('./countUpcommingInterest');
const DeleteUpcommingProducts = require('./deleteUpcommingProducts');

const upcommingProductsItem = [
  {
    _id: '1',
    title: 'title',
    active: false,
    description: 'description',
    dropDate: '2020-02-22T01:49:00.000+0000',
    pictureUrl: '',
    url: 'url',
    count: 4,
  },
];
const RegistrationOfInterest = (agent, app) => {
  const spyUpcommingProductsFind = (data) => {
    jest.spyOn(app.db.upcommingProducts, 'find').mockImplementationOnce(() => {
      return {
        sort: () => ({
          toArray: () => data,
        }),
      };
    });
  };

  const spyUpcommingProductsFindOne = (data) => {
    jest
      .spyOn(app.db.upcommingProducts, 'findOne')
      .mockImplementationOnce(() => {
        return data;
      });
  };

  const spyUpcommingProductsUpdate = (data) => {
    jest
      .spyOn(app.db.upcommingProducts, 'update')
      .mockImplementationOnce((arg) => {
        return data;
      });
  };

  const spyUpcommingProductsDeleteOne = (data) => {
    jest
      .spyOn(app.db.upcommingProducts, 'deleteOne')
      .mockImplementationOnce(() => {
        return data;
      });
  };

  //Get information on active upcoming products
  //GET /api/v4/getUpcommingProducts
  describe('Get information on active upcoming products', () => {
    GetUpcommingProducts(agent, app, {
      spyUpcommingProductsFind,
      upcommingProductsItem,
    });
  });

  //Get information on all upcoming products
  //GET /api/v4/getAllUpcommingProducts
  describe('Get information on all upcoming products', () => {
    GetAllUpcommingProducts(agent, app, {
      spyUpcommingProductsFind,
      upcommingProductsItem,
    });
  });

  //Get count of how many people clicked to create a calendar invite for a specific upcoming product
  //GET /api/v4/countUpcommingInterest/:id
  describe('Get count of how many people clicked to create a calendar invite for a specific upcoming product', () => {
    CountUpcommingInterest(agent, app, {
      spyUpcommingProductsFindOne,
      upcommingProductsItem,
      spyUpcommingProductsUpdate,
    });
  });

  //Delete information on a specific upcoming product
  //DELETE /api/v4/deleteUpcommingProducts/:id
  describe('Delete information on a specific upcoming product', () => {
    DeleteUpcommingProducts(agent, app, {
      spyUpcommingProductsFindOne,
      spyUpcommingProductsDeleteOne,
      upcommingProductsItem,
    });
  });
};
module.exports = RegistrationOfInterest;
