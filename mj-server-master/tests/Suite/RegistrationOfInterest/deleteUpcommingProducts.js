const deleteUpcommingProducts = (agent, app, fn) => {
  const upcommingProductId = 1;
  const {
    spyUpcommingProductsFindOne,
    spyUpcommingProductsDeleteOne,
    upcommingProductsItem,
  } = fn;
  it('Should return deleted', async () => {
    spyUpcommingProductsFindOne(upcommingProductsItem[0]);
    spyUpcommingProductsDeleteOne({});
    const res = await agent.delete(
      `/api/v4/deleteUpcommingProducts/${upcommingProductId}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
  });

  it('Should return upcomming product not found', async () => {
    spyUpcommingProductsFindOne(null);
    const res = await agent.delete(`/api/v4/deleteUpcommingProducts/12345`);
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('Not found');
  });
};

module.exports = deleteUpcommingProducts;
