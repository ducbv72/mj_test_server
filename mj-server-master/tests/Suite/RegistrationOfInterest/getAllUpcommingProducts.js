const getAllUpcommingProducts = (agent, app, fn) => {
  const { spyUpcommingProductsFind, upcommingProductsItem } = fn;
  it('Should return upcomming products', async () => {
    spyUpcommingProductsFind(upcommingProductsItem);
    const res = await agent.get('/api/v4/getAllUpcommingProducts');
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeArray();
    expect(res.body.data).toStrictEqual(upcommingProductsItem);
  });
};

module.exports = getAllUpcommingProducts;
