/* eslint-disable no-undef */
const RegistrationOfInterest = (agent, app) => {
  const upcommingProductsItem = [
    {
      _id: '1',
      title: 'title',
      active: false,
      description: 'description',
      dropDate: '2020-02-22T01:49:00.000+0000',
      pictureUrl: '',
      url: 'url',
      count: 4,
    },
  ];
  const spyUpcommingProductsFind = (data) => {
    jest.spyOn(app.db.upcommingProducts, 'find').mockImplementationOnce(() => {
      return {
        sort: () => ({
          toArray: () => data,
        }),
      };
    });
  };

  const spyUpcommingProductsFindOne = (data) => {
    jest.spyOn(app.db.upcommingProducts, 'findOne').mockImplementation(() => {
      return data;
    });
  };


  //Delete information on a specific upcoming product
  //DELETE /api/v4/deleteUpcommingProducts/:id
  describe('Delete information on a specific upcoming product', () => {
    it('Should return deleted', async () => {
      const res = await agent.delete(
        `/api/v4/deleteUpcommingProducts/${upcommingProductId}`,
      );
      expect(res.status).toBe(200);
      expect(res.body.message).toBe('Deleted');
    });

    it('Should return upcomming product not found', async () => {
      const res = await agent.delete(`/api/v4/deleteUpcommingProducts/12345`);
      expect(res.status).toBe(400);
      expect(res.body.message).toBe('Not found');
    });
  });
};
module.exports = RegistrationOfInterest;
