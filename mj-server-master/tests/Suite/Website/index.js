/* eslint-disable no-undef */

const RegistrationOfInterest = require('../RegistrationOfInterest');
const StockXAPI = require('stockx-api');
const nock = require('nock');
const StockXbySku = require('../Products/stockXbySku');
const Zendesksync = require('./zendesksync');

const Website = (agent, config, app) => {
  const zendeskUrl =
    'https://mightyjaxx.zendesk.com/api/v2/help_center/en-us/sections/360000589673/articles.json';

  const spyProductsFindOne = (sku, data) => {
    jest.spyOn(app.db.products, 'findOne').mockImplementation((arg) => {
      if (arg.productVariantSku === sku) {
        return data;
      }
      if (Object.keys(arg)[0] === 'productTitle') {
        return data;
      }
      return null;
    });
  };

  const spyProductsUpdateOne = (data) => {
    jest.spyOn(app.db.products, 'updateOne').mockImplementation(() => {
      return data;
    });
  };

  const spyStockXapi = (data, isError, errorMessage) => {
    jest
      .spyOn(StockXAPI.prototype, 'newSearchProducts')
      .mockImplementation(() => {
        if (isError) {
          return Promise.reject(new Error(errorMessage));
        }
        return data;
      });
  };

  const nockZendesk = (status, products) => {
    nock(zendeskUrl).persist().get(/.*/).reply(status, products);
  };

  //sync zendesk order link
  //POST /api/v4/zendesksync
  describe('Should return sync success', () => {
    Zendesksync(agent, zendeskUrl, {
      nockZendesk,
      spyProductsFindOne,
      spyProductsUpdateOne,
    });
  });

  describe('Registration Of Interest', () => {
    RegistrationOfInterest(agent, app);
  });
};
module.exports = Website;
