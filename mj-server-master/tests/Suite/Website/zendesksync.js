const { product } = require('../../mocks/E-Commerce');
const zendesksync = (agent, zendeskUrl, fn) => {
  const { nockZendesk, spyProductsFindOne, spyProductsUpdateOne } = fn;
  it('Should return sync success', async () => {
    nockZendesk(200, {
      next_page: null,
      articles: [{ title: 'title', id: '1', html_url: 'html_url' }],
    });
    spyProductsFindOne(null, product(1));
    spyProductsUpdateOne(product(1));
    const res = await agent.post(`/api/v4/zendesksync`);
    expect(res.status).toBe(200);
  });

  it('Should return sync success (case next page)', async () => {
    nockZendesk(200, {
      next_page: null,
      articles: [{ title: 'title', id: '1', html_url: 'html_url' }],
    });
    spyProductsFindOne();
    spyProductsUpdateOne(product(1));
    const res = await agent.post(`/api/v4/zendesksync`);
    expect(res.status).toBe(200);
  });
};

module.exports = zendesksync;
