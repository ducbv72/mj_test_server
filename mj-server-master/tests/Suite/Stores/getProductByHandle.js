const {
  shopifyProduct,
  dbProducts,
  dbProductNotFindProd,
} = require('../../mocks/stores');

const GetProductByHandle = (agent, app, fn) => {
  const handleSpecial = 'mighty-allstars-f1';
  const handle = 'all';
  const after = null;
  const {
    spyShopifyGrapql,
    spyShopifyGrapqlError,
    spyFindProduct,
    nockShopifyProduct,
    nockShopifyProductError,
  } = fn;

  it('Should return success response', async () => {
    spyShopifyGrapql(shopifyProduct());
    spyFindProduct(dbProducts);
    const res = await agent.get(
      `/api/v4/stores/${handleSpecial}/products?after=${after}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(4);
  });

  it('Should return success response (case not have same product)', async () => {
    spyShopifyGrapql(shopifyProduct());
    spyFindProduct(dbProductNotFindProd);
    const res = await agent.get(
      `/api/v4/stores/${handleSpecial}/products?after=${after}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(4);
  });

  it('Should return success response (case has next page)', async () => {
    spyShopifyGrapql(shopifyProduct(true));
    spyShopifyGrapql(shopifyProduct(false));
    spyFindProduct(dbProducts);
    const res = await agent.get(
      `/api/v4/stores/${handleSpecial}/products?after=${after}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(8);
  });

  it('Should return success response (case handle is not special case)', async () => {
    nockShopifyProduct({ data: shopifyProduct() });
    spyFindProduct(dbProducts);
    const res = await agent.get(`/api/v4/stores/${handle}/products`);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(4);
  });

  it('Should return success response (case collectionByHandle: null)', async () => {
    nockShopifyProduct({ data: { collectionByHandle: null } });
    spyFindProduct(dbProducts);
    const res = await agent.get(
      `/api/v4/stores/${handle}/products?after=${after}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  });

  it('Should return success response (case return errors)', async () => {
    nockShopifyProduct({ errors: [{ message: 'Error' }] });
    spyFindProduct(dbProducts);
    const res = await agent.get(
      `/api/v4/stores/${handle}/products?after=${after}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
    expect(res.body.msg).toBe('Invalid');
  });

  it('Should return error response (case catch error)', async () => {
    nockShopifyProductError('Test Catch Error');
    const res = await agent.get(
      `/api/v4/stores/${handle}/products?after=${after}`,
    );
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
    expect(res.body.msg).toBe('Internal Error Server');
  });
};

module.exports = GetProductByHandle;
