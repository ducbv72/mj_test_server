const { listStores } = require('../../mocks/stores');

const ListStores = (agent, app, fn) => {
  const { spyFindStores, spyFindStoresError } = fn;
  it('Should return success response', async () => {
    spyFindStores(listStores);
    const res = await agent.get(`/api/v4/stores`);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.msg).toBe('ok');
    expect(res.body.data).toStrictEqual(listStores);
  });

  it('Should return error response (case test catch error)', async () => {
    spyFindStoresError();
    const res = await agent.get(`/api/v4/stores`);
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
    expect(res.body.msg).toBe('Internal Error Server');
  });
};

module.exports = ListStores;
