const { storefrontProducts, dbProducts } = require('../../mocks/stores');

const searchProduct = (agent, app, fn) => {
  let data = {
    after: null,
    sortBy: 'TITLE',
    reverse: false,
    query: 'tag:Mighty AllStars',
  };

  const { nockShopifyProduct, nockShopifyProductError, spyFindProduct } = fn;

  it('Should return success response', async () => {
    nockShopifyProduct(storefrontProducts);
    spyFindProduct(dbProducts);
    const res = await agent.post(`/api/v4/stores/searchProducts`).send(data);
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(3);
  });

  it('Should return success response (case collectionByHandle: [])', async () => {
    nockShopifyProduct(null);
    spyFindProduct(dbProducts);
    const res = await agent.post(`/api/v4/stores/searchProducts`).send(data);
    expect(res.status).toBe(200);
    expect(res.body.data).toBeArray().toBeArrayOfSize(0);
  });

  it('Should return error response (case catch error)', async () => {
    data.after = 'after';
    data.query = null;
    const message = 'Test Catch Error';
    nockShopifyProductError(message);
    const res = await agent.post(`/api/v4/stores/searchProducts`).send(data);
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.msg).toBe(message);
  });
};

module.exports = searchProduct;
