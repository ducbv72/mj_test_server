/* eslint-disable no-undef */
const Shopify = require('shopify-api-node');
const nock = require('nock');
const ListStores = require('./listStores');
const ProductByHandle = require('./getProductByHandle');
const SearchProduct = require('./searchProduct');

const StoreModule = (agent, app) => {
  const spyFindProduct = (data) => {
    jest.spyOn(app.db.products, 'find').mockImplementation(() => ({
      sort: () => ({
        toArray: () => data,
      }),
    }));
  };

  const spyFindStores = (data) => {
    jest.spyOn(app.db.stores, 'find').mockImplementationOnce(() => ({
      toArray: () => data,
    }));
  };

  const spyFindStoresError = () => {
    jest.spyOn(app.db.stores, 'find').mockImplementationOnce(() => ({
      toArray: () => Promise.reject(new Error('Test Catch Error')),
    }));
  };

  const spyShopifyGrapql = (data) => {
    jest
      .spyOn(Shopify.prototype, 'graphql')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyShopifyGrapqlError = () => {
    jest
      .spyOn(Shopify.prototype, 'graphql')
      .mockImplementationOnce(() =>
        Promise.reject(new Error('Test Catch Error')),
      );
  };

  const nockShopifyProduct = (data) => {
    nock('https://mightyjaxx.myshopify.com/api/2021-01/graphql.json')
      .persist()
      .post(/.*/)
      .reply(200, data);
  };

  const nockShopifyProductError = (message) => {
    nock('https://mightyjaxx.myshopify.com/api/2021-01/graphql.json')
      .post(/.*/)
      .replyWithError(message);
  };

  // Get list of stores
  // GET /api/v4/stores
  describe('Get list of stores', () => {
    ListStores(agent, app, { spyFindStores, spyFindStoresError });
  });

  // Get list of product by handle
  // GET /api/v4/stores/:handle/products
  describe('Get list of product by handle', () => {
    ProductByHandle(agent, app, {
      spyShopifyGrapql,
      spyShopifyGrapqlError,
      spyFindProduct,
      nockShopifyProduct,
      nockShopifyProductError,
    });
  });

  // Search a product in stores
  // GET /api/v4/stores/searchProducts
  describe('Search a product in stores', () => {
    SearchProduct(agent, app, {
      nockShopifyProduct,
      nockShopifyProductError,
      spyFindProduct,
    });
  });
};

module.exports = StoreModule;
