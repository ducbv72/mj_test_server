const { exclusiveTags } = require('../../mocks/exclusiveSale');

const exclusiveTag = (agent, app, fn) => {
  const { spyExclusiveSaleFind, spyExclusiveSaleFindError } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/exclusive-sale-tag`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    spyExclusiveSaleFind([exclusiveTags]);
    const res = await agent
      .get(`/api/v4/exclusive-sale-tag`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.message).toEqual('ok');
    expect(res.body.data).toBeArray().toStrictEqual([exclusiveTags]);
  });

  it('It should return error (case test catch error)', async () => {
    const message = 'Test Catch Error';
    spyExclusiveSaleFindError();
    const res = await agent
      .get(`/api/v4/exclusive-sale-tag`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(500);
    expect(res.body.message).toEqual(message);
  });
};

module.exports = exclusiveTag;
