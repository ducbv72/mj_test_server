const { nyamsumTag } = require('../../mocks/exclusiveSale');

const exclusiveV2 = (agent, app, fn) => {
  const { spyAggregateProducts, nockShopifyProduct, nockShopifyProductError } =
    fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/exclusive-sale-v2`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response', async () => {
    nockShopifyProduct(nyamsumTag);
    spyAggregateProducts([{ _id: '5395326465', productArtist: 'testArtist' }]);
    const res = await agent
      .get(`/api/v4/exclusive-sale-v2`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.statusCode).toEqual(200);
    expect(res.body.result).toBeArray();
  });

  it('It should return success response (case not found artist)', async () => {
    nockShopifyProduct(nyamsumTag);
    spyAggregateProducts([{ _id: '1', productArtist: 'testArtist' }]);
    const res = await agent
      .get(`/api/v4/exclusive-sale-v2`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.statusCode).toEqual(200);
    expect(res.body.result).toBeArray();
  });

  it('It should return error response (case product not found)', async () => {
    nockShopifyProduct(nyamsumTag);
    spyAggregateProducts(null);
    const res = await agent
      .get(`/api/v4/exclusive-sale-v2`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(400);
    expect(res.body.message).toBe('Cannot find this product in database');
  });

  it('It should return error response (case catch error)', async () => {
    const messageError = 'Test Catch Error';
    nockShopifyProductError(messageError);
    spyAggregateProducts(null);
    const res = await agent
      .get(`/api/v4/exclusive-sale-v2`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.statusCode).toEqual(500);
    expect(res.body.message).toBe('Internal Error Server');
    expect(res.body.error_message).toBe(messageError);
  });
};

module.exports = exclusiveV2;
