const nock = require('nock');
const admin = require('firebase-admin');
const Shopify = require('shopify-api-node');
const ExclusiveTag = require('./exclusiveTag');
const ExclusiveV2 = require('./exclusiveV2');

const ECommerce = (agent, app) => {
  //Defined evironment for all suite
  const spyExclusiveSaleFind = (data) => {
    jest
      .spyOn(app.db.exclusiveSale, 'find')
      .mockImplementation(() => ({ sort: () => ({ toArray: () => data }) }));
  };

  const spyExclusiveSaleFindError = () => {
    jest.spyOn(app.db.exclusiveSale, 'find').mockImplementation(() => ({
      sort: () => ({
        toArray: () => Promise.reject(new Error('Test Catch Error')),
      }),
    }));
  };

  const spyAggregateProducts = (data) => {
    jest
      .spyOn(app.db.products, 'aggregate')
      .mockImplementation(() => ({ toArray: () => data }));
  };

  const nockShopifyProduct = (data) => {
    nock('https://mightyjaxx.myshopify.com/api/2021-01/graphql.json')
      .persist()
      .post(/.*/)
      .reply(200, data);
  };

  const nockShopifyProductError = (message) => {
    nock('https://mightyjaxx.myshopify.com/api/2021-01/graphql.json')
      .post(/.*/)
      .replyWithError(message);
  };

  //Get exclusive sale tag
  //GET /api/v4/exclusive-sale-tag
  describe('Get exclusive sale tag', () => {
    ExclusiveTag(agent, app, {
      spyExclusiveSaleFind,
      spyExclusiveSaleFindError,
    });
  });

  //Get exclusive items
  //GET /api/v4/exclusive-sale-v2
  describe('Get exclusive items', () => {
    ExclusiveV2(agent, app, {
      spyAggregateProducts,
      nockShopifyProduct,
      nockShopifyProductError,
    });
  });
};

module.exports = ECommerce;
