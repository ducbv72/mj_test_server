const {
  getProducts,
  stores,
  shopifyTagExclusiveSale,
} = require('../../mocks/E-Commerce');

const getAllSpecialData = (agent, app, fn) => {
  const {
    spyFindProduct,
    spyFindStores,
    spyExclusiveSaleAggregate,
    spyShopifyCustomer,
  } = fn;
  it('It should return data for home screen', async () => {
    spyFindProduct(getProducts);
    spyFindStores(stores);
    spyExclusiveSaleAggregate([]);
    const res = await agent.get(`/api/v4/getAllSpecialData`);
    expect(res.status).toBe(200);
    expect(res.body.message).toEqual('ok');
    expect(res.body.data.LunchList).toBeArray();
    expect(res.body.data.SoldOutList).toBeArray();
    expect(res.body.data.artists).toBeArray();
    expect(res.body.data.StoreList).toBeArray();
  });

  it("It should return data for home screen (Case user's tags contain exclusive-mobile-nyansum)", async () => {
    spyFindProduct(getProducts);
    spyFindStores(stores);
    spyShopifyCustomer(shopifyTagExclusiveSale(true));
    spyExclusiveSaleAggregate([{ productTag: 'tags' }]);
    const res = await agent
      .get(`/api/v4/getAllSpecialData`)
      .set({ authorization: `Bearer ${app.token}` });
    expect(res.status).toBe(200);
    expect(res.body.message).toEqual('ok');
    expect(res.body.data.LunchList).toBeArray();
    expect(res.body.data.SoldOutList).toBeArray();
    expect(res.body.data.artists).toBeArray();
    expect(res.body.data.StoreList).toBeArray();
  });

  it("It should return data for home screen (Case user's tags contain exclusive-mobile-nyansum and tag not in productTag)", async () => {
    spyFindProduct(getProducts);
    spyFindStores(stores);
    spyShopifyCustomer(shopifyTagExclusiveSale(true));
    spyExclusiveSaleAggregate([{ productTag: 'testTag' }]);
    const res = await agent
      .get(`/api/v4/getAllSpecialData`)
      .set({ authorization: `Bearer ${app.token}` });
    expect(res.status).toBe(200);
    expect(res.body.message).toEqual('ok');
    expect(res.body.data.LunchList).toBeArray();
    expect(res.body.data.SoldOutList).toBeArray();
    expect(res.body.data.artists).toBeArray();
    expect(res.body.data.StoreList).toBeArray();
  });

  it('It should return data for home screen (Case authorization undefined or null)', async () => {
    spyFindProduct(getProducts);
    spyFindStores(stores);
    spyExclusiveSaleAggregate([{ productTag: 'testTag' }]);
    const res = await agent
      .get(`/api/v4/getAllSpecialData`)
      .set({ authorization: `undefined` });
    expect(res.status).toBe(200);
    expect(res.body.message).toEqual('ok');
    expect(res.body.data.LunchList).toBeArray();
    expect(res.body.data.SoldOutList).toBeArray();
    expect(res.body.data.artists).toBeArray();
    expect(res.body.data.StoreList).toBeArray();
  });
};

module.exports = getAllSpecialData;
