const { getProducts, stores } = require('../../mocks/E-Commerce');

const home = (agent, app, fn) => {
  const { spyFindProduct, spyFindStores } = fn;
  it('It should return data for home screen', async () => {
    spyFindProduct(getProducts);
    spyFindStores(stores);
    const res = await agent.get(`/api/v4/home`);
    expect(res.status).toBe(200);
    expect(res.body.message).toEqual('ok');
    expect(res.body.data.ProductFeature).toBeArray().toBeArrayOfSize(2);
    expect(res.body.data.ProductRecommend).toBeArray().toBeArrayOfSize(2);
    expect(res.body.data.StoreList).toBeArray().toBeArrayOfSize(1);
    expect(res.body.data.HomePopup).toBeArray().toBeArrayOfSize(0);
  });
};

module.exports = home;
