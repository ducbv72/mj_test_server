/* eslint-disable prettier/prettier */
const Shopify = require('shopify-api-node');
const apiHome = require('./home');
const GetAllSpecialData = require('./getAllSpecialData');

const Home = (agent, app) => {
  const spyFindProduct = (fn) => {
    jest.spyOn(app.db.products, 'find').mockImplementation((arg) => ({
      sort: () => ({
        toArray: () => fn(arg),
      }),
    }));
  };

  const spyExclusiveSaleAggregate = (data) => {
    jest.spyOn(app.db.exclusiveSale, 'aggregate').mockImplementation(() => ({
      toArray: () => data,
    }));
  };

  const spyFindStores = (data) => {
    jest.spyOn(app.db.stores, 'find').mockImplementationOnce(() => ({
      toArray: () => data,
    }));
  };

  const spyShopifyCustomer = (data) => {
    jest
      .spyOn(Shopify.prototype.customer, 'get')
      .mockImplementation(() => data);
  };

  // Get sold out and launch date data for products
  // GET /api/v4/home
  describe('It should return data for home screen', () => {
    apiHome(agent, app, { spyFindProduct, spyFindStores });
  });

  //Get All Special Data
  //GET /api/v4/getAllSpecialData
  describe('Should return All Special Data', () => {
    GetAllSpecialData(agent, app, {
      spyFindProduct,
      spyFindStores,
      spyExclusiveSaleAggregate,
      spyShopifyCustomer,
    });
  });
};

module.exports = Home;
