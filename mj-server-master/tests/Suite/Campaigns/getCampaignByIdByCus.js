const { campaign, user } = require('../../mocks/vaultFeatures');

const getCampaignByIdByCus = (agent, app, fn) => {
  const { spyCustomersFindOne, spyCampaignsFindOne } = fn;
  const campaign_id = 1;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/campaignByCustomer/${campaign_id}`);
    expect(res.status).toEqual(401);
  });

  it('Should return all campaigns', async () => {
    spyCustomersFindOne(user);
    spyCampaignsFindOne(campaign);
    const res = await agent
      .get(`/api/v4/campaignByCustomer/${campaign_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.campaign).toBeObject();
    expect(res.body.campaign.id).toBe(campaign_id);
    expect(res.body.campaign.scan_count >= 0).toBeTruthy();
  });

  it('Should return error with message: "Campaign not found"', async () => {
    spyCustomersFindOne(user);
    spyCampaignsFindOne(null);
    const res = await agent
      .get(`/api/v4/campaignByCustomer/${campaign_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Campaign not found');
  });

  it('Should return all campaigns (case not found user)', async () => {
    spyCustomersFindOne(null);
    spyCampaignsFindOne(campaign);
    const res = await agent
      .get(`/api/v4/campaignByCustomer/${campaign_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.campaign).toStrictEqual(campaign);
  });
};

module.exports = getCampaignByIdByCus;
