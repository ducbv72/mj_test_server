const { campaign, campaignParticipates } = require('../../mocks/vaultFeatures');
const { user } = require('../../mocks/vaultFeatures');

const GetCampaignsByCustomer = (agent, app, fn) => {
  const {
    spyCustomersFindOne,
    spyCampaignsFind,
    spyCampaignParticipatesAggregate,
    spyCampaignParticipatesFind,
  } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/campaignsByCustomer`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response (customer in database)', async () => {
    spyCustomersFindOne(user);
    spyCampaignsFind([campaign]);
    spyCampaignParticipatesAggregate([campaignParticipates]);
    spyCampaignParticipatesFind([campaignParticipates]);
    const res = await agent
      .get(`/api/v4/campaignsByCustomer`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.campaigns).toBeArray().toBeArrayOfSize(1);
  });

  it('It should return success response (case campaigns and scanningcount have same item)', async () => {
    campaign.id = '1';
    spyCustomersFindOne(user);
    spyCampaignsFind([campaign]);
    spyCampaignParticipatesAggregate([campaignParticipates]);
    spyCampaignParticipatesFind([campaignParticipates]);
    const res = await agent
      .get(`/api/v4/campaignsByCustomer`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.campaigns).toBeArray().toBeArrayOfSize(1);
  });

  it('It should return success response (case lastScanning is empty)', async () => {
    campaign.id = '1';
    spyCustomersFindOne(user);
    spyCampaignsFind([campaign]);
    spyCampaignParticipatesAggregate([campaignParticipates]);
    spyCampaignParticipatesFind([]);
    const res = await agent
      .get(`/api/v4/campaignsByCustomer`)
      .auth(app.token, { type: 'bearer' });
    campaign.id = 1;
    expect(res.status).toEqual(200);
    expect(res.body.campaigns).toBeArray().toBeArrayOfSize(1);
  });

  it('It should return success response (customer not found)', async () => {
    spyCustomersFindOne(null);
    spyCampaignsFind([campaign]);
    spyCampaignParticipatesAggregate([campaignParticipates]);
    spyCampaignParticipatesFind([campaignParticipates]);
    const res = await agent
      .get(`/api/v4/campaignsByCustomer`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.campaigns).toBeArray();
    expect(res.body.campaigns).toEqual([campaign]);
  });
};

module.exports = GetCampaignsByCustomer;
