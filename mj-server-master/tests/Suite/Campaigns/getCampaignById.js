const { campaign } = require('../../mocks/vaultFeatures');

const getCampaignById = (agent, app, spyCampaignsFindOne) => {
  const campaign_id = 1;

  it('Should return all campaigns', async () => {
    spyCampaignsFindOne(campaign);
    const res = await agent
      .get(`/api/v4/campaign/${campaign_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.campaign).toBeObject();
    expect(res.body.campaign.id).toBe(campaign_id);
  });

  it('Should return campaigns not found', async () => {
    spyCampaignsFindOne(null);
    const res = await agent
      .get(`/api/v4/campaign/99`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('Campaign not found');
  });
};

module.exports = getCampaignById;
