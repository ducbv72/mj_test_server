const GetCampaignById = require('./getCampaignById');
const GetCampaignByIdByCus = require('./getCampaignByIdByCus');
const GetCampaignsByCustomer = require('./getCampaignsByCustomer');
const GetCampaigns = require('./getCampaigns');

const CampaignsModule = (agent, app) => {
  const spyCustomersFindOne = (data) => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyCampaignsFind = (data) => {
    jest.spyOn(app.db.campaigns, 'find').mockImplementationOnce(() => {
      return {
        toArray: () => data,
      };
    });
  };

  const spyCampaignsFindOne = (data) => {
    jest.spyOn(app.db.campaigns, 'findOne').mockImplementationOnce(() => {
      return data;
    });
  };

  const spyCampaignParticipatesAggregate = (data) => {
    jest
      .spyOn(app.db.campaignParticipates, 'aggregate')
      .mockImplementationOnce(() => {
        return {
          toArray: () => data,
        };
      });
  };

  const spyCampaignParticipatesFind = (data) => {
    jest
      .spyOn(app.db.campaignParticipates, 'find')
      .mockImplementationOnce(() => {
        return {
          sort: () => ({
            limit: () => ({
              toArray: () => data,
            }),
          }),
        };
      });
  };

  //Get active campaigns
  //GET /api/v4/campaigns
  describe('Should return active campaigns', () => {
    GetCampaigns(agent, spyCampaignsFind);
  });

  //Get details of specific campaign
  //GET /api/v4/campaign/:campaign_id
  describe('Get details of specific campaign', () => {
    GetCampaignById(agent, app, spyCampaignsFindOne);
  });

  //Get details of specific campaign including customer participation information
  //GET /api/v4/campaignByCustomer/:campaign_id
  describe('Get details of specific campaign including customer participation information', () => {
    GetCampaignByIdByCus(agent, app, {
      spyCustomersFindOne,
      spyCampaignsFindOne,
    });
  });

  //Get active campaigns including customer participation information
  //GET /api/v4/campaignsByCustomer
  describe('Get active campaigns including customer participation information', () => {
    GetCampaignsByCustomer(agent, app, {
      spyCustomersFindOne,
      spyCampaignsFind,
      spyCampaignParticipatesAggregate,
      spyCampaignParticipatesFind,
    });
  });
};

module.exports = CampaignsModule;
