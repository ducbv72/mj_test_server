const { campaign } = require('../../mocks/vaultFeatures');
const getCampaigns = (agent, spyCampaignsFind) => {
  it('Should return active campaigns', async () => {
    spyCampaignsFind([campaign]);
    const res = await agent.get('/api/v4/campaigns');
    expect(res.status).toBe(200);
    expect(res.body.campaigns).toBeArray().toBeArrayOfSize(1);
  });
};
module.exports = getCampaigns;
