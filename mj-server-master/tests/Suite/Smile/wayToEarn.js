const wayToEarn = (agent) => {
  it('Get ways to earn VIP points from Smile', async () => {
    const res = await agent.get('/api/v4/smile-io/way-to-earn');
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeArray().toBeArrayOfSize(6);
  });
};
module.exports = wayToEarn;
