const nock = require('nock');
const ListPointProduct = require('./listPointProduct');
const SmileCustomerInfo = require('./smileCustomerInfo');
const WayToEarn = require('./wayToEarn');
const PurchasePointProduct = require('./purchasePointProduct');

const Smile = (agent, app) => {
  const smileList = (status, products) => {
    nock(`https://api.smile.io/v1/points_products`)
      .get(/.*/)
      .reply(status, products);
  };

  const purchasePointProduct = (status, products) => {
    nock(`https://api.smile.io/v1/points_products`)
      .post(/.*/)
      .reply(status, products);
  };

  const pointsTransactions = (status, products) => {
    nock(`https://api.smile.io/v1/points_transactions?customer_id=1`)
      .get(/.*/)
      .reply(status, products);
  };

  const purchasePointProductError = () => {
    nock(`https://api.smile.io/v1/points_products`)
      .post(/.*/)
      .replyWithError('Test error');
  };

  const customerInfo = (status, products) => {
    nock(`https://api.smile.io/v1/customers/search`)
      .get(/.*/)
      .reply(status, products);
  };

  const customerInfoError = (message) => {
    nock(`https://api.smile.io/v1/customers/search`)
      .get(/.*/)
      .replyWithError(message);
  };

  const reward_programs = (status, products) => {
    nock(`https://api.smile.io/v1/reward_programs`)
      .get(/.*/)
      .reply(status, products);
  };
  //Get list point product
  //GET /api/v4/smile-io/list-point-product
  describe('List point product', () => {
    ListPointProduct(agent, smileList);
  });

  //Get smile info for this customer
  //GET /api/v4/smile-io/customers
  describe('Get smile info for this customer', () => {
    SmileCustomerInfo(agent, app, {
      customerInfo,
      reward_programs,
      customerInfoError,
      pointsTransactions,
    });
  });

  //Get ways to earn VIP points from Smile
  //GET /api/v4/smile-io/way-to-earn
  describe('Get ways to earn VIP points from Smile', () => {
    WayToEarn(agent);
  });

  //Send request to smile to redeem a voucher
  //POST /api/v4/smile-io/purchase-point-product
  describe('Send request to smile to redeem a voucher', () => {
    PurchasePointProduct(
      agent,
      app,
      purchasePointProduct,
      purchasePointProductError,
    );
  });
};

module.exports = Smile;
