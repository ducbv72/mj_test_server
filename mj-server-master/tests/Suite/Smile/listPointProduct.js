const { listProduct } = require('../../mocks/smile');
const listPointProduct = (agent, smileList) => {
  it('Get list point product (isDisable: true)', async () => {
    smileList(200, listProduct);
    const res = await agent.get(`/api/v4/smile-io/list-point-product`);
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeArray();
  });

  it('Get list point product (isDisable: false)', async () => {
    const isDisable = false;
    smileList(200, listProduct);
    const res = await agent.get(
      `/api/v4/smile-io/list-point-product?isDisable=${isDisable}`,
    );
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data)
      .toBeArray()
      .toBeArrayOfSize(listProduct.points_products.length);
  });

  it('Should return product not found (Found 0 products)', async () => {
    smileList(200, { points_products: [] });
    const res = await agent.get(`/api/v4/smile-io/list-point-product`);
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('Not found');
    expect(res.body.data).toBe(null);
  });

  it('Should return product not found (fetch error)', async () => {
    smileList(500, 'Something went wrong');
    const res = await agent.get(`/api/v4/smile-io/list-point-product`);
    expect(res.status).toBe(404);
    expect(res.body.message).toBe('Not found');
    expect(res.body.data).toBe(null);
  });
};

module.exports = listPointProduct;
