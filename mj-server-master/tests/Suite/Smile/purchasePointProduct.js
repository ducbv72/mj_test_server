const { points_purchase } = require('../../mocks/smile');
const purchasePointProduct = (
  agent,
  app,
  purchasePointProduct,
  purchasePointProductError,
) => {
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/smile-io/purchase-point-product`);
    expect(res.status).toEqual(401);
  });
  it('Should return success response', async () => {
    purchasePointProduct(200, points_purchase);
    const res = await agent
      .post(`/api/v4/smile-io/purchase-point-product`)
      .auth(app.token, { type: 'bearer' })
      .send({
        point_product_id: '1',
        points_to_spend: '1',
        customer_id: '1',
        store_font_key: 'store_font_key',
      });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data.points_spent).toBe(points_purchase.points_spent);
  });

  it('Should return success response', async () => {
    const res = await agent
      .post(`/api/v4/smile-io/purchase-point-product`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(412);
    expect(res.body.message).toBe('Validation failed');
  });

  it('Should return error to purchase', async () => {
    const errorData = { error: { message: 'Test error' } };
    purchasePointProduct(400, errorData);
    const res = await agent
      .post(`/api/v4/smile-io/purchase-point-product`)
      .auth(app.token, { type: 'bearer' })
      .send({
        point_product_id: '1',
        points_to_spend: '1',
        customer_id: '1',
        store_font_key: 'store_font_key',
      });
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Test error');
    expect(res.body.data).toStrictEqual(errorData);
  });
};
module.exports = purchasePointProduct;
