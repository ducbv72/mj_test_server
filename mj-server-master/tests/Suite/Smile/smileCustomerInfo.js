const {
  customerSmileIo,
  reward_programs: rewardPrograms,
  pointTransaction,
} = require('../../mocks/smile');
const smileCustomerInfo = (agent, app, fn) => {
  const {
    customerInfo,
    reward_programs,
    customerInfoError,
    pointsTransactions,
  } = fn;
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/smile-io/customers`);
    expect(res.status).toEqual(401);
  });
  it('Should return success response', async () => {
    customerInfo(200, customerSmileIo);
    reward_programs(200, rewardPrograms);
    pointsTransactions(200, pointTransaction);
    const res = await agent
      .get(`/api/v4/smile-io/customers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.msg).toBe('ok');
    expect(res.body.data).toBeObject();
  });

  it('Should return success response (case no reward program)', async () => {
    customerInfo(200, customerSmileIo);
    reward_programs(200, {});
    pointsTransactions(200, pointTransaction);
    const res = await agent
      .get(`/api/v4/smile-io/customers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.msg).toBe('ok');
    expect(res.body.data).toBeObject();
  });

  it('Should return response with no customer found', async () => {
    customerInfo(200, { customers: [] });
    const res = await agent
      .get(`/api/v4/smile-io/customers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.msg).toBe('User Not found');
    expect(res.body.data).toBe(null);
  });

  it('Should return error response (case catch error)', async () => {
    const error_message = 'Test Error';
    customerInfoError(error_message);
    const res = await agent
      .get(`/api/v4/smile-io/customers`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(500);
    expect(res.body.code).toBe(500);
    expect(res.body.message).toBe('Internal Error Server');
    expect(res.body.data).toBe(null);
  });
};

module.exports = smileCustomerInfo;
