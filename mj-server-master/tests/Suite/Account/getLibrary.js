const { user, userSetting } = require('../../mocks/vaultFeatures');
const { product } = require('../../mocks/E-Commerce');

const getLibrary = (agent, app, fn) => {
  const {
    spyCustomersFindOne,
    spyUserSettingsFindOne,
    spyUserSettingsUpdate,
    spyUserSettingsInsertOne,
    spyOrdersAggregate,
  } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/library/`);
    expect(res.status).toEqual(401);
  });

  it('It should return error with status 401', async () => {
    spyCustomersFindOne(null);
    const res = await agent
      .get(`/api/v4/library/`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(401);
    expect(res.body.message).toBe(
      'Unauthorised. Please refer to administrator.',
    );
    expect(res.body.data).toBe(null);
  });

  it('It should return library (update data in database)', async () => {
    spyCustomersFindOne(user);
    spyUserSettingsFindOne('1', userSetting);
    spyUserSettingsUpdate(userSetting);
    spyOrdersAggregate([product(1)]);
    const res = await agent
      .get(`/api/v4/library?sort=name&limit=10`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeObject();
  });

  it('It should return library (insert new setting to database)', async () => {
    spyCustomersFindOne(user);
    spyUserSettingsFindOne();
    spyUserSettingsInsertOne(userSetting);
    spyOrdersAggregate([product(1)]);
    const res = await agent
      .get(`/api/v4/library/`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeObject();
  });
};

module.exports = getLibrary;
