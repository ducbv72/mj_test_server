const { order } = require('../../mocks/E-Commerce').ordersConstant;

const orderById = (agent, app, fn) => {
  const { spyShopifyOrder, spyShopifyOrderError } = fn;
  const orderId = 1;
  //   token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/orders-by-id?order_id=${orderId}`);
    expect(res.status).toEqual(401);
  });

  it('It should return order', async () => {
    spyShopifyOrder(order);
    const res = await agent
      .get(`/api/v4/orders-by-id?order_id=${orderId}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeObject();
  });

  it('It should return order not found', async () => {
    const order_id = 'invalid_id';
    spyShopifyOrderError();
    const res = await agent
      .get(`/api/v4/orders-by-id?order_id=${order_id}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Not found');
    expect(res.body.data).toBeArray();
  });
};

module.exports = orderById;
