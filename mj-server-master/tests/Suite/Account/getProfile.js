const customerData = {
  customerId: '1',
  userName: 'Tester',
  userNameLower: null,
  email: 'tester@test.com',
  tags: 'tags, exclusive-mobile-nyansum',
};

const exclusiveData = [
  {
    customerTag: ['apple_login,ok'],
  },
  {
    customerTag: ['efgh,abcd,ok'],
  },
];

const getProfile = (agent, app, fn) => {
  const { spyAggregateFindCustomer, spyShopifyCustomer, spyExclusiveSaleFind } =
    fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/customer/profile`);
    expect(res.status).toEqual(401);
  });

  it('It should return success response (case exclusive is empty array)', async () => {
    spyAggregateFindCustomer(true);
    spyShopifyCustomer(customerData);
    spyExclusiveSaleFind([
      {
        customerTag: [],
      },
    ]);
    const res = await agent
      .get(`/api/v4/customer/profile`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.data).toBeObject();
    expect(Object.keys(res.body.data).length).toBeGreaterThan(0);
  });

  it('It should return success response (case exclusive is true array)', async () => {
    spyAggregateFindCustomer(true);
    spyShopifyCustomer(customerData);
    spyExclusiveSaleFind(exclusiveData);
    const res = await agent
      .get(`/api/v4/customer/profile`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.data).toBeObject();
    expect(Object.keys(res.body.data).length).toBeGreaterThan(0);
  });

  it("It should return success response (case exclusive is true array and customer's tag contains exclusive tags)", async () => {
    customerData.tags += ', ok';
    spyAggregateFindCustomer(true);
    spyShopifyCustomer(customerData);
    spyExclusiveSaleFind(exclusiveData);
    const res = await agent
      .get(`/api/v4/customer/profile`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.data).toBeObject();
    expect(Object.keys(res.body.data).length).toBeGreaterThan(0);
  });

  it('It should return catch error', async () => {
    spyAggregateFindCustomer(false);
    const res = await agent
      .get(`/api/v4/customer/profile`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.error.message).toBe('Test Catch Error');
  });
};

module.exports = getProfile;
