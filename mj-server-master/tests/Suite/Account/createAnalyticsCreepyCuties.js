const { user } = require('../../mocks/vaultFeatures');
const valueOfArrOld = [
  {
    productSku: 'SKU1',
    customerId: '1',
    actionType: 2,
    userId: '1',
    isPut: true,
  },
  {
    productSku: 'SKU2',
    customerId: '1',
    actionType: 2,
    userId: '1',
    isPut: true,
  },
  {
    productSku: 'SKU3',
    customerId: '1',
    actionType: 2,
    userId: '1',
    isPut: true,
  },
];

const analyticsCreepyCutiesData = {
  _id: '1',
  productSku: 'SKU',
  customerId: '1',
  actionType: 2,
  userId: '1',
  isPut: true,
  executeDate: '2021-12-02T07:16:07.574+0000',
};

const createAnalyticsCreepyCuties = (agent, app, fn) => {
  const {
    spyCustomersFindOne,
    spyAnalyticsCreepyCutiesInsertOne,
    spyAnalyticsCreepyCutiesFindOne,
    spyAnalyticsCreepyCutiesUpdate,
    spyAnalyticsCreepyCutiesAggregate,
  } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/analyticsCreepyCuties/insert`);
    expect(res.status).toEqual(401);
  });

  it('It should create new Analytics Creepy Cuties (Case actionType !== 2)', async () => {
    const actionType = 3;
    spyCustomersFindOne(user);
    spyAnalyticsCreepyCutiesInsertOne({ result: { n: 1 } });
    const res = await agent
      .post(`/api/v4/analyticsCreepyCuties/insert`)
      .auth(app.token, { type: 'bearer' })
      .send({ productSku: 'SKU', actionType });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Tutorial success');
    expect(res.body.data.actionType).toBe(actionType);
  });

  it('It should create new Analytics Creepy Cuties (Case actionType === 2 & productSkus)', async () => {
    const actionType = 2;
    spyCustomersFindOne(user);
    spyAnalyticsCreepyCutiesUpdate({ result: { n: 1 } });
    spyAnalyticsCreepyCutiesFindOne(analyticsCreepyCutiesData);
    spyAnalyticsCreepyCutiesAggregate(valueOfArrOld);
    spyAnalyticsCreepyCutiesUpdate({ result: { n: 1 } });
    spyAnalyticsCreepyCutiesInsertOne({ result: { n: 1 } });
    const res = await agent
      .post(`/api/v4/analyticsCreepyCuties/insert`)
      .auth(app.token, { type: 'bearer' })
      .send({ productSkus: ['SKU1', 'SKU2', 'SKU3'], actionType });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Put success');
    expect(res.body.data.actionType).toBe(actionType);
  });

  it('It should create new Analytics Creepy Cuties (Case actionType === 2 & productSkus is empty array)', async () => {
    const actionType = 2;
    spyCustomersFindOne(user);
    spyAnalyticsCreepyCutiesUpdate({ result: { n: 1 } });
    spyAnalyticsCreepyCutiesAggregate(valueOfArrOld);
    spyAnalyticsCreepyCutiesUpdate({ result: { n: 1 } });
    const res = await agent
      .post(`/api/v4/analyticsCreepyCuties/insert`)
      .auth(app.token, { type: 'bearer' })
      .send({ productSkus: [], actionType });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Put success');
    expect(res.body.data.actionType).toBe(actionType);
  });

  it('It should create new Analytics Creepy Cuties (Case actionType === 2 & productSkus & result.length > 0)', async () => {
    const actionType = 2;
    spyCustomersFindOne(user);
    spyAnalyticsCreepyCutiesUpdate({ result: { n: 1 } });
    spyAnalyticsCreepyCutiesAggregate(valueOfArrOld);
    spyAnalyticsCreepyCutiesFindOne(null);
    spyAnalyticsCreepyCutiesUpdate({ result: { n: 1 } });
    spyAnalyticsCreepyCutiesInsertOne({ result: { n: 1 } });
    const res = await agent
      .post(`/api/v4/analyticsCreepyCuties/insert`)
      .auth(app.token, { type: 'bearer' })
      .send({ productSkus: ['SKU1'], actionType });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Put success');
    expect(res.body.data.actionType).toBe(actionType);
  });

  it('It should return error cause insert false', async () => {
    const actionType = 3;
    spyCustomersFindOne(user);
    spyAnalyticsCreepyCutiesInsertOne({ result: { n: 0 } });
    const res = await agent
      .post(`/api/v4/analyticsCreepyCuties/insert`)
      .auth(app.token, { type: 'bearer' })
      .send({ productSku: 'SKU', actionType });
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Insert failed');
    expect(res.body.data).toBe(null);
  });

  it('It should return error (case user not found)', async () => {
    spyCustomersFindOne(null);
    const res = await agent
      .post(`/api/v4/analyticsCreepyCuties/insert`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(401);
    expect(res.body.message).toBe(
      'Unauthorised. Please refer to administrator.',
    );
    expect(res.body.data).toBe(null);
  });

  it('It should return error (case invalid parameters', async () => {
    const actionType = 2.4;
    spyCustomersFindOne(user);
    const res = await agent
      .post(`/api/v4/analyticsCreepyCuties/insert`)
      .auth(app.token, { type: 'bearer' })
      .send({ productSku: 'SKU', actionType });
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Invalid parameters');
    expect(res.body.data).toBe(null);
  });
};

module.exports = createAnalyticsCreepyCuties;
