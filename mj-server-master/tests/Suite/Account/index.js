/* eslint-disable no-undef */
const SaveUsername = require('./save-username');
const CheckUsername = require('./check-username');
const GetProfile = require('./getProfile');
const LoginAdmin = require('./login-admin');
const OrderByClient = require('./orderByClient');
const OrderById = require('./orderById');
const GetLibrary = require('./getLibrary');
const GetLibraryByUsername = require('./getLibraryByUsername');
const UpdateEmail = require('./update-email');
const CreateAnalyticsCreepyCuties = require('./createAnalyticsCreepyCuties');
const DeleteAnalyticsCreepyCuties = require('./deleteAnalyticsCreepyCuties');
const Shopify = require('shopify-api-node');
const common = require('../../../lib/common');

// const customerId = '1';
const userName = 'Tester';
// const userNameLower = 'tester';
const email = 'tester@test.com';

const UserInfo = (agent, app) => {
  /**
   *
   * @param {String} userNameLower username in lower
   * @param {String} customerId    this id of customer in token
   * @param {String} id            id wanna return
   * @param {String} username      username wanna return
   * @param {Boolean} isHaveCustomer return data have customer or not
   */
  const spyFindCustomer = (
    userNameLower,
    customerId,
    id,
    username,
    isHaveCustomer = false,
  ) => {
    jest.spyOn(app.db.customers, 'findOne').mockImplementation((arg) => {
      if (userNameLower || customerId) {
        let data;
        if (arg['$and'] && arg['$and'][0].userNameLower === userNameLower) {
          data = {
            customerId: id,
          };
        }
        if (arg['$and'] && arg['$and'][0].customerId === customerId) {
          data = isHaveCustomer
            ? null
            : {
                customerId,
                userName,
                userNameLower,
                email,
              };
        }
        if (arg.customerId === customerId) {
          data = {
            customerId,
            userName,
            userNameLower,
            email,
          };
        }
        return data;
      }

      if ((arg.userNameLower = userName.toLocaleLowerCase())) {
        return username;
      }

      return null;
    });
  };

  const spyShopifyGrapql = (data) => {
    jest
      .spyOn(Shopify.prototype, 'graphql')
      .mockImplementation(() => Promise.resolve(data));
  };

  const spyShopifyGrapqlError = () => {
    jest
      .spyOn(Shopify.prototype, 'graphql')
      .mockImplementationOnce(() =>
        Promise.reject(new Error('Test Catch Error')),
      );
  };

  const spyShopifyOrder = (data) => {
    jest
      .spyOn(Shopify.prototype.order, 'get')
      .mockImplementation((num) => Promise.resolve(data(num)));
  };

  const spyShopifyOrderError = () => {
    jest
      .spyOn(Shopify.prototype.order, 'get')
      .mockImplementation(() => Promise.reject(new Error('Test Catch Error')));
  };

  const spyCustomersFindOne = (data) => {
    jest
      .spyOn(app.db.customers, 'findOne')
      .mockImplementationOnce(() => Promise.resolve(data));
  };

  const spyUserSettingsFindOne = (id, data) => {
    jest.spyOn(app.db.userSettings, 'findOne').mockImplementationOnce((arg) => {
      if (arg.customerId === id) {
        return data;
      }
      return null;
    });
  };

  const spyUserSettingsUpdate = (data) => {
    jest.spyOn(app.db.userSettings, 'update').mockImplementationOnce(() => {
      return data;
    });
  };

  const spyUserSettingsInsertOne = (data) => {
    jest.spyOn(app.db.userSettings, 'insertOne').mockImplementationOnce(() => {
      return data;
    });
  };

  const spyOrdersAggregate = (data) => {
    jest.spyOn(app.db.orders, 'aggregate').mockImplementationOnce(() => {
      return {
        sort: () => ({
          toArray: () => data,
        }),
      };
    });
  };

  const spyCustomersUpdate = (data) => {
    jest.spyOn(app.db.customers, 'update').mockImplementation(() => data);
  };

  const spyOdersUpdate = (data) => {
    jest.spyOn(app.db.orders, 'update').mockImplementation(() => data);
  };

  const spyAnalyticsCreepyCutiesFindOne = (data) => {
    jest
      .spyOn(app.db.analyticsCreepyCuties, 'findOne')
      .mockImplementation(() => data);
  };

  const spyAnalyticsCreepyCutiesDeleteOne = (data) => {
    jest
      .spyOn(app.db.analyticsCreepyCuties, 'deleteOne')
      .mockImplementation(() => data);
  };

  const spyAnalyticsCreepyCutiesUpdate = (data) => {
    jest
      .spyOn(app.db.analyticsCreepyCuties, 'update')
      .mockImplementation(() => data);
  };

  const spyAnalyticsCreepyCutiesAggregate = (data) => {
    jest
      .spyOn(app.db.analyticsCreepyCuties, 'aggregate')
      .mockImplementation(() => ({ toArray: () => data }));
  };

  const spyAnalyticsCreepyCutiesInsertOne = (data) => {
    jest
      .spyOn(app.db.analyticsCreepyCuties, 'insertOne')
      .mockImplementation(() => data);
  };

  const spyExclusiveSaleFind = (data) => {
    jest.spyOn(app.db.exclusiveSale, 'find').mockImplementation(() => ({
      sort: () => ({
        toArray: () => data,
      }),
    }));
  };

  const spyFindCustomerCallback = (err, result) => {
    jest.spyOn(app.db.customers, 'findOne').mockImplementation((ar, cb) => {
      return cb(err, result);
    });
  };

  const spyCommon = (data) => {
    jest.spyOn(common, 'decryptDataFromClient').mockImplementation(() => data);
  };

  /**
   *
   * @param { Object } data result want to return
   */
  const spyShopifySearchCustomer = (data) => {
    jest
      .spyOn(Shopify.prototype.customer, 'search')
      .mockImplementation((arg) => {
        if (arg.id === '1') {
          return data;
        }
      });
  };

  const spyShopifyCustomer = (data) => {
    jest
      .spyOn(Shopify.prototype.customer, 'get')
      .mockImplementation(() => data);
  };

  const spyAggregateFindCustomer = (isPass) => {
    jest.spyOn(app.db.customers, 'aggregate').mockImplementation((arg) => {
      if (arg[0]['$match'].customerId === '1') {
        if (isPass) {
          return {
            toArray: () => [
              {
                _id: '1',
                following: [],
                followers: [],
                follow: [],
                userName,
              },
            ],
          };
        } else {
          return {
            toArray: () => Promise.reject(new Error('Test Catch Error')),
          };
        }
      }
      return {};
    });
  };

  /*********************************** **************************************/

  //Get order history for a specific customer from Shopify
  //GET /api/v4/orders-by-client
  describe('Get order history for a specific customer from Shopify', () => {
    OrderByClient(agent, app, {
      spyShopifyGrapql,
      spyShopifyOrder,
      spyShopifyGrapqlError,
    });
  });

  //Get details of a specific order from Shopify
  //GET /api/v4/orders-by-id
  describe('Get details of a specific order from Shopify', () => {
    OrderById(agent, app, { spyShopifyOrder, spyShopifyOrderError });
  });

  //Send login credentials for a specific user
  //POST /api/v4/login
  describe('Send login credentials for a specific user', () => {
    LoginAdmin(agent, app);
  });

  //Check to see if username exists
  //POST /api/v4/customer/check-username
  describe('Check to see if username exists', () => {
    CheckUsername(agent, { spyFindCustomerCallback, spyCommon });
  });

  //Save username
  //POST /api/v4/customer/save-username
  describe('Save username', () => {
    SaveUsername(agent, app, {
      spyFindCustomer,
      spyCustomersUpdate,
      spyOdersUpdate,
    });
  });

  //Update email from shopify
  //POST /api/v4/customer/update-email
  describe('Update email from shopify', () => {
    UpdateEmail(agent, app, { spyFindCustomer, spyShopifySearchCustomer });
  });

  //Get vault settings for the user
  //GET /api/v4/library/
  describe('Get vault settings for the user', () => {
    GetLibrary(agent, app, {
      spyCustomersFindOne,
      spyUserSettingsFindOne,
      spyUserSettingsUpdate,
      spyUserSettingsInsertOne,
      spyOrdersAggregate,
    });
  });

  //Get products to populate user's vault
  //GET /api/v4/feed/library/:username
  describe("Get products to populate user's vault", () => {
    GetLibraryByUsername(agent, app, {
      spyCustomersFindOne,
      spyUserSettingsFindOne,
      spyUserSettingsUpdate,
      spyUserSettingsInsertOne,
      spyOrdersAggregate,
    });
  });

  //Get customer's profile
  //GET /api/v4/customer/profile
  describe(`Get customer's profile`, () => {
    GetProfile(agent, app, {
      spyAggregateFindCustomer,
      spyShopifyCustomer,
      spyExclusiveSaleFind,
    });
  });

  //Create new analyticsCreepyCuties
  //POST /api/v4/analyticsCreepyCuties/insert
  describe(`Create new analyticsCreepyCuties`, () => {
    CreateAnalyticsCreepyCuties(agent, app, {
      spyCustomersFindOne,
      spyAnalyticsCreepyCutiesInsertOne,
      spyAnalyticsCreepyCutiesFindOne,
      spyAnalyticsCreepyCutiesUpdate,
      spyAnalyticsCreepyCutiesAggregate,
    });
  });

  //Delete an analyticsCreepyCuties
  //DELETE /api/v4/analyticsCreepyCuties/delete/:id
  describe(`Delete an analyticsCreepyCuties`, () => {
    DeleteAnalyticsCreepyCuties(agent, app, {
      spyAnalyticsCreepyCutiesFindOne,
      spyAnalyticsCreepyCutiesDeleteOne,
    });
  });
};
module.exports = UserInfo;
