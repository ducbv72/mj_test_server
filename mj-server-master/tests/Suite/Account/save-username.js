const saveUsername = (agent, app, fn) => {
  const { spyFindCustomer, spyCustomersUpdate, spyOdersUpdate } = fn;
  const userNameLower = 'tester';
  const user_name = 'Tester';
  const customerId = '1';
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.post(`/api/v4/customer/save-username`);
    expect(res.status).toEqual(401);
  });

  it('Should create user for email tester@test.com', async () => {
    spyFindCustomer(null, customerId, '1');
    spyCustomersUpdate({ data: 'success' });
    spyOdersUpdate({ data: 'success' });
    const res = await agent
      .post(`/api/v4/customer/save-username`)
      .auth(app.token, { type: 'bearer' })
      .send({ user_name: user_name });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('Update Success');
    expect(res.body.data.email).toBe('tester@test.com');
    expect(res.body.data.userName).toBe(user_name);
    expect(res.body.data.userNameLower).toBe(userNameLower);
  });

  it('Should return nothing updated', async () => {
    spyFindCustomer(userNameLower, null, '1');
    const res = await agent
      .post(`/api/v4/customer/save-username`)
      .auth(app.token, { type: 'bearer' })
      .send({ user_name: userNameLower });
    expect(res.body.message).toBe('Nothing updated');
    expect(res.body.code).toBe(200);
    expect(res.status).toBe(200);
  });

  it('Should return username exist', async () => {
    spyFindCustomer(userNameLower, null, '2');
    const res = await agent
      .post(`/api/v4/customer/save-username`)
      .auth(app.token, { type: 'bearer' })
      .send({ user_name: user_name, user_name_lower: userNameLower });
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Username exist');
  });

  it('Should return "Customer Not Found"', async () => {
    spyFindCustomer(null, customerId, '1', null, true);
    const res = await agent
      .post(`/api/v4/customer/save-username`)
      .auth(app.token, { type: 'bearer' })
      .send({ user_name: user_name, user_name_lower: userNameLower });
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Customer Not Found');
  });

  it('Should return invalid data', async () => {
    const res = await agent
      .post(`/api/v4/customer/save-username`)
      .auth(app.token, { type: 'bearer' });
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('invalid data');
    expect(res.body.data).toBe(null);
  });
};

module.exports = saveUsername;
