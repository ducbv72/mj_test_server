const { orders, order } = require('../../mocks/E-Commerce').ordersConstant;

const orderByClient = (agent, app, fn) => {
  const { spyShopifyOrder, spyShopifyGrapql, spyShopifyGrapqlError } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(`/api/v4/orders-by-client`);
    expect(res.status).toEqual(401);
  });

  it('Should return error with message: "Test Catch Error"', async () => {
    spyShopifyGrapqlError();
    const res = await agent
      .get(`/api/v4/orders-by-client?pageSize=`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.message).toBe('Test Catch Error');
  });

  it('Should return orders', async () => {
    spyShopifyGrapql(orders(0));
    spyShopifyOrder(order);
    const res = await agent
      .get(`/api/v4/orders-by-client?pageSize=11&nextCursor=`)
      .auth(app.token, { type: 'bearer' });
    expect(res.body.message).toBe('ok');
    expect(res.body.code).toBe(200);
    expect(res.body.orders).toBeArray();
    expect(res.body.orders.length).toBeLessThanOrEqual(10);
  });

  it('Should return next page', async () => {
    const limit = 10;
    const cursor = 'testCursor';
    spyShopifyGrapql(orders(limit, true));
    spyShopifyOrder(order);
    const res = await agent
      .get(`/api/v4/orders-by-client?pageSize=${limit}&nextCursor=${cursor}`)
      .auth(app.token, { type: 'bearer' });
    expect(res.body.message).toBe('ok');
    expect(res.body.code).toBe(200);
    expect(res.body.orders).toBeArray();
    expect(res.body.orders.length).toBeLessThanOrEqual(limit);
  });

  it('Should catch error', async () => {
    const res = await agent
      .get(`/api/v4/orders-by-client?pageSize=a`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(412);
    expect(res.body.message).toBe('Validation failed');
  });
};

module.exports = orderByClient;
