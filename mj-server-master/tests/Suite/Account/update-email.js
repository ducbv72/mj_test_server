const customerData = {
  customerId: '1',
  userName: 'Tester',
  userNameLower: null,
  email: 'tester@test.com',
};
const updateEmail = (agent, app, fn) => {
  const { spyFindCustomer, spyShopifySearchCustomer } = fn;
  test('should require authorization', async () => {
    const res = await agent.post(`/api/v4/customer/update-email`);
    expect(res.status).toEqual(401);
  });

  test('should return updated email', async () => {
    spyFindCustomer(null, '1');
    spyShopifySearchCustomer([customerData]);
    const res = await agent
      .post(`/api/v4/customer/update-email`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data).toBeArray();
  });

  test('should return error with message: "Cannot find this customer in shopify."', async () => {
    spyFindCustomer(null, '1');
    spyShopifySearchCustomer([]);
    const res = await agent
      .post(`/api/v4/customer/update-email`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toBe('Cannot find this customer in shopify.');
    expect(res.body.data).toBeArray();
  });

  test('should return error with message: "Have any error..."', async () => {
    spyFindCustomer(null, '1');
    spyShopifySearchCustomer(null);
    const res = await agent
      .post(`/api/v4/customer/update-email`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toBe(
      'Have any error when get customer info from shopify.',
    );
    expect(res.body.data).toBeArray();
  });

  test('should return error with message: "Cannot find this customer in database."', async () => {
    spyFindCustomer();
    // spyShopifySearchCustomer(null);
    const res = await agent
      .post(`/api/v4/customer/update-email`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toEqual(200);
    expect(res.body.code).toEqual(404);
    expect(res.body.message).toBe('Cannot find this customer in database.');
    expect(res.body.data).toBeArray();
  });
};

module.exports = updateEmail;
