const analyticsCreepyCutiesData = {
  _id: '1',
  productSku: 'SKU',
  customerId: '1',
  actionType: 2,
  userId: '1',
  isPut: true,
  executeDate: '2021-12-02T07:16:07.574+0000',
};

const deleteAnalyticsCreepyCuties = (agent, app, fn) => {
  const { spyAnalyticsCreepyCutiesFindOne, spyAnalyticsCreepyCutiesDeleteOne } =
    fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.delete(`/api/v4/analyticsCreepyCuties/delete/1`);
    expect(res.status).toEqual(401);
  });

  it('It should return successfully delete Creepy Cuties', async () => {
    spyAnalyticsCreepyCutiesFindOne(analyticsCreepyCutiesData);
    spyAnalyticsCreepyCutiesDeleteOne('Success');
    const res = await agent
      .delete(`/api/v4/analyticsCreepyCuties/delete/1`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('delete succesfully');
    expect(res.body.data).toBe(null);
  });

  it('It should return error cause analyticsCreepyCutiesData not found', async () => {
    spyAnalyticsCreepyCutiesFindOne(null);
    const res = await agent
      .delete(`/api/v4/analyticsCreepyCuties/delete/1`)
      .auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Analytics Creepy Cuties Not Found');
    expect(res.body.data).toBeArray();
  });
};

module.exports = deleteAnalyticsCreepyCuties;
