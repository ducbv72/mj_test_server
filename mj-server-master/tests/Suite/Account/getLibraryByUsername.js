const { user, userSetting } = require('../../mocks/vaultFeatures');
const { product } = require('../../mocks/E-Commerce');

const getLibrary = (agent, app, fn) => {
  const apiUrl = '/api/v4/feed/library/:username';
  apiUrl.replace(':username', 'tester');
  const {
    spyCustomersFindOne,
    spyUserSettingsFindOne,
    spyUserSettingsUpdate,
    spyUserSettingsInsertOne,
    spyOrdersAggregate,
  } = fn;
  // token not being sent - should respond with a 401
  it('It should require authorization', async () => {
    const res = await agent.get(apiUrl);
    expect(res.status).toEqual(401);
  });

  it('It should return error with status 401', async () => {
    spyCustomersFindOne(null);
    const res = await agent.get(apiUrl).auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(401);
    expect(res.body.message).toBe(
      'Unauthorised. Please refer to administrator.',
    );
    expect(res.body.data).toBe(null);
  });

  it('It should return library (update data in database)', async () => {
    spyCustomersFindOne(user);
    spyUserSettingsFindOne('1', userSetting);
    spyUserSettingsUpdate(userSetting);
    spyOrdersAggregate([product(1)]);
    const res = await agent.get(apiUrl).auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.data).toBeObject();
  });

  it('It should return library (insert new setting to database)', async () => {
    spyCustomersFindOne(user);
    spyUserSettingsFindOne();
    spyUserSettingsInsertOne(userSetting);
    spyOrdersAggregate([product(1)]);
    const res = await agent.get(apiUrl).auth(app.token, { type: 'bearer' });
    expect(res.status).toBe(200);
    expect(res.body.data).toBeObject();
  });
};

module.exports = getLibrary;
