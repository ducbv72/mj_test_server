const checkUsername = (agent, fn) => {
  const user_name = 'Tester';
  const user_name_encrypted = 'Tester';
  const iv = 'iv';
  const { spyFindCustomerCallback, spyCommon } = fn;

  it('Should return error cannot find this username (case send username)', async () => {
    const err = null;
    const result = { userName: user_name };
    spyFindCustomerCallback(err, result);
    const res = await agent
      .post('/api/v4/customer/check-username')
      .send({ user_name });
    expect(res.status).toBe(400);
    expect(res.body.message).toBe('error');
  });

  it('Should return success response', async () => {
    const err = null;
    const result = { userName: user_name_encrypted };
    spyCommon(user_name_encrypted);
    spyFindCustomerCallback(err, result);
    const res = await agent
      .post('/api/v4/customer/check-username')
      .send({ user_name_encrypted, iv });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data.exist).toBeTruthy();
  });

  it('Should return error cannot find this username', async () => {
    const err = { stack: 'Stack error' };
    spyCommon(user_name_encrypted);
    spyFindCustomerCallback(err, null);
    const res = await agent
      .post('/api/v4/customer/check-username')
      .send({ user_name_encrypted, iv });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('');
    expect(res.body.data.exist).toBeFalsy();
  });

  it('Should return response with exist fail', async () => {
    const err = null;
    const result = { userName: `${user_name_encrypted}A` };
    spyCommon(user_name_encrypted);
    spyFindCustomerCallback(err, result);
    const res = await agent
      .post('/api/v4/customer/check-username')
      .send({ user_name_encrypted, iv });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data.exist).toBeFalsy();
  });
};

module.exports = checkUsername;
