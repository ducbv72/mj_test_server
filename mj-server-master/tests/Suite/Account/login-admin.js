const bcrypt = require('bcryptjs');

const loginAdmin = (agent, app) => {
  const username = 'tester';
  const password = 'test';
  const data = {
    usersName: username,
    userPassword: password,
  };
  const spyBcrypt = (isEqual) => {
    jest.spyOn(bcrypt, 'compare').mockImplementation(() => isEqual);
  };

  const spyUsers = (data) => {
    jest.spyOn(app.db.users, 'findOne').mockImplementation((arg) => {
      if (arg.usersName === username) {
        return data;
      }
    });
  };

  it('Should login fail', async () => {
    const res = await agent.post('/api/v4/login');
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Not found');
  });

  it('Should login fail cause user not found', async () => {
    spyUsers(null);
    spyBcrypt(false);
    const res = await agent.post('/api/v4/login').send({ username, password });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe('Not found');
  });

  it('Should login cause password not exactly', async () => {
    spyUsers(data);
    spyBcrypt(false);
    const res = await agent.post('/api/v4/login').send({ username, password });
    expect(res.status).toBe(404);
    expect(res.body.code).toBe(404);
    expect(res.body.message).toBe(
      'Access denied. Check password and try again.',
    );
  });

  it('Should login to admin dashboard', async () => {
    spyUsers(data);
    spyBcrypt(true);
    const res = await agent.post('/api/v4/login').send({ username, password });
    expect(res.status).toBe(200);
    expect(res.body.code).toBe(200);
    expect(res.body.message).toBe('ok');
    expect(res.body.data.usersName).toEqual(username);
  });
};

module.exports = loginAdmin;
