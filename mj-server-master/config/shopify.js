const Shopify = require('shopify-api-node');

const multipassSecret = '8b983a0df8d3075a0ae34aa2afc21adc';
const MJStoreDomain = process.env.MJ_STORE_DOMAIN;
const MJApiKey = process.env.MJ_API_KEY;
const MJApiPass = process.env.MJ_API_PASSWORD;
const MJSharedSecretKey = process.env.MJ_SHARED_SECRET_KEY;
const MJStoreFrontAccessToken = process.env.MJ_STORE_FRONT_ACCESS_TOKEN;
const MJStoreFrontAdmin = process.env.MJ_STORE_FRONT_ADMIN;
const storeUrl = `https://mightyjaxx.myshopify.com/`;

const shopify = new Shopify({
  shopName: MJStoreDomain,
  apiKey: MJApiKey,
  password: MJApiPass,
});

module.exports = {
  shopify,
  multipassSecret,
  MJApiKey,
  MJStoreDomain,
  MJApiPass,
  MJSharedSecretKey,
  MJStoreFrontAccessToken,
  storeUrl,
  MJStoreFrontAdmin,
};
