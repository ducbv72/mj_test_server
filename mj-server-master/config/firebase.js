//init firebase
const adminFirebase = require('firebase-admin');
adminFirebase.initializeApp({
  credential: adminFirebase.credential.cert(
    './config/mighty-jaxx-firebase-adminsdk-1pj49-f59493a030.json',
  ),
  databaseURL: 'https://mighty-jaxx.firebaseio.com',
});

module.exports = {
  adminFirebase,
};
