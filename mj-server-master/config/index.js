module.exports = {
  jwtSecret: '3aEZW5BYSoFN4qehQzktKnjhicCJxyt21AqlTjxv',
  ServerPrivateKey: './key/encrypted-key/product_private.key.pem',
  ServerPublicKey: './key/encrypted-key/product_public_key.pub',
  MJ_CHANNEL: 'MJ',
  MJ_STAGING_CHANNEL: 'MJ-STAGING',

  winformToken:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
  apiToken:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1pZ2h0dHlqYXh4IiwiaWF0IjoxNTE2MjM5MDIyfQ.YUw2k590OjjpNrOBmoFIZmlQ-vc6soSXNfsKOiye3x0',
  giftCodeToken:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1pZ2h0dHlqYXh4IiwiaWF0IjoxNTE2MjM5MDIyfQ.YUw2k590OjjpNrOBmoFIZmlQ-vc6soSXNfsKOiye3x0',
  stockxToken:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1pZ2h0dHlqYXh4IiwiaWF0IjoxNTE2MjM5MDIyfQ.YUw2k590OjjpNrOBmoFIZmlQ-vc6soSXNfsKOiye3x0',
  campaignToken:
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImp0aSI6ImEzYTExODgyLWM0MDUtNDFmYy1iMmY5LTI0MzQxZmQ3ZmYwOSIsImlhdCI6MTYwMzQyNjQyMiwiZXhwIjoxNjAzNDMwMDIyfQ.pN-0OLyJk0r2VW6rt2l5KLamafxdAG4RrlfWtDbWPFA',
  librayToken:
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImp0aSI6ImEzYTExODgyLWM0MDUtNDFmYy1iMmY5LTI0MzQxZmQ3ZmYwOSIsImlhdCI6MTYwMzQyNjQyMiwiZXhwIjoxNjAzNDMwMDIyfQ.pN-0OLyJk0r2VW6rt2l5KLamafxdAG4RrlfWtDbWPFA',
  affiliateUrl:
    'https://stockx.sjv.io/c/2439294/530344/9060?u=https://stockx.com/',
  tradeGeckoToken: 'sX82qchpgQOHgqsdxCbXBP7HMUPAAA1yEN7SHM8KuJ4',

  KEY_ZIP_DOWNLOAD_URL: '{ZIP_FILE_DOWNLOAD_URL}',

  S3: {
    accessKeyId: 'AKIAXTWAYNRKLI4HCSWQ',
    secretAccessKey: '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',
    bucket: 'social-feed-media',
    socialFeedFolder: 'images',
    storeImageFolder: 'store',
    productImageFolder: 'products',
    profileImageFolder: 'profiles',
    pollsImageFolder: 'polls',
    quizsImageFolder: 'quizs',
    exclusiveImageFolder: 'exclusive',
  },

  S3UdidRequest: {
    region: 'ap-southeast-1',
    accessKeyId: 'AKIAXTWAYNRKLI4HCSWQ',
    secretAccessKey: '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',
    bucket: 'udid-request',
    folder: 'files',
  },

  cryptoHexKey:
    'd6e2c950ee64ec8abff46e8c7c210d9418485cfdd94bc4ca587d7b34fbce5a0a',
};
