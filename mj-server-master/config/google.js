const CLIENT_ID_IOS =
  '835476841347-ajqm15dfjsbooios341g0nimtngpknep.apps.googleusercontent.com';
const CLIENT_ID_ANDROID =
  '835476841347-06g1quvhd4acgripuu9s63a9os8300d7.apps.googleusercontent.com';
const CLIENT_ID_ANDROID_2 =
  '835476841347-hnm72592v0g80u7h9s3gkskkok92m873.apps.googleusercontent.com';
const CLIENT_ID_WEB =
  '835476841347-dsrnsu7fostsml4q5sodo8mv1h0kv1lc.apps.googleusercontent.com';

const audience = [
  CLIENT_ID_ANDROID,
  CLIENT_ID_ANDROID_2,
  CLIENT_ID_IOS,
  CLIENT_ID_WEB,
];

module.exports = { audience };
