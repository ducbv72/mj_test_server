const shopify_online_order = {};

const bonus_points_update = (
  customerId,
  reason,
  facing_note,
  account_id,
  bonus_points,
) => {
  return {
    points_transaction: {
      points_amount: null,
      points_change: bonus_points || 0,
      comment: facing_note || 'Manual adjustment',
      internal_note: reason || null,
      description: null,
      points_purchase_id: null,
      customer_id: customerId,
      account_id: account_id,
      is_cancelled: false,
      does_cancel: false,
      created_at: null,
      updated_at: null,
      activity_id: null,
    },
  };
};

module.exports = { shopify_online_order, bonus_points_update };
