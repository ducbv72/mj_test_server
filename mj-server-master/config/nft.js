const forrealAbi = require('./forrealAbi');
const tteAbi = require('./tteAbi');
const mjContractAbi = require('./mjContractAbi');
// Only whitelisted addresses can mint doge NFTs
const ONLY_WHITELISTED = true;

const PINATA = {
  KEY: '438920a1f6ada161fe01',
  SECRET: '56eb50668fde3301aa6ae7b20cba3f5ded857c0ac930080d9a3a0b08c426cf15',
};

const NODE_ENV = process.env.NODE_ENV;

const useTestnet = NODE_ENV === 'development' || NODE_ENV === 'test';

const FORREAL_CONTRACT_ADDRESS = useTestnet
  ? '0xb05cd792db77a7489ec308febf6e8ad1c788254d'
  : '0xDd347F8959012721890e22601Fc9929473924b4e';

const TTE_CONTRACT_ADDRESS = useTestnet
  ? '0x6d6201eCBd1C057dc7bd7B2f2fE8b24777758D53'
  : '0xDe5aCe64b5Df04193db5D5Fa0A64a9Dd900a6F7c';

const ALCHEMY_KEY_POLYGON = useTestnet
  ? 'https://polygon-mumbai.g.alchemy.com/v2/heDodAHl-mZlltbtBJ4R9-XvQBqRsDG4'
  : 'https://polygon-mainnet.g.alchemy.com/v2/W-ui9fYLoK9OBLZSTSD7xNN0PCD7ezTH';

const PRIVATE_KEY_POLYGON = useTestnet
  ? 'b9f84b8cd33e6c8b7e7022e7901cb48d136504d3c43bcc108dd1913a44818c68'
  : process.env.PRIVATE_KEY;

const PRIVATE_KEY_TTE = process.env.PRIVATE_KEY_TTE_WALLET;
const PUBLIC_KEY_TTE = process.env.PUBLIC_KEY_TTE_WALLET;

const baseUrl = 'ipfs://QmezGPkkuManNzSdsGF2aXWLKW1oFFTdxCr7D9HewW8emT';

const TTE_NFT_VARIANTS = {
  ['MET-22NTANNCMOG01']: {
    image: `${baseUrl}/Coconut.gif`,
    variantName: 'Coco-maew',
    nftType: 1,
  },
  ['MET-22NTANNMSROG02']: {
    image: `${baseUrl}/MangoStickyRice.gif`,
    variantName: 'Maew-go Sticky Rice',
    nftType: 2,
  },
  ['MET-22NTANNTYMOG03']: {
    image: `${baseUrl}/TomYum.gif`,
    variantName: 'Tom Yum Maew',
    nftType: 3,
  },
  ['MET-22NTANNFMOG04']: {
    image: `${baseUrl}/SetMeal.gif`,
    variantName: 'Family Maew',
    nftType: 4,
  },
};

module.exports = {
  TTE_NFT_VARIANTS,
  TTE_CONTRACT_ADDRESS,
  tteAbi,
  mjContractAbi,
  ONLY_WHITELISTED,
  forrealAbi,
  PINATA,
  FORREAL_CONTRACT_ADDRESS,
  ALCHEMY_KEY_POLYGON,
  PRIVATE_KEY_POLYGON,
  PRIVATE_KEY_TTE,
  PUBLIC_KEY_TTE,
};
