const {
  customerAccessTokenCreateWithMultipass,
  queryCustomer,
  customerUpdate,
  generateMultipassToken,
} = require('../services/shopify');

// Edit the following variables:
const NEW_TAGS = ['ENTER-TAG'];
const EMAILS = ['example@example.com'];

const getAccessTokenFromSocialProfile = async (email) => {
  const customerData = {
    email,
  };

  const multipassToken = generateMultipassToken(customerData);
  const storefrontRes = await customerAccessTokenCreateWithMultipass(
    multipassToken,
  );

  const accessToken =
    (storefrontRes &&
      storefrontRes.customerAccessToken &&
      storefrontRes.customerAccessToken.accessToken) ||
    '';

  if (!accessToken) {
    throw new Error('Invalid access token');
  }

  const fields = `{
      tags,
      id,
    }`;

  const { tags: existingTags, id: customerId } = await queryCustomer(
    accessToken,
    fields,
  );

  const tags = [...existingTags, ...NEW_TAGS];
  // Remove duplicate values
  const uniqTags = [...new Set(tags)];

  await customerUpdate({
    tags: uniqTags,
    id: customerId,
  });

  const result = { accessToken, customerData, tags: uniqTags, customerId };

  return result;
};

const main = async () => {
  console.log('Data length:', EMAILS.length);

  let current = 0;
  let done = 0;

  const promises = EMAILS.map(async (email) => {
    current += 1;

    setTimeout(async () => {
      await getAccessTokenFromSocialProfile(email);
      done += 1;
      console.log('Remaining:', EMAILS.length - done);
    }, current * 200);
  });

  await Promise.all(promises);
};

main();
