const ethers = require('ethers');
const crypto = require('crypto');
const saveToCSV = require('./saveToCSV');

const createWallet = () => {
  const id = crypto.randomBytes(32).toString('hex');
  const privateKey = '0x' + id;
  const wallet = new ethers.Wallet(privateKey);

  return {
    address: wallet.address,
    privateKey,
  };
};

const main = async (numOfAddreses) => {
  const result = Array.from(Array(numOfAddreses)).map(createWallet);
  const fileName = 'ListOfEthAddresses.csv';
  const header = [
    { id: 'address', title: 'Address' },
    { id: 'privateKey', title: 'Private Key' },
  ];
  saveToCSV(result, fileName, header);
};

const numOfAddresses = 40;

main(numOfAddresses);
