#!/bin/bash
  SSH_USER=ec2-user
  SSH_URL=testing.mightyjaxx.technology
  echo $1
  echo $2
  pwd
  chmod 400 key/ssh-key/testing_server_key.pem
  ssh -i key/ssh-key/testing_server_key.pem -t $SSH_USER@$SSH_URL "bash -lci 'bdeploy $1 $2'";