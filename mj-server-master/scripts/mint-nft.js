require('dotenv').config();
const { createAlchemyWeb3 } = require('@alch/alchemy-web3');

const { API_URL, PUBLIC_KEY, PRIVATE_KEY, NFT_CONTRACT_ADDRESS } = process.env;

const web3 = createAlchemyWeb3(API_URL);
const contract = require('../artifacts/contracts/mightyJaxxNFT.sol/mightyJaxxNFT.json');

const nftContract = new web3.eth.Contract(contract.abi, NFT_CONTRACT_ADDRESS);

const mintNFT = async (tokenURI) => {
  const nonce = await web3.eth.getTransactionCount(PUBLIC_KEY, 'latest'); // get latest nonce

  const tx = {
    from: PUBLIC_KEY,
    to: NFT_CONTRACT_ADDRESS,
    nonce: nonce,
    gas: 500000,
    maxPriorityFeePerGas: 1999999987,
    data: nftContract.methods.mintNFT(PUBLIC_KEY, tokenURI).encodeABI(),
  };

  const signPromise = web3.eth.accounts.signTransaction(tx, PRIVATE_KEY);
  signPromise
    .then((signedTx) => {
      web3.eth.sendSignedTransaction(signedTx.rawTransaction, (err, hash) => {
        if (!err) {
          console.log(
            'The hash of your transaction is: ',
            hash,
            "\nCheck Alchemy's Mempool to view the status of your transaction!",
          );
          return;
        }

        console.log(
          'Something went wrong when submitting your transaction:',
          err,
        );
      });
    })
    .catch((err) => {
      console.log('Promise failed:', err);
    });
};

mintNFT(
  'https://gateway.pinata.cloud/ipfs/QmYueiuRNmL4MiA2GwtVMm6ZagknXnSpQnB3z2gWbz36hP',
);
