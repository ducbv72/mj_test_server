#!/bin/bash
# ./deploy-next <branch>
# ie. ./deploy master
#set -x
echo Running deployment script...

case "$1" in
  "test1"):
    DIR=/var/www/html/test1.mightyjaxx.technology/
    export PENV=test1.mightyjaxx.technology
    BRANCH=$2
                ;;
  "test2"):
    DIR=/var/www/html/test2.mightyjaxx.technology/
    export PENV=test2.mightyjaxx.technology
    BRANCH=$2
        ;;
        "test3"):
    DIR=/var/www/html/test3.mightyjaxx.technology/
    export PENV=test3.mightyjaxx.technology
    BRANCH=$2
    ;;
        "test4"):
    DIR=/var/www/html/test4.mightyjaxx.technology/
    export PENV=test4.mightyjaxx.technology
    BRANCH=$2
    ;;
        "test5"):
    DIR=/var/www/html/test5.mightyjaxx.technology/
    export PENV=test5.mightyjaxx.technology
    BRANCH=$2
    ;;
        "test6"):
    DIR=/var/www/html/test6.mightyjaxx.technology/
    export PENV=test6.mightyjaxx.technology
    BRANCH=$2
    ;;
  "test7"):
    DIR=/var/www/html/test7.mightyjaxx.technology/
    export PENV=test7.mightyjaxx.technology
    BRANCH=$2
    ;;
  "test8"):
    DIR=/var/www/html/test8.mightyjaxx.technology/
    export PENV=test8.mightyjaxx.technology
    BRANCH=$2
    ;;
  "test9"):
    DIR=/var/www/html/test9.mightyjaxx.technology/
    export PENV=test9.mightyjaxx.technology
    BRANCH=$2
    ;;
esac


cd $DIR
git checkout -B temp-branch-name
git branch -D $BRANCH
git checkout -B $BRANCH
git branch -D temp-branch-name

git fetch origin
git reset --hard origin/$BRANCH

yarn

pm2 start mj_admin_dashboard.js --name $PENV
pm2 restart $PENV