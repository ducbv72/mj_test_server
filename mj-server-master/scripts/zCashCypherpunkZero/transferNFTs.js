require('dotenv').config();
const { isValidAddress } = require('ethereumjs-util');
const { createAlchemyWeb3 } = require('@alch/alchemy-web3');

const addressList = require('./addressList');
const saveToCSV = require('../saveToCSV');
const contractABI = require('./contractABI');

const startAtTokenId = 278; // Modify this value depending on the NFT Token ID that needs to be transferred
const { API_URL, PUBLIC_KEY, PRIVATE_KEY } = process.env;
const NFT_CONTRACT_ADDRESS = '0x3e86d6cf041b719c575f57050697c115f0a53758';
const web3 = createAlchemyWeb3(API_URL);

let initialNonce = null;

const transferNFT = async (toAddress, tokenId, index) => {
  const nftContract = new web3.eth.Contract(contractABI, NFT_CONTRACT_ADDRESS);

  if (!initialNonce) {
    initialNonce = await web3.eth.getTransactionCount(PUBLIC_KEY, 'latest'); // get latest nonce
  }

  const nonce = initialNonce + index;

  const tx = {
    from: PUBLIC_KEY,
    to: NFT_CONTRACT_ADDRESS,
    gas: 500000,
    nonce,
    maxPriorityFeePerGas: 2999999987,
    value: 0,
    data: nftContract.methods
      .transferFrom(PUBLIC_KEY, toAddress, tokenId)
      .encodeABI(),
  };

  const signedTx = await web3.eth.accounts.signTransaction(tx, PRIVATE_KEY);

  const hash = await sendSignedTransaction(signedTx);
  return hash;
};

const sendSignedTransaction = (signedTx) =>
  new Promise((resolve, reject) => {
    web3.eth.sendSignedTransaction(signedTx.rawTransaction, (err, hash) => {
      if (!err) {
        console.log(
          'The hash of your transaction is: ',
          hash,
          "\nCheck Alchemy's Mempool to view the status of your transaction!",
        );
        return resolve(hash);
      }

      console.log(
        'Something went wrong when submitting your transaction:',
        err,
      );
      return reject(err);
    });
  });

const main = async () => {
  if (!addressList || !addressList.length || !Array.isArray(addressList)) {
    console.log('Invalid addressList, must be an array');
    process.exit(1);
  }

  let hasInvalidAddresses = false;
  const invalidAddressList = [];

  addressList.forEach((address) => {
    if (!address || !isValidAddress(address)) {
      hasInvalidAddresses = true;
      invalidAddressList.push(address);
    }
  });

  if (hasInvalidAddresses) {
    console.log('Has invalid addresses =>', invalidAddressList);
    process.exit(1);
  }

  const result = [];

  try {
    for (let index = 0; index < addressList.length; index++) {
      try {
        const address = addressList[index];
        const tokenId = index + startAtTokenId;

        const transactionID = await transferNFT(address, tokenId, index);
        const time = new Date().toLocaleString();

        result.push({
          tokenId,
          time,
          transactionID,
          address,
        });
      } catch (error) {
        console.log('Error in for loop:', error);
      }
    }

    const sortedResult = result.sort(
      (a, b) => parseInt(a.tokenId) - parseInt(b.tokenId),
    );
    const fileName = 'Report.csv';
    const header = [
      { id: 'tokenId', title: 'NFT Token ID' },
      { id: 'time', title: 'Time of Transfer' },
      { id: 'address', title: 'Recipient ETH address' },
      { id: 'transactionID', title: 'Transaction ID' },
    ];

    saveToCSV(sortedResult, fileName, header);
  } catch (error) {
    console.log('Error:', error);
    process.exit(1);
  }
};

main();
