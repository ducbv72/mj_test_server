const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const saveToCSV = (data, fileName, header) => {
  const csvWriter = createCsvWriter({
    path: fileName,
    header,
  });

  csvWriter
    .writeRecords(data)
    .then(() => console.log('The CSV file was written successfully'));
};

module.exports = saveToCSV;
