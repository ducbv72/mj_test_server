const Validator = require('validatorjs');
const validator = (method, rules, customMessages, callback) => {
  const validation = new Validator(method, rules, customMessages);
  validation.passes(() => callback(null, true));
  validation.fails(() => callback(validation.errors, false));
};

module.exports = { validator };
