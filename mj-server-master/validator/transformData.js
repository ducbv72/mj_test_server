const transform = (method) => (req, res, next) => {
  const { limit, post_id, pageSize } = req[method];
  const list = Object.keys(req[method]);
  if (!list.length) {
    return next();
  }
  list[limit] ? (list[limit] = Number(list[limit])) : list[limit];
  list[post_id] ? (list[post_id] = Number(list[post_id])) : list[post_id];
  list[pageSize] ? (list[pageSize] = Number(list[pageSize])) : list[pageSize];
  return next();
};

module.exports = { transform };
