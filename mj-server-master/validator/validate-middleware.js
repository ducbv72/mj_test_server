const { validator } = require('./validateRule');

const checkValidData = (method) => (req, res, next) => {
  const validationRule = {
    limit: 'numeric',
    username: 'string',
    post_id: 'numeric',
    search: 'string',
    user: 'string',
    pageSize: 'numeric',
    nextCursor: 'string',
    like: 'boolean',
    // is_following: [{ in: ['true', 'false'] }],
  };
  if (Object.keys(req[method]).length === 0) {
    return next();
  }
  validator(req[method], validationRule, {}, (err, status) => {
    if (!status) {
      return res.status(412).send({
        success: false,
        message: 'Validation failed',
        data: err,
      });
    } else {
      return next();
    }
  });
};

module.exports = { checkValidData };
