const { validator } = require('./validateRule');

//Endpoint: /api/v4/smile-io/purchase-point-product/
const customValid = (rule) => (req, res, next) => {
  const validationRule = {
    purchasePointProduct: {
      point_product_id: 'required',
      customer_id: 'required',
    },
  };
  validator(req.body, validationRule[rule], {}, (err, status) => {
    if (!status) {
      return res.status(412).send({
        success: false,
        message: 'Validation failed',
        data: err,
      });
    } else {
      return next();
    }
  });
};

module.exports = { customValid };
