const { transform } = require('./transformData');
const { checkValidData } = require('./validate-middleware');
const { customValid } = require('./custom.vallidate');

module.exports = {
  transform,
  checkValidData,
  customValid,
};
