const getUserFragment = () => `
id
firstName
lastName
displayName
email,
phone
`;

exports.getUserFragment = getUserFragment;

exports.getCustomerInfoQuery = (customerAccessToken) => {
  return `
    query{
        customer(customerAccessToken: "${customerAccessToken}") {
            ${getUserFragment()}
          }                            
      }
    `;
};
