# Admin Dashboard

### Build Status

[![Run Tests and Deploy Production](https://github.com/Mighty-Jaxx-International-Pte-Ltd/mj-server/actions/workflows/run-tests-and-deploy-production.yml/badge.svg)](https://github.com/Mighty-Jaxx-International-Pte-Ltd/mj-server/actions/workflows/run-tests-and-deploy-production.yml)
[![Run Tests and Deploy Staging](https://github.com/Mighty-Jaxx-International-Pte-Ltd/mj-server/actions/workflows/run-tests-and-deploy-staging.yml/badge.svg?branch=develop)](https://github.com/Mighty-Jaxx-International-Pte-Ltd/mj-server/actions/workflows/run-tests-and-deploy-staging.yml)

### Linters

If you are using Visual Studio code, install the following extensions:

- Prettier
- ESLint

In your project root folder, create a new folder called `.vscode` and add the following code:

```
 {
 "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    "html",
    "typescript",
    "typescriptreact"
  ],
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  }
}
```

### Run test

Open termial in Visual Studio code:

- In MacOS or Linux:

```sh
yarn test: To run unit test
yarn watch_coverage: To open coverage file
```

- In Windows:

```sh
yarn test_win: To run unit test
yarn watch_coverage_win: To open coverage file
```

### Node App

#### Admin Server Access

1. Place `your-production-key.pem` ssh key in the .ssh folder of your computer. Run `chmod 700 ~/.ssh/your-production-key.pem` to set the correct file permissions. Bear in mind that for the staging server you will need another ssh key.
2. To access the production server, run `ssh -i ~/.ssh/your-production-key.pem ec2-user@13.213.64.243`. For staging: `ssh -i ~/.ssh/your-staging-key.pem ec2-user@13.250.113.250`.

#### .env Files

- Production: The `.env` file is located at `/var/www/html/mj-server/.env`. To open the file you need to access the production admin server ([Admin Server Access](#admin-server-access)) and run `vim /var/www/html/mj-server/.env`.
- Staging: The `.env` file is located at `mj-beta-test/mj-server/.env`. To open the file you need to access the staging admin server ([Admin Server Access](#admin-server-access)) and run `vim mj-beta-test/mj-server.env`.

### MongoDB

#### MongoDB Server Access

1. Place `demoapp.pem` ssh key in the .ssh folder of your computer. Run `chmod 700 ~/.ssh/demoapp.pem` to set the correct file permissions.
2. Go to the security group of the EC2 intance (currently available at https://ap-southeast-1.console.aws.amazon.com/ec2/v2/home?region=ap-southeast-1#SecurityGroup:securityGroupId=sg-06d098938238d79eb). Add an inboud rule to allow SSH access using your IP address.
3. Run `ssh -i ~/.ssh/demoapp.pem ec2-user@18.140.3.255`

#### MongoDB GUI

1. Download and launch Robo 3T
2. Click on create connection
3. Get the mongoDB SRV address from the desired environment (staging or production). You can find it in the `.env` file, which is located in the staging/production server (check the steps at [.env Files](#env-files) to read the file).
4. Paste the mongoDB SRV address in the text input next to "From SRV". Click on "From SRV".
5. Click on save and connect.
6. Open the desired database.

### Scripts

#### Tag shopify users

The purpose of the script is to tag customers on Shopify. After running the script, you can check a customer in the shopify admin dashboard to make sure the tagging is successful: https://mightyjaxx.myshopify.com/admin/customers

Instructions:

- Open `server/scripts/tagShopifyUsers.js`.
- Edit the values of `NEW_TAGS` and `EMAILS`.
- Run the script: `node server/scripts/tagShopifyUsers.js`

#### Deploy code from branch to test environment

- Currently, we have 9 testing domains: test1-test9.mightyjaxx.technology
- To deploy code please use step as below:

1. chmod +x scripts/deploy-test-environment.sh
2. run command:

```sh
yarn deploy-test [testing_domain_name] [branch_name]
```

test_name: test1 - test9

branch_name: name of branch that you want to deploy. eg: deploy

Login details for the testing admin dashboard:

- Email address: admin@mj.com
- Password: Lab@123456
- Token: 222222

Developer environment assignment:

- test1 - Nil
- test2 - Dovu
- test3 - Son
- test4 - Dzung
- test5 - Ngoc
- test6 - Tuan
- test7 - Nhan
- test8 - Huy
- test9 - Trieu

Directory location of test environments: `/var/www/html`. Deployment script: `/var/www/html/scripts/deploy.sh` (a backup of that script can be found in this repo at `scripts/deploy-test-environment-from-server.sh`).

### Stripe

#### Additional steps for webhook forwarding

Certain payment methods require a [webhook listener](https://stripe.com/docs/payments/payment-intents/verifying-status#webhooks) to notify you of changes in the status. When developing locally, you can use the [Stripe CLI](https://stripe.com/docs/stripe-cli) to forward webhook events to your local dev server.

- [Install the `stripe-cli`](https://stripe.com/docs/stripe-cli#install)
- Run `stripe listen --forward-to localhost:8080/api/v4/stripe/webhook`
- The CLI will print a webhook secret (such as, `whsec_***`) to the console. Set STRIPE_WEBHOOK_SECRET to this value in your `.env` file.
