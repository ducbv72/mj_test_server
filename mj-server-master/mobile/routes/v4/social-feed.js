const express = require('express');
const router = express.Router();
const { transform, checkValidData } = require('../../../validator');
const dayjs = require('dayjs');
const { shopify } = require('../../../config/shopify');

/**
 * @swagger
 * /api/v4/feed/blogs:
 *  get:
 *    security: []
 *    tags:
 *      - Social-feed
 *    description: Get list blogs in shopify
 *    parameters:
 *    - in: query
 *      name: limit
 *      description: Number of item per page.
 *      schema:
 *        type: integer
 *    - in: query
 *      name: since_id
 *      description: Id of last item in page.
 *      schema:
 *        type: string
 *    responses:
 *      '200':
 *        description: A successful response
 *      '500':
 *        description: Internal Error Server
 */

router.get(
  '/api/v4/feed/blogs',
  transform('query'),
  checkValidData('query'),
  async (req, res) => {
    const query = {
      limit: req.query.limit || 10,
      since_id: req.query.since_id,
      published_status: 'published',
    };

    try {
      const blogs = await shopify.blog.list();

      const blog_new = blogs.find((blog) => blog.handle === 'news');

      if (!blog_new) {
        return res
          .status(200)
          .json({ code: 200, message: 'Success', data: [] });
      }

      const news = await shopify.article.list(blog_new.id, query);

      return res
        .status(200)
        .json({ code: 200, message: 'Success', data: news });
    } catch (error) {
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

/**
 * @swagger
 * /api/v4/feed/artists:
 *  get:
 *    security: []
 *    tags:
 *      - Social-feed
 *    description: Get list blogs artist in shopify
 *    parameters:
 *    - in: query
 *      name: limit
 *      description: Number of item per page.
 *      schema:
 *        type: integer
 *    - in: query
 *      name: since_id
 *      description: Id of last item in page.
 *      schema:
 *        type: string
 *    responses:
 *      '200':
 *        description: A successful response
 *      '500':
 *        description: Internal Error Server
 */

router.get(
  '/api/v4/feed/artists',
  transform('query'),
  checkValidData('query'),
  async (req, res) => {
    const query = {
      limit: req.query.limit || 10,
      since_id: req.query.since_id,
    };

    try {
      const blogs = await shopify.blog.list();

      const blog_artist = blogs.find((blog) => blog.handle === 'artists');

      if (!blog_artist) {
        return res
          .status(200)
          .json({ code: 200, message: 'Success', data: [] });
      }

      const artists = await shopify.article.list(blog_artist.id, query);

      return res
        .status(200)
        .json({ code: 200, message: 'Success', data: artists });
    } catch (error) {
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

/**
 * @swagger
 * /api/v4/feed/videos:
 *  get:
 *    security: []
 *    tags:
 *      - Social-feed
 *    description: Get list videos
 *    parameters:
 *    - in: query
 *      name: type
 *      description: Type of video what you wanna search.
 *      schema:
 *        type: string
 *      example: 'unboxx'
 *      enum:
 *            - unboxx
 *            - feature
 *    responses:
 *      '200':
 *        description: A successful response
 */

router.get('/api/v4/feed/videos', async (req, res) => {
  const query = {
    type: req.query.type || 'unboxx',
  };

  const videos_feature = [
    {
      title: 'Cat Feline Whiskers Animal',
      created_at: dayjs().subtract(1, 'day').toDate(),
      url: 'https://player.vimeo.com/video/516532097?title=0&portrait=0&byline=0&autoplay=0&loop=0&transparent=1&controls=0',
    },
    {
      title: 'Bee Honey Insect Beehive Nature',
      created_at: dayjs().subtract(2, 'day').toDate(),
      url: 'https://player.vimeo.com/video/420985147?title=0&portrait=0&byline=0&autoplay=0&loop=0&transparent=1&controls=0',
    },
    {
      title: 'Traffic City Traffic Drone',
      created_at: dayjs().subtract(3, 'day').toDate(),
      url: 'https://player.vimeo.com/video/476396222?title=0&portrait=0&byline=0&autoplay=0&loop=0&transparent=1&controls=0',
    },
    {
      title: 'Airport People Crowd Busy',
      created_at: dayjs().subtract(4, 'day').toDate(),
      url: 'https://player.vimeo.com/video/411342239?title=0&portrait=0&byline=0&autoplay=0&loop=0&transparent=1&controls=0',
    },
    {
      title: 'Waterfall Water River Nature',
      created_at: dayjs().subtract(5, 'day').toDate(),
      url: 'https://player.vimeo.com/video/413229662?title=0&portrait=0&byline=0&autoplay=0&loop=0&transparent=1&controls=0',
    },
  ];

  const videos_unboxx = [
    {
      title: 'Cat Feline Whiskers Animal',
      created_at: dayjs().subtract(1, 'day').toDate(),
      url: 'https://player.vimeo.com/video/516532097?title=0&portrait=0&byline=0&autoplay=0&loop=0&transparent=1&controls=0',
    },
    {
      title: 'Bee Honey Insect Beehive Nature',
      created_at: dayjs().subtract(2, 'day').toDate(),
      url: 'https://player.vimeo.com/video/420985147?title=0&portrait=0&byline=0&autoplay=0&loop=0&transparent=1&controls=0',
    },
    {
      title: 'Waterfall Water River Nature',
      created_at: dayjs().subtract(5, 'day').toDate(),
      url: 'https://player.vimeo.com/video/413229662?title=0&portrait=0&byline=0&autoplay=0&loop=0&transparent=1&controls=0',
    },
  ];

  if (query.type === 'feature') {
    return res
      .status(200)
      .json({ code: 200, message: 'Success', data: videos_feature });
  }

  if (query.type === 'unboxx') {
    return res
      .status(200)
      .json({ code: 200, message: 'Success', data: videos_unboxx });
  }

  return res.status(200).json({ code: 200, message: 'Success', data: [] });
});

module.exports = router;
