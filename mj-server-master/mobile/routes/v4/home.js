const express = require('express');
const router = express.Router();
const Promise = require('promise');
const moment = require('moment-timezone');
const jwt = require('jsonwebtoken');
const { shopify } = require('../../../config/shopify');

/**
 * @swagger
 * /api/v4/home:
 *  get:
 *    security: []
 *    tags:
 *      - Mobile Home Screen
 *    description: Get data to populate the home screen on the mobile app
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/api/v4/home', async (req, res) => {
  // find feature
  const db = req.app.db;
  const [
    featureList,
    recommendList,
    lunchProductList,
    soldProductList,
    storeList,
  ] = await Promise.all([
    db.products
      .find(
        {
          productFeature: true,
          productTags: { $not: /.*exclusive-mobile-nyansum.*/i },
        },
        {
          projection: {
            productTags: 1,
            shopifyProductId: 1,
            productVariantSku: 1,
            productTitle: 1,
            productPrice: 1,
            productComment: 1,
            productNew: 1,
            productImageUrl: 1,
            productBackgroundImage: 1,
            productBackgroundImageS3: 1,
            productImageUrlS3: 1,
            productTitleOnApp: 1,
            productArtist: 1,
            productLaunchColor: 1,
            productLaunchDate: 1,
            productLaunchStatus: 1,
            productLaunchTime: 1,
            productLaunchVipStatus: 1,
            productSoldOutDate: 1,
            productSoldOutTime: 1,
            productSoldOutStatus: 1,
            CurrentDate: moment
              .tz('Asia/Singapore')
              .format('YYYY-MM-DD HH:mm:ss'),
          },
        },
      )
      .sort({ productOrder: 1, productAddedDate: -1 })
      .toArray(),

    db.products
      .find(
        {
          productRecommendation: true,
          productTags: { $not: /.*exclusive-mobile-nyansum.*/i },
        },
        {
          projection: {
            productTags: 1,
            shopifyProductId: 1,
            productVariantSku: 1,
            productTitle: 1,
            productPrice: 1,
            productComment: 1,
            productNew: 1,
            productImageUrl: 1,
            productBackgroundImage: 1,
            productBackgroundImageS3: 1,
            productImageUrlS3: 1,
            productTitleOnApp: 1,
            productArtist: 1,
            productLaunchColor: 1,
            productLaunchDate: 1,
            productLaunchStatus: 1,
            productLaunchTime: 1,
            productLaunchVipStatus: 1,
            productSoldOutDate: 1,
            productSoldOutTime: 1,
            productSoldOutStatus: 1,
            CurrentDate: moment
              .tz('Asia/Singapore')
              .format('YYYY-MM-DD HH:mm:ss'),
          },
        },
      )
      .sort({ productOrder: 1, productAddedDate: -1 })
      .toArray(),

    db.products
      .find(
        {
          productLaunchStatus: 'true',
          productTags: { $not: /.*exclusive-mobile-nyansum.*/i },
        },
        {
          projection: {
            productTags: 1,
            shopifyProductId: 1,
            productVariantSku: 1,
            productTitle: 1,
            productPrice: 1,
            productComment: 1,
            productNew: 1,
            productImageUrl: 1,
            productBackgroundImage: 1,
            productBackgroundImageS3: 1,
            productImageUrlS3: 1,
            productTitleOnApp: 1,
            productArtist: 1,
            productLaunchColor: 1,
            productLaunchDate: 1,
            productLaunchStatus: 1,
            productLaunchTime: 1,
            productLaunchVipStatus: 1,
            productSoldOutDate: 1,
            productSoldOutTime: 1,
            productSoldOutStatus: 1,
            CurrentDate: moment
              .tz('Asia/Singapore')
              .format('YYYY-MM-DD HH:mm:ss'),
          },
        },
      )
      .sort({ productOrder: 1, productAddedDate: -1 })
      .toArray(),

    db.products
      .find(
        {
          productSoldOutStatus: 'true',
          productTags: { $not: /.*exclusive-mobile-nyansum.*/i },
        },
        {
          projection: {
            productTags: 1,
            shopifyProductId: 1,
            productVariantSku: 1,
            productTitle: 1,
            productPrice: 1,
            productComment: 1,
            productNew: 1,
            productImageUrl: 1,
            productBackgroundImage: 1,
            productBackgroundImageS3: 1,
            productImageUrlS3: 1,
            productTitleOnApp: 1,
            productArtist: 1,
            productLaunchColor: 1,
            productLaunchDate: 1,
            productLaunchStatus: 1,
            productLaunchTime: 1,
            productLaunchVipStatus: 1,
            productSoldOutDate: 1,
            productSoldOutTime: 1,
            productSoldOutStatus: 1,
            CurrentDate: moment
              .tz('Asia/Singapore')
              .format('YYYY-MM-DD HH:mm:ss'),
          },
        },
      )
      .sort({ productOrder: 1, productAddedDate: -1 })
      .toArray(),

    db.stores.find({}).toArray(),
  ]);

  const homePopupList = [];

  featureList.forEach((item) => {
    item.currentDateUTC = moment
      .tz('Asia/Singapore')
      .format('YYYY-MM-DD HH:mm:ss');
    item.productBackgroundImage = item.productNew
      ? item.productBackgroundImage
      : null;
    item.productBackgroundImageS3 = item.productNew
      ? item.productBackgroundImageS3
      : null;
  });

  lunchProductList.forEach((item) => {
    item.currentDateUTC = moment
      .tz('Asia/Singapore')
      .format('YYYY-MM-DD HH:mm:ss');
  });

  soldProductList.forEach((item) => {
    item.currentDateUTC = moment
      .tz('Asia/Singapore')
      .format('YYYY-MM-DD HH:mm:ss');
  });

  recommendList.forEach((item) => {
    item.currentDateUTC = moment
      .tz('Asia/Singapore')
      .format('YYYY-MM-DD HH:mm:ss');
  });

  storeList.forEach((item) => {
    item.isHideStore = item.isHideStore || false;
  });

  const lastFeatureList = featureList.map((item, index) => {
    let result = item;
    const lunchProduct = lunchProductList.find(
      (product) => product.productVariantSku === item.productVariantSku,
    );

    if (lunchProduct) {
      result = {
        ...result,
        productLaunchColor: lunchProduct.productLaunchColor,
        productLaunchDate: lunchProduct.productLaunchDate,
        productLaunchStatus: lunchProduct.productLaunchStatus,
        productLaunchTime: lunchProduct.productLaunchTime,
        productLaunchVipStatus: lunchProduct.productLaunchVipStatus,
      };
    }

    const soldoutProduct = soldProductList.find(
      (product) => product.productVariantSku === item.productVariantSku,
    );

    if (soldoutProduct) {
      result = {
        ...result,
        productLaunchColor: soldoutProduct.productLaunchColor,
        productLaunchDate: soldoutProduct.productLaunchDate,
        productLaunchStatus: soldoutProduct.productLaunchStatus,
        productLaunchTime: soldoutProduct.productLaunchTime,
        productLaunchVipStatus: soldoutProduct.productLaunchVipStatus,
      };
    }
    return result;
  });

  const lastRecommendList = recommendList.map((item, index) => {
    let result = item;

    const lunchProduct = lunchProductList.find(
      (product) => product.productVariantSku === item.productVariantSku,
    );

    if (lunchProduct) {
      result = {
        ...result,
        productLaunchColor: lunchProduct.productLaunchColor,
        productLaunchDate: lunchProduct.productLaunchDate,
        productLaunchStatus: lunchProduct.productLaunchStatus,
        productLaunchTime: lunchProduct.productLaunchTime,
        productLaunchVipStatus: lunchProduct.productLaunchVipStatus,
      };
    }

    const soldoutProduct = soldProductList.find(
      (product) => product.productVariantSku === item.productVariantSku,
    );

    if (soldoutProduct) {
      result = {
        ...result,
        productLaunchColor: soldoutProduct.productLaunchColor,
        productLaunchDate: soldoutProduct.productLaunchDate,
        productLaunchStatus: soldoutProduct.productLaunchStatus,
        productLaunchTime: soldoutProduct.productLaunchTime,
        productLaunchVipStatus: soldoutProduct.productLaunchVipStatus,
      };
    }

    return result;
  });

  res.status(200).json({
    message: 'ok',
    data: {
      ProductFeature: lastFeatureList,
      ProductRecommend: lastRecommendList,
      StoreList: storeList,
      HomePopup: homePopupList,
      // LunchList: lunchProductList,
      // SoldOutList: soldProductList,
      // artists: artists,
      CurrentDate: moment.tz('Asia/Singapore').format('YYYY-MM-DD HH:mm:ss'),
      IsVip: false,
    },
  });
});

/**
 * @swagger
 * /api/v4/getAllSpecialData:
 *  get:
 *    security: []
 *    tags:
 *      - Mobile Home Screen
 *    description: Get all special data for the mobile app home screen - launch countdown and sold out timer
 *    responses:
 *      '200':
 *        description: A successful response
 */

router.get('/api/v4/getAllSpecialData', async (req, res, next) => {
  // find feature
  const db = req.app.db;
  const lstTagNotAlowSearch = [],
    customerTagArr = [];

  lstTagNotAlowSearch.push(new RegExp('exclusive-mobile-nyansum', 'i'));
  if (req.headers.authorization) {
    const userData = jwt.decode(
      req.headers.authorization.replace('Bearer ', ''),
    );
    if (userData) {
      const { tags } = await shopify.customer.get(Number(userData.customerId));
      for (const tag of tags.split(',')) {
        customerTagArr.push(tag.trim());
      }
    }
  }

  const productTags = await db.exclusiveSale
    .aggregate([
      {
        $group: {
          _id: { productTag: '$productTag' },
        },
      },
      {
        $project: { productTag: '$_id.productTag', _id: 0 },
      },
    ])
    .toArray();
  if (productTags.length > 0) {
    for (const product of productTags) {
      product.productTag.split(', ').forEach((e) => {
        if (!customerTagArr.includes(e)) {
          lstTagNotAlowSearch.push(new RegExp(product.productTag, 'i'));
        }
      });
    }
  }
  const query = {
    productLaunchStatus: 'true',
    productTags: { $nin: lstTagNotAlowSearch },
  };

  const [lunchProductList, soldProductList, artists, storeList] =
    await Promise.all([
      db.products
        .find(query, {
          projection: {
            productVariantSku: 1,
            productLaunchColor: 1,
            productLaunchDate: 1,
            productLaunchTime: 1,
            productLaunchStatus: 1,
            productLaunchVipStatus: 1,
            shopifyProductId: 1,
            CurrentDate: moment
              .tz('Asia/Singapore')
              .format('YYYY-MM-DD HH:mm:ss'),
          },
        })
        .sort({ productOrder: 1 })
        .toArray(),
      db.products
        .find(
          { productSoldOutStatus: 'true' },
          {
            projection: {
              productVariantSku: 1,
              productSoldOutDate: 1,
              productSoldOutTime: 1,
              productSoldOutStatus: 1,
              shopifyProductId: 1,
              CurrentDate: moment
                .tz('Asia/Singapore')
                .format('YYYY-MM-DD HH:mm:ss'),
            },
          },
        )
        .sort({ productOrder: 1 })
        .toArray(),
      db.products
        .find(
          {
            $or: [
              {
                $and: [
                  { productTitleOnApp: { $ne: '' } },
                  { productTitleOnApp: { $ne: null } },
                  { productTitleOnApp: { $exists: true } },
                ],
              },
              {
                $and: [
                  { productArtist: { $ne: '' } },
                  { productArtist: { $ne: null } },
                  { productArtist: { $exists: true } },
                ],
              },
            ],
          },
          {
            projection: {
              shopifyProductId: 1,
              productTitleOnApp: 1,
              productArtist: 1,
            },
          },
        )
        .sort({ productOrder: 1 })
        .toArray(),
      db.stores.find({}).toArray(),
    ]);

  lunchProductList.forEach((item) => {
    item.currentDateUTC = moment
      .tz('Asia/Singapore')
      .format('YYYY-MM-DD HH:mm:ss');
  });

  soldProductList.forEach((item) => {
    item.currentDateUTC = moment
      .tz('Asia/Singapore')
      .format('YYYY-MM-DD HH:mm:ss');
  });

  storeList.forEach((item) => {
    item.isHideStore = item.isHideStore || false;
  });

  res.status(200).json({
    message: 'ok',
    data: {
      LunchList: lunchProductList,
      SoldOutList: soldProductList,
      artists: artists,
      StoreList: storeList,
    },
  });
});

router.get('/api/v4/app-version', async (req, res) => {
  // find feature
  const db = req.app.db;
  try {
    const [appRelease] = await Promise.all([
      db.appReleased.find({}).sort({ dateRelease: -1 }).toArray(),
    ]);
    let currentReleaseVersion = null;
    if (appRelease.length > 0) {
      currentReleaseVersion = appRelease[0];
    }
    if (currentReleaseVersion) {
      let releaseDate = new moment(currentReleaseVersion.dateRelease);
      releaseDate = releaseDate.add(
        currentReleaseVersion.delayToRelease
          ? currentReleaseVersion.delayToRelease
          : 0,
        'hours',
      );
      if (moment.now().valueOf() > releaseDate.valueOf()) {
        res.status(200).json({
          message: 'ok',
          version: currentReleaseVersion.versionCode,
        });
      } else {
        if (appRelease.length > 1) {
          const olderReleaseVersion = appRelease[1];
          res.status(200).json({
            message: 'ok',
            version: olderReleaseVersion.versionCode,
          });
        } else {
          res.status(200).json({
            message: 'no released',
          });
        }
      }
    } else {
      res.status(200).json({
        message: 'no released',
      });
    }
  } catch (ex) {
    console.log(ex);
    res.status(500).json({
      message: 'no released',
    });
  }
});

module.exports = router;
