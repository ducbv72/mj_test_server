const express = require('express');
const router = express.Router();
const Promise = require('promise');
const passport = require('passport');
const { nyansumTag, productTransform } = require('../../../services/shopify');

/**
 * @swagger
 * /api/v4/exclusive-sale-tag:
 *  get:
 *    security: []
 *    tags:
 *      - Exclusive sale
 *    description: Get exclusive sale tag
 *    parameters:
 *    - in: query
 *      name: limit
 *      description: Number of item per page.
 *      schema:
 *        type: string
 *    - in: query
 *      name: page
 *      schema:
 *        type: string
 *    responses:
 *      '200':
 *        description: A successful response
 *      '500':
 *        description: Internal Server Error
 *      '401':
 *        description: Not a validated user.  Need to login first
 */
router.get(
  '/api/v4/exclusive-sale-tag',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;

      const exclusiveSaleList = await db.exclusiveSale
        .find()
        .sort({
          updatedAt: -1,
        })
        .toArray();

      res.status(200).json({ message: 'ok', data: exclusiveSaleList });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },
);

/**
 * @swagger
 * /api/v4/exclusive-sale-v2:
 *  get:
 *    security: []
 *    tags:
 *      - Exclusive sale
 *    description: Get exclusive items
 *    parameters:
 *    - in: query
 *      name: pageSize
 *      description: Number of item per page.
 *      schema:
 *        type: string
 *    - in: query
 *      name: nextCursor
 *      description: The id of last item in page view.
 *      schema:
 *        type: string
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Server Error
 */
router.get(
  '/api/v4/exclusive-sale-v2',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const product = await nyansumTag(req);
      const { products } = product;
      const listProductId = products.edges.map((e) => {
        const buff = Buffer.from(e.node.id.toString(), 'base64');
        const customerIdExtract = buff.toString('utf-8');
        const array = customerIdExtract.split('/');
        return array[4];
      });

      const result = await productTransform(product);

      const listProducts = await db.products
        .aggregate([
          {
            $match: { shopifyProductId: { $in: listProductId } },
          },
          {
            $group: {
              _id: '$shopifyProductId',
              productArtist: { $first: '$productArtist' },
            },
          },
        ])
        .toArray();
      if (!listProducts) {
        return res
          .status(400)
          .json({ message: 'Cannot find this product in database' });
      }
      result.result.forEach((v) => {
        const buff = Buffer.from(v.shopifyProductId, 'base64');
        const customerIdExtract = buff.toString('utf-8');
        const array = customerIdExtract.split('/');
        const shopifyProductId = array[4];
        const findProduct = listProducts.find(
          (e) => e._id === shopifyProductId.toString(),
        );
        if (findProduct) {
          v.artistName = findProduct.productArtist;
        }
      });

      return res.json({
        statusCode: 200,
        result: result['result'],
        pageInfo: result['pageInfo'],
      });
    } catch (error) {
      console.log('error: ', error);
      return res.json({
        statusCode: 500,
        message: 'Internal Error Server',
        error_message: error.message,
      });
    }
  },
);

module.exports = router;
