const express = require('express');
const router = express.Router();

const getTransactionFee = require('../../controllers/getTransactionFee');
const checkIfCustomerCanMint = require('../../controllers/nft/doge/checkIfCustomerCanMint');

// Stripe
const createPaymentIntent = require('../../controllers/stripe/createPaymentIntent');
const handleStripeWebhook = require('../../controllers/stripe/handleWebhook');

// NFT
/**
 * @swagger
 * /api/v4/transaction-fee:
 *  get:
 *    security: []
 *    tags:
 *      - NFT
 *    description: Get ethereum transaction fee
 *    parameters:
 *    - in: query
 *      name: address
 *      schema:
 *        type: string
 *      example: '0x3599689E6292b81B2d85451025146515070129Bb'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: invalid_parameters
 *      '500':
 *        description: Internal Server Error
 */

router.get('/api/v4/transaction-fee', getTransactionFee);
/**
 * @swagger
 * /api/v4/stripe/create-payment-intent:
 *  post:
 *    security: []
 *    tags:
 *      - NFT
 *    description: Get transaction fee
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *              currency:
 *                type: string
 *              request_three_d_secure:
 *                type: string
 *              payment_method_types:
 *                type: string
 *              customerAddress:
 *                type: string
 *            required:
 *             - email
 *             - currency
 *             - customerAddress
 *          example: {email: '', currency: '',request_three_d_secure: '',payment_method_types: '',customerAddress: '0x3599689E6292b81B2d85451025146515070129Bb',}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: invalid_parameters
 *      '500':
 *        description: Internal Server Error
 */

router.post('/api/v4/stripe/create-payment-intent', createPaymentIntent);

/**
 * @swagger
 * /api/v4/stripe/webhook:
 *  post:
 *    security: []
 *    tags:
 *      - NFT
 *    description: stripe webhook
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Webhook signature verification failed.
 *      '500':
 *        description: Internal Server Error
 */
router.post('/api/v4/stripe/webhook', handleStripeWebhook);

// Doge NFT

/**
 * @swagger
 * /api/v4/nft/doge/whitelisted:
 *  get:
 *    security: []
 *    tags:
 *      - NFT
 *    description: whitelisted doge nft
 *    parameters:
 *    - in: query
 *      name: customerAddress
 *      schema:
 *        type: string
 *      example: '0x3599689E6292b81B2d85451025146515070129Bb'
 *    - in: query
 *      name: productTitle
 *      schema:
 *        type: string
 *      example: ''
 *    - in: query
 *      name: nftSymbol
 *      schema:
 *        type: string
 *      example: ''
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid parameter
 *      '500':
 *        description: Internal Server Error
 */

router.get('/api/v4/nft/doge/whitelisted', checkIfCustomerCanMint);

module.exports = router;
