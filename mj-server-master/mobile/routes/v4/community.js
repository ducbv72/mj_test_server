const express = require('express');
const passport = require('passport');
const router = express.Router();
const common = require('../../../lib/common');
const Promise = require('promise');
const ObjectId = require('mongodb').ObjectId;

/**
 * @swagger
 * /api/v4/community-profile:
 *  get:
 *    security: []
 *    tags:
 *      - Community
 *    description: Get list of community-profile
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: error message
 *      '500':
 *        description: Internal Server Error
 */

//Community-profile
router.get('/api/v4/community-profile', async (req, res) => {
  try {
    const db = req.app.db;
    const profileType = req.app.profileType;
    const user = req.query.user;
    let getUserId;
    if (user) {
      const findUser = await db.customers.findOne({
        customerId: user.toString(),
      });
      getUserId = findUser._id;
    } else {
      getUserId = '';
    }
    const query = {
      isPublished: true,
    };

    if (profileType) {
      query.profileType = profileType;
    }

    let result = await db.profiles
      .aggregate([
        {
          $match: query,
        },
        {
          $lookup: {
            from: 'polls',
            let: { id: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$profileId'] }],
                  },
                  pollStartOn: { $lte: new Date() },
                  pollExpiresOn: { $gt: new Date() },
                  statusOfPoll: { $ne: 'Draft' },
                  isDraft: { $ne: true },
                },
              },
              {
                $project: {
                  profileName: 1,
                  question: 1,
                  options: 1,
                  statusBreakDown: 1,
                  pollStartOn: 1,
                  pollExpiresOn: 1,
                  profileId: 1,
                  statusOfPoll: 'Active',
                  bannerImage: 1,
                },
              },
            ],
            as: 'activePolls',
          },
        },
        {
          $lookup: {
            from: 'polls',
            let: { id: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$profileId'] }],
                  },
                  pollExpiresOn: { $lt: new Date() },
                  statusOfPoll: { $ne: 'Draft' },
                  isDraft: { $ne: true },
                },
              },
              {
                $project: {
                  profileName: 1,
                  question: 1,
                  options: 1,
                  statusBreakDown: 1,
                  pollStartOn: 1,
                  pollExpiresOn: 1,
                  profileId: 1,
                  statusOfPoll: 'Expired',
                  bannerImage: 1,
                },
              },
            ],
            as: 'expiredPolls',
          },
        },
        {
          $lookup: {
            from: 'polls',
            let: { id: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$profileId'] }],
                  },
                  pollStartOn: { $gte: new Date() },
                  statusOfPoll: { $ne: 'Draft' },
                  isDraft: { $ne: true },
                },
              },
              {
                $project: {
                  profileName: 1,
                  question: 1,
                  options: 1,
                  statusBreakDown: 1,
                  pollStartOn: 1,
                  pollExpiresOn: 1,
                  profileId: 1,
                  statusOfPoll: 'Incoming',
                  bannerImage: 1,
                },
              },
            ],
            as: 'incomingPolls',
          },
        },
        {
          $addFields: {
            polls: {
              $concatArrays: [
                '$activePolls',
                '$incomingPolls',
                '$expiredPolls',
              ],
            },
          },
        },
        { $unwind: { path: '$polls', preserveNullAndEmptyArrays: true } },
        {
          $lookup: {
            from: 'userPolls',
            let: { id: '$polls._id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$pollId'] }],
                  },
                  statusOfPoll: { $ne: 'Draft' },
                  isDraft: { $ne: true },
                  userId: { $eq: getUserId },
                },
              },
              {
                $project: {
                  pollId: 1,
                  userId: 1,
                  nameOfAnswer: 1,
                  createdAt: 1,
                },
              },
            ],
            as: 'userPolls',
          },
        },
        {
          $set: { 'polls.checkPoll': { $gt: [{ $size: '$userPolls' }, 0] } },
        },
        { $sort: { 'polls.pollStartOn': 1 } },
        {
          $group: {
            _id: '$_id',
            profileName: { $first: '$profileName' },
            profileType: { $first: '$profileType' },
            cardImage: { $first: '$cardImage' },
            logoImage: { $first: '$logoImage' },
            bannerImage: { $first: '$bannerImage' },
            createdAt: { $first: '$createdAt' },
            updatedAt: { $first: '$updatedAt' },
            canPublish: { $first: '$canPublish' },
            isPublished: { $first: '$isPublished' },
            order: { $first: '$order' },
            userPolls: { $first: '$userPolls' },
            polls: { $push: '$polls' },
          },
        },
        {
          $sort: { order: 1 },
        },
        {
          $project: {
            profileName: 1,
            profileType: 1,
            cardImage: 1,
            logoImage: 1,
            bannerImage: 1,
            createdAt: 1,
            updatedAt: 1,
            polls: 1,
          },
        },
      ])
      .toArray();
    const status = {
      Active: 1,
      Incoming: 2,
      Expired: 3,
    };

    result = result.map((e) => {
      if (e.polls.length) {
        e.polls = e.polls
          .sort(
            (a, b) =>
              new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
          )
          .sort((a, b) => status[a.statusOfPoll] - status[b.statusOfPoll]);
      }
      return e;
    });

    res.status(200).json({ message: 'ok', data: result });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

/**
 * @swagger
 * /api/v4/community-profile/{id}:
 *  get:
 *    security: []
 *    tags:
 *      - Community
 *    description: Get details of community-profile by id
 *    parameters:
 *    - in: path
 *      name: id
 *      required: true
 *      schema:
 *        type: string
 *      example: '6124dbb35b605822fd32fc06'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: error message
 *      '500':
 *        description: Internal Server Error
 */

router.get('/api/v4/community-profile/:id', async (req, res) => {
  try {
    const db = req.app.db;
    const user = req.query.user;
    let getUserId;
    if (user) {
      const findUser = await db.customers.findOne({
        customerId: user.toString(),
      });
      getUserId = findUser._id;
    } else {
      getUserId = '';
    }
    let result = await db.profiles
      .aggregate([
        {
          $match: {
            isPublished: true,
            _id: ObjectId(req.params.id),
          },
        },
        {
          $lookup: {
            from: 'polls',
            let: { id: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$profileId'] }],
                  },
                  pollStartOn: { $lte: new Date() },
                  pollExpiresOn: { $gt: new Date() },
                  statusOfPoll: { $ne: 'Draft' },
                  isDraft: { $ne: true },
                },
              },
              {
                $project: {
                  profileName: 1,
                  question: 1,
                  options: 1,
                  statusBreakDown: 1,
                  pollStartOn: 1,
                  pollExpiresOn: 1,
                  profileId: 1,
                  statusOfPoll: 'Active',
                  bannerImage: 1,
                },
              },
            ],
            as: 'activePolls',
          },
        },
        {
          $lookup: {
            from: 'polls',
            let: { id: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$profileId'] }],
                  },
                  pollExpiresOn: { $lt: new Date() },
                  statusOfPoll: { $ne: 'Draft' },
                  isDraft: { $ne: true },
                },
              },
              {
                $project: {
                  profileName: 1,
                  question: 1,
                  options: 1,
                  statusBreakDown: 1,
                  pollStartOn: 1,
                  pollExpiresOn: 1,
                  profileId: 1,
                  statusOfPoll: 'Expired',
                  bannerImage: 1,
                },
              },
            ],
            as: 'expiredPolls',
          },
        },
        {
          $lookup: {
            from: 'polls',
            let: { id: '$_id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$profileId'] }],
                  },
                  pollStartOn: { $gte: new Date() },
                  statusOfPoll: { $ne: 'Draft' },
                  isDraft: { $ne: true },
                },
              },
              {
                $project: {
                  profileName: 1,
                  question: 1,
                  options: 1,
                  statusBreakDown: 1,
                  pollStartOn: 1,
                  pollExpiresOn: 1,
                  profileId: 1,
                  statusOfPoll: 'Incoming',
                  bannerImage: 1,
                },
              },
            ],
            as: 'incomingPolls',
          },
        },
        {
          $addFields: {
            polls: {
              $concatArrays: [
                '$incomingPolls',
                '$expiredPolls',
                '$activePolls',
              ],
            },
          },
        },
        { $unwind: { path: '$polls', preserveNullAndEmptyArrays: true } },
        {
          $lookup: {
            from: 'userPolls',
            let: { id: '$polls._id' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [{ $eq: ['$$id', '$pollId'] }],
                  },
                  isDraft: { $ne: true },
                  userId: { $eq: getUserId },
                  isDraft: { $ne: true },
                },
              },
              {
                $project: {
                  pollId: 1,
                  userId: 1,
                  nameOfAnswer: 1,
                  createdAt: 1,
                },
              },
            ],
            as: 'userPolls',
          },
        },
        {
          $set: { 'polls.checkPoll': { $gt: [{ $size: '$userPolls' }, 0] } },
        },
        { $sort: { 'polls.pollStartOn': 1 } },
        {
          $group: {
            _id: '$_id',
            profileName: { $first: '$profileName' },
            profileType: { $first: '$profileType' },
            cardImage: { $first: '$cardImage' },
            logoImage: { $first: '$logoImage' },
            bannerImage: { $first: '$bannerImage' },
            createdAt: { $first: '$createdAt' },
            updatedAt: { $first: '$updatedAt' },
            canPublish: { $first: '$canPublish' },
            isPublished: { $first: '$isPublished' },
            order: { $first: '$order' },
            polls: { $push: '$polls' },
          },
        },
        {
          $project: {
            profileName: 1,
            profileType: 1,
            cardImage: 1,
            logoImage: 1,
            bannerImage: 1,
            createdAt: 1,
            updatedAt: 1,
            polls: 1,
          },
        },
      ])
      .toArray();

    const status = {
      Active: 1,
      Incoming: 2,
      Expired: 3,
    };

    result = result.map((e) => {
      if (e.polls.length) {
        e.polls = e.polls
          .sort(
            (a, b) =>
              new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
          )
          .sort((a, b) => status[a.statusOfPoll] - status[b.statusOfPoll]);
      }
      return e;
    });

    res.status(200).json({ message: 'ok', data: result });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

/**
 * @swagger
 * /api/v4/polls/{pollId}/:
 *  get:
 *    tags:
 *      - Community
 *    description: API get information vote for polls management
 *    parameters:
 *    - in: path
 *      name: pollId
 *      required: true
 *      schema:
 *        type: string
 *      example: '611a221e9f035922442df9f5'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '404':
 *        description: Poll not found
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

//Community-polls
//API get information vote for polls management
router.get(
  '/api/v4/polls/:pollId/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const user = req.user;
      const db = req.app.db;

      //Check user already exists
      const checkUser = await db.customers.findOne({
        _id: common.getId(user._id),
      });
      if (!checkUser) {
        return res
          .status(400)
          .json({ statusCode: 400, message: 'User not found' });
      }
      // check user already answered
      const [userPoll, [poll], userPollWithPollId] = await Promise.all([
        db.userPolls.findOne({
          $and: [
            { pollId: common.getId(req.params.pollId) },
            { userId: common.getId(user._id) },
          ],
        }),
        db.polls
          .aggregate([
            {
              $match: { _id: ObjectId(req.params.pollId) },
            },
            {
              $lookup: {
                from: 'profiles',
                localField: 'profileId',
                foreignField: '_id',
                as: 'profile',
              },
            },
          ])
          .toArray(),
        db.userPolls
          .find({ pollId: common.getId(req.params.pollId) })
          .toArray(),
      ]);

      if (!poll) {
        return res.status(404).json({ code: 404, message: 'Poll not found' });
      }

      poll.profileName = poll.profile[0].profileName;
      delete poll.profile;

      const countAllAnswerWithPollId = userPollWithPollId.length;
      const checkPollExpiresOn = new Date(poll.pollExpiresOn);
      const currentTime = new Date();
      if (
        poll.statusBreakDown == true ||
        checkPollExpiresOn.getTime() < currentTime.getTime()
      ) {
        //Get All vote by option
        const countOption1 = userPollWithPollId.filter(function (item) {
          return item.nameOfAnswer[0].key == 'option1';
        }).length;
        const countOption2 = userPollWithPollId.filter(function (item) {
          return item.nameOfAnswer[0].key == 'option2';
        }).length;
        const countOption3 = userPollWithPollId.filter(function (item) {
          return item.nameOfAnswer[0].key == 'option3';
        }).length;
        const countOption4 = userPollWithPollId.filter(function (item) {
          return item.nameOfAnswer[0].key == 'option4';
        }).length;
        let rateOption1;
        let rateOption2;
        let rateOption3;
        let rateOption4;
        if (countAllAnswerWithPollId === 0) {
          rateOption1 = 0;
          rateOption2 = 0;
          rateOption3 = 0;
          rateOption4 = 0;
        } else {
          rateOption1 = Math.round(
            (countOption1 / countAllAnswerWithPollId) * 100,
          );
          rateOption2 = Math.round(
            (countOption2 / countAllAnswerWithPollId) * 100,
          );
          rateOption3 = Math.round(
            (countOption3 / countAllAnswerWithPollId) * 100,
          );
          rateOption4 = Math.round(
            (countOption4 / countAllAnswerWithPollId) * 100,
          );
        }
        const totalRate = [rateOption1, rateOption2, rateOption3, rateOption4];
        let percentage = 100;
        const mapindex = totalRate.map((e, i) => {
          if (e !== 0) {
            return { e: e, i: i };
          } else {
            return { e: 0, i: i };
          }
        });
        const removeZeroPercent = mapindex.filter((e) => e.e !== 0);
        if (removeZeroPercent.length) {
          removeZeroPercent.forEach((e, i) => {
            if (i === removeZeroPercent.length - 1) {
              return (totalRate[e.i] = percentage);
            } else {
              return (percentage = percentage - e.e);
            }
            return e;
          });
        }
        poll.options[0].Rate = totalRate[0];
        poll.options[1].Rate = totalRate[1];
        poll.options[2].Rate = totalRate[2];
        poll.options[3].Rate = totalRate[3];
      }

      res.status(200).json({
        poll: poll,
        userPoll: userPoll,
        countAllAnswerWithPollId: countAllAnswerWithPollId,
      });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },
);

//API Crate UserPolls
/**
 * @swagger
 * /api/v4/userPoll/insert:
 *  post:
 *    tags:
 *      - Community
 *    description: API save user's choice in poll
 *    consumes:
 *    - application/json
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              pollId:
 *                type: string
 *              nameOfAnswer:
 *                type: string
 *          example: {pollId: '61a0541dda992d1cb56ce0f1', nameOfAnswer: 'option 1'}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Key or Name Is not valid/User already vote
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Internal Server Error
 */
// Post reports
router.post(
  '/api/v4/userPoll/insert',
  passport.authenticate('jwt', { session: false }),
  async (req, res, next) => {
    try {
      const db = req.app.db;
      const user = req.user;
      const { pollId, nameOfAnswer } = req.body;
      const key = nameOfAnswer[0].key;
      const name = nameOfAnswer[0].name;
      const userId = ObjectId(user._id);

      if (!key || !name) {
        return res
          .status(400)
          .json({ statusCode: 400, message: 'Key or Name Is not valid' });
      }
      //Check userPoll is already exists
      const [userPoll] = await Promise.all([
        db.userPolls.findOne({
          $and: [{ pollId: common.getId(pollId) }, { userId: userId }],
        }),
      ]);
      if (userPoll) {
        return res
          .status(400)
          .json({ code: 400, message: 'User already vote' });
      }
      //Check user already exists
      const checkUser = await db.customers.findOne({
        _id: common.getId(userId),
      });
      if (!checkUser) {
        return res
          .status(404)
          .json({ statusCode: 404, message: 'User not found' });
      }
      //Check poll already exists
      const checkPollId = await db.polls.findOne({
        $and: [
          { _id: common.getId(pollId) },
          { pollExpiresOn: { $gt: new Date() } },
        ],
      });
      if (!checkPollId) {
        return res
          .status(400)
          .json({ code: 400, error: { message: 'Poll Not found or Expired' } });
      }
      await Promise.all([
        db.userPolls.insert({
          pollId: ObjectId(pollId),
          userId: userId,
          nameOfAnswer: nameOfAnswer,
          createdAt: new Date(),
        }),
      ])
        .then(() => {
          return res.json({
            code: 200,
            data: 'Success!',
          });
        })
        .catch((err) => {
          return res.json({
            code: 400,
            message: err.message,
          });
        });
    } catch (error) {
      return res.json({ code: 500, error: { message: error.message } });
    }
  },
);

//Quiz
/**
 * @swagger
 * /api/v4/community-quiz/list-quizs:
 *  get:
 *    security: []
 *    tags:
 *      - Community
 *    description: API get list active quizs
 *    parameters:
 *    - in: query
 *      name: customerId
 *      schema:
 *        type: string
 *      example: '60cc162d5ec12147a8ea2903'
 *    - in: query
 *      name: profileId
 *      required: true
 *      description: This is community profile what contains this quiz
 *      schema:
 *        type: string
 *      example: '6124dbb35b605822fd32fc06'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Server Error
 */
router.get('/api/v4/community-quiz/list-quizs', async (req, res) => {
  try {
    const db = req.app.db;
    const customerId = req.query.customerId;
    const profileId = req.query.profileId;
    const findOrders = customerId
      ? await db.orders
          .find({ orderSmartContract: { $exists: true }, customerId })
          .toArray()
      : null;
    let findUserAnswer;
    if (customerId) {
      const getUser = await db.customers.findOne({ customerId });
      findUserAnswer = await db.quizQuestions
        .find({
          userId: getUser._id,
        })
        .toArray();
    }
    const query = [
      {
        $match: { statusOfQuiz: { $ne: 'Draft' }, isDraft: false },
      },
      {
        $lookup: {
          from: 'profiles',
          localField: 'profileId',
          foreignField: '_id',
          as: 'profile',
        },
      },
      {
        $sort: { quizStartOn: 1 },
      },
    ];
    if (profileId) {
      query[0].$match.profileId = ObjectId(profileId);
    }
    let result = await db.quizs.aggregate(query).toArray();
    result.forEach(async (e, i) => {
      if (!e.profile.length) {
        delete result[i];
        await db.quizs.updateOne(
          { _id: e._id },
          { $set: { statusOfQuiz: 'Draft' } },
          {},
        );
        return;
      }
      e.profileName = e.profile[0].profileName;
      delete e.profile;
      let checkAvaiableSKUs = false;
      let checkUserAnswer = false;
      if (findOrders) {
        //Check tags
        if (!e.availableSkus || (e.availableSkus && !e.availableSkus.length)) {
          checkAvaiableSKUs = true;
        }
        if (e.availableSkus) {
          const quizAvaiableTags = e.availableSkus;
          const B2BSkus = findOrders.map((order) => order.sku);
          B2BSkus.forEach((sku) => {
            const findTags = quizAvaiableTags.find((i) => i === sku);
            if (findTags) {
              checkAvaiableSKUs = true;
              return;
            }
            return;
          });
        }
      }

      if (findUserAnswer) {
        if (
          findUserAnswer.filter((a) => a.quizId.toString() === e._id.toString())
            .length
        ) {
          checkUserAnswer = true;
        }
      }

      const start = new Date(e.quizStartOn).getTime();
      const expired = new Date(e.quizExpiresOn).getTime();
      const current = new Date().getTime();

      if (expired < current && !e.isDraft) {
        e.statusOfQuiz = 'Expired';
      }

      if (expired < current && e.isDraft && e.canPublish) {
        e.statusOfQuiz = 'Unpublished';
      }

      if (!e.isDraft && start < current && expired > current) {
        e.statusOfQuiz = 'Active';
      }

      if (!e.isDraft && start > current) {
        e.statusOfQuiz = 'Incoming';
      }

      if (e.statusOfQuiz === 'Expired') {
        checkAvaiableSKUs = true;
      }

      e.checkUserAnswer = checkUserAnswer;
      e.checkAvaiableSKUs = checkAvaiableSKUs;

      return e;
    });
    result = result.filter((e) => e.statusOfQuiz !== 'Unpublished');
    const activeQuiz = result
      .filter((e) => e.statusOfQuiz === 'Active')
      .sort(
        (a, b) =>
          new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
      )
      .sort((a, b) => parseInt(a.checkUserAnswer - b.checkUserAnswer));
    const incomingQuiz = result
      .filter((e) => e.statusOfQuiz === 'Incoming')
      .sort(
        (a, b) =>
          new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
      );
    const expiredQuiz = result
      .filter((e) => e.statusOfQuiz === 'Expired')
      .sort(
        (a, b) =>
          new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime(),
      );
    result = activeQuiz.concat(incomingQuiz, expiredQuiz);
    res.status(200).json({ message: 'ok', result });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error.message });
  }
});

/**
 * @swagger
 * /api/v4/community-quiz/retrieve-quiz/:quizId:
 *  get:
 *    security: []
 *    tags:
 *      - Community
 *    description: API retrieve a quiz
 *    parameters:
 *    - in: path
 *      name: quizId
 *      schema:
 *        type: string
 *      example: '619b47ba7621d355dc36b916'
 *    - in: query
 *      name: customerId
 *      schema:
 *        type: string
 *      example: '60cc162d5ec12147a8ea2903'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Sorry, this quiz is currently unavaiable
 *      '500':
 *        description: Internal Server Error
 */
router.get('/api/v4/community-quiz/retrieve-quiz/:quizId', async (req, res) => {
  try {
    const db = req.app.db;
    const id = req.params.quizId;
    let checkAvaiableSKUs = false;
    const customerId = req.query.customerId;

    const [findOrders, quiz] = await Promise.all([
      customerId
        ? db.orders
            .find({ orderSmartContract: { $exists: true }, customerId })
            .toArray()
        : null,
      db.quizs
        .aggregate([
          {
            $match: {
              _id: ObjectId(id),
              statusOfQuiz: { $ne: 'Draft' },
              isDraft: false,
            },
          },
          {
            $lookup: {
              from: 'profiles',
              localField: 'profileId',
              foreignField: '_id',
              as: 'profile',
            },
          },
        ])
        .toArray(),
    ]);
    const result = quiz[0];
    if (!result || !result.profile.length) {
      await db.quizs.updateOne(
        { _id: ObjectId(id) },
        { $set: { statusOfQuiz: 'Draft' } },
        {},
      );
      return res.status(400).json({
        code: 400,
        message: 'Sorry, this quiz is currently unavailable',
      });
    }
    result.profileName = result.profile[0].profileName;
    delete result.profile;
    if (findOrders) {
      //Check tags
      if (result.availableSkus) {
        const quizAvaiableTags = result.availableSkus;
        const B2BSkus = findOrders.map((e) => e.sku);
        B2BSkus.forEach((e) => {
          const findTags = quizAvaiableTags.find((i) => i === e);
          if (findTags) {
            checkAvaiableSKUs = true;
            return;
          }
          return;
        });
      }

      if (
        !result.availableSkus ||
        (result.availableSkus && !result.availableSkus.length)
      ) {
        checkAvaiableSKUs = true;
      }
    }

    if (!result || result.isDraft) {
      return res.status(400).json({
        message: 'Sorry, this quiz is currently unavailable',
      });
    }

    const start = new Date(result.quizStartOn).getTime();
    const expired = new Date(result.quizExpiresOn).getTime();
    const current = new Date().getTime();

    if (expired < current && !result.isDraft) {
      result.statusOfQuiz = 'Expired';
    }

    if (!result.isDraft && start < current && expired > current) {
      result.statusOfQuiz = 'Active';
    }

    if (!result.isDraft && start > current) {
      result.statusOfQuiz = 'Incoming';
    }

    if (result.statusOfQuiz === 'Expired') {
      checkAvaiableSKUs = true;
    }

    if (!checkAvaiableSKUs) {
      return res
        .status(200)
        .json({ message: "Your's products are not on the approved list" });
    }

    res.status(200).json({ message: 'ok', result });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error.message });
  }
});

/**
 * @swagger
 * /api/v4/community-quiz/retrieve-answer:
 *  get:
 *    tags:
 *      - Community
 *    description: API retrieve an answer
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: User not answer this quiz
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Server Error
 */
router.get(
  '/api/v4/community-quiz/retrieve-answer',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const user = req.user;
      const getUserAnswer = await db.quizQuestions.findOne({
        userId: ObjectId(user._id),
      });

      if (!getUserAnswer) {
        return res.status(400).json({ message: 'User not answer this quiz' });
      }

      res.status(200).json({ message: 'ok', result: getUserAnswer });
    } catch (error) {
      return res.status(500).json({
        message: 'Internal Server Error',
        error_message: error.message,
      });
    }
  },
);

/**
 * @swagger
 * /api/v4/community-quiz/answers:
 *  post:
 *    tags:
 *      - Community
 *    description: Create new answer
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              quizId:
 *                type: string
 *              quizAnswers:
 *                type: array
 *          example: {quizId: '614d4ed7bcfcac4ac01484f7', quizAnswers: [{_id: '614d4ed7bcfcac4ac01484f5', questionNumber: 1, nameOfAnswer: "option1"}, {_id: '614d4ed7bcfcac4ac01484f6', questionNumber: 2, nameOfAnswer: "option4"}]}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: You've already completed the quiz!/Need fill all message, please try again!/Update failed, please try again!
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Server Error
 */
router.post(
  '/api/v4/community-quiz/answers',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const user = req.user;
      const userId = ObjectId(req.user._id);
      let checkAvaiableSKUs = false;
      const { quizId, quizAnswers } = req.body;

      const [findOrder, findQuiz, findQuizQuestions] = await Promise.all([
        db.orders.find(user.customerId).toArray(),
        db.quizs.findOne({ _id: ObjectId(quizId) }),
        db.quizQuestions.findOne({
          quizId: ObjectId(quizId),
          userId: ObjectId(userId),
        }),
      ]);
      if (!findQuiz.availableSkus) {
        checkAvaiableSKUs = true;
      }
      if (findQuiz.availableSkus) {
        const quizAvaiableTags = findQuiz.availableSkus;
        const B2BSkus = findOrder.map((e) => e.sku);
        B2BSkus.forEach((e) => {
          const findTags = quizAvaiableTags.find((i) => i === e);
          if (findTags) {
            checkAvaiableSKUs = true;
            return;
          }
          return;
        });
      }

      if (!checkAvaiableSKUs) {
        return res
          .status(200)
          .json({ message: "Your's products are not on the approved list" });
      }

      if (findQuizQuestions) {
        return res.status(400).json({
          message: "You've already completed the quiz!",
        });
      }

      if (
        !findQuiz ||
        findQuiz.questions.length !== quizAnswers.length ||
        findQuiz.isDraft ||
        new Date(findQuiz.quizExpiresOn).getTime() < new Date().getTime()
      ) {
        return res.status(400).json({
          message: 'Sorry, this quiz is currently unavailable',
        });
      }

      const checkEmptyField = quizAnswers.find((e) => e.nameOfAnswer === '');
      if (checkEmptyField) {
        return res
          .status(400)
          .json({ message: 'Need fill all message, please try again!' });
      }

      //create answer
      quizAnswers.forEach(async (e) => {
        const newAnswer = {
          quizId: ObjectId(quizId),
          questionId: ObjectId(e._id),
          userId,
          keyOfAnswer: e.nameOfAnswer,
          createdAt: new Date(),
        };
        const saveAnswer = await db.quizAnswers.insertOne(newAnswer);
      });

      const doc = {
        quizId: ObjectId(quizId),
        userId,
        quizAnswers,
        createdAt: new Date(),
      };

      const result = await db.quizQuestions.insertOne(doc);
      if (!result.result.n) {
        return res
          .status(400)
          .json({ message: 'Update failed, please try again!' });
      }
      res.status(200).json({ message: 'ok', result: result.ops[0] });
    } catch (error) {
      console.log(error);
      res.status(500).json({
        message: 'Internal Server Error',
        error_message: error.message,
      });
    }
  },
);

/**
 * @swagger
 * /api/v4/community-quiz/quiz-report/:quizId:
 *  get:
 *    tags:
 *      - Community
 *    description: API get report all answer
 *    parameters:
 *    - in: path
 *      name: quizId
 *      schema:
 *        type: string
 *      example: '619b47ba7621d355dc36b916'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Sorry, this quiz is currently unavaiable
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Server Error
 */
router.get(
  '/api/v4/community-quiz/quiz-report/:quizId',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const user = req.user;
      const quizId = req.params.quizId;
      const getQuiz = await db.quizs.findOne({ _id: ObjectId(quizId) });
      const listQuestionId = getQuiz.questions.map((e) => ObjectId(e._id));
      const getQuestions = await db.quizAnswers
        .find({
          questionId: { $in: listQuestionId },
        })
        .toArray();

      const result = [];

      listQuestionId.forEach((e) => {
        const getQuestion = getQuestions.filter(
          (j) => j.questionId.toString() === e.toString(),
        );
        const total = getQuestion.length;
        const option1 = getQuestion.filter(
          (e) => e.keyOfAnswer === 'option1',
        ).length;
        const option2 = getQuestion.filter(
          (e) => e.keyOfAnswer === 'option2',
        ).length;
        const option3 = getQuestion.filter(
          (e) => e.keyOfAnswer === 'option3',
        ).length;
        const option4 = getQuestion.filter(
          (e) => e.keyOfAnswer === 'option4',
        ).length;
        const rate1 = Math.round((option1 / total) * 100);
        const rate2 = Math.round((option2 / total) * 100);
        const rate3 = Math.round((option3 / total) * 100);
        const rate4 = Math.round((option4 / total) * 100);

        let totalRate = [rate1, rate2, rate3, rate4];
        let percentage = 100;
        const mapindex = totalRate.map((e, i) => {
          if (e !== 0) {
            return { e: e, i: i };
          } else {
            return { e: 0, i: i };
          }
        });
        const removeZeroPercent = mapindex.filter((e) => e.e !== 0);
        if (removeZeroPercent.length) {
          removeZeroPercent.forEach((e, i) => {
            if (i === removeZeroPercent.length - 1) {
              return (totalRate[e.i] = percentage);
            } else {
              return (percentage = percentage - e.e);
            }
          });
        }

        totalRate = totalRate.map((e) => {
          if (!e) {
            return (e = 0);
          }
          return e;
        });

        const questionInfo = getQuiz.questions.find(
          (i) => i._id.toString() === e.toString(),
        );
        const userAnswer = getQuestion.find(
          (e) => e.userId.toString() === user._id.toString(),
        );

        return result.push({
          _id: e.questionId,
          question: questionInfo.question,
          options: [
            {
              key: questionInfo.options[0].key,
              name: questionInfo.options[0].name,
              is_correct: questionInfo.options[0].is_correct,
              rate: totalRate[0],
              customerAnswer: userAnswer
                ? questionInfo.options[0].key === userAnswer.keyOfAnswer
                : false,
            },
            {
              key: questionInfo.options[1].key,
              name: questionInfo.options[1].name,
              is_correct: questionInfo.options[1].is_correct,
              rate: totalRate[1],
              customerAnswer: userAnswer
                ? questionInfo.options[1].key === userAnswer.keyOfAnswer
                : false,
            },
            {
              key: questionInfo.options[2].key,
              name: questionInfo.options[2].name,
              is_correct: questionInfo.options[2].is_correct,
              rate: totalRate[2],
              customerAnswer: userAnswer
                ? questionInfo.options[2].key === userAnswer.keyOfAnswer
                : false,
            },
            {
              key: questionInfo.options[3].key,
              name: questionInfo.options[3].name,
              is_correct: questionInfo.options[3].is_correct,
              rate: totalRate[3],
              customerAnswer: userAnswer
                ? questionInfo.options[3].key === userAnswer.keyOfAnswer
                : false,
            },
          ],
        });
      });

      res.status(200).json({ message: 'ok', result });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: 'Internal Server Error',
        error_message: error.message,
      });
    }
  },
);

module.exports = router;
