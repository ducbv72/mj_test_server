const express = require('express');
const { isArray } = require('lodash');
const passport = require('passport');
const router = express.Router();
const Config = require('../../../config');
const common = require('../../../lib/common');
const Promise = require('promise');
const fs = require('fs');
const {
  nyansumTag,
  productTransform,
  getProductById,
  getProductBySku,
  getProductByIds,
} = require('../../../services/shopify');
const moment = require('moment-timezone');
const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime');
const formidable = require('formidable');
dayjs.extend(relativeTime);
const ObjectId = require('mongodb').ObjectId;
const axios = require('axios');
const { bonus_points_update } = require('../../../config/smileModel');
const url = require('../../../config/url.json');
const { shopify } = require('../../../config/shopify');
const ipfsDag = require('ipfs-dag');
const StockXAPI = require('stockx-api');
const admin = require('firebase-admin');
const stockX = new StockXAPI();
const whiteListSku = '';
const { storefrontInstance } = require('../../../services/shopify');

const {
  productQuery,
  parseProductFromServer,
  parseProductShopifyWithServer,
  decodeBase64,
  encodeBase64,
  mergeCreepyCuties,
  handleCertificate,
} = require('../../../services/product');
const { post } = require('../../../tests/mocks/social-feed');
const { campaignHandle } = require('../../../services/campaign');
const { scanToUnLockHandle } = require('../../../services/scanToUnLock');

/****************** Function ***************/
const coinsAwarded = async (user, sku, variant_id) => {
  try {
    const [{ data: account }, { data: customer }] = await Promise.all([
      axios.get(url.smileAccountUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
        },
      }),
      axios.get(url.smileCustomerUrl.replace('{email}', user.email), {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
          'smile-client': 'smile-admin',
        },
      }),
    ]);

    const account_id = account.accounts.find(
      (e) => e.name === 'Mighty Jaxx',
    ).id;

    if (!customer || !customer.customers.length) {
      return 0;
    }

    const smile_customer_id = customer.customers[0].id;

    let points_change = 0;

    if (variant_id) {
      await shopify.productVariant.get(variant_id).then((product_variant) => {
        points_change = Math.ceil(Number(product_variant.price));
      });
    }

    if (sku && !points_change) {
      const query = `
        {
          productVariants(first:1, query:"sku:${sku}") {
            edges {
              node {
                id
                price
              }
            }
          }
        }
      `;

      await shopify.graphql(query).then((product_variant) => {
        if (product_variant.productVariants.edges.length) {
          points_change = Math.ceil(
            Number(product_variant.productVariants.edges[0].node.price),
          );
        }
      });
    }

    const update_data = bonus_points_update(
      smile_customer_id,
      'Coins awarded on product registration on app',
      'Coins awarded',
      account_id,
      points_change,
    );

    if (points_change > 0) {
      await axios
        .post(
          url.smilePointsTransactionUrl,
          { ...update_data },
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            },
          },
        )
        .then((result) => {
          // console.log(result);
        });
    }

    return points_change;
  } catch (e) {
    console.log('Not found award for this: ' + variant_id, e.message);
    return 0;
  }
};

// Check nyansum tag in customer account
async function checkTagNyan(req, res, next) {
  req.b2bchanel = 'Nyansum';
  if (process.env.IGNORE_TOKEN === 'true') {
    req.b2bchanel = 'Nyansum-staging';
  }
  if (process.env.NODE_ENV === 'test') {
    req.b2bchanel = 'Nyansum-test';
  }
  const { tags } = await shopify.customer.get(Number(req.user.customerId));
  if (tags && tags.includes('exclusive-mobile-nyansum')) {
    next();
  } else {
    res.status(400).json({
      code: 400,
      message: 'This customer does not include exclusive-mobile-nyansum tag.',
    });
  }
}

// Product Altar
const getProductAltar = async (db, user, type) => {
  const [orders] = await Promise.all([
    db.orders
      .aggregate([
        {
          $match: {
            orderEmail: user.email,
          },
        },
        {
          $unwind: {
            path: '$orderProducts',
          },
        },
        {
          $match: {
            'orderProducts.productAltarOptions': {
              $exists: true,
              $ne: null,
            },
          },
        },
        {
          $unwind: {
            path: '$orderProducts',
          },
        },
        {
          $sort: {
            orderDate: -1,
          },
        },
      ])
      .toArray(),
  ]);
  const productsFormat = orders
    .filter((v, index) => {
      const find_index = orders.findIndex(
        (el) =>
          el.orderProducts.productVariantSku ===
          v.orderProducts.productVariantSku,
      );

      return find_index === index;
    })
    .map((v) => ({
      order_id: v._id,
      order: v.orderProducts.productAltarOptions.order,
      variant_id: v.orderProducts.productVariantId,
      title: v.orderProducts.productTitle,
      sku: v.orderProducts.productVariantSku,
      image: v.orderProducts.productImageUrl,
      card_images: v.orderProducts.productAltarOptions.productImageCards,
    }));

  if (type === 'card') {
    const productAltar = productsFormat
      .sort((a, b) => a.order - b.order)
      .map((v) => ({
        order_id: v.order_id,
        variant_id: v.variant_id,
        title: v.title,
        sku: v.sku,
        order: v.order,
        card_images: v.card_images,
      }));

    return { productAltar };
  }

  const productAltar = productsFormat
    .filter((v) => v.order && v.order !== -1)
    .sort((a, b) => a.order - b.order)
    .map((v) => ({
      order_id: v.order_id,
      variant_id: v.variant_id,
      title: v.title,
      sku: v.sku,
      order: v.order,
      image: v.image,
    }));

  const altarOrder = {
    one: { order: 1 },
    two: { order: 2 },
    three: { order: 3 },
    four: { order: 4 },
    five: { order: 5 },
    six: { order: 6 },
    seven: { order: 7 },
  };

  Object.keys(altarOrder).map((i) => {
    altarOrder[i] = productAltar.find((e) => e.order === altarOrder[i].order)
      ? productAltar.find((e) => e.order === altarOrder[i].order)
      : altarOrder[i];
  });

  const productAll = productsFormat.map((v) => ({
    order_id: v.order_id,
    variant_id: v.variant_id,
    title: v.title,
    sku: v.sku,
    image: v.image,
    order: v.order,
    is_delete: Boolean(v.order && v.order !== -1),
  }));
  return { productAltar: altarOrder, productAll };
};
/****************** End Function ***************/

//API

/**
 * @swagger
 * /api/v4/product/{sku}:
 *  get:
 *    security: []
 *    tags:
 *      - Product
 *    description: Get details of product by sku
 *    parameters:
 *    - in: path
 *      name: sku
 *      required: true
 *      schema:
 *        type: string
 *      example: 'LAB-21GBWLTTBSOG42'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '404':
 *        description: No product matching that sku for that product is wrong
 */

router.get('/api/v4/product/:sku', async (req, res) => {
  try {
    const db = req.app.db;

    const product = await getProductBySku(req.params.sku, db);

    if (!product) {
      return res.status(400).json({
        code: 400,
        message: 'invalid sku',
      });
    }

    if (product.productTags) {
      const productTagArr = product.productTags.split(',');
      for (const productTag of product.productTags.split(',')) {
        productTagArr.push(productTag.trim());
      }
      // Query case insensitive
      const optRegexp = [];

      productTagArr.forEach(function (opt) {
        optRegexp.push(new RegExp('^' + opt + '$', 'i'));
      });

      const isExclusiveTag = await db.exclusiveSale.count({
        productTag: {
          $in: optRegexp,
        },
        status: true,
      });

      if (isExclusiveTag > 0) {
        product['type'] = 'exclusive'; // for mobile: to distinguish between regular products and special products
      }
    }
    return res
      .status(200)
      .json({ code: 200, message: 'Product found', data: product });
  } catch (error) {
    res.status(500).json({
      code: 500,
      message: 'Internal Server Error',
      error_message: error.message,
    });
  }
});

/**
 * @swagger
 * /api/v4/product-by-toy-id/{id}:
 *  get:
 *    security: []
 *    tags:
 *      - Product
 *    description: Get toy with digital content by digital content  ID
 *    parameters:
 *    - in: path
 *      name: id
 *      required: true
 *      schema:
 *        type: string
 *      example: '12'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '404':
 *        description: Not found
 */

// To be refactored - Rob
router.get('/api/v4/product-by-toy-id/:id', async (req, res) => {
  const db = req.app.db;
  const toyId = req.params.id;

  const [digitalContent] = await Promise.all([
    db.digitalContents.findOne({
      toyId: toyId,
    }),
  ]);

  if (!digitalContent) {
    res.status(404).json({ message: 'not found', data: [] });
    return;
  }

  const [product] = await Promise.all([
    db.products.findOne({
      productVariantSku: digitalContent.productSku,
    }),
  ]);

  if (!product) {
    res.status(404).json({ message: 'not found', data: [] });
  } else {
    res.status(200).json({ message: 'ok', data: product });
  }
});

/**
 * @swagger
 * /api/v4/product-by-encrypted-message:
 *  post:
 *    tags:
 *      - Product
 *    description: First scan - Send UDID from chip to database to register ownership and return encrypted string to validate authentic chip
 *    consumes:
 *    - application/json
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              order_id:
 *                type: string
 *              encrypted:
 *                type: string
 *              customer_id:
 *                type: string
 *              store_font_key:
 *                type: string
 *              image_uri:
 *                type: string
 *              file_name:
 *                type: string
 *              udid:
 *                type: string
 *          examples:
 *            request:
 *              value: {
 *                order_id: '',
 *                encrypted: 'r5yhYi8Wz8cQezGrSBsUBC8PuXJ7JL68sesHDShLbLxEDpMx+AjiwAOJGO9dlJTf2Pz1VFeXm89uoWKXNfTi95hpHHlRPIgF6lTqNq48x8IoG/Be0Yf+7Z2t04+TIKeI89GaS+/WhtLY43O1deqGXwlKcfsQ4NSq5nVQddMxzfU=',
 *                customer_id: '3897224593542',
 *                store_font_key: '',
 *                image_uri: '',
 *                file_name: '',
 *                udid: '040564727F6381'
 *              }
 *    responses:
 *      '200':
 *        description: A successful response
 *      '404':
 *        description: Product not found
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Server Error
 */
router.post(
  '/api/v4/product-by-encrypted-message',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const user = req.user;
    const customerId = user.customerId;
    const order_id = req.body.order_id;
    const encrypted = req.body.encrypted;
    const udid = req.body.udid ? req.body.udid : '';
    const customer = req.user;
    try {
      if (!customerId || (!encrypted && !udid)) {
        res.status(200).json({
          message: 'Not found',
          code: 404,
          data: null,
        });
        return;
      }
      if (order_id) {
        const order = db.orders.findOne({ shopifyOrderId: order_id });
        if (!order) {
          return res.status(404).json({
            message: 'Product not found',
            code: 404,
            data: null,
          });
        }
        res.status(200).json({
          message: 'ok',
          code: 200,
          data: order,
        });
      } else {
        const order = await db.orders.findOne({
          $and: [
            {
              $or: [
                { encrytedMessage: encrypted },
                { udid: udid.toLocaleUpperCase() },
                { udid: udid.toLocaleLowerCase() },
              ],
            },
            { udid: { $ne: '' } },
            { udid: { $ne: null } },
          ],
        });
        if (order) {
          const qrcode = await db.qrcodes.findOne({
            encrytedMessage: order.encrytedMessage,
          });
          if (!qrcode) {
            console.log('Not found 1: ', udid, ' - ', customerId);
            res.status(404).json({ message: 'Product not found', code: 404 });
          } else {
            // used for scan to win campaign
            if (
              order.orderCertNumber &&
              order.customerId === customer.customerId
            ) {
              const campaign = await db.campaigns.findOne({
                $and: [
                  { isDisable: { $ne: 'true' } },
                  { end_date: { $gte: new Date() } },
                  { start_date: { $lte: new Date() } },
                ],
              });
              if (campaign) {
                const [participateCampaign, userScanedToday] =
                  await Promise.all([
                    db.campaignParticipates.findOne({
                      $and: [
                        { campaign_id: campaign.id },
                        { udid: qrcode.udid },
                        {
                          scan_date: moment(moment.now()).format('DD-MM-YYYY'),
                        },
                      ],
                    }),
                    db.campaignParticipates.findOne({
                      $and: [
                        { campaign_id: campaign.id },
                        { customerId: customer.customerId },
                        {
                          scan_date: moment(moment.now()).format('DD-MM-YYYY'),
                        },
                      ],
                    }),
                  ]);
                if (!participateCampaign && !userScanedToday) {
                  const newParticipate = {
                    campaign_id: campaign.id,
                    toy_id: qrcode.productVariantSku,
                    udid: qrcode.udid,
                    customerLocalId: customer._id,
                    customerId: customer.customerId,
                    scan_date: moment(moment.now()).format('DD-MM-YYYY'),
                    createdAt: moment.now(),
                    scanCount: 1,
                  };

                  await db.campaignParticipates.insert(newParticipate);
                }
              }
            }

            const product = await db.products.findOne({
              _id: common.getId(qrcode.productId),
            });
            //force update image
            if (product) {
              // update product and cc
              const ccProduct = await common.getCreepyCutiesProductsInfo(
                product.productVariantSku,
              );

              if (ccProduct) {
                if (!order.orderProducts[0].productAltarOptions) {
                  order.orderProducts[0].productAltarOptions =
                    ccProduct.productAltarOptions;
                }
              }
              order.orderProducts[0].productImageUrl = product.productImageUrl;
              order.orderProducts[0].productImageUrlS3 =
                product.productImageUrlS3;
              order.orderProducts[0].productBackgroundImageS3 =
                product.productBackgroundImageS3;
            }

            if (!order.udid || !order.password) {
              order.udid = qrcode.udid;
              order.orderProducts[0].password = qrcode.password
                ? qrcode.password
                : '';

              const resultSaveOrder = await db.orders.updateOne(
                { _id: order._id },
                { $set: order },
                { multiple: false },
              );
              if (!resultSaveOrder) {
                console.log('Not found 2: ', udid, ' - ', customerId);
                res
                  .status(404)
                  .json({ message: 'Product not found', code: 404 });
                return;
              }
              if (encrypted) {
                const query = `
                    {
                      productVariants(first:1, query:"sku:${order.sku}") {
                        edges {
                          node {
                            product {
                              tags
                            }
                          }
                        }
                      }
                    }
                  `;

                let tags = [];
                await shopify.graphql(query).then((product_variant) => {
                  if (product_variant.productVariants.edges.length) {
                    tags =
                      product_variant.productVariants.edges[0].node.product
                        .tags;
                  }
                });

                if (tags.length && tags.includes('F12021')) {
                  // Will uncomment later
                  // const coin_awarded = await coinsAwarded(
                  //   user,
                  //   order.sku,
                  //   Number(order.variant_id),
                  // );
                  // order.coin_awarded = coin_awarded;
                }
              }
              res.status(200).json({
                message: 'ok',
                code: 200,
                data: order,
              });
            } else {
              if (encrypted) {
                const query = `
                    {
                      productVariants(first:1, query:"sku:${order.sku}") {
                        edges {
                          node {
                            product {
                              tags
                            }
                          }
                        }
                      }
                    }
                  `;

                let tags = [];
                await shopify.graphql(query).then((product_variant) => {
                  if (product_variant.productVariants.edges.length) {
                    tags =
                      product_variant.productVariants.edges[0].node.product
                        .tags;
                  }
                });

                if (tags.length && tags.includes('F12021')) {
                  // Will uncomment later
                  // const coin_awarded = await coinsAwarded(
                  //   user,
                  //   order.sku,
                  //   Number(order.variant_id),
                  // );
                  // order.coin_awarded = coin_awarded;
                }
              }
              res.status(200).json({
                message: 'ok',
                code: 200,
                data: order,
              });
            }
          }
        } else {
          const qrcode = await db.qrcodes.findOne({
            $and: [
              {
                $or: [
                  { encrytedMessage: encrypted },
                  { udid: udid.toLocaleUpperCase() },
                  { udid: udid.toLocaleLowerCase() },
                ],
              },
              { udid: { $ne: '' } },
              { udid: { $ne: null } },
            ],
          });
          if (!qrcode) {
            console.log('Not found 3: ', udid, ' - ', customerId);
            res.status(404).json({ message: 'product not found', code: 404 });
          } else {
            if (!qrcode.udid) {
              console.log('Not found 4 : ', udid, ' - ', customerId);
              res.status(404).json({ message: 'Product not found', code: 404 });
            } else {
              let product = await db.products.findOne({
                _id: common.getId(qrcode.productId),
              });
              if (!product) {
                res.status(404).json({
                  message: 'Product not found',
                  code: 404,
                  data: null,
                });
              } else {
                try {
                  const customerShopify = await shopify.customer.get(
                    Number(customerId),
                  );
                  if (customerShopify) {
                    product.qrcode = qrcode.qrcodeImage;
                    product.udid = qrcode.udid;
                    product.password = qrcode.password ? qrcode.password : '';
                    product.productModelNumber = product.productVariantSku;
                    product.productEncryptedModel = qrcode.encrytedMessage;
                    if (
                      whiteListSku.indexOf(product.productVariantSku + ' ') > -1
                    ) {
                      product.password = '';
                    } else {
                      if (product.password === '' || !product.password) {
                        product.password = '';
                      }
                    }

                    //merge CreepyCuties
                    product = await mergeCreepyCuties(product);

                    const orderDoc = {
                      shopifyOrderId: new Date().getTime().toString(),
                      customerId_encryted: customerId,
                      customerId: customerId,
                      orderEmail: customerShopify.email,
                      orderFirstname: customerShopify.first_name
                        ? customerShopify.first_name
                        : '',
                      orderLastname: customerShopify.last_name
                        ? customerShopify.last_name
                        : '',
                      orderAddr1: '',
                      orderAddr2: '',
                      orderCountry: '',
                      orderState: '',
                      orderStatus: 'Paid',
                      orderOffline: false,
                      orderPostcode: '',
                      orderPhoneNumber: '',
                      orderComment: '',
                      orderDate: new Date(),
                      orderProducts: [product],
                      line_id: '',
                      shopifyProductId: '',
                      variant_id: product.productVariantId,
                      title: product.productTitle,
                      variant_title: product.productTitle,
                      product_name: product.productTitle,
                      sku: product.productVariantSku,
                      encrytedMessage: qrcode.encrytedMessage,
                      udid: qrcode.udid,
                      editionNumber: qrcode.editionNumber,
                    };

                    await db.orders.insert(orderDoc);
                    res.status(200).json({
                      message: 'ok',
                      code: 200,
                      data: orderDoc,
                    });
                  } else {
                    res.status(404).json({
                      message: 'Product Not found',
                      code: 404,
                      data: null,
                    });
                  }
                } catch (e) {
                  console.error(e);
                  res.status(500).json({
                    message: 'Internal Server Error',
                    code: 500,
                    data: null,
                  });
                }
              }
            }
          }
        }
      }
    } catch (e) {
      console.error(e);
      console.error('Scan fail with customerId: ', customerId, ' - ', udid);
      res.status(500).json({
        message: 'Internal Server Error',
        code: 500,
        data: null,
      });
    }
  },
);

/**
 * @swagger
 * /api/v4/register-to-ether:
 *  post:
 *    tags:
 *      - Product
 *    description: Send chip data to create record in toy database after encrypted string validation
 *    consumes:
 *    - application/json
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              order_id:
 *                type: string
 *              encrypted:
 *                type: string
 *              customer_id:
 *                type: string
 *              udid:
 *                type: string
 *          examples:
 *            request:
 *              value: {
 *                order_id: '',
 *                encrypted: 'r5yhYi8Wz8cQezGrSBsUBC8PuXJ7JL68sesHDShLbLxEDpMx+AjiwAOJGO9dlJTf2Pz1VFeXm89uoWKXNfTi95hpHHlRPIgF6lTqNq48x8IoG/Be0Yf+7Z2t04+TIKeI89GaS+/WhtLY43O1deqGXwlKcfsQ4NSq5nVQddMxzfU=',
 *                customer_id: '3897224593542',
 *                udid: '040564727F6381'
 *              }
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid Parameters/This item has been registered
 *      '404':
 *        description: User not found
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Server Error
 */
router.post(
  '/api/v4/register-to-ether',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const user = req.user;
      const customerId = user.customerId;
      const form = new formidable.IncomingForm();
      form.uploadDir = './public/recipient/';
      form.parse(req, async (err, fields, files) => {
        if (!customerId || !fields.encrypted) {
          return res
            .status(400)
            .json({ message: 'Invalid Parameters', code: 400 });
        }
        const order = await db.orders.findOne({
          'orderProducts.productEncryptedModel': fields.encrypted,
        });
        if (!order) {
          return res
            .status(404)
            .json({ message: 'Product not found', code: 400 });
        }
        if (order && order.orderCertNumber) {
          return res
            .status(400)
            .json({ message: 'This item has been registered', code: 400 });
        }
        const product = order.orderProducts[0];
        //find index if encrypted string in list
        const customerShopify = await shopify.customer.get(Number(customerId));
        if (!customerShopify) {
          return res
            .status(404)
            .json({ message: 'Product not found', code: 400 });
        }

        // generate new certificate + mint
        const newCertificate = await handleCertificate(
          db,
          user,
          product,
          customerShopify,
          order,
          fields.encrypted,
        );

        // handle active campaign
        await campaignHandle(db, user, product, newCertificate);

        // handle scan to unlock event
        await scanToUnLockHandle(db, newCertificate);

        return res.status(200).json({
          message: 'Congratulation!!! Your registration has been completed',
          data: newCertificate,
        });
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

router.post(
  '/api/v4/register-to-polygon',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const user = req.user;
      const customerId = user.customerId;
      const form = new formidable.IncomingForm();
      form.uploadDir = './public/recipient/';
      form.parse(req, async (err, fields, files) => {
        if (!customerId || !fields.encrypted) {
          return res
            .status(400)
            .json({ message: 'Invalid Parameters', code: 400 });
        }
        const order = await db.orders.findOne({
          'orderProducts.productEncryptedModel': fields.encrypted,
        });
        if (!order) {
          return res
            .status(404)
            .json({ message: 'Product not found', code: 400 });
        }
        if (order && order.orderCertNumber) {
          return res
            .status(400)
            .json({ message: 'This item has been registered', code: 400 });
        }
        const product = order.orderProducts[0];
        //find index if encrypted string in list
        const customerShopify = await shopify.customer.get(Number(customerId));
        if (!customerShopify) {
          return res
            .status(404)
            .json({ message: 'Product not found', code: 400 });
        }

        // generate new certificate + mint
        const newCertificate = await handleCertificate(
          db,
          user,
          product,
          customerShopify,
          order,
          fields.encrypted,
        );

        // handle active campaign
        await campaignHandle(db, user, product, newCertificate);

        // handle scan to unlock event
        await scanToUnLockHandle(db, newCertificate);
        return res.status(200).json({
          message: 'Congratulation!!! Your registration has been completed',
          data: newCertificate,
        });
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'Internal Server Error' });
    }
  },
);

/**
 * @swagger
 * /api/v4/change-owner-ship:
 *  post:
 *    tags:
 *      - Product
 *    description: Send new username to database to update ownership
 *    consumes:
 *    - application/json
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              encrypted:
 *                type: string
 *              newOwnerUserName:
 *                type: string
 *              order_id:
 *                type: string
 *          examples:
 *            request:
 *              value: {
 *                encrypted: 'r5yhYi8Wz8cQezGrSBsUBC8PuXJ7JL68sesHDShLbLxEDpMx+AjiwAOJGO9dlJTf2Pz1VFeXm89uoWKXNfTi95hpHHlRPIgF6lTqNq48x8IoG/Be0Yf+7Z2t04+TIKeI89GaS+/WhtLY43O1deqGXwlKcfsQ4NSq5nVQddMxzfU=',
 *                newOwnerUserName: '3897224593542',
 *                order_id: '1616655078851',
 *              }
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid Parameters/Something went wrong
 *      '404':
 *        description: User Not Found/Certificate Not Found
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '403':
 *        description: Not allowed
 */
router.post(
  '/api/v4/change-owner-ship',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const user = req.user;
    const order_id = req.body.order_id;
    const customer_id = user.customerId;
    const newOwnerUserName = req.body.newOwnerUserName
      ? req.body.newOwnerUserName
      : '';
    const newOwnerUserNameLower = req.body.newOwnerUserName
      ? req.body.newOwnerUserName.toLowerCase()
      : '';
    // let newOwnerEmail = req.body.newOwnerEmail ? req.body.newOwnerEmail : '';
    // let newOwnerFirstName = req.body.newOwnerFirstName ? req.body.newOwnerFirstName : '';
    // let newOwnerLastName = req.body.newOwnerLastName ? req.body.newOwnerLastName : '';
    const store_font_key = req.body.store_font_key;
    if (
      !store_font_key ||
      !customer_id ||
      !newOwnerUserName ||
      newOwnerUserName === ''
    ) {
      return res.status(400).json({
        message: 'Invalid Parameters',
        code: 400,
        data: [],
      });
    }

    const customerData = await db.customers.findOne({
      userNameLower: newOwnerUserNameLower,
    });

    if (!customerData) {
      return res.status(404).json({
        message: 'User Not Found',
        code: 400,
      });
    }
    if (customer_id === customerData.customerId) {
      return res.status(400).json({ message: 'Invalid Parameters' });
    }

    const customers = await shopify.customer.search({
      id: customerData.customerId,
    });

    if (!customers) {
      return res.status(400).json({ message: 'Email not found' });
    }
    if (customers.length === 0) {
      return res.status(404).json({ message: 'User not found' });
    }
    const customer = customers[0];

    if (customer === undefined) {
      return res.status(404).json({ message: 'User not found' });
    }

    const result = await db.orders.findOne({ shopifyOrderId: order_id });
    if (!result) {
      return res.status(404).json({ message: 'Certificate Not Found' });
    }

    if (result.customerId !== customer_id) {
      return res.status(403).json({ message: 'Not allowed', code: 403 });
    }

    if (result.orderEmail === customer.email) {
      return res.status(400).json({ message: 'Invalid Parameters' });
    }

    const orderProducts = result.orderProducts;
    let product = null;
    for (let i = 0; i < orderProducts.length; i++) {
      const item = orderProducts[i];
      if (item.productEncryptedModel === result.encrytedMessage) {
        product = item;
        break;
      }
    }
    if (
      product == null ||
      (product != null && product.orderSmartContract == null)
    ) {
      return res.status(400).json({ message: 'error1' });
    }

    if (!result.oldOwnerEmail) {
      result.oldOwnerEmail = [];
    }
    result.oldOwnerEmail.push(result.orderEmail);
    if (!result.oldUserName) {
      result.oldUserName = [];
    }
    result.oldUserName.push(result.orderUserName);

    if (product.blockHashChange && !product.changeOwnerDone) {
      return res.status(400).json({ message: 'processing...' });
    }

    const dataToHash =
      product.productCertNumber + ',' + customer.email + ',' + product.sku;
    const blockHash = new ipfsDag.Node(dataToHash).multihash;

    const numReplaced = await db.orders.update(
      {
        'orderProducts.productEncryptedModel': result.encrytedMessage,
      },
      {
        $set: {
          'orderProducts.$.blockHashChange': blockHash,
          'orderProducts.$.changeOwnerDone': true,
          'orderProducts.$.newOwnerId': customer.id.toString(),
          'orderProducts.$.newOwnerEmail': customer.email,
          'orderProducts.$.newOwnerFirstName': customer.first_name,
          'orderProducts.$.newOwnerLastName': customer.last_name,
          newCustomerId: customer.id.toString(),
          oldUserName: result.oldUserName,
          oldOwnerEmail: result.oldOwnerEmail,
          orderUserName: customerData.userName,
          orderEmail: customer.email,
          customerId: customer.id.toString(),
        },
      },
      { multi: false },
    );

    if (!numReplaced) {
      return res.status(400).json({ message: 'Something went wrong' });
    }

    const payload = {};
    payload.orderId = result.shopifyOrderId.toString();
    payload.productId = product.shopifyProductId.toString();
    payload.newOwnerEmail = customer.email;
    payload.type = 'ChangeOwner';
    const message = {
      notification: {
        title: 'MightyJaxx',
        body: "You're become the owner of a new toy. Click here to view certificate of authenticity",
      },
      data: payload,
    };
    const options = {
      priority: 'high',
    };

    admin
      .messaging()
      .sendToTopic(customer.id.toString(), message, options)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log('Error sending message:', error);
      });
    common.indexOrders(req.app);
    res.status(200).json({
      message: 'Your request has been applied',
    });
  },
);

/**
 * @swagger
 * /api/v4/get-certificate-by-order-id:
 *  get:
 *    tags:
 *      - Product
 *    description: Get owner information for a specific product that is scanned and registered
 *    parameters:
 *    - in: query
 *      name: order_id
 *      required: true
 *      schema:
 *        type: string
 *      example: '1610525812649'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '404':
 *        description: This id type is not true or has been delete from database
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

router.get(
  '/api/v4/get-certificate-by-order-id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const order_id = req.query.order_id;
    const order = await db.orders.findOne({ shopifyOrderId: order_id });

    if (!order) {
      return res.status(404).json({
        code: 404,
        message: 'This id type is not true or has been delete from database',
      });
    }

    return res.status(200).json({ code: 200, data: order });
  },
);

/**
 * @swagger
 * /api/v4/product-stockx-by-sku/{token}/{sku}:
 *  get:
 *    tags:
 *      - Product
 *    description: Get product stockx by sku
 *    parameters:
 *    - in: path
 *      name: token
 *      required: true
 *      schema:
 *        type: string
 *      example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1pZ2h0dHlqYXh4IiwiaWF0IjoxNTE2MjM5MDIyfQ.YUw2k590OjjpNrOBmoFIZmlQ-vc6soSXNfsKOiye3x0'
 *    - in: path
 *      name: sku
 *      required: true
 *      schema:
 *        type: string
 *      example: 'SWDJOG'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: product not found
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

router.get('/api/v4/product-stockx-by-sku/:token/:sku', async (req, res) => {
  try {
    const db = req.app.db;
    const sku = req.params.sku;
    const token = req.params.token;
    if (token !== Config.stockxToken) {
      return res.status(401).json({
        msg: 'Unauthorised. Please refer to administrator.',
        data: null,
      });
    }

    const [product] = await Promise.all([
      db.products.findOne({
        productVariantSku: sku,
      }),
    ]);
    if (product) {
      const slug = product.stockxSlug;
      if ((slug && slug.length === 0) || !product.isShowStockxBanner) {
        res.status(404).json({
          msg: 'Product not found',
          data: null,
        });
      } else {
        const productList = await stockX.searchProducts(slug);
        if (productList && productList.length > 0) {
          const productItem = productList[0];
          productItem.affiliate_url = Config.affiliateUrl + slug;
          productItem.lowest_ask = productItem.market.lowestAsk;
          res.status(200).json({
            msg: 'Product found',
            data: productItem,
          });
        } else {
          res.status(404).json({
            msg: 'Product not found',
            data: null,
          });
        }
      }
    } else {
      res.status(404).json({
        msg: 'Product not found',
        data: null,
      });
    }
  } catch (error) {
    res.status(500).json({
      msg: 'Internal Server Error',
      error_message: error.message,
    });
  }
});

/**
 * @swagger
 * /api/v4/exclusive-sale:
 *  get:
 *    tags:
 *      - Product
 *    description: Get list products with an exclusive sale
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: This customer does not include exclusive-mobile-nyansum tag.
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Error Server
 */

router.get(
  '/api/v4/exclusive-sale',
  passport.authenticate('jwt', { session: false }),
  checkTagNyan,
  async (req, res) => {
    try {
      const db = req.app.db;
      const product = await nyansumTag(req);
      const { products } = product;
      const listProductId = products.edges.map((e) => {
        const buff = Buffer.from(e.node.id.toString(), 'base64');
        const customerIdExtract = buff.toString('utf-8');
        const array = customerIdExtract.split('/');
        return array[4];
      });

      const result = await productTransform(product);

      const [listProducts] = await db.products
        .aggregate([
          {
            $match: { shopifyProductId: { $in: listProductId } },
          },
          {
            $group: {
              _id: '$shopifyProductId',
              productArtist: { $first: '$productArtist' },
            },
          },
        ])
        .toArray();
      if (!listProducts) {
        return res.status(404).json({ message: 'Product not found' });
      }
      result.result[0].artistName = listProducts.productArtist;
      return res.json({ statusCode: 200, result });
    } catch (error) {
      console.error(error);
      res.json({
        statusCode: 500,
        message: 'Internal Error Server',
        error_message: error.message,
      });
    }
  },
);

// Subscribe to nyan channel

/**
 * @swagger
 * /api/v4/exclusive-sale/subscribe:
 *  post:
 *    tags:
 *      - Product
 *    description: Subscribe the notification channel when customer has a nyansumb2b tag
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              registrationTokens:
 *                type: string
 *            required:
 *             - registrationTokens
 *          example: {registrationTokens: 'e26nR_Sw1EQ-l2PpsC0VhW:APA91bFAChf6h10Xm8rV5vt6WkGV3DA0iKhj-15rtNSojcxDot1B1VDFngFI7BX3I6mhs6yEtwuRuWd6YlnA5DyTPj-06fTCASzXsfv-PY6FLkb2BmzCzSCGsMq-jFVcoLH-g_vHoKI2'}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Error subscribing to channel
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Error Server
 */
router.post(
  '/api/v4/exclusive-sale/subscribe',
  passport.authenticate('jwt', { session: false }),
  checkTagNyan,
  async (req, res) => {
    try {
      // These registration tokens come from the client FCM SDKs.
      const registrationTokens = req.body.registrationTokens;
      const topic = req.b2bchanel;
      // Subscribe the devices corresponding to the registration tokens to the
      // topic.
      admin
        .messaging()
        .subscribeToTopic(registrationTokens, topic)
        .then(function (respone) {
          // See the MessagingTopicManagementResponse reference documentation
          // for the contents of response.
          console.log('Successfully subscribed to topic:', respone);
          res.json({
            code: 200,
            message: `Successfully subscribed to topic: ${topic}`,
          });
        })
        .catch(function (error) {
          console.log('Error subscribing to topic:', error);
          res.json({
            code: 400,
            message: `Error subscribing to topic: ${topic}`,
          });
        });
    } catch (error) {
      console.log(error.message);
      res.json({
        code: 500,
        message: 'Internal Error Server',
      });
    }
  },
);

// Unsubscribe to nyan channel
/**
 * @swagger
 * /api/v4/exclusive-sale/unsubscribe:
 *  post:
 *    tags:
 *      - Product
 *    description: Unubscribe the notification channel when customer has a nyansumb2b tag
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              registrationTokens:
 *                type: string
 *            required:
 *             - registrationTokens
 *          example: {registrationTokens: 'e26nR_Sw1EQ-l2PpsC0VhW:APA91bFAChf6h10Xm8rV5vt6WkGV3DA0iKhj-15rtNSojcxDot1B1VDFngFI7BX3I6mhs6yEtwuRuWd6YlnA5DyTPj-06fTCASzXsfv-PY6FLkb2BmzCzSCGsMq-jFVcoLH-g_vHoKI2'}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Error unsubscribing from channel
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Error Server
 */

router.post(
  '/api/v4/exclusive-sale/unsubscribe',
  passport.authenticate('jwt', { session: false }),
  checkTagNyan,
  async (req, res) => {
    try {
      // These registration tokens come from the client FCM SDKs.
      const registrationTokens = req.body.registrationTokens;
      const topic = req.b2bchanel;
      // Unsubscribe the devices corresponding to the registration tokens from
      // the topic.
      admin
        .messaging()
        .unsubscribeFromTopic(registrationTokens, topic)
        .then(function (response) {
          // See the MessagingTopicManagementResponse reference documentation
          // for the contents of response.
          console.log('Successfully unsubscribed from topic:', response);
          res.json({
            code: 200,
            message: `Successfully unsubscribed from topic: ${topic}`,
          });
        })
        .catch(function (error) {
          console.log('Error unsubscribing from topic:', error);
          res.json({
            code: 400,
            message: `Error unsubscribing from topic: ${topic}`,
          });
        });
    } catch (error) {
      res.json({
        code: 500,
        message: 'Internal Error Server',
      });
    }
  },
);

/**
 * @swagger
 * /api/v4/products/altar:
 *  get:
 *    tags:
 *      - Product
 *    description: Get Altar for creepy cuties
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: An error occurred while processing your request
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Error Server
 */

router.get(
  '/api/v4/products/altar',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const user = req.user;
    const { type = 'image' } = req.query;

    try {
      const products = await getProductAltar(db, user, type);

      return res.status(200).json({
        code: 200,
        message: 'Success',
        data: products,
      });
    } catch (error) {
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

/**
 * @swagger
 * /api/v4/products/altar/change-location:
 *  post:
 *    tags:
 *      - Product
 *    description: Change positon of product on the Creepy Cuties Altar
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              productAltar:
 *                type: object
 *                properties:
 *                  one:
 *                    type: object
 *                    properties:
 *                      order:
 *                        type: string
 *                    required:
 *                     - order
 *                  two:
 *                    type: object
 *                    properties:
 *                      order:
 *                        type: string
 *                    required:
 *                     - order
 *                  three:
 *                    type: object
 *                    properties:
 *                      order:
 *                        type: string
 *                    required:
 *                     - order
 *                  four:
 *                    type: object
 *                    properties:
 *                      order:
 *                        type: string
 *                    required:
 *                     - order
 *                  five:
 *                    type: object
 *                    properties:
 *                      order:
 *                        type: string
 *                    required:
 *                     - order
 *                  six:
 *                    type: object
 *                    properties:
 *                      order:
 *                        type: string
 *                    required:
 *                     - order
 *                  seven:
 *                    type: object
 *                    properties:
 *                      order:
 *                        type: string
 *                    required:
 *                     - order
 *                required:
 *                 - one
 *                 - two
 *                 - three
 *                 - four
 *                 - five
 *                 - six
 *                 - seven
 *            required:
 *             - productAltar
 *          example: {productAltar: {one: {order: 1},two: {order: 2},three: {order: 3},four: {order: 4},five: {order: 5},six: {order: 6},seven: {order: 7}}}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: product not found
 *      '401':
 *        description: Not a validated user.  Need to login first
 */
router.post(
  '/api/v4/products/altar/change-location',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const user = req.user;
      const { productAltar } = req.body;

      const productsAltarNew = Object.keys(productAltar).map(
        (key) => productAltar[key],
      );
      const [_ordersCreepyCutiesOld] = await Promise.all([
        db.orders
          .aggregate([
            {
              $match: {
                orderEmail: user.email,
              },
            },
            {
              $unwind: { path: '$orderProducts' },
            },
            {
              $match: {
                'orderProducts.productAltarOptions': {
                  $ne: null,
                },
              },
            },
            {
              $group: {
                _id: '$_id',
                doc: {
                  $first: '$$ROOT',
                },
              },
            },
            {
              $replaceRoot: {
                newRoot: '$doc',
              },
            },
          ])
          .toArray(),
      ]);
      const ordersCreepyCutiesOld = _ordersCreepyCutiesOld.map((v) => {
        if (isArray(v.orderProducts)) {
          return v;
        }

        return {
          ...v,
          orderProducts: [v.orderProducts],
        };
      });
      const ordersCreepyCutiesNew = ordersCreepyCutiesOld
        .filter((v) => {
          const findProductAltarNew = productsAltarNew.find(
            (el) => el.order_id === String(v._id),
          );
          const findProductAltarOld = v.orderProducts.find(
            (el) => el.productAltarOptions,
          );

          if (
            !findProductAltarNew &&
            findProductAltarOld &&
            findProductAltarOld.productAltarOptions.order &&
            findProductAltarOld.productAltarOptions.order > 0
          ) {
            return true;
          }

          if (
            findProductAltarNew &&
            findProductAltarOld &&
            findProductAltarNew.order !==
              findProductAltarOld.productAltarOptions.order
          ) {
            return true;
          }

          return false;
        })
        .map((v) => {
          const findProductAltarNew = productsAltarNew.find(
            (el) => el.order_id === String(v._id),
          );
          const findProductAltarOld = v.orderProducts.find(
            (el) => el.productAltarOptions,
          );

          let order = -1;

          if (
            findProductAltarNew &&
            findProductAltarOld &&
            findProductAltarNew.order !==
              findProductAltarOld.productAltarOptions.order
          ) {
            order = findProductAltarNew.order;
          }

          const orderProducts = v.orderProducts.map((el) => {
            if (String(el._id) === String(findProductAltarOld._id)) {
              return {
                ...el,
                productAltarOptions: {
                  ...el.productAltarOptions,
                  order: Number(order),
                },
              };
            }

            return el;
          });

          return {
            ...v,
            orderProducts,
          };
        });
      await Promise.all([
        ordersCreepyCutiesNew.map((v) =>
          db.orders.updateOne(
            { _id: ObjectId(v._id) },
            { $set: { orderProducts: v.orderProducts } },
          ),
        ),
      ]);

      return res.status(200).json({ code: 200, message: 'Success' });
    } catch (error) {
      return res.status(400).json({ code: 400, message: error.message });
    }
  },
);

/**
 * @swagger
 * /api/v4/getCodesForProduct/:udid:
 *  get:
 *    tags:
 *      - Product
 *    description: Api for scan to unlock voucher codes
 *    parameters:
 *    - in: path
 *      name: udid
 *      required: true
 *      schema:
 *        type: string
 *      example: '049F25727F6380'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Not found
 *      '500':
 *        description: Internal Error Server
 */

// Api for scan to unlock game/shift codes
router.get('/api/v4/getCodesForProduct/:udid', async (req, res) => {
  const db = req.app.db;
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  const udid = req.params.udid;

  const [data] = await Promise.all([
    db.gameAndShiftCode.find({ udid: udid }).toArray(),
  ]);

  if (!data) {
    return res.status(400).json({ code: 400, message: 'Not found' });
  }

  return res.status(200).json({
    code: 200,
    data: data,
    message: 'ok',
  });
});

/**
 * @swagger
 * /api/v4/scan-to-unlock-code:
 *  post:
 *    tags:
 *      - Product
 *    description: Api for scan to unlock codes
 *    parameters:
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              id:
 *                type: string
 *                example: '520a19a2e03e822c7235ca10'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorised. Please refer to administrator.
 *      '404':
 *        description: Code Not Found
 *      '500':
 *        description: Internal Error Server
 */

router.post(
  '/api/v4/scan-to-unlock-code',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const id = req.body.id;
      const user = req.user;
      const [customer, gameAndShiftCode] = await Promise.all([
        db.customers.findOne({ customerId: user.customerId }),
        db.gameAndShiftCode.findOne({ _id: common.getId(id) }),
      ]);

      if (!customer) {
        res.status(401).json({
          message: 'Unauthorised. Please refer to administrator.',
          data: null,
        });
        return;
      }
      if (!gameAndShiftCode) {
        res.status(404).json({
          message: 'Code Not Found',
          data: null,
        });
        return;
      }
      if (gameAndShiftCode.status === 'Unallocated') {
        const [updateDoc] = await Promise.all([
          db.gameAndShiftCode.update(
            {
              _id: gameAndShiftCode._id,
            },
            { $set: { status: 'Allocated' } },
            { multi: false },
          ),
        ]);

        if (!updateDoc.result.n) {
          return res
            .status(400)
            .json({ code: 400, message: 'Update failed', data: null });
        }

        const result = await Promise.all([
          db.gameAndShiftCode.findOne({
            _id: gameAndShiftCode._id,
          }),
        ]);

        return res.status(200).json({
          message: 'Unlock Code Success',
          data: result,
        });
      } else {
        return res.status(400).json({
          message: 'Code has been unlocked or expired',
          data: [],
        });
      }
    } catch (error) {
      res.json({
        code: 500,
        message: 'Internal server error',
      });
    }
  },
);

/**
 * @swagger
 * /api/v4/getVoucherCodesForProduct/:udid:
 *  get:
 *    tags:
 *      - Product
 *    description: Api for scan to unlock voucher codes
 *    parameters:
 *    - in: path
 *      name: udid
 *      required: true
 *      schema:
 *        type: string
 *      example: '049F25727F6380'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Your product is not valid type
 *      '500':
 *        description: Internal Error Server
 */

// Api for scan to unlock voucher codes
router.get(
  '/api/v4/getVoucherCodesForProduct/:sku',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept',
    );
    const email = req.user.email;
    const udid = req.query.udid;
    const sku = req.params.sku;

    const data = await db.voucherCodes
      .aggregate([
        {
          $match: { udid: udid },
        },
        {
          $unwind: '$unlockVoucherWithSKU',
        },
        {
          $lookup: {
            from: 'products',
            let: { unlockVoucherWithSKU: '$unlockVoucherWithSKU' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      {
                        $eq: ['$$unlockVoucherWithSKU', '$productVariantSku'],
                      },
                    ],
                  },
                },
              },
              {
                $project: {
                  productTitle: 1,
                  productImageUrl: 1,
                },
              },
            ],
            as: 'product',
          },
        },
        {
          $unwind: '$product',
        },
        {
          $group: {
            _id: '$_id',
            products: { $push: '$product' },
            udid: { $first: '$udid' },
            productVariantSku: { $first: '$productVariantSku' },
            createdAt: { $first: '$createdAt' },
            status: { $first: '$status' },
          },
        },
      ])
      .toArray();

    return res.status(200).json({
      code: 200,
      data: data,
      message: 'ok',
    });
  },
);

/**
 * @swagger
 * /api/v4/scan-to-unlock-voucher-code:
 *  post:
 *    tags:
 *      - Product
 *    description: Api for scan to unlock voucher codes
 *    parameters:
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              id:
 *                type: string
 *                example: '61dfe3df4a26b337896f29e1'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorised. Please refer to administrator.
 *      '404':
 *        description: Code Not Found
 *      '500':
 *        description: Internal Error Server
 */

router.post(
  '/api/v4/scan-to-unlock-voucher-code',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const id = req.body.id;
      const voucherCode = await db.voucherCodes.findOne({
        _id: common.getId(id),
      });

      if (!voucherCode) {
        res.status(404).json({
          message: 'Voucher Code Not Found',
          data: null,
        });
        return;
      }

      if (voucherCode.status !== 'Unallocated') {
        return res.status(400).json({
          code: 400,
          message:
            'This voucher code had been unlocked or expried, please try again.',
        });
      }

      const updateVoucherCode = await db.voucherCodes.updateOne(
        { _id: common.getId(id) },
        { $set: { status: 'Allocated' } },
        {},
      );

      if (!updateVoucherCode.result.n) {
        return res.status(400).json({ code: 400, message: 'Update failed' });
      }

      res.status(200).json({
        code: 200,
        message: 'Unlocked successfully',
      });
    } catch (error) {
      console.log('POST /api/v4/scan-to-unlock-voucher-code', error);
      res.status(500).json({
        code: 500,
        message: 'Internal server error',
      });
    }
  },
);

/**
 * @swagger
 * /api/v4/product/byHandle/:handle:
 *  get:
 *    tags:
 *      - Product
 *    description: Get product by id (base64)
 *    parameters:
 *    - in: path
 *      name: hanlde
 *      required: true
 *      schema:
 *        type: string
 *      example: 'all'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Your product is not valid type
 *      '500':
 *        description: Internal Error Server
 */

router.get('/api/v4/product/byHandle/:handle', async (req, res) => {
  try {
    const db = req.app.db;
    const { handle } = req.params;
    const query = `
        query{
          productByHandle(handle: "${handle}"){
            ${productQuery()}
          }
        }
        `;

    const productByHandle = await storefrontInstance.post(null, {
      query: query,
    });

    if (!productByHandle.data) {
      return res.json({ data: {} });
    }

    const { data } = productByHandle;
    const product = data.data.productByHandle;

    if (!product) {
      return res.status(200).json({
        code: 200,
        message: 'Success',
        data: {},
      });
    }
    const [dbProduct] = await Promise.all([
      db.products.findOne({ shopifyProductId: decodeBase64(product.id) }),
    ]);

    const findProduct =
      dbProduct && Object.keys(dbProduct).length
        ? parseProductFromServer(dbProduct)
        : null;

    if (findProduct) {
      parseProductShopifyWithServer(product, findProduct);
    }

    return res.status(200).json({
      code: 200,
      message: 'Success',
      data: product,
    });
  } catch (error) {
    return res.status(400).json({ code: 400, message: error.message });
  }
});

/**
 * @swagger
 * /api/v4/product/byId/:id:
 *  get:
 *    tags:
 *      - Product
 *    description: Get product by id (base64)
 *    parameters:
 *    - in: path
 *      name: id
 *      required: true
 *      schema:
 *        type: string
 *      example: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzM5ODkzNDYwMQ=='
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Your product is not valid type
 *      '500':
 *        description: Internal Error Server
 */

router.get('/api/v4/product/byId/:id', async (req, res) => {
  try {
    const db = req.app.db;
    const { id } = req.params;
    const product = await getProductById(id, db);
    if (product && product.productCrossSellContent) {
      product.productCrossSellContent = encodeBase64(
        `gid://shopify/Product/${product.productCrossSellContent}`,
      );
    }
    if (!product) {
      return res.status(400).json({
        code: 400,
        message: 'Invalid global id',
      });
    } else {
      return res.status(200).json({
        code: 200,
        message: 'Success',
        data: product,
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(400).json({ code: 400, message: error.message });
  }
});

/**
 * @swagger
 * /api/v4/products/byIds/:ids:
 *  get:
 *    tags:
 *      - Product
 *    description: Get list products by array of id
 *    parameters:
 *    - in: path
 *      name: ids
 *      required: true
 *      schema:
 *        type: string
 *      example: 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzM5ODkzNDYwMQ==,Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0LzM5ODk4OTg0MQ=='
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Your product is not valid type
 *      '500':
 *        description: Internal Error Server
 */

router.get('/api/v4/products/byIds/:ids', async (req, res) => {
  try {
    const db = req.app.db;
    const { ids } = req.params;
    if (!ids) {
      return res.status(400).json({ code: 400, message: 'Invalid global id' });
    }
    if (ids !== 'null') {
      const listIdBase64 = ids.split(',');
      const products = await getProductByIds(listIdBase64, db);
      return res.status(200).json({
        code: 200,
        message: 'Success',
        data: products,
      });
    } else {
      return res.status(200).json({
        code: 200,
        data: [],
        message: 'Success',
      });
    }
  } catch (error) {
    console.log(error);
    return res.status(400).json({ code: 400, message: error.message });
  }
});

/**
 * @swagger
 * /api/v4/update-cc-product/:sku:
 *  get:
 *    tags:
 *      - Product
 *    description: Update creepy cuties info when update json
 *    parameters:
 *    - in: path
 *      name: sku
 *      required: true
 *      schema:
 *        type: string
 *      example: 'LND-21CCSKWH-08'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Your product is not valid type
 *      '500':
 *        description: Internal Error Server
 */
router.get('/api/v4/update-cc-product/:sku', async (req, res) => {
  const db = req.app.db;
  const sku = req.params.sku;

  const findCCOrders = await db.orders.find({ sku }).toArray();
  if (!findCCOrders || !findCCOrders.length) {
    return res
      .status(400)
      .json({ message: 'Your product is not a cc product' });
  }
  const ccProduct = await common.getCreepyCutiesProductsInfo(sku);
  if (ccProduct) {
    const updateData = await db.orders.updateMany(
      { sku },
      {
        $set: {
          'orderProducts.0.productAltarOptions': ccProduct.productAltarOptions,
          'orderProducts.0.productImageUrl': ccProduct.productImageUrl,
          'orderProducts.0.productImageUrlS3': ccProduct.productImageUrlS3,
          'orderProducts.0.productBackgroundImageS3':
            ccProduct.productBackgroundImageS3,
        },
      },
    );
    if (!updateData.result.n) {
      return res.status(400).json({ message: 'Updated failed!' });
    }
  }

  res.status(200).json({ message: 'Updated!' });
});

module.exports = router;
