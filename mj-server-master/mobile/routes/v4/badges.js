const express = require('express');
const router = express.Router();
const Promise = require('promise');
const common = require('../../../lib/common');
const passport = require('passport');
//function check award badge
const checkAwardBadges = async (userId, db) => {
  const [badges, allUserBadges, userOrders] = await Promise.all([
    db.badges
      .find()
      .project({
        _id: 1,
        name: 1,
        scanCountRequired: 1,
        unlockCriteria: 1,
        matchingCriteria: 1,
      })
      .toArray(),
    db.userBadges.findOne({ userId: userId }),
    db.orders.find({ customerId: userId }).toArray(),
  ]);
  // get all user sku : userSkus
  const allProducts = [];
  for (const products of userOrders) {
    allProducts.push(...products.orderProducts);
  }
  const listAllSku = allProducts.map((e) => {
    return e.productVariantSku;
  });
  // remove all duplicate
  const listProductSku = unique(listAllSku);
  if (listProductSku.length > 0) {
    const badgesNotAward = badges.filter((e) => {
      if (allUserBadges) {
        let check = true;
        const userBadges = allUserBadges.badges;
        for (const userBadge of userBadges) {
          if (e._id == userBadge.id && userBadge.unlockedDate != null) {
            check = false;
          }
        }
        if (check) {
          return e;
        }
      } else {
        return e;
      }
    });
    const arrBadges = [];
    const idAward = [];
    // check cond and award or update
    for (const badge of badgesNotAward) {
      if (badge.unlockCriteria == 'countUniqueSku') {
        const skuBadges = badge.matchingCriteria.split(',');
        let scanCount = 0;
        for (const skuBadge of skuBadges) {
          for (const sku of listProductSku) {
            if (skuBadge == sku) {
              scanCount += 1;
            }
          }
        }
        if (scanCount >= Number(badge.scanCountRequired)) {
          const bg = {
            id: badge._id.toString(),
            name: badge.name,
            unlockedDate: new Date(),
            isViewed: false,
            scanCount: badge.scanCountRequired,
          };
          arrBadges.push(bg);
          idAward.push(common.getId(bg.id));
          //await awardBadge(userId, bg, db);
        } else if (scanCount != 0) {
          const bg = {
            id: badge._id.toString(),
            name: badge.name,
            unlockedDate: null,
            isViewed: false,
            scanCount: scanCount,
          };
          arrBadges.push(bg);
          //await awardBadge(userId, bg, db);
        }
      }
      if (badge.unlockCriteria == 'countSku') {
        // badge sku require
        const skuBadges = badge.matchingCriteria.split(',');
        let scanCount = 0;
        for (const skuBadge of skuBadges) {
          const skuBadgeUpper = skuBadge.toUpperCase();
          for (const sku of listProductSku) {
            const skuUpper = sku.toUpperCase();
            if (skuUpper.includes(skuBadgeUpper)) {
              scanCount += 1;
            }
          }
        }
        if (scanCount >= Number(badge.scanCountRequired)) {
          const bg = {
            id: badge._id.toString(),
            name: badge.name,
            unlockedDate: new Date(),
            isViewed: false,
            scanCount: badge.scanCountRequired,
          };
          arrBadges.push(bg);
          idAward.push(common.getId(bg.id));

          //await awardBadge(userId, bg, db);
        } else if (scanCount != 0) {
          const bg = {
            id: badge._id.toString(),
            name: badge.name,
            unlockedDate: null,
            isViewed: false,
            scanCount: scanCount,
          };
          arrBadges.push(bg);
          //await awardBadge(userId, bg, db);
        }
      }
    }
    await awardBadges(userId, arrBadges, db);
    await increaseCount(idAward, db);
  }
};
const awardBadges = async (userId, arrBadges, db) => {
  const userBadges = await db.userBadges.findOne({ userId: userId });
  if (userBadges) {
    const badges = [];
    const userBgs = userBadges.badges.filter((e) => {
      if (e.unlockedDate == null) {
        return e;
      }
    });
    for (const bg of userBadges.badges) {
      if (bg.unlockedDate != null) {
        badges.push(bg);
      }
    }
    for (const badge of arrBadges) {
      let checkAvailable = false;
      for (const item of userBgs) {
        if (item.id == badge.id) {
          badges.push(badge);
          checkAvailable = true;
        }
      }
      if (!checkAvailable) {
        badges.push(badge);
      }
    }
    await db.userBadges.update(
      { userId: userId },
      { $set: { badges: badges } },
    );
  } else {
    return await db.userBadges.insert({ userId: userId, badges: arrBadges });
  }
};
// function bubble sort
function bubble(badges) {
  let check = false;
  for (let i = 0; i < badges.length - 1; i++) {
    if (
      badges[i].step < badges[i].totalStep &&
      badges[i + 1].step == badges[i + 1].totalStep
    ) {
      const tmp = badges[i + 1];
      badges[i + 1] = badges[i];
      badges[i] = tmp;
      check = true;
    }
  }
  if (check == true) {
    return bubble(badges);
  } else return badges;
}
// increase number of user have been awarded
const increaseCount = async (arrIdBadges, db) => {
  await db.badges.updateMany(
    { _id: { $in: arrIdBadges } },
    { $inc: { totalAward: 1 } },
  );
  return;
};
// remove duplicate
function unique(arr) {
  let newArr = [];
  newArr = arr.filter(function (item) {
    return newArr.includes(item) ? '' : newArr.push(item);
  });
  return newArr;
}
router.get(
  '/api/v4/awardBadges',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const userId = req.user.customerId;
      await checkAwardBadges(userId, db);
      res.status(200).json({
        message: 'ok',
      });
    } catch (err) {
      res.status(400).json({
        message: 'Opps! Something wrong',
        data: err,
      });
    }
  },
);
// all badges
router.get(
  '/api/v4/badges',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const userId = req.user.customerId;
    await checkAwardBadges(userId, db);
    const [userBadges, cate] = await Promise.all([
      db.userBadges.findOne({ userId: userId }),
      db.badges
        .aggregate([
          {
            $group: {
              _id: '$badgeCategory',
              count: { $sum: 1 },
            },
          },
          { $sort: { _id: 1 } },
        ])
        .toArray(),
    ]);

    const badgeCategories = [];
    const ids = cate.map((c) => c._id);
    let badges = await db.badges
      .aggregate([
        { $match: { badgeCategory: { $in: ids } } },
        {
          $project: {
            _id: 1,
            id: 1,
            createdDate: 1,
            badgeCategory: 1,
            scanCountRequired: 1,
            name: 1,
            description: 1,
            iconImage: 1,
            iconImageInActive: 1,
            shelfSkin: 1,
          },
        },
        { $sort: { name: 1 } },
      ])
      .toArray();
    badges = badges.map((b) => {
      b.totalStep = Number(b.scanCountRequired);
      b.step = b.step ? b.step : 0;
      return b;
    });
    for (const cateItem of cate) {
      let MyBadgesUnlocked = 0;
      const workingBanges = badges.filter(
        (b) => b.badgeCategory == cateItem._id,
      );

      if (userBadges) {
        for (const uban of userBadges.badges) {
          const bandgeItems = workingBanges.filter((b) => b._id == uban.id);

          if (bandgeItems && bandgeItems.length > 0) {
            bandgeItems[0].step = Number(uban.scanCount);
            MyBadgesUnlocked += uban.unlockedDate != null ? 1 : 0;
          }
        }
      } else {
        for (const item of workingBanges) {
          item.totalStep = Number(item.scanCountRequired);
          item.step = 0;
        }
      }
      // sort the data result ascending scan required to unlock badge
      await workingBanges.sort((a, b) =>
        a.scanCountRequired >= b.scanCountRequired ? 1 : -1,
      );
      //sort the data result badge awarded first
      //await bubble(workingBanges);

      const item = {
        totalBadges: cateItem.count,
        MyBadgesUnlocked: MyBadgesUnlocked,
        Name: cateItem._id,
        Badges: workingBanges,
      };
      badgeCategories.push(item);
    }

    res.status(200).json({
      message: 'ok',
      data: { BadgeCategories: badgeCategories },
    });
  },
);
// show how to get a badge
router.get(
  '/api/v4/badges/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const idBadge = req.params.id;
    const userId = req.user.customerId;
    const [badge, userBadges] = await Promise.all([
      db.badges.findOne({ _id: common.getId(idBadge) }),
      db.userBadges.findOne({ userId: userId }),
    ]);
    if (!badge) {
      res.status(200).json({
        message: 'Not found this badge',
        data: [],
      });
    } else {
      const result = {
        _id: badge._id,
        createdDate: badge.createdDate,
        name: badge.name,
        description: badge.description,
        iconImage: badge.iconImage,
        backgroundSkin: badge.backgroundSkin,
        shelfSkin: badge.shelfSkin,
        totalStep: Number(badge.scanCountRequired),
        step: 0,
      };
      if (userBadges) {
        for (const uban of userBadges.badges) {
          if (result._id == uban.id) {
            result.step = Number(uban.scanCount);
          }
        }
      }

      res.status(200).json({
        message: 'ok',
        data: result,
      });
    }
  },
);

// show badges were awarded but not view
router.get(
  '/api/v4/badgeViewed',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const userId = req.user.customerId;
    const [userBadges] = await Promise.all([
      db.userBadges.findOne({ userId: userId }),
    ]);
    if (!userBadges) {
      return res.status(200).json({
        message: 'No badges',
        data: [],
      });
    } else {
      const myBadges = userBadges.badges;
      const badges = myBadges.filter((e) => {
        return !e.isViewed && e.unlockedDate != null ? e.id : '';
      });
      const listId = badges.map((v) => {
        return common.getId(v.id);
      });
      const result = await db.badges
        .aggregate([
          { $match: { _id: { $in: listId } } },
          {
            $project: {
              _id: 1,
              name: 1,
              description: 1,
              iconImage: 1,
              shelfSkin: 1,
              backgroundSkin: 1,
            },
          },
        ])
        .toArray();

      res.status(200).json({
        message: 'ok',
        data: result,
      });
    }
  },
);
// view badge
router.get(
  '/api/v4/badgeViewed/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const userId = req.user.customerId;
    const idBadge = req.params.id;
    const [userBadges] = await Promise.all([
      db.userBadges.findOne({ userId: userId }),
    ]);
    if (userBadges) {
      const list = userBadges.badges.map((v) => {
        if (v.id == idBadge && v.unlockedDate != null) {
          v.isViewed = true;
        }
        return v;
      });
      db.userBadges.update(
        { userId: userId },
        { $set: { badges: list } },
        {},
        (err) => {
          if (!err) {
            res.status(200).json({
              message: 'update success',
            });
          }
        },
      );
    } else {
      res.status(400).json({
        message: 'This badge is not available or you do not earned this',
      });
    }
  },
);
// api get all awarded selfskin and background
router.get(
  '/api/v4/badgeSkins',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const userId = req.user.customerId;
    const [userBadges] = await Promise.all([
      db.userBadges.findOne({ userId: userId }),
    ]);
    if (userBadges) {
      const listMyBadges = userBadges.badges.filter((v) => {
        return v.unlockedDate != null ? v : '';
      });
      const listIdMyBadges = listMyBadges.map((v) => {
        return common.getId(v.id);
      });
      const [skins, backs] = await Promise.all([
        db.badges
          .aggregate([
            { $match: { _id: { $in: listIdMyBadges } } },
            {
              $project: {
                shelfSkin: 1,
                name: 1,
              },
            },
            { $unwind: '$shelfSkin' },
          ])
          .toArray(),
        db.badges
          .aggregate([
            { $match: { _id: { $in: listIdMyBadges } } },
            {
              $project: {
                backgroundSkin: 1,
                name: 1,
              },
            },
            { $unwind: '$backgroundSkin' },
          ])
          .toArray(),
      ]);
      res.status(200).json({
        message: 'ok',
        data: {
          listBackgroundSkin: backs,
          listShelfSkin: skins,
        },
      });
    } else {
      res.status(200).json({
        message: 'Not Awarded',
        data: [],
      });
    }
  },
);
module.exports = router;
