const express = require('express');
const router = express.Router();

const account = require('./account');
const artist = require('./artist');
const campaigns = require('./campaigns');
const community = require('./community');
const fileManagement = require('./file-management');
const home = require('./home');
const nft = require('./nft');
const product = require('./product');
const smile = require('./smile');
const socialFeed = require('./social-feed');
const socialSignIn = require('./social-sign-in');
const vip = require('./vip');
const exclusiveSale = require('./exclusive_sale');
const stores = require('./stores');
const badges = require('./badges');
const web3Auth = require('./web3Auth');

router.use('/', account);
router.use('/', artist);
router.use('/', campaigns);
router.use('/', community);
router.use('/', fileManagement);
router.use('/', home);
router.use('/', nft);
router.use('/', product);
router.use('/', smile);
router.use('/', socialFeed);
router.use('/', socialSignIn);
router.use('/', vip);
router.use('/', exclusiveSale);
router.use('/', stores);
router.use('/', badges);
router.use('/', web3Auth);

module.exports = router;
