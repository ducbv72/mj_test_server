const express = require('express');
const passport = require('passport');
const router = express.Router();
const Promise = require('promise');
const { customValid } = require('../../../validator');
const dayjs = require('dayjs');
const axios = require('axios');
const url = require('../../../config/url.json');
const request = require('request');

/**
 * @swagger
 * /api/v4/smile-io/customers:
 *  get:
 *    tags:
 *      - Smile
 *    description: Get a specific customer's information from Smile
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user. Need to login first
 *      '404':
 *        description: User Not found
 *      '500':
 *        description: Internal Error Server
 */

router.get(
  '/api/v4/smile-io/customers',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const { email } = req.user;
    const customerUrl = url.smileCustomerUrl.replace('{email}', email);
    const pointsTransactionUrl = url.smilePointsTransactionUrl;
    let smileAccountId = null;
    let smileIoCustomer = null;
    try {
      const [{ data: customerInfo }] = await Promise.all([
        axios.get(customerUrl, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        }),
      ]);

      if (customerInfo.customers.length > 0) {
        const customer = customerInfo.customers[0];
        smileAccountId = customer.account_id;
        smileIoCustomer = {
          id: customer.id,
          first_name: customer.first_name,
          last_name: customer.last_name,
          email: customer.email,
          date_of_birth: customer.date_of_birth,
          points_balance: customer.points_balance,
          referral_url: customer.referral_url,
          state: customer.state,
          total_points: customer.vip_metric,
          delta_to_next_vip_tier: customer.delta_to_next_vip_tier,
          vip_tier_id: customer.vip_tier_id,
          created_at: customer.created_at,
          updated_at: customer.updated_at,
          vip_tier: {
            name: customer.vip_tier.name,
            image_url: customer.vip_tier.image_url,
          },
          expire_in: customer.vip_tier_expires_at,
        };
      } else {
        return res.status(404).json({
          msg: 'User Not found',
          code: 404,
          data: null,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Internal Error Server',
        code: 500,
        data: null,
      });
    }

    const rewardProgramsPointUrl = url.smileRewardProgramsPointUrl.replace(
      '{account_id}',
      smileAccountId,
    );

    const [{ data: pointPrograms }, { data: pointTransactions }] =
      await Promise.all([
        axios.get(rewardProgramsPointUrl, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        }),
        axios.get(pointsTransactionUrl + '?customer_id=' + smileIoCustomer.id, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${process.env.SMILEIO_PRIVATE_KEY}`,
          },
        }),
      ]);

    let createdAt = new Date();
    let points_expired_at = null;
    if (pointTransactions && pointTransactions.points_transactions.length) {
      const pointTransaction = pointTransactions.points_transactions.find(
        (e) => e.points_change > 0,
      );
      if (pointTransaction) {
        createdAt = pointTransaction.created_at;
      }
    }

    if (
      pointPrograms &&
      pointPrograms.reward_programs &&
      pointPrograms.reward_programs[0].points_expiry_is_enabled
    ) {
      points_expired_at = dayjs(createdAt)
        .add(
          pointPrograms.reward_programs[0].points_expiry_interval_count,
          'day',
        )
        .toDate();
    }

    smileIoCustomer = { ...smileIoCustomer, points_expired_at };

    return res.status(200).json({
      msg: 'ok',
      code: 200,
      data: smileIoCustomer,
    });
  },
);

/**
 * @swagger
 * /api/v4/smile-io/list-point-product:
 *  get:
 *    security: []
 *    tags:
 *      - Smile
 *    description: Get list point products available for the Smile membership program
 *    parameters:
 *    - in: query
 *      name: isDisable
 *      schema:
 *        type: boolean
 *        allowEmptyValue: true
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: error message
 */

router.get('/api/v4/smile-io/list-point-product', async (req, res) => {
  const db = req.app.db;
  const headers = {
    Authorization: 'Bearer ' + process.env.SMILEIO_PRIVATE_KEY,
  };
  const isDisable = req.query.isDisable ? req.query.isDisable : true;
  const options = {
    url: 'https://api.smile.io/v1/points_products',
    method: 'GET',
    headers: headers,
  };

  function callback(error, response, body) {
    if (!error && response.statusCode === 200) {
      const dataJson = JSON.parse(body);
      if (dataJson.points_products.length > 0) {
        if (isDisable === true) {
          dataJson.points_products = dataJson.points_products.filter(
            (e) => e.reward.value !== null,
          );
        }
        res.status(200).json({
          message: 'ok',
          code: 200,
          data: dataJson.points_products,
        });
      } else {
        res.status(404).json({
          message: 'Not found',
          code: 404,
          data: null,
        });
      }
    } else {
      res.status(404).json({
        message: 'Not found',
        code: 404,
        data: null,
      });
    }
  }

  request(options, callback);
});

/**
 * @swagger
 * /api/v4/smile-io/purchase-point-product:
 *  post:
 *    tags:
 *      - Smile
 *    description: Redeem points from the Smile membership program
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              point_product_id:
 *                type: string
 *              customer_id:
 *                type: string
 *            required:
 *             - point_product_id
 *             - customer_id
 *          example: {point_product_id: '150922', customer_id: '844340112'}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: product not found
 *      '401':
 *        description: Not a validated user.  Need to login first
 */
router.post(
  '/api/v4/smile-io/purchase-point-product/',
  passport.authenticate('jwt', { session: false }),
  customValid('purchasePointProduct'),
  async (req, res) => {
    const db = req.app.db;
    const point_product_id = req.body.point_product_id;
    const points_to_spend = req.body.points_to_spend;
    const customer_id = req.body.customer_id;
    const store_font_key = req.body.store_font_key;
    const data = {
      customer_id: customer_id.toString(),
      // 'points_to_spend': points_to_spend.toString()
    };
    const formData = JSON.stringify(data);
    const contentLength = formData.length;

    request(
      {
        headers: {
          Authorization: 'Bearer ' + process.env.SMILEIO_PRIVATE_KEY,
          'Content-Length': contentLength,
          'Content-Type': 'application/json',
        },
        url:
          'https://api.smile.io/v1/points_products/' +
          point_product_id +
          '/purchase',
        body: formData,
        method: 'POST',
      },
      (err, res1, body) => {
        console.log(err);
        const dataJson = JSON.parse(body);
        if (err || dataJson.error) {
          res.status(400).json({
            message: dataJson.error.message,
            code: 400,
            data: dataJson,
          });
        } else {
          const dataJson = JSON.parse(body);
          res.status(200).json({
            message: 'ok',
            code: 200,
            data: dataJson,
          });
        }
      },
    );
  },
);

/**
 * @swagger
 * /api/v4/smile-io/way-to-earn:
 *  get:
 *    security: []
 *    tags:
 *      - Smile
 *    description: Get list of ways to earn points with the Smile membership program
 *    responses:
 *      '200':
 *        description: A successful response
 */

router.get('/api/v4/smile-io/way-to-earn', async (req, res) => {
  const arrayWayToEarn = [
    {
      name: 'Join the crew!',
      description: '200 Mighty Coins',
      image_url: '/images/way_to_earn/join_the_crew.png',
    },
    {
      name: 'Own some Treasure',
      description: '10 Mighty Coins for every $1 spent',
      image_url: '/images/way_to_earn/own_some_treasure.png',
    },
    {
      name: 'Mighty Like our Facebook Page',
      description: '100 Mighty Coins',
      image_url: '/images/way_to_earn/mighty_like_our_facebook_page.png',
    },
    {
      name: 'Spy on our Instagram',
      description: '100 Mighty Coins',
      image_url: '/images/way_to_earn/spy_on_our_instagram.png',
    },
    {
      name: "Cheers! It's your Birthday",
      description: '500 Mighty Coins',
      image_url: '/images/way_to_earn/cheers_its_your_birthday.png',
    },
    {
      name: 'Join the crew!',
      description: '200 Mighty Coins',
      image_url: '/images/way_to_earn/join_the_crew.png',
    },
  ];
  res.status(200).json({
    message: 'ok',
    code: 200,
    data: arrayWayToEarn,
  });
});

module.exports = router;
