const express = require('express');
const passport = require('passport');
const router = express.Router();
const Promise = require('promise');

/**
 * @swagger
 * /api/v4/list-udid_requests/:
 *  get:
 *    tags:
 *      - File-management
 *    description: Get list-udid_requests
 *    parameters:
 *    - in: query
 *      name: page
 *      schema:
 *        type: integer
 *        example: 10
 *    - in: query
 *      name: email
 *      schema:
 *        type: string
 *        example: 'dovu@mightyjaxx.com'
 *    - in: query
 *      name: limit
 *      schema:
 *        type: integer
 *        example: 50
 *    - in: query
 *      name: from
 *      schema:
 *        type: string
 *        example: '10/10/2021'
 *    - in: query
 *      name: to
 *      schema:
 *        type: string
 *        example: '20/10/2021'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '500':
 *        description: Internal Server Error
 */

router.get(
  '/api/v4/list-udid_requests/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const db = req.app.db;
      let page = req.query.page;
      const email = req.query.email;
      const limit = req.query.limit;
      const from = req.query.from;
      const to = req.query.to;
      if (!page || page <= 0) page = 1;
      const skip = (page - 1) * Number(limit);

      const condition = [];
      if (from) {
        condition.push({ createdAt: { $gte: new Date(from + ' 00:00:00') } });
      } else {
        condition.push({
          createdAt: { $gte: new Date('1970-01-01 00:00:00') },
        });
      }
      if (to) {
        condition.push({ createdAt: { $lte: new Date(to + ' 23:59:59') } });
      }
      if (email) {
        condition.push({ emailManufacture: email });
      }

      const [count, files] = await Promise.all([
        db.sendEmailToManufacture.count({ $and: condition }),
        db.sendEmailToManufacture
          .aggregate([
            {
              $match: {
                $and: condition,
              },
            },
          ])
          .sort({ createdAt: -1 })
          .skip(skip)
          .limit(Number(limit))
          .toArray(),
      ]);

      return res.status(200).json({
        code: 200,
        data: {
          totalCount: count,
          files: files,
        },
      });
    } catch (error) {
      return res.status(500).json({ code: 500, message: error.message });
    }
  },
);

module.exports = router;
