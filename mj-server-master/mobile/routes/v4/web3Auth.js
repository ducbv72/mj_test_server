const express = require('express');
const passport = require('passport');

const getMessageToSign = require('../../controllers/web3Auth/getMessageToSign');
const verifySignature = require('../../controllers/web3Auth/verifySignature');
const disconnetWallet = require('../../controllers/web3Auth/disconnectWallet');
const getWalletStatus = require('../../controllers/web3Auth/getWalletStatus');

const router = express.Router();
const baseUrl = '/api/v4/web3-auth';

router.get(
  `${baseUrl}/wallet`,
  passport.authenticate('jwt', { session: false }),
  getWalletStatus,
);

router.get(
  `${baseUrl}/message`,
  passport.authenticate('jwt', { session: false }),
  getMessageToSign,
);

router.post(
  `${baseUrl}/verify-sig`,
  passport.authenticate('jwt', { session: false }),
  verifySignature,
);

router.put(
  `${baseUrl}/disconnet`,
  passport.authenticate('jwt', { session: false }),
  disconnetWallet,
);

module.exports = router;
