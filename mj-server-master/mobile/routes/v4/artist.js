const express = require('express');
const router = express.Router();
const Promise = require('promise');

/**
 * @swagger
 * /api/v4/artists:
 *  get:
 *    security: []
 *    tags:
 *      - Artist
 *    description: Get list of artists
 *    responses:
 *      '200':
 *        description: A successful response
 */

router.get('/api/v4/artists', async (req, res) => {
  const db = req.app.db;
  const [artists] = await Promise.all([
    db.artists.find({}, { artistName: 1, artistImage: 1 }).toArray(),
  ]);
  res.status(200).json({
    message: 'ok',
    data: artists,
  });
});

/**
 * @swagger
 * /api/v4/artist/{id}:
 *  get:
 *    security: []
 *    tags:
 *      - Artist
 *    description: Get details of artists by id
 *    parameters:
 *    - in: path
 *      name: id
 *      required: true
 *      schema:
 *        type: string
 *      example: '7178616881'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '404':
 *        description: No artist matching that id for that artist is wrong
 */

router.get('/api/v4/artist/:id', async (req, res) => {
  const db = req.app.db;
  const [artist] = await Promise.all([
    db.artists.findOne({ articleId: req.params.id }),
  ]);

  if (!artist) {
    return res.status(404).json({ code: 404, message: 'Cannot find artist' });
  }

  res.status(200).json({
    code: 200,
    message: 'ok',
    data: artist,
  });
});

module.exports = router;
