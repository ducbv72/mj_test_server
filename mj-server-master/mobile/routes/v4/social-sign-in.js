const express = require('express');
const router = express.Router();

const getAccessTokenFromGoogleToken = require('../../controllers/getAccessTokenFromGoogleToken');
const getAccessTokenFromAppleToken = require('../../controllers/getAccessTokenFromAppleToken');
const getAccessTokenFromFacebookToken = require('../../controllers/getAccessTokenFromFacebookToken');
const getMultipassUrlFromFacebookCode = require('../../controllers/getMultipassUrlFromFacebookCode');
const acceptTermsAndConditions = require('../../controllers/acceptTermsAndConditions');

// Social login

/**
 * @swagger
 * /api/v4/google/access-token:
 *  get:
 *    security: []
 *    tags:
 *      - Social Sign in
 *    description: Get google access-token
 *    parameters:
 *    - in: query
 *      name: id_token
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    - in: query
 *      name: full_name
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    - in: query
 *      name: user_id
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: invalid_parameters
 *      '500':
 *        description: Internal Server Error
 */

router.get('/api/v4/google/access-token', getAccessTokenFromGoogleToken);

/**
 * @swagger
 * /api/v4/apple/access-token:
 *  get:
 *    security: []
 *    tags:
 *      - Social Sign in
 *    description: Get apple access-token
 *    parameters:
 *    - in: query
 *      name: identity_token
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    - in: query
 *      name: full_name
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: invalid_parameters
 *      '500':
 *        description: Internal Server Error
 */

router.get('/api/v4/apple/access-token', getAccessTokenFromAppleToken);

/**
 * @swagger
 * /api/v4/facebook/access-token:
 *  get:
 *    security: []
 *    tags:
 *      - Social Sign in
 *    description: Get facebook access-token
 *    parameters:
 *    - in: query
 *      name: fbToken
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    - in: query
 *      name: emailFromQuery
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: invalid_parameters
 *      '500':
 *        description: Internal Server Error
 */

router.get('/api/v4/facebook/access-token', getAccessTokenFromFacebookToken);

/**
 * @swagger
 * /api/v4/facebook/multipass:
 *  get:
 *    security: []
 *    tags:
 *      - Social Sign in
 *    description: Get facebook multipass
 *    parameters:
 *    - in: query
 *      name: code
 *      required: true
 *      description: facebook token
 *      schema:
 *        type: string
 *      example: ''
 *    - in: query
 *      name: redirect_to_path
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: invalid_parameters
 *      '500':
 *        description: Internal Server Error
 */

router.get('/api/v4/facebook/multipass', getMultipassUrlFromFacebookCode);

/**
 * @swagger
 * /api/v4/terms-and-conditions:
 *  get:
 *    security: []
 *    tags:
 *      - Social Sign in
 *    description: Get terms and conditions
 *    parameters:
 *    - in: query
 *      name: termsToken
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    - in: query
 *      name: customerId
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    - in: query
 *      name: email
 *      required: true
 *      schema:
 *        type: string
 *      example: ''
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: invalid_parameters
 *      '500':
 *        description: Internal Server Error
 */
router.get('/api/v4/terms-and-conditions', acceptTermsAndConditions);

module.exports = router;
