const express = require('express');
const router = express.Router();
const { shopify } = require('../../../config/shopify');
const moment = require('moment');
const { storefrontInstance } = require('../../../services/shopify');

async function getProducts(products, query, handle, nextPage, after) {
  const data = await shopify.graphql(query(handle, after));
  products.collectionByHandle.products.edges.push(
    ...data.collectionByHandle.products.edges,
  );
  if (data.collectionByHandle.products.pageInfo.hasNextPage) {
    let cursor =
      data.collectionByHandle.products.edges[
        data.collectionByHandle.products.edges.length - 1
      ].cursor;
    cursor = `"${cursor}"`;
    await getProducts(
        products,
        query,
        handle,
        data.collectionByHandle.products.pageInfo.hasNextPage,
        cursor.toString(),
    );
  }
  return products;
}

async function getProductsV2(products, query, handle, nextPage, after, res) {
  // const data = await shopify.graphql(query(handle, after));
  const data = await storefrontInstance.post(null, {query: query(handle, after)});
  const errors = data.data.errors;
  if (errors && errors.length){
    return res.status(500).json({ code: 500, msg: 'Invalid' });
  }
  const dataCollection = data.data.data
  products.collectionByHandle.products.edges.push(
    ...dataCollection.collectionByHandle.products.edges,
  );
  if (dataCollection.collectionByHandle.products.pageInfo.hasNextPage) {
    let cursor =
    dataCollection.collectionByHandle.products.edges[
      dataCollection.collectionByHandle.products.edges.length - 1
      ].cursor;
    cursor = `"${cursor}"`;
    await getProducts(
      products,
      query,
      handle,
      dataCollection.collectionByHandle.products.pageInfo.hasNextPage,
      cursor.toString(),
      res
    );
  }
  return products;
}

/**
 * @swagger
 * /api/v4/stores:
 *  get:
 *    security: []
 *    tags:
 *      - Stores
 *    description: Get list of stores
 *    responses:
 *      '200':
 *        description: A successful response
 *      '500':
 *        description: Internal Error Server
 */

router.get('/api/v4/stores', async (req, res) => {
  try {
    const db = req.app.db;
    const [storeList] = await Promise.all([db.stores.find({}).toArray()]);

    return res.status(200).json({
      code: 200,
      msg: 'ok',
      data: storeList,
    });
  } catch (error) {
    return res.status(500).json({ code: 500, msg: 'Internal Error Server' });
  }
});

/**
 * @swagger
 * /api/v4/stores/:handle/products:
 *  get:
 *    security: []
 *    tags:
 *      - Stores
 *    description: Get list of product by handle
 *    parameters:
 *    - in: path
 *      name: handle
 *      required: true
 *      schema:
 *        type: string
 *      example: 'all'
 *    - in: query
 *      name: after
 *      required: true
 *      description: The last id of item in view page
 *      schema:
 *        type: string
 *      example: ''
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Store not found
 *      '500':
 *        description: Internal Error Server
 */
router.get('/api/v4/stores/:handle/products', async (req, res) => {
  try {
    const db = req.app.db;
    const { handle } = req.params;
    const after = req.query.after ? `"${req.query.after}"` : null;
    const query = function (handle, after) {
      return `{
        collectionByHandle(handle: "${handle}") {
          products(first: 20, after: ${after}, sortKey: MANUAL, reverse:false ) {
            edges {
              cursor
              node {
                createdAt
                description
                descriptionHtml
                handle
                vendor
                id
                title
                tags
                onlineStoreUrl
                images(first: 10) {
                  edges {
                    node {
                      originalSrc
                      transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
                    }
                  }
                }
                productType
                options {
                  id
                  name
                  values
                }
                priceRange {
                  maxVariantPrice {
                    amount
                    currencyCode
                  }
                  minVariantPrice {
                    amount
                    currencyCode
                  }
                }
                variants(first: 10) {
                  edges {
                    node {
                      compareAtPrice
                      id
                      image {
                        originalSrc
                        transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
                      }
                      price
                      sku
                      selectedOptions {
                        name
                        value
                      }
                      title
                      weight
                      weightUnit
                    }
                  }
                }
              }
            }
            pageInfo {
              hasNextPage
            }
          }
        }
      }
      `;
    };
    let collectionByHandle;
    if (handle === 'mighty-allstars-f1') {
      const result = { collectionByHandle: { products: { edges: [] } } };
      const data = await getProducts(result, query, handle, null, null, res);
      collectionByHandle = data.collectionByHandle;
    } else {
      // const data = await shopify.graphql(query(handle, after));
      const data = await storefrontInstance.post(null, {
        query: query(handle, after),
      });
      const errors = data.data.errors;
      if (errors && errors.length) {
        return res.status(500).json({ code: 500, msg: 'Invalid' });
      }
      const dataCollection = data.data.data;
      collectionByHandle = dataCollection.collectionByHandle;
    }

    if (!collectionByHandle) {
      return res.json({ data: [] });
    }

    const { products } = collectionByHandle;

    const encodeBase64 = (string) => {
      const buffer = Buffer.from(String(string));
      return buffer.toString('base64');
    };

    const skus = [];
    let shopifyProducts = products.edges.map((v) => {
      return {
        id: v.node.id.includes('gid://shopify/Product/')
          ? encodeBase64(v.node.id)
          : v.node.id,
        title: v.node.title,
        handle: v.node.handle,
        productType: v.node.productType,
        status: v.node.status,
        artistName: '',
        vendor: v.node.vendor,
        productLaunchTime: '00:00:00',
        productSoldOutTime: '00:00:00',
        titleOnapp: '',
        createdAt: v.node.createdAt,
        description: v.node.description,
        descriptionHtml: v.node.descriptionHtml,
        onlineStoreUrl: v.node.onlineStoreUrl,
        tags: v.node.tags,
        priceRange: v.node.priceRange,
        options: v.node.options,
        variants: v.node.variants.edges.map((el) => {
          skus.push(el.node.sku);

          return {
            id: el.node.id,
            image: el.node.image,
            optionValues: el.node.selectedOptions,
            price: el.node.price,
            title: el.node.title,
            compareAtPrice: el.node.compareAtPrice,
            weight: el.node.weight,
            weightUnit: el.node.weightUnit,
            sku: el.node.sku,
            productHandle: v.node.handle,
            productId: v.node.id,
            outOfStock: false,
          };
        }),
        images: v.node.images.edges.map((el) => ({
          src: el.node.originalSrc,
          transformedSrc: el.node.transformedSrc,
        })),
        availableForSale: true,
        cursor: v.cursor,
      };
    });

    let [dbProducts] = await Promise.all([
      db.products
        .find({
          productVariantSku: { $in: skus },
          productTags: { $not: /.*exclusive-nyansum.*/i },
        })
        .sort({ productOrder: 1 })
        .toArray(),
    ]);

    dbProducts = dbProducts.map((v) => {
      return {
        id: encodeBase64(`gid://shopify/Product/` + v.shopifyProductId),
        title: v.productTitle,
        isFeature: v.productFeature,
        isNewest: v.productNew,
        isTrending: v.productTrending,
        isRecommend: v.productRecommendation,
        backgroundImageUrl: v.productBackgroundImageS3,
        productLaunchColor: v.productLaunchColor,
        productLaunchDate: v.productLaunchDate,
        productLaunchTime: v.productLaunchTime
          ? v.productLaunchTime
          : '00:00:00',
        productLaunchStatus: v.productLaunchStatus == 'true',
        productOrder: Number.isInteger(parseInt(v.productOrder))
          ? parseInt(v.productOrder)
          : null,
        productLaunchVipStatus: v.productLaunchVipStatus == 'true',
        productSoldOutDate: v.productSoldOutDate,
        productSoldOutTime: v.productSoldOutTime
          ? v.productSoldOutTime
          : '00:00:00',
        productSoldOutStatus: v.productSoldOutStatus == 'true',
        artistName: v.productArtist,
        titleOnapp: v.productTitleOnApp,
        sku: v.productVariantSku,
        currentDateUTC: moment
          .tz('Asia/Singapore')
          .format('YYYY-MM-DD HH:mm:ss'),
      };
    });

    shopifyProducts.forEach((v) => {
      const findProduct = dbProducts.find((el) => el.id === v.id);
      let minPrice = 999999;
      let foundSmallerPrice = false;
      v.variants.forEach((variant) => {
        const price = parseFloat(variant.price);
        if (minPrice > price) {
          minPrice = price;
          foundSmallerPrice = true;
        }
      });

      if (foundSmallerPrice) {
        v.lowestPrice = minPrice;
      }

      v.imageSources = v.images.map((image) =>
        image.transformedSrc ? image.transformedSrc : image.src,
      );

      if (v.imageSources.length > 0) {
        v.thumbnailSrc = v.imageSources[0];
      }

      if (findProduct) {
        v.productLaunchColor = findProduct.productLaunchColor;
        v.productLaunchDate = findProduct.productLaunchDate;
        v.productLaunchStatus = findProduct.productLaunchStatus;
        v.productLaunchTime = findProduct.productLaunchTime;
        v.productLaunchVipStatus = findProduct.productLaunchVipStatus;
        v.productSoldOutDate = findProduct.productSoldOutDate;
        v.productSoldOutStatus = findProduct.productSoldOutStatus;
        v.productSoldOutTime = findProduct.productSoldOutTime;
        v.allowVIPEarlyAccess = findProduct.productLaunchVipStatus;
        v.productOrder = findProduct.productOrder;
        v.backgroundImageUrl = findProduct.backgroundImageUrl;
        v.titleOnapp = findProduct.titleOnapp;
        v.timeGettingEvent = moment.utc().format('YYYY-MM-DD HH:mm:ss');

        if (findProduct.productLaunchDate) {
          v.launchDateColor = findProduct.productLaunchColor;
        }

        v.currentDateUTC = moment
          .utc(findProduct.currentDateUTC)
          .format('YYYY-MM-DD HH:mm:ss');

        if (!v.artistName && findProduct.artistName) {
          v.artistName = findProduct.artistName;
          v.title = findProduct.title;
        }

        if (findProduct.productLaunchStatus && findProduct.productLaunchDate) {
          v.availableForSale = false;
          v.productLaunchTime;
          v.launchDate = moment(
            findProduct.productLaunchDate + ' ' + findProduct.productLaunchTime,
            'YYYY-MM-DD HH:mm:ss',
          )
            .tz('America/New_York')
            .toISOString();
          v.soldOutDate = undefined;
        }

        if (
          findProduct.productSoldOutStatus &&
          findProduct.productSoldOutDate
        ) {
          v.soldOutDate = moment(
            findProduct.productSoldOutDate +
              ' ' +
              findProduct.productSoldOutTime,
            'YYYY-MM-DD HH:mm:ss',
          )
            .tz('Asia/Singapore')
            .toISOString();
          v.launchDate = undefined;
        }

        if (
          v.launchDate &&
          moment(v.launchDate).isAfter(moment(v.currentDateUTC))
        ) {
          v.soldOutDate = undefined;
        } else {
          v.launchDate = undefined;
        }
      }
    });
    shopifyProducts = shopifyProducts.filter((e) => e.status !== 'DRAFT');
    return res.status(200).json({ code: 200, data: shopifyProducts });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ code: 500, msg: 'Internal Error Server' });
  }
});

/**
 * @swagger
 * /api/v4/stores/searchProducts:
 *  get:
 *    security: []
 *    tags:
 *      - Stores
 *    description: Search a product in stores
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              after:
 *                type: string
 *              sortBy:
 *                type: string
 *              reverse:
 *                type: boolean
 *              query:
 *                type: string
 *            required:
 *             - after
 *             - sortBy
 *             - reverse
 *             - query
 *          example: {after: null, sortBy: 'TITLE',reverse: false,query: 'tag:Mighty AllStars'}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Store not found
 *      '500':
 *        description: Internal Error Server
 */
router.post('/api/v4/stores/searchProducts', async (req, res) => {
  try {
    const db = req.app.db;
    const { after, sortBy, reverse, query } = req.body;
    const toGraphQueryString = () => {
      return `first: 20, after: ${
        after ? `"${after}"` : null
      }, sortKey: ${sortBy}, 
      reverse:${reverse} ${query ? `, query: "${query}"` : ''}`;
    };

    const queryString = `{
      products(${toGraphQueryString()}){
        edges{
          cursor
          node{
            createdAt
            description
            descriptionHtml
            handle
            id
            title      
            tags     
            onlineStoreUrl           
            images(first: 100){
              edges{
                node{
                  originalSrc
                  transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
                }
              }
            }
            productType
            options{
              id
              name
              values
            }
            priceRange{
              maxVariantPrice{
                amount
                currencyCode
              }
              minVariantPrice{
                amount
                currencyCode
              }
            }
            variants(first: 100){
              edges{
                node{
                  compareAtPrice
                  id
                  image{
                    originalSrc
                    transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
                  }
                  price
                  sku
                  selectedOptions{
                    name
                    value
                  }
                  title
                  weight
                  weightUnit
                }
              }
            }
          }
        }
        pageInfo{
          hasNextPage
        }
      }
      }
      `;

    const collectionByHandle = await storefrontInstance.post(null, {
      query: queryString,
    });

    if (!collectionByHandle.data) {
      return res.json({ data: [] });
    }

    const { data } = collectionByHandle;
    const products = data.data.products;

    const encodeBase64 = (string) => {
      const buffer = Buffer.from(String(string));
      return buffer.toString('base64');
    };

    const skus = [];

    const shopifyProducts = products.edges.map((v) => {
      return {
        id: v.node.id,
        title: v.node.title,
        handle: v.node.handle,
        productType: v.node.productType,
        artistName: '',
        vendor: v.node.vendor,
        productLaunchTime: '00:00:00',
        productSoldOutTime: '00:00:00',
        titleOnapp: '',
        createdAt: v.node.createdAt,
        description: v.node.description,
        descriptionHtml: v.node.descriptionHtml,
        onlineStoreUrl: v.node.onlineStoreUrl,
        tags: v.node.tags,
        priceRange: v.node.priceRange,
        options: v.node.options,
        variants: v.node.variants.edges.map((el) => {
          skus.push(el.node.sku);

          return {
            id: encodeBase64(el.node.id),
            image: el.node.image,
            optionValues: el.node.selectedOptions,
            price: el.node.price,
            title: el.node.title,
            compareAtPrice: el.node.compareAtPrice,
            weight: el.node.weight,
            weightUnit: el.node.weightUnit,
            sku: el.node.sku,
            productHandle: v.node.handle,
            productId: encodeBase64(v.node.id),
            outOfStock: false,
          };
        }),
        images: v.node.images.edges.map((el) => ({
          src: el.node.originalSrc,
          transformedSrc: el.node.transformedSrc,
        })),
        availableForSale: true,
        cursor: v.cursor,
      };
    });

    let [dbProducts] = await Promise.all([
      db.products
        .find({
          productVariantSku: { $in: skus },
          productTags: { $not: /.*exclusive-mobile-nyansum.*/i },
        })
        .sort({ productOrder: 1 })
        .toArray(),
    ]);

    dbProducts = dbProducts.map((v) => {
      return {
        id: encodeBase64(`gid://shopify/Product/` + v.shopifyProductId),
        title: v.productTitle,
        isFeature: v.productFeature,
        isNewest: v.productNew,
        isTrending: v.productTrending,
        isRecommend: v.productRecommendation,
        backgroundImageUrl: v.productBackgroundImageS3,
        productLaunchColor: v.productLaunchColor,
        productLaunchDate: v.productLaunchDate,
        productLaunchTime: v.productLaunchTime
          ? v.productLaunchTime
          : '00:00:00',
        productLaunchStatus: v.productLaunchStatus == 'true',
        productLaunchVipStatus: v.productLaunchVipStatus == 'true',
        productSoldOutDate: v.productSoldOutDate,
        productSoldOutTime: v.productSoldOutTime
          ? v.productSoldOutTime
          : '00:00:00',
        productSoldOutStatus: v.productSoldOutStatus == 'true',
        artistName: v.productArtist,
        titleOnapp: v.productTitleOnApp,
        sku: v.productVariantSku,
        currentDateUTC: moment
          .tz('Asia/Singapore')
          .format('YYYY-MM-DD HH:mm:ss'),
      };
    });

    shopifyProducts.forEach((v) => {
      const findProduct = dbProducts.find((el) => el.id === v.id);

      let minPrice = 999999;
      let foundSmallerPrice = false;
      v.variants.forEach((variant) => {
        const price = parseFloat(variant.price);
        if (minPrice > price) {
          minPrice = price;
          foundSmallerPrice = true;
        }
      });

      if (foundSmallerPrice) {
        v.lowestPrice = minPrice;
      }

      v.imageSources = v.images.map((image) =>
        image.transformedSrc ? image.transformedSrc : image.src,
      );

      if (v.imageSources.length > 0) {
        v.thumbnailSrc = v.imageSources[0];
      }

      if (findProduct) {
        v.productLaunchColor = findProduct.productLaunchColor;
        v.productLaunchDate = findProduct.productLaunchDate;
        v.productLaunchStatus = findProduct.productLaunchStatus;
        v.productLaunchTime = findProduct.productLaunchTime;
        v.productLaunchVipStatus = findProduct.productLaunchVipStatus;
        v.productSoldOutDate = findProduct.productSoldOutDate;
        v.productSoldOutStatus = findProduct.productSoldOutStatus;
        v.productSoldOutTime = findProduct.productSoldOutTime;
        v.allowVIPEarlyAccess = findProduct.productLaunchVipStatus;
        v.backgroundImageUrl = findProduct.backgroundImageUrl;
        v.titleOnapp = findProduct.titleOnapp;
        v.timeGettingEvent = moment.utc().format('YYYY-MM-DD HH:mm:ss');

        if (findProduct.productLaunchDate) {
          v.launchDateColor = findProduct.productLaunchColor;
        }

        if (findProduct.currentDateUTC) {
          v.currentDateUTC = moment
            .utc(findProduct.currentDateUTC)
            .format('YYYY-MM-DD HH:mm:ss');
        }

        if (!v.artistName && findProduct.artistName) {
          v.artistName = findProduct.artistName
            ? findProduct.artistName
            : v.title;
          v.title = findProduct.title;
        }

        if (findProduct.productLaunchStatus && findProduct.productLaunchDate) {
          v.availableForSale = false;
          v.productLaunchTime;
          v.launchDate = moment(
            findProduct.productLaunchDate + ' ' + findProduct.productLaunchTime,
            'YYYY-MM-DD HH:mm:ss',
          )
            .tz('America/New_York')
            .toISOString();
          v.soldOutDate = undefined;
        }

        if (
          findProduct.productSoldOutStatus &&
          findProduct.productSoldOutDate
        ) {
          v.soldOutDate = moment(
            findProduct.productSoldOutDate +
              ' ' +
              findProduct.productSoldOutTime,
            'YYYY-MM-DD HH:mm:ss',
          )
            .tz('Asia/Singapore')
            .toISOString();
          v.launchDate = undefined;
        }

        if (v.currentDateUTC) {
          if (
            v.launchDate &&
            moment(v.launchDate).isAfter(moment(v.currentDateUTC))
          ) {
            v.soldOutDate = undefined;
          } else {
            v.launchDate = undefined;
          }
        }
      }
    });

    return res.status(200).json({ code: 200, data: shopifyProducts });
  } catch (error) {
    console.log(error);
    return res.status(400).json({ code: 400, msg: error.message });
  }
});

router.post('/api/v4/stores/mas', async (req, res) => {
  try {
    const db = req.app.db;
    const { handle, collection, edition } = req.body;
    const after = req.query.after ? `"${req.query.after}"` : null;
    const query = function (handle, after) {
      return `{
        collectionByHandle(handle: "${handle}") {
          products(first: 20, after: ${after}, sortKey: MANUAL, reverse:false ) {
            edges {
              cursor
              node {
                createdAt
                description
                descriptionHtml
                handle
                vendor
                id
                title
                tags
                onlineStoreUrl
                images(first: 10) {
                  edges {
                    node {
                      originalSrc
                      transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
                    }
                  }
                }
                productType
                options {
                  id
                  name
                  values
                }
                priceRange {
                  maxVariantPrice {
                    amount
                    currencyCode
                  }
                  minVariantPrice {
                    amount
                    currencyCode
                  }
                }
                variants(first: 10) {
                  edges {
                    node {
                      compareAtPrice
                      id
                      image {
                        originalSrc
                        transformedSrc(maxHeight: 512, maxWidth: 512, preferredContentType: PNG)
                      }
                      price
                      sku
                      selectedOptions {
                        name
                        value
                      }
                      title
                      weight
                      weightUnit
                    }
                  }
                }
              }
            }
            pageInfo {
              hasNextPage
            }
          }
        }
      }
      `;
    };
    let collectionByHandle;
    const result = { collectionByHandle: { products: { edges: [] } } };
    const data = await getProductsV2(result, query, handle, null, null, res);
    collectionByHandle = data.collectionByHandle;
    if (!collectionByHandle) {
      return res.json({ data: [] });
    }

    const { products } = collectionByHandle;

    const encodeBase64 = (string) => {
      const buffer = Buffer.from(String(string));
      return buffer.toString('base64');
    };

    const skus = [];

    let shopifyProducts = products.edges.map((v) => {
      return {
        id: v.node.id.includes('gid://shopify/Product/')
          ? encodeBase64(v.node.id)
          : v.node.id,
        title: v.node.title,
        handle: v.node.handle,
        productType: v.node.productType,
        status: v.node.status,
        artistName: '',
        vendor: v.node.vendor,
        productLaunchTime: '00:00:00',
        productSoldOutTime: '00:00:00',
        titleOnapp: '',
        createdAt: v.node.createdAt,
        description: v.node.description,
        descriptionHtml: v.node.descriptionHtml,
        onlineStoreUrl: v.node.onlineStoreUrl,
        tags: v.node.tags,
        priceRange: v.node.priceRange,
        options: v.node.options,
        variants: v.node.variants.edges.map((el) => {
          skus.push(el.node.sku);

          return {
            id: el.node.id,
            image: el.node.image,
            optionValues: el.node.selectedOptions,
            price: el.node.price,
            title: el.node.title,
            compareAtPrice: el.node.compareAtPrice,
            weight: el.node.weight,
            weightUnit: el.node.weightUnit,
            sku: el.node.sku,
            productHandle: v.node.handle,
            productId: v.node.id,
            outOfStock: false,
          };
        }),
        images: v.node.images.edges.map((el) => ({
          src: el.node.originalSrc,
          transformedSrc: el.node.transformedSrc,
        })),
        availableForSale: true,
        cursor: v.cursor,
      };
    });

    console.log('PRODUCT', products.edges);
    // Check product by tag
    // Get product by tag
    if (collection) {
      shopifyProducts = shopifyProducts.filter((p) =>
        p.tags.includes(collection),
      );
    }
    if (edition) {
      shopifyProducts = shopifyProducts.filter((p) => p.tags.includes(edition));
    }
    //

    let [dbProducts] = await Promise.all([
      db.products
        .find({
          productVariantSku: { $in: skus },
          productTags: { $not: /.*exclusive-nyansum.*/i },
        })
        .sort({ productOrder: 1 })
        .toArray(),
    ]);
    // console.log('PRODUCT===>',dbProducts)

    dbProducts = dbProducts.map((v) => {
      // let backgroundImageUrl = '';

      // if (v.productBackgroundImageS3) {
      //   backgroundImageUrl = v.productBackgroundImageS3;
      // } else if (v.productBackgroundImage) {
      //   backgroundImageUrl = v.productBackgroundImage;
      // }

      return {
        id: encodeBase64(`gid://shopify/Product/` + v.shopifyProductId),
        title: v.productTitle,
        isFeature: v.productFeature,
        isNewest: v.productNew,
        isTrending: v.productTrending,
        isRecommend: v.productRecommendation,
        backgroundImageUrl: v.productBackgroundImageS3,
        productLaunchColor: v.productLaunchColor,
        productLaunchDate: v.productLaunchDate,
        productLaunchTime: v.productLaunchTime
          ? v.productLaunchTime
          : '00:00:00',
        productLaunchStatus: v.productLaunchStatus == 'true',
        productOrder: Number.isInteger(parseInt(v.productOrder))
          ? parseInt(v.productOrder)
          : null,
        productLaunchVipStatus: v.productLaunchVipStatus == 'true',
        productSoldOutDate: v.productSoldOutDate,
        productSoldOutTime: v.productSoldOutTime
          ? v.productSoldOutTime
          : '00:00:00',
        productSoldOutStatus: v.productSoldOutStatus == 'true',
        artistName: v.productArtist,
        titleOnapp: v.productTitleOnApp,
        sku: v.productVariantSku,
        currentDateUTC: moment
          .tz('Asia/Singapore')
          .format('YYYY-MM-DD HH:mm:ss'),
      };
    });

    shopifyProducts.forEach((v) => {
      const findProduct = dbProducts.find((el) => el.id === v.id);
      let minPrice = 999999;
      let foundSmallerPrice = false;
      v.variants.forEach((variant) => {
        const price = parseFloat(variant.price);
        if (minPrice > price) {
          minPrice = price;
          foundSmallerPrice = true;
        }
      });

      if (foundSmallerPrice) {
        v.lowestPrice = minPrice;
      }

      if (v.images) {
        v.imageSources = v.images.map((image) =>
          image.transformedSrc ? image.transformedSrc : image.src,
        );

        if (v.imageSources.length > 0) {
          v.thumbnailSrc = v.imageSources[0];
        }
      }

      if (findProduct) {
        v.productLaunchColor = findProduct.productLaunchColor;
        v.productLaunchDate = findProduct.productLaunchDate;
        v.productLaunchStatus = findProduct.productLaunchStatus;
        v.productLaunchTime = findProduct.productLaunchTime;
        v.productLaunchVipStatus = findProduct.productLaunchVipStatus;
        v.productSoldOutDate = findProduct.productSoldOutDate;
        v.productSoldOutStatus = findProduct.productSoldOutStatus;
        v.productSoldOutTime = findProduct.productSoldOutTime;
        v.allowVIPEarlyAccess = findProduct.productLaunchVipStatus;
        v.productOrder = findProduct.productOrder;
        v.backgroundImageUrl = findProduct.backgroundImageUrl;
        v.titleOnapp = findProduct.titleOnapp;
        v.timeGettingEvent = moment.utc().format('YYYY-MM-DD HH:mm:ss');

        if (findProduct.productLaunchDate) {
          v.launchDateColor = findProduct.productLaunchColor;
        }

        if (findProduct.currentDateUTC) {
          v.currentDateUTC = moment
            .utc(findProduct.currentDateUTC)
            .format('YYYY-MM-DD HH:mm:ss');
        }

        if (!v.artistName && findProduct.artistName) {
          v.artistName = findProduct.artistName
            ? findProduct.artistName
            : v.title;
          v.title = findProduct.title;
        }

        if (findProduct.productLaunchStatus && findProduct.productLaunchDate) {
          v.availableForSale = false;
          v.productLaunchTime;
          v.launchDate =
            !findProduct.productLaunchDate || !findProduct.productLaunchTime
              ? undefined
              : moment(
                  findProduct.productLaunchDate +
                    ' ' +
                    findProduct.productLaunchTime,
                  'YYYY-MM-DD HH:mm:ss',
                )
                  .tz('Asia/Singapore')
                  .toISOString();
          v.soldOutDate = undefined;
        }

        if (
          findProduct.productSoldOutStatus &&
          findProduct.productSoldOutDate
        ) {
          v.soldOutDate =
            !findProduct.productSoldOutDate || !findProduct.productSoldOutTime
              ? undefined
              : moment(
                  findProduct.productSoldOutDate +
                    ' ' +
                    findProduct.productSoldOutTime,
                  'YYYY-MM-DD HH:mm:ss',
                )
                  .tz('Asia/Singapore')
                  .toISOString();
          v.launchDate = undefined;
        }

        if (v.currentDateUTC) {
          if (
            v.launchDate &&
            moment(v.launchDate).isAfter(moment(v.currentDateUTC))
          ) {
            v.soldOutDate = undefined;
          } else {
            v.launchDate = undefined;
          }
        }
      }
    });
    shopifyProducts = shopifyProducts.filter((e) => e.status !== 'DRAFT');
    return res.status(200).json({ code: 200, data: shopifyProducts });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ code: 500, msg: 'Internal Error Server' });
  }
});

router.get('/api/v4/stores/edition/:handle', async (req, res) => {
  try {
    const { handle } = req.params;
    const query = function (handle) {
      return `{
          collectionByHandle(handle: "${handle}") {
            ruleSet {
              rules {
                column
                condition
                relation
              }
            }
          }
      }
      `;
    };

    const data = await shopify.graphql(query(handle));
    const conditions = data.collectionByHandle.ruleSet.rules;
    let listEditions = [];
    let listSeries = [];
    let countEdition = 1;
    let countSeries = 1;
    console.log('TAG', conditions);
    conditions.map((c) => {
      if (c.condition.includes('Edition_')) {
        const id = countEdition;
        const tag = c.condition;
        const title = c.condition.replace('Edition_', '');
        listEditions.push({ id, tag, title });
        countEdition += 1;
      }
      if (c.condition.includes('Series_')) {
        const id = countSeries;
        const tag = c.condition;
        let title = c.condition.replace('Series_', '');
        if (c.condition.includes('F1')) {
          title = title.replace('F1','Formula 1®');
        }
        listSeries.push({ id, tag, title });
        countSeries += 1;
      }
    });

    return res
      .status(200)
      .json({ code: 200, data: { listEditions, listSeries } });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ code: 500, msg: 'Internal Error Server' });
  }
});

module.exports = router;
