const express = require('express');
const passport = require('passport');
const router = express.Router();
const Promise = require('promise');

/**
 * @swagger
 * /api/v4/campaignsByCustomer:
 *  get:
 *    tags:
 *      - Campaigns
 *    description: Get list campaigns of customer
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

router.get(
  '/api/v4/campaignsByCustomer',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const user = req.user;
    const [customer] = await Promise.all([
      db.customers.findOne({ customerId: user.customerId }),
    ]);

    if (customer) {
      const [campaigns, scanningcount, lastScanning] = await Promise.all([
        db.campaigns
          .find(
            {
              $and: [
                { end_date: { $gte: new Date() } },
                { start_date: { $lte: new Date() } },
                { isDisable: { $ne: 'true' } },
              ],
            },
            {
              isDisable: 1,
              name: 1,
              description: 1,
              url: 1,
              max_number_of_scans: 1,
              start_date: 1,
              end_date: 1,
              id: 1,
              _id: 0,
            },
          )
          .toArray(),
        db.campaignParticipates
          .aggregate([
            {
              $match: {
                $and: [{ customerId: customer.customerId }],
              },
            },
            { $group: { _id: '$campaign_id', count: { $sum: '$scanCount' } } },
          ])
          .toArray(),
        db.campaignParticipates
          .find({ customerId: customer.customerId })
          .sort({ createdAt: -1 })
          .limit(1)
          .toArray(),
      ]);

      campaigns.forEach(async (item) => {
        item.scan_count = 0;
        scanningcount.forEach(async (item1) => {
          if (item1._id === item.id) {
            item.scan_count = item1.count;
          }
        });
        if (lastScanning.length > 0) {
          if (item.id === lastScanning[0].campaign_id) {
            item.last_scan = lastScanning[0].scan_date;
          } else {
            item.last_scan = null;
          }
        } else {
          item.last_scan = null;
        }
      });

      res.status(200).json({ campaigns: campaigns });
    } else {
      const [campaigns] = await Promise.all([
        db.campaigns
          .find(
            {
              $and: [
                { end_date: { $gte: new Date() } },
                { start_date: { $lte: new Date() } },
                { isDisable: { $ne: 'true' } },
              ],
            },
            {
              isDisable: 1,
              name: 1,
              description: 1,
              url: 1,
              max_number_of_scans: 1,
              start_date: 1,
              end_date: 1,
              id: 1,
              _id: 0,
            },
          )
          .toArray(),
      ]);

      res.status(200).json({ campaigns: campaigns });
    }
  },
);

/**
 * @swagger
 * /api/v4/campaigns:
 *  get:
 *    security: []
 *    tags:
 *      - Campaigns
 *    description: Get list of campaigns
 *    responses:
 *      '200':
 *        description: A successful response
 */

router.get('/api/v4/campaigns', async (req, res) => {
  const db = req.app.db;
  const [campaigns] = await Promise.all([
    db.campaigns
      .find(
        {
          $and: [
            { end_date: { $gte: new Date() } },
            { start_date: { $lte: new Date() } },
            { isDisable: { $ne: 'true' } },
          ],
        },
        {
          projection: {
            isDisable: 1,
            name: 1,
            description: 1,
            url: 1,
            max_number_of_scans: 1,
            start_date: 1,
            end_date: 1,
            id: 1,
          },
        },
      )
      .toArray(),
  ]);

  res.status(200).json({
    campaigns: campaigns,
  });
});

/**
 * @swagger
 * /api/v4/campaign/{campaign_id}:
 *  get:
 *    security: []
 *    tags:
 *      - Campaigns
 *    description: Get details of campaign by id
 *    parameters:
 *    - in: path
 *      name: campaign_id
 *      schema:
 *        type: string
 *      example: '1615386409888'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '404':
 *        description: Campaign not found
 */

router.get('/api/v4/campaign/:campaign_id', async (req, res) => {
  const db = req.app.db;

  const [campaign] = await Promise.all([
    db.campaigns.findOne(
      { id: parseInt(req.params.campaign_id) },
      {
        winner: 1,
        name: 1,
        description: 1,
        url: 1,
        max_number_of_scans: 1,
        start_date: 1,
        end_date: 1,
        id: 1,
        _id: 0,
      },
    ),
  ]);

  if (!campaign) {
    return res.status(404).json({ code: 404, message: 'Campaign not found' });
  }

  res.status(200).json({
    campaign: campaign,
  });
});

/**
 * @swagger
 * /api/v4/campaignByCustomer/{campaign_id}:
 *  get:
 *    tags:
 *      - Campaigns
 *    description: Get details of campaign's customer by id
 *    parameters:
 *    - in: path
 *      name: campaign_id
 *      schema:
 *        type: string
 *      example: '1615386409888'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '404':
 *        description: Campaign not found
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

router.get(
  '/api/v4/campaignByCustomer/:campaign_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const user = req.user;

    const [campaign, customer] = await Promise.all([
      db.campaigns.findOne(
        { id: parseInt(req.params.campaign_id) },
        {
          winner: 1,
          name: 1,
          description: 1,
          url: 1,
          max_number_of_scans: 1,
          start_date: 1,
          end_date: 1,
          id: 1,
          _id: 0,
        },
      ),
      db.customers.findOne({ userName: user.userName }),
    ]);
    if (!campaign) {
      return res.status(404).json({ code: 404, message: 'Campaign not found' });
    }

    if (!customer) {
      campaign.scan_count = 0;
      res.status(200).json({
        campaign: campaign,
      });
    } else {
      const [countParticipateCampaign] = await Promise.all([
        db.campaignParticipates.count({
          $and: [
            { customerId: customer.customerId },
            { campaign_id: campaign.id },
          ],
        }),
      ]);
      campaign.scan_count = countParticipateCampaign;
      res.status(200).json({
        campaign: campaign,
      });
    }
  },
);

module.exports = router;
