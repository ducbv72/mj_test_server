const express = require('express');
const router = express.Router();
const requestCountry = require('req-country');
const passport = require('passport');
const Config = require('../../../config');
const common = require('../../../lib/common');
const Promise = require('promise');
const { transform, checkValidData } = require('../../../validator');
const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime');
dayjs.extend(relativeTime);
const { shopify } = require('../../../config/shopify');
const bcrypt = require('bcryptjs');

/**
 * @swagger
 * /api/v4/orders-by-client:
 *  get:
 *    tags:
 *      - User's Account
 *    description: Get details of orders for a specific customer from Shopify
 *    parameters:
 *    - in: query
 *      name: pageSize
 *      schema:
 *        type: number
 *      example: 10
 *    - in: query
 *      name: nextCursor
 *      schema:
 *        type: string
 *      example: 'eyJsYXN0X2lkIjozMDY1NzAxMTM4NTY2LCJsYXN0X3ZhbHVlIjoiMzA2NTcwMTEzODU2NiJ9'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 */
router.get(
  '/api/v4/orders-by-client',
  passport.authenticate('jwt', { session: false }),
  transform('query'),
  checkValidData('query'),
  async (req, res) => {
    const user = req.user;
    let pageSize = req.query.pageSize || 10;
    if (pageSize > 10 || pageSize < 0) {
      pageSize = 10;
    }
    let nextCursor = req.query.nextCursor;
    let customerId = user.customerId;
    const buff = Buffer.from(customerId.toString(), 'base64');
    const customerIdExtract = buff.toString('utf-8');
    const array = customerIdExtract.split('/');
    if (array.length === 5) {
      customerId = array[4];
    }
    if (nextCursor === 'null' || nextCursor === 'undefined') {
      nextCursor = null;
    }
    const nextPage = nextCursor
      ? `first:${pageSize}, after:"${nextCursor}"`
      : `first:${pageSize}, after:null`;
    const query = `
        {
          customers(first:1, query:"id:${customerId}") {
            edges {
              node {
                id
                displayName
                orders(reverse:true,sortKey:CREATED_AT,${nextPage}){
                  pageInfo {
                    hasNextPage
                    hasPreviousPage
                  }
                  edges{
                    cursor
                    node{
                      id
                      name
                      email
                      phone
                      updatedAt 
                      createdAt 
                      tags
                      fullyPaid
                      totalPriceSet{
                        presentmentMoney{
                          amount 
                          currencyCode 
                        }
                        shopMoney{
                          amount 
                          currencyCode 
                        }
                      }
                    }
                  } 
                }
              }
            }
          }
        }
      `;
    await shopify
      .graphql(query)
      .then(async (data) => {
        let { customers } = data;
        [customers] = customers.edges.map((e) => e.node);
        customers.pageInfo = {
          nextCursor: null,
        };
        if (customers.orders.pageInfo.hasNextPage === false) {
          customers.pageInfo.nextCursor = null;
        }
        if (customers.orders.pageInfo.hasNextPage === true) {
          customers.pageInfo.nextCursor =
            customers.orders.edges[customers.orders.edges.length - 1].cursor;
        }
        customers.orders = customers.orders.edges.map((e) => e.node);
        const listId = [];
        for (const order of customers.orders) {
          const splitId = order.id.split('/');
          order.id = splitId[splitId.length - 1];
          listId.push(order.id);
        }
        for (let i = listId.length; i < 10; i++) {
          listId.push(0);
        }
        let [...orders] = await Promise.all([
          listId[0] ? shopify.order.get(Number(listId[0])) : null,
          listId[1] ? shopify.order.get(Number(listId[1])) : null,
          listId[2] ? shopify.order.get(Number(listId[2])) : null,
          listId[3] ? shopify.order.get(Number(listId[3])) : null,
          listId[4] ? shopify.order.get(Number(listId[4])) : null,
          listId[5] ? shopify.order.get(Number(listId[5])) : null,
          listId[6] ? shopify.order.get(Number(listId[6])) : null,
          listId[7] ? shopify.order.get(Number(listId[7])) : null,
          listId[8] ? shopify.order.get(Number(listId[8])) : null,
          listId[9] ? shopify.order.get(Number(listId[9])) : null,
        ]);
        orders = orders.filter(function (order) {
          return order !== null;
        });
        res.json({
          message: 'ok',
          code: 200,
          nextCursor: customers.pageInfo.nextCursor,
          orders,
        });
      })
      .catch((err) =>
        res.status(400).json({
          code: 400,
          message: err.message,
        }),
      );
  },
);

/**
 * @swagger
 * /api/v4/orders-by-id:
 *  get:
 *    tags:
 *      - User's Account
 *    description: Get details of a specific order from Shopify
 *    parameters:
 *    - in: query
 *      name: order_id
 *      schema:
 *        type: string
 *      example: '4174459404422'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: No order found in Shopify
 */
router.get(
  '/api/v4/orders-by-id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const orderId = req.query.order_id;
    shopify.order
      .get(orderId)
      .then((orders) => {
        res.status(200).json({
          message: 'ok',
          code: 200,
          data: orders,
        });
      })
      .catch(() => {
        res.status(404).json({
          message: 'Not found',
          code: 404,
          data: [],
        });
      });
  },
);

/**
 * @swagger
 * /api/v4/login:
 *  post:
 *    security: []
 *    tags:
 *      - User's Account
 *    description: Login to a users account
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              username:
 *                type: string
 *                description: The user's username.
 *                example: test123456
 *              password:
 *                type: string
 *                description: The user's password.
 *                example: test123456
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not authorised.  Need to login
 *      '404':
 *        description: No user matching that username or the password for that account is wrong
 */

router.post('/api/v4/login', async (req, res) => {
  const db = req.app.db;
  if (req.body.username === undefined || req.body.password === undefined) {
    return res.status(404).json({
      message: 'Not found',
      code: 404,
      data: null,
    });
  } else {
    const user = await db.users.findOne({ usersName: req.body.username });
    if (!user) {
      return res.status(404).json({
        message: 'Not found',
        code: 404,
        data: null,
      });
    }
    const comparePass = await bcrypt.compare(
      req.body.password,
      user.userPassword,
    );
    if (!comparePass) {
      return res.status(404).json({
        message: 'Access denied. Check password and try again.',
        code: 404,
        data: null,
      });
    }
    user.ID = user._id;
    user.Token = Config.winformToken;
    user.Level = '';
    user.Name = user.usersName;
    user.LevelName = '';
    user.CreateAt = '';
    user.Avatar = '';
    user.Permissions = '';
    user.Menus = '';
    return res.status(200).json({
      message: 'ok',
      code: 200,
      data: user,
    });
  }
});

/**
 * @swagger
 * /api/v4/customer/check-username:
 *  post:
 *    security: []
 *    tags:
 *      - User's Account
 *    description: Check whether the username exists
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            type: object
 *            properties:
 *              user_name:
 *                type: string
 *                description: The encrypted username
 *                example: P3psVlNEYndLWu3PZx2Jeg==
 *              iv:
 *                type: string
 *                description: The iv of encrypted string.
 *                example: 0f19cb1d4456cce4038def2a2d54915b
 *            required:
 *              - user_name
 *              - iv
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid encrypted username
 */

router.post('/api/v4/customer/check-username', async (req, res) => {
  const db = req.app.db;
  const encryptedUserName = req.body.user_name_encrypted || req.body.user_name;
  let decryptedUserName = '';
  try {
    decryptedUserName = common.decryptDataFromClient(
      encryptedUserName,
      req.body.iv,
    );
    db.customers.findOne(
      { userNameLower: decryptedUserName.toLowerCase() },
      (err, result) => {
        if (err) {
          console.info(err.stack);
        }
        if (result) {
          if (
            result.userName.toLowerCase() === decryptedUserName.toLowerCase()
          ) {
            res.status(200).json({
              message: 'ok',
              code: 200,
              data: {
                exist: true,
              },
            });
          } else {
            res.status(200).json({
              message: 'ok',
              code: 200,
              data: {
                exist: false,
              },
            });
          }
        } else {
          res.status(200).json({
            message: '',
            code: 200,
            data: {
              exist: false,
            },
          });
        }
      },
    );
  } catch (e) {
    console.log(e);
    res.status(400).json({
      message: 'error',
      code: 400,
      data: null,
    });
  }
});

/**
 * @swagger
 * /api/v4/customer/save-username:
 *  post:
 *    tags:
 *      - User's Account
 *    description: Save username of a customer
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              user_name:
 *                type: string
 *                description: The user's username
 *                example: test123456
 *            required:
 *              - user_name
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Username already exists
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: Customer Not Found
 */

router.post(
  '/api/v4/customer/save-username',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const customer_id = req.user.customerId;
    const user_name = req.body.user_name ? req.body.user_name : '';
    const user_name_lower = req.body.user_name
      ? req.body.user_name.toLowerCase()
      : '';
    if (!customer_id || !user_name) {
      res.status(400).json({
        message: 'invalid data',
        code: 400,
        data: null,
      });
      return;
    }
    let customer = await db.customers.findOne({
      $and: [{ userNameLower: user_name_lower }],
    });
    if (customer) {
      if (customer.customerId != customer_id) {
        res.status(400).json({
          message: 'Username exist',
          code: 400,
        });
        return;
      }
      res.status(200).json({
        message: 'Nothing updated',
        code: 200,
      });
      return;
    }
    customer = await db.customers.findOne({
      $and: [{ customerId: customer_id }],
    });
    if (customer) {
      customer.userName = user_name;
      customer.userNameLower = user_name_lower;

      await Promise.all([
        db.customers.update(
          {
            _id: common.getId(customer._id),
          },
          { $set: { userName: user_name, userNameLower: user_name_lower } },
          {},
        ),
        db.orders.update(
          { customerId: customer_id },
          { $set: { orderUserName: customer.userName } },
          { multi: true },
        ),
      ]);

      res.status(200).json({
        message: 'Update Success',
        code: 200,
        data: customer,
      });
      return;
    }

    res.status(400).json({
      message: 'Customer Not Found',
      code: 400,
    });
  },
);

/**
 * @swagger
 * /api/v4/customer/update-email:
 *  post:
 *    tags:
 *      - User's Account
 *    description: Update a user's email from Shopify
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            type: object
 *            properties:
 *              customer_id:
 *                type: string
 *                description: The user's customer ID on Shopify.
 *                example: 5058575597702
 *              username:
 *                type: string
 *                description: The user's username.
 *                example: nubbies_test6
 *            required:
 *              - customer_id
 *              - username
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user. Need to login first
 *      '404':
 *        description: Cannot find this customer in shopify.
 */

router.post(
  '/api/v4/customer/update-email',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const customer_id = req.user.customerId;
    const customer = await db.customers.findOne({ customerId: customer_id });
    if (customer) {
      const customers = await shopify.customer.search({ id: customer_id });
      if (customers && !customers.length) {
        return res.status(200).json({
          message: 'Cannot find this customer in shopify.',
          code: 404,
          data: [],
        });
      }

      if (customers && customers.length > 0) {
        customer.email = customers[0].email;
        await Promise.all([
          db.customers.update(
            { _id: customer._id },
            { $set: customer },
            { multi: false },
          ),
          db.orders.update(
            { customerId: customer_id },
            { $set: { orderEmail: customer.email } },
            { multi: true },
          ),
        ]);
        return res.status(200).json({
          message: 'ok',
          code: 200,
          data: [],
        });
      }
      // Please investigate what this does
      return res.status(200).json({
        message: 'Have any error when get customer info from shopify.',
        code: 404,
        data: [],
      });
    }
    return res.status(200).json({
      message: 'Cannot find this customer in database.',
      code: 404,
      data: [],
    });
  },
);

/**
 * @swagger
 * /api/v4/library:
 *  get:
 *    tags:
 *      - User's Account
 *    description: Get vault settings for the user
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

router.get(
  '/api/v4/library/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const user = req.user;
    const [customer] = await Promise.all([
      db.customers.findOne({ customerId: user.customerId }),
    ]);

    if (!customer) {
      return res.status(401).json({
        message: 'Unauthorised. Please refer to administrator.',
        data: null,
      });
    }

    //check IP valid for digital content
    // const userCountry = await requestCountry(req);
    const disableDigitalContent = false;
    // if (userCountry) {
    //   console.log('User country code: ' + userCountry);
    //   // //list country release
    //   // const contriesCode = process.env.COUNTRY_CODE
    //   //   ? process.env.COUNTRY_CODE
    //   //   : '';
    //   // const arrCode = contriesCode.split(',');
    //   // if (arrCode.includes(userCountry)) {
    //   //   disableDigitalContent = false;
    //   // }
    // }

    let [userSetting] = await Promise.all([
      db.userSettings.findOne({
        customerId: customer.customerId,
        settingName: 'library.list.sorting',
      }),
    ]);
    if (!userSetting) {
      userSetting = {
        customerId: customer.customerId,
        settingName: 'library.list.sorting',
        value: 'newest',
      };
    }
    const filter = req.query.filter ? req.query.filter : '';
    let sort = req.query.sort;
    if (sort == null || sort == undefined || sort == '') {
      sort = userSetting.value;
    } else {
      userSetting.value = sort;
    }

    if (userSetting._id) {
      userSetting = await Promise.all([
        db.userSettings.update(
          { _id: common.getId(userSetting._id) },
          { $set: userSetting },
          {},
        ),
      ]);
    } else {
      userSetting = await Promise.all([db.userSettings.insertOne(userSetting)]);
    }

    const sortPipeline = {};

    switch (sort.toLowerCase()) {
      case 'az':
        sortPipeline['productTitle'] = 1;
        break;
      case 'za':
        sortPipeline['productTitle'] = -1;
        break;
      case 'newest':
        sortPipeline['registrationDate'] = -1;
        break;
      case 'oldest':
        sortPipeline['registrationDate'] = 1;
        break;
      default:
        sortPipeline['registrationDate'] = -1;
        break;
    }
    const character = !disableDigitalContent
      ? {
          $cond: {
            if: { $gte: [{ $size: '$digitalContents' }, 0] },
            then: { $arrayElemAt: ['$digitalContents', 0] },
            else: '$digitalContents',
          },
        }
      : null;
    const products = await db.orders
      .aggregate([
        {
          $match: {
            $expr: {
              $and: [{ $eq: ['$customerId', String(customer.customerId)] }],
            },
          },
        },
        { $match: { orderCertNumber: { $exists: true, $nin: ['', null] } } },
        {
          $match: {
            $expr: {
              $and: [
                { $eq: ['$customerId', String(customer.customerId)] },
                {
                  $and: [
                    { $ne: ['$orderCertNumber', null] },
                    { $ne: ['$orderCertNumber', ''] },
                    { $eq: ['$customerId', String(customer.customerId)] },
                  ],
                },
              ],
            },
          },
        },
        {
          $group: {
            _id: {
              orderLocalId: '$_id',
              productVariantSku: '$sku',
            },
            orders: {
              $push: {
                productTitle: { $first: '$orderProducts.productTitle' },
                productImageUrl: { $first: '$orderProducts.productImageUrl' },
                productImageUrlS3: {
                  $first: '$orderProducts.productImageUrlS3',
                },
                productBackgroundImageS3: {
                  $first: '$orderProducts.productBackgroundImageS3',
                },
                _id: '$_id',
                orderId: '$shopifyOrderId',
                orderNumber: '$orderNumber',
                udid: '$udid',
                serialNumber: '$orderCertNumber',
                editionNumber: '$editionNumber',
                orderDate: '$orderDate',
                orderStatus: {
                  $cond: [
                    { $ne: ['$orderSmartContract', ''] },
                    'Registered',
                    '$orderStatus',
                  ],
                },
              },
            },
          },
        },
        {
          $lookup: {
            from: 'products',
            let: { productVariantSku: '$_id.productVariantSku' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ['$productVariantSku', '$$productVariantSku'] },
                    ],
                  },
                },
              },
              {
                $project: {
                  productTitle: '$productTitle',
                  productImageUrl: '$productImageUrl',
                  productImageUrlS3: '$productImageUrlS3',
                  productBackgroundImageS3: '$productBackgroundImageS3',
                  enableEditionNumber: '$enableEditionNumber',
                  productAssetsFile: '$productAssetsFile',
                },
              },
            ],
            as: 'products',
          },
        },
        {
          $lookup: {
            from: 'digitalContents',
            let: { productVariantSku: '$_id.productVariantSku' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      {
                        $in: [
                          '$$productVariantSku',
                          { $ifNull: ['$productSkus', []] },
                        ],
                      },
                      { $ne: ['$isDisable', true] },
                    ],
                  },
                },
              },
              {
                $project: {
                  characterCode: '$toyId',
                  characterName: '$toyName',
                  characterToken: '$character_token',
                  characterContentType: '$digitalContentType',
                  contentUrlOrScreen: '$contentUrlOrScreen',
                  buttonText: '$buttonText',
                  buttonColor: '$buttonColor',
                  textColor: '$textColor',
                  isHostedInaApp: '$isHostedInApp',
                },
              },
            ],
            as: 'digitalContents',
          },
        },
        {
          $lookup: {
            from: 'forrealNfts',
            let: { orderId: '$_id.orderLocalId' },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      {
                        $eq: ['$orderId', '$$orderId'],
                      },
                      { $ne: ['$lastTxHash', null] },
                      { $ne: ['$lastTxHash', ''] },
                    ],
                  },
                },
              },
              {
                $project: {
                  lastTxHash: '$lastTxHash',
                },
              },
            ],
            as: 'forrealNfts',
          },
        },
        {
          $project: {
            _id: '$_id',
            productTitle: { $first: '$products.productTitle' },
            productImageUrl: { $first: '$products.productImageUrl' },
            productImageUrlS3: { $first: '$products.productImageUrlS3' },
            productBackgroundImageUrlS3: {
              $first: '$products.productBackgroundImageS3',
            },
            enableEditionNumber: {
              $first: '$products.enableEditionNumber',
            },
            productAssetsFile: { $first: '$products.productAssetsFile' },
            sku: '$_id.productVariantSku',
            certificates: '$orders',
            character,
            lastTxHash: { $first: '$forrealNfts.lastTxHash' },
            gameUrl: process.env.GAME_URL || null,
            registrationDate: { $min: '$orders.orderDate' },
          },
        },
      ])
      .sort(sortPipeline)
      .toArray();

    // handle missing product image if product deleted because we still cache product inside order
    products.forEach((item) => {
      if (
        !item.productTitle &&
        item.certificates &&
        item.certificates.length > 0
      ) {
        item.productTitle = item.certificates[0].productTitle;
        item.productImageUrl = item.certificates[0].productImageUrl;
        item.productImageUrlS3 = item.certificates[0].productImageUrlS3;
        item.productBackgroundImageUrlS3 =
          item.certificates[0].productBackgroundImageUrlS3;
      }
    });
    const resultProduct = !filter
      ? products
      : products.filter((item) => {
          return (
            item.productTitle?.toLowerCase().indexOf(filter.toLowerCase()) > -1
          );
        });
    res.status(200).json({
      code: 200,
      message: 'ok',
      data: {
        username: customer.userName,
        email: customer.email,
        filter_setting: 'date_desc',
        background: 'normal',
        customer_id: customer.customerId,
        products: resultProduct,
      },
    });
  },
);

/**
 * @swagger
 * /api/v4/feed/library/:username:
 *  get:
 *    tags:
 *      - User's Account
 *    description: Get products to populate user's vault
 *    parameters:
 *    - in: path
 *      name: username
 *      schema:
 *        type: string
 *      example: 'test123456'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

router.get(
  '/api/v4/feed/library/:username',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const [customer] = await Promise.all([
      db.customers.findOne({ userName: req.params.username }),
    ]);
    if (!customer) {
      return res.status(401).json({
        message: 'Unauthorised. Please refer to administrator.',
        data: null,
      });
    }

    //check IP valid for digital content
    const userCountry = await requestCountry(req);
    const disableDigitalContent = false;
    if (userCountry) {
      console.log('User country code: ' + userCountry);
      // //list country release
      // const contriesCode = process.env.COUNTRY_CODE
      //   ? process.env.COUNTRY_CODE
      //   : '';
      // const arrCode = contriesCode.split(',');
      // if (arrCode.includes(userCountry)) {
      //   disableDigitalContent = false;
      // }
    }

    let [userSetting] = await Promise.all([
      db.userSettings.findOne({
        customerId: customer.customerId,
        settingName: 'library.list.sorting',
      }),
    ]);
    if (!userSetting) {
      userSetting = {
        customerId: customer.customerId,
        settingName: 'library.list.sorting',
        value: 'newest',
      };
    }
    const filter = req.query.filter ? req.query.filter : '';
    let sort = req.query.sort;
    if (sort == null || sort == undefined || sort == '') {
      sort = userSetting.value;
    } else {
      userSetting.value = sort;
    }

    if (userSetting._id) {
      userSetting = await Promise.all([
        db.userSettings.update(
          { _id: common.getId(userSetting._id) },
          { $set: userSetting },
          {},
        ),
      ]);
    } else {
      userSetting = await Promise.all([db.userSettings.insertOne(userSetting)]);
    }

    const sortPipeline = {};

    switch (sort.toLowerCase()) {
      case 'az':
        sortPipeline['productTitle'] = 1;
        break;
      case 'za':
        sortPipeline['productTitle'] = -1;
        break;
      case 'newest':
        sortPipeline['registrationDate'] = -1;
        break;
      case 'oldest':
        sortPipeline['registrationDate'] = 1;
        break;
      default:
        sortPipeline['registrationDate'] = -1;
        break;
    }

    const character = !disableDigitalContent
      ? {
          $cond: {
            if: { $gte: [{ $size: '$digitalContents' }, 0] },
            then: { $arrayElemAt: ['$digitalContents', 0] },
            else: '$digitalContents',
          },
        }
      : null;

    const [products] = await Promise.all([
      db.orders
        .aggregate([
          {
            $match: {
              $expr: {
                $and: [{ $eq: ['$customerId', String(customer.customerId)] }],
              },
            },
          },
          { $match: { orderCertNumber: { $exists: true, $nin: ['', null] } } },
          {
            $lookup: {
              from: 'products',
              localField: 'sku',
              foreignField: 'productVariantSku',
              as: 'product',
            },
          },
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$customerId', String(customer.customerId)] },
                  {
                    $and: [
                      { $ne: ['$orderCertNumber', null] },
                      { $ne: ['$orderCertNumber', ''] },
                      { $eq: ['$customerId', String(customer.customerId)] },
                    ],
                  },
                ],
              },
            },
          },
          {
            $match: {
              $and: [
                { 'product.productTitle': { $regex: filter, $options: 'i' } },
              ],
            },
          },
          {
            $unwind: '$product',
          },
          {
            $group: {
              _id: '$product',
              orders: {
                $push: {
                  orderId: '$shopifyOrderId',
                  udid: '$udid',
                  serialNumber: '$orderCertNumber',
                  editionNumber: '$editionNumber',
                  orderDate: '$orderDate',
                  orderStatus: {
                    $cond: [
                      { $ne: ['$orderSmartContract', ''] },
                      'Registered',
                      '$orderStatus',
                    ],
                  },
                },
              },
            },
          },
          {
            $lookup: {
              from: 'digitalContents',
              let: { productVariantSku: '$_id.productVariantSku' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        {
                          $in: [
                            '$$productVariantSku',
                            { $ifNull: ['$productSkus', []] },
                          ],
                        },
                        { $ne: ['$isDisable', true] },
                      ],
                    },
                  },
                },
                {
                  $project: {
                    _id: '$_id.id',
                    characterCode: '$toyId',
                    characterName: '$toyName',
                    characterToken: '$character_token',
                    characterContentType: '$digitalContentType',
                    contentUrlOrScreen: '$contentUrlOrScreen',
                    buttonText: '$buttonText',
                    buttonColor: '$buttonColor',
                    textColor: '$textColor',
                    isHostedInaApp: '$isHostedInApp',
                  },
                },
              ],
              as: 'digitalContents',
            },
          },
          {
            $project: {
              _id: '$_id.id',
              productTitle: '$_id.productTitle',
              description: '$_id.productDescription',
              productImageUrl: '$_id.productImageUrl',
              sku: '$_id.productVariantSku',
              certificates: '$orders',
              character,
              gameUrl: process.env.GAME_URL || null,
              registrationDate: { $min: '$orders.orderDate' },
              productBackgroundImage: '$productBackgroundImage',
              productBackgroundImageS3: '$productBackgroundImageS3',
            },
          },
        ])
        .sort(sortPipeline)
        .toArray(),
    ]);

    res.status(200).json({
      data: {
        username: customer.userName,
        email: customer.email,
        filter_setting: 'date_desc',
        background: 'normal',
        customer_id: customer.customerId,
        products: products,
      },
    });
  },
);

/**
 * @swagger
 * /api/v4/customer/profile:
 *  get:
 *    tags:
 *      - User's Account
 *    description: Get profile for current customer
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

// Get current customer
router.get(
  '/api/v4/customer/profile',
  passport.authenticate('jwt', {
    session: false,
  }),
  async (req, res) => {
    try {
      const db = req.app.db;
      const user = req.user;
      const _customer = await db.customers
        .aggregate([
          {
            $match: {
              customerId: user.customerId,
            },
          },
          {
            $lookup: {
              from: 'userSettings',
              let: {
                customerId: '$customerId',
              },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $eq: ['$customerId', '$$customerId'],
                    },
                  },
                },
                {
                  $project: {
                    _id: 0,
                    customerId: 0,
                  },
                },
              ],
              as: 'userSettings',
            },
          },
          {
            $project: {
              password: 0,
            },
          },
        ])
        .toArray();

      // --- Start: Get customer tags from Shopify and mapping with customer tag in our DB (exclusiveSale collection)
      const { tags } = await shopify.customer.get(Number(req.user.customerId));

      const customerTagArr = [];
      const exclusiveSaleTags = [];
      if (tags.length > 0) {
        for (const tag of tags.split(',')) {
          customerTagArr.push(tag.trim());
        }
      }

      if (customerTagArr.length > 0) {
        const exclusiveSales = await db.exclusiveSale
          .find({
            status: true,
          })
          .sort({
            updatedAt: -1,
          })
          .toArray();
        if (exclusiveSales.length > 0) {
          for (const exclusiveSale of exclusiveSales) {
            const exclusiveSaleTagArr = [];
            if (exclusiveSale.customerTag.length > 0) {
              for (const tag of exclusiveSale.customerTag[0].split(',')) {
                exclusiveSaleTagArr.push(tag.trim());
              }
            }
            const isFounded = customerTagArr.some((ai) =>
              exclusiveSaleTagArr.includes(ai),
            );
            if (isFounded) {
              exclusiveSaleTags.push({
                id: exclusiveSale._id,
                title: exclusiveSale.titleTag,
                description: exclusiveSale.description,
                handle: exclusiveSale.productTag,
                imageSrc: exclusiveSale.imgUrl,
                updatedAt: exclusiveSale.updatedAt,
                bannerText: exclusiveSale.bannerText,
                type: 'exclusive',
              });
            }
          }
        }
      }
      // --- End: Get customer tags from Shopify and mapping with customer tag in our DB (exclusiveSale collection)

      const customer = {
        ..._customer[0],
        exclusiveSaleTags: exclusiveSaleTags,
      };

      res.status(200).json({
        code: 200,
        data: customer,
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({
        code: 400,
        error: {
          message: error.message,
        },
      });
    }
  },
);

/**
 * @swagger
 * /api/v4/analyticsCreepyCuties/insert:
 *  post:
 *    tags:
 *      - User's Account
 *    description: Analytics for Creepy Cuties
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              after:
 *                type: string
 *                description: Enter cursor to go to the next page
 *                example: null
 *              sortBy:
 *                type: string
 *                example: TITLE
 *              reverse:
 *                type: boolean
 *                example: false
 *              query:
 *                type: string
 *                example: -tag:exclusive title:a wood
 *            required:
 *              - query
 *              - reverse
 *              - sortBy
 *              - after
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid parameter
 *      '401':
 *        description: Not a validated user.  Need to login first
 */

router.post(
  '/api/v4/analyticsCreepyCuties/insert',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const user = req.user;
    let doc;
    const [customer] = await Promise.all([
      db.customers.findOne({ customerId: user.customerId }),
    ]);

    if (!customer) {
      res.status(401).json({
        message: 'Unauthorised. Please refer to administrator.',
        data: null,
      });
      return;
    }

    if (
      !Number.isInteger(req.body.actionType) ||
      (!req.body.productSku && req.body.actionType !== 2) ||
      (!req.body.productSkus && req.body.actionType === 2) ||
      !(req.body.actionType > 0 && req.body.actionType < 5)
    ) {
      res.status(400).json({
        code: 400,
        message: 'Invalid parameters',
        data: null,
      });
      return;
    }

    if (req.body.actionType === 2) {
      if (req.body.productSkus) {
        if (req.body.productSkus.length === 0) {
          const [analyticsCCExists] = await Promise.all([
            db.analyticsCreepyCuties.update(
              {
                $and: [
                  {
                    executeDate: {
                      $lte: new Date(
                        new Date().toJSON().slice(0, 10) + 'T23:59:59+0000',
                      ),
                    },
                  },
                  {
                    executeDate: {
                      $gte: new Date(
                        new Date().toJSON().slice(0, 10) + 'T00:00:00+0000',
                      ),
                    },
                  },
                  { customerId: customer.customerId },
                  { actionType: req.body.actionType },
                ],
              },
              {
                $set: { isPut: false },
              },
              { multi: true },
            ),
          ]);
        } else {
          for (let i = 0; i < req.body.productSkus.length; i++) {
            let [analyticsCC] = await Promise.all([
              db.analyticsCreepyCuties.findOne({
                $and: [
                  {
                    executeDate: {
                      $lte: new Date(
                        new Date().toJSON().slice(0, 10) + 'T23:59:59+0000',
                      ),
                    },
                  },
                  {
                    executeDate: {
                      $gte: new Date(
                        new Date().toJSON().slice(0, 10) + 'T00:00:00+0000',
                      ),
                    },
                  },
                  { customerId: customer.customerId },
                  { actionType: req.body.actionType },
                  { productSku: req.body.productSkus[i] },
                ],
              }),
            ]);

            if (analyticsCC) {
              analyticsCC = await Promise.all([
                db.analyticsCreepyCuties.update(
                  {
                    _id: common.getId(analyticsCC._id),
                  },
                  {
                    $set: { isPut: true },
                  },
                  { multi: false },
                ),
              ]);
            } else {
              doc = {
                productSku: req.body.productSkus[i],
                customerId: customer.customerId,
                actionType: req.body.actionType,
                userId: user._id,
                isPut: true,
                executeDate: new Date(),
              };
            }
          }
        }

        //Find skus not exists in productSkus of request
        const analyticsCCOld = await db.analyticsCreepyCuties
          .aggregate(
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$customerId', customer.customerId] },
                    { $eq: ['$actionType', req.body.actionType] },
                  ],
                },
                $and: [
                  {
                    executeDate: {
                      $gte: new Date(
                        new Date().toJSON().slice(0, 10) + 'T00:00:00+0000',
                      ),
                    },
                  },
                  {
                    executeDate: {
                      $lte: new Date(
                        new Date().toJSON().slice(0, 10) + 'T23:59:59+0000',
                      ),
                    },
                  },
                ],
              },
            },
            {
              $project: {
                _id: 0,
                productSku: '$productSku',
              },
            },
          )
          .toArray();
        const valueOfArrReq = req.body.productSkus;
        const valueOfArrOld = analyticsCCOld.map((a) => a.productSku);

        // A comparer used to determine if two entries are equal.
        const isSameUser = (valueOfArrReq, valueOfArrOld) =>
          valueOfArrReq === valueOfArrOld;
        // Get items that only occur in the left array,
        // using the compareFunction to determine equality.
        const onlyInLeft = (left, right, compareFunction) =>
          left.filter(
            (leftValue) =>
              !right.some((rightValue) =>
                compareFunction(leftValue, rightValue),
              ),
          );

        const onlyInOld = onlyInLeft(valueOfArrOld, valueOfArrReq, isSameUser);
        const result = [...onlyInOld];
        // For each element have sku not exists in request
        if (result.length > 0) {
          for (let i = 0; i <= result.length; i++) {
            await db.analyticsCreepyCuties.update(
              {
                $and: [
                  {
                    executeDate: {
                      $lte: new Date(
                        new Date().toJSON().slice(0, 10) + 'T23:59:59+0000',
                      ),
                    },
                  },
                  {
                    executeDate: {
                      $gte: new Date(
                        new Date().toJSON().slice(0, 10) + 'T00:00:00+0000',
                      ),
                    },
                  },
                  { customerId: customer.customerId },
                  { actionType: req.body.actionType },
                  { productSku: result[i] },
                ],
              },
              {
                $set: { isPut: false },
              },
              { multi: false },
            );
          }
        } else {
          await db.analyticsCreepyCuties.update(
            {
              $and: [
                {
                  executeDate: {
                    $lte: new Date(
                      new Date().toJSON().slice(0, 10) + 'T23:59:59+0000',
                    ),
                  },
                },
                {
                  executeDate: {
                    $gte: new Date(
                      new Date().toJSON().slice(0, 10) + 'T00:00:00+0000',
                    ),
                  },
                },
                { customerId: customer.customerId },
                { actionType: req.body.actionType },
              ],
            },
            {
              $set: { isPut: true },
            },
            { multi: true },
          );
        }
      }
    } else {
      doc = {
        productSku: req.body.productSku,
        customerId: customer.customerId,
        actionType: req.body.actionType,
        userId: user._id,
        executeDate: new Date(),
      };
    }
    if (doc) {
      const result = await db.analyticsCreepyCuties.insertOne(doc);
      if (!result.result.n) {
        return res
          .status(400)
          .json({ code: 400, message: 'Insert failed', data: null });
      }
    }

    const actionType = [
      'Share success',
      'Put success',
      'Tutorial success',
      'Scan success',
    ];
    const result = { actionType: req.body.actionType };
    res.status(200).json({
      code: 200,
      message: actionType[req.body.actionType - 1],
      data: result,
    });
  },
);

/**
 * @swagger
 * /api/v4/analyticsCreepyCuties/delete/:id:
 *  delete:
 *    tags:
 *      - User's Account
 *    description: Delete an analytics
 *    parameters:
 *    - in: path
 *      name: id
 *      schema:
 *        type: string
 *      example: 612769f69a24c408657ca1c2
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: Not found analytics
 */

router.delete(
  '/api/v4/analyticsCreepyCuties/delete/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const db = req.app.db;
    const analytics = await db.analyticsCreepyCuties.findOne({
      _id: common.getId(req.params.id),
    });
    if (!analytics) {
      res.status(404).json({
        code: 404,
        message: 'Analytics Creepy Cuties Not Found',
        data: [],
      });
      return;
    }
    await db.analyticsCreepyCuties.deleteOne({
      _id: common.getId(req.params.id),
    });
    res
      .status(200)
      .json({ code: 200, message: 'delete succesfully', data: null });
  },
);

module.exports = router;
