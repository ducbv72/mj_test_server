const express = require('express');
const passport = require('passport');
const router = express.Router();
const Promise = require('promise');
const { transform, checkValidData } = require('../../../validator');
const dayjs = require('dayjs');
const axios = require('axios');
const url = require('../../../config/url.json');
const { shopify } = require('../../../config/shopify');

/**
 * @swagger
 * /api/v4/discount-code-used/{token}:
 *  get:
 *    security: []
 *    tags:
 *      - Vip
 *    description: Get redeemed discount codes for a customer by token
 *    parameters:
 *    - in: path
 *      name: token
 *      required: true
 *      schema:
 *        type: string
 *        description: Token had been signed in shopify website.
 *        example: 'N2IyMjYzNzU3Mzc0NmY2ZDY1NzI0OTY0MjIzYTIyMzMzMTMxMzAzMTM0MzQzODM2MzgzNDM4MzYyMjJjMjI2NTZkNjE2OTZjMjIzYTIyNjQ2Zjc2NzU0MDZkNjk2NzY4NzQ3OTZhNjE3ODc4MmU2MzZmNmQyMjdk'
 *    - in: query
 *      name: email
 *      required: true
 *      schema:
 *        type: string
 *        example: 'dovu@mightyjaxx.com'
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid Parameters
 *      '404':
 *        description: User not found
 *      '403':
 *        description: Forbidden
 *      '500':
 *        description: Internal Error Server
 */

/**
 * encode:
 * { customerId: '3110144868486', email: 'dovu@mightyjaxx.com' }
 * JSON.stringify(data)
 * data.split('').map(c=>c.charCodeAt(0).toString(16)).join('')
 * token = Buffer.from(data).toString('base64')
 */

router.get('/api/v4/discount-code-used/:token', async (req, res) => {
  try {
    const db = req.app.db;
    const token = req.params.token;
    const email = req.query.email;

    if (!email) {
      return res.status(400).json({ code: 400, message: 'Invalid Parameters' });
    }

    // Decode token to get customer info
    const hex = new Buffer.from(token, 'base64').toString();
    const decoded = hex
      .split(/(\w\w)/g)
      .filter((p) => !!p)
      .map((c) => String.fromCharCode(parseInt(c, 16)))
      .join('');
    const customerInfo = JSON.parse(decoded);

    const [customerDb] = await Promise.all([
      db.customers.findOne({ email: email }),
    ]);

    if (!customerDb) {
      return res.status(404).json({
        code: 404,
        message: 'User not found',
        data: null,
      });
    }

    if (
      customerDb.customerId !== customerInfo.customerId ||
      email !== customerInfo.email
    ) {
      return res.status(403).json({
        code: 403,
        message: 'Forbidden',
        data: null,
      });
    }

    return res.status(200).json({
      code: 200,
      data: { discountCodes: customerDb.appliedDiscountCodes || [] },
    });
  } catch (error) {
    return res
      .status(500)
      .json({ code: 500, message: 'Internal Error Server' });
  }
});

/**
 * @swagger
 * /api/v4/vip/ways-to-earn:
 *  get:
 *    security: []
 *    tags:
 *      - Vip
 *    description: Get ways-to-earn
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Internal Error Server
 */

/**
 *  Vip page smile info
 */

// See if we can remove
router.get(
  '/api/v4/vip/ways-to-earn',
  passport.authenticate('jwt', { session: false }),
  transform('query'),
  checkValidData('query'),
  checkValidData('params'),
  async (req, res) => {
    try {
      const { email } = req.user;

      const [{ data: customer }] = await Promise.all([
        axios.get(url.smileCustomerUrl.replace('{email}', email), {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        }),
      ]);

      if (!customer || !customer.customers.length) {
        return res.status(404).json({ code: 404, message: 'User not found' });
      }

      const [{ data: rewardPrograms }] = await Promise.all([
        axios.get(
          url.smileRewardProgramsReferralUrl.replace(
            '{account_id}',
            customer.customers[0].account_id,
          ),
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            },
          },
        ),
      ]);

      const referralsReward = rewardPrograms.reward_programs.find(
        (e) => e.type === 'referrals',
      );

      const [
        {
          data: { customer_activity_rules },
        },
        {
          data: { referral_rewards },
        },
      ] = await Promise.all([
        axios.get(
          url.smileWayToEarnUrl.replace(
            '{customer_id}',
            customer.customers[0].id,
          ),
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${customer.customers[0].authentication_token}`,
              'smile-client': 'smile-ui',
              'smile-channel-key': process.env.SMILEIO_PUBLIC_KEY,
            },
          },
        ),
        axios.get(
          url.smileReferralsUrl.replace(
            '{reward_program_id}',
            referralsReward.id,
          ),
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            },
          },
        ),
      ]);

      return res.status(200).json({
        code: 200,
        data: {
          customer_activity_rules: customer_activity_rules.map((v) => {
            if (
              v.activity_rule.type === 'customer_birthday' &&
              customer.customers[0].date_of_birth
            ) {
              return { ...v, is_available: false };
            }

            return v;
          }),
          referral_rewards: {
            ...referral_rewards[0],
            referral: customer.customers[0].referral_url,
          },
        },
      });
    } catch (error) {
      console.log(error.message);
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

/**
 * @swagger
 * /api/v4/vip/change-birthday:
 *  post:
 *    tags:
 *      - Vip
 *    description: Update customer date of birthday
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              date_of_birth:
 *                type: string
 *            required:
 *             - date_of_birth
 *          example: {date_of_birth: '03-05'}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Invalid parameters
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Internal Error Server
 */
router.post(
  '/api/v4/vip/change-birthday',
  passport.authenticate('jwt', { session: false }),
  transform('query'),
  checkValidData('body'),
  async (req, res) => {
    try {
      const { email } = req.user;
      const { date_of_birth } = req.body;

      if (!dayjs(date_of_birth).isValid()) {
        return res
          .status(400)
          .json({ code: 400, message: 'Invalid parameters' });
      }

      const [{ data: customer }] = await Promise.all([
        axios.get(url.smileCustomerUrl.replace('{email}', email), {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        }),
      ]);

      if (!customer || !customer.customers.length) {
        return res.status(404).json({ code: 404, message: 'User not found' });
      }

      const customerData = {
        customer: {
          date_of_birth: dayjs(date_of_birth)
            .startOf('day')
            // Change to current year
            .set('year', 1004)
            .format('YYYY-MM-DDT00:00:00[.000Z]'),
        },
      };

      const [
        {
          data: { customer: customerInfo },
        },
      ] = await Promise.all([
        axios.put(
          url.smileChangeBirthday.replace(
            '{customer_id}',
            customer.customers[0].id,
          ),
          customerData,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${customer.customers[0].authentication_token}`,
            },
          },
        ),
      ]);

      return res.status(200).json({ code: 200, data: customerInfo });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

/**
 * @swagger
 * /api/v4/vip/smile-activities:
 *  post:
 *    tags:
 *      - Vip
 *    description: Create an smile activity
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              activity_token:
 *                type: string
 *            required:
 *             - activity_token
 *          example: {activity_token: 'activity_token'}
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: User not found
 *      '401':
 *        description: Invalid parameters
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Internal Error Server
 */
router.post(
  '/api/v4/vip/smile-activities',
  passport.authenticate('jwt', { session: false }),
  transform('query'),
  checkValidData('body'),
  async (req, res) => {
    try {
      const { email } = req.user;
      const { activity_token } = req.body;

      if (!activity_token) {
        return res
          .status(400)
          .json({ code: 400, message: 'Invalid parameters' });
      }

      const [{ data: customer }] = await Promise.all([
        axios.get(url.smileCustomerUrl.replace('{email}', email), {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        }),
      ]);

      if (!customer || !customer.customers.length) {
        return res.status(404).json({ code: 404, message: 'User not found' });
      }

      const activityData = {
        activity: {
          token: activity_token,
        },
      };

      const [
        {
          data: { activity: activityInfo },
        },
      ] = await Promise.all([
        axios.post(url.smilePlatformActivities, activityData, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${customer.customers[0].authentication_token}`,
            'smile-client': 'smile-ui',
            'smile-channel-key': process.env.SMILEIO_PUBLIC_KEY,
          },
        }),
      ]);

      return res.status(200).json({ code: 200, data: activityInfo });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

/**
 * @swagger
 * /api/v4/vip/vouchers:
 *  get:
 *    tags:
 *      - Vip
 *    description: Get vouchers for a specific user
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: User not found
 */

router.get(
  '/api/v4/vip/vouchers',
  passport.authenticate('jwt', { session: false }),
  transform('query'),
  checkValidData('query'),
  checkValidData('params'),
  async (req, res) => {
    try {
      const db = req.app.db;
      const { email } = req.user;

      const [{ data: customer }] = await Promise.all([
        axios.get(url.smileCustomerUrl.replace('{email}', email), {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        }),
      ]);

      if (!customer || !customer.customers.length) {
        return res.status(404).json({ code: 404, message: 'User not found' });
      }

      const [
        {
          data: { reward_fulfillments },
        },
        customerDb,
      ] = await Promise.all([
        axios.get(
          url.smileVouchersUrl.replace(
            '{customer_id}',
            customer.customers[0].id,
          ),
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
              'smile-client': 'smile-admin',
            },
          },
        ),
        db.customers.findOne({ email }),
      ]);

      const appliedDiscountCodes =
        (customerDb && customerDb.appliedDiscountCodes) || [];

      const vouchers = reward_fulfillments
        .filter((e) => e.usage_status === 'unused')
        .filter(
          (e) =>
            !appliedDiscountCodes.includes(e.code) &&
            (!e.expires_at ||
              (e.expires_at &&
                dayjs().valueOf() < dayjs(e.expires_at).valueOf())),
        )
        .map((e) => ({
          id: e.id,
          name: e.name,
          usage_status: 'unused',
          reason_text: e.reason_text,
          source_description: e.source_description,
          code: e.code,
          expires_at: e.expires_at,
          is_expired: e.is_expired,
        }));

      return res.status(200).json({ code: 200, data: vouchers });
    } catch (error) {
      console.error(error);
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

/**
 * @swagger
 * /api/v4/vip/past-vouchers:
 *  get:
 *    tags:
 *      - Vip
 *    description: Get vouchers that have been used by a specific user
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: User not found
 */

router.get(
  '/api/v4/vip/past-vouchers',
  passport.authenticate('jwt', { session: false }),
  transform('query'),
  checkValidData('query'),
  checkValidData('params'),
  async (req, res) => {
    try {
      const db = req.app.db;
      const { email } = req.user;

      const [{ data: customer }] = await Promise.all([
        axios.get(url.smileCustomerUrl.replace('{email}', email), {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        }),
      ]);

      if (!customer || !customer.customers.length) {
        return res.status(404).json({ code: 404, message: 'User not found' });
      }

      const [
        {
          data: { reward_fulfillments },
        },
        customerDb,
      ] = await Promise.all([
        axios.get(
          url.smileVouchersUrl.replace(
            '{customer_id}',
            customer.customers[0].id,
          ),
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
              'smile-client': 'smile-admin',
            },
          },
        ),
        db.customers.findOne({ email }),
      ]);

      const appliedDiscountCodes =
        (customerDb && customerDb.appliedDiscountCodes) || [];

      const _usedVouchers = reward_fulfillments.filter(
        (e) =>
          e.usage_status === 'used' || appliedDiscountCodes.includes(e.code),
      );

      let usedVouchers = [];

      for (let i = 0; i < _usedVouchers.length; i++) {
        const e = _usedVouchers[i];
        let used_at = e.used_at;

        if (appliedDiscountCodes.includes(e.code)) {
          try {
            const { updated_at } = await shopify.discountCode.lookup({
              code: e.code,
            });

            used_at = updated_at;
          } catch (error) {
            used_at = e.updated_at;
          }
        }

        usedVouchers = [
          ...usedVouchers,
          {
            id: e.id,
            name: e.name,
            usage_status: 'used',
            reason_text: e.reason_text,
            source_description: e.source_description,
            code: e.code,
            expires_at: e.expires_at,
            is_expired: e.is_expired,
            used_at,
            created_at: e.created_at,
          },
        ];
      }

      const expiredVouchers = reward_fulfillments
        .filter(
          (e) =>
            e.usage_status === 'unused' &&
            !appliedDiscountCodes.includes(e.code) &&
            e.expires_at &&
            dayjs().valueOf() >= dayjs(e.expires_at).valueOf(),
        )
        .map((e) => {
          return {
            id: e.id,
            name: e.name,
            usage_status: 'expired',
            reason_text: e.reason_text,
            source_description: e.source_description,
            code: e.code,
            expires_at: e.expires_at,
            is_expired: e.is_expired,
            used_at: null,
            created_at: e.created_at,
          };
        });

      const vouchers = [...usedVouchers, ...expiredVouchers].sort(
        (a, b) => dayjs(b.created_at).valueOf() - dayjs(a.created_at).valueOf(),
      );

      return res.status(200).json({ code: 200, data: vouchers });
    } catch (error) {
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

/**
 * @swagger
 * /api/v4/vip/transaction-history:
 *  get:
 *    tags:
 *      - Vip
 *    description: Get point earning history for a specific user
 *    parameters:
 *    - in: query
 *      name: page
 *      schema:
 *        type: integer
 *        example: 1
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Not a validated user.  Need to login first
 *      '404':
 *        description: User not found
 */

router.get(
  '/api/v4/vip/transaction-history',
  passport.authenticate('jwt', { session: false }),
  transform('query'),
  checkValidData('query'),
  checkValidData('params'),
  async (req, res) => {
    try {
      const { email } = req.user;
      const page = (req.query.page && Number(req.query.z)) || 1;

      const [{ data: customer }] = await Promise.all([
        axios.get(url.smileCustomerUrl.replace('{email}', email), {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
            'smile-client': 'smile-admin',
          },
        }),
      ]);

      if (!customer || !customer.customers.length) {
        return res.status(404).json({ code: 404, message: 'User not found' });
      }

      const [
        {
          data: { points_transactions },
        },
      ] = await Promise.all([
        axios.get(
          `${url.smilePointsTransactionUrl}?customer_id=${customer.customers[0].id}&page=${page}&page_size=10`,
          {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Token ${process.env.SMILEIO_PRIVATE_KEY}`,
              'smile-client': 'smile-admin',
            },
          },
        ),
      ]);

      return res.status(200).json({ code: 200, data: points_transactions });
    } catch (error) {
      return res
        .status(500)
        .json({ code: 500, message: 'Internal Error Server' });
    }
  },
);

module.exports = router;
