const express = require('express');
const router = express.Router();
const Config = require('../../../config');
const common = require('../../../lib/common');
const Promise = require('promise');
const jwt = require('jsonwebtoken');
const moment = require('moment-timezone');
const dayjs = require('dayjs');
const relativeTime = require('dayjs/plugin/relativeTime');
dayjs.extend(relativeTime);
const { shopify } = require('../../../config/shopify');
const request = require('request');
const StockXAPI = require('stockx-api');
const stockX = new StockXAPI();

router.get('/api/v1/generate-testing-token/:email', async (req, res) => {
  if (process.env.IGNORE_TOKEN !== 'true') {
    res.status(400).json({
      message: 'user not found',
      data: [],
    });
  }
  const config = req.app.config;
  const db = req.app.db;
  const regex = new RegExp(['^', req.params.email, '$'].join(''), 'i');
  const [user] = await Promise.all([
    db.customers.find({ email: regex }).toArray(),
  ]);
  if (user.length == 1) {
    const token = jwt.sign(
      {
        email: user[0].email,
        customerId: user[0].customerId,
        shopifyToken: '',
      },
      config.jwtSecret,
    );
    res.status(200).json({
      message: 'ok',
      token: token,
    });
  } else {
    res.status(400).json({
      message: 'user not found',
      data: [],
    });
  }
});

router.post('/api/v3/zendesksync', async (req, res) => {
  const db = req.app.db;
  let nextPage =
    'https://mightyjaxx.zendesk.com/api/v2/help_center/en-us/sections/360000589673/articles.json?page=0&per_page=50';
  const header = 'Title' + '\t' + 'Id' + '\t' + 'Url' + '\n';
  let content = header;
  while (nextPage != null) {
    const request = await fetch(nextPage);
    const result = await request.json();
    nextPage = result.next_page;
    const articles = result.articles;

    for (const i in articles) {
      const filter = articles[i].title;
      const [product] = await Promise.all([
        db.products.findOne({
          productTitle: { $regex: new RegExp(filter, 'i') },
        }),
      ]);

      if (product) {
        product.zendeskUrl = articles[i].html_url;
        await Promise.all([
          db.products.updateOne(
            { _id: product._id },
            { $set: product },
            {},
            false,
          ),
        ]);
        continue;
      } else {
        content += articles[i].title + '\t';
        content += articles[i].id + '\t';
        content += articles[i].html_url + '\t';
        content += '\n';
      }
    }
  }
  res.setHeader('Content-Type', 'application/vnd.openxmlformats');
  res.setHeader(
    'Content-Disposition',
    'attachment; filename=zendesksyncresult.xls',
  );
  res.write(new Buffer([0xff, 0xfe]));
  // res.write(iconv.convert(content));
  res.end();
});

router.get('/api/v1/getUpcommingProducts', async (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  const db = req.app.db;
  const [data] = await Promise.all([
    db.upcommingProducts
      .find({
        $and: [{ active: true }],
      })
      .sort({ dropDate: 1 })
      .toArray(),
  ]);
  res.status(200).json({
    msg: 'ok',
    code: 200,
    data: data,
  });
});

router.get('/api/v1/getAllUpcommingProducts', async (req, res) => {
  const db = req.app.db;
  const [data] = await Promise.all([
    db.upcommingProducts.find({}).sort({ dropDate: 1 }).toArray(),
  ]);
  res.status(200).json({
    msg: 'ok',
    code: 200,
    data: data,
  });
});

router.get('/api/v1/countUpcommingInterest/:id', async (req, res) => {
  const db = req.app.db;
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  const id = req.params.id;
  const [data] = await Promise.all([
    db.upcommingProducts.findOne({ _id: common.getId(id) }),
  ]);
  if (!data.count) {
    data.count = 0;
  }
  data.count = data.count + 1;
  db.upcommingProducts.update(
    { _id: common.getId(id) },
    {
      $set: {
        count: data.count,
      },
    },
    {},
    (err, numReplaced) => {
      if (err) {
        res.status(400).json({
          message: 'Invalid data',
        });
      } else {
        res.status(200).json({
          message: 'ok',
        });
      }
    },
  );
});

router.get('/api/v1/deleteUpcommingProducts/:id', async (req, res) => {
  const db = req.app.db;
  const id = req.params.id;
  const data = await Promise.all([
    db.upcommingProducts.findOne({ _id: common.getId(id) }),
  ]);
  await Promise.all([
    db.upcommingProducts.deleteOne({ _id: common.getId(id) }),
  ]);
  res.status(200).json({
    msg: 'ok',
    code: 200,
  });
});

router.get('/api/v1/product-stockx-by-sku/:token/:sku', async (req, res) => {
  const db = req.app.db;
  const sku = req.params.sku;
  const token = req.params.token;
  if (token !== Config.stockxToken) {
    res.status(401).json({
      msg: 'Unauthorised. Please refer to administrator.',
      data: null,
    });
  }

  if (!sku || (sku && sku.length === 0)) {
    res.status(400).json({
      msg: 'Invalid data',
      data: null,
    });
  }

  const [product] = await Promise.all([
    db.products.findOne({
      productVariantSku: sku,
    }),
  ]);
  if (product) {
    const slug = product.stockxSlug;
    if ((slug && slug.length === 0) || !product.isShowStockxBanner) {
      res.status(400).json({
        msg: 'Product not found',
        data: null,
      });
    } else {
      const productList = await stockX.searchProducts(slug);
      if (productList && productList.length > 0) {
        const productItem = productList[0];
        productItem.affiliate_url = Config.affiliateUrl + slug;
        productItem.lowest_ask = productItem.market.lowestAsk;
        res.status(200).json({
          msg: 'Product found',
          data: productItem,
        });
      } else {
        res.status(400).json({
          msg: 'Product not found',
          data: null,
        });
      }
    }
  } else {
    res.status(400).json({
      msg: 'Product not found',
      data: null,
    });
  }
});

router.get('/api/v1/get-certificate-by-order-id', async (req, res) => {
  const db = req.app.db;
  const order_id = req.param('order_id');

  db.orders.findOne({ shopifyOrderId: order_id }, (err, order) => {
    if (err) {
      console.info(err.stack);
      res.status(404).json({
        msg: 'Not found',
        code: 404,
        data: [],
      });
    } else {
      res.status(200).json({
        msg: 'ok',
        code: 200,
        data: order,
      });
    }
  });
});

router.get('/api/v1/product/:sku', (req, res) => {
  const db = req.app.db;
  db.products.findOne({ productVariantSku: req.params.sku }, (err, product) => {
    if (err) {
      res.status(400).json({ message: 'not found', data: [] });
    } else if (!product) {
      res.status(200).json({ data: [] });
    } else {
      product.currentDateUTC = moment
        .tz('Asia/Singapore')
        .format('YYYY-MM-DD HH:mm:ss');
      res.status(200).json({ message: 'ok', data: product });
    }
  });
});

router.get('/api/v1/artists', async (req, res) => {
  const db = req.app.db;
  const [artists] = await Promise.all([
    db.artists.find({}, { artistName: 1, artistImage: 1 }).toArray(),
  ]);
  res.status(200).json({
    message: 'ok',
    data: artists,
  });
});

router.get('/api/v1/artist/:id', async (req, res) => {
  const db = req.app.db;
  const [artist] = await Promise.all([
    db.artists.findOne({ articleId: req.params.id }),
  ]);

  res.status(200).json({
    message: 'ok',
    data: artist,
  });
});

router.get('/api/v1/smile-io/list-point-product', async (req, res) => {
  const db = req.app.db;
  const headers = {
    Authorization: 'Bearer ' + process.env.SMILEIO_PRIVATE_KEY,
  };
  const isDisable = req.query.isDisable ? req.query.isDisable : true;
  const options = {
    url: 'https://api.smile.io/v1/points_products',
    method: 'GET',
    headers: headers,
  };

  function callback(error, response, body) {
    if (!error && response.statusCode === 200) {
      const dataJson = JSON.parse(body);
      if (dataJson.points_products.length > 0) {
        if (isDisable === true) {
          dataJson.points_products = dataJson.points_products.filter(
            (e) => e.reward.value !== null,
          );
        }
        res.status(200).json({
          msg: 'ok',
          code: 200,
          data: dataJson.points_products,
        });
      } else {
        res.status(404).json({
          msg: 'Not found',
          code: 404,
          data: null,
        });
      }
    } else {
      res.status(404).json({
        msg: 'Not found',
        code: 404,
        data: null,
      });
    }
  }

  request(options, callback);
});

router.post('/api/v1/smile-io/purchase-point-product/', async (req, res) => {
  const db = req.app.db;
  const point_product_id = req.body.point_product_id;
  const points_to_spend = req.body.points_to_spend;
  const customer_id = req.body.customer_id;
  const store_font_key = req.body.store_font_key;
  const data = {
    customer_id: customer_id.toString(),
    // 'points_to_spend': points_to_spend.toString()
  };
  const formData = JSON.stringify(data);
  const contentLength = formData.length;

  request(
    {
      headers: {
        Authorization: 'Bearer ' + process.env.SMILEIO_PRIVATE_KEY,
        'Content-Length': contentLength,
        'Content-Type': 'application/json',
      },
      url:
        'https://api.smile.io/v1/points_products/' +
        point_product_id +
        '/purchase',
      body: formData,
      method: 'POST',
    },
    (err, res1, body) => {
      console.log(err);
      const dataJson = JSON.parse(body);
      if (err || dataJson.error) {
        res.status(404).json({
          msg: dataJson.error.message,
          code: 400,
          data: dataJson,
        });
      } else {
        const dataJson = JSON.parse(body);
        res.status(200).json({
          msg: 'ok',
          code: 200,
          data: dataJson,
        });
      }
    },
  );
});

router.get('/api/v1/smile-io/way-to-earn', async (req, res) => {
  const db = req.app.db;
  const store_font_key = req.body.store_font_key;
  const arrayWayToEarn = [
    {
      name: 'Join the crew!',
      description: '200 Mighty Coins',
      image_url: '/images/way_to_earn/join_the_crew.png',
    },
    {
      name: 'Own some Treasure',
      description: '10 Mighty Coins for every $1 spent',
      image_url: '/images/way_to_earn/own_some_treasure.png',
    },
    {
      name: 'Mighty Like our Facebook Page',
      description: '100 Mighty Coins',
      image_url: '/images/way_to_earn/mighty_like_our_facebook_page.png',
    },
    {
      name: 'Spy on our Instagram',
      description: '100 Mighty Coins',
      image_url: '/images/way_to_earn/spy_on_our_instagram.png',
    },
    {
      name: "Cheers! It's your Birthday",
      description: '500 Mighty Coins',
      image_url: '/images/way_to_earn/cheers_its_your_birthday.png',
    },
    {
      name: 'Join the crew!',
      description: '200 Mighty Coins',
      image_url: '/images/way_to_earn/join_the_crew.png',
    },
  ];
  res.status(200).json({
    msg: 'ok',
    code: 200,
    data: arrayWayToEarn,
  });
});

router.post('/api/v1/customer/check-username', async (req, res) => {
  const db = req.app.db;
  console.log(req.body.user_name);
  db.customers.findOne(
    { userNameLower: req.body.user_name.toLowerCase() },
    (err, result) => {
      if (err) {
        console.info(err.stack);
      }
      if (result) {
        if (
          result.userName.toLowerCase() === req.body.user_name.toLowerCase()
        ) {
          res.status(200).json({
            msg: 'ok',
            code: 200,
            data: {
              exist: true,
            },
          });
        } else {
          res.status(200).json({
            msg: 'ok',
            code: 200,
            data: {
              exist: false,
            },
          });
        }
      } else {
        res.status(200).json({
          msg: '',
          code: 200,
          data: {
            exist: false,
          },
        });
      }
    },
  );
});

router.post('/api/v1/customer/update-email', async (req, res) => {
  const db = req.app.db;
  const customer_id = req.body.customer_id;
  const [customer] = await Promise.all([
    db.customers.findOne({ customerId: customer_id }),
  ]);

  if (customer) {
    shopify.customer
      .search({ id: customer_id })
      .then(async (customers) => {
        if (customers.length > 0) {
          // update email customer
          customer.email = customers[0].email;
          const [result] = await Promise.all([
            db.customers.update(
              { _id: customer._id },
              { $set: customer },
              { multi: false },
            ),
          ]);
          // update email owner
          const [result1] = await Promise.all([
            db.orders.update(
              { customerId: customer_id },
              { $set: { orderEmail: customer.email } },
              { multi: true },
            ),
          ]);
          res.status(200).json({
            msg: 'ok',
            code: 200,
            data: [],
          });
        } else {
          res.status(200).json({
            msg: 'Not found',
            code: 404,
            data: [],
          });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(200).json({
          msg: 'Not found',
          code: 404,
          data: [],
        });
      });
  } else {
    res.status(200).json({
      msg: 'Not found',
      code: 404,
      data: [],
    });
  }
});

module.exports = router;
