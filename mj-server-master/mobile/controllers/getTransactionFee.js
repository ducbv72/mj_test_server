const { isValidAddress } = require('ethereumjs-util');

const handleErrors = require('../../lib/handleErrors');
const { getTxFee } = require('../../services/web3');
const { getUSDPrice } = require('../../services/coingecko');

const getTransactionFee = async (req, res) => {
  try {
    const customerAddress = req.query.address;

    if (!customerAddress || !isValidAddress(customerAddress)) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_parameters',
      });
    }
    const [ethPriceInUsd, fee] = await Promise.all([
      getUSDPrice('ethereum'),
      getTxFee(customerAddress),
    ]);
    const txFeeInUsd = fee * ethPriceInUsd;

    return res.status(200).json({
      status: 'success',
      tx_fee_in_usd: parseFloat(txFeeInUsd).toFixed(2),
    });
  } catch (error) {
    handleErrors(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = getTransactionFee;
