const { isValidAddress } = require('ethereumjs-util');
const Stripe = require('stripe');

const handleErrors = require('../../../lib/handleErrors');
const { getUSDPrice } = require('../../../services/coingecko');
const { getTxFee } = require('../../../services/web3');
const logger = require('../../../logger');

const createPaymentIntent = async (req, res) => {
  try {
    const {
      email,
      currency,
      request_three_d_secure,
      payment_method_types = [],
      address: customerAddress,
    } = req.body;

    if (!customerAddress || !isValidAddress(customerAddress)) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_parameters',
      });
    }

    const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
      apiVersion: '2020-08-27',
    });

    const [customer, ethPriceInUsd, fee] = await Promise.all([
      stripe.customers.create({ email }),
      getUSDPrice('ethereum'),
      getTxFee(customerAddress),
    ]);

    let txFeeInUsd = fee * ethPriceInUsd;
    const minTxFee = 40;

    if (txFeeInUsd < minTxFee) {
      // Make sure tx fee is high enough to cover the gas fee
      txFeeInUsd = minTxFee;
    }

    const amount = Math.ceil(txFeeInUsd * 100);

    // Create a PaymentIntent with the order amount and currency.
    const params = {
      amount,
      currency,
      customer: customer.id,
      metadata: {
        eth_address: customerAddress,
        email,
      },
      payment_method_options: {
        card: {
          request_three_d_secure: request_three_d_secure || 'automatic',
        },
        sofort: {
          preferred_language: 'en',
        },
      },
      payment_method_types: payment_method_types,
    };

    logger.info('Create payment intent with the following data:');
    logger.info({ amount, currency, customerAddress, email, txFeeInUsd });

    const paymentIntent = await stripe.paymentIntents.create(params);

    return res.send({
      clientSecret: paymentIntent.client_secret,
      publishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
      amount,
    });
  } catch (error) {
    handleErrors(error);
    logger.error(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = createPaymentIntent;
