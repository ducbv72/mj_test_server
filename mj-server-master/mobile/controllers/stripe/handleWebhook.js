const Stripe = require('stripe');
const Sentry = require('@sentry/node');
const logger = require('../../../logger');

const handleErrors = require('../../../lib/handleErrors');
const { mintNftToAddress } = require('../../../services/web3');

const handleWebhook = async (req, res) => {
  try {
    const stripe = new Stripe(process.env.STRIPE_SECRET_KEY, {
      apiVersion: '2020-08-27',
    });
    let event = null;

    try {
      const sig = req.headers['stripe-signature'] || [];
      event = stripe.webhooks.constructEvent(
        req.rawBody,
        sig,
        process.env.STRIPE_WEBHOOK_SECRET,
      );
    } catch (err) {
      logger.error(`⚠️  Webhook signature verification failed.`);
      return res.sendStatus(400);
    }

    // Extract the data from the event.
    const data = event.data;
    const eventType = event.type;
    const pi = (data && data.object) || null;

    if (eventType === 'payment_intent.succeeded') {
      // Cast the event into a PaymentIntent to make use of the types.

      // Funds have been captured
      // To cancel the payment after capture you will need to issue a Refund (https://stripe.com/docs/api/refunds).
      logger.info(`🔔  Webhook received: ${pi.object} ${pi.status}!`);
      logger.info('💰 Payment captured!');

      if (pi.id === 'pi_3K2wosC45JevuU9Z085un448') {
        // This webhook event has already been handled manually for the customer
        return res.sendStatus(200);
      }

      const customerAddress = (pi.metadata && pi.metadata.eth_address) || '';

      if (!customerAddress) {
        return res.sendStatus(200);
      }

      const email = pi.metadata.email;
      const feePaidByCustomer = pi.amount / 100;

      logger.info({ customerAddress, email, feePaidByCustomer });

      const onSuccess = (txHash, tokenURI) => {
        const updateDoc = {
          eth_address: customerAddress,
          NFTs: [
            {
              transaction_hash: txHash,
              token_URI: tokenURI,
              sent_to_address: customerAddress,
            },
          ],
        };

        logger.info({ updateDoc });

        req.app.db.customers.updateOne(
          { email },
          {
            $set: updateDoc,
          },
          (err) => {
            if (err) {
              logger.error('Failed updating user');
            } else {
              logger.info('User updated');
            }
          },
        );
      };
      await mintNftToAddress(customerAddress, feePaidByCustomer, onSuccess);
    } else if (eventType === 'payment_intent.payment_failed') {
      // Cast the event into a PaymentIntent to make use of the types.
      logger.info(`🔔  Webhook received: ${pi.object} ${pi.status}!`);
      logger.info('❌ Payment failed.');
    }

    return res.sendStatus(200);
  } catch (error) {
    handleErrors(error);
    Sentry.captureException(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = handleWebhook;
