const axios = require('axios');
const uuid = require('uuid-random');

const {
  generateMultipassUrl,
  generateMultipassToken,
  customerAccessTokenCreateWithMultipass,
  queryCustomer,
  customerUpdate,
} = require('../../services/shopify');
const { getFacebookAccessToken } = require('../../services/facebook');
const handleErrors = require('../../lib/handleErrors');

const getMultipassUrlFromFacebookCode = async (req, res) => {
  try {
    const fbCode = req.query.code;
    const redirectToPath = req.query.redirect_to_path;
    const statusCode = 400;
    const payload = {
      status: 'failed',
      error_message: 'invalid_parameters',
    };

    if (fbCode) {
      const host = req.get('Host');
      const { path: currentRoute, protocol } = req;
      const redirectUri = `${protocol}://${host}${currentRoute}`;

      const fbToken = await getFacebookAccessToken(fbCode, redirectUri);
      
      console.log('Getting facebook user from token:', fbToken);

      const fbBaseUrl = 'https://graph.facebook.com';
      const url = `${fbBaseUrl}/me?access_token=${fbToken}`;

      const response = await axios.get(url);
      const { name, id } = response.data;

      const fbResponse = await axios.get(
        `${fbBaseUrl}/${id}?fields=email&access_token=${fbToken}`,
      );
      const { email } = fbResponse.data;

      if (!email) {
        throw new Error('Email not found');
      }
      
      const nameArray = (name && name.split(' ')) || [];
      const firstName = (nameArray && nameArray[0]) || null;
      const lastName =
        (nameArray && nameArray.slice(1) && nameArray.slice(1).join(' ')) ||
        null;

      const customerData = {
        first_name: firstName,
        last_name: lastName,
        email,
        verified_email: true,
        created_at: new Date().toISOString(),
      };

      const multipassToken = generateMultipassToken(customerData);
      const storefrontRes = await customerAccessTokenCreateWithMultipass(
        multipassToken,
      );

      const accessToken =
        (storefrontRes &&
          storefrontRes.customerAccessToken &&
          storefrontRes.customerAccessToken.accessToken) ||
        '';

      if (!accessToken) {
        throw new Error('Invalid access token');
      }

      const newTags = ['social_login', 'facebook_login'];
      const fields = `{
        tags,
        id,
      }`;

      const { tags: existingTags, id: customerId } = await queryCustomer(
        accessToken,
        fields,
      );

      let hasTermsToken = false;

      if (existingTags && existingTags.length) {
        hasTermsToken =
          existingTags.filter((tag) => tag.includes('terms_token')).length > 0;
      }

      if (!hasTermsToken) {
        newTags.push(`terms_token_${uuid()}`);
      }

      await customerUpdate({
        tags: [...existingTags, ...newTags],
        id: customerId,
      });

      const multipassUrl = generateMultipassUrl(redirectToPath, multipassToken);

      return res.redirect(multipassUrl);
    }

    return res.status(statusCode).json(payload);
  } catch (error) {
    handleErrors(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = getMultipassUrlFromFacebookCode;
