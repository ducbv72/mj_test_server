const { OAuth2Client } = require('google-auth-library');

const { getAccessTokenFromSocialProfile } = require('../../services/shopify');
const { registerCustomerInDB } = require('../../services/mongodb');
const { audience } = require('../../config/google');
const handleErrors = require('../../lib/handleErrors');

const getAccessTokenFromGoogleToken = async (req, res) => {
  try {
    const encodedToken = req.query.id_token;
    const encodedName = req.query.full_name;
    const encodedUserId = req.query.user_id;
    const fullName = decodeURI(encodedName);
    const idToken = decodeURI(encodedToken);
    const googleUserId = decodeURI(encodedUserId);

    let statusCode = 400;
    let payload = {
      status: 'failed',
      error_message: 'invalid_parameters',
    };

    if (idToken && encodedToken) {
      const client = new OAuth2Client(audience);

      const ticket = await client.verifyIdToken({
        idToken,
        audience,
      });
      const googlePayload = ticket.getPayload();
      const {
        email,
        sub: userId,
        email_verified: emailVerified,
      } = googlePayload;

      if (!email) {
        throw new Error('Email not found');
      }

      if (userId !== googleUserId) {
        throw new Error('Details do not match');
      }

      const socialMethod = 'google_login';
      const { accessToken, customerData, tags } =
        await getAccessTokenFromSocialProfile(
          fullName,
          email,
          emailVerified,
          socialMethod,
        );
      const db = req.app.db;
      const config = req.app.config;

      await registerCustomerInDB(db, accessToken, config);

      statusCode = 200;
      payload = {
        status: 'success',
        accessToken,
        customerData,
        tags,
      };
    }

    return res.status(statusCode).json(payload);
  } catch (error) {
    handleErrors(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };

    return res.status(500).json(payload);
  }
};

module.exports = getAccessTokenFromGoogleToken;
