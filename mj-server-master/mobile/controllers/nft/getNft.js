const handleErrors = require('../../../lib/handleErrors');

const getNft = async (req, res) => {
  try {
    const db = req.app.db;
    const tokenId = req.params.tokenId;

    if (!tokenId) {
      return res.status(400).json({ message: 'Product not found.' });
    }

    db.nftProducts.findOne({ tokenId: '1' }, (err, result) => {
      if (err) {
        return res.status(400).json({ error: err });
      }

      if (!result) {
        return res.status(400).json({ message: 'Product not found.' });
      }

      const attributes = [];
      const dataKeys = ['name', 'description', 'image', 'animation_url'];
      const skipKeys = ['_id', 'tokenId'];
      const data = {};

      Object.keys(result).forEach((key) => {
        if (skipKeys.includes(key)) {
          return;
        }

        const value = result[key];

        if (dataKeys.includes(key)) {
          data[key] = value;
        } else {
          attributes.push({
            trait_type: key,
            value,
          });
        }
      });

      attributes.push({
        trait_type: 'Serial Number',
        value: `#${tokenId}`,
      });

      data.attributes = attributes;

      return res.status(200).json(data);
    });
  } catch (error) {
    handleErrors(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = getNft;
