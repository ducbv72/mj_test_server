const { ONLY_WHITELISTED } = require('../../../../config/nft');
const handleErrors = require('../../../../lib/handleErrors');

const checkIfAddressIsWhitelisted = async (db, address, nftSymbol) => {
  const whitelistedAddress = await db.whitelistedAddresses.findOne({
    address,
    nftSymbol,
  });

  return !!whitelistedAddress;
};

const getOrders = async (db, address, productTitle) => {
  const { customerId } = await db.orders.findOne({
    eth_address: address,
  });

  const orders = await db.orders.find({
    customerId,
    title: productTitle,
  });

  return orders;
};

const checkIfCustomerCanMint = async (req, res) => {
  try {
    const db = req.app.db;
    const { customerAddress, productTitle, nftSymbol } = req.query;

    if (!customerAddress) {
      return res.status(400).json({ message: 'Invalid address' });
    }

    const [isWhitelisted, orders] = await Promise.all([
      checkIfAddressIsWhitelisted(db, customerAddress, nftSymbol),
      getOrders(db, customerAddress, productTitle),
    ]);

    const numOrders = (orders && orders.length) || 0;
    let canMintNFT = false;
    const customerHasProduct = numOrders > 0 || false;

    if (numOrders) {
      orders.forEach(({ nftMinted }) => {
        if (!nftMinted) {
          canMintNFT = true;
        }
      });
    }

    if (!isWhitelisted && ONLY_WHITELISTED) {
      return res.status(400).json({ message: 'Address is not whitelisted' });
    }

    if (!customerHasProduct) {
      return res
        .status(400)
        .json({ message: 'Customer does not own the product' });
    }

    if (!canMintNFT) {
      return res.status(400).json({ message: 'NFT has already been minted' });
    }

    return res.status(200).json({ customer_can_mint: true });
  } catch (error) {
    handleErrors(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = checkIfCustomerCanMint;
