const Sentry = require('@sentry/node');

const handleErrors = require('../../../lib/handleErrors');

const disconnectWallet = async (req, res) => {
  try {
    const { db } = req.app;
    const customerId = String(req?.user?.customerId).toString();

    if (!customerId) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_customerId',
      });
    }

    const web3AuthDoc = await db.web3Auth.findOne({ customerId });
    const { updatedAt } = web3AuthDoc;

    if (!updatedAt) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_web3_auth_doc',
      });
    }

    await db.web3Auth.updateOne(
      { customerId },
      {
        $set: {
          messageToSign: null,
          verified: null,
          address: null,
          previousAddress: web3AuthDoc?.address,
          previousVerifiedStatus: web3AuthDoc?.verified,
          updatedAt: new Date(),
        },
      },
    );

    return res.status(200).json({
      status: 'success',
    });
  } catch (error) {
    handleErrors(error);
    Sentry.captureException(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = disconnectWallet;
