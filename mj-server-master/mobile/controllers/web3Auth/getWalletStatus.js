const Sentry = require('@sentry/node');

const handleErrors = require('../../../lib/handleErrors');

const getWalletStatus = async (req, res) => {
  try {
    const { db } = req.app;
    const customerId = String(req?.user?.customerId).toString();

    if (!customerId) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_customerId',
      });
    }

    const web3AuthDoc = await db.web3Auth.findOne({ customerId });
    const address = web3AuthDoc?.address ?? '';
    const messageToSign = web3AuthDoc?.messageToSign ?? '';
    const verified = !!web3AuthDoc?.verified ?? false;

    return res.status(200).json({
      status: 'success',
      verified,
      address,
      messageToSign,
    });
  } catch (error) {
    handleErrors(error);
    Sentry.captureException(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = getWalletStatus;
