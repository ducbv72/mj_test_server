const Sentry = require('@sentry/node');
const { isValidAddress } = require('ethereumjs-util');
const Web3 = require('web3');

const handleErrors = require('../../../lib/handleErrors');
const { isValidSignature } = require('../../../services/web3');

const verifySignature = async (req, res) => {
  try {
    let customerAddress = req.body.address;
    const { signature } = req.body;
    const { db } = req.app;
    const customerId = String(req?.user?.customerId).toString();

    if (!customerAddress || !signature || !isValidAddress(customerAddress)) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_parameters',
      });
    }

    customerAddress = Web3.utils.toChecksumAddress(customerAddress);

    const web3AuthDoc = await db.web3Auth.findOne({ address: customerAddress });
    const { messageToSign } = web3AuthDoc;

    if (!messageToSign) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_message_to_sign',
      });
    }

    if (web3AuthDoc?.customerId && web3AuthDoc?.customerId !== customerId) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'address_already_connected_to_existing_account',
      });
    }

    const validSignature = isValidSignature(
      customerAddress,
      signature,
      messageToSign,
    );

    if (!validSignature) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_signature',
      });
    }

    // Delete messageToSign as it's meant to be used once
    await db.web3Auth.updateOne(
      { address: customerAddress },
      {
        $set: {
          messageToSign: null,
          verified: true,
          customerId,
          updatedAt: new Date(),
        },
      },
    );

    return res.status(200).json({
      status: 'success',
    });
  } catch (error) {
    handleErrors(error);
    Sentry.captureException(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = verifySignature;
