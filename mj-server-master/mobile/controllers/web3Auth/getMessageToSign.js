const Sentry = require('@sentry/node');
const { isValidAddress } = require('ethereumjs-util');
const Web3 = require('web3');

const { randomNumber } = require('../../../lib/common');

const handleErrors = require('../../../lib/handleErrors');

const getMessageToSign = async (req, res) => {
  try {
    const db = req.app.db;
    const customerId = String(req?.user?.customerId).toString();
    const customerAddress = Web3.utils.toChecksumAddress(req?.query?.address);

    if (!customerId) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_customerId',
      });
    }

    if (!customerAddress || !isValidAddress(customerAddress)) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'invalid_parameters',
      });
    }

    const nonce = randomNumber(100000, 999999);
    let messageToSign = `Welcome to MightyJaxx! Wallet address: ${customerAddress} Nonce: ${nonce}`;

    const web3AuthDoc = await db.web3Auth.findOne({
      $or: [{ address: customerAddress }, { customerId }],
    });

    if (web3AuthDoc?.customerId && web3AuthDoc?.customerId !== customerId) {
      return res.status(400).json({
        status: 'failed',
        error_message: 'address_already_connected_to_existing_account',
      });
    } else if (web3AuthDoc?.customerId) {
      // Update existing web3AuthDoc
      const verified =
        customerAddress === web3AuthDoc?.address
          ? web3AuthDoc?.verified
          : false;

      await db.web3Auth.updateOne(
        { customerId },
        {
          $set: {
            messageToSign,
            address: customerAddress,
            updatedAt: new Date(),
            verified,
          },
        },
      );
    } else if (web3AuthDoc && web3AuthDoc.messageToSign) {
      // Use existing messageToSign
      messageToSign = web3AuthDoc.messageToSign;
    } else {
      // messageToSign doesn't exist, save to DB
      await db.web3Auth.insertOne({
        address: customerAddress,
        messageToSign,
        createdAt: new Date(),
        verified: false,
      });
    }

    return res.status(200).json({
      status: 'success',
      message_to_sign: messageToSign,
    });
  } catch (error) {
    handleErrors(error);
    Sentry.captureException(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = getMessageToSign;
