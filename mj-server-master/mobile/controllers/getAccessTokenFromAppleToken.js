const appleSignin = require('apple-signin-auth');
const jwtDecode = require('jwt-decode');

const { getAccessTokenFromSocialProfile } = require('../../services/shopify');
const { registerCustomerInDB } = require('../../services/mongodb');
const handleErrors = require('../../lib/handleErrors');

const getAccessTokenFromAppleToken = async (req, res) => {
  try {
    const encodedToken = req.query.identity_token;
    const encodedName = req.query.full_name;
    const fullName = decodeURI(encodedName);
    const identityToken = decodeURI(encodedToken);

    let statusCode = 400;
    let payload = {
      status: 'failed',
      error_message: 'invalid_parameters',
    };

    if (identityToken) {
      const { sub: userAppleId } = await appleSignin
        .verifyIdToken(identityToken, {
          audience: 'com.mightyjaxx',
          ignoreExpiration: true,
        })
        .catch(() => {
          throw new Error('Failed to verify identityToken');
        });
      const decodedToken = jwtDecode(identityToken);
      const {
        email,
        sub: userAppleIdFromToken,
        email_verified: emailVerified,
      } = decodedToken;

      if (!email) {
        throw new Error('Email not found');
      }

      if (userAppleId !== userAppleIdFromToken) {
        throw new Error('Details do not match');
      }

      const socialMethod = 'apple_login';
      const { accessToken, customerData, tags } =
        await getAccessTokenFromSocialProfile(
          fullName,
          email,
          emailVerified,
          socialMethod,
        );

      const db = req.app.db;
      const config = req.app.config;

      await registerCustomerInDB(db, accessToken, config);

      statusCode = 200;
      payload = {
        status: 'success',
        accessToken,
        customerData,
        tags,
      };
    }

    return res.status(statusCode).json(payload);
  } catch (error) {
    handleErrors(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = getAccessTokenFromAppleToken;
