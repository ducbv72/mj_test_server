const axios = require('axios');

const { getAccessTokenFromSocialProfile } = require('../../services/shopify');
const { registerCustomerInDB } = require('../../services/mongodb');
const handleErrors = require('../../lib/handleErrors');

const getAccessTokenFromFacebookToken = async (req, res) => {
  try {
    const { token: fbToken, email: emailFromQuery } = req.query;
    let statusCode = 400;
    let payload = {
      status: 'failed',
      error_message: 'invalid_parameters',
    };

    if (fbToken) {
      console.log('Getting facebook user from token:', fbToken);

      const fbBaseUrl = 'https://graph.facebook.com';
      const url = `${fbBaseUrl}/me?access_token=${fbToken}`;

      const response = await axios.get(url);
      if (!response || !response.data || !response.data.id) {
        // Invalid facebook id
        return res.status(400).json({
          status: 'error',
          error: 'facebook_id_not_found',
          message: 'facebook_id_not_found',
        });
      }

      const { name, id } = response.data;
      const db = req.app.db;
      const getEmailFromDb = async (id, db) => {
        const result = await db.customers.findOne({ facebookId: id });
        if (result && result.email) {
          return result.email;
        }

        return null;
      };
      const [fbResponse, emailFromDb] = await Promise.all([
        axios.get(`${fbBaseUrl}/${id}?fields=email&access_token=${fbToken}`),
        getEmailFromDb(id, db),
      ]);
      // Use emailFromDb by default
      // Use emailFromFacebook if emailFromDb doesn't exist
      const { email: emailFromFacebook } = fbResponse.data;
      const email = emailFromDb || emailFromFacebook || emailFromQuery;
      if (!email) {
        // User doesn't have an email
        return res.status(400).json({
          status: 'error',
          error: 'facebook_email_not_found',
          message: 'facebook_email_not_found',
        });
      }

      const emailVerified = true;
      const socialMethod = 'facebook_login';
      const { accessToken, customerData, tags } =
        await getAccessTokenFromSocialProfile(
          name,
          email,
          emailVerified,
          socialMethod,
        );

      const config = req.app.config;

      await registerCustomerInDB(db, accessToken, config, id);

      statusCode = 200;
      payload = {
        status: 'success',
        accessToken,
        customerData,
        tags,
      };
    }

    return res.status(statusCode).json(payload);
  } catch (error) {
    handleErrors(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = getAccessTokenFromFacebookToken;
