const handleErrors = require('../../lib/handleErrors');
const { customerUpdate, queryCustomerById } = require('../../services/shopify');

const getMultipassUrlFromFacebookCode = async (req, res) => {
  try {
    const {
      terms_token: termsToken,
      customer_id: customerId,
      email,
    } = req.query;
    let statusCode = 400;
    let payload = {
      status: 'failed',
      error_message: 'invalid_parameters',
    };

    if (email && customerId && termsToken) {
      const newTags = ['accepted_terms_and_privacy_policy'];
      const fields = `{
        tags,
        id,
      }`;

      const gid = `gid://shopify/Customer/${customerId}`;

      const { tags: existingTags, id: fetchedCustomerId } =
        await queryCustomerById(customerId, fields);
      if (fetchedCustomerId !== gid) {
        throw new Error('Invalid customerId');
      }

      let fetchedTermsToken = '';
      existingTags.forEach((tag) => {
        if (tag.includes('terms_token')) {
          fetchedTermsToken = tag;
        }
      });

      if (fetchedTermsToken !== termsToken) {
        throw new Error('Invalid termsToken');
      }

      await customerUpdate({
        tags: [...existingTags, ...newTags],
        id: gid,
      });

      statusCode = 200;
      payload = {
        status: 'success',
      };

      return res.status(statusCode).json(payload);
    }

    return res.status(statusCode).json(payload);
  } catch (error) {
    handleErrors(error);

    const payload = {
      error: 'server_error',
      message: 'Internal Server Error',
    };
    return res.status(500).json(payload);
  }
};

module.exports = getMultipassUrlFromFacebookCode;
