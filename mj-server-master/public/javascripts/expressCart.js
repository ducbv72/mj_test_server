let productSkuAdded = [];
const KEY_REMOVE_SKU_FROM_LIST_DONT_ASK = 'KEY_REMOVE_SKU_FROM_LIST_DONT_ASK';
const KEY_ADDED_SKU = 'KEY_ADDED_SKU';
const maxiumFile = 7 * 1000 * 1000; // 25MB
const dataSave = localStorage.getItem(KEY_ADDED_SKU);
if (dataSave) {
  try {
    productSkuAdded = JSON.parse(dataSave);
  } catch (e) {
    console.log(e);
  }
}
// var collapseItem = localStorage.getItem('collapseItem');

function centerModal() {
  $(this).css('display', 'block');
  const $dialog = $(this).find('.modal-dialog');
  const offset = ($(window).height() - $dialog.height()) / 2;
  // Center modal vertically in window
  $dialog.css('margin-top', offset);
}

/* eslint-disable no-trailing-spaces */
$(document).ready(() => {
  let productSku = [];
  if ($('#productSku').val()) {
    productSku = JSON.parse($('#productSku').val());
  }

  if ($('#productSkuAdded').val()) {
    try {
      const itemAdded = JSON.parse($('#productSkuAdded').val());
      if (itemAdded && itemAdded.sku) {
        const found = productSkuAdded.find((itm) => {
          return itm.sku === itemAdded.sku;
        });
        if (!found) {
          newItem = {
            id: itemAdded.sku,
            sku: itemAdded.sku,
            total: parseInt(itemAdded.total),
            pending: parseInt(itemAdded.pending),
            send: parseInt(itemAdded.send),
            completed: parseInt(itemAdded.completed),
            empty: 0,
          };
          productSkuAdded.push(newItem);
          localStorage.setItem(KEY_ADDED_SKU, JSON.stringify(productSkuAdded));
          window.location = '/admin/qrcode/nfc-udid-request';
        }
      }
    } catch (e) {
      console.log(e);
    }
  }
  if ($('#stockx-productTable')) {
    try {
      $('#stockx-productTable').DataTable({
        columnDefs: [
          {
            searchable: false,
            orderable: false,
            targets: 0,
          },
        ],
        order: [[3, 'desc']],
      });
    } catch (e) {
      console.log(e);
    }
  }

  setupMenuBehavior();

  regenerateTable();

  $(() => {
    if ($('#datetimepicker1').length > 0) {
      $('#datetimepicker1').datetimepicker();
      $('#scheduleTime').val($('#scheduleTimeDefault').val());
    }
  });
  // setup if material theme
  if ($('#cartTheme').val() === 'Material') {
    $('.materialboxed').materialbox();
  }

  if ($(window).width() < 768) {
    $('.menu-side').on('click', (e) => {
      e.preventDefault();
      $('.menu-side li:not(".active")').slideToggle();
    });

    $('.menu-side li:not(".active")').hide();
    $('.menu-side>.active').html(
      '<i class="fa fa-bars" aria-hidden="true"></i>',
    );
    $('.menu-side>.active').addClass('menu-side-mobile');

    // hide menu if there are no items in it
    if ($('#navbar ul li').length === 0) {
      $('#navbar').hide();
    }

    $('#offcanvasClose').hide();
  }

  $('.shipping-form input').each(function (e) {
    $(this).wrap('<fieldset></fieldset>');
    const tag = $(this).attr('placeholder');
    $(this).after('<label for="name" class="hidden">' + tag + '</label>');
  });

  $('.shipping-form input').on('focus', function () {
    $(this).next().addClass('floatLabel');
    $(this).next().removeClass('hidden');
  });

  $('.flagged-click-toreview').click(function () {
    const indexItem = $('tr.flagged-click-toreview').index(this);
    const thisItem = document.getElementsByClassName('flagged-click-toreview')[
      indexItem
    ];
    const id = thisItem.cells[5].innerText;
    $.ajax({
      method: 'GET',
      url: `/admin/flagged-review?id=${id}`,
    })
      .done((msg) => {
        window.location = `/admin/flagged-review?id=${id}`;
      })
      .fail((msg) => {
        showNotification(msg.responseJSON.message, 'danger');
      });
  });
  $('.shipping-form input').on('blur', function () {
    if ($(this).val() === '') {
      $(this).next().addClass('hidden');
      $(this).next().removeClass('floatLabel');
    }
  });

  $('.menu-btn').on('click', (e) => {
    e.preventDefault();
  });

  $('#sendTestEmail').on('click', (e) => {
    e.preventDefault();
    $.ajax({
      method: 'POST',
      url: '/admin/testEmail',
    })
      .done((msg) => {
        showNotification(msg, 'success');
      })
      .fail((msg) => {
        showNotification(msg.responseJSON.message, 'danger');
      });
  });

  $('#attachedSku').keyup(() => {
    const keyword = $('#attachedSku').val();
    const foundSku = productSku.filter((itm) => {
      return (
        itm.productVariantSku.toLowerCase().indexOf(keyword.toLowerCase()) > -1
      );
    });
    if (foundSku && keyword.length > 0) {
      regenerateSearchTable(foundSku);
    } else {
      $('#resultSearch').html('');
    }
  });

  $('th i.fa.fa-angle-down').on('click', function () {
    $('.dropdown-status').toggleClass('open');
    console.log('click');
  });

  $('#btn-add-sku').on('click', (e) => {});

  if ($('#footerHtml').length) {
    const footerHTML = window.CodeMirror.fromTextArea(
      document.getElementById('footerHtml'),
      {
        mode: 'xml',
        tabMode: 'indent',
        theme: 'flatly',
        lineNumbers: true,
        htmlMode: true,
        fixedGutter: false,
      },
    );

    footerHTML.setValue(footerHTML.getValue());
  }

  if ($('#googleAnalytics').length) {
    window.CodeMirror.fromTextArea(document.getElementById('googleAnalytics'), {
      mode: 'xml',
      tabMode: 'indent',
      theme: 'flatly',
      lineNumbers: true,
      htmlMode: true,
      fixedGutter: false,
    });
  }

  if ($('#customCss').length) {
    const customCss = window.CodeMirror.fromTextArea(
      document.getElementById('customCss'),
      {
        mode: 'text/css',
        tabMode: 'indent',
        theme: 'flatly',
        lineNumbers: true,
      },
    );

    const customCssBeautified = window.cssbeautify(customCss.getValue(), {
      indent: '   ',
      autosemicolon: true,
    });
    customCss.setValue(customCssBeautified);
  }

  // add the table class to all tables
  $('table').each(function () {
    $(this).addClass('table table-hover');
  });

  $(document)
    .on('click', '.dashboard_list', function (e) {
      window.document.location = $(this).attr('href');
    })
    .hover(function () {
      $(this).toggleClass('hover');
    });

  // Call to API for a change to the published state of a product
  $('input[class="published_state"]').change(function () {
    $.ajax({
      method: 'POST',
      url: '/admin/product/published_state',
      data: { id: this.id, state: this.checked },
    })
      .done((msg) => {
        showNotification(msg.message, 'success');
      })
      .fail((msg) => {
        showNotification(msg.responseJSON.message, 'danger');
      });
  });

  $(document).on('click', '.btn-qty-minus', (e) => {
    const qtyElement = $(e.target)
      .parent()
      .parent()
      .find('.cart-product-quantity');
    $(qtyElement).val(parseInt(qtyElement.val()) - 1);
    cartUpdate(qtyElement);
  });

  $(document).on('click', '.btn-qty-add', (e) => {
    const qtyElement = $(e.target)
      .parent()
      .parent()
      .find('.cart-product-quantity');
    $(qtyElement).val(parseInt(qtyElement.val()) + 1);
    cartUpdate(qtyElement);
  });

  $(document).on('change', '.cart-product-quantity', (e) => {
    cartUpdate(e.target);
  });

  $(document).on('click', '.btn-delete-from-cart', (e) => {
    deleteFromCart($(e.target));
  });

  if ($('#pager').length) {
    const pageNum = $('#pageNum').val();
    const pageLen = $('#productsPerPage').val();
    const productCount = $('#totalProductCount').val();
    const paginateUrl = $('#paginateUrl').val();
    let searchTerm = $('#searchTerm').val();

    if (searchTerm !== '') {
      searchTerm = searchTerm + '/';
    }

    const pagerHref = '/' + paginateUrl + '/' + searchTerm + '{{number}}';
    const totalProducts = Math.ceil(productCount / pageLen);

    if (parseInt(productCount) > parseInt(pageLen)) {
      $('#pager').bootpag({
        total: totalProducts,
        page: pageNum,
        maxVisible: 5,
        href: pagerHref,
        wrapClass: 'pagination',
        prevClass: 'waves-effect',
        nextClass: 'waves-effect',
        activeClass: 'pag-active waves-effect',
      });
    }
  }

  $(document).on('click', '#btnPageUpdate', (e) => {
    e.preventDefault();
    $.ajax({
      method: 'POST',
      url: '/admin/settings/pages/update',
      data: {
        page_id: $('#page_id').val(),
        pageName: $('#pageName').val(),
        pageSlug: $('#pageSlug').val(),
        pageEnabled: $('#pageEnabled').is(':checked'),
        pageContent: $('#pageContent').val(),
      },
    })
      .done((msg) => {
        showNotification(msg.message, 'success', true);
      })
      .fail((msg) => {
        showNotification(msg.responseJSON.message, 'danger');
      });
  });

  $(document).on('click', '.product_opt_remove', function (e) {
    e.preventDefault();
    const name = $(this).closest('li').find('.opt-name').html();

    $.ajax({
      method: 'POST',
      url: '/admin/settings/option/remove/',
      data: { productId: $('#frmProductId').val(), optName: name },
    })
      .done((msg) => {
        showNotification(msg.message, 'success', true);
      })
      .fail((msg) => {
        showNotification(msg.responseJSON.message, 'danger');
      });
  });

  $(document).on('click', '#product_opt_add', (e) => {
    e.preventDefault();

    const optName = $('#product_optName').val();
    const optLabel = $('#product_optLabel').val();
    const optType = $('#product_optType').val();
    const optOptions = $('#product_optOptions').val();

    let optJson = {};
    if ($('#productOptJson').val() !== '') {
      optJson = JSON.parse($('#productOptJson').val());
    }

    let html = '<li class="list-group-item">';
    html += '<div class="row">';
    html += '<div class="col-lg-2 opt-name">' + optName + '</div>';
    html += '<div class="col-lg-2">' + optLabel + '</div>';
    html += '<div class="col-lg-2">' + optType + '</div>';
    html += '<div class="col-lg-4">' + optOptions + '</div>';
    html += '<div class="col-lg-2 text-right">';
    html +=
      '<button class="product_opt_remove btn btn-danger btn-sm">Remove</button>';
    html += '</div></div></li>';

    // append data
    $('#product_opt_wrapper').append(html);

    // add to the stored json string
    optJson[optName] = {
      optName: optName,
      optLabel: optLabel,
      optType: optType,
      optOptions: $.grep(optOptions.split(','), (n) => {
        return n === 0 || n;
      }),
    };

    // write new json back to field
    $('#productOptJson').val(JSON.stringify(optJson));

    // clear inputs
    $('#product_optName').val('');
    $('#product_optLabel').val('');
    $('#product_optOptions').val('');
  });

  $('#customerLogout').on('click', (e) => {
    $.ajax({
      method: 'POST',
      url: '/customer/logout',
      data: {},
    }).done((msg) => {
      location.reload();
    });
  });

  $('#loginForm').on('click', (e) => {
    // console.log('fdsafsdf');
    if (!e.isDefaultPrevented()) {
      e.preventDefault();
      $.ajax({
        method: 'POST',
        url: '/admin/login_action',
        data: {
          email: $('#email').val(),
          password: $('#password').val(),
          token: $('#token').val(),
        },
      })
        .done((msg) => {
          window.location = '/admin';
        })
        .fail((msg) => {
          showNotification(msg.responseJSON.message, 'danger');
        });
    }
    e.preventDefault();
  });

  // call update settings API
  $('#customerLogin').on('click', (e) => {
    if (!e.isDefaultPrevented()) {
      e.preventDefault();
      $.ajax({
        method: 'POST',
        url: '/customer/login_action',
        data: {
          loginEmail: $('#customerLoginEmail').val(),
          loginPassword: $('#customerLoginPassword').val(),
        },
      })

        .done((msg) => {
          const customer = msg.customer;
          // Fill in customer form
          $('#shipEmail').val(customer.email);
          $('#shipFirstname').val(customer.firstName);
          $('#shipLastname').val(customer.lastName);
          $('#shipAddr1').val(customer.address1);
          $('#shipAddr2').val(customer.address2);
          $('#shipCountry').val(customer.country);
          $('#shipState').val(customer.state);
          $('#shipPostcode').val(customer.postcode);
          $('#shipPhoneNumber').val(customer.phone);
          location.reload();
        })
        .fail((msg) => {
          showNotification(msg.responseJSON.message, 'danger');
        });
    }
    e.preventDefault();
  });

  $(document).on('click', '.image-next', (e) => {
    const thumbnails = $('.thumbnail-image');
    let index = 0;
    let matchedIndex = 0;

    // get the current src image and go to the next one
    $('.thumbnail-image').each(function () {
      if ($('#product-title-image').attr('src') === $(this).attr('src')) {
        if (index + 1 === thumbnails.length || index + 1 < 0) {
          matchedIndex = 0;
        } else {
          matchedIndex = index + 1;
        }
      }
      index++;
    });

    // set the image src
    $('#product-title-image').attr(
      'src',
      $(thumbnails).eq(matchedIndex).attr('src'),
    );
  });

  $(document).on('click', '.image-prev', (e) => {
    const thumbnails = $('.thumbnail-image');
    let index = 0;
    let matchedIndex = 0;

    // get the current src image and go to the next one
    $('.thumbnail-image').each(function () {
      if ($('#product-title-image').attr('src') === $(this).attr('src')) {
        if (index - 1 === thumbnails.length || index - 1 < 0) {
          matchedIndex = thumbnails.length - 1;
        } else {
          matchedIndex = index - 1;
        }
      }
      index++;
    });

    // set the image src
    $('#product-title-image').attr(
      'src',
      $(thumbnails).eq(matchedIndex).attr('src'),
    );
  });

  $(document).on('click', '#orderStatusUpdate', (e) => {
    $.ajax({
      method: 'POST',
      url: '/admin/order/statusupdate',
      data: { order_id: $('#order_id').val(), status: $('#orderStatus').val() },
    })
      .done((msg) => {
        showNotification(msg.message, 'success', true);
      })
      .fail((msg) => {
        showNotification(msg.responseJSON.message, 'danger');
      });
  });

  $(document).on('click', '#addmodelNumber', (e) => {
    const dialog = bootbox.dialog({
      title: 'Enter Model Number & UDID',
      message:
        '<input type="text" placeholder="UDID" id="txt-udid" class="bootbox-input bootbox-input-email form-control"></input><br/> <input type="text" placeholder="Model Number" id="txt-model" class="bootbox-input bootbox-input-email form-control"></input>',
      size: 'large',
      buttons: {
        cancel: {
          label: 'Cancel',
          className: 'btn-danger',
          callback: function () {},
        },
        ok: {
          label: 'OK',
          className: 'btn-info',

          callback: function () {
            const udid = $('#txt-udid').val();
            const model = $('#txt-model').val();

            $.ajax({
              method: 'POST',
              url: '/admin/order/update-model',
              data: {
                order_id: $('#order_id').val(),
                udid: udid,
                model_name: model,
              },
            })
              .done((msg) => {
                showNotification(msg.message, 'success', true);
              })
              .fail((msg) => {
                showNotification(msg.responseJSON.message, 'danger');
              });
          },
        },
      },
    });
  });

  $(document).on('click', '.product-add-to-cart', (e) => {
    const productOptions = getSelectedOptions();

    if (parseInt($('#product_quantity').val()) < 0) {
      $('#product_quantity').val(0);
    }

    $.ajax({
      method: 'POST',
      url: '/product/addtocart',
      data: {
        productId: $('#productId').val(),
        productQuantity: $('#product_quantity').val(),
        productOptions: JSON.stringify(productOptions),
        productComment: $('#product_comment').val(),
      },
    })
      .done((msg) => {
        $('#cart-count').text(msg.totalCartItems);
        updateCartDiv();
        showNotification(msg.message, 'success');
      })
      .fail((msg) => {
        showNotification(msg.responseJSON.message, 'danger');
      });
  });

  $('.cart-product-quantity').on('input', () => {
    cartUpdate();
  });

  $(document).on('click', '.pushy-link', (e) => {
    $('body').removeClass('pushy-open-right');
  });

  $(document).on('click', '.add-to-cart', function (e) {
    let productLink = '/product/' + $(this).attr('data-id');
    if ($(this).attr('data-link')) {
      productLink = '/product/' + $(this).attr('data-link');
    }

    if ($(this).attr('data-has-options') === 'true') {
      window.location = productLink;
    } else {
      $.ajax({
        method: 'POST',
        url: '/product/addtocart',
        data: { productId: $(this).attr('data-id') },
      })
        .done((msg) => {
          $('#cart-count').text(msg.totalCartItems);
          updateCartDiv();
          showNotification(msg.message, 'success');
        })
        .fail((msg) => {
          showNotification(msg.responseJSON.message, 'danger');
        });
    }
  });

  $(document).on('click', '#empty-cart', (e) => {
    $.ajax({
      method: 'POST',
      url: '/product/emptycart',
    }).done((msg) => {
      $('#cart-count').text(msg.totalCartItems);
      updateCartDiv();
      showNotification(msg.message, 'success', true);
    });
  });

  $('.qty-btn-minus').on('click', () => {
    const number = parseInt($('#product_quantity').val()) - 1;
    $('#product_quantity').val(number > 0 ? number : 1);
  });

  $('.qty-btn-plus').on('click', () => {
    $('#product_quantity').val(parseInt($('#product_quantity').val()) + 1);
  });

  // product thumbnail image click
  $('.thumbnail-image').on('click', function () {
    $('#product-title-image').attr('src', $(this).attr('src'));
  });

  $('.set-as-main-image').on('click', function () {
    $.ajax({
      method: 'POST',
      url: '/admin/product/setasmainimage',
      data: {
        product_id: $('#frmProductId').val(),
        productImage: $(this).attr('data-id'),
      },
    })
      .done((msg) => {
        showNotification(msg.message, 'success', true);
      })
      .fail((msg) => {
        showNotification(msg.responseJSON.message, 'danger');
      });
  });

  // Call to API to check if a permalink is available
  $(document).on('click', '#validate_permalink', (e) => {
    if ($('#frmProductPermalink').val() !== '') {
      $.ajax({
        method: 'POST',
        url: '/admin/api/validate_permalink',
        data: {
          permalink: $('#frmProductPermalink').val(),
          docId: $('#frmProductId').val(),
        },
      })
        .done((msg) => {
          showNotification(msg, 'success');
        })
        .fail((msg) => {
          showNotification(msg.responseJSON.message, 'danger');
        });
    } else {
      showNotification('Please enter a permalink to validate', 'danger');
    }
  });

  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    customClass: 'toast-import',
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    },
  });

  $('.import-file').on('change', function (e) {
    // Show loading when import file
    Swal.fire({
      title: 'Processing .......',
      customClass: 'sweetalert-fontsize',
      didOpen: () => {
        Swal.showLoading();
      },
    });

    const indexItem = $('input.import-file').index(this);
    const myFile =
      document.getElementsByClassName('import-file')[indexItem].files;
    const url = document.getElementsByClassName('btn-file')[indexItem].action;
    const method = $('.btn-file').attr('method');
    const formData = new FormData();
    formData.append('myfile', myFile[0], myFile[0].name);

    $.ajax({
      url: url,
      type: method,
      data: formData,
      processData: false,
      contentType: false,
    })
      .done((res) => {
        Toast.fire({
          icon: 'success',
          title: res.message,
        }).then(() => location.reload());
      })
      .fail((res) => {
        Swal.fire({
          icon: 'error',
          customClass: 'sweetalert-fontsize',
          title: 'Import failed',
          html: res.responseJSON.message,
        }).then(() => location.reload());
      });
  });

  $('#product_filter').keypress((e) => {
    const key = e.which;
    if (key == 13) {
      // the enter key code
      if ($('#product_filter').val() !== '') {
        window.location.href =
          '/admin/products/filter/' +
          encodeURIComponent($('#product_filter').val());
      } else {
        showNotification('Please enter a keyword to filter', 'danger');
      }
    }
  });

  $('#artist_filter').keypress((e) => {
    const key = e.which;
    if (key == 13) {
      // the enter key code
      if ($('#artist_filter').val() !== '') {
        window.location.href =
          '/admin/artist/filter?search=' + $('#artist_filter').val();
      } else {
        window.location.href = '/admin/artists/0';
      }
    }
  });

  $('#certificate_keyword').keypress((e) => {
    const key = e.which;
    if (key == 13) {
      const keyword = $('#certificate_keyword').val();
      const filterType = $('#filter_type').val();
      const sortType = $('#sort_type').val();
      window.location.href =
        '/admin/certificates?keyword=' +
        keyword +
        '&filterType=' +
        filterType +
        '&sortType=' +
        sortType;
    }
  });

  // applies an product filter
  $(document).on('click', '#btn_product_filter', (e) => {
    if ($('#product_filter').val() !== '') {
      window.location.href =
        '/admin/products/filter/' +
        encodeURIComponent($('#product_filter').val());
    } else {
      showNotification('Please enter a keyword to filter', 'danger');
    }
  });

  // applies an artist filter
  $(document).on('click', '#btn_artist_filter', (e) => {
    if ($('#artist_filter').val() !== '') {
      window.location.href =
        '/admin/artist/filter?search=' + $('#artist_filter').val();
    } else {
      showNotification('Please enter a keyword to filter', 'danger');
    }
  });

  // applies an product filter
  $(document).on('click', '#btn_qrcode_filter', (e) => {
    if ($('#qrcode_filter').val() !== '') {
      const productId = $('#product_id').val();

      window.location.href =
        '/admin/qrcode/filter/' + productId + '/' + $('#qrcode_filter').val();
    } else {
      showNotification('Please enter a keyword to filter', 'danger');
    }
  });

  // applies an order filter
  $('#sort_type').bind('change', function () {
    const keyword = $('#certificate_keyword').val();
    const filterType = $('#filter_type').val();
    const sortType = $('#sort_type').val();
    window.location.href =
      '/admin/certificates?keyword=' +
      keyword +
      '&filterType=' +
      filterType +
      '&sortType=' +
      sortType;
  });

  // applies an product filter
  $(document).on('click', '#btn_customer_filter', (e) => {
    if ($('#customer_filter').val() !== '') {
      window.location.href =
        '/admin/customers/filter/' + $('#customer_filter').val();
    } else {
      showNotification('Please enter a keyword to filter', 'danger');
    }
  });

  // resets the order filter
  $(document).on('click', '#btn_search_reset', (e) => {
    window.location.replace('/');
  });

  // resets the order filter
  $(document).on('click', '#btn_verify', (e) => {
    e.preventDefault();
    if ($('#frm-smart-contract').val().trim() === '') {
      showNotification('Please enter contract address', 'danger');
    } else {
      window.location.href = '/verify/' + $('#frm-smart-contract').val();
    }
  });

  // search button click event
  $(document).on('click', '#btn_search', (e) => {
    e.preventDefault();
    if ($('#frm_search').val().trim() === '') {
      showNotification('Please enter a search value', 'danger');
    } else {
      window.location.href = '/search/' + $('#frm_search').val();
    }
  });

  // create a permalink from the product title if no permalink has already been set
  $(document).on('click', '#frm_edit_product_save', (e) => {
    debugger;
    if (
      $('#frmProductPermalink').val() === '' &&
      $('#frmProductTitle').val() !== ''
    ) {
      $('#frmProductPermalink').val(slugify($('#frmProductTitle').val()));
    }
  });

  if ($('#input_notify_message').val() !== '') {
    // save values from inputs
    const messageVal = $('#input_notify_message').val();
    const messageTypeVal = $('#input_notify_messageType').val();

    // clear inputs
    $('#input_notify_message').val('');
    $('#input_notify_messageType').val('');

    // alert
    showNotification(messageVal, messageTypeVal, false);
  }

  // submit form
  $('#btn-create-qrcode').on('click', function () {
    const $this = $(this);
    const numberValue = $('#qrcode-number').val();
    if (numberValue < 0) {
      bootbox.alert('Please enter number');
    }
    $('#btn-create-qrcode')
      .html(
        '<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Processing...',
      )
      .addClass('disabled');
    // $this.button('loading');
    $('#qr_code_edit_form').submit();
  });

  // submit form
  $('#btn-download-csv').on('click', () => {
    const qrcodeLength = $('#qrcode_length').val();
    const productId = $('#product_id').val();
    console.log(productId);
    console.log(qrcodeLength);
    if (qrcodeLength === 0) {
      bootbox.alert('There is not qrcode!!!');
      return;
    }
    if (qrcodeLength <= 50000) {
      window.open(
        '/admin/qrcode/export-qrcode-csv/' + productId + '/0' + '/50000',
        '_blank',
      );
    } else {
      let html = '<p id="message_error" class="bg-danger" ></p>';
      html += '<p id="message"> Please enter from and to</p> ';
      html +=
        '<input class="bootbox-input bootbox-input-email form-control" autocomplete="off" placeholder="0" type="number" id ="from_number" ><br/>';
      html +=
        '<input class="bootbox-input bootbox-input-email form-control" autocomplete="on" placeholder="50000" type="number" id ="to_number">';

      const dialog = bootbox.dialog({
        title:
          'Limitation is 50000 items. please enter the range of item that you want to export',
        message: html,
        closeButton: false,
        buttons: {
          cancel: {
            label: '<i class="fa fa-times"></i> Cancel',
            className: 'btn-default',
            callback: () => {},
          },
          OK: {
            label: '<i class="fa fa-check"></i> Confirm',
            className: 'btn-success',

            callback: function () {
              const fromNumber = $('#from_number').val();
              const toNumber = $('#to_number').val();
              if (fromNumber < 0) {
                bootbox.alert('Please enter from');
                return false;
              }
              if (toNumber === 0) {
                bootbox.alert('Please enter to');
                return false;
              }
              window.open(
                '/admin/qrcode/export-qrcode-csv/' +
                  productId +
                  '/' +
                  fromNumber +
                  '/' +
                  toNumber,
                '_blank',
              );
            },
          },
        },
      });
    }
  });

  $('#slection-page').bind('change', function () {
    // bind change event to select
    const number = $(this).val(); // get selected value
    const productId = $('#product_id').val();

    window.location.href = '/admin/qrcode/view/' + productId + '/' + number;
    return true;
  });

  $('#slection-page-product').bind('change', function () {
    // bind change event to select
    const number = $(this).val(); // get selected value
    window.location.href = '/admin/products/' + number;
    return true;
  });

  $('#slection-page-artist').bind('change', function () {
    // bind change event to select
    const number = $(this).val(); // get selected value
    window.location.href = '/admin/artists/' + number;
    return true;
  });

  $('#selection-page-f1-races').bind('change', function () {
    // bind change event to select
    const number = $(this).val(); // get selected value
    window.location.href = '/admin/f1/races/' + number;
    return true;
  });

  $('#slection-page-stockx-report').bind('change', function () {
    // bind change event to select
    const number = $(this).val(); // get selected value
    window.location.href = '/admin/analytics/stockx?id=' + number;
    return true;
  });

  $('#slection-page-order').bind('change', function () {
    // bind change event to select
    const number = $(this).val(); // get selected value

    window.location.href = '/admin/orders/' + number;
    return true;
  });

  // submit form

  $('#submit-csv-file').on('click', function () {
    const $this = $(this);
    $('#submit-csv-file')
      .html(
        '<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Processing...',
      )
      .addClass('disabled');
    // $this.button('loading');
    $('#qrcode-import-csv').submit();
  });

  $('.accordion').on('click', function () {
    rotateMenuArrowAngle($(this));
  });

  keepMenuOpen();

  // show file name when admin chose the file
  $('#upload_file').change(function () {
    const file = $('#upload_file')[0].files[0].name;
    $('#file_name').text(file);
  });

  $('#upload_product_file').change(function () {
    const file = $('#upload_product_file')[0].files[0].name;
    $('#file_product_name').text(file);
  });

  $('#myModalBackground').on('hidden.bs.modal', function () {
    $('#file_name').text('');
    $('.modal-backdrop').hide();
  });

  $('#myModalProduct').on('hidden.bs.modal', function () {
    $('#file_product_name').text('');
    $('.modal-backdrop').hide();
  });
});

function deleteDataForTableAssetsFile(productId, fileId) {
  debugger;
  var confirm = window.confirm('Are you sure delete?');
  if (confirm) {
    var _$imageUploadAssetsFile = $('#assetsImageData');
    var imageTable = _$imageUploadAssetsFile.DataTable();
    var item = imageTable.row($('#delete_' + fileId).parents('tr')).data();
    var indexRow = imageTable.row($('#delete_' + fileId).parents('tr')).index();
    $.ajax({
      url: '/admin/deleteDataForGoodies',
      //enctype: 'application/json',
      processData: false, // Important!
      contentType: 'application/json',
      cache: false,
      data: JSON.stringify({
        id: productId,
        fileId: fileId,
      }),
      type: 'post',
      success: function (response) {
        imageTable.row(indexRow).remove();
        imageTable.draw();
        showNotification(response.message, 'success', false);
        var dataOfTable = [];
        var data = imageTable.rows().data();
        for (var i = 0; i < data.length; i++) {
          dataOfTable.push(data[i]);
        }
        $('input[name=imageAddingData]').val(JSON.stringify(dataOfTable));
      },
      error: function (response) {
        debugger;
        showNotification(response.responseJSON.message, 'danger');
      },
    });
  }
}

function checkAllowDownload(fileId) {
  debugger;
  var _$imageUploadAssetsFile = $('#assetsImageData');
  var imageTable = _$imageUploadAssetsFile.DataTable();
  var check = $('#allowDownload_' + fileId).is(':checked');
  var _$imageUploadAssetsFile = $('#assetsImageData');
  var item = imageTable.row($('#allowDownload_' + fileId).parents('tr')).data();
  var indexRow = imageTable
    .row($('#allowDownload_' + fileId).parents('tr'))
    .index();
  var imageTable = _$imageUploadAssetsFile.DataTable();
  var dataOfTable = [];
  var data = imageTable.rows().data();
  for (var i = 0; i < data.length; i++) {
    if (data[i].fileId == fileId) {
      data[i].allowDownload = check;
    }
    dataOfTable.push(data[i]);
  }
  $('input[name=imageAddingData]').val(JSON.stringify(dataOfTable));
}

function getDataForTableAssetsFile(productId) {
  debugger;
  let imageData = [];
  let _$imageUploadAssetsFile = $('#assetsImageData');
  let imageTable = _$imageUploadAssetsFile.DataTable({
    paging: false,
    serverSide: false,
    retrieve: false,
    data: imageData,
    columnDefs: [
      {
        targets: 0,
        searchable: false,
        responsive: true,
        orderable: false,
        data: null,
        render: function (data, type, row) {
          if (type === 'display' && data.assetsFileName != null) {
            debugger;
            data.assetsFileName = data.assetsFileName.replace(
              /<(?:.|\\n)*?>/gm,
              '',
            );
            if (data.assetsFileName.length > 20) {
              return (
                '<span class="show-ellipsis" title="' +
                data.assetsFileName +
                '">' +
                data.assetsFileName.substr(0, 20) +
                '</span>'
              );
            } else {
              return data.assetsFileName;
            }
          } else {
            return data.assetsFileName;
          }
        },
      },
      {
        targets: 1,
        searchable: false,
        orderable: false,
        data: null,
        render: function (data) {
          if (data.allowDownload) {
            return (
              '<input onclick="checkAllowDownload(\'' +
              data.fileId +
              '\')" id="allowDownload_' +
              data.fileId +
              '" type="checkbox" name="" value="' +
              data.allowDownload +
              '" checked>'
            );
          } else {
            return (
              '<input onclick="checkAllowDownload(\'' +
              data.fileId +
              '\')" id="allowDownload_' +
              data.fileId +
              '" type="checkbox" name="" value="' +
              data.allowDownload +
              '">'
            );
          }
        },
      },
      {
        targets: 2,
        searchable: false,
        orderable: false,
        data: null,
        render: function (data) {
          data.productId = productId;
          return (
            '<button type="button" onclick="deleteDataForTableAssetsFile(\'' +
            productId +
            "','" +
            data.fileId +
            '\')" id="delete_' +
            data.fileId +
            '" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill remove" title="' +
            'Delete' +
            '"><i class="fa fa-trash"></i></button>'
          );
        },
        className: 'text-center',
      },
      {
        targets: 3,
        visible: false,
        data: 'assetsFileThumbUrl',
      },
    ],
  });
  $.ajax({
    url: '/admin/listDataForGoodies/' + productId,
    enctype: 'application/json',
    processData: false, // Important!
    contentType: false,
    cache: false,
    type: 'get',
    success: function (response) {
      debugger;
      _$imageUploadAssetsFile = $('#assetsImageData');
      imageTable = _$imageUploadAssetsFile.DataTable();
      if (response.data.productAssetsFile) {
        for (let i = 0; i < response.data.productAssetsFile.length; i++) {
          imageData.push(response.data.productAssetsFile[i]);
        }
        imageTable.clear();
        imageTable.rows.add(imageData);
        imageTable.draw();
        $('.image-list-result').show();
        $('#assetsImageData_filter').hide();
        $('#assetsImageData_info').hide();
        var dataOfTable = [];
        var data = imageTable.rows().data();
        for (var i = 0; i < data.length; i++) {
          dataOfTable.push(data[i]);
        }
        $('input[name=imageAddingData]').val(JSON.stringify(dataOfTable));
      }
    },
    error: function (response) {
      debugger;
      showNotification(response.responseJSON.message, 'danger');
    },
  });
}

function removeFileUpload(fileId, imageData) {
  debugger;
  let imageUploadAdding = JSON.parse($('input[name*=imageAdding]').val());
  let index = imageUploadAdding.findIndex(
    (x) => x.idName === 'cancel_' + fileId,
  );
  imageUploadAdding.splice(index, 1);
  imageData.splice(index, 1);
  let btnClick = $('#cancel_' + fileId);
  if (imageData.length == 0) {
    $('#saveAssetsButton').hide();
    $('#divFiles').html('');
    imageData = [];
  } else {
    $('#cancel_' + fileId)
      .parent()
      .parent()
      .remove();
    $('div#notify_' + fileId).remove();
  }
  $('.file-upload-input').val('');
  $('input[name*=imageAdding]').val(JSON.stringify(imageUploadAdding));
  return imageData;
}

async function uploadAssetsFilesToServer(imageDataAsset) {
  debugger;
  let list_filedata = null;
  let arrayData = [];
  let arrayThumb = [];
  list_filedata = JSON.parse($('input[name*=imageAdding]').val());
  for (let i = 0; i < imageDataAsset.length; i++) {
    debugger;
    arrayData.push(imageDataAsset[i]);
    if (imageDataAsset[i].type == 'video/mp4') {
      let videoThumbBlob = await createAssetVideoThumbnail(imageDataAsset[i]);
      let videoThumbName =
        imageDataAsset[i].name.substring(
          0,
          imageDataAsset[i].name.lastIndexOf('.'),
        ) + '_thumb.jpg';
      let videoThumbFile = new File([videoThumbBlob], videoThumbName);
      arrayThumb.push(videoThumbFile);
    }
  }
  const product_id = $('#frmProductId').val();
  const form_data = new FormData();
  for (let i = 0; i < arrayData.length; i++) {
    form_data.append('upload_file', arrayData[i]);
  }
  for (let i = 0; i < arrayThumb.length; i++) {
    form_data.append('upload_thumb_file', arrayThumb[i]);
  }
  form_data.append('productId', product_id);
  form_data.append('upload_type', 'assetsFiles');
  let imageData = [];
  let _$imageUploadAssetsFile = $('#assetsImageData');
  let imageTable = _$imageUploadAssetsFile.DataTable();
  $('#saveAssetsButton').button('loading');
  $.ajax({
    url: '/admin/files/multiUpload',
    enctype: 'multipart/form-data',
    processData: false, // Important!
    contentType: false,
    cache: false,
    data: form_data,
    type: 'post',
    success: function (response) {
      debugger;
      $('#saveAssetsButton').button('reset');
      $.ajax({
        url: '/admin/listDataForGoodies/' + product_id,
        enctype: 'application/json',
        processData: false, // Important!
        contentType: false,
        cache: false,
        type: 'get',
        success: function (response) {
          debugger;
          if (response.data.productAssetsFile) {
            for (let i = 0; i < response.data.productAssetsFile.length; i++) {
              imageData.push(response.data.productAssetsFile[i]);
            }
            imageTable.clear();
            imageTable.rows.add(imageData);
            imageTable.draw();
            var dataOfTable = [];
            var data = imageTable.rows().data();
            for (var i = 0; i < data.length; i++) {
              dataOfTable.push(data[i]);
            }
            $('input[name=imageAddingData]').val(JSON.stringify(dataOfTable));
          }
        },
        error: function (response) {
          debugger;
          showNotification(response.responseJSON.message, 'danger');
        },
      });
      $('#saveAssetsButton').hide();
      $('#divFiles').html('');
      $('.file-upload-input').val('');
      $('input[name*=imageAdding]').val('');
      showNotification(response.message, 'success', false);
    },
    error: function (response) {
      debugger;
      $('#saveAssetsButton').button('reset');
      showNotification(response.responseJSON.message, 'danger');
    },
  });
}

function createAssetVideoThumbnail(file, seekTo = 0.0) {
  console.log('getting video cover for file: ', file);
  return new Promise((resolve) => {
    const canvas = document.createElement('canvas');
    const video = document.createElement('video');

    // this is important
    video.autoplay = true;
    video.muted = true;
    video.src = URL.createObjectURL(file);

    video.onloadeddata = () => {
      let ctx = canvas.getContext('2d');

      canvas.width = video.videoWidth;
      canvas.height = video.videoHeight;

      ctx.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
      video.pause();
      canvas.toBlob(
        (blob) => {
          resolve(blob);
          video.remove();
          canvas.remove();
        },
        'image/jpg',
        0.75,
      );
      //return resolve(canvas.toBlob("image/png"));
    };
  });
}

function uploadImageFileToServer(uploadType) {
  let file_data = null;
  if (uploadType === 'product') {
    file_data = $('#upload_product_file').prop('files')[0];
  } else {
    file_data = $('#upload_file').prop('files')[0];
  }
  const product_id = $('#frmProductId').val();
  const form_data = new FormData();
  form_data.append('upload_file', file_data);
  form_data.append('productId', product_id);
  form_data.append('upload_type', uploadType);
  $('#saveBackgroundButton').button('loading');
  $('#saveProductImageButton').button('loading');
  $.ajax({
    url: '/admin/file/upload',
    enctype: 'multipart/form-data',
    processData: false, // Important!
    contentType: false,
    cache: false,
    data: form_data,
    type: 'post',
    success: function (response) {
      $('#myModalBackground').modal('hide');
      $('#myModalProduct').modal('hide');
      $('.modal-backdrop').hide();
      $('#saveBackgroundButton').button('reset');
      $('#saveProductImageButton').button('reset');
      if (uploadType === 'backgroundImage') {
        $('#productBackgroundImage').attr('src', response.data.imageUrl);
      } else {
        $('#productImage').attr('src', response.data.imageUrl);
      }
      showNotification(response.message, 'success', false);
    },
    error: function (response) {
      $('#saveBackgroundButton').button('reset');
      $('#saveProductImageButton').button('reset');
      showNotification(response.responseJSON.message, 'danger');
    },
  });
}

function keepMenuOpen() {
  const collapseItem = $('#selected-menu').val();
  $('#' + collapseItem).addClass('in');
  $('#' + collapseItem).attr('aria-expanded', 'true');
  const child = $('#' + collapseItem + '-child');
  if (child) {
    $('#' + collapseItem + '-child').addClass('in');
    $('#' + collapseItem + '-child').attr('aria-expanded', 'true');
  }
  // if (collapseItem != null && collapseItem != "") {
  //     $(collapseItem).addClass('in');
  //     $(collapseItem).attr('aria-expanded','true');
  // } else {
  //     $("#collapse-product").addClass('in');
  //     $("#collapse-product").attr('aria-expanded','true');
  // }
}

function slugify(str) {
  let $slug = '';
  const trimmed = $.trim(str);
  $slug = trimmed
    .replace(/[^a-z0-9-æøå]/gi, '-')
    .replace(/-+/g, '-')
    .replace(/^-|-$/g, '')
    .replace(/æ/gi, 'ae')
    .replace(/ø/gi, 'oe')
    .replace(/å/gi, 'a');
  return $slug.toLowerCase();
}

function getSelectedOptions() {
  const options = {};
  $('.product-opt').each(function () {
    if ($(this).attr('name') === 'opt-') {
      options[$(this).val().trim()] = $(this).prop('checked');
      return;
    }
    let optionValue = $(this).val().trim();
    if ($(this).attr('type') === 'radio') {
      optionValue = $(
        'input[name="' + $(this).attr('name') + '"]:checked',
      ).val();
    }
    options[$(this).attr('name').substring(4, $(this).attr('name').length)] =
      optionValue;
  });
  return options;
}

// show notification popup
function showNotification(msg, type, reloadPage) {
  // defaults to false
  reloadPage = reloadPage || false;

  $('#notify_message').removeClass();
  $('#notify_message').addClass('bg-' + type);
  $('#notify_message').html(msg);
  $('#notify_message')
    .slideDown(600)
    .delay(2500)
    .slideUp(600, () => {
      if (reloadPage === true) {
        location.reload();
      }
    });
}

function removeSkuFromList(sku) {
  console.log(sku);
  const found = productSkuAdded.find((itm) => {
    return itm.sku === sku;
  });
  if ($('#' + sku)) {
    const index = productSkuAdded.indexOf(found);
    if (index > -1) {
      if (!localStorage.getItem(KEY_REMOVE_SKU_FROM_LIST_DONT_ASK)) {
        const dialog = bootbox.dialog({
          message:
            '<h3 style="padding-left: 25px;padding-right: 20px; line-height: 1.3; font-weight: bold;">Are you sure you want to remove<br/>' +
            'this product?</h3>' +
            '<div style="padding: 25px; padding-top: 5px">' +
            '<label>You cannot undo this action.</label><br/><br/>' +
            '<div>' +
            '<span><input type="checkbox" value="" id="dont-ask-me-again" checked"></input><span><label style="color: #797979;font-size: 13px;padding-left: 10px;padding-bottom: 10px;">Do not ask me again</label></span></span>' +
            '</div>' +
            '<div class="add-sku-table-middle-text">' +
            '<button id="btn-cancel-add-sku-popup" onclick="btnCancelAddSkuPopup()" class="btn-cancel-add-sku-popup">Cancel</button>' +
            '<button id="btn-create-add-sku-popup" onclick="btnRemoveSkuFromList(\'' +
            found.sku +
            '\')" class="btn-create-add-sku-popup">Remove' +
            '</button>' +
            '</div>' +
            '</div>',
          className: 'add-new-encrypted-string-dialog',
          closeButton: false,
        });
      } else {
        btnRemoveSkuFromList(sku);
      }
    } else {
      regenerateTable();
      verifyToEnableSendButton();
    }
  }
}

function btnRemoveSkuFromList(sku) {
  const found = productSkuAdded.find((itm) => {
    return itm.sku === sku;
  });
  if ($('#' + sku)) {
    bootbox.hideAll();
    if ($('#dont-ask-me-again').is(':checked')) {
      localStorage.setItem(KEY_REMOVE_SKU_FROM_LIST_DONT_ASK, true);
    }
    const index = productSkuAdded.indexOf(found);
    productSkuAdded.splice(index, 1);
    localStorage.setItem(KEY_ADDED_SKU, JSON.stringify(productSkuAdded));
    // ($('#' + sku)).remove();
    regenerateTable();
    verifyToEnableSendButton();
  }
}

function verifyToEnableSendButton() {
  let enableButton = true;
  let totalETAFileSize = 0;
  if (productSkuAdded.length > 0) {
    productSkuAdded.forEach((newItem) => {
      if (newItem.send === 0) {
        enableButton = false;
      }
      totalETAFileSize += newItem.send * 152.323;
    });
  } else {
    enableButton = false;
  }
  if (totalETAFileSize >= maxiumFile) {
    enableButton = false;
    const fileInMb = totalETAFileSize / 1000 / 1000;
    $('#label-error').html(
      'File size around ' +
        Number.parseFloat(fileInMb).toFixed(2) +
        'MB. It is quite big (maximum is 7MB). Please reduce it.',
    );
  } else {
    $('#label-error').html('');
  }
  $('#btn-send-email-multiple').attr('disabled', !enableButton);
  if (productSkuAdded.length > 0) {
    $('#tableAddedSKUFirstRow').css('display', 'none');
  } else {
    $('#tableAddedSKUFirstRow').css('display', 'contents');
  }
}

function regenerateSearchTable(array) {
  if (array.length > 0) {
    $('#resultSearch').html('');
    array.forEach((newItem) => {
      let row = '<tr>';
      row +=
        '<th scope="row" style="vertical-align: middle;width: 100%;">' +
        newItem.productVariantSku +
        '</th>';
      row += '<td style="vertical-align: middle;">';
      row += '<div class="pull-right">';
      row +=
        '<button type="button" id="btn-remove-sku" onclick="addSkuToList(\'' +
        newItem.productVariantSku +
        '\')" class="btn btn-remove"><span>Attach</span></button>';
      row += '</div>';
      row += '</td>';
      row += '</tr>';
      $('#resultSearch').append(row);
    });
  } else {
    $('#resultSearch').html('');
  }
}

function CreateMoreEncryptedString(sku) {
  const found = productSkuAdded.find((item) => {
    return item.sku === sku;
  });
  if (!found) {
    return;
  }
  const dialog = bootbox.dialog({
    // title: 'Create New Encrypted String for ' + sku,
    message:
      '<h3 style="padding-left: 25px;padding-right: 25px; line-height: 1.3; font-weight: bold;"> Add new encrypted string(s) for <br/>' +
      '[' +
      found.sku +
      ']</h3>' +
      '<div style="padding: 25px">' +
      '<label style="color: #797979;">Number of Encrypted String(s)</label>' +
      '<input type="number" min="1" placeholder="0" id="numberOfNewEncryptedString" class="bootbox-input bootbox-input-email form-control"></input>' +
      '<span><label id="txt_error" class="text-danger" style="font-size: 12px; display: none">Invalid number</label></span>' +
      '<br/>' +
      '<div>' +
      '<span><input type="checkbox" value="" id="generatePassword" checked"></input><span><label style="color: #797979;font-size: 13px;padding-left: 10px">Generate Password</label></span></span>' +
      '</div>' +
      '<br/>' +
      '<div id="nfc_type">' +
      '<input type="radio" value="1" name="nfc_type" data-num="1" class="form-check-input" checked><span for="male"><label style="color: #797979;font-size: 13px;padding-left: 10px;margin-right: 20px;">NFC Chips</label></span>' +
      '<input type="radio" value="2" name="nfc_type" data-num="2" class="form-check-input"><span for="male"><label style="color: #797979;font-size: 13px;padding-left: 10px;">Forreal Leather Keyfob</label></span>' +
      '</div><br/>' +
      '<div class="add-sku-table-middle-text">' +
      '<button id="btn-cancel-add-sku-popup" onclick="btnCancelAddSkuPopup()" class="btn-cancel-add-sku-popup">Cancel</button>' +
      '<button id="btn-create-add-sku-popup" onclick="btnCreateAddSkuPopup(\'' +
      found.sku +
      '\')" class="btn-create-add-sku-popup">Create' +
      '</button>' +
      '</div>' +
      '</div>',
    className: 'add-new-encrypted-string-dialog',
    closeButton: false,
  });
}

function editSkuToSend(sku) {
  const found = productSkuAdded.find((item) => {
    return item.sku === sku;
  });
  if (!found) {
    return;
  }
  let remainChipText = '';
  let remainChip = found.total - found.pending - found.completed;
  if (remainChip < 0) remainChip = 0;
  if (remainChip > 0) {
    remainChipText =
      '<span><label style="color: #797979;font-size: 12px;padding-top: 10px">Remainder chips: ' +
      remainChip +
      '</label></span>';
  }
  const dialog = bootbox.dialog({
    message:
      '<h3 style="padding-left: 25px;padding-right: 25px; line-height: 1.3; font-weight: bold"> Update amount of encrypted <br/>' +
      'strings to send</h3>' +
      '<div style="padding: 25px">' +
      '<label style="color: #797979;">New</label>' +
      '<input type="number" min="1" max="' +
      remainChip +
      '" placeholder="0"  id="numberOfNewEncryptedString" class="bootbox-input bootbox-input-email form-control"></input>' +
      remainChipText +
      '<br/>' +
      '<span><label id="txt_error" class="text-danger" style="font-size: 12px; display: none">Invalid number</label></span>' +
      '<br/><br/>' +
      '<div class="add-sku-table-middle-text">' +
      '<button id="btn-cancel-add-sku-popup" onclick="btnCancelAddSkuPopup()" class="btn-cancel-add-sku-popup">Cancel</button>' +
      '<button id="btn-create-add-sku-popup" onclick="btnUpdateToSendSkuPopup(\'' +
      found.sku +
      '\')" class="btn-create-add-sku-popup">Update' +
      '</button>' +
      '</div>' +
      '</div>',
    className: 'add-new-encrypted-string-dialog',
    closeButton: false,
  });
}

function btnUpdateToSendSkuPopup(sku) {
  $('#txt_error').css('display', 'none');
  const found = productSkuAdded.find((item) => {
    return item.sku === sku;
  });
  if (!found) {
    return;
  }
  const numberOfItem = parseInt($('#numberOfNewEncryptedString').val());
  if (numberOfItem <= 0) return;
  let remainChip = found.total - found.pending - found.completed;
  if (remainChip < 0) remainChip = 0;
  if (numberOfItem <= remainChip) {
    found.send = parseInt(numberOfItem);
    $('#created-number-send-' + found.sku).html(found.send);
    localStorage.setItem(KEY_ADDED_SKU, JSON.stringify(productSkuAdded));
  } else {
    $('#txt_error').css('display', 'contents');
    return;
  }

  bootbox.hideAll();
}

function btnCreateAddSkuPopup(sku) {
  // do something in the background
  const numberOfItem = $('#numberOfNewEncryptedString').val();
  const nfcType = $("#nfc_type input[type='radio']:checked").val();
  $('#txt_error').css('display', 'none');
  if (numberOfItem == 0) {
    $('#txt_error').css('display', 'contents');
    return;
  }
  console.log(sku);
  const loadingText =
    '<i class="fa fa-circle-o-notch fa-spin"></i> Processing...';
  if ($('#btn-create-add-sku-popup').html() !== loadingText) {
    $('#btn-create-add-sku-popup').data(
      'original-text',
      $('#btn-create-add-sku-popup').html(),
    );
    $('#btn-create-add-sku-popup').html(loadingText);
    $('#btn-create-add-sku-popup').attr('disabled', true);
    $('#btn-cancel-add-sku-popup').attr('disabled', true);
  }
  $.ajax({
    method: 'POST',
    url: '/admin/qrcode/create-new-encrypted-string',
    data: {
      numberOfItem: $('#numberOfNewEncryptedString').val(),
      sku: sku,
      generatePassword: $('#generatePassword').is(':checked') ? 'on' : 'off',
      isForrealLeatherKeyfob: nfcType == 1 ? false : true,
    },
  })
    .done((msg) => {
      bootbox.hideAll();
      console.log(msg);
      const found = productSkuAdded.find((item) => {
        return item.sku === sku;
      });
      found.empty = 0;
      found.total = parseInt(msg.data.total);
      found.pending = parseInt(msg.data.pending);
      found.send = parseInt(msg.data.send);
      found.completed = parseInt(msg.data.completed);
      found.empty = 0;
      localStorage.setItem(KEY_ADDED_SKU, JSON.stringify(productSkuAdded));
      showNotification('Success', 'success', false);
      $('#created-number-total-' + found.sku).html(found.total);
      $('#created-number-pending-' + found.sku).html(found.pending);
      $('#created-number-send-' + found.sku).html(found.send);
      $('#created-number-completed-' + found.sku).html(found.completed);
      verifyToEnableSendButton();
    })
    .fail((msg) => {
      bootbox.hideAll();
      showNotification('Something went wrong', 'danger');
    });
}

function btnCancelAddSkuPopup() {
  bootbox.hideAll();
}

function addSkuToList(sku) {
  const resultSku = sku;
  const foundAdded = productSkuAdded.find((itm) => {
    return itm.sku.toLowerCase() === resultSku.toLowerCase();
  });

  if (foundAdded) {
    showNotification('SKU has been added', 'success', false);
    return;
  }

  $.ajax({
    method: 'GET',
    url: '/admin/qrcode/count/' + resultSku,
  })
    .done((msg) => {
      console.log(msg);
      if (resultSku) {
        let newItem = {};
        newItem = {
          id: resultSku.replace(/[&\/\\#, +()$~%.'":*?<>{}]/g, '_'),
          sku: resultSku,
          total: parseInt(msg.data.total),
          pending: parseInt(msg.data.pending),
          send: parseInt(msg.data.send),
          completed: parseInt(msg.data.completed),
          empty: 0,
        };
        $('#attachedSku').val('');
        $('#resultDiv').hide();
        $('#resultSku').val('');
        productSkuAdded.push(newItem);
        localStorage.setItem(KEY_ADDED_SKU, JSON.stringify(productSkuAdded));
        generateTableSkuRow(newItem, productSkuAdded.length);
        $('#resultSearch').html('');
        verifyToEnableSendButton();
      }
    })
    .fail((msg) => {
      showNotification(msg.responseJSON.message, 'danger');
    });
}

function generateDefautlTableRow() {
  const row =
    '<tr id="tableAddedSKUFirstRow" style="display: contents;"> ' +
    '<td scope="col" className="tableUUIDRequest-first-column"></td> ' +
    '<td className="td-udid-empty" scope="col-5" colSpan="7" style="text-align: center;"> Please search and attach SKU </td></tr>';
  $('#tableAddedSKU').append(row);
}

function generateTableSkuRow(newItem, index) {
  console.log(newItem);
  let row = '<tr id="' + newItem.sku + '" >';
  row +=
    '<td class="tableUUIDRequest-first-column add-sku-table-row-vertical-align"><label id="sku-name-' +
    newItem.sku +
    '">' +
    index +
    '</label></td>';
  row +=
    '<td class="add-sku-table-row-vertical-align"><label id="sku-name-' +
    newItem.sku +
    '">' +
    newItem.sku +
    '</label></td>';
  row +=
    '<td class="add-sku-table-row-middle-text"><label id="created-number-total-' +
    newItem.sku +
    '">' +
    newItem.total +
    '</label></td>';
  row +=
    '<td class="add-sku-table-row-middle-text"><label id="created-number-send-' +
    newItem.sku +
    '">' +
    newItem.send +
    '</label><img onclick="editSkuToSend(\'' +
    newItem.sku +
    '\')" class="img-edit-send" src="/images/icon/ic_artist@2x.png"></td>';
  row +=
    '<td class="add-sku-table-row-middle-text"><label id="created-number-pending-' +
    newItem.sku +
    '">' +
    newItem.pending +
    '</label></td>';
  row +=
    '<td class="add-sku-table-row-middle-text"><label id="created-number-completed-' +
    newItem.sku +
    '">' +
    newItem.completed +
    '</label></td>';
  row +=
    '<td class="add-sku-table-row-middle-text"><button type="button" id="btn-add-create-' +
    newItem.sku +
    '" onclick="CreateMoreEncryptedString(\'' +
    newItem.sku +
    '\')" class="btn btn-remove btn-add-sku"><span>Add</span></button></td>';
  row += '<td style="vertical-align: middle;">';
  row += '<div class="pull-right">';
  row +=
    '<button type="button" id="btn-add-sku" onclick="removeSkuFromList(\'' +
    newItem.sku +
    '\')" class="btn btn-remove btn-remove-sku"><i class="fa fa-trash-o"></i></button>';
  row += '</div>';
  row += '</td>';
  row += '</tr>';
  $('#tableAddedSKU').append(row);
}

function regenerateTable() {
  $('#tableAddedSKU').empty();
  if (productSkuAdded.length > 0) {
    let i = 0;
    productSkuAdded.forEach((newItem) => {
      i++;
      generateTableSkuRow(newItem, i);
    });
  } else {
    generateDefautlTableRow();
  }
  verifyToEnableSendButton();
}

function btnUdidRequestContinue() {
  if (productSkuAdded.length > 0) {
    let messageError = '';
    productSkuAdded.forEach((item) => {
      if (item.send === 0) {
        messageError = 'Please create encrypted string for sku: ' + item.sku;
      }
    });
    if (messageError.length !== 0) {
      bootbox.alert(messageError);
      return;
    }

    let htmlContent =
      '<h3 style="padding-left: 25px;padding-right: 25px; line-height: 1.3">Send encrypted strings email<br/>' +
      'to manufacture</h3>' +
      '<div style="padding: 25px">' +
      '<input type="text" style="margin-bottom: 10px" placeholder="To" id="emailManufacture" class="bootbox-input bootbox-input-email form-control"></input>' +
      '<input type="text" style="margin-bottom: 10px" placeholder="Cc" id="emailCC" class="bootbox-input bootbox-input-email form-control"></input>' +
      '<input type="text" style="margin-bottom: 10px" placeholder="Subject" id="subject" class="bootbox-input bootbox-input-email form-control"></input>' +
      '<textarea style="margin-bottom: 10px;height: 133px" placeholder="Instructions" id="content" class="bootbox-input bootbox-input-email form-control"></textarea>';
    if ($('#enableSendEmailV2').val() === 'true') {
      htmlContent =
        htmlContent +
        '<br/><p style="margin-bottom: 10px;font-size: 11px;line-height: 1.5;color: darkgrey">' +
        '<b>Note: Please add {ZIP_FILE_DOWNLOAD_URL} to row where you want to show download url</b></p>';
    }
    htmlContent =
      htmlContent +
      '<div class="add-sku-table-middle-text">' +
      '<button id="btn-cancel-add-sku-popup" onclick="btnCancelAddSkuPopup()" class="btn-cancel-add-sku-popup">Cancel</button>' +
      '<button id="btn-create-add-sku-popup" onclick="sendSkuViaEmail()" class="btn-create-add-sku-popup">Confirm' +
      '</button>' +
      '</div>' +
      '</div>';

    const dialog = bootbox.dialog({
      message: htmlContent,
      className: 'add-new-encrypted-string-dialog',
      closeButton: false,
    });
  }
}

function sendSkuViaEmail() {
  bootbox.hideAll();
  if (productSkuAdded.length > 0) {
    if ($('#emailManufacture').val().length === 0) {
      bootbox.alert('Please enter email manufacture');
      return;
    }
    if ($('#emailCC').val().length === 0) {
      bootbox.alert('Please enter email to cc');
      return;
    }
    if ($('#subject').val().length === 0) {
      bootbox.alert('Please enter subject');
      return;
    }
    if ($('#content').val().length === 0) {
      bootbox.alert('Please enter instruction');
      return;
    }
    if ($('#enableSendEmailV2').val() === 'true') {
      if (!$('#content').val().includes('{ZIP_FILE_DOWNLOAD_URL}')) {
        bootbox.alert(
          `Please enter enter "{ZIP_FILE_DOWNLOAD_URL}" to email content `,
        );
        return;
      }
    }

    let messageError = '';
    productSkuAdded.forEach((item) => {
      if (item.send === 0) {
        messageError = 'Please create encrypted string for sku: ' + item.sku;
      }
    });
    if (messageError.length === 0) {
      const dialog = bootbox.dialog({
        message:
          '<div style="text-align: center">' +
          '<img onclick="closeSendEmail()" style="width: 100%" src="/images/icon/ic_email_sent.png">' +
          '<img id="img-sent-check" class="img-sent-check" style="display: none;" src="/images/icon/ic_email_check.png">' +
          '<progress id="send-email-progress" class="pure-material-progress-circular processing-send-email"/>' +
          '<label style="color: #797979;" id="label-send-email">Sending</label>' +
          '</div>',
        backdrop: true,
        className: 'email-processing',
        closeButton: false,
      });
      $.ajax({
        method: 'POST',
        url: '/admin/qrcode/send-bulk-csv-to-manufacture',
        data: {
          arraySku: JSON.stringify(productSkuAdded),
          emailManufacture: $('#emailManufacture').val(),
          emailCC: $('#emailCC').val(),
          subject: $('#subject').val(),
          content: $('#content').val(),
        },
      })
        .done((msg) => {
          $('#send-email-progress').fadeOut('slow');
          $('#img-sent-check').fadeIn('slow');
          $('#label-send-email').html('Email sent');
          productSkuAdded = [];
          localStorage.setItem(KEY_ADDED_SKU, productSkuAdded);
          setTimeout(function () {
            window.location.href = '/admin/products/0';
          }, 2000);
        })
        .fail((msg) => {
          showNotification(msg.msg, 'danger');
        });
    } else {
      bootbox.hideAll();
      bootbox.alert(messageError);
    }
  }
}

function closeSendEmail() {
  if ($('#img-sent-check').is(':visible')) {
    window.location.href = '/admin/products/0';
  }
}

function rotateMenuArrowAngle(el) {
  // localStorage.setItem('collapseItem', el.attr('href'));
  const itag = el.find('i');
  const iclass = itag.attr('class');
  if (iclass == 'fa fa-angle-down') {
    itag.attr('class', 'fa fa-angle-up');
  } else {
    itag.attr('class', 'fa fa-angle-down');
  }
}

function setupMenuBehavior() {
  // menu
  const target0 = document.querySelector('#collapse-analytic');
  if (target0) {
    const observer0 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer0.observe(target0, {
      attributes: true,
    });
  }
  const target1 = document.querySelector('#collapse-product');
  if (target1) {
    const observer1 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer1.observe(target1, {
      attributes: true,
    });
  }
  const target2 = document.querySelector('#collapse-campaign');
  if (target2) {
    const observer2 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer2.observe(target2, {
      attributes: true,
    });
  }
  const target3 = document.querySelector('#collapse-store');
  if (target3) {
    const observer3 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer3.observe(target3, {
      attributes: true,
    });
  }

  const target4 = document.querySelector('#collapse-certificate');
  if (target4) {
    const observer4 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer4.observe(target4, {
      attributes: true,
    });
  }
  const target5 = document.querySelector('#collapse-artist');
  if (target5) {
    const observer5 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer5.observe(target5, {
      attributes: true,
    });
  }
  const target6 = document.querySelector('#collapse-notification');
  if (target6) {
    const observer6 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer6.observe(target6, {
      attributes: true,
    });
  }

  const target7 = document.querySelector('#collapse-artist');
  if (target7) {
    const observer7 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer7.observe(target7, {
      attributes: true,
    });
  }

  const target8 = document.querySelector('#collapse-sesame');
  if (target8) {
    const observer8 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer8.observe(target8, {
      attributes: true,
    });
  }

  const target9 = document.querySelector('#collapse-setting');
  if (target9) {
    const observer9 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer9.observe(target9, {
      attributes: true,
    });
  }
  const target10 = document.querySelector('#collapse-flagged');
  if (target10) {
    const observer10 = new MutationObserver((mutations) => {
      if (mutations.length > 0) {
        const item = mutations[0];
        $('#' + item.target.id + '-item')
          .parent()
          .find('li.active')
          .removeClass('active');
        $('#' + item.target.id + '-item').addClass('active');
      }
    });
    observer10.observe(target10, {
      attributes: true,
    });
  }
}

function generateQrcode(qrcode_id, message) {
  console.log(message);
  const qrcode = new QRCode(document.getElementById(qrcode_id), {
    width: 200,
    height: 200,
  });
  qrcode.makeCode(message);
  $('#' + qrcode_id).show();
}

function showEmailContent(subject, content, reciever) {
  const dialog = bootbox.dialog({
    message:
      '<h3 style="padding-left: 25px;padding-right: 25px; line-height: 1.3">Content</h3>' +
      '<div style="padding: 25px">' +
      '<input value="' +
      reciever +
      '" type="text" style="margin-bottom: 10px" placeholder="To" id="emailManufacture" class="bootbox-input bootbox-input-email form-control"></input>' +
      '<input value="' +
      subject +
      '" type="text" style="margin-bottom: 10px" placeholder="Subject" id="subject" class="bootbox-input bootbox-input-email form-control"></input>' +
      '<textarea style="margin-bottom: 10px;height: 133px" placeholder="Instructions" id="content" class="bootbox-input bootbox-input-email form-control">' +
      content +
      '</textarea>' +
      '<br/>' +
      '<div class="add-sku-table-middle-text">' +
      '</button>' +
      '</div>' +
      '</div>',
    className: 'add-new-encrypted-string-dialog',
    closeButton: true,
  });
}
