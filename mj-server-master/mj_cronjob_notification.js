const MongoClient = require('mongodb').MongoClient;
const mongodbUri = require('mongodb-uri');
const cron = require('node-cron');
const common = require('./lib/common');
const sendEmailCrondjob = require('./services/sendemail-cronjob');
const express = require('express');
const colors = require('colors');
const { Schedule } = require('./lib/common');
const app = express();
//init firebase;
const { adminFirebase } = require('./config/firebase');

// get config
const config = common.getConfig();

const option = {
  socketTimeoutMS: 9000000,
  keepAlive: true,
  reconnectTries: 30000,
};

MongoClient.connect(config.databaseConnectionString, option, (err, client) => {
  // On connection error we display then exit
  if (err) {
    console.log(colors.red('Error connecting to MongoDB: ' + err));
    process.exit(2);
  }
  if (process.env.ENABLE_CRONJOB === 'true') {
    // select DB
    const dbUriObj = mongodbUri.parse(config.databaseConnectionString);
    let db;
    // if in testing, set the testing DB
    if (process.env.NODE_ENV === 'test') {
      db = client.db('testingdb');
    } else {
      db = client.db(dbUriObj.database);
    }

    // setup the collections
    common.dbInitCollection(db);

    // add db to app for routes
    app.dbClient = client;
    app.db = db;
    app.config = config;
    app.port = app.get('8001');

    console.log('staring job send notification');
    cron.schedule('*/5 * * * *', () => {
      sendEmailCrondjob.check_schedule_notification(app);
      console.log('job send notification');
    });

    Schedule(
      app,
      'upcommingProducts',
      'dropDate',
      { active: false },
      'upcommingProducts',
    );

  } else {
    console.log('Job not enable');
  }
});

module.exports = app;
