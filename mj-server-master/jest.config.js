module.exports = {
  collectCoverageFrom: ['mobile/routes/v4/**', 'controllers/**'],
  setupFilesAfterEnv: ['./testSetup.js', 'jest-chain', 'jest-extended'],
  preset: '@shelf/jest-mongodb',
  moduleDirectories: ['packages', 'node_modules'],
  testPathIgnorePatterns: ['/node_modules/'],
  modulePaths: ['<rootDir>/node_modules/'],
  testMatch: [
    '<rootDir>/tests/**/*.test.js',
  ],
};
