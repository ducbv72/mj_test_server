// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

abstract contract KrapInterface {
    function transferKrap(address to, uint weiAmount) public virtual;
    function owner() public view virtual returns (address);
    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public virtual returns (bool);
    function transfer(address recipient, uint256 amount) public virtual returns (bool);
    function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool);
    function approve(address spender, uint256 amount) public virtual returns (bool);
}

contract IKRAP is ERC20, Ownable {
    // Array of addresses that have tokens
    address[] public addressesForRound1;
    address[] public addressesForRound2;
    address[] public addressesForRound3;

    // Mapping from address to allocated tokens
    mapping (address => uint) public allocationForRound1;
    mapping (address => uint) public allocationForRound2;
    mapping (address => uint) public allocationForRound3;

    // Maximum number of tokens for each round
    mapping (uint => uint) public maxSupplyPerRound;
    // Number of allocated tokens for each round 
    mapping (uint => uint) public allocatedPerRound;
    // The release rate (in percentage) for each round 
    mapping (uint => uint) public releaseRatePerRound;
    // The percentage of tokens that have already been released for each round
    mapping (uint => uint) public releasedPerRound;

    KrapInterface krap;

    bool public allowSwap = false;

    constructor(
        string memory name,
        string memory symbol,
        uint[3] memory _maxSupplyPerRound,
        uint[3] memory _releaseRatePerRound

    ) ERC20(name, symbol) {
        require(_maxSupplyPerRound.length == 3, "Must have 3 funding rounds");

        uint initialSupply = 0;
        setReleaseRate(_releaseRatePerRound);

        for (uint i = 0; i < _maxSupplyPerRound.length; i++) {
            require(_maxSupplyPerRound[i] > 0, "Supply can't be 0");

            maxSupplyPerRound[i + 1] = _maxSupplyPerRound[i];
            initialSupply += _maxSupplyPerRound[i];
        }

        _mint(msg.sender, initialSupply * 1 ether);
    }

    function setReleaseRate(uint[3] memory _releaseRatePerRound) public onlyOwner {
        require(_releaseRatePerRound.length == 3, "Must have 3 funding rounds");

        for (uint i = 0; i < _releaseRatePerRound.length; i++) {
            require(_releaseRatePerRound[i] > 0 && _releaseRatePerRound[i] < 100, "Release rate must be between 0 and 100");

            releaseRatePerRound[i + 1] = _releaseRatePerRound[i];
        }
    }

    function setKrapContractAddress(address _contractAddress, bool _allowSwap) external onlyOwner {
        krap = KrapInterface(_contractAddress);
        allowSwap = _allowSwap;
    }

    function getContractAddress() public view returns (address) {
        return address(this);
    }

    function setAllocation(uint round, uint amount, address to) external onlyOwner {        
        require(round >= 1 && round <= 3, "There're only 3 funding rounds");
        require(amount > 0, "Can't allocate 0 tokens");
        require(amount + allocatedPerRound[round] <= maxSupplyPerRound[round], "Exceeds maximum supply");

        if (round == 1) {
            addressesForRound1.push(to);
            allocationForRound1[to] = amount;
        } else if (round == 2) {
            addressesForRound2.push(to);
            allocationForRound2[to] = amount;
        } else {
            addressesForRound3.push(to);
            allocationForRound3[to] = amount;
        }

        allocatedPerRound[round] += amount;
    }

    function releaseTokensHelper(address[] storage addresses, mapping (address => uint) storage allocation, uint releaseRate) private {
        for (uint i = 0; i < addresses.length; i++) {
            address to = addresses[i];
            uint totalAllocation = allocation[to];
            uint weiAmount = (totalAllocation * releaseRate * 1 ether) / 100;

            transfer(to, weiAmount);
        }
    }

    function releaseTokens(uint round) external onlyOwner {
        require(round >= 1 && round <= 3, "There're only 3 funding rounds");
        require(releasedPerRound[round] < 100, "All tokens have been released");

        if (round == 1) {
            releaseTokensHelper(addressesForRound1, allocationForRound1, releaseRatePerRound[round]);
        } else if (round == 2) {
            releaseTokensHelper(addressesForRound2, allocationForRound2, releaseRatePerRound[round]);
        } else if (round == 3) {
            releaseTokensHelper(addressesForRound3, allocationForRound3, releaseRatePerRound[round]);
        }

        releasedPerRound[round] += releaseRatePerRound[round];
    }

    function setAllowSwap(bool _allowSwap) external onlyOwner {
        allowSwap = _allowSwap;
    }

    function transferKrap(address to, uint amount) external onlyOwner {
        uint weiAmount = amount * 1 ether;

        krap.transferKrap(to, weiAmount);
    }

    function swapTokens(uint amount) public {
        uint weiAmount = amount * 1 ether;
        address to = msg.sender;

        require(allowSwap, "Swap tokens is disabled");
        require(weiAmount > 0, "Can't swap 0 tokens");
        require(balanceOf(to) >= weiAmount, "Not enough tokens");
        require(to != address(0), "Invalid address");

        krap.transferKrap(to, weiAmount);
        _burn(to, weiAmount);
    }
}