// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract KRAP is ERC20, Ownable {
    address private ikrapContractAddress;

    constructor(
        string memory name,
        string memory symbol,
        uint krapSupply, 
        uint ikrapSupply,
        address ikrapAddress

    ) ERC20(name, symbol) {
        require(krapSupply > ikrapSupply, "Krap supply must be higher than ikrap supply");

        _mint(msg.sender, (krapSupply - ikrapSupply) * 1 ether);
        _mint(ikrapAddress, ikrapSupply * 1 ether);

        setIkrapContractAddress(ikrapAddress);
    }

    function setIkrapContractAddress(address _contractAddress) public onlyOwner {
        ikrapContractAddress = _contractAddress;
    }

    modifier onlyIkrap() {
        require(msg.sender == ikrapContractAddress, "You need to use IKRAP contract address");
        _;
    }

    function getContractAddress() public view returns (address) {
        return address(this);
    }

    function transferKrap(address to, uint weiAmount) public onlyIkrap {
        require(ikrapContractAddress != address(0), "IKRAP contract address has not been set");
        require(to != address(0), "Invalid address");
        require(weiAmount > 0, "Can't swap 0 tokens");

        transfer(to, weiAmount);
    }
}