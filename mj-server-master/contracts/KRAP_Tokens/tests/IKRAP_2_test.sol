// SPDX-License-Identifier: GPL-3.0
    
pragma solidity >=0.4.22 <0.9.0;

// This import is automatically injected by Remix
import "remix_tests.sol"; 

// This import is required to use custom transaction context
// Although it may fail compilation in 'Solidity Compiler' plugin
// But it will work fine in 'Solidity Unit Testing' plugin
import "remix_accounts.sol";
import "hardhat/console.sol";

import "../IKRAP.sol";
import "../KRAP.sol";

contract testSuite2 {
    IKRAP ikrap;
    KRAP krap;

    uint ikrapSupply = 2000;
    uint krapSupply = 6000;
    uint allocationForAccount = 100;
    uint rateRound1 = 10;

    address account1 = TestsAccounts.getAccount(1);

    function beforeAll() public {
        // IKRAP
        uint[3] memory maxSupplyPerRound = [uint(ikrapSupply/3), uint(ikrapSupply/3), uint(ikrapSupply/3)];
        uint[3] memory releaseRatePerRound = [rateRound1, uint(20), uint(25)];
        ikrap = new IKRAP("name", "symbol", maxSupplyPerRound, releaseRatePerRound);
        address ikrapAddress = ikrap.getContractAddress();

        // KRAP
        krap = new KRAP("name", "symbol", krapSupply, ikrapSupply, ikrapAddress);
    }

    function testSwapWithInvalidAmount() public {
        try ikrap.swapTokens(0) {
            Assert.ok(false, "Invalid amount");
        } catch {
            Assert.ok(true, "Success");
        }
    }

    function testSwapIfDisabled() public {
        try ikrap.swapTokens(1) {
            Assert.ok(false, "Swap tokens is disabled");
        } catch {
            Assert.ok(true, "Success");
        }
    }

    function testSetAllowSwap() public {
        try ikrap.setAllowSwap(true) {
            Assert.ok(true, "Success");
        } catch Error(string memory reason) {
            console.log(reason);
            Assert.ok(false, "failed unexpected");
        }
    }

    function testSwap() public {
        address ikrapAddress = ikrap.getContractAddress();
        address krapAddress = krap.getContractAddress();

        ikrap.setKrapContractAddress(krapAddress, true);
        krap.setIkrapContractAddress(ikrapAddress);

        uint swapAmount = 5;
        uint initialAmount = ikrap.balanceOf(ikrap.owner()) / 1 ether;
        uint initialAmountKrap = krap.balanceOf(krap.owner()) / 1 ether;

        console.log("balance of ikrap for owner", ikrap.balanceOf(ikrap.owner()) / 1 ether);
        console.log("balance of krap for owner", krap.balanceOf(krap.owner()) / 1 ether);
        console.log("balance of krap for ikrap contract", krap.balanceOf(ikrap.getContractAddress()) / 1 ether);

        try ikrap.swapTokens(swapAmount) {
            uint finalAmount = ikrap.balanceOf(ikrap.owner()) / 1 ether; 
            uint finalAmountKrap = krap.balanceOf(krap.owner()) / 1 ether; 

            console.log("finalAmount", finalAmount);
            console.log("finalAmountKrap", finalAmountKrap);

            Assert.equal((initialAmount - swapAmount), finalAmount, "Invalid final amount for IKRAP");
            Assert.equal((initialAmountKrap + swapAmount), finalAmountKrap, "Invalid final amount for KRAP");
        } catch Error(string memory reason) {
            console.log(reason);
            Assert.ok(false, "failed unexpected");
        } 
    }
}
