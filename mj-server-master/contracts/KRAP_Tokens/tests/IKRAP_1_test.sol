// SPDX-License-Identifier: GPL-3.0
    
pragma solidity >=0.4.22 <0.9.0;

// This import is automatically injected by Remix
import "remix_tests.sol"; 

// This import is required to use custom transaction context
// Although it may fail compilation in 'Solidity Compiler' plugin
// But it will work fine in 'Solidity Unit Testing' plugin
import "remix_accounts.sol";
import "hardhat/console.sol";

import "../IKRAP.sol";
import "../KRAP.sol";

contract testSuite {
    IKRAP ikrap;
    KRAP krap;

    uint ikrapSupply = 3000;
    uint krapSupply = 6000;
    uint allocationForAccount = 100;
    uint rateRound1 = 10;

    address account1 = TestsAccounts.getAccount(1);
    address account2 = TestsAccounts.getAccount(2);

    function beforeAll() public {
        // IKRAP
        uint[3] memory maxSupplyPerRound = [uint(ikrapSupply/3), uint(ikrapSupply/3), uint(ikrapSupply/3)];
        uint[3] memory releaseRatePerRound = [rateRound1, uint(20), uint(25)];
        ikrap = new IKRAP("name", "symbol", maxSupplyPerRound, releaseRatePerRound);
        address ikrapAddress = ikrap.getContractAddress();

        // KRAP
        krap = new KRAP("name", "symbol", krapSupply, ikrapSupply, ikrapAddress);
    }

    function testSupply() public {
        try ikrap.totalSupply() returns (uint supply) {
            Assert.equal(supply, ikrapSupply * 1 ether, "Invalid supply");
        } catch Error(string memory reason) {
            console.log(reason);
            Assert.ok(false, "failed unexpected");
        } 
    }

    function testSetAllocation() public {
        try ikrap.setAllocation(2, allocationForAccount, account2) {
            Assert.ok(true, "Success");
        } catch Error(string memory reason) {
            console.log(reason);
            Assert.ok(false, "failed unexpected");
        } 

        try ikrap.setAllocation(0, allocationForAccount, account2) {
            Assert.ok(false, "Invalid round");
        } catch {
            Assert.ok(true, "Success");
        }

        try ikrap.setAllocation(1, 0, account2) {
            Assert.ok(false, "Invalid amount");
        } catch {
            Assert.ok(true, "Success");
        }

        try ikrap.setAllocation(1, ikrapSupply, account2) {
            Assert.ok(false, "Invalid amount");
        } catch {
            Assert.ok(true, "Success");
        }
    }

    function testReleaseTokens() public {
        try ikrap.setAllocation(1, allocationForAccount, account1) {
            Assert.ok(true, "Success");
        } catch Error(string memory reason) {
            console.log(reason);
            Assert.ok(false, "failed unexpected");
        } 

        try ikrap.releaseTokens(0) {
            Assert.ok(false, "There're only 3 funding rounds");
        } catch {
            Assert.ok(true, "Success");        
        } 

        uint numberOfPhases = 100 / rateRound1;

        for (uint i = 0; i < numberOfPhases; i++) {
            try ikrap.releaseTokens(1) {
                uint expectedBalance = (allocationForAccount * rateRound1 * 1 ether) / 100 * (i + 1);
                Assert.equal(ikrap.balanceOf(account1), expectedBalance, "Invalid balance for account1");

                console.log("Phase", i + 1, ": balanceOf account1", ikrap.balanceOf(account1) / 1 ether);
            } catch Error(string memory reason) {
                console.log(reason);
                Assert.ok(false, "failed unexpected");
            } 
        }
        
        try ikrap.releaseTokens(1) {
            Assert.ok(false, "All tokens have been released");
        } catch {
            Assert.ok(true, "Success");        
        } 
    }

    function testSetContractAddress() public {
        address krapAddress = krap.getContractAddress();

        try ikrap.setKrapContractAddress(krapAddress, true) {
            Assert.ok(true, "Success");
        } catch Error(string memory reason) {
            console.log(reason);
            Assert.ok(false, "failed unexpected");
        } 
    }

    function testTransferKrap() public {
        address owner = ikrap.owner();
        uint amount = 500;

        uint initialAmountKrap = krap.balanceOf(krap.owner()) / 1 ether; 
        console.log("balance of krap for owner", initialAmountKrap);
        console.log("balance of krap for ikrap contract", krap.balanceOf(ikrap.getContractAddress()) / 1 ether);

        try ikrap.transferKrap(owner, amount) {
            uint finalAmountKrap = krap.balanceOf(krap.owner()) / 1 ether; 

            console.log("finalAmountKrap", finalAmountKrap);

            Assert.equal((initialAmountKrap + amount), finalAmountKrap, "Invalid final amount for KRAP");
        } catch Error(string memory reason) {
            console.log(reason);
            Assert.ok(false, "failed unexpected");
        } 
    }
}
