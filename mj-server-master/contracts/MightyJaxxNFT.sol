// SPDX-License-Identifier: MIT

pragma solidity 0.8.6;

import "https://github.com/0xcert/ethereum-erc721/src/contracts/tokens/nf-token-metadata.sol";
import "https://github.com/0xcert/ethereum-erc721/src/contracts/ownership/ownable.sol";

contract MightyJaxxNFT is NFTokenMetadata, Ownable {
    uint256 count;

    constructor()
    {
        nftName = "Mighty Jaxx";
        nftSymbol = "MightyJaxx";
    }

    function mintNFT(
        address _to,
        string memory tokenURI
    )
        external
        onlyOwner
        returns (uint256)
    {
        count++;
        super._mint(_to, count);
        super._setTokenUri(count, tokenURI);

        return count;
    }
}