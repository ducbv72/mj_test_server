// SPDX-License-Identifier: MIT

pragma solidity ^0.8.6;

import "./ERC721EnumerableCustom.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract DogeNFT is ERC721EnumerableCustom, Ownable {
    using Strings for uint;
    
    uint public price = 0.08 ether;
    uint public maxOrder = 20;
    uint public maxSupply = 300;
    
    bool public isActive = true;
    
    string private _baseTokenURI = "localhost:8080/admin/products/nft/doge/";
    string private _tokenURISuffix = "";
    
    constructor() ERC721Custom("Doge", "DOGE") {}
    
    function mintNFT(uint quantity) external payable {
        uint supply = totalSupply();

        require(isActive, "Sale is not active");
        require(quantity <= maxOrder, "Order too big");
        require(msg.value >= price * quantity, "Ether sent is not correct");
        require(supply + quantity <= maxSupply, "Mint order exceeds supply" );

        for (uint i; i < quantity; ++i) {
            _safeMint(msg.sender, supply++, "");
        }
    }
    
    function gift(uint[] calldata quantity, address[] calldata recipient) external onlyOwner {
        require(quantity.length == recipient.length, "Must provide equal quantities and recipients" );
        
        uint totalQuantity;
        uint supply = totalSupply();
        for (uint i; i < quantity.length; ++i) {
            totalQuantity += quantity[i];
        }

        require(supply + totalQuantity <= maxSupply, "Mint order exceeds supply" );
        
        for (uint i; i < recipient.length; ++i) {
            for (uint j; j < quantity[i]; ++j) {
                _safeMint(recipient[i], supply++, "");
            }
        }
    }
    
    function setActive(bool _isActive) external onlyOwner {
        require(isActive != _isActive, "New value matches old" );
        isActive = _isActive;
    }
    
    function setMaxOrder(uint _maxOrder) external onlyOwner {
        require(maxOrder != _maxOrder, "New value matches old" );
        maxOrder = _maxOrder;
    }

    function setMaxSupply(uint _maxSupply) external onlyOwner{
        require(maxSupply != _maxSupply, "New value matches old" );
        require(_maxSupply >= totalSupply(), "Specified supply is lower than current balance" );
        maxSupply = _maxSupply;
    }
    
    function setPrice(uint _price) external onlyOwner {
        require(price != _price, "New value matches old" );
        price = _price;
    }
    
    function setBaseURI(string calldata _newBaseURI, string calldata _newSuffix) external onlyOwner {
        _baseTokenURI = _newBaseURI;
        _tokenURISuffix = _newSuffix;
    }
    
    function tokenURI(uint tokenId) external view virtual override returns (string memory) {
        require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");
        return string(abi.encodePacked(_baseTokenURI, tokenId.toString(), _tokenURISuffix));
    }

    // Withdraw contract funds to owner
    function withdraw() external payable onlyOwner {
        (bool success, ) = payable(msg.sender).call{value: address(this).balance}("");
        require(success);
    }
}