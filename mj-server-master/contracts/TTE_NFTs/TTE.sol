// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract TTE is ERC721, ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter public _tokenIdCounter;

    // Max supply
    uint256 public coconutMaxSupply = 200;
    uint256 public mangoStickyRiceMaxSupply = 200;
    uint256 public tomYumMaxSupply = 200;
    uint256 public setMealMaxSupply = 600;

    // Current supply
    uint256 public coconutSupply = 0;
    uint256 public mangoStickyRiceSupply = 0;
    uint256 public tomYumSupply = 0;
    uint256 public setMealSupply = 0;

    bool isContractLocked = false;

    constructor() ERC721("Nyammy Treats: Aroy Nyan Nyan", "MET-22NTANN") {}

    function safeMint(uint256 quantity, address to, string memory uri, uint256 nftType) public onlyOwner {
        uint256 supply = totalSupply();
        require(supply + quantity <= maxSupply(), "Mint order exceeds max supply" );

        for (uint256 i; i < quantity; i++) {
            if (nftType == 1) {
                // Coconut
                require(coconutSupply + quantity <= coconutMaxSupply, "Mint order exceeds Coconut max supply" );
                coconutSupply++;
            } else if  (nftType == 2) {
                // Mango Sticky Rice
                require(mangoStickyRiceSupply + quantity <= mangoStickyRiceMaxSupply, "Mint order exceeds Mango Sticky Rice max supply" );
                mangoStickyRiceSupply++;
            } else if  (nftType == 3) {
                // Tom Yum
                require(tomYumSupply + quantity <= tomYumMaxSupply, "Mint order exceeds Tom Yum max supply" );
                tomYumSupply++;
            } else if  (nftType == 4) {
                // Set Meal
                require(setMealSupply + quantity <= setMealMaxSupply, "Mint order exceeds Set Meal max supply" );
                setMealSupply++;
            } 

            uint256 tokenId = _tokenIdCounter.current();
            _tokenIdCounter.increment();
            _safeMint(to, tokenId);
            _setTokenURI(tokenId, uri);
        }
    }

    function setMaxSupply(uint256 _coconutMaxSupply, uint256 _mangoStickyRiceMaxSupply, uint256 _tomYumMaxSupply, uint256 _setMealMaxSupply) public onlyOwner {
        uint256 _maxSupply = _coconutMaxSupply + _mangoStickyRiceMaxSupply + _tomYumMaxSupply + _setMealMaxSupply;
        require(_maxSupply >= totalSupply(), "Specified supply is lower than current balance" );
        require(!isContractLocked, "Contract is locked");

        coconutMaxSupply = _coconutMaxSupply;
        mangoStickyRiceMaxSupply = _mangoStickyRiceMaxSupply;
        tomYumMaxSupply = _tomYumMaxSupply;
        setMealMaxSupply = _setMealMaxSupply;
    }

    function setSupply(uint256 _coconutSupply, uint256 _mangoStickyRiceSupply, uint256 _tomYumSupply, uint256 _setMealSupply) public onlyOwner {
        require(!isContractLocked, "Contract is locked");

        coconutSupply = _coconutSupply;
        mangoStickyRiceSupply = _mangoStickyRiceSupply;
        tomYumSupply = _tomYumSupply;
        setMealSupply = _setMealSupply;
    }

    function maxSupply() public view virtual returns (uint256) {
        return coconutMaxSupply + mangoStickyRiceMaxSupply + tomYumMaxSupply + setMealMaxSupply;
    }

    function totalSupply() public view virtual returns (uint256) {
        return coconutSupply + mangoStickyRiceSupply + tomYumSupply + setMealSupply;
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function lockContract() public virtual onlyOwner {
        require(!isContractLocked, "Contract is locked");
        isContractLocked = true;
    }

    function setTokenURI(uint256 tokenId, string memory uri) public virtual onlyOwner {
        require(!isContractLocked, "Contract is locked");
        _setTokenURI(tokenId, uri);
    }

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function burn(uint256 tokenId) public virtual onlyOwner {
        require(!isContractLocked, "Contract is locked");
        _burn(tokenId);
    }

    function withdraw() external payable onlyOwner {
        (bool success, ) = payable(msg.sender).call{value: address(this).balance}("");
        require(success);
    }
}