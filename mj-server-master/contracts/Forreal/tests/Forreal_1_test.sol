// SPDX-License-Identifier: GPL-3.0
    
pragma solidity >=0.4.22 <0.9.0;

// This import is automatically injected by Remix
import "remix_tests.sol"; 

// This import is required to use custom transaction context
// Although it may fail compilation in 'Solidity Compiler' plugin
// But it will work fine in 'Solidity Unit Testing' plugin
import "remix_accounts.sol";
import "hardhat/console.sol";

import "../Forreal.sol";

contract testSuite {
    Forreal forreal;

    address account0 = TestsAccounts.getAccount(0);
    address account1 = TestsAccounts.getAccount(1);

    function beforeAll() public {
        forreal = new Forreal();
    }

    function testMint() public {
        try forreal.safeMint(account0, "") {
            Assert.ok(true, "Success");
        } catch Error(string memory reason) {
            console.log(reason);
            Assert.ok(false, "failed unexpected");
        } 
    }
}
