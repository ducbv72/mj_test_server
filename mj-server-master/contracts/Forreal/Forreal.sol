// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract Forreal is ERC721, ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;
    bool onlyOwnerCanTransfer = true;

    Counters.Counter public _tokenIdCounter;

    constructor() ERC721("Forreal Certificate of Authenticity", "FORREAL") {}

    function setOnlyOwnerCanTransfer(bool _onlyOwnerCanTransfer) public onlyOwner {
        onlyOwnerCanTransfer = _onlyOwnerCanTransfer;
    }

    function safeMint(address to, string memory uri) public onlyOwner {
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
    }

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override
    {
        if (onlyOwnerCanTransfer) {
            require(owner() == _msgSender(), "Only owner can transfer");
        }
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function transferByOwner(address from, address to, uint256 tokenId)
        public
        onlyOwner
    {
        _transfer(from, to, tokenId);
    }

    function burn(uint256 tokenId) public virtual onlyOwner {
        _burn(tokenId);
    }

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function withdraw() external payable onlyOwner {
        (bool success, ) = payable(msg.sender).call{value: address(this).balance}("");
        require(success);
    }
}