const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const MongoStore = require('connect-mongodb-session')(session);
const MongoClient = require('mongodb').MongoClient;
const helmet = require('helmet');
const colors = require('colors');
const Sentry = require('@sentry/node');
const mongodbUri = require('mongodb-uri');
let handlebars = require('express-handlebars');
const paginate = require('handlebars-paginate');
const passport = require('passport');
const Handlebars = require('handlebars');
const JwtStrategy = require('passport-jwt').Strategy,
  ExtractJwt = require('passport-jwt').ExtractJwt;
const { Server } = require('socket.io');
const http = require('http');
const json2xls = require('json2xls');
const cors = require('cors');

const common = require('./lib/common');

const socket = require('./services/socket');
const { handlebarsHelper } = require('./lib/handleBarsHelper');
const { SENTRY_DSN } = require('./config/sentry');
// Init firebase. Do not remove this line
const { adminFirebase } = require('./config/firebase');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

Handlebars.registerHelper('paginate', paginate);

const NODE_ENV = process.env.NODE_ENV;

if (NODE_ENV !== 'development' && NODE_ENV !== 'test') {
  Sentry.init({
    dsn: SENTRY_DSN,
    environment: NODE_ENV,
    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
  });
}

// Validate our settings schema
const Ajv = require('ajv');
const ajv = new Ajv({ useDefaults: true });

const baseConfig = ajv.validate(
  require('./config/baseSchema'),
  require('./config/settings.json'),
);
if (baseConfig === false) {
  console.log(colors.red(`settings.json incorrect: ${ajv.errorsText()}`));
  process.exit(2);
}

// get config
let config = common.getConfig();

// Validate the payment gateway config
if (config.paymentGateway === 'paypal') {
  const paypalConfig = ajv.validate(
    require('./config/paypalSchema'),
    require('./config/paypal.json'),
  );
  if (paypalConfig === false) {
    console.log(colors.red(`PayPal config is incorrect: ${ajv.errorsText()}`));
    process.exit(2);
  }
}
if (config.paymentGateway === 'stripe') {
  const stripeConfig = ajv.validate(
    require('./config/stripeSchema'),
    require('./config/stripe.json'),
  );
  if (stripeConfig === false) {
    console.log(colors.red(`Stripe config is incorrect: ${ajv.errorsText()}`));
    process.exit(2);
  }
}
if (config.paymentGateway === 'authorizenet') {
  const authorizenetConfig = ajv.validate(
    require('./config/authorizenetSchema'),
    require('./config/authorizenet.json'),
  );
  if (authorizenetConfig === false) {
    console.log(
      colors.red(`Authorizenet config is incorrect: ${ajv.errorsText()}`),
    );
    process.exit(2);
  }
}

// require the routes
const adminDashboard = require('./admin-dashboard/routes/v4');
const apiMobile = require('./mobile/routes/v4');

const app = express();

const swaggerOptions = {
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'MJ API',
      description: 'MJ API documentation',
      servers: [process.env.APP_URL],
    },
    basePath: '/',
    schemes: ['https', 'http'],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  },
  apis: ['./mobile/routes/v4/*.js', './admin-dashboard/routes/v4/*.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use(json2xls.middleware);
app.use(cors()); // Use this after the variable declaration
// view engine setup
app.set('views', path.join(__dirname, '/views'));
app.engine(
  'hbs',
  handlebars({
    extname: 'hbs',
    layoutsDir: path.join(__dirname, 'views', 'layouts'),
    defaultLayout: 'layout.hbs',
    partialsDir: [path.join(__dirname, 'views')],
  }),
);
app.set('view engine', 'hbs');

// helpers for the handlebar templating platform
handlebars = handlebarsHelper(config);

// session store
const store = new MongoStore({
  uri: config.databaseConnectionString,
  collection: 'sessions',
});

app.enable('trust proxy');
app.use(helmet());
app.set('port', process.env.PORT || 80);
app.use(logger('dev'));
app.use(
  express.json({
    verify: (req, res, buf) => {
      // Now the raw body is available on req.rawBody
      req.rawBody = buf;
    },
  }),
);
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser('5TOCyfH3HuszKGzFZntk'));
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: 'pAgGxo8Hzg7PFlv1HpO8Eg0Y6xtP7zYx',
    cookie: {
      path: '/',
      httpOnly: true,
      maxAge: 3600000 * 24,
    },
    store: store,
  }),
);

// serving static content
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views', 'themes')));

// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use(
  '/api-docs',
  common.restrict,
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocs),
);

// Make stuff accessible to our router
app.use((req, res, next) => {
  req.handlebars = handlebars;
  next();
});

// update config when modified
app.use((req, res, next) => {
  next();
  if (res.configDirty) {
    config = common.getConfig();
    app.config = config;
  }
});

// Ran on all routes
app.use((req, res, next) => {
  res.setHeader('Cache-Control', 'no-cache, no-store');
  next();
});

// Passport configuration: use JWT Bearer scheme, config secret
const passportOpts = {};
passportOpts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
passportOpts.secretOrKey = config.jwtSecret;

// Passport auth: find customer with email in JWT claim
passport.use(
  new JwtStrategy(passportOpts, async function (jwt_payload, done) {
    //this is allow access from file management system
    if (jwt_payload.is_from_file_management) {
      return done(null, true);
    }
    const customers = await app.db.customers
      .find({
        customerId: jwt_payload.customerId,
      })
      .toArray();

    if (customers.length > 0) {
      return done(null, customers[0]);
    }

    return done(null, false);
  }),
);
// setup the routes
app.use('/', adminDashboard);
app.use('/', apiMobile);
// app.use(
//     '/api-docs',
//     common.restrict,
//     function (req, res, next) {
//         next();
//     },
//     swaggerUi.serve,
//     swaggerUi.setup(swaggerDocs),
// );
// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    console.error(colors.red(err.stack));
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      helpers: handlebars.helpers,
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  console.error(colors.red(err.stack));
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
    helpers: handlebars.helpers,
  });
});

// Nodejs version check
const nodeVersionMajor = parseInt(
  process.version.split('.')[0].replace('v', ''),
);
if (nodeVersionMajor < 14) {
  console.log(
    colors.red(
      `Please use Node.js version 14.x or above. Current version: ${nodeVersionMajor}`,
    ),
  );
  process.exit(2);
}

app.on('uncaughtException', (err) => {
  console.error(colors.red(err.stack));
  process.exit(2);
});

app.config = config;
app.port = app.get('port');

const option = {
  useUnifiedTopology: true,
  socketTimeoutMS: 9000000,
  keepAlive: true,
};

if (process.env.NODE_ENV !== 'test') {
  MongoClient.connect(
    config.databaseConnectionString,
    option,
    (err, client) => {
      // On connection error we display then exit
      if (err) {
        console.log(colors.red('Error connecting to MongoDB: ' + err));
        process.exit(2);
      }

      console.log(
        `Connected to MongoDb, environment is ${process.env.NODE_ENV}`.green,
      );

      // select DB
      const dbUriObj = mongodbUri.parse(config.databaseConnectionString);
      let db;
      // if in testing, set the testing DB
      if (process.env.NODE_ENV === 'test') {
        db = client.db('testingdb');
      } else {
        db = client.db(dbUriObj.database);
      }

      // setup the collections
      common.dbInitCollection(db);

      // add db to app for routes
      app.dbClient = client;
      app.db = db;

      const httpServer = http.createServer(app);
      global.io = new Server();
      global.io.attach(httpServer);
      socket.socket(app);

      httpServer.listen(app.port, () => {
        console.log('HTTP Server running on port: ', app.port);
      });
    },
  );
}

module.exports = app;
