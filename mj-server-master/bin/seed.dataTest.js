const { MongoClient, ObjectId } = require('mongodb');
const common = require('../lib/common');
const fs = require('fs');
const rawTestData = fs.readFileSync('./bin/testdata.json', 'utf-8');
const mockData = JSON.parse(rawTestData);

const AWS = require('aws-sdk');
const Config = require('../config');
const emptyS3Directory = require('../services/deleteFolderS3');

const s3 = new AWS.S3({
  accessKeyId: Config.S3.accessKeyId,
  secretAccessKey: Config.S3.secretAccessKey,
});

MongoClient.connect(
  process.env.MONGODB_TESTING_CONNECTION,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  async function (err, client) {
    if (err) {
      console.log(err);
    }
    await emptyS3Directory(Config.S3.bucket, s3, 'images/testing/');
    const db = client.db('testingdb');
    common.dbInitCollection(db);
    //importData mock data
    const importData = async () => {
      try {
        await db.collection('tags').insertMany(
          mockData.tags.modelData.map((e) => ({
            _id: new ObjectId(e._id),
            name: e.name,
            createdAt: new Date(),
            updatedAt: new Date(),
          })),
        );
        await db.collection('posts').insertMany(
          mockData.posts.modelData.map((e) => ({
            ...e,
            posted_date: new Date(),
            last_updated_date: new Date(),
          })),
        );
        await db.collection('qrcodes').insertMany(
          mockData.qrcodes.map((e) => ({
            ...e,
            _id: new ObjectId(e._id),
            productId: new ObjectId(e.productId),
          })),
        );
        await db.collection('products').insertMany(mockData.stockXProduct.data);
        await db.collection('products').insertMany(mockData.products);
        await db.collection('stores').insertMany(mockData.stores);
        await db.collection('artists').insertMany(mockData.artists);
        await db.collection('orders').insertMany(mockData.orders);
        await db
          .collection('upcommingProducts')
          .insertMany(mockData.upcommingProducts);
        await db
          .collection('digitalContents')
          .insertMany(mockData.digitalContents);
        console.log('Data For Testing Has Been Imported...'.green.inverse);
        process.exit();
      } catch (err) {
        console.error(err);
      }
    };

    // Delete data
    const deleteData = async () => {
      try {
        await db.collection('tags').deleteMany();
        await db.collection('posts').deleteMany();
        await db.collection('reports').deleteMany();
        await db.collection('likers').deleteMany();
        await db.collection('follows').deleteMany();
        await db.collection('products').deleteMany();
        await db.collection('stores').deleteMany();
        await db.collection('users').deleteMany();
        await db.collection('digitalContents').deleteMany();
        await db.collection('artists').deleteMany();
        await db.collection('customers').deleteMany();
        await db.collection('campaigns').deleteMany();
        await db.collection('campaignParticipates').deleteMany();
        await db.collection('userSettings').deleteMany();
        await db.collection('upcommingProducts').deleteMany();
        await db.collection('orders').deleteMany();
        await db.collection('qrcodes').deleteMany();
        console.log('Data For Testing Has Been Destroyed...'.red.inverse);
        process.exit();
      } catch (err) {
        console.error(err);
      }
    };
    if (process.argv[2] === '-i') {
      await importData();
    } else if (process.argv[2] === '-d') {
      await deleteData();
    }
    client.close();
  },
);
